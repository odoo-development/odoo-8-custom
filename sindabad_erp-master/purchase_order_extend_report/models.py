# -*- coding: utf-8 -*-

from openerp.osv import fields, osv
from openerp.tools.translate import _

class purchase_order(osv.osv):

    _inherit = "purchase.order"

    def _get_po_payment(self, cr, uid, ids, field_name, args, context=None):
        res = dict.fromkeys(ids, 0)
        for po_obj in self.browse(cr, uid, ids, context=context):

            po_payment = 0
            account_invoice = self.pool.get('account.invoice')
            for ai in po_obj.invoice_ids:
                po_payment += (ai.amount_total-ai.residual)
                # print 'amount_total, residual, po_payment ==', ai.amount_total, ai.residual, po_payment
            res[po_obj.id] = po_payment
        return res

    def _get_last_payment_date(self, cr, uid, ids, field_name, args, context=None):
        res = dict.fromkeys(ids, 0)
        for po_obj in self.browse(cr, uid, ids, context=context):

            last_payment_date = ''
            account_invoice = self.pool.get('account.invoice')
            account_invoice_obj = account_invoice.search(cr, uid, [('journal_id', '=', po_obj.journal_id.id)], order='write_date', context=context)
            last_payment_date = account_invoice.browse(cr, uid, account_invoice_obj[0]).write_date
            res[po_obj.id] = last_payment_date
        return res

    def _get_customer_name(self, cr, uid, ids, field_name, args, context=None):
        res = dict.fromkeys(ids, 0)
        for po_obj in self.browse(cr, uid, ids, context=context):

            customer_name = ''
            sale_order = self.pool.get('sale.order')
            sale_order_obj = sale_order.search(cr, uid, [('client_order_ref', '=', po_obj.client_order_ref)], context=context)
            if sale_order_obj:
                customer_name = sale_order.browse(cr, uid, sale_order_obj[0]).partner_id.name
                res[po_obj.id] = customer_name
            else:
                res[po_obj.id] = ''
        return res

    _columns = {
        'po_payment': fields.function(_get_po_payment, string='Purchase Order Payment Amount', type='integer'),
        'last_payment_date': fields.function(_get_last_payment_date, string='Last Payment Date', type='char'),
        'customer_name': fields.function(_get_customer_name, string='Customer Name', type='char'),
    }


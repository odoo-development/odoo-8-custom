{
    'name': 'WMS Manifest Extends',
    'version': '8.0.0.6.0',
    'author': "Shahjalal Hossain",
    'category': 'SMS',
    'summary': 'WMS Manifest Extends',
    'depends': ['base', 'odoo_magento_connect', 'account', 'sale', 'purchase', 'wms_manifest', 'send_sms_on_demand'],
    'data': [
        'security/manifest_extends.xml',
        'security/ir.model.access.csv',

        'wizard/manifest_with_full_delivered.xml',
        'wizard/manifest_with_return.xml',
        'wizard/manifest_with_full_return.xml',
        'wizard/manifest_reschedule.xml',
        'wms_manifest_view_extend.xml',
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}

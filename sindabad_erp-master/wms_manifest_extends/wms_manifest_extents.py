from datetime import datetime, timedelta
import time
from datetime import date
from openerp import SUPERUSER_ID
from openerp.osv import fields, osv
from openerp.tools.translate import _
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT
import openerp.addons.decimal_precision as dp
from openerp.exceptions import except_orm, Warning, RedirectWarning
from openerp import workflow
from openerp import api
import datetime
import json


class wms_manifest_outbound(osv.osv):
    _inherit = "wms.manifest.outbound"

    _columns = {
        'man_name': fields.char('Manifest No'),
        'delivery_time': fields.char('Delivery Time'),
    }


class wms_manifest_outbound_line(osv.osv):
    _inherit = "wms.manifest.outbound.line"

    def _get_return_amount_fnc(self, cr, uid, ids, prop, unknow_none, context=None):
        res = {}

        """
        for partner in self.browse(cr, uid, ids, context=context):
            res[partner.id] = self._display_address(cr, uid, partner, context=context)
        """

        return res

    _columns = {
        'return_reason': fields.char('Return Reason'),
        # 'return_amount': fields.char('Return Amount')
        # 'return_amount': fields.function(_get_return_amount_fnc, type="char", string='Return Amount'),
    }


class wms_manifest_process(osv.osv):
    _inherit = "wms.manifest.process"

    _columns = {
        'name': fields.char('Manifest No'),
        'assigned_user_ids': fields.many2one('res.users', string='Assigned User'),
    }

    @api.model
    def create(self, vals):

        record = super(wms_manifest_process, self).create(vals)
        name = "MANIFEST0" + str(record.id)
        record.name = name

        return record

    def create_delivery_from_manifest(self, cr, uid, ids, context=None):

        # record = super(wms_manifest_process, self).confirm_wms_manifest(cr, uid, ids, context=context)

        # wms_man = self.pool.get("wms.manifest.process").browse(cr, uid, ids, context=context)
        inv_obj = self.pool.get("account.invoice")
        wms_man_out = self.pool.get("wms.manifest.outbound")

        # for line in wms_man.picking_line:
        invoice_id = ids
        inv = inv_obj.browse(cr, uid, invoice_id, context=context)

        name = context.get('name')  # Magento Ref
        man_name = context.get('man_name')  # Manifest No
        delivery_date = context['delivery_date']

        if inv.date_invoice >  delivery_date:
            raise osv.except_osv(_('Warning!'), _('Delivery date can not less than invoice date!'))
        out_data = {
            'name': name,
            'man_name': man_name,
            'delivery_date': delivery_date,
            'delivery_time': context['delivery_time'],
            'state': 'confirm',
            'confirm': True,
            'confirm_by':uid,
            'source': context.get('source') if context.get('source') else 'Web',
            'delivery_done_by': uid,
            'delivery_done_datetime': datetime.datetime.today(),
        }

        outbound_line_list = list()

        for inv_line in inv.invoice_line:
            if float(inv_line.quantity)>0.00:
                outbound_line_list.append([0, False, {
                    'invoice_id': invoice_id[0],
                    'magento_no': name,
                    'description': str(inv_line.name),
                    'product_id': inv_line.product_id.id,
                    'amount': float(inv_line.price_subtotal),
                    'quantity': float(inv_line.quantity),
                    'delivered': float(inv_line.quantity)
                }])

        out_data['outbound_line'] = outbound_line_list

        wms_man_id = wms_man_out.create(cr, uid, out_data, context=context)
        wms_man_out.write(cr, uid, wms_man_id, {'state': 'confirm','confirm': True}, context=context)

        return True

    def process_confirm(self, cr, uid, ids, data, context=None):

        magento_no = data['name']
        query_for_SO = self.pool['sale.order'].search(cr, uid, [('client_order_ref', '=', magento_no)])

        if len(query_for_SO) > 0:

            sales = self.pool.get('sale.order').browse(cr, uid, query_for_SO, context=context)[0]

            sale_order = self.pool.get("sale.order")
            for so_item in sales:
                if so_item.state != 'cancel':
                    if so_item.delivered_amount == so_item.amount_total:
                        dispatch_wms_query = "UPDATE sale_order SET invoiced_dispatch=TRUE  WHERE id='{1}'".format(uid,
                                                                                                                   so_item.id)
                        cr.execute(dispatch_wms_query)
                        cr.commit()
                        # sale_order.action_status(cr, uid, [so_item.id, ], 'dispatched', context=context)
                    else:
                        dispatch_wms_query = "UPDATE sale_order SET partially_invoiced_dispatch=TRUE  WHERE id='{1}'".format(
                            uid, so_item.id)
                        cr.execute(dispatch_wms_query)
                        cr.commit()
                        # sale_order.action_status(cr, uid, [so_item.id, ], 'partially_dispatched', context=context)
        # --------------------------------------------------

        # if the last invoice then set state confirm
        man_pro_obj = self.pool.get("wms.manifest.line")
        inv_obj = self.pool.get("account.invoice")
        man_pro_line_list = man_pro_obj.search(cr, uid, [('invoice_id', '=', data['invoice_id']),('reschedule', '=', False)], context=context)
        man_process_line = man_pro_obj.browse(cr, uid, man_pro_line_list, context=context)

        man_process = man_process_line.wms_manifest_id

        if man_process.id is False and data.has_key('wms_manifest_id'):
            man_process = data['wms_manifest_id']

        total_invoices = len(man_process.picking_line)
        processed_inv = 0

        for man_p in man_process.picking_line:

            if str(man_p.manifest_out_status) == "assigned":
                processed_inv += 1

        if total_invoices == processed_inv:

            # call confirm. otherwise "sms" will not go
            # self.pool.get('wms.manifest.process').confirm_wms_manifest(cr, uid, [man_process.id], context=context)

            try:

                confirm_wms_query = "UPDATE wms_manifest_process SET state='confirm', confirm_by='{0}', confirm_time='{1}' WHERE id='{2}'".format(
                    uid, str(fields.datetime.now()), man_process.id)
                cr.execute(confirm_wms_query)
                cr.commit()

                wms_man_lin_query = "UPDATE wms_manifest_line SET delivery_date='{0}', deliver_time='{1}' WHERE invoice_id='{2}' AND magento_no='{3}'".format(data['delivery_date'], data['delivery_time'], data['invoice_id'], data['name'])
                cr.execute(wms_man_lin_query)
                cr.commit()

            except:
                pass
        else:

            wms_man_lin_query = "UPDATE wms_manifest_line SET delivery_date='{0}', deliver_time='{1}' WHERE invoice_id='{2}' AND magento_no='{3}'".format(
                data['delivery_date'], data['delivery_time'], data['invoice_id'], data['name'])
            cr.execute(wms_man_lin_query)
            cr.commit()

        return True


class wms_manifest_line(osv.osv):
    _inherit = "wms.manifest.line"

    _columns = {

        'manifest_out_status': fields.char('Manifest Out Status'),
        'delivery_date': fields.date('Delivery Date'),
        'deliver_time': fields.char('Delivery Time'),
        'reschedule': fields.boolean('Reschedule'),
        'rm_name': fields.char('RM Name'),
        'rm_phone': fields.char('RM Phone'),
        'full_delivered': fields.boolean('Full Delivered'),
        'partial_delivered': fields.boolean('Partial Delivered'),
        'full_return': fields.boolean('Full Return'),



    }

    @api.model
    def create(self, vals):

        record = super(wms_manifest_line, self).create(vals)

        try:

            so_obj = self.pool.get("sale.order")
            so_list = so_obj.search(self._cr, self._uid, [('client_order_ref', '=', record.magento_no)])

            for so in so_obj.browse(self._cr, self._uid, so_list):

                if so.custom_salesperson:
                    record.rm_name = so.custom_salesperson.name
                    record.rm_phone = so.custom_salesperson.mobile
                record.priority_customer = so.new_customer
        except:
            pass

        return record

    def reschedule_invoice_delivery(self, cr, uid, ids, context=None):

        wms_man_line = self.browse(cr, uid, ids, context=context)

        ndr_pro_obj = self.pool.get("ndr.process")
        wms_man_pro = self.pool.get("wms.manifest.process")
        man_line_obj = self.pool.get('wms.manifest.line')
        inv_obj = self.pool.get("account.invoice")
        so_obj = self.pool.get("sale.order")
        inv_list = inv_obj.search(cr, uid, [('id', '=', wms_man_line.invoice_id)])
        inv = inv_obj.browse(cr, uid, inv_list, context=context)
        so_list = so_obj.search(cr, uid, [('client_order_ref', '=', str(inv.name))], context=context)
        so = so_obj.browse(cr, uid, so_list, context=context)

        if ndr_pro_obj.search(cr, uid, [('invoice_id', '=', inv.id)]) and wms_man_line.reschedule:
            raise osv.except_osv(_('Warning!'),_('NDR Process Already done!!! Can not do again!'))

        if inv.number:
            ndr_data = dict()
            ndr_data['invoice_number'] = inv.number
            ndr_data['invoice'] = inv
            ndr_data['magento_number'] = str(inv.name)
            ndr_data['order_number'] = so.name
            ndr_data['order_id'] = so.id
            ndr_data['state'] = 'draft'
            ndr_data['ndr_reason'] = 'Created from outbound'
            ndr_data['source'] = context['source']

            inv_line_list = list()

            for invoice_line in inv.invoice_line:
                if invoice_line.quantity > 0.00:
                    inv_line_list.append([0, False, {
                        'product': str(invoice_line.name),
                        'product_id': int(invoice_line.product_id.id),
                        'product_ean': str(invoice_line.product_id.ean13) if invoice_line.product_id.ean13 else '',
                        'quantity': invoice_line.quantity,
                        # 'received_quantity': 0,

                    }])

            ndr_data['ndr_process_line'] = inv_line_list

            # ndr.process
            ndr_pro_id = ndr_pro_obj.create(cr, uid, ndr_data, context=context)
            ndr_pro_obj.confirm_ndr(cr, uid, [ndr_pro_id], context=context)

            # status assigned
            data = dict()
            data['invoice_id'] = inv.id
            data['name'] = str(inv.name)
            data['delivery_date'] = datetime.datetime.now().strftime('%Y-%m-%d')
            data['delivery_time'] = ''
            data['wms_manifest_id'] = man_line_obj.browse(cr, uid, ids, context=context).wms_manifest_id

            source = context.get('source') if context.get('source') else 'Web'
            delivery_done_by = uid
            delivery_done_datetime = datetime.datetime.today()

            man_line_obj.write(cr, uid, ids, {'manifest_out_status': 'assigned','reschedule':True,'source':source,
                                              'delivery_done_by':delivery_done_by,'delivery_done_datetime':delivery_done_datetime}, context=context)

            wms_man_pro.process_confirm(cr, uid, ids, data, context=context)

        else:
            raise osv.except_osv(_('Warning!'),_('The invoice is not validate yet!!! Please validate the invoice'))


        return True

    def action_invoice_full_delivered(self, cr, uid, ids, context=None):

        manifest_line = self.browse(cr, uid, ids, context=context)

        context['name'] = str(manifest_line.magento_no)
        context['man_name'] = str(manifest_line.wms_manifest_id.name)
        context['invoice_id'] = manifest_line.invoice_id
        context['magento_no'] = str(manifest_line.magento_no)
        context['manifest_line_id'] = manifest_line.id
        context['source'] = context.get('source') if context.get('source') else 'Web'

        source = context['source']
        delivery_done_by = uid
        delivery_done_datetime = datetime.datetime.today()

        self.pool.get("wms.manifest.process").create_delivery_from_manifest(cr, uid, [manifest_line.invoice_id], context=context)

        self.write(cr, uid, ids, {'manifest_out_status': 'assigned','full_delivered':True,'source':source,
                                              'delivery_done_by':delivery_done_by,'delivery_done_datetime':delivery_done_datetime}, context=context)

        # --------------------------------------------------
        data = dict()
        data['name'] = str(manifest_line.magento_no)
        data['invoice_id'] = manifest_line.invoice_id
        data['delivery_date'] = context['delivery_date']
        data['delivery_time'] = context['delivery_time']
        self.pool.get('wms.manifest.process').process_confirm(cr, uid, ids, data, context=None)
        # --------------------------------------------------

        return True


class manifest_with_full_return(osv.osv):
    _name = "manifest.with.full.return"
    _description = "Manifest with full return"

    _columns = {
        'invoice_id': fields.integer('Invoice ID'),
        'order_no': fields.char('Order No'),
        'return_reason': fields.selection([
            ('cash_problems', 'Cash Problems'),
            ('customer_did_not_ordered', 'Customer Did Not Ordered'),
            ('customer_not_interested', 'Customer Not Interested'),
            ('customer_not_reachable', 'Customer Not Reachable'),
            ('customer_wants_full_delivery', 'Customer Wants Full Delivery'),
            ('customer_wants_high_quality', 'Customer Wants High Quality'),
            ('customer_wants_low_quality', 'Customer Wants Low Quality'),
            ('fake_order', 'Fake Order'),
            ('late_delivery', 'Late Delivery'),
            ('less_qty_required', 'Less Qty Required'),
            ('more_qty_required', 'More Qty Required'),
            ('office_closed', 'Office Closed'),
            ('pack_size_issue', 'Pack Size Issue'),
            ('price_issue', 'Price Issue'),
            ('product_damaged', 'Product Damaged'),
            ('product_mismatch', 'Product Mismatch'),
            ('returned_due_to_other_products', 'Returned Due to Other Products'),
            ('wrong_address', 'Wrong Address'),
            ('wrong_order_placed', 'Wrong Order Placed'),
            ('wrongly_packed', 'Wrongly Packed'),
        ], 'Return Reason', select=True),
        'delivery_line': fields.one2many('manifest.with.full.return.line', 'deliver_line_id', 'Delivery Line', required=True),
    }

    def default_get(self, cr, uid, fields, context=None):
        # product_pricelist = self.pool.get('product.pricelist')
        if context is None:
            context = {}
        res = super(manifest_with_full_return, self).default_get(cr, uid, fields, context=context)

        man_line_obj = self.pool.get("wms.manifest.line")
        man_line = man_line_obj.browse(cr, uid, context['active_id'], context=context)

        res['invoice_id'] = man_line.invoice_id
        res['order_no'] = man_line.magento_no

        inv_obj = self.pool.get("account.invoice")
        inv = inv_obj.browse(cr, uid, [man_line.invoice_id], context=context)

        items = list()

        for line in inv.invoice_line:
            if line.quantity > 0.00:
                item = {
                    'product_id': line.product_id.id,
                    'description': line.name,
                    'quantity': line.quantity,
                    'amount': float(line.price_subtotal),
                    'delivered': line.quantity
                }

                if line.product_id.default_code and line.product_id.default_code != '':
                    items.append(item)

        res.update(delivery_line=items)

        return res

    def create_deliver_with_full_return(self, cr, uid, ids, context=None):

        delivery_return = self.browse(cr, uid, ids, context=context)
        context['source']= context.get('source') if context.get('source') else 'Web'

        man_line_obj = self.pool.get('wms.manifest.line')
        man_line_list = man_line_obj.search(cr, uid, [('invoice_id', '=', delivery_return.invoice_id),('reschedule', '=', False)], context=context)
        man_line = man_line_obj.browse(cr, uid, man_line_list, context=context)

        # delivery_return.delivery_line.description

        process = True
        if not delivery_return.return_reason:
            process = False

        # if len(man_line) >1:
        #     raise osv.except_osv(_('Warning!'), _('Multiple manifest create using same invoice.'))


        if process:
            out_data = {
                'name': delivery_return.order_no,
                'man_name': str(man_line.wms_manifest_id.name),
                'delivery_date': datetime.datetime.now().strftime('%Y-%m-%d'),
                'source': context.get('source') if context.get('source') else 'Web',
                'delivery_done_by':uid,
                'delivery_done_datetime':datetime.datetime.today(),
            }

            outbound_line_list = list()

            for line in delivery_return.delivery_line:
                if float(line.quantity)>0.00:
                    outbound_line_list.append([0, False, {
                        'invoice_id': delivery_return.invoice_id,
                        'magento_no': delivery_return.order_no,
                        'description': str(line.description),
                        'product_id': line.product_id,
                        'amount': float(line.amount),
                        'quantity': float(line.quantity),
                        'delivered': 0,
                        'return_reason': str(delivery_return.return_reason),
                    }])

            out_data['outbound_line'] = outbound_line_list
            context['manifest_line_id'] = man_line.id

            wms_man_out = self.pool.get("wms.manifest.outbound")
            man_out_id = wms_man_out.create(cr, uid, out_data, context=context)

            returned_qty = 0
            man_out = wms_man_out.browse(cr, uid, [man_out_id], context=context)
            for line_return in man_out.outbound_line:
                if line_return.return_qty > 0:
                    returned_qty += line_return.return_qty

            source = context['source']
            delivery_done_by = uid
            delivery_done_datetime = datetime.datetime.today()

            if man_out_id and returned_qty > 0:
                context['full_return']=True
                wms_man_out.receipt_return(cr, uid, [man_out_id], context=context)

                man_line_obj.write(cr, uid, man_line_list, {'manifest_out_status': 'assigned','full_return':True,'source':source,
                                              'delivery_done_by':delivery_done_by,'delivery_done_datetime':delivery_done_datetime}, context=context)

            # --------------------------------------------------
            data = dict()
            data['name'] = str(man_line.magento_no)
            data['invoice_id'] = delivery_return.invoice_id
            data['delivery_date'] = datetime.datetime.today().date().strftime("%Y-%m-%d")
            data['delivery_time'] = ''
            self.pool.get('wms.manifest.process').process_confirm(cr, uid, ids, data, context=None)
            # --------------------------------------------------

        else:
            # pop up error
            raise osv.except_osv(_('Warning!'),_('Reason can not be blank!!!'))

        return True


class manifest_with_full_return_line(osv.osv):
    _name = "manifest.with.full.return.line"
    _description = "Manifest with full return line"

    _columns = {
        'product_id': fields.integer('Product ID'),
        'description': fields.char('Description'),
        'quantity': fields.float('Quantity'),
        'delivered': fields.float('Delivered'),
        'return': fields.float('Return'),
        'amount': fields.float('Amount'),
        # 'return_reason': fields.char('Return Reason'),
        'return_reason': fields.selection([
            ('cash_problems', 'Cash Problems'),
            ('customer_did_not_ordered', 'Customer Did Not Ordered'),
            ('customer_not_interested', 'Customer Not Interested'),
            ('customer_not_reachable', 'Customer Not Reachable'),
            ('customer_wants_full_delivery', 'Customer Wants Full Delivery'),
            ('customer_wants_high_quality', 'Customer Wants High Quality'),
            ('customer_wants_low_quality', 'Customer Wants Low Quality'),
            ('fake_order', 'Fake Order'),
            ('late_delivery', 'Late Delivery'),
            ('less_qty_required', 'Less Qty Required'),
            ('more_qty_required', 'More Qty Required'),
            ('office_closed', 'Office Closed'),
            ('pack_size_issue', 'Pack Size Issue'),
            ('price_issue', 'Price Issue'),
            ('product_damaged', 'Product Damaged'),
            ('product_mismatch', 'Product Mismatch'),
            ('returned_due_to_other_products', 'Returned Due to Other Products'),
            ('wrong_address', 'Wrong Address'),
            ('wrong_order_placed', 'Wrong Order Placed'),
            ('wrongly_packed', 'Wrongly Packed'),
        ], 'Return Reason', select=True),
        'deliver_line_id': fields.many2one('manifest.with.full.return', 'Delivery Line', required=True, ondelete='cascade', select=True, readonly=True),
    }


class manifest_with_return(osv.osv):
    _name = "manifest.with.return"
    _description = "Manifest with return"

    _columns = {
        'invoice_id': fields.integer('Invoice ID'),
        'order_no': fields.char('Order No'),
        'delivery_date': fields.date('Delivery Date'),
        'delivery_line': fields.one2many('manifest.with.return.line', 'deliver_line_id', 'Delivery Line', required=True),
        'delivery_time': fields.selection([
            ('07 AM - 08 AM', '07 AM - 08 AM'),
            ('08 AM - 09 AM', '08 AM - 09 AM'),
            ('09 AM - 10 AM', '09 AM - 10 AM'),
            ('10 AM - 11 AM', '10 AM - 11 AM'),
            ('11 AM - 12 PM', '11 AM - 12 PM'),
            ('12 PM - 01 PM', '12 PM - 01 PM'),
            ('01 PM - 02 PM', '01 PM - 02 PM'),
            ('02 PM - 03 PM', '02 PM - 03 PM'),
            ('03 PM - 04 PM', '03 PM - 04 PM'),
            ('04 PM - 05 PM', '04 PM - 05 PM'),
            ('05 PM - 06 PM', '05 PM - 06 PM'),
            ('06 PM - 07 PM', '06 PM - 07 PM'),
            ('07 PM - 08 PM', '07 PM - 08 PM'),
            ('08 PM - 09 PM', '08 PM - 09 PM'),
            ('09 PM - 10 PM', '09 PM - 10 PM'),
        ], 'Deliver Time', select=True),
    }

    def default_get(self, cr, uid, fields, context=None):
        # product_pricelist = self.pool.get('product.pricelist')
        if context is None:
            context = {}
        res = super(manifest_with_return, self).default_get(cr, uid, fields, context=context)

        man_line_obj = self.pool.get("wms.manifest.line")
        man_line = man_line_obj.browse(cr, uid, context['active_id'], context=context)

        res['invoice_id'] = man_line.invoice_id
        res['order_no'] = man_line.magento_no
        res['delivery_date'] = datetime.datetime.today().date().strftime("%Y-%m-%d")

        inv_obj = self.pool.get("account.invoice")
        inv = inv_obj.browse(cr, uid, [man_line.invoice_id], context=context)

        items = list()

        for line in inv.invoice_line:
            if line.quantity>0.00:
                item = {
                    'product_id': line.product_id.id,
                    'description': line.name,
                    'quantity': line.quantity,
                    'amount': float(line.price_subtotal),
                    'delivered': line.quantity
                }

                if line.product_id.default_code and line.product_id.default_code != '':
                    items.append(item)
        abc = json.dumps(items)
        res['line_list']=abc

        res.update(delivery_line=items)

        return res

    def create_deliver_with_return(self, cr, uid, ids, context=None):

        delivery_return = self.browse(cr, uid, ids, context=context)
        context['source']= context.get('source') if context.get('source') else 'Web'

        for line in delivery_return.delivery_line:
            if line.delivered < 0 or line.delivered > line.quantity:
                raise osv.except_osv(_('Warning!'),_('Delivered quantity should be within ordered quantity!!!'))

        man_line_obj = self.pool.get('wms.manifest.line')
        man_line_list = man_line_obj.search(cr, uid, [('invoice_id', '=', delivery_return.invoice_id),('reschedule', '=', False)], context=context)
        man_line = man_line_obj.browse(cr, uid, man_line_list, context=context)

        # delivery_return.delivery_line.description

        # if len(man_line) >1:
        #     raise osv.except_osv(_('Warning!'), _('Multiple manifest create using same invoice.'))


        out_data = {
            'name': delivery_return.order_no,
            'man_name': str(man_line.wms_manifest_id.name),
            'delivery_date': delivery_return.delivery_date,
            'delivery_time': delivery_return.delivery_time,
            'source': context.get('source') if context.get('source') else 'Web',
            'delivery_done_by':uid,
            'delivery_done_datetime':datetime.datetime.today(),

        }

        outbound_line_list = list()

        for line in delivery_return.delivery_line:
            if not line.return_reason and (float(line.quantity)- float(line.delivered)) > 0:
                raise osv.except_osv(_('Warning!'), _('Reason can not be blank!!!'))

            if float(line.quantity) >0.00:
                outbound_line_list.append([0, False, {
                    'invoice_id': delivery_return.invoice_id,
                    'magento_no': delivery_return.order_no,
                    'description': str(line.description),
                    'product_id': line.product_id,
                    'amount': float(line.amount),
                    'quantity': float(line.quantity),
                    'delivered': float(line.delivered),
                    'return_reason': str(line.return_reason),
                }])

        out_data['outbound_line'] = outbound_line_list

        context['manifest_line_id'] = man_line.id

        wms_man_out = self.pool.get("wms.manifest.outbound")
        man_out_id = wms_man_out.create(cr, uid, out_data, context=context)

        returned_qty = 0
        man_out = wms_man_out.browse(cr, uid, [man_out_id], context=context)
        for line_return in man_out.outbound_line:
            if line_return.return_qty > 0:
                returned_qty += line_return.return_qty

        source = context['source']
        delivery_done_by = uid
        delivery_done_datetime = datetime.datetime.today()
        if man_out_id and returned_qty > 0:
            wms_man_out.receipt_return(cr, uid, [man_out_id], context=context)

            man_line_obj.write(cr, uid, man_line_list, {'manifest_out_status': 'assigned','partial_delivered':True,
                                                        'source': source,
                                                        'delivery_done_by': delivery_done_by,
                                                        'delivery_done_datetime': delivery_done_datetime
                                                        }, context=context)
        else:
            man_line_obj.write(cr, uid, man_line_list, {'manifest_out_status': 'assigned', 'full_delivered': True, 'source': source,
             'delivery_done_by': delivery_done_by, 'delivery_done_datetime': delivery_done_datetime}, context=context)


        # --------------------------------------------------
        data = dict()
        data['name'] = str(man_line.magento_no)
        data['invoice_id'] = delivery_return.invoice_id
        data['delivery_date'] = delivery_return.delivery_date
        data['delivery_time'] = delivery_return.delivery_time
        self.pool.get('wms.manifest.process').process_confirm(cr, uid, ids, data, context=None)
            # --------------------------------------------------


        return True


class manifest_with_return_line(osv.osv):
    _name = "manifest.with.return.line"
    _description = "Manifest with return line"

    _columns = {
        'product_id': fields.integer('Product ID'),
        'description': fields.char('Description'),
        'quantity': fields.float('Quantity'),
        'delivered': fields.float('Delivered'),
        'returned_qty': fields.float('Return'),
        'return': fields.float('Return'),
        'amount': fields.float('Amount'),
        # 'return_reason': fields.char('Return Reason'),
        'return_reason': fields.selection([
            ('sku_full_received', 'SKU Full Received'),
            ('cash_problems', 'Cash Problems'),
            ('customer_did_not_ordered', 'Customer Did Not Ordered'),
            ('customer_not_interested', 'Customer Not Interested'),
            ('customer_not_reachable', 'Customer Not Reachable'),
            ('customer_wants_full_delivery', 'Customer Wants Full Delivery'),
            ('customer_wants_high_quality', 'Customer Wants High Quality'),
            ('customer_wants_low_quality', 'Customer Wants Low Quality'),
            ('fake_order', 'Fake Order'),
            ('late_delivery', 'Late Delivery'),
            ('less_qty_required', 'Less Qty Required'),
            ('more_qty_required', 'More Qty Required'),
            ('office_closed', 'Office Closed'),
            ('pack_size_issue', 'Pack Size Issue'),
            ('price_issue', 'Price Issue'),
            ('product_damaged', 'Product Damaged'),
            ('product_mismatch', 'Product Mismatch'),
            ('returned_due_to_other_products', 'Returned Due to Other Products'),
            ('wrong_address', 'Wrong Address'),
            ('wrong_order_placed', 'Wrong Order Placed'),
            ('wrongly_packed', 'Wrongly Packed'),
        ], 'Return Reason', select=True),
        'deliver_line_id': fields.many2one('manifest.with.return', 'Delivery Line', required=True, ondelete='cascade', select=True, readonly=True),
    }

    @api.onchange('delivered')
    def _onchange_return_qty(self):

        self.returned_qty = self.quantity - self.delivered if self.quantity - self.delivered > 0 else 0


class manifest_with_full_delivered(osv.osv):
    _name = "manifest.with.full.delivered"
    _description = "Manifest with full Delivered"

    _columns = {
        'invoice_id': fields.char('Invoice ID'),
        'order_no': fields.char('Order No'),
        'delivery_date': fields.date('Delivery Date'),
        'deliver_time': fields.selection([
            ('07 AM - 08 AM', '07 AM - 08 AM'),
            ('08 AM - 09 AM', '08 AM - 09 AM'),
            ('09 AM - 10 AM', '09 AM - 10 AM'),
            ('10 AM - 11 AM', '10 AM - 11 AM'),
            ('11 AM - 12 PM', '11 AM - 12 PM'),
            ('12 PM - 01 PM', '12 PM - 01 PM'),
            ('01 PM - 02 PM', '01 PM - 02 PM'),
            ('02 PM - 03 PM', '02 PM - 03 PM'),
            ('03 PM - 04 PM', '03 PM - 04 PM'),
            ('04 PM - 05 PM', '04 PM - 05 PM'),
            ('05 PM - 06 PM', '05 PM - 06 PM'),
            ('06 PM - 07 PM', '06 PM - 07 PM'),
            ('07 PM - 08 PM', '07 PM - 08 PM'),
            ('08 PM - 09 PM', '08 PM - 09 PM'),
            ('09 PM - 10 PM', '09 PM - 10 PM'),
        ], 'Deliver Time', select=True),
    }

    def default_get(self, cr, uid, fields, context=None):
        # product_pricelist = self.pool.get('product.pricelist')
        if context is None:
            context = {}
        res = super(manifest_with_full_delivered, self).default_get(cr, uid, fields, context=context)

        man_line_obj = self.pool.get("wms.manifest.line")
        man_line = man_line_obj.browse(cr, uid, context['active_id'], context=context)

        res['invoice_id'] = man_line.invoice_id
        res['order_no'] = man_line.magento_no

        return res

    def create_full_delivery(self, cr, uid, ids, context=None):

        delivery = self.browse(cr, uid, ids, context=context)

        context['delivery_date'] = delivery.delivery_date
        context['delivery_time'] = delivery.deliver_time
        context['source']= context.get('source') if context.get('source') else 'Web'

        man_line_obj = self.pool.get("wms.manifest.line")

        man_line_list = man_line_obj.search(cr, uid, [('invoice_id', '=', delivery.invoice_id), ('magento_no', '=', delivery.order_no),('reschedule', '=', False)])

        if len(man_line_list) > 0:
            man_line_list = [man_line_list[0]]


        man_line_obj.action_invoice_full_delivered(cr, uid, man_line_list, context)

        return True


class manifest_reschedule(osv.osv):

    _name = "manifest.reschedule"
    _description = "Manifest Reschedule"

    _columns = {
        'invoice_id': fields.char('Invoice ID'),
        'manifest_line_id': fields.integer('Manifest Line ID'),
        'ndr': fields.selection([
            ('cash_problems', 'Cash Problems'),
            ('customer_did_not_ordered', 'Customer Did Not Ordered'),
            ('customer_not_interested', 'Customer Not Interested'),
            ('customer_not_reachable', 'Customer Not Reachable'),
            ('customer_wants_full_delivery', 'Customer Wants Full Delivery'),
            ('customer_wants_high_quality', 'Customer Wants High Quality'),
            ('customer_wants_low_quality', 'Customer Wants Low Quality'),
            ('fake_order', 'Fake Order'),
            ('late_delivery', 'Late Delivery'),
            ('less_qty_required', 'Less Qty Required'),
            ('more_qty_required', 'More Qty Required'),
            ('office_closed', 'Office Closed'),
            ('pack_size_issue', 'Pack Size Issue'),
            ('price_issue', 'Price Issue'),
            ('product_damaged', 'Product Damaged'),
            ('product_mismatch', 'Product Mismatch'),
            ('returned_due_to_other_products', 'Returned Due to Other Products'),
            ('wrong_address', 'Wrong Address'),
            ('wrong_order_placed', 'Wrong Order Placed'),
            ('wrongly_packed', 'Wrongly Packed'),
        ], 'Not Delivery Reason', select=True),


    }

    def default_get(self, cr, uid, fields, context=None):
        if context is None:
            context = {}
        res = super(manifest_reschedule, self).default_get(cr, uid, fields, context=context)

        man_line_obj = self.pool.get("wms.manifest.line")
        man_line = man_line_obj.browse(cr, uid, context['active_id'], context=context)

        res['invoice_id'] = man_line.invoice_id
        res['manifest_line_id'] = context['active_id']

        return res



    def reschedule_invoice(self, cr, uid, ids, context=None):

        try:
            data = self.read(cr, uid, ids)[0]
        except:
            data = self.read(cr, uid, ids)
        context['source']= context.get('source') if context.get('source') else 'Web'

        self.pool.get("wms.manifest.line").reschedule_invoice_delivery(cr, uid, data['manifest_line_id'], context)
        return True

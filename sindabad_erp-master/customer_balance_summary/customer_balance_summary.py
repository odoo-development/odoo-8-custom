# Author Mufti Muntasir Ahmed 25-04-2018

from openerp import models, api


class account_move_line(models.Model):
    _inherit = 'res.partner'

    @api.model
    def _report_xls_fields(self):

        return [
            'name',
            'x_classification',
            'x_industry',
            'credit_days',
            'monthly_limit',
            'prev_balance',
            'purchase_amount',
            'total_amount',
            'paid_amount',
            'total_outstanding',
            'days_30_age',
            'days_60_age',
            'days_90_age',
            'more_then_90_days',
            'ait_paid_amount',

        ]

    # Change/Add Template entries
    @api.model
    def _report_xls_template(self):

        return {}

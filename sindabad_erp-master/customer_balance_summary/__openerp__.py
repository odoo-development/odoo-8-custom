# -*- coding: utf-8 -*-
# Author S. M. Sazedul Haque 2018/11/06
{
    'name': "Customer Balance Summary Report",

    'summary': """
        Customer Balance Summary Report """,

    'description': """
        This module will help to link stock location in each stock move
    """,

    'author': "Mufti Muntasir Ahmed",
    'website': "http://zerogravity.com.bd/",

    'category': 'Accounts',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['account', 'sale', 'report_xls'],

    # always loaded
    'data': [
        # 'customer_summary_menu.xml',
        'report/customer_balance_summary_xls.xml',
        'wizard/customer_balance_summary_view.xml',
    ],

}

# Author S. M. Sazedul Haque 2018/11/06

import xlwt
from datetime import datetime, timedelta
import time
from openerp.osv import orm
from openerp.report import report_sxw
from openerp.addons.report_xls.report_xls import report_xls
from openerp.addons.report_xls.utils import rowcol_to_cell, _render
from openerp.tools.translate import translate, _
import logging
_logger = logging.getLogger(__name__)


_ir_translation_name = 'account.move.line.xls'


class account_move_line_xls_parser(report_sxw.rml_parse):

    def __init__(self, cr, uid, name, context):
        super(account_move_line_xls_parser, self).__init__(
            cr, uid, name, context=context)
        move_obj = self.pool.get('res.partner')
        self.context = context
        wanted_list = move_obj._report_xls_fields(cr, uid, context)
        template_changes = move_obj._report_xls_template(cr, uid, context)
        self.localcontext.update({
            'datetime': datetime,
            'wanted_list': wanted_list,
            'template_changes': template_changes,
            '_': self._,
        })

    def _(self, src):
        lang = self.context.get('lang', 'en_US')
        return translate(self.cr, _ir_translation_name, 'report', lang, src) \
            or src


class account_move_line_xls(report_xls):

    def __init__(self, name, table, rml=False, parser=False, header=True, store=False, context=None):
        super(account_move_line_xls, self).__init__(
            name, table, rml, parser, header, store)
        # Cell Styles
        _xs = self.xls_styles
        # header
        rh_cell_format = _xs['bold'] + _xs['fill'] + _xs['borders_all']
        self.rh_cell_style = xlwt.easyxf(rh_cell_format)
        self.rh_cell_style_center = xlwt.easyxf(rh_cell_format + _xs['center'])
        self.rh_cell_style_right = xlwt.easyxf(rh_cell_format + _xs['right'])
        # lines
        aml_cell_format = _xs['borders_all']
        self.aml_cell_style = xlwt.easyxf(aml_cell_format)
        self.aml_cell_style_center = xlwt.easyxf(
            aml_cell_format + _xs['center'])
        self.aml_cell_style_date = xlwt.easyxf(
            aml_cell_format + _xs['left'],
            num_format_str=report_xls.date_format)
        self.aml_cell_style_decimal = xlwt.easyxf(
            aml_cell_format + _xs['right'],
            num_format_str=report_xls.decimal_format)
        # totals
        rt_cell_format = _xs['bold'] + _xs['fill'] + _xs['borders_all']
        self.rt_cell_style = xlwt.easyxf(rt_cell_format)
        self.rt_cell_style_right = xlwt.easyxf(rt_cell_format + _xs['right'])
        self.rt_cell_style_decimal = xlwt.easyxf(
            rt_cell_format + _xs['right'],
            num_format_str=report_xls.decimal_format)

        # XLS Template
        self.col_specs_template = {
            'name': {
                'header': [1, 20, 'text', _render("_('Client Name')")],
                'lines': [1, 0, 'text', _render("line.name or ''")],
                'totals': [1, 0, 'text', None]},
            'x_classification': {
                'header': [1, 20, 'text', _render("_('Classification')")],
                'lines': [1, 0, 'text', _render("line.x_classification or ''")],
                'totals': [1, 0, 'text', None]},
            'x_industry': {
                'header': [1, 20, 'text', _render("_('Industry')")],
                'lines': [1, 0, 'text', _render("line.x_industry or ''")],
                'totals': [1, 0, 'text', None]},
            'credit_days': {
                'header': [1, 20, 'text', _render("_('Approved Credit Days')")],
                'lines': [1, 0, 'text', _render("line.credit_days or ''")],
                'totals': [1, 0, 'text', None]},
            'monthly_limit': {
                'header': [1, 20, 'text', _render("_('Approved Credit limit')")],
                'lines': [1, 0, 'text', _render("line.monthly_limit or ''")],
                'totals': [1, 0, 'text', None]},
            'prev_balance': {
                'header': [1, 20, 'text', _render("_('Previous Balance')")],
                'lines': [1, 0, 'text', _render("prev_balance or ''")],
                'totals': [1, 0, 'text', None]},
            'purchase_amount': {
                'header': [1, 20, 'text', _render("_('Total Purchase Amount')")],
                'lines': [1, 0, 'text', _render("purchase_amount or ''")],
                'totals': [1, 0, 'text', None]},
            'total_amount': {
                'header': [1, 20, 'text', _render("_('Total Amount')")],
                'lines': [1, 0, 'text', _render("total_amount or ''")],
                'totals': [1, 0, 'text', None]},
            'paid_amount': {
                'header': [1, 20, 'text', _render("_('Collected Amount')")],
                'lines': [1, 0, 'text', _render("paid_amount or ''")],
                'totals': [1, 0, 'text', None]},
            'total_outstanding': {
                'header': [1, 20, 'text', _render("_('Total Outstanding')")],
                'lines': [1, 0, 'text', _render("total_outstanding or ''")],
                'totals': [1, 0, 'text', None]},
            'days_30_age': {
                'header': [1, 20, 'text', _render("_('1-30 days')")],
                'lines': [1, 0, 'text', _render("days_30_age or ''")],
                'totals': [1, 0, 'text', None]},
            'days_60_age': {
                'header': [1, 20, 'text', _render("_('30-60 days')")],
                'lines': [1, 0, 'text', _render("days_60_age or ''")],
                'totals': [1, 0, 'text', None]},
            'days_90_age': {
                'header': [1, 20, 'text', _render("_('60-90 days')")],
                'lines': [1, 0, 'text', _render("days_90_age or ''")],
                'totals': [1, 0, 'text', None]},
            'more_then_90_days': {
                'header': [1, 20, 'text', _render("_('More then 90 days')")],
                'lines': [1, 0, 'text', _render("more_then_90_days or ''")],
                'totals': [1, 0, 'text', None]},
            'ait_paid_amount': {
                'header': [1, 20, 'text', _render("_('AIT Amount')")],
                'lines': [1, 0, 'text', _render("ait_paid_amount or ''")],
                'totals': [1, 0, 'text', None]},

        }

    def _lines_get(self, partner):

        start_date = self.context.get('start_date')
        end_date = self.context.get('end_date')

        moveline_obj = self.pool['account.move.line']

        # order_numbers = [str(obj)]
        # purchase_ref_cus = None
        # self.cr.execute("SELECT x_puchase_ref_cus FROM sale_order WHERE name=%s", (order_numbers))
        parent_list = []

        if partner.parent_id:
            parent_list.append(partner.parent_id.id)

            self.cr.execute("select id from  res_partner where customer=True and id=%s", ([partner.parent_id.id]))

            for items in self.cr.fetchall():
                parent_list.append(items[0])
        else:
            parent_list.append(partner.id)

        self.cr.execute("select id from  res_partner where customer=True and parent_id=%s", ([partner.id]))
        for items in self.cr.fetchall():
            parent_list.append(items[0])

        movelines = moveline_obj.search(self.cr, self.uid,
                                        [('partner_id', 'in', parent_list),
                                         ('account_id.type', 'in', ['receivable', 'payable']),
                                         ('state', '<>', 'draft'), ('date', '<=', end_date), ('date', '>=', start_date)
                                         ])

        movelines = moveline_obj.browse(self.cr, self.uid, movelines)

        return movelines

    def outstanding_by_age(self,days_value,parent_list, start_date, no_start=False):



        amount = 0
        new_list=[]
        new_parent_list=[]
        moveline_obj = self.pool['account.move.line']
        inv_obj = self.pool['account.invoice']
        mov_inv_obj = self.pool['account.invoice']
        res_partner = self.pool['res.partner'].browse(self.cr, self.uid, parent_list, context=self.context)
        today_date = datetime.today().date()
        start_date = today_date
        if days_value ==30:
            end_date = today_date - timedelta(days=30)
        elif days_value ==60:
            start_date = today_date - timedelta(days=31)
            end_date = today_date - timedelta(days=60)
        elif days_value ==90:
            start_date = today_date - timedelta(days=61)
            end_date = today_date - timedelta(days=90)
        else:
            end_date =today_date - timedelta(days=91)


        for res in res_partner:
            for c_id in res.child_ids:
                new_list.append(c_id.id)

        new_parent_list = parent_list + new_list

        mov_invoice_obj = mov_inv_obj.search(self.cr, self.uid,
                                     [('partner_id', 'in', new_parent_list),
                                      ('state', '<>', 'cancel')
                                      ])

        mov_invoice_list = mov_inv_obj.browse(self.cr, self.uid, mov_invoice_obj)

        move_list = []

        for item in mov_invoice_list:
            move_list.append(item.move_id.id)
            if item.payment_ids:
                for tes in item.payment_ids:
                    move_list.append(tes.move_id.id)
                for it in item.move_id.line_id:
                    move_list.append(it.id)






        if no_start:
            account_move_obj = moveline_obj.search(self.cr, self.uid,
                                                   [('partner_id', 'in', parent_list),
                                                    ('account_id.type', 'in', ['receivable', 'payable']),
                                                       ('state', '<>', 'draft'),
                                                       ('date', '<=', end_date)
                                                    ])
            invoice_obj = inv_obj.search(self.cr, self.uid,
                                                              [('partner_id', 'in', new_parent_list),
                                                               ('state', '<>', 'cancel'),
                                                               ('date_invoice', '<=', end_date)
                                                               ])
        else:
            account_move_obj = moveline_obj.search(self.cr, self.uid,
                                                   [('partner_id', 'in', parent_list),
                                                    ('account_id.type', 'in', ['receivable', 'payable']),
                                                       ('state', '<>', 'draft'),
                                                       ('date', '<=', start_date),
                                                       ('date', '>=', end_date)
                                                    ])
            invoice_obj = inv_obj.search(self.cr, self.uid,
                                         [('partner_id', 'in', new_parent_list),
                                          ('state', '<>', 'cancel'),
                                          ('date_invoice', '<=', start_date),
                                          ('date_invoice', '>=', end_date )
                                          ])




        invoice_list = inv_obj.browse(self.cr,self.uid,invoice_obj)



        for item in invoice_list:
            if str(item.state) == str('draft'):
                amount +=item.amount_total
            else:
                amount +=item.residual



        days_30_age_obj = moveline_obj.browse(self.cr, self.uid, account_move_obj)
        for line in days_30_age_obj:
            if line.move_id.id not in move_list:
                amount = amount + (line.debit - line.credit)


        return amount

    def _get_aging(self, partner):

        parent_list = []

        if partner.parent_id:
            parent_list.append(partner.parent_id.id)

            self.cr.execute("select id from  res_partner where customer=True and id=%s", ([partner.parent_id.id]))

            for items in self.cr.fetchall():
                parent_list.append(items[0])
        else:
            parent_list.append(partner.id)

        self.cr.execute("select id from  res_partner where customer=True and parent_id=%s", ([partner.id]))
        for items in self.cr.fetchall():
            parent_list.append(items[0])

        string_date = self.context.get('end_date')
        start_date = datetime.strptime(string_date, "%Y-%m-%d")
        days_30_age = self.outstanding_by_age(days_value = 30, parent_list=parent_list,start_date=start_date, no_start=False)
        days_60_age = self.outstanding_by_age(days_value = 60, parent_list=parent_list, start_date=start_date, no_start=False)
        days_90_age = self.outstanding_by_age(days_value = 90, parent_list=parent_list, start_date=start_date, no_start=False)
        more_then_90_days = self.outstanding_by_age(days_value = 91, parent_list=parent_list, start_date=start_date, no_start=True)

        return {
            'days_30_age': days_30_age,
            'days_60_age': days_60_age,
            'days_90_age': days_90_age,
            'more_then_90_days': more_then_90_days
        }

    def _init_balance_get(self, partner):

        parent_list = []

        if partner.parent_id:
            parent_list.append(partner.parent_id.id)

            self.cr.execute("select id from  res_partner where customer=True and id=%s", ([partner.parent_id.id]))

            for items in self.cr.fetchall():
                parent_list.append(items[0])
        else:
            parent_list.append(partner.id)

        self.cr.execute("select id from  res_partner where customer=True and parent_id=%s", ([partner.id]))
        for items in self.cr.fetchall():
            parent_list.append(items[0])

        start_date = self.context.get('start_date')

        moveline_obj = self.pool['account.move.line']
        movelines = moveline_obj.search(self.cr, self.uid,
                                        [('partner_id', 'in', parent_list),
                                         ('account_id.type', 'in', ['receivable', 'payable']),
                                         ('state', '<>', 'draft'), ('date', '<', start_date)],
                                        )

        movelines = moveline_obj.browse(self.cr, self.uid, movelines)

        previous_balance = 0

        for items in movelines:
            previous_balance += (items.debit - items.credit)

        return previous_balance

    def _init_lines_get(self, partner):

        parent_list = []

        if partner.parent_id:
            parent_list.append(partner.parent_id.id)

            self.cr.execute("select id from  res_partner where customer=True and id=%s", ([partner.parent_id.id]))

            for items in self.cr.fetchall():
                parent_list.append(items[0])
        else:
            parent_list.append(partner.id)

        self.cr.execute("select id from  res_partner where customer=True and parent_id=%s", ([partner.id]))
        for items in self.cr.fetchall():
            parent_list.append(items[0])

        start_date = self.context.get('start_date')

        moveline_obj = self.pool['account.move.line']
        movelines = moveline_obj.search(self.cr, self.uid,
                                        [('partner_id', 'in', parent_list),
                                         ('account_id.type', 'in', ['receivable', 'payable']),
                                            ('state', '<>', 'draft'), ('date', '<', start_date)])

        movelines = moveline_obj.browse(self.cr, self.uid, movelines)

        previous_balance = 0

        for items in movelines:
            previous_balance += (items.debit - items.credit)

        return movelines

    def generate_xls_report(self, _p, _xs, data, objects, wb):

        wanted_list = _p.wanted_list
        self.col_specs_template.update(_p.template_changes)
        _ = _p._

        context = self.context
        st_date = data.get('form').get('date_from')
        end_date = data.get('form').get('date_to')
        context['start_date'] = st_date
        context['end_date'] = end_date
        self.context = context
        date_range = _("Date: %s to %s" % (st_date, end_date))

        # report_name = objects[0]._description or objects[0]._name
        report_name = _("Customer's sales summary")
        ws = wb.add_sheet(report_name[:31])
        ws.panes_frozen = True
        ws.remove_splits = True
        ws.portrait = 0  # Landscape
        ws.fit_width_to_pages = 1
        row_pos = 0

        # set print header/footer
        ws.header_str = self.xls_headers['standard']
        ws.footer_str = self.xls_footers['standard']

        # Title
        cell_style = xlwt.easyxf(_xs['xls_title'])
        c_specs = [
            ('report_name', 4, 3, 'text', report_name),
        ]
        row_data = self.xls_row_template(c_specs, ['report_name'])
        row_pos = self.xls_write_row(
            ws, row_pos, row_data, row_style=cell_style)
        row_pos += 1

        cell_style = xlwt.easyxf(_xs['xls_title'])
        c_specs = [
            ('date_range', 4, 3, 'text', date_range),
        ]
        row_data = self.xls_row_template(c_specs, ['date_range'])
        row_pos = self.xls_write_row(
            ws, row_pos, row_data, row_style=cell_style)
        row_pos += 1
        # Column headers
        c_specs = map(lambda x: self.render(
            x, self.col_specs_template, 'header', render_space={'_': _p._}),
            wanted_list)
        row_data = self.xls_row_template(c_specs, [x[0] for x in c_specs])
        row_pos = self.xls_write_row(
            ws, row_pos, row_data, row_style=self.rh_cell_style,
            set_column_size=True)
        ws.set_horz_split_pos(row_pos)

        prev_balance = ''
        purchase_amount = ''  # due
        total_amount = ''
        paid_amount = ''  # paid
        total_outstanding = ''
        days_30_age = 0
        days_60_age = 0
        days_90_age = 0
        more_then_90_days = 0
        # res.partner
        for line in objects:
            purchase_amount = reduce(lambda x, y: x + (
                (y['account_id']['type'] == 'receivable' and y['debit'] or 0) or (
                    y['account_id']['type'] == 'payable' and y['credit'] * -1 or 0)),
                self._lines_get(line), 0)
            paid_amount = reduce(lambda x, y: x + (
                (y['account_id']['type'] == 'receivable' and y['credit'] or 0) or (
                    y['account_id']['type'] == 'payable' and y['debit'] * -1 or 0)),
                self._lines_get(line), 0)
            ait_paid_amount = reduce(lambda x, y: x + (
                (y['journal_id']['code'] == 'SAIT' and y['account_id']['type'] == 'receivable' and y['credit'] or 0) or (
                    y['journal_id']['code'] == 'SAIT' and y['account_id']['type'] == 'payable' and y['debit'] * -1 or 0)),
                self._lines_get(line), 0)
            prev_balance = reduce(lambda x, y: x + (y['debit'] - y['credit']), self._init_lines_get(line), 0)
            c_specs = map(
                lambda x: self.render(x, self.col_specs_template, 'lines'),
                wanted_list)
            total_amount = prev_balance + purchase_amount
            total_outstanding = total_amount - paid_amount
            aging = self._get_aging(line)

            days_30_age = aging['days_30_age']
            days_60_age = aging['days_60_age']
            days_90_age = aging['days_90_age']
            more_then_90_days = aging['more_then_90_days']

            for list_data in c_specs:

                if str(list_data[0]) == str('name'):
                    list_data[4] = str(line.name)
                if str(list_data[0]) == str('credit_days'):
                    list_data[4] = str(line.credit_days)
                if str(list_data[0]) == str('monthly_limit'):
                    list_data[4] = str(line.monthly_limit)
                if str(list_data[0]) == str('prev_balance'):
                    list_data[4] = str(prev_balance)
                if str(list_data[0]) == str('purchase_amount'):
                    list_data[4] = str(purchase_amount)
                if str(list_data[0]) == str('total_amount'):
                    list_data[4] = str(total_amount)
                if str(list_data[0]) == str('paid_amount'):
                    list_data[4] = str(paid_amount)
                if str(list_data[0]) == str('total_outstanding'):
                    list_data[4] = str(total_outstanding)
                if str(list_data[0]) == str('days_30_age'):
                    list_data[4] = str(days_30_age)
                if str(list_data[0]) == str('days_60_age'):
                    list_data[4] = str(days_60_age)
                if str(list_data[0]) == str('days_90_age'):
                    list_data[4] = str(days_90_age)
                if str(list_data[0]) == str('more_then_90_days'):
                    list_data[4] = str(more_then_90_days)
                if str(list_data[0]) == str('ait_paid_amount'):
                    list_data[4] = str(ait_paid_amount)

            row_data = self.xls_row_template(c_specs, [x[0] for x in c_specs])
            row_pos = self.xls_write_row(
                ws, row_pos, row_data, row_style=self.aml_cell_style)


account_move_line_xls('report.account.move.line.xls',
                      'res.partner',
                      parser=account_move_line_xls_parser)

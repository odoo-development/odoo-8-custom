from openerp.osv import fields, osv
import datetime


class PendingDashboardWiz(osv.osv_memory):

    _name = 'pending.dashboard.wiz'
    _description = 'COGS Report Wiz'

    _columns = {
        'days': fields.char("Days")

    }


    def _build_contexts(self, cr, uid, ids, data, context=None):
        if context is None:
            context = {}
        result = {'days': data['form']['days']}

        return result

    def check_report(self, cr, uid, ids, context=None):
        if context is None:
            context = {}

        data = {}

        data = self.read(cr, uid, ids, ['days'], context=context)[0]

        datas = {
            'ids': context.get('active_ids', []),
            'model': 'pending.dashboard.wiz',
            'form': data
        }

        return {'type': 'ir.actions.report.xml',
                'report_name': 'pending.dashboard.xls',
                'datas': datas
                }

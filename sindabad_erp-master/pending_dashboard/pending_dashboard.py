from openerp import models, api


class sale_order(models.Model):
    _inherit = 'sale.order'

    @api.model
    def _report_xls_fields_pd(self):

        return [
            'category', 'total_pending_value', 'total_pending_orders', 'pending_value_3', 'pending_orders_3', 'po_cre_not_rec', 'advance_po_cre_not_rec', 'credit_po_cre_not_rec', 'po_cre_rec_not_del', 'advance_po_rec_not_del', 'credit_po_rec_not_del', 'po_not_cre'

        ]

    # Change/Add Template entries
    @api.model
    def _report_xls_template_pd(self):

        return {}

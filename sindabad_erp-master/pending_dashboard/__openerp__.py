{
    'name': 'Pending Dashboard',
    'version': '8.0.1',
    'author': "Shahjalal & Rocky",
    'category': 'Sales',
    'summary': 'Sales order pending dashboard',
    'depends': ['sale', 'report_xls'],
    'data': [

        'security/pending_dashboard_security.xml',
        'security/ir.model.access.csv',

        'wizard/pending_dashboard_filter.xml',
        'report/pending_dashboard_xls.xml',

    ],
}

# -*- coding: utf-8 -*-
##########################################################################
#
#    Copyright (c) 2017-Present Webkul Software Pvt. Ltd. (<https://webkul.com/>)
#
########################################################################## 

from openerp.osv import fields, osv
from openerp.tools.translate import _

class res_partner(osv.osv):
    _inherit = "res.partner"

    _columns = {
        "industry": fields.char(string='Industry'),
        "vendor_id": fields.char(string='Vendor Id'),
        "url": fields.char(string='Shop Url'),
        "organization_name": fields.char(string='Organization Name'),
        "business_name": fields.char(string='Business Name'),
        "trade_license": fields.char(string='Trade License'),

        "wkv_trade_license_copy": fields.char(string='Trade License Copy'),
        "wkv_unique_cheque_copy": fields.char(string='Unique Cheque Copy'),
        "wkv_vat_number": fields.char(string='VAT/TIN Copy'),
        "wkv_buss_registration_copy": fields.char(string='Business Registration Copy'),

        "business_registration_number": fields.char(string='Business Registration Number'),
        "isSeller":fields.boolean(string='Is Seller'),
        "primary_name":fields.char(string='Name'),
        "primary_mobile":fields.char(string='Mobile'),
        "primary_email":fields.char(string='Email'),
        "secondary_name":fields.char(string='Name'),
        "secondary_mobile":fields.char(string='Mobile'),
        "secondary_email":fields.char(string='Email'),
        "account_title":fields.char(string='Account Title'),
        "account_number":fields.char(string='Account Number'),
        "bank_name":fields.char(string='Bank Name'),
        "branch_name":fields.char(string='Branch Name'),
        "routing_number":fields.char(string='Routing Number'),
        "business_type":fields.char(string='Business Type'),
        "created":fields.datetime(string='Vendor Creation Date'),
        "activated":fields.datetime(string='Vendor Activation Date'),
    }

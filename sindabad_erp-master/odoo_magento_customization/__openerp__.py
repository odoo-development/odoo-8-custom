# -*- coding: utf-8 -*-
##########################################################################
#
#    Copyright (c) 2017-Present Webkul Software Pvt. Ltd. (<https://webkul.com/>)
#
##########################################################################

{
    'name': 'Magento Odoo Customization',
    'version': '2.4.3',
    'category': 'Generic Modules',
    'sequence': 1,
    'author': 'Webkul Software Pvt. Ltd.',
    'website': 'http://www.webkul.com',
    'summary': 'Basic Customization',
    'depends': [
        'odoo_magento_connect',
        'order_approval_process',
    ],
    'data': [
        'views/res_partner_view.xml',
    ],
    'sequence': 1,
    'application': True,
    'installable': True,
    'auto_install': False,
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:

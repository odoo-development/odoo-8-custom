# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2009 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import time
from openerp.report import report_sxw
from openerp.osv import osv


class collectinvoicedata(report_sxw.rml_parse):

  def __init__(self, cr, uid, name, context):
    super(collectinvoicedata, self).__init__(cr, uid, name, context)
    self.localcontext.update({
        'get_all_invoices_data': self.get_all_invoices_data,
        'total_without_taxes': self.total_without_taxes,
        'get_client_invoice_address': self.get_client_invoice_address,
        'get_client_shipping_address': self.get_client_shipping_address,


    })

  def get_all_invoices_data(self, obj):

    try:
      order_numbers = [obj.split(',')[0]]
    except:
      order_numbers = [str(obj)]
    group_id = None
    order_name = None
    order_id = None
    data_list = []

    self.cr.execute("SELECT group_id FROM stock_picking WHERE name=%s", (order_numbers))
    for item in self.cr.fetchall():
      group_id = [item[0]]

    if group_id is not None:
      self.cr.execute("SELECT name FROM procurement_group WHERE id=%s", (group_id))
      for data in self.cr.fetchall():
        order_name = [data[0]]

      self.cr.execute("SELECT id FROM sale_order WHERE name=%s", (order_name))
      for data in self.cr.fetchall():
        order_id = data[0]

      order_data = self.pool.get('sale.order').browse(self.cr, self.uid, order_id)

      account_invoice_ids = order_data.invoice_ids

      for it in account_invoice_ids:

        if str(it.state) != 'cancel':
          str_state = 'Received' if str(it.type) == 'out_invoice' else 'Return'

          for items in it.invoice_line:
            data_list.append({'name': 'cool',
                              'product_name': items.product_id.name,
                              'quantity': items.quantity,
                              'unit_cost': items.price_unit,
                              'date_invoice': it.date_invoice,
                              'subtotal': items.price_subtotal,
                              # 'uom':items.product_id.uom_id.name,
                              'uom': items.product_id.prod_uom,
                              'discount': items.discount,
                              'status_data': str_state

                              })

    return data_list

  def total_without_taxes(self, obj):

    try:
      order_numbers = [obj.split(',')[0]]
    except:
      order_numbers = [str(obj)]
    group_id = None
    order_name = None
    order_id = None
    total_amount = 0.00
    total_amount_tax = 0.00
    net_total = 0.00
    delivered_amount = 0.00
    delivered_amount_without_tax = 0.00
    delivered_amount_tax = 0.00
    refund_amount = 0.00
    refund_amount_without_tax = 0.00

    refund_amount_tax = 0.00
    grand_amount = 0.00
    purchase_ref = ''
    clinet_order_ref = ''
    note = ''
    carrier = ''
    order_date = ''
    self.cr.execute("SELECT group_id FROM stock_picking WHERE name=%s", (order_numbers))
    for item in self.cr.fetchall():
      group_id = [item[0]]

    if group_id is not None:
      self.cr.execute("SELECT name FROM procurement_group WHERE id=%s", (group_id))
      for data in self.cr.fetchall():
        order_name = [data[0]]

      self.cr.execute("SELECT id FROM sale_order WHERE name=%s", (order_name))
      for data in self.cr.fetchall():
        order_id = data[0]

      order_data = self.pool.get('sale.order').browse(self.cr, self.uid, order_id)

      account_invoice_ids = order_data.invoice_ids

      purchase_ref = order_data.x_puchase_ref_cus
      clinet_order_ref = order_data.client_order_ref
      note = order_data.note
      carrier = order_data.carrier_id.name
      order_date = order_data.date_order

      for it in account_invoice_ids:

        if str(it.state) != 'cancel' and str(it.type) == 'out_refund':

          refund_amount_without_tax += it.amount_untaxed
          refund_amount_tax += it.amount_tax
          refund_amount += it.amount_total
        else:
          total_amount += it.amount_untaxed
          total_amount_tax += it.amount_tax
          net_total += it.amount_total
          delivered_amount_without_tax += it.amount_untaxed
          delivered_amount_tax += it.amount_tax
          delivered_amount += it.amount_total

    return [{'refund_amount_without_tax': refund_amount_without_tax,
             'refund_amount_tax': refund_amount_tax,
             'refund_amount': refund_amount,
             'delivered_amount_without_tax': delivered_amount_without_tax,
             'delivered_amount_tax': delivered_amount_tax,
             'delivered_amount': delivered_amount,
             'grand_total_with_out_tax': delivered_amount_without_tax - refund_amount_without_tax,
             'grand_total_tax': delivered_amount_tax - refund_amount_tax,
             'grand_total': delivered_amount - refund_amount,
             'purchase_ref': purchase_ref,
             'clinet_order_ref': clinet_order_ref,
             'note': note,
             'carrier': carrier,
             'order_date': order_date,
             }]

  def get_client_invoice_address(self, obj):
    origin = [str(obj)]
    picking_origin = None

    if origin is not None:
      self.cr.execute("SELECT origin FROM stock_picking WHERE name=%s", (origin))

      for item in self.cr.fetchall():
        picking_origin = item[0]

    order_numbers = [picking_origin]

    context = {
        'lang': 'en_US',
        'params': {'action': 404},
        'tz': 'Asia/Dhaka',
        'uid': self.uid
    }

    partner_invoice_id_query = "SELECT partner_invoice_id FROM sale_order WHERE name='{0}'".format(order_numbers[0])
    self.cr.execute(partner_invoice_id_query)
    for item in self.cr.fetchall():
      partner_invoice_id = int(item[0])

    res_partner_obj = self.pool.get('res.partner')
    res_partner = res_partner_obj.browse(self.cr, self.uid, [partner_invoice_id], context=context)

    return res_partner if res_partner else '-'

  def get_client_shipping_address(self, obj):

    origin = [str(obj)]
    picking_origin = None

    if origin is not None:
      self.cr.execute("SELECT origin FROM stock_picking WHERE name=%s", (origin))

      for item in self.cr.fetchall():
        picking_origin = item[0]

    order_numbers = [picking_origin]

    context = {
        'lang': 'en_US',
        'params': {'action': 404},
        'tz': 'Asia/Dhaka',
        'uid': self.uid
    }

    partner_shipping_id_query = "SELECT partner_shipping_id FROM sale_order WHERE name='{0}'".format(order_numbers[0])
    self.cr.execute(partner_shipping_id_query)
    for item in self.cr.fetchall():
      partner_shipping_id = int(item[0])

    res_partner_obj = self.pool.get('res.partner')
    res_partner = res_partner_obj.browse(self.cr, self.uid, [partner_shipping_id], context=context)

    return res_partner if res_partner else '-'


class report_final_invoice_layout(osv.AbstractModel):
  _name = 'report.custom_invoice_report.report_finalinvoice'
  _inherit = 'report.abstract_report'
  _template = 'custom_invoice_report.report_finalinvoice'
  _wrapped_report_class = collectinvoicedata

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:

# Author Mufti Muntasir Ahmed

{
    'name': 'Final Invoice',
    'version': '8.0.0',
    'category': 'Sales',
    'description': """
Allows you to print Clear Cut Picture of sales history   
==============================================================

You will get the invoices of return challsna

""",
    'author': 'Mufti Muntasir Ahmed',
    'depends': ['account','sale'],
    'data': [

        'account_invoice_report.xml',
        'views/report_finalinvoice.xml',

    ],

    'installable': True,
    'auto_install': False,
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:

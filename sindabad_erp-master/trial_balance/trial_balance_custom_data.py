from openerp import models, api


class TrialBalanceCustomReport(models.Model):
    _inherit = 'account.account'

    @api.model
    def _report_xls_fields_custom(self):

        return [
            'name', 'code',
        ]

    # Change/Add Template entries
    @api.model
    def _report_xls_template_custom(self):

        return {}

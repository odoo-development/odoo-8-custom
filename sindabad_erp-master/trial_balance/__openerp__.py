{
    'name': 'Trial Balance Report',
    'version': '1.0',
    'category': 'Accounting',
    'author': 'Shahjalal',
    'summary': 'Trial Balance Report',
    'website': 'https://www.sindabad.com',
    'description': "Vat Calculation of Invoices",
    'depends': [
        'base',
        'odoo_magento_connect',
        'account',
        'delivery_customer_name',
    ],
    'data': [

        'wizard/trial_balance_custom_wizard_view.xml',
        'report/trial_balance_report_xls.xml',
        'trial_balance_custom_menu.xml',

    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}

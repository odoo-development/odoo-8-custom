##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.osv import fields, osv
class PO(osv.osv):

    _inherit = 'purchase.order'
    _columns = {
        #'adv_po_snd': fields.selection([ ('credit', 'Credit'),('advance', 'Advance'),], 'Payment Status', change_default='true', track_visibility='always'), 
        'adv_po_snd': fields.selection([ ('credit', 'Credit'),('advance', 'Advance'),('cash','By Cash')], 'Payment Status', required=False, states={'draft': [('required', True)], 'sent': [('required', True)]}, change_default=True, track_visibility='always'),
    }
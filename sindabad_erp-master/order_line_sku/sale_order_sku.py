# -*- coding: utf-8 -*-

from openerp.osv import fields, osv


class sale_order_line(osv.osv):
    _inherit = "sale.order.line"
    _columns = {
        'default_code': fields.related('product_id', 'default_code', store=False, readonly=True, string='SKU', type='char'),
    }

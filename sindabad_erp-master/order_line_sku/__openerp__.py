# -*- coding: utf-8 -*-
{
    'name': "Sale Order SKU",

    'summary': """
		Fixing Sale Order line sku """,

    'description': """
		Fixing Sale Order line sku
	""",

    'author': "S. M. Sazedul Haque - Zero Gravity Ventures Ltd",
    'website': "http://zerogravity.com.bd/",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/openerp/addons/base/module/module_data.xml
    # for the full list
    'category': 'custom',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': [
            'sale',
    ],

    # always loaded
    'data': [
    ],
    'application': True,
    'installable': True,
    'auto_install': False,
}

{
    'name': 'SO Salesperson',
    'version': '1.0',
    'category': 'Sale',
    'author': 'Shuvarthi Dhar',
    'summary': 'SO Sales Person Sync',
    'description': 'SO Sales Person Sync',
    'depends': ['base', 'sale','odoo_magento_connect'],
    'data': [
        'so_salesperson_view.xml',
        'so_salesperson_pop.xml',
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}

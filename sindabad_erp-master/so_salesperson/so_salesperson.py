from datetime import date, datetime
from openerp.osv import fields, osv, expression


class sale_order(osv.osv):
    _inherit = "sale.order"

    _columns = {
        'custom_salesperson': fields.many2one('res.users', 'Sales Person', required=False)
    }

    def _populate_sales_person(self, cr, uid, ids=None, context=None):

        res_partner_obj = self.pool.get("res.partner")

        for so in self.browse(cr, uid, self.search(cr, uid, []), context=context):
            parent = so.partner_id

            company_id = None
            for i in range(5):
                if len(parent.parent_id) == 1:
                    parent = parent.parent_id
                else:

                    company_id = parent.id
                    break

            company = res_partner_obj.browse(cr, uid, [company_id], context=context)

            sp = company.user_id.id if company.user_id else None

            so.write({'custom_salesperson': sp})

        return True

    def create(self, cr, uid, vals, context=None):

        res_partner_obj = self.pool.get("res.partner")

        parent = res_partner_obj.browse(cr, uid, [vals['partner_id']], context=context)

        company_id = None
        for i in range(5):
            if len(parent.parent_id) == 1:
                parent = parent.parent_id
            else:

                company_id = parent.id
                break

        company = res_partner_obj.browse(cr, uid, [company_id], context=context)

        vals['custom_salesperson'] = company.user_id.id if company.user_id else None

        res = super(sale_order, self).create(cr, uid, vals, context=context)

        return res

    def write(self, cr, uid, ids, vals, context=None):

        so_obj = self.pool.get("sale.order")
        res_partner_obj = self.pool.get("res.partner")

        so = so_obj.browse(cr, uid, ids, context=context)

        parent = so.partner_id

        company_id = None
        for i in range(5):
            if len(parent.parent_id) == 1:
                parent = parent.parent_id
            else:

                company_id = parent.id
                break

        company = res_partner_obj.browse(cr, uid, [company_id], context=context)

        vals['custom_salesperson'] = company.user_id.id if company.user_id else None

        record = super(sale_order, self).write(cr, uid, ids, vals, context=context)

        return record



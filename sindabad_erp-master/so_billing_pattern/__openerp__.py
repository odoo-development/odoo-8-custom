{
    'name': "Sales Order Billing Pattern",
    'version': '1.0',
    'category': 'Sale',
    'author': "Md Rockibul Alam Babu",
    'description': """
Customer Billing Pattern in sales order form
    """,
    'website': "http://www.sindabad.com",
    'depends': [
        'sale', 'order_approval_process','send_sms_on_demand'
    ],
    'data': [
        'views/so_billing_pattern_script.xml',
    ],

    'installable': True,
    'application': True,
    'auto_install': False,
}

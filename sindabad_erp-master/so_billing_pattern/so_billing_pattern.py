from openerp.osv import fields, osv


class sale_order(osv.osv):
    _inherit = "sale.order"



    def _script_order_billing_pattern(self, cr, uid, ids=None, context=None):

        for so in self.browse(cr, uid, self.search(cr, uid, [('state', '!=', 'cancel'),('date_order','>','2019-12-15'),('so_billing_pattern', '=', False)]), context=context):
            parent = so.partner_id

            company = None
            for i in range(5):
                if len(parent.parent_id) == 1:
                    parent = parent.parent_id

                else:
                    company = parent
                    break

            if company.cus_on_demand or parent.cus_on_demand == True:
                so_billing_pattern = 'on_demand'

            else:
                so_billing_pattern = 'on_delivery'

            so.write({'so_billing_pattern': so_billing_pattern})

        return True



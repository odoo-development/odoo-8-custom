# Author Mufti Muntasir Ahmed 15-05-2018


import itertools
from lxml import etree

from openerp import models, fields, api, _
from openerp.exceptions import except_orm, Warning, RedirectWarning
from openerp.tools import float_compare
import openerp.addons.decimal_precision as dp



class account_invoice(models.Model):
    _inherit = "account.invoice"

    pack_id = fields.Many2one('packing.list', string='Packing',required=False, track_visibility='onchange')
    from_packed = fields.Boolean(string='Packed')
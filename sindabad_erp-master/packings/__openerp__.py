# Author Mufti Muntasir Ahmed 11/04/2018


{
    'name': 'Packing List Generation',
    'version': '8.0.0',
    'category': 'Sales',
    'description': """
Generate Packing List from Delivery Challan / Sales Order   
==============================================================

""",
    'author': 'Mufti Muntasir Ahmed',
    'depends': ['stock'],
    'data': [

        'wizard/stock_packing_select_view.xml',
        'security/packing_security.xml',
        'security/ir.model.access.csv',
        'packing_view.xml',
        'views/report_packing.xml',
        'packing.xml',




    ],

    'installable': True,
    'auto_install': False,
}
# Author Mufti Muntasir Ahmed 17-04-2018
import time
from openerp.report import report_sxw
from openerp.osv import osv


class PackingListReport(report_sxw.rml_parse):

    def __init__(self, cr, uid, name, context):
        super(PackingListReport, self).__init__(cr, uid, name, context)
        self.localcontext.update({
            'get_magento_order_numbers': self.get_magento_order_numbers,
            'get_customer_name': self.get_customer_name,
            'get_product_list': self.get_product_list,
            'get_client_invoice_address': self.get_client_invoice_address,
            'get_client_shipping_address': self.get_client_shipping_address,
            'get_purchase_ref': self.get_purchase_ref,
            'get_payment_method': self.get_payment_method,
        })

    def get_magento_order_numbers(self, obj):

        stock_packing_ids = [it.stock_id for it in obj.packing_line]
        magento_numbers = ''

        for items in stock_packing_ids:
            if items.mag_no:
                magento_numbers = magento_numbers + str(items.mag_no) + ', '

        return magento_numbers

    def get_customer_name(self, obj):

        stock_picking = self.pool.get('stock.picking').browse(obj._cr, obj._uid, [int(obj.picking_id)], context=obj._context)

        customer_name = str(stock_picking.partner_id.name)

        return customer_name

    def get_product_list(self, obj):

        stock_packing_ids = [it.stock_id for it in obj.packing_line]
        total_moveline_list = [items.move_lines for items in stock_packing_ids]
        product_id_lists = []
        moves_list = []
        for items in total_moveline_list:
            for att in items:
                product_id_lists.append(att.product_id.id)
                moves_list.append(att)


        # product_id_lists = list(set(product_id_lists))
        #
        # p_list=[]
        #
        #
        # for product_id in product_id_lists:
        #     qty_count=0
        #     product_info_dict ={}
        #     for mov in moves_list:
        #         if mov.product_id.id == product_id:
        #             qty_count += mov.product_qty
        #
        #     for mov in moves_list:
        #         if mov.product_id.id == product_id:
        #
        #             product_info_dict['product_name'] = mov.product_id.name
        #             product_info_dict['default_code'] = mov.product_id.default_code
        #             product_info_dict['qty'] = qty_count
        #             break
        #
        #     p_list.append(product_info_dict)

        return moves_list

    def get_client_invoice_address(self, obj):

        order_numbers = [str(obj)]
        context = {
            'lang': 'en_US',
            'params': {'action': 404},
            'tz': 'Asia/Dhaka',
            'uid': self.uid
        }
        partner_invoice_id = None
        partner_invoice_id_query = "SELECT partner_invoice_id FROM sale_order WHERE name='{0}'".format(order_numbers[0])
        self.cr.execute(partner_invoice_id_query)
        for item in self.cr.fetchall():
            partner_invoice_id = int(item[0])

        res_partner_obj = self.pool.get('res.partner')
        res_partner = res_partner_obj.browse(self.cr, self.uid, [partner_invoice_id], context=context)

        return res_partner if res_partner else '-'

    def get_client_shipping_address(self, obj):

        order_numbers = [str(obj)]
        context = {
            'lang': 'en_US',
            'params': {'action': 404},
            'tz': 'Asia/Dhaka',
            'uid': self.uid
        }
        partner_shipping_id=None
        partner_shipping_id_query = "SELECT partner_shipping_id FROM sale_order WHERE name='{0}'".format(
            order_numbers[0])
        self.cr.execute(partner_shipping_id_query)
        for item in self.cr.fetchall():
            partner_shipping_id = int(item[0])

        res_partner_obj = self.pool.get('res.partner')
        res_partner = res_partner_obj.browse(self.cr, self.uid, [partner_shipping_id], context=context)

        return res_partner if res_partner else '-'

    def get_purchase_ref(self, obj):
        order_numbers = [str(obj)]
        purchase_ref_cus = None
        self.cr.execute("SELECT x_puchase_ref_cus FROM sale_order WHERE name=%s", (order_numbers))
        for item in self.cr.fetchall():
            purchase_ref_cus = item[0]

        return purchase_ref_cus if purchase_ref_cus else '---'

    def get_payment_method(self, obj):
        order_numbers = [str(obj)]
        note = None
        self.cr.execute("SELECT note FROM sale_order WHERE name=%s", (order_numbers))
        for item in self.cr.fetchall():
            note = item[0]
        return note if note else '.'


class report_packinglayout(osv.AbstractModel):
    _name = 'report.packings.report_packinglayout'
    _inherit = 'report.abstract_report'
    _template = 'packings.report_packinglayout'
    _wrapped_report_class = PackingListReport

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:

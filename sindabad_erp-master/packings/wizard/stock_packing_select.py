# Author mufti muntasir ahmed 17-04-2018

##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.osv import fields, osv
from openerp.tools.translate import _
from datetime import date, datetime


class sale_make_invoice(osv.osv_memory):
    _name = "packing.list.multiple"
    _description = "Packing List"
    _columns = {
        'packing_date': fields.datetime('Date', required=True, readonly=True, select=True, copy=False),
        'description': fields.char('Reference/Description'),
        'create_date': fields.datetime('Creation Date', readonly=True, select=True,
                                       help="Date on which sales order is created."),
        'date_confirm': fields.date('Confirmation Date', readonly=True, select=True,
                                    help="Date on which sales order is confirmed.", copy=False),
        'user_id': fields.many2one('res.users', 'Assigned to', select=True, track_visibility='onchange'),

        'packing_line': fields.one2many('packing.list.line', 'packing_id', 'Packing List Lines', required=True),

    }

    _defaults = {
        'packing_date': fields.datetime.now,
        'user_id': lambda obj, cr, uid, context: uid,

    }



    def make_packing(self, cr, uid, ids, context=None):
        packing_obj = self.pool.get('packing.list')

        if context is None:
            context = {}
        get_data = self.read(cr, uid, ids)[0]
        data ={}
        packing_line = []
        today_date = date.today()


        data['description'] = get_data.get('description')

        for items in context.get('active_ids'):
            packing_line.append([0, False, {'stock_id': items, 'receive_date': today_date.strftime('%Y-%m-%d'), 'details': False, 'state': 'pending', 'return_date': False}])

        data['packing_line'] = packing_line

        save_the_data = packing_obj.create(cr, uid, data, context=context)




        return save_the_data


# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:

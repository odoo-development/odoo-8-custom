# Author Mufti Muntasir Ahmed 10-04-2018


from openerp.osv import osv, fields
from openerp import SUPERUSER_ID, api
from openerp.tools.translate import _
from datetime import datetime
import datetime


class stock_packing(osv.osv):
    _inherit = 'stock.picking'

    _columns = {
        'packed': fields.boolean('Packed'),
        # 'system_transferred': fields.boolean('Transferred'),
        # 'system_invoiced': fields.boolean('Invoiced'),

    }


class PackingList(osv.osv):
    _name = 'packing.list'
    _description = "Packing List"

    _columns = {
        'packing_date': fields.datetime('Date', required=True, readonly=True, select=True, copy=False),
        'description': fields.char('Packer Name'),
        'picking_id': fields.char('Picking', required=False),
        'create_date': fields.datetime('Creation Date', readonly=True, select=True,
                                       help="Date on which sales order is created."),
        'date_confirm': fields.datetime('Confirmation Date', readonly=True, select=True,
                                    help="Date on which sales order is confirmed.", copy=False),
        'user_id': fields.many2one('res.users', 'Assigned to', select=True, track_visibility='onchange'),

        'packing_line': fields.one2many('packing.list.line', 'packing_id', 'Packing List Lines', required=True),
        'state': fields.selection([
            ('pending', 'Waiting for Packing'),
            ('done', 'Done'),
            ('cancel', 'Cancelled'),

        ], 'Status', readonly=True, copy=False, help="Gives the status of the quotation or sales order", select=True),

    }

    _defaults = {
        'packing_date': fields.datetime.now,
        'user_id': lambda obj, cr, uid, context: uid,
        'state': 'pending',

    }


    def create_invoice(self, cr, uid, ids, context=None):

        for packing_id in ids:
            cr.execute("select stock_id,magento_no from  packing_list_line where packing_id=%s", ([packing_id]))
            pick_list =cr.fetchall()
            for picking_id in pick_list:
                picking=[picking_id[0]]
                order_id = str(picking_id[1])
                stock_picking_id=picking_id[0]


                sale_ids = self.pool.get('sale.order').search(cr, uid, [('client_order_ref', '=', order_id)],context=context)
                sale_object = self.pool.get('sale.order').browse(cr, uid, sale_ids, context=context)[0]
                new_customer = sale_object.new_customer
                # if 'cancel' not in [sale_object.state]:
                #
                #     context.update({
                #
                #         'active_model': 'stock.picking',
                #         'active_ids': picking,
                #         'active_id': len(picking) and picking[0] or False
                #     })
                #     created_id = self.pool['stock.transfer_details'].create(cr, uid, {'picking_id': len(picking) and picking[0] or False}, context)
                #
                #     if created_id:
                #         created_id = self.pool.get('stock.transfer_details').browse(cr, uid, created_id, context=context)
                #         if created_id.picking_id.state in ['assigned', 'partially_available']:
                #             # raise Warning(_('You cannot transfer a picking in state \'%s\'.') % created_id.picking_id.state)
                #
                #             processed_ids = []
                #             # Create new and update existing pack operations
                #             for lstits in [created_id.item_ids, created_id.packop_ids]:
                #                 for prod in lstits:
                #                     pack_datas = {
                #                         'product_id': prod.product_id.id,
                #                         'product_uom_id': prod.product_uom_id.id,
                #                         'product_qty': prod.quantity,
                #                         'package_id': prod.package_id.id,
                #                         'lot_id': prod.lot_id.id,
                #                         'location_id': prod.sourceloc_id.id,
                #                         'location_dest_id': prod.destinationloc_id.id,
                #                         'result_package_id': prod.result_package_id.id,
                #                         'date': prod.date if prod.date else datetime.now(),
                #                         'owner_id': prod.owner_id.id,
                #                     }
                #                     if prod.packop_id:
                #                         prod.packop_id.with_context(no_recompute=True).write(pack_datas)
                #                         processed_ids.append(prod.packop_id.id)
                #                     else:
                #                         pack_datas['picking_id'] = created_id.picking_id.id
                #                         packop_id = created_id.env['stock.pack.operation'].create(pack_datas)
                #                         processed_ids.append(packop_id.id)
                #             # Delete the others
                #             packops = created_id.env['stock.pack.operation'].search(
                #                 ['&', ('picking_id', '=', created_id.picking_id.id), '!', ('id', 'in', processed_ids)])
                #             packops.unlink()
                #
                #
                #             # Execute the transfer of the picking
                #             created_id.picking_id.do_transfer()
                #
                #             # cr.execute("update stock_picking set system_transferred='True' where id=%s", ([picking_id[0]]))

                stock_inv_obj = self.pool.get('stock.picking').browse(cr, uid, stock_picking_id, context=context)

                acc_id = None

                # try:
                #     acc_id = stock_inv_obj.partner_id.property_account_receivable.id
                # except:
                #     if acc_id is None or acc_id is False:
                #         partner_id = stock_inv_obj.partner_id.commercial_partner_id.id
                #         self.pool.get('res.partner').write(cr, uid, [partner_id],
                #                                                       {'property_account_receivable': 7074},
                #                                                       context=context)
                #     cr.commit()
                # if created_id.picking_id.invoice_state != 'invoiced':
                if stock_inv_obj.invoice_state != 'invoiced':
                    # Now Create the Invoice for this picking
                    picking_pool = self.pool.get('stock.picking')
                    from datetime import date
                    context['date_inv'] = date.today()
                    context['inv_type'] = 'out_invoice'
                    active_ids = [picking_id[0]]

                    ## Get customer A/R I



                    try:
                        res = picking_pool.action_invoice_create(cr, uid, active_ids,
                                                                 journal_id=1146,
                                                                 group=True,
                                                                 type='out_invoice',
                                                                 context=context)
                    except:
                        raise osv.except_osv(_('Warning!'),
                                             _('Accounting Info missing The operation can not be completed, Probably due to the Customer Account information properly not assigned. Please, contact the finance team.!!!'))

                    # Following Query flag that invoice and transfer will set boolean value
                    if res:
                        inv_id = res[0]
                        cr.execute("DELETE FROM account_invoice_tax WHERE invoice_id=%s",([res[0]]))
                        cr.commit()


                        tax_obj = self.pool.get('account.tax')
                        tax_data = tax_obj.browse(cr, uid, [5], context=context)[0]

                        cr.execute("select sum(five_vat) as total_vat from account_invoice_line where invoice_id=%s group by invoice_id", ([res[0]]))
                        total_vat_amount = cr.fetchall()
                        if len(total_vat_amount) > 0:
                            total_vat_amount=total_vat_amount[0][0]

                            cr.execute("select amount_untaxed from account_invoice where id=%s", ([res[0]]))
                            base_amount = cr.fetchall()[0][0]

                            tax_line_data = {
                                'account_id': tax_data.account_collected_id.id,
                                'sequence': 1,
                                'company_id': 1,
                                'manual': False,
                                'base_amount': base_amount,
                                'amount': total_vat_amount,
                                'base': base_amount,
                                'tax_amount': total_vat_amount,
                                'name': tax_data.name,
                            }


                            account_invoice = self.pool['account.invoice']
                            account_invoice.write(cr, uid, inv_id, {'tax_line': [(0,0,tax_line_data)]},
                                             context=context)

                            cr.commit()

                            ### Check for Product Warranty exist or not
                            found_warranty_items = False

                            if inv_id:
                                invoice_data = self.pool.get('account.invoice').browse(cr, uid, inv_id, context=context)[0]
                                invoice_data.signal_workflow('invoice_open')
                                try:

                                    for items in invoice_data.invoice_line:
                                        if items.product_id.warranty_applicable == True:
                                            found_warranty_items= True
                                            break
                                except:
                                    pass



                        cr.execute("update account_invoice set pack_id=%s where id=%s", ([packing_id, res[0]]))
                        cr.execute("update account_invoice set from_packed='True' where id=%s", ([res[0]]))
                        cr.execute("update account_invoice set x_priority_customer=%s where id=%s", ([new_customer,res[0]]))
                        if found_warranty_items == True:
                            cr.execute("update packing_list_line set invoice_id=%s,warranty_serials=%s where stock_id=%s and packing_id=%s",
                                       ([res[0],found_warranty_items, picking_id[0], packing_id]))
                        else:


                            cr.execute("update packing_list_line set invoice_id=%s where stock_id=%s and packing_id=%s", ([res[0], picking_id[0],packing_id]))

                        cr.commit()



        return True

    def test_create_invoice(self, cr, uid, ids, context=None):
        context = dict(context or {})
        picking_pool = self.pool.get('stock.picking')
        data = self.browse(cr, uid, ids[0], context=context)
        journal2type = {'sale':'out_invoice', 'purchase':'in_invoice', 'sale_refund':'out_refund', 'purchase_refund':'in_refund'}
        context['date_inv'] = data.invoice_date
        acc_journal = self.pool.get("account.journal")
        inv_type = journal2type.get(data.journal_type) or 'out_invoice'
        context['inv_type'] = inv_type

        active_ids = context.get('active_ids', [])
        res = picking_pool.action_invoice_create(cr, uid, active_ids,
              journal_id = data.journal_id.id,
              group = data.group,
              type = inv_type,
              context=context)
        return res

    def make_confirm(self, cr, uid, ids, context=None):

        id_list = ids
        stock_packing_ids = []

        for element in id_list:
            ids = [element]

            if ids is not None:
                cr.execute("update packing_list set state='done' where id=%s", (ids))
                cr.execute("update packing_list_line set state='done' where packing_id=%s", (ids))

            cr.execute("select stock_id from  packing_list_line where packing_id=%s", (ids))

            for item in cr.fetchall():
                for tuple_items in list(item):
                    stock_packing_ids.append(tuple_items)
        for items in stock_packing_ids:
            cr.execute("update stock_picking set packed='True' where id=%s", ([items]))

        for single_id in id_list:
            confirm_date_update_query = "UPDATE packing_list SET date_confirm='{0}' WHERE id={1}".format(str(fields.datetime.now()), single_id)
            cr.execute(confirm_date_update_query)
            cr.commit()

        return True

    def make_cancel(self, cr, uid, ids, context=None):
        id_list = ids
        stock_packing_ids = []
        for element in id_list:
            ids = [element]

            if ids is not None:
                cr.execute("update packing_list set state='cancel' where id=%s", (ids))
                cr.execute("update packing_list_line set state='cancel' where packing_id=%s", (ids))

            cr.execute("select stock_id from  packing_list_line where packing_id=%s", (ids))


            for item in cr.fetchall():
                for tuple_items in list(item):
                    stock_packing_ids.append(tuple_items)

        for items in stock_packing_ids:
            cr.execute("update stock_picking set packed='False' where id=%s", ([items]))

        return 'True'

    def create(self, cr, uid, data, context=None):
        context = dict(context or {})

        stock_packing_list=[]
        if data.get('packing_line'):
            packing_line=[]
            for items in data.get('packing_line'):
                att_data={}
                for att_data in items:
                    if type(att_data) is dict:
                        stock_id = att_data.get('stock_id')
                        stock_packing_list.append(stock_id)
                        stock_obj = self.pool.get('stock.picking').browse(cr, uid, stock_id, context=context)

                        att_data['magento_no']=stock_obj.mag_no
                        att_data['order_number']=stock_obj.origin
                packing_line.append([0,False,att_data])


            data['packing_line']=packing_line

        if stock_packing_list:
            abc=[]
            for items in stock_packing_list:
                cr.execute("select packing_id from  packing_list_line where state !='cancel' and stock_id=%s", ([items]))

                abc += cr.fetchall()

            if len(abc)>0:
                raise osv.except_osv(_('Message'), _("Already Packed the numbers"))

        packing_id = super(PackingList, self).create(cr, uid, data, context=context)



        if packing_id:
            for items in data['packing_line']:
                for att in items:
                    if isinstance(att, dict):
                        cr.execute("update stock_picking set packed='True' where id=%s", (att.get('stock_id'),))

        return packing_id


    def write(self, cr, uid, ids, vals, context=None):
        if isinstance(ids, (int, long)):
            ids = [ids]
        res = super(PackingList, self).write(cr, uid, ids, vals, context=context)
        return res







class PackingListLine(osv.osv):
    _name = 'packing.list.line'
    _description = "Packing Line List"

    _columns = {
        'packing_id': fields.many2one('packing.list', 'Order Reference', required=True, ondelete='cascade', select=True,
                                      readonly=True),

        'stock_id': fields.many2one('stock.picking', string='Stock Packing'),
        'invoice_id': fields.many2one('account.invoice', string='Invoice'),
        'sequence': fields.integer('Sequence',
                                   help="Gives the sequence order when displaying a list of sales order lines."),
        'details': fields.char('Details'),
        'receive_date': fields.date('Receive Date', required=True),
        'return_date': fields.date('Return Date'),
        'order_number': fields.char('Order Number'),
        'magento_no': fields.char('Magento No'),
        'state': fields.selection([
            ('pending', 'Waiting for Packing'),
            ('done', 'Done'),
            ('cancel', 'Cancelled'),

        ], 'Status', readonly=True, copy=False, help="Gives the status of the quotation or sales order", select=True),
    }

    _defaults = {
        'receive_date': fields.datetime.now,
        'state': 'pending',

    }




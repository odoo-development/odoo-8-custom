# Mufti Muntasir Ahmed 10-05-2018
from openerp.osv import fields, osv
from openerp.tools.translate import _
import openerp.addons.decimal_precision as dp

from openerp import models, api
from openerp.exceptions import Warning
from datetime import datetime




class so_approval_amount_range(osv.osv):

    _name = "so.approval.amount.range"

    _columns = {
        'head_range1': fields.integer('Head of Sales Ranges From'),
        'head_range2': fields.integer('Head of Sales Ranges To'),
        'cfo_range1': fields.integer('CFO Ranges From'),
        'cfo_range2': fields.integer('CFO Ranges To'),
        'ceo_range1': fields.integer('CEO Ranges From'),
        'ceo_range2': fields.integer('CEO Ranges To'),
    }


class sale_order(osv.osv):
    _inherit = "sale.order"


    def _get_credit_day(self, cursor, user, ids, name, arg, context=None):
        res = False
        res = {}

        for sale in self.browse(cursor, user, ids, context=context):
            res[sale.id] = sale.partner_id.parent_id.credit_days if sale.partner_id.parent_id else sale.partner_id.parent_id.credit_days
        return res

    def _get_credit_limit(self, cursor, user, ids, name, arg, context=None):
        res = False
        res = {}

        for sale in self.browse(cursor, user, ids, context=context):
            res[sale.id] = sale.partner_id.parent_id.credit_limit if sale.partner_id.parent_id else sale.partner_id.parent_id.credit_limit
        return res

    def _get_credit(self, cursor, user, ids, name, arg, context=None):
        res = False
        res = {}

        for sale in self.browse(cursor, user, ids, context=context):
            res[sale.id] = sale.partner_id.parent_id.credit if sale.partner_id.parent_id else sale.partner_id.parent_id.credit
        return res

    _columns = {

        'credit': fields.function(_get_credit, string='Total Receivable'),
        'credit_days': fields.function(_get_credit_day, string='Credit Days'),
        'credit_limit': fields.function(_get_credit_limit, string='Credit Limit'),
        'state': fields.selection([
            ('approval_pending', 'Approval Pending'),
            ('check_pending', 'Check Pending'),
            ('ceo_check_pending', 'CEO Check Pending'),
            ('new_cus_approval_pending', 'New Customer Approval Pending'),
            ('draft', 'Draft Quotation'),
            ('sent', 'Quotation Sent'),
            ('cancel', 'Cancelled'),
            ('waiting_date', 'Waiting Schedule'),
            ('progress', 'Sales Order'),
            ('manual', 'Sale to Invoice'),
            ('association_exception', 'Company Exception'),
            ('shipping_except', 'Shipping Exception'),
            ('invoice_except', 'Invoice Exception'),
            ('done', 'Done'),
        ], 'Status', readonly=True, copy=False, help="Gives the status of the quotation or sales order.\
                      \nThe exception status is automatically set when a cancel operation occurs \
                      in the invoice validation (Invoice Exception) or in the picking list process (Shipping Exception).\nThe 'Waiting Schedule' status is set when the invoice is confirmed\
                       but waiting for the scheduler to run on the order date.", select=True),

    }


# ------------------- CREDIT LIMIT EXCEED ---------------------
class approve_limit_exceed(osv.osv):
    _inherit = "approve.limit.exceed"
    _description = "Approve Limit Exceed"

    def so_approve_limit_exceed(self, cr, uid, ids, context=None):
        ids = context['active_ids']  # [15526]
        if ids:
            for id in ids:
                sale = self.pool.get('sale.order').browse(cr, uid, ids, context=context)
                if sale.state == 'ceo_check_pending':
                    approve_query = "UPDATE sale_order SET state='draft' WHERE id='{0}'".format(id)
                    cr.execute(approve_query)
                    cr.commit()
                else:
                    super(approve_limit_exceed, self).so_approve_limit_exceed(cr, uid, ids, context)
                    try:
                        data_range = self.pool.get('so.approval.amount.range').browse(cr, uid, [1], context=context)[0]
                        if float(sale.amount_total) < float(data_range.head_range2):
                            approve_query = "UPDATE sale_order SET state='draft' WHERE id='{0}'".format(id)
                            cr.execute(approve_query)
                            cr.commit()
                    except:
                        pass
        else:
            super(approve_limit_exceed, self).so_approve_limit_exceed(cr, uid, ids, context)

        return True

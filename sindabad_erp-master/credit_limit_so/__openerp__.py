{
    'name': "Credit Limit SO",
    'version': '8.0.1.0.0',
    'summary': """Credit limit SO list""",
    'description': "",
    'author': "Shuvarthi",
    'website': "https://www.sindabad.com",
    'company': '',
    'maintainer': '',
    'category': 'sale',
    'depends': ['ait_vat_credit'],

    'data': [

            'views/credit_limit_exceed_so_list.xml',
            'views/credit_limit_co_approval_so_list.xml',
            'views/so_approval.xml',
    ],
    'installable': True,
    'auto_install': False,
    'application': False,
}
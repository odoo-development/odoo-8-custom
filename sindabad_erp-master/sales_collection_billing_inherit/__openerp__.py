{
    'name': "Sales Collection Billing Process Inherit",
    'version': '1.0',
    'category': 'WMS',
    'author': "Shuvarthi Dhar",
    'description': """
Sales Collection Billing Process Inherit
    """,
    'website': "http://www.sindabad.com",
    'depends': ['sales_collection_billing_process','so_invoice_create_full_refund_mark'],
    'data': [
        'sale_view.xml',
    ],

    'installable': True,
    'application': True,
    'auto_install': False,
}

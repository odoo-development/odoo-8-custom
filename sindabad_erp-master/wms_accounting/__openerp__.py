{
    'name': 'WMS Accounting',
    'version': '1.0',
    'category': 'WMS',
    'author': 'Rocky',
    'summary': 'WMS Accounting for warehouse team',
    'description': 'WMS Accounting for warehouse team',
    'depends': [
        'account',
    ],
    'data': [
        'security/wms_accounting_security.xml',
        'wms_accounting_view.xml',
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}
import time
from openerp.report import report_sxw
from openerp.osv import osv


class packingsChallanData(report_sxw.rml_parse):

    def __init__(self, cr, uid, name, context):
        super(packingsChallanData, self).__init__(cr, uid, name, context)
        self.localcontext.update({
            'get_client_order_ref': self.get_client_order_ref,
            'get_packing_slip': self.get_packing_slip,
            'get_purchase_ref': self.get_purchase_ref,
            'get_payment_method': self.get_payment_method,
            'get_client_invoice_address': self.get_client_invoice_address,
            'get_client_shipping_address': self.get_client_shipping_address,

        })

    def get_client_order_ref(self, obj):

        order_numbers = [str(obj)]
        client_ref_name=None
        self.cr.execute("SELECT client_order_ref FROM sale_order WHERE name=%s", (order_numbers))
        for item in self.cr.fetchall():
            client_ref_name = item[0]
        return client_ref_name if client_ref_name else '-'

    def get_packing_slip(self, obj):
        order_numbers = [str(obj)]
        pack_ref_cus =None

        self.cr.execute("SELECT x_pack_ref_cus FROM sale_order WHERE name=%s", (order_numbers))
        for item in self.cr.fetchall():
            pack_ref_cus = item[0]

        return pack_ref_cus if pack_ref_cus else '--'

    def get_purchase_ref(self, obj):
        order_numbers = [str(obj)]
        purchase_ref_cus =None
        self.cr.execute("SELECT x_puchase_ref_cus FROM sale_order WHERE name=%s", (order_numbers))
        for item in self.cr.fetchall():
            purchase_ref_cus = item[0]

        return purchase_ref_cus if purchase_ref_cus else '---'

    def get_payment_method(self, obj):
        order_numbers = [str(obj)]
        note = None
        """
        # self.cr.execute("SELECT note FROM sale_order WHERE name=%s", (order_numbers))
        payment_method_query = "SELECT note FROM sale_order WHERE name='{0}'".format(order_numbers[0])
        self.cr.execute(payment_method_query)

        for item in self.cr.fetchall():
            note = item[0]
        """
        # import pdb;pdb.set_trace()

        context = {
            'lang': 'en_US',
            'params': {'action': 404},
            'tz': 'Asia/Dhaka',
            'uid': self.uid
        }

        so_obj = self.pool.get('sale.order')
        so_list = so_obj.search(self.cr, self.uid, [('name', '=', str(order_numbers[0]))])
        # so_obj.browse(self.cr, self.uid, so_list, context=context)

        # return note if note else '.'
        return so_obj.browse(self.cr, self.uid, so_list, context=context)

    def get_client_invoice_address(self, obj):

        order_numbers = [str(obj)]
        context = {
            'lang': 'en_US',
            'params': {'action': 404},
            'tz': 'Asia/Dhaka',
            'uid': self.uid
        }

        partner_invoice_id_query = "SELECT partner_invoice_id FROM sale_order WHERE name='{0}'".format(order_numbers[0])
        self.cr.execute(partner_invoice_id_query)
        partner_invoice_id = 0
        for item in self.cr.fetchall():
            partner_invoice_id = int(item[0])

        res_partner_obj = self.pool.get('res.partner')
        res_partner = res_partner_obj.browse(self.cr, self.uid, [partner_invoice_id], context=context)

        return res_partner if res_partner else '-'

    def get_client_shipping_address(self, obj):

        order_numbers = [str(obj)]
        context = {
            'lang': 'en_US',
            'params': {'action': 404},
            'tz': 'Asia/Dhaka',
            'uid': self.uid
        }

        partner_shipping_id_query = "SELECT partner_shipping_id FROM sale_order WHERE name='{0}'".format(order_numbers[0])
        self.cr.execute(partner_shipping_id_query)
        for item in self.cr.fetchall():
            partner_shipping_id = int(item[0])

        res_partner_obj = self.pool.get('res.partner')
        res_partner = res_partner_obj.browse(self.cr, self.uid, [partner_shipping_id], context=context)

        return res_partner if res_partner else '-'


class report_packings_challan_layout(osv.AbstractModel):
    _name = 'report.packings_invoice_challan.report_packings_challan_layout'
    _inherit = 'report.abstract_report'
    _template = 'packings_invoice_challan.report_packings_challan_layout'
    _wrapped_report_class = packingsChallanData
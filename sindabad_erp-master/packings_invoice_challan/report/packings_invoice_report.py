import time
from openerp.report import report_sxw
from openerp.osv import osv


class packingsInvoiceData(report_sxw.rml_parse):
    def __init__(self, cr, uid, name, context):
        super(packingsInvoiceData, self).__init__(cr, uid, name, context)
        self.localcontext.update({
            'get_client_shipping_address': self.get_client_shipping_address,
        })



    def get_client_shipping_address(self, obj):

        origin = [str(obj)]
        picking_origin = None

        if origin is not None:
            self.cr.execute("SELECT origin FROM stock_picking WHERE name=%s", (origin))

            for item in self.cr.fetchall():
                picking_origin = item[0]

        order_numbers = [picking_origin]

        context = {
            'lang': 'en_US',
            'params': {'action': 404},
            'tz': 'Asia/Dhaka',
            'uid': self.uid
        }

        partner_shipping_id_query = "SELECT partner_shipping_id FROM sale_order WHERE name='{0}'".format(order_numbers[0])
        self.cr.execute(partner_shipping_id_query)
        for item in self.cr.fetchall():
            partner_shipping_id = int(item[0])

        res_partner_obj = self.pool.get('res.partner')
        res_partner = res_partner_obj.browse(self.cr, self.uid, [partner_shipping_id], context=context)

        return res_partner if res_partner else '-'




class report_packings_invoice_layout(osv.AbstractModel):
    _name = 'report.packings_invoice_challan.report_packings_invoice_layout'
    _inherit = 'report.abstract_report'
    _template = 'packings_invoice_challan.report_packings_invoice_layout'
    _wrapped_report_class = packingsInvoiceData
from datetime import datetime, timedelta
from openerp.osv import fields, osv
from openerp import api



class PackingList(osv.osv):
    _inherit = "packing.list"




    def print_packing_invoice_report(self, cr, uid, ids, context=None):

        context = dict(context or {}, active_ids=ids)
        return self.pool.get("report").get_action(cr, uid, ids, 'packings_invoice_challan.report_packings_invoice_layout', context=context)




    def print_packing_challan_report(self, cr, uid, ids, context=None):

        context = dict(context or {}, active_ids=ids)
        return self.pool.get("report").get_action(cr, uid, ids, 'packings_invoice_challan.report_packings_challan_layout', context=context)
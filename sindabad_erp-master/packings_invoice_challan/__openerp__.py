{
    'name': 'Packing Invoices and Delivery Challans Generate',
    'version': '8.0.0',
    'category': 'Sales',
    'description': """
Packing all invoices and delivery challans are printed from individual button action.  
=====================================================================================

""",
    'author': 'Md Rockibul Alam Babu',
    'depends': ['packings','account'],
    'data': [
        'views/report_packings_invoice_layout.xml',
        'views/report_packings_challan_layout.xml',

        'report/packings_inv_cha_report_menu.xml',

        'packings_inv_cha_view.xml',

    ],

    'installable': True,
    'auto_install': False,
}
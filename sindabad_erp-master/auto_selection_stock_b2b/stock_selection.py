from openerp import api
import logging
from datetime import datetime
from dateutil.relativedelta import relativedelta
from operator import itemgetter
import time

import openerp
from openerp import SUPERUSER_ID, api
from openerp import tools
from openerp.osv import fields, osv, expression
from openerp.tools.translate import _
from openerp.tools.float_utils import float_round as round

from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT, DATETIME_FORMATS_MAP
from openerp.tools.float_utils import float_compare
import openerp.addons.decimal_precision as dp

class auto_stock_selection(osv.osv_memory):

    _inherit = "back.to.back.order"

    @api.onchange('picking_type_id')
    def stock_location_onchange(self):

        default_stock = self.pool.get('stock.picking.type').browse(self._cr, self._uid, int(self.picking_type_id), context=self._context)

        default_stock_id = default_stock.default_location_dest_id

        self.location_id = default_stock_id

        return "True"



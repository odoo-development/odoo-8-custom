{
     'name': 'Auto stock selection in Back to Back PO',
     'description': 'In Back to Back PO if a Warehouse operation is selected then perspective stock will be selected automatically',
     'author': 'Mehedi Hasan',
     'depends': ['sgeede_b2b' ],

 }
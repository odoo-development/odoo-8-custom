# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2009 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import time
from openerp.report import report_sxw
from openerp.osv import osv


class salesmissingdata(report_sxw.rml_parse):

    def __init__(self, cr, uid, name, context):
        super(salesmissingdata, self).__init__(cr, uid, name, context)
        self.localcontext.update({
            'get_client_order_ref': self.get_client_order_ref,
            'get_packing_slip': self.get_packing_slip,
            'get_purchase_ref': self.get_purchase_ref,
            'get_payment_method': self.get_payment_method,
            'get_client_invoice_address': self.get_client_invoice_address,
            'get_client_shipping_address': self.get_client_shipping_address,

        })

    def get_client_order_ref(self, obj):

        origin = [str(obj)]
        picking_origin = None

        if origin is not None:
            self.cr.execute("SELECT origin FROM stock_picking WHERE name=%s", (origin))

            for item in self.cr.fetchall():
                picking_origin = item[0]


        client_ref_name = None

        if picking_origin:
            order_numbers = [picking_origin]
            self.cr.execute("SELECT client_order_ref FROM sale_order WHERE name=%s", (order_numbers))
            for item in self.cr.fetchall():
                client_ref_name = item[0]
        return client_ref_name if client_ref_name else '-'

    def get_packing_slip(self, obj):

        origin = [str(obj)]
        picking_origin = None

        if origin is not None:
            self.cr.execute("SELECT origin FROM stock_picking WHERE name=%s", (origin))

            for item in self.cr.fetchall():
                picking_origin = item[0]



        order_numbers = [picking_origin]
        pack_ref_cus =None

        if picking_origin:

            self.cr.execute("SELECT x_pack_ref_cus FROM sale_order WHERE name=%s", (order_numbers))
            for item in self.cr.fetchall():
                pack_ref_cus = item[0]

        return pack_ref_cus if pack_ref_cus else '--'

    def get_purchase_ref(self, obj):
        origin = [str(obj)]
        picking_origin = None

        if origin is not None:
            self.cr.execute("SELECT origin FROM stock_picking WHERE name=%s", (origin))

            for item in self.cr.fetchall():
                picking_origin = item[0]

        order_numbers = [picking_origin]
        purchase_ref_cus =None

        if picking_origin:

            self.cr.execute("SELECT x_puchase_ref_cus FROM sale_order WHERE name=%s", (order_numbers))
            for item in self.cr.fetchall():
                purchase_ref_cus = item[0]

        return purchase_ref_cus if purchase_ref_cus else '---'

    def get_payment_method(self, obj):
        origin = [str(obj)]
        picking_origin = None

        if origin is not None:
            self.cr.execute("SELECT origin FROM stock_picking WHERE name=%s", (origin))

            for item in self.cr.fetchall():
                picking_origin = item[0]

        order_numbers = [picking_origin]

        note =None
        if picking_origin:
            self.cr.execute("SELECT note FROM sale_order WHERE name=%s", (order_numbers))
            for item in self.cr.fetchall():
                note = item[0]
        return note if note else '.'

    def get_client_invoice_address(self, obj):
        origin = [str(obj)]
        picking_origin = None

        if origin is not None:
            self.cr.execute("SELECT origin FROM stock_picking WHERE name=%s", (origin))

            for item in self.cr.fetchall():
                picking_origin = item[0]

        order_numbers = [picking_origin]


        context = {
            'lang': 'en_US',
            'params': {'action': 404},
            'tz': 'Asia/Dhaka',
            'uid': self.uid
        }

        partner_invoice_id_query = "SELECT partner_invoice_id FROM sale_order WHERE name='{0}'".format(order_numbers[0])
        self.cr.execute(partner_invoice_id_query)
        for item in self.cr.fetchall():
            partner_invoice_id = int(item[0])

        res_partner_obj = self.pool.get('res.partner')
        res_partner = res_partner_obj.browse(self.cr, self.uid, [partner_invoice_id], context=context)

        return res_partner if res_partner else '-'

    def get_client_shipping_address(self, obj):

        origin = [str(obj)]
        picking_origin = None

        if origin is not None:
            self.cr.execute("SELECT origin FROM stock_picking WHERE name=%s", (origin))

            for item in self.cr.fetchall():
                picking_origin = item[0]

        order_numbers = [picking_origin]

        context = {
            'lang': 'en_US',
            'params': {'action': 404},
            'tz': 'Asia/Dhaka',
            'uid': self.uid
        }

        partner_shipping_id_query = "SELECT partner_shipping_id FROM sale_order WHERE name='{0}'".format(order_numbers[0])
        self.cr.execute(partner_shipping_id_query)
        for item in self.cr.fetchall():
            partner_shipping_id = int(item[0])

        res_partner_obj = self.pool.get('res.partner')
        res_partner = res_partner_obj.browse(self.cr, self.uid, [partner_shipping_id], context=context)

        return res_partner if res_partner else '-'

class report_deliverychallanlayout(osv.AbstractModel):
    _name = 'report.custom_delivery_report.report_deliverychallanlayout'
    _inherit = 'report.abstract_report'
    _template = 'custom_delivery_report.report_deliverychallanlayout'
    _wrapped_report_class = salesmissingdata

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:

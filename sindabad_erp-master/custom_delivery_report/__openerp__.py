# Author Mufti Muntasir Ahmed

{
    'name': 'Custom Delivery Challan Report',
    'version': '8.0.0',
    'category': 'Sales',
    'description': """
Allows you to print delivery challan    
==============================================================

It is a sample delivery challan report with custom fields which 
are exported from magento version 1.

""",
    'author': 'Mufti Muntasir Ahmed',
    'depends': ['stock'],
    'data': [

        'stock_report.xml',
        'views/report_deliverychallanlayout.xml',

    ],

    'installable': True,
    'auto_install': False,
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:

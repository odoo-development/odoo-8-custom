{
     'name': 'IRN Status in Purchase',
     'description': 'IRN Status in Purchase',
     'author': 'Mehedi Hasan',
     'depends': ['wms_inbound', 'purchase'],
     'data' : ['purchase/irn_status_view_po.xml']

 }
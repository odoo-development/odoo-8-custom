from datetime import datetime, timedelta
import time
from datetime import date
from openerp import SUPERUSER_ID
from openerp.osv import fields, osv
from openerp.tools.translate import _
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT
import openerp.addons.decimal_precision as dp
from openerp import workflow
from openerp import api
import datetime
from openerp.exceptions import except_orm, Warning, RedirectWarning
from openerp.tools.translate import _

class irn_control(osv.osv):
    _inherit = "irn.control"


    def confirm_wms_inbound_irn(self, cr, uid, ids, context=None):

        irn_env = self.pool.get('irn.control')
        irn_obj = irn_env.browse(cr, uid, ids, context=context)

        # check if already irn pushed

        if not irn_obj.qc_pushed:

            qc_data = {
                'irn_number': str(irn_obj.irn_number),
                'irn_id': irn_obj.id,
                'warehouse_id': irn_obj.warehouse_id.id,
                'po_number': str(irn_obj.po_number),
                'po_id': irn_obj.po_id,
                'remark': str(irn_obj.remark),
                'received_by': irn_obj.received_by.id,
            }

            qc_line_list = list()

            for line_item in irn_obj.irn_line:
                qc_line_list.append([0, False,
                                     {
                                         'product': line_item.product,
                                         'product_id': line_item.product_id,
                                         'product_ean': line_item.product_ean,
                                         'description': line_item.remark,
                                         'purchase_quantity': line_item.ordered_quantity,
                                         'received_quantity': line_item.received_quantity,
                                         'accepted_quantity': 0,
                                         'pending_quantity': line_item.received_quantity,
                                     }])

            qc_data['qc_line'] = qc_line_list

            qc_env = self.pool.get('quality.control')
            saved_qc_id = qc_env.create(cr, uid, qc_data, context=context)

            for single_id in ids:
                qc_pushed_update_query = "UPDATE irn_control SET qc_pushed=TRUE, date_confirm='{0}' WHERE id={1}".format(str(fields.datetime.now()), single_id)
                cr.execute(qc_pushed_update_query)
                cr.commit()

                qc_pushed_update_query_irn_line = "UPDATE irn_control_line SET qc_pushed=TRUE WHERE irn_line_id={0}".format(single_id)
                cr.execute(qc_pushed_update_query_irn_line)
                cr.commit()


                irn_status_update_query = "UPDATE purchase_order SET irn_created=TRUE WHERE id={0}".format(irn_obj.po_id)
                cr.execute(irn_status_update_query)
                cr.commit()

            # update irn end-to-end
            self.pool.get('inb.end.to.end').irn_confirm_update(cr, uid, str(irn_obj.po_number), irn_obj.po_id, str(fields.datetime.now()), context=context)

        return True


class purchase_order(osv.osv):
    _inherit = 'purchase.order'

    def irn_status_for_old_po(self, cr, uid, ids, context = None):

        get_po_number_from_irn = "SELECT po_id FROM irn_control WHERE qc_pushed = TRUE"
        cr.execute(get_po_number_from_irn)
        query_fetch = cr.fetchall()

        po_list = [line[0] for line in query_fetch]

        update_query = "UPDATE purchase_order SET irn_created = TRUE WHERE id IN {0}".format(tuple(po_list))
        cr.execute(update_query)
        cr.commit()

        return True

        #irn_obj = self.pool.get('irn.control')
        #confirm_irn = irn_obj.search([('qc_pushed', '=', 'TRUE')])
        #confirm_irn_id = self.browse(confirm_irn)



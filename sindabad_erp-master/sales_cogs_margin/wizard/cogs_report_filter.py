# Author S. M. Sazedul Haque 2018/11/07

from openerp.osv import fields, osv
import datetime


class COGSReportWiz(osv.osv_memory):
    """
    This wizard will provide the partner Ledger report by periods, between any two dates.
    """
    _name = 'cogs.report.wiz'
    _description = 'COGS Report Wiz'

    _columns = {
        'date_from': fields.datetime("Start Date"),
        'date_to': fields.datetime("End Date"),
        'customer_classification': fields.selection([('All', 'All'),
                                     ('Ananta', 'Ananta'),
                                     ('Corporate', 'Corporate'),
                                     ('Retail', 'Retail'),
                                     ('Employee', 'Employee'),
                                     ('General', 'General'),
                                     ('SOHO', 'SOHO'),
                                                     ],
                                    'Customer Classification'),

        'company': fields.many2one('res.partner', 'Customer')

    }

    _defaults = {
        # 'date_from': datetime.datetime.today(),
        'date_from': datetime.datetime.now(),
        # 'date_to': datetime.datetime.today()
        'date_to': datetime.datetime.now()
    }

    def _build_contexts(self, cr, uid, ids, data, context=None):
        if context is None:
            context = {}
        result = {
            'date_from': data['form']['date_from'], 'date_to': data['form']['date_to'],
            'customer_classification': data['customer_classification']['date_from'], 'company': data['form']['company'],
                  }

        return result

    def check_report(self, cr, uid, ids, context=None):
        if context is None:
            context = {}

        data = {}

        data = self.read(cr, uid, ids, ['date_from', 'date_to','customer_classification','company'], context=context)[0]

        datas = {
            'ids': context.get('active_ids', []),
            'model': 'cogs.report.wiz',
            'form': data
        }
        return {'type': 'ir.actions.report.xml',
                'report_name': 'stock.xls',
                'datas': datas
                }

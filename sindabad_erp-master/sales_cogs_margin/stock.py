# Author Mufti Muntasir Ahmed 25-04-2018

from openerp import models, api


class sales_cogs_margin(models.Model):
    _inherit = 'stock.picking'

    @api.model
    def _report_xls_fields(self):
        # 'date_done',
        return [
            'order_date','date_done','order','warehouse','return','odoo_order','customer_name','company_name','company_code','classification','email','company_email','product_name','product_sku','product_category','delivered_qty','cost_rate_avg','total_cost','sales_rate','total_sales','profit','x_custom_discount','x_discount','status','invoice_number','creation_date', 'invoice_state','delivery_date','comment'

        ]

    # Change/Add Template entries
    @api.model
    def _report_xls_template(self):

        return {}

# Author Mufti Muntasir Ahmed 25-04-2018

{
    'name': 'Sales Cogs Margin Report',
    'version': '8.0.1',
    'author': "Mufti Muntasir Ahmed",
    'category': 'Sales',
    'summary': 'Details Summary of Delievred quantity with profit and Loss Statement',
    'depends': ['stock', 'report_xls'],
    'data': [
        'security/sales_cogs_security.xml',
        'security/ir.model.access.csv',
        'wizard/cogs_report_filter.xml',
        'report/stock_xls.xml',
    ],
}

# Author S. M. Sazedul Haque 2018/11/07

from openerp.osv import fields, osv
import datetime


class ManifestReprotSummary(osv.osv_memory):
    """
    This wizard will provide the partner Ledger report by periods, between any two dates.
    """
    _name = 'manifest.report.summary'
    _description = 'Manifest Summary'

    _columns = {
        'date_from': fields.date("Start Date"),
        'date_to': fields.date("End Date"),

    }

    _defaults = {
        'date_from': datetime.datetime.today(),
        'date_to': datetime.datetime.today()
    }

    def _build_contexts(self, cr, uid, ids, data, context=None):
        if context is None:
            context = {}
        result = {}

        result['date_from'] = data['form']['date_from']
        result['date_to'] = data['form']['date_to']

        return result

    def check_report(self, cr, uid, ids, context=None):
        if context is None:
            context = {}

        data = {}

        data = self.read(cr, uid, ids, ['date_from', 'date_to'], context=context)[0]

        datas = {
            'ids': context.get('active_ids', []),
            'model': 'manifest.report.summary',
            'form': data
        }
        return {'type': 'ir.actions.report.xml',
                'report_name': 'wms.manifest.process.xls',
                'datas': datas
                }

# -*- coding: utf-8 -*-
# Author S. M. Sazedul Haque 2018/11/06
{
    'name': "WMS Manifest Summary Report",

    'summary': """
        WMS Manifest Summary Report """,

    'description': """
        This module will help to generate WMS Manifest Summary Report
    """,

    'author': "S. M. Sazedul Haque - Zero Gravity Ventures Ltd",
    'website': "http://zerogravity.com.bd/",

    'category': 'Accounts',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['wms_manifest', 'sale', 'report_xls'],

    # always loaded
    'data': [
        'report/wms_manifest_summary_xls.xml',
        'wizard/wms_manifest_summary_view.xml',
    ],

}

# Author S. M. Sazedul Haque 2018/11/06

from openerp import models, api


class wms_manifest_line(models.Model):
    _inherit = 'wms.manifest.process'

    @api.model
    def _report_xls_fields(self):

        return [
            'sl_no',
            'wms_manifest_id',
            'magento_no',
            'invoice_number',
            'invoice_status',
            'customer_name',
            'address',
            'shipment_date',
            'amount',
            'vehicle_info',
            # 'vehicle_start',
            # 'vehicle_end',
            'assigned_to',
            'status',
            'remark',

        ]

    # Change/Add Template entries
    @api.model
    def _report_xls_template(self):

        return {}

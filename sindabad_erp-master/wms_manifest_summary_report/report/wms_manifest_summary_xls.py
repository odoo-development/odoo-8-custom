# Author S. M. Sazedul Haque 2018/11/06

import xlwt
from datetime import datetime, timedelta
import time
from openerp.osv import orm
from openerp.report import report_sxw
from openerp.addons.report_xls.report_xls import report_xls
from openerp.addons.report_xls.utils import rowcol_to_cell, _render
from openerp.tools.translate import translate, _
import logging
_logger = logging.getLogger(__name__)


_ir_translation_name = 'wms.manifest.process.xls'


class wms_manifest_line_xls_parser(report_sxw.rml_parse):

    def __init__(self, cr, uid, name, context):
        super(wms_manifest_line_xls_parser, self).__init__(
            cr, uid, name, context=context)
        move_obj = self.pool.get('wms.manifest.process')
        self.context = context
        wanted_list = move_obj._report_xls_fields(cr, uid, context)
        template_changes = move_obj._report_xls_template(cr, uid, context)
        self.localcontext.update({
            'datetime': datetime,
            'wanted_list': wanted_list,
            'template_changes': template_changes,
            '_': self._,
        })

    def _(self, src):
        lang = self.context.get('lang', 'en_US')
        return translate(self.cr, _ir_translation_name, 'report', lang, src) \
            or src


class wms_manifest_line_xls(report_xls):

    def __init__(self, name, table, rml=False, parser=False, header=True, store=False, context=None):
        super(wms_manifest_line_xls, self).__init__(
            name, table, rml, parser, header, store)
        # Cell Styles
        _xs = self.xls_styles
        # header
        rh_cell_format = _xs['bold'] + _xs['fill'] + _xs['borders_all']
        self.rh_cell_style = xlwt.easyxf(rh_cell_format)
        self.rh_cell_style_center = xlwt.easyxf(rh_cell_format + _xs['center'])
        self.rh_cell_style_right = xlwt.easyxf(rh_cell_format + _xs['right'])
        # lines
        aml_cell_format = _xs['borders_all']
        self.aml_cell_style = xlwt.easyxf(aml_cell_format)
        self.aml_cell_style_center = xlwt.easyxf(
            aml_cell_format + _xs['center'])
        self.aml_cell_style_date = xlwt.easyxf(
            aml_cell_format + _xs['left'],
            num_format_str=report_xls.date_format)
        self.aml_cell_style_decimal = xlwt.easyxf(
            aml_cell_format + _xs['right'],
            num_format_str=report_xls.decimal_format)
        # totals
        rt_cell_format = _xs['bold'] + _xs['fill'] + _xs['borders_all']
        self.rt_cell_style = xlwt.easyxf(rt_cell_format)
        self.rt_cell_style_right = xlwt.easyxf(rt_cell_format + _xs['right'])
        self.rt_cell_style_decimal = xlwt.easyxf(
            rt_cell_format + _xs['right'],
            num_format_str=report_xls.decimal_format)

        # XLS Template
        self.col_specs_template = {
            'sl_no': {
                'header': [1, 20, 'text', _render("_('SL#')")],
                'lines': [1, 0, 'text', _render("sl_no or ''")],
                'totals': [1, 0, 'text', None]},
            'wms_manifest_id': {
                'header': [1, 20, 'text', _render("_('Manifest no.')")],
                'lines': [1, 0, 'text', _render("line.wms_manifest_id.id or ''")],
                'totals': [1, 0, 'text', None]},
            'magento_no': {
                'header': [1, 20, 'text', _render("_('Order')")],
                'lines': [1, 0, 'text', _render("line.magento_no or ''")],
                'totals': [1, 0, 'text', None]},
            'invoice_number': {
                'header': [1, 20, 'text', _render("_('Invoice No.')")],
                'lines': [1, 0, 'text', _render("line.invoice_number or ''")],
                'totals': [1, 0, 'text', None]},
            'invoice_status': {
                'header': [1, 20, 'text', _render("_('Invoice Status')")],
                'lines': [1, 0, 'text', _render("invoice_status or ''")],
                'totals': [1, 0, 'text', None]},
            'customer_name': {
                'header': [1, 20, 'text', _render("_('Customer Name')")],
                'lines': [1, 0, 'text', _render("line.customer_name or ''")],
                'totals': [1, 0, 'text', None]},
            'address': {
                'header': [1, 20, 'text', _render("_('Delivery Address')")],
                'lines': [1, 0, 'text', _render("line.address or ''")],
                'totals': [1, 0, 'text', None]},
            'shipment_date': {
                'header': [1, 20, 'text', _render("_('Shipment Date')")],
                'lines': [1, 0, 'text', _render("manifest_process.confirm_time or ''")],
                'totals': [1, 0, 'text', None]},
            'amount': {
                'header': [1, 20, 'text', _render("_('Invoice Amount')")],
                'lines': [1, 0, 'text', _render("line.amount or ''")],
                'totals': [1, 0, 'text', None]},
            'vehicle_info': {
                'header': [1, 20, 'text', _render("_('Vehicle Number')")],
                'lines': [1, 0, 'text', _render("manifest_process.vehicle_number or ''")],
                'totals': [1, 0, 'text', None]},
            'vehicle_start': {
                'header': [1, 20, 'text', _render("_('Vehicle From')")],
                'lines': [1, 0, 'text', _render("manifest_process.vehicle_from or ''")],
                'totals': [1, 0, 'text', None]},
            'vehicle_end': {
                'header': [1, 20, 'text', _render("_('Vehicle To')")],
                'lines': [1, 0, 'text', _render("manifest_process.vehicle_to or ''")],
                'totals': [1, 0, 'text', None]},
            'assigned_to': {
                'header': [1, 20, 'text', _render("_('Delivery Person')")],
                'lines': [1, 0, 'text', _render("manifest_process.assigned_to or ''")],
                'totals': [1, 0, 'text', None]},
            'status': {
                'header': [1, 20, 'text', _render("_('Payment Status')")],
                'lines': [1, 0, 'text', _render("line.payment_type or ''")],
                'totals': [1, 0, 'text', None]},
            'remark': {
                'header': [1, 20, 'text', _render("_('Remarks')")],
                'lines': [1, 0, 'text', _render("manifest_process.remark or ''")],
                'totals': [1, 0, 'text', None]},

        }

    def _lines_get(self, partner):

        start_date = self.context.get('start_date')
        end_date = self.context.get('end_date')

        moveline_obj = self.pool['account.move.line']

        # order_numbers = [str(obj)]
        # purchase_ref_cus = None
        # self.cr.execute("SELECT x_puchase_ref_cus FROM sale_order WHERE name=%s", (order_numbers))
        parent_list = []

        if partner.parent_id:
            parent_list.append(partner.parent_id.id)

            self.cr.execute("select id from  res_partner where customer=True and id=%s", ([partner.parent_id.id]))

            for items in self.cr.fetchall():
                parent_list.append(items[0])
        else:
            parent_list.append(partner.id)

        self.cr.execute("select id from  res_partner where customer=True and parent_id=%s", ([partner.id]))
        for items in self.cr.fetchall():
            parent_list.append(items[0])

        movelines = moveline_obj.search(self.cr, self.uid,
                                        [('create_date', '<=', end_date), ('create_date', '>=', start_date)
                                         ])

        movelines = moveline_obj.browse(self.cr, self.uid, movelines)

        return movelines

    def _get_invoice_status(self, cr, uid, line, context):
        reserved_date = ''
        account_invoice = self.pool.get('account.invoice').browse(cr, uid, [line.invoice_id], context=context)

        return account_invoice.state

    def generate_xls_report(self, _p, _xs, data, objects, wb):

        wanted_list = _p.wanted_list
        self.col_specs_template.update(_p.template_changes)
        _ = _p._

        context = self.context
        st_date = data.get('form').get('date_from')
        end_date = data.get('form').get('date_to')
        context['start_date'] = st_date
        context['end_date'] = end_date
        self.context = context
        date_range = _("Date: %s to %s" % (st_date, end_date))

        # report_name = objects[0]._description or objects[0]._name
        report_name = _("WMS Manifest summary")
        ws = wb.add_sheet(report_name[:31])
        ws.panes_frozen = True
        ws.remove_splits = True
        ws.portrait = 0  # Landscape
        ws.fit_width_to_pages = 1
        row_pos = 0

        # set print header/footer
        ws.header_str = self.xls_headers['standard']
        ws.footer_str = self.xls_footers['standard']

        # Title
        cell_style = xlwt.easyxf(_xs['xls_title'])
        c_specs = [
            ('report_name', 4, 3, 'text', report_name),
        ]
        row_data = self.xls_row_template(c_specs, ['report_name'])
        row_pos = self.xls_write_row(
            ws, row_pos, row_data, row_style=cell_style)
        row_pos += 1

        # date_range Column --------------------------#
        c_specs = [
            ('date_range', 4, 3, 'text', date_range),
        ]
        row_data = self.xls_row_template(c_specs, ['date_range'])
        row_pos = self.xls_write_row(
            ws, row_pos, row_data, row_style=cell_style)
        row_pos += 1


        # Total Amount Column --------------------------#

        manifest_process_total_amount = 0
        manifest_process_total_amount = sum(o.manifest_total_amount for o in objects)
        text_of_amount = "Total Amount: %s" % manifest_process_total_amount
        c_specs = [
            ('text_of_amount', 4, 3, 'text', text_of_amount),
        ]
        row_data = self.xls_row_template(c_specs, ['text_of_amount'])
        row_pos = self.xls_write_row(
            ws, row_pos, row_data, row_style=cell_style)
        row_pos += 1

        # Column headers--------------------------#
        c_specs = map(lambda x: self.render(
            x, self.col_specs_template, 'header', render_space={'_': _p._}),
            wanted_list)
        row_data = self.xls_row_template(c_specs, [x[0] for x in c_specs])
        row_pos = self.xls_write_row(
            ws, row_pos, row_data, row_style=self.rh_cell_style,
            set_column_size=True)
        ws.set_horz_split_pos(row_pos)

        sl_no = 0
        for manifest_process in objects:
            create_date = datetime.strptime(manifest_process.create_date, "%Y-%m-%d %H:%M:%S").date()
            start_date = datetime.strptime(st_date, "%Y-%m-%d").date()
            ed_date = datetime.strptime(end_date, "%Y-%m-%d").date()
            # manifest_process_total_amount = manifest_process_total_amount + manifest_process.manifest_total_amount

            if   create_date >=   start_date and create_date <=  ed_date:

                total_amount = 0
                for line in manifest_process.picking_line:
                    invoice_status = self._get_invoice_status(self.cr, self.uid, line, self.context)

                    sl_no +=1
                    c_specs = map(
                        lambda x: self.render(x, self.col_specs_template, 'lines'),
                        wanted_list)

                    for list_data in c_specs:

                        if str(list_data[0]) == str('sl_no'):
                            list_data[4] = str(sl_no)
                        if str(list_data[0]) == str('wms_manifest_id'):
                            list_data[4] = str(line.wms_manifest_id.id)
                        if str(list_data[0]) == str('magento_no'):
                            list_data[4] = str(line.magento_no)
                        if str(list_data[0]) == str('invoice_number'):
                            list_data[4] = str(line.invoice_number)
                        if str(list_data[0]) == str('invoice_status'):
                            list_data[4] = str(invoice_status)
                        if str(list_data[0]) == str('customer_name'):
                            list_data[4] = str(line.customer_name)
                        if str(list_data[0]) == str('address'):
                            list_data[4] = str(line.address)
                        if str(list_data[0]) == str('shipment_date'):
                            list_data[4] = str(manifest_process.confirm_time)
                        if str(list_data[0]) == str('amount'):
                            list_data[4] = str(line.amount)
                        if str(list_data[0]) == str('vehicle_info'):
                            list_data[4] = str(manifest_process.vehicle_number)
                        if str(list_data[0]) == str('vehicle_start'):
                            list_data[4] = str(manifest_process.vehicle_from)
                        if str(list_data[0]) == str('vehicle_end'):
                            list_data[4] = str(manifest_process.vehicle_from)
                        if str(list_data[0]) == str('assigned_to'):
                            list_data[4] = str(manifest_process.assigned_to)
                        if str(list_data[0]) == str('status'):
                            list_data[4] = str(line.payment_type)
                        if str(list_data[0]) == str('remark'):
                            list_data[4] = str(manifest_process.remark)

                    row_data = self.xls_row_template(c_specs, [x[0] for x in c_specs])
                    row_pos = self.xls_write_row(
                        ws, row_pos, row_data, row_style=self.aml_cell_style)



wms_manifest_line_xls('report.wms.manifest.process.xls',
                      'wms.manifest.process',
                      parser=wms_manifest_line_xls_parser)

# Author S. M. Sazedul Haque 2019-01-19

{
    'name': 'Scheduler for Send Common Email with Report',
    'version': '1.0.0',
    'category': 'Accounts',
    'description': """
Allows you to Send Common Email   
==============================================================

""",
    'author': 'S. M. Sazedul Haque',
    'depends': ['account', 'report', 'purchase'],
    'data': [
        'send_purchase_email_template.xml',
    ],
    'installable': True,
    'auto_install': False,
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:

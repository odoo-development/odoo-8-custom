# Author S. M. Sazedul Haque 2019-01-01

import time
from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta
from openerp.report import report_sxw
from openerp.osv import fields, osv
import calendar


class customer_statement(osv.osv):
    """
    This Method will help to send customer statement via email using
    """

    _inherit = 'res.partner'


    def send_common_mail(self, cr, uid, ids=None, templateEmail=None, cron_mode=True, context=None):
        print "ids, templateEmail=", ids, templateEmail, '*-'*20

        #
        # template = request.env.ref('module_name.email_template_id')
        # template.with_context(local_context).send_mail(object.id, force_send=True, raise_exception=True)
        if templateEmail is None:
            templateEmail = 'Send Common Email'

        template_id = \
        self.pool.get('email.template').search(cr, uid, [('name', '=', templateEmail)],
                                               context=context)
        if template_id:
            today = datetime.today().date()
            first_date_of_month = today.strftime('%Y-%m-')+'01'
            partner_obj = self.pool.get('res.partner')
            if not ids is None:
                res_partner_obj = partner_obj.search(cr, uid, [('id', 'in', ids)],
                                                       context=context)
                i = 1
                email_lists=[]
                for id in res_partner_obj:
                    c_obj = partner_obj.browse(cr, uid, [id])
                    print 'SL#', i
                    i =i+1
                    email_lists.append(c_obj.email)
                    if c_obj.email:
                        try:
                            email_obj = self.pool.get('email.template').send_mail(cr, uid, template_id[0], id, force_send=True)
                            return True
                        except:
                            return False



class po_inherit(osv.osv):
    """
    This Method will help to send customer statement via email using
    """

    _inherit = 'purchase.order'


    def send_common_mail(self, cr, uid, status=False, id=None, templateEmail=None, cron_mode=True, context=None):
        print 'status=', status, '*-'*15
        if status is True:
            print "id, templateEmail=", id, templateEmail, '*-' * 20

            if not templateEmail is None:
                template_id = \
                    self.pool.get('email.template').search(cr, uid, [('name', '=', templateEmail)])
                if template_id:
                    if not id is None:
                        try:
                            print 'id=',id, 'template_id=',template_id
                            self.pool.get('email.template').send_mail(cr, uid, template_id[0], id, force_send=True)
                            return True
                        except:
                            return False

import time
import datetime
from openerp import tools
from openerp.osv import fields, osv
from openerp import api
from openerp.tools.translate import _


class pickup_request(osv.osv):
    _name = "pickup.request"
    _description = "Pickup Request"
    _order = 'id desc'

    READONLY_STATES = {
        'confirmed': [('readonly', True)],
        'done': [('readonly', True)]
    }

    def compute_next_day_date(self, strdate):
        nextday = datetime.timedelta(days=1)
        curdate = datetime.datetime.strptime(strdate, tools.DEFAULT_SERVER_DATE_FORMAT)
        return datetime.date.strftime(curdate + nextday, tools.DEFAULT_SERVER_DATE_FORMAT)

    # auto pickup close after 84 hrs
    # def _close_status_update(self, cr, uid, ids, fields, arg, context=None):
    #     res = {}
    #     for single_id in ids:
    #         pickup_req_obj = self.pool.get('pickup.request')
    #         pickup_obj= pickup_req_obj.browse(cr, uid, single_id, context=context)
    #         if pickup_obj.pickup_not_receive_date:
    #
    #             reattempt_time = datetime.datetime.strptime(pickup_obj.pickup_not_receive_date,'%Y-%m-%d %H:%M:%S')
    #             cancel_time = str(reattempt_time+datetime.timedelta(hours=84))
    #             current_datetime = str(datetime.datetime.now())
    #             if current_datetime > cancel_time and pickup_obj.state=='unsuccessful':
    #                 pickup_req_obj.cancel_wms_inbound_pickup_request(cr, uid, [single_id], context)
    #     return res

    _columns = {
        # 'reattempt_pickup_auto_cancel': fields.function(_close_status_update, string="Manifest", type="many2many", relation='pickup.manifest'),
        'scan': fields.char('PO Scan'),
        'product_scan': fields.char('Product Scan'),
        'warehouse_id': fields.many2one('stock.warehouse', string='Pickup Warehouse'),
        'warehouse': fields.integer(),
        'po_warehouse_id': fields.many2one('stock.warehouse', string='PO Warehouse'),
        'po_warehouse': fields.integer(),
        'name': fields.char('Pickup Number'),
        'po_number': fields.char('PO Number'),
        'po': fields.char('PO'),
        'po_id': fields.integer('Purchase ID'),
        'po_id_ref': fields.many2one('purchase.order', 'Purchase'),
        'partner_id': fields.many2one('res.partner', 'Vendor',
                                      change_default=True, track_visibility='always'),
        'partner': fields.integer(),
        'partial_pickup_ref_id': fields.integer(),
        'reattempt_number': fields.integer(string="Re-attempt Count"),
        'partner_address': fields.char('Vendor Address', required=True),


        'area': fields.selection([
            ("Abdullahpur", "Abdullahpur"),
            ("Adabor", "Adabor"),
            ("Aditmari", "Aditmari"),
            ("AEPZ (Adamjee Export Processing Zone)", "AEPZ (Adamjee Export Processing Zone)"),
            ("Agailzhara", "Agailzhara"),
            ("Agargaon", "Agargaon"),
            ("Ahsanganj", "Ahsanganj"),
            ("Airport", "Airport"),
            ("Akhaura", "Akhaura"),
            ("Akkelpur", "Akkelpur"),
            ("Alaipur", "Alaipur"),
            ("Alamdanga", "Alamdanga"),
            ("Alamdighi", "Alamdighi"),
            ("Alfadanga", "Alfadanga"),
            ("Alikadam", "Alikadam"),
            ("Amtali", "Amtali"),
            ("Anawara", "Anawara"),
            ("Araihazar", "Araihazar"),
            ("Arambagh", "Arambagh"),
            ("Arpara", "Arpara"),
            ("Ashashuni", "Ashashuni"),
            ("Ashulia", "Ashulia"),
            ("Atpara", "Atpara"),
            ("Azampur", "Azampur"),
            ("Azimpur", "Azimpur"),
            ("Azmireeganj", "Azmireeganj"),
            ("Babu Bazar", "Babu Bazar"),
            ("Babuganj", "Babuganj"),
            ("Badalgachhi", "Badalgachhi"),
            ("Badda", "Badda"),
            ("Bagerhat Sadar", "Bagerhat Sadar"),
            ("Bagha", "Bagha"),
            ("Bagharpara", "Bagharpara"),
            ("Bahubal", "Bahubal"),
            ("Baidder Bazar", "Baidder Bazar"),
            ("Baiddya Jam Toil", "Baiddya Jam Toil"),
            ("Bajitpur", "Bajitpur"),
            ("Bakshi Bazar", "Bakshi Bazar"),
            ("Bakshigonj", "Bakshigonj"),
            ("Balaganj", "Balaganj"),
            ("Baliadangi", "Baliadangi"),
            ("Baliakandi", "Baliakandi"),
            ("Bamna", "Bamna"),
            ("Banani", "Banani"),
            ("Banani Cantonment", "Banani Cantonment"),
            ("Banani Dohs", "Banani Dohs"),
            ("Banaripara", "Banaripara"),
            ("Banasree", "Banasree"),
            ("Banchharampur", "Banchharampur"),
            ("Bandar", "Bandar"),
            ("Bandarban Sadar", "Bandarban Sadar"),
            ("Bangabhaban", "Bangabhaban"),
            ("Bangla Bazar", "Bangla Bazar"),
            ("Bangla Hili", "Bangla Hili"),
            ("Bangla Motor", "Bangla Motor"),
            ("Bangshal", "Bangshal"),
            ("Baniachang", "Baniachang"),
            ("Banwarinagar", "Banwarinagar"),
            ("Barajalia", "Barajalia"),
            ("Barakal", "Barakal"),
            ("Baralekha", "Baralekha"),
            ("Barguna Sadar", "Barguna Sadar"),
            ("Barhamganj", "Barhamganj"),
            ("Barhatta", "Barhatta"),
            ("Baridhara", "Baridhara"),
            ("Baridhara Dohs", "Baridhara Dohs"),
            ("Barishal Sadar", "Barishal Sadar"),
            ("Barura", "Barura"),
            ("Basabo", "Basabo"),
            ("Basail", "Basail"),
            ("Bashundhara R/A", "Bashundhara R/A"),
            ("Basurhat", "Basurhat"),
            ("Batiaghat", "Batiaghat"),
            ("Bauphal", "Bauphal"),
            ("Begumganj", "Begumganj"),
            ("Belabo", "Belabo"),
            ("Belkuchi", "Belkuchi"),
            ("Bera", "Bera"),
            ("Betagi", "Betagi"),
            ("Bhabaniganj", "Bhabaniganj"),
            ("Bhairob", "Bhairob"),
            ("Bhaluka", "Bhaluka"),
            ("Bhandaria", "Bhandaria"),
            ("Bhanga", "Bhanga"),
            ("Bhangura", "Bhangura"),
            ("Bhedorganj", "Bhedorganj"),
            ("Bheramara", "Bheramara"),
            ("Bhola Sadar", "Bhola Sadar"),
            ("Bholahat", "Bholahat"),
            ("Bhuapur", "Bhuapur"),
            ("Bhurungamari", "Bhurungamari"),
            ("Bianibazar", "Bianibazar"),
            ("Bilaichhari", "Bilaichhari"),
            ("Biral", "Biral"),
            ("Birampur", "Birampur"),
            ("Birganj", "Birganj"),
            ("Bishamsarpur", "Bishamsarpur"),
            ("Bishwanath", "Bishwanath"),
            ("Boalkhali", "Boalkhali"),
            ("Boalmari", "Boalmari"),
            ("Boda", "Boda"),
            ("Bogra Sadar", "Bogra Sadar"),
            ("Bonarpara", "Bonarpara"),
            ("Borhanuddin Upo", "Borhanuddin Upo"),
            ("Brahamanbaria Sadar", "Brahamanbaria Sadar"),
            ("Brahmanpara", "Brahmanpara"),
            ("Burichang", "Burichang"),
            ("Cantonment (Dhaka)", "Cantonment (Dhaka)"),
            ("Chalna Ankorage", "Chalna Ankorage"),
            ("Chalna Bazar", "Chalna Bazar"),
            ("Chandina", "Chandina"),
            ("Chandpur Sadar", "Chandpur Sadar"),
            ("Chankharpool", "Chankharpool"),
            ("Chapinawabganj Sadar", "Chapinawabganj Sadar"),
            ("Char Alexgander", "Char Alexgander"),
            ("Charbhadrasan", "Charbhadrasan"),
            ("Charfashion", "Charfashion"),
            ("Charghat", "Charghat"),
            ("Chatkhil", "Chatkhil"),
            ("Chatmohar", "Chatmohar"),
            ("Chaugachha", "Chaugachha"),
            ("Chawk Bazar", "Chawk Bazar"),
            ("Chhagalnaia", "Chhagalnaia"),
            ("Chhatak", "Chhatak"),
            ("Chilmari", "Chilmari"),
            ("Chiringga", "Chiringga"),
            ("Chitalmari", "Chitalmari"),
            ("Chittagong Sadar", "Chittagong Sadar"),
            ("Chotto Dab", "Chotto Dab"),
            ("Chouddagram", "Chouddagram"),
            ("Chrirbandar", "Chrirbandar"),
            ("Chuadanga Sadar", "Chuadanga Sadar"),
            ("Chunarughat", "Chunarughat"),
            ("Comilla Sadar", "Comilla Sadar"),
            ("Coxs Bazar Sadar", "Coxs Bazar Sadar"),
            ("Dabiganj", "Dabiganj"),
            ("Dagonbhuia", "Dagonbhuia"),
            ("Dakshin Kafrul", "Dakshin Kafrul"),
            ("Dakshinkhan", "Dakshinkhan"),
            ("Damudhya", "Damudhya"),
            ("Damurhuda", "Damurhuda"),
            ("Darus-Salam", "Darus-Salam"),
            ("Dashmina", "Dashmina"),
            ("Daudkandi", "Daudkandi"),
            ("Davidhar", "Davidhar"),
            ("Debbhata", "Debbhata"),
            ("Debottar", "Debottar"),
            ("Delduar", "Delduar"),
            ("Demra", "Demra"),
            ("DEPZ (Dhaka Export Processing Zone)", "DEPZ (Dhaka Export Processing Zone)"),
            ("Dewangonj", "Dewangonj"),
            ("Dhamrai", "Dhamrai"),
            ("Dhamuirhat", "Dhamuirhat"),
            ("Dhangora", "Dhangora"),
            ("Dhanmondi", "Dhanmondi"),
            ("Dharmapasha", "Dharmapasha"),
            ("Dhaur", "Dhaur"),
            ("Dhirai Chandpur", "Dhirai Chandpur"),
            ("Dhobaura", "Dhobaura"),
            ("Dhunat", "Dhunat"),
            ("Digalia", "Digalia"),
            ("Diginala", "Diginala"),
            ("Dilkusha", "Dilkusha"),
            ("Dimla", "Dimla"),
            ("Dinajpur Sadar", "Dinajpur Sadar"),
            ("Dip Nagar", "Dip Nagar"),
            ("Dohar", "Dohar"),
            ("Dokkhin Khan", "Dokkhin Khan"),
            ("Domar", "Domar"),
            ("Doulatganj", "Doulatganj"),
            ("Doulatkhan", "Doulatkhan"),
            ("Doulatpur", "Doulatpur"),
            ("Duara Bazar", "Duara Bazar"),
            ("Duaripara", "Duaripara"),
            ("Dupchachia", "Dupchachia"),
            ("Durgapur", "Durgapur"),
            ("East Joara", "East Joara"),
            ("Elephant Road", "Elephant Road"),
            ("Eskaton", "Eskaton"),
            ("Estern Housing", "Estern Housing"),
            ("Fakirhat", "Fakirhat"),
            ("Faridganj", "Faridganj"),
            ("Faridpur Sadar", "Faridpur Sadar"),
            ("Fatikchhari", "Fatikchhari"),
            ("Fatullah", "Fatullah"),
            ("Fenchuganj", "Fenchuganj"),
            ("Feni Sadar", "Feni Sadar"),
            ("Firmgate", "Firmgate"),
            ("Fulbaria", "Fulbaria"),
            ("Fulbaria", "Fulbaria"),
            ("Gabtoli", "Gabtoli"),
            ("Gaforgaon", "Gaforgaon"),
            ("Gaibandha Sadar", "Gaibandha Sadar"),
            ("Gajaria", "Gajaria"),
            ("Galachipa", "Galachipa"),
            ("Gangachara", "Gangachara"),
            ("Gangni", "Gangni"),
            ("Gazipur Sadar", "Gazipur Sadar"),
            ("Gendaria", "Gendaria"),
            ("Ghatail", "Ghatail"),
            ("Gheor", "Gheor"),
            ("Ghungiar", "Ghungiar"),
            ("Goainhat", "Goainhat"),
            ("Gobindaganj", "Gobindaganj"),
            ("Godagari", "Godagari"),
            ("Gopalganj", "Gopalganj"),
            ("Gopalganj Sadar", "Gopalganj Sadar"),
            ("Gopalpur", "Gopalpur"),
            ("Gopalpur Upo", "Gopalpur Upo"),
            ("Gorakghat", "Gorakghat"),
            ("Goran", "Goran"),
            ("Gosairhat", "Gosairhat"),
            ("Gouranadi", "Gouranadi"),
            ("Gouripur", "Gouripur"),
            ("Green Road", "Green Road"),
            ("Gulistan", "Gulistan"),
            ("Gulshan 1", "Gulshan 1"),
            ("Gulshan 2", "Gulshan 2"),
            ("Hajiganj", "Hajiganj"),
            ("Hajirhat", "Hajirhat"),
            ("Haluaghat", "Haluaghat"),
            ("Harinakundu", "Harinakundu"),
            ("Harua", "Harua"),
            ("Hatgurudaspur", "Hatgurudaspur"),
            ("Hathazari", "Hathazari"),
            ("Hatibandha", "Hatibandha"),
            ("Hatirjheel", "Hatirjheel"),
            ("Hatirpool", "Hatirpool"),
            ("Hatiya", "Hatiya"),
            ("Hatshoshiganj", "Hatshoshiganj"),
            ("Hayemchar", "Hayemchar"),
            ("Hazaribagh", "Hazaribagh"),
            ("Hobiganj Sadar", "Hobiganj Sadar"),
            ("Homna", "Homna"),
            ("Hossenpur", "Hossenpur"),
            ("Ishwardi", "Ishwardi"),
            ("Islambagh", "Islambagh"),
            ("Islampur", "Islampur"),
            ("Islampur", "Islampur"),
            ("Isshwargonj", "Isshwargonj"),
            ("Itna", "Itna"),
            ("Jagnnathpur", "Jagnnathpur"),
            ("Jaintapur", "Jaintapur"),
            ("Jajira", "Jajira"),
            ("Jakiganj", "Jakiganj"),
            ("Jaldhaka", "Jaldhaka"),
            ("Jaldi", "Jaldi"),
            ("Jamalpur", "Jamalpur"),
            ("Janipur", "Janipur"),
            ("Jarachhari", "Jarachhari"),
            ("Jatrabari", "Jatrabari"),
            ("Jessore Sadar", "Jessore Sadar"),
            ("Jhalokathi Sadar", "Jhalokathi Sadar"),
            ("Jhikargachha", "Jhikargachha"),
            ("Jhinaigati", "Jhinaigati"),
            ("Jibanpur", "Jibanpur"),
            ("Jigatala", "Jigatala"),
            ("Jinaidaha Sadar", "Jinaidaha Sadar"),
            ("Joypurhat Sadar", "Joypurhat Sadar"),
            ("Jurain", "Jurain"),
            ("Kachua", "Kachua"),
            ("Kachua Upo", "Kachua Upo"),
            ("Kadamtali", "Kadamtali"),
            ("Kafrul", "Kafrul"),
            ("Kahalu", "Kahalu"),
            ("Kakrail", "Kakrail"),
            ("Kalabagan", "Kalabagan"),
            ("Kalachadpur", "Kalachadpur"),
            ("Kalai", "Kalai"),
            ("Kalampati", "Kalampati"),
            ("Kalaroa", "Kalaroa"),
            ("Kalauk", "Kalauk"),
            ("Kalia", "Kalia"),
            ("Kaliakaar", "Kaliakaar"),
            ("Kaliganj", "Kaliganj"),
            ("Kaliganj Upo", "Kaliganj Upo"),
            ("Kalihati", "Kalihati"),
            ("Kalkini", "Kalkini"),
            ("Kallyanpur", "Kallyanpur"),
            ("Kalmakanda", "Kalmakanda"),
            ("Kalshi", "Kalshi"),
            ("Kamalapur", "Kamalapur"),
            ("Kamalganj", "Kamalganj"),
            ("Kamrangirchar", "Kamrangirchar"),
            ("Kanaighat", "Kanaighat"),
            ("Kapashia", "Kapashia"),
            ("Kaptai", "Kaptai"),
            ("Karimganj", "Karimganj"),
            ("Karwan Bazar", "Karwan Bazar"),
            ("Kasba", "Kasba"),
            ("Kashiani", "Kashiani"),
            ("Kashkaolia", "Kashkaolia"),
            ("Kathalbagan", "Kathalbagan"),
            ("Kathalia", "Kathalia"),
            ("Katiadi", "Katiadi"),
            ("Kaukhali", "Kaukhali"),
            ("Kaunia", "Kaunia"),
            ("Kazipara", "Kazipara"),
            ("Kazipur", "Kazipur"),
            ("Kendua", "Kendua"),
            ("Keraniganj", "Keraniganj"),
            ("Keshabpur", "Keshabpur"),
            ("Khagrachari Sadar", "Khagrachari Sadar"),
            ("Khaliajuri", "Khaliajuri"),
            ("Khansama", "Khansama"),
            ("Khepupara", "Khepupara"),
            ("Khetlal", "Khetlal"),
            ("Khilgaon", "Khilgaon"),
            ("Khilkhet", "Khilkhet"),
            ("Khod Mohanpur", "Khod Mohanpur"),
            ("Khulna Sadar", "Khulna Sadar"),
            ("Kishoreganj Sadar", "Kishoreganj Sadar"),
            ("Kishoriganj", "Kishoriganj"),
            ("Kompanyganj", "Kompanyganj"),
            ("Kotalipara", "Kotalipara"),
            ("Kotchandpur", "Kotchandpur"),
            ("Kulaura", "Kulaura"),
            ("Kuliarchar", "Kuliarchar"),
            ("Kumarkhali", "Kumarkhali"),
            ("Kurigram Sadar", "Kurigram Sadar"),
            ("Kuril", "Kuril"),
            ("Kustia Sadar", "Kustia Sadar"),
            ("Kutubdia", "Kutubdia"),
            ("Lakshimpur Sadar", "Lakshimpur Sadar"),
            ("Lalbagh", "Lalbagh"),
            ("Lalitganj", "Lalitganj"),
            ("Lalmatia", "Lalmatia"),
            ("Lalmohan Upo", "Lalmohan Upo"),
            ("Lalmonirhat Sadar", "Lalmonirhat Sadar"),
            ("Laxman", "Laxman"),
            ("Laxmichhari", "Laxmichhari"),
            ("Laxmipasha", "Laxmipasha"),
            ("Lechhraganj", "Lechhraganj"),
            ("Lohagara", "Lohagara"),
            ("Lohajong", "Lohajong"),
            ("Longachh", "Longachh"),
            ("Madan", "Madan"),
            ("Madaripur Sadar", "Madaripur Sadar"),
            ("Madhabpur", "Madhabpur"),
            ("Madhupur", "Madhupur"),
            ("Madinabad", "Madinabad"),
            ("Madukhali", "Madukhali"),
            ("Magura Sadar", "Magura Sadar"),
            ("Mahadebpur", "Mahadebpur"),
            ("Mahalchhari", "Mahalchhari"),
            ("Maharajganj", "Maharajganj"),
            ("Mahendiganj", "Mahendiganj"),
            ("Maheshpur", "Maheshpur"),
            ("Maksudpur", "Maksudpur"),
            ("Malandah", "Malandah"),
            ("Malibag", "Malibag"),
            ("Manikchhari", "Manikchhari"),
            ("Manikganj Sadar", "Manikganj Sadar"),
            ("Marishya", "Marishya"),
            ("Mathargonj", "Mathargonj"),
            ("Mathbaria", "Mathbaria"),
            ("Matiranga", "Matiranga"),
            ("Matlab North", "Matlab North"),
            ("Matlab South", "Matlab South"),
            ("Meherpur Sadar", "Meherpur Sadar"),
            ("Meradia", "Meradia"),
            ("Mirpur", "Mirpur"),
            ("Mirpur", "Mirpur"),
            ("Mirpur 1", "Mirpur 1"),
            ("Mirpur 10", "Mirpur 10"),
            ("Mirpur 11", "Mirpur 11"),
            ("Mirpur 12", "Mirpur 12"),
            ("Mirpur 13", "Mirpur 13"),
            ("Mirpur 14", "Mirpur 14"),
            ("Mirpur 2", "Mirpur 2"),
            ("Mirpur 6", "Mirpur 6"),
            ("Mirpur 7", "Mirpur 7"),
            ("Mirpur DOHS", "Mirpur DOHS"),
            ("Mirsharai", "Mirsharai"),
            ("Mirzapur", "Mirzapur"),
            ("Mitford", "Mitford"),
            ("Mithamoin", "Mithamoin"),
            ("Mithapukur", "Mithapukur"),
            ("Moghbazar", "Moghbazar"),
            ("Mohajan", "Mohajan"),
            ("Mohakhali", "Mohakhali"),
            ("Mohakhali Dohs", "Mohakhali Dohs"),
            ("Mohammadpur", "Mohammadpur"),
            ("Mohammadpur", "Mohammadpur"),
            ("Mollahat", "Mollahat"),
            ("Monirampur", "Monirampur"),
            ("Monnunagar", "Monnunagar"),
            ("Monohordi", "Monohordi"),
            ("Morelganj", "Morelganj"),
            ("Motijheel", "Motijheel"),
            ("Mouchak", "Mouchak"),
            ("Moulvibazar Sadar", "Moulvibazar Sadar"),
            ("Mugdapara", "Mugdapara"),
            ("Muktagachha", "Muktagachha"),
            ("Muladi", "Muladi"),
            ("Munshiganj Sadar", "Munshiganj Sadar"),
            ("Mymensingh Sadar", "Mymensingh Sadar"),
            ("Nababganj", "Nababganj"),
            ("Nabiganj", "Nabiganj"),
            ("Nabinagar", "Nabinagar"),
            ("Nachol", "Nachol"),
            ("Nadda", "Nadda"),
            ("Nagarkanda", "Nagarkanda"),
            ("Nagarpur", "Nagarpur"),
            ("Nageshwar", "Nageshwar"),
            ("Naikhong", "Naikhong"),
            ("Nakipur", "Nakipur"),
            ("Nakla", "Nakla"),
            ("Nalchhiti", "Nalchhiti"),
            ("Naldanga", "Naldanga"),
            ("Nalitabari", "Nalitabari"),
            ("Nandail", "Nandail"),
            ("Nandigram", "Nandigram"),
            ("Naniachhar", "Naniachhar"),
            ("Naogaon Sadar", "Naogaon Sadar"),
            ("Narail Sadar", "Narail Sadar"),
            ("Narayanganj Sadar", "Narayanganj Sadar"),
            ("Naria", "Naria"),
            ("Narinda", "Narinda"),
            ("Narshingdi Sadar", "Narshingdi Sadar"),
            ("Nasirnagar", "Nasirnagar"),
            ("Natore Sadar", "Natore Sadar"),
            ("Nawabganj", "Nawabganj"),
            ("Nawabpur", "Nawabpur"),
            ("Naya Bazar", "Naya Bazar"),
            ("Nazirpur", "Nazirpur"),
            ("New Market", "New Market"),
            ("Niamatpur", "Niamatpur"),
            ("Niketon", "Niketon"),
            ("Nikunja-1", "Nikunja-1"),
            ("Nikunja-2", "Nikunja-2"),
            ("Nilkhet", "Nilkhet"),
            ("Nilphamari Sadar", "Nilphamari Sadar"),
            ("Nitpur", "Nitpur"),
            ("Noakhali Sadar", "Noakhali Sadar"),
            ("Noapara", "Noapara"),
            ("Noya Paltan", "Noya Paltan"),
            ("Pabna Sadar", "Pabna Sadar"),
            ("Paikgachha", "Paikgachha"),
            ("Palash", "Palash"),
            ("Palashbari", "Palashbari"),
            ("Palashy", "Palashy"),
            ("Panchagra Sadar", "Panchagra Sadar"),
            ("Panchbibi", "Panchbibi"),
            ("Panchhari", "Panchhari"),
            ("Pangsha", "Pangsha"),
            ("Panthapath", "Panthapath"),
            ("Paribag", "Paribag"),
            ("Pashurampur", "Pashurampur"),
            ("Patgram", "Patgram"),
            ("Patharghata", "Patharghata"),
            ("Patia", "Patia"),
            ("Patnitala", "Patnitala"),
            ("Patuakhali Sadar", "Patuakhali Sadar"),
            ("Phulchhari", "Phulchhari"),
            ("Phulpur", "Phulpur"),
            ("Phultala", "Phultala"),
            ("Pirgachha", "Pirgachha"),
            ("Pirganj", "Pirganj"),
            ("Pirojpur Sadar", "Pirojpur Sadar"),
            ("Poschim Kafrul", "Poschim Kafrul"),
            ("Posta", "Posta"),
            ("Postagola", "Postagola"),
            ("Prasadpur", "Prasadpur"),
            ("Purana Paltan", "Purana Paltan"),
            ("Purbachal", "Purbachal"),
            ("Putia", "Putia"),
            ("Rafayetpur", "Rafayetpur"),
            ("Rajapur", "Rajapur"),
            ("Rajarbag", "Rajarbag"),
            ("Rajarhat", "Rajarhat"),
            ("Rajbari Sadar", "Rajbari Sadar"),
            ("Rajibpur", "Rajibpur"),
            ("Rajnagar", "Rajnagar"),
            ("Rajoir", "Rajoir"),
            ("Rajshahi Sadar", "Rajshahi Sadar"),
            ("Rajsthali", "Rajsthali"),
            ("Ramganj", "Ramganj"),
            ("Ramghar Head Office", "Ramghar Head Office"),
            ("Ramna", "Ramna"),
            ("Rampal", "Rampal"),
            ("Rampura", "Rampura"),
            ("Ramu", "Ramu"),
            ("Rangpur Sadar", "Rangpur Sadar"),
            ("Rangunia", "Rangunia"),
            ("Rani Sankail", "Rani Sankail"),
            ("Rayenda", "Rayenda"),
            ("Raypur", "Raypur"),
            ("Raypura", "Raypura"),
            ("Roanchhari", "Roanchhari"),
            ("Rohanpur", "Rohanpur"),
            ("Roumari", "Roumari"),
            ("Rouzan", "Rouzan"),
            ("Ruma", "Ruma"),
            ("Rupganj", "Rupganj"),
            ("Rupnagar", "Rupnagar"),
            ("Saadullapur", "Saadullapur"),
            ("Sachna", "Sachna"),
            ("Sadarghat", "Sadarghat"),
            ("Sadarpur", "Sadarpur"),
            ("Sahebganj", "Sahebganj"),
            ("Sajiara", "Sajiara"),
            ("Sandwip", "Sandwip"),
            ("Sangsad Bhaban", "Sangsad Bhaban"),
            ("Sarail", "Sarail"),
            ("Sariakandi", "Sariakandi"),
            ("Sarsa", "Sarsa"),
            ("Sathia", "Sathia"),
            ("Satkania", "Satkania"),
            ("Satkhira Sadar", "Satkhira Sadar"),
            ("Saturia", "Saturia"),
            ("Savar", "Savar"),
            ("Sayedabad", "Sayedabad"),
            ("Science Laboratory", "Science Laboratory"),
            ("Segun Bagicha", "Segun Bagicha"),
            ("Senbag", "Senbag"),
            ("Shahbagh", "Shahbagh"),
            ("Shahjadpur", "Shahjadpur"),
            ("Shahjahanpur", "Shahjahanpur"),
            ("Shahrasti", "Shahrasti"),
            ("Shailakupa", "Shailakupa"),
            ("Shakhipur", "Shakhipur"),
            ("Shantibag", "Shantibag"),
            ("Shantinagar", "Shantinagar"),
            ("Shariatpur Sadar", "Shariatpur Sadar"),
            ("Shaymoli", "Shaymoli"),
            ("Sher e Bangla Nagar", "Sher e Bangla Nagar"),
            ("Sherpur", "Sherpur"),
            ("Sherpur Shadar", "Sherpur Shadar"),
            ("Shewrapara", "Shewrapara"),
            ("Shibganj", "Shibganj"),
            ("Shibganj U.P.O", "Shibganj U.P.O"),
            ("Shibloya", "Shibloya"),
            ("Shibpur", "Shibpur"),
            ("Shorishabari", "Shorishabari"),
            ("Shriangan", "Shriangan"),
            ("Shribardi", "Shribardi"),
            ("Shripur", "Shripur"),
            ("Shukrabad", "Shukrabad"),
            ("Shyampur", "Shyampur"),
            ("Siddeswary", "Siddeswary"),
            ("Siddirganj", "Siddirganj"),
            ("Singari", "Singari"),
            ("Singra", "Singra"),
            ("Sirajdikhan", "Sirajdikhan"),
            ("Sirajganj Sadar", "Sirajganj Sadar"),
            ("Sitakunda", "Sitakunda"),
            ("Sobhanbag", "Sobhanbag"),
            ("Sonagazi", "Sonagazi"),
            ("Sonargaon", "Sonargaon"),
            ("Sreepur", "Sreepur"),
            ("Srimangal", "Srimangal"),
            ("Srinagar", "Srinagar"),
            ("Sripur", "Sripur"),
            ("Subidkhali", "Subidkhali"),
            ("Sujanagar", "Sujanagar"),
            ("Sunamganj Sadar", "Sunamganj Sadar"),
            ("Sundarganj", "Sundarganj"),
            ("Susung Durgapur", "Susung Durgapur"),
            ("Sutrapur", "Sutrapur"),
            ("Swarupkathi", "Swarupkathi"),
            ("Syedpur", "Syedpur"),
            ("Sylhet Sadar", "Sylhet Sadar"),
            ("Taala", "Taala"),
            ("Tahirpur", "Tahirpur"),
            ("Tangail Sadar", "Tangail Sadar"),
            ("Tangibari", "Tangibari"),
            ("Tanor", "Tanor"),
            ("Taraganj", "Taraganj"),
            ("Tarash", "Tarash"),
            ("Technical", "Technical"),
            ("Tejgaon", "Tejgaon"),
            ("Tejkunipara", "Tejkunipara"),
            ("Tejturi Bazar", "Tejturi Bazar"),
            ("Teknaf", "Teknaf"),
            ("Terakhada", "Terakhada"),
            ("Tetulia", "Tetulia"),
            ("Thakurgaon Sadar", "Thakurgaon Sadar"),
            ("Thanchi", "Thanchi"),
            ("Tongi", "Tongi"),
            ("Trishal", "Trishal"),
            ("Tungipara", "Tungipara"),
            ("Turag", "Turag"),
            ("Tushbhandar", "Tushbhandar"),
            ("Ukhia", "Ukhia"),
            ("Ulipur", "Ulipur"),
            ("Ullapara", "Ullapara"),
            ("Uttar Khan", "Uttar Khan"),
            ("Uttara Sector-1", "Uttara Sector-1"),
            ("Uttara Sector-10", "Uttara Sector-10"),
            ("Uttara Sector-11", "Uttara Sector-11"),
            ("Uttara Sector-12", "Uttara Sector-12"),
            ("Uttara Sector-13", "Uttara Sector-13"),
            ("Uttara Sector-14", "Uttara Sector-14"),
            ("Uttara Sector-2", "Uttara Sector-2"),
            ("Uttara Sector-3", "Uttara Sector-3"),
            ("Uttara Sector-4", "Uttara Sector-4"),
            ("Uttara Sector-5", "Uttara Sector-5"),
            ("Uttara Sector-6", "Uttara Sector-6"),
            ("Uttara Sector-7", "Uttara Sector-7"),
            ("Uttara Sector-8", "Uttara Sector-8"),
            ("Uttara Sector-9", "Uttara Sector-9"),
            ("Uzirpur", "Uzirpur"),
            ("Vatara", "Vatara"),
            ("Wari", "Wari"),
        ], 'Pickup Area', copy=False, help="Reason", select=True, required=True),

        # 'npr': fields.text('No Pick UP Reason'),
        'npr': fields.selection([
            ("Vendor was unreachable", "Vendor was unreachable"),
            ("Vendor refused to provide", "Vendor refused to provide"),
            ("Unable to reach", "Unable to reach"),
            ("Vendors place was closed", "Vendors place was closed"),
            ("Cash problem", "Cash problem"),
            ("Vendor rescheduled", "Vendor rescheduled"),
            ("Wrong address given", "Wrong address given"),
            ("Unable to reach on Time", "Unable to reach on Time"),
            ("Quality issue", "Quality issue"),
            ("Due to traffic issue", "Due to traffic issue")

        ], 'No Pick UP Reason', copy=False, help="Reason", select=True),

        'remark': fields.text('Remark'),
        'requested_by': fields.many2one('res.users', 'Requested By'),
        'received_by': fields.many2one('res.users', 'Received By'),

        'cancel_roll_back_pending_time': fields.datetime('Cancel Roll Back Pending Time'),

        'confirm_by': fields.many2one('res.users', 'Confirm By'),
        'confirm_time': fields.datetime('Confirmation Time'),

        'reattempt_by': fields.many2one('res.users', 'Reattempt By'),
        'reattempt_time': fields.datetime('Reattempt Time'),

        'cancel_by': fields.many2one('res.users', 'Cancel By'),
        'cancel_time': fields.datetime('Cancelled Time'),

        'create_manifest_time': fields.datetime('Create Manifest Time'),

        'request_date': fields.datetime('Request Date', readonly=True, select=False),
        'date_pickup': fields.date('Pick Up Date', required=True),
        'reattempt_date_pickup': fields.date('Reattempt Pickup Date'),
        'state': fields.selection([
            ('pending', 'Pending'),
            ('confirmed', 'Confirmed'),
            ('create_manifest', 'Create Manifest'),
            ('unsuccessful', 'Unsuccessful'),
            ('done', 'Done'),
            ('cancel', 'Cancel'),

        ], 'Status', readonly=True, copy=False, help="Gives the status of the Pick up request", select=True),
        'request_type': fields.selection([
            ('full_received', 'Full Request'),
            ('partial_received', 'Partial Request'),

        ], 'Request Type', readonly=True, copy=False, help="Gives the status of the Pick up request", select=True),
        'pickup_type': fields.selection([
            ('full_received', 'Full Received'),
            ('partial_received', 'Partial Received'),
            ('not_received', 'Not Received'),

        ], 'Pickup Type', readonly=True, copy=False, help="Gives the status of the Pick up request", select=True),
        'pickup_request_line': fields.one2many('pickup.request.line', 'pickup_request_line_id', 'Pickup Request Line',
                                               required=True),

        'pickup_request_cancel_date': fields.datetime('Cancel Pickup Request Date'),
        'pickup_request_cancel_by': fields.many2one('res.users', 'Cancel Pickup Request By'),
        'pickup_request_cancel_reason': fields.text('Cancel Reason'),
        'pickup_not_receive_date': fields.datetime('No Pickup receive Date'),
        'pickup_not_receive_by': fields.many2one('res.users', 'No Pickup receive by'),
        'reattempted': fields.boolean('Reattempted'),
        'reattempted_cancel': fields.boolean('Reattempted Cancel'),
    }

    _defaults = {
        'user_id': lambda obj, cr, uid, context: uid,
        'requested_by': lambda obj, cr, uid, context: uid,
        'date_pickup': lambda self, cr, uid, ctx: self.compute_next_day_date(fields.date.context_today(self, cr, uid, context=ctx)),
        'reattempt_date_pickup': lambda self, cr, uid, ctx: self.compute_next_day_date(fields.date.context_today(self, cr, uid, context=ctx)),
        'state': 'pending',
        'request_date': lambda self, cr, uid, context={}: context.get('request_date',
                                                                      time.strftime("%Y-%m-%d %H:%M:%S")),

    }


    def _check_pickup_request_reattempt(self,cr, uid, single_id, context=None):
        pickup_req_obj = self.pool.get('pickup.request').browse(cr, uid, single_id, context=context)
        reattempt = pickup_req_obj.reattempt_number if pickup_req_obj.reattempt_number else 0.00
        reattempt_number = reattempt +1

        return reattempt_number


    def re_attempt_pickup_request(self, cr, uid, ids, context=None):
        pickup_request_line = []
        data={}

        for single_id in ids:
            reattempt_number = self._check_pickup_request_reattempt(cr, uid, single_id, context=context)
            pickup_req = self.pool.get('pickup.request')
            pickup_obj=pickup_req.browse(cr, uid, single_id, context=context)

            if pickup_obj.pickup_type!='not_received':
                for pickup_line in pickup_obj.pickup_request_line:
                    if pickup_line.remaining_quantity > 0.0:
                        pickup_request_line.append([0, False, {
                            'po_id': pickup_line.po_id.id,
                            'product': pickup_line.product,
                            'product_id': pickup_line.product_id.id,
                            'ordered_quantity': pickup_line.ordered_quantity,
                            'updated_ordered_quantity': pickup_line.remaining_quantity,
                            'request_quantity': pickup_line.remaining_quantity,
                            'product_uom': pickup_line.product_uom

                        }])
                data['pickup_request_line'] = pickup_request_line
                data['po_number'] = pickup_obj.po_number
                data['po'] = pickup_obj.po_number
                data['po_id'] = pickup_obj.po_id
                data['warehouse_id'] = pickup_obj.warehouse_id.id
                data['warehouse'] = pickup_obj.warehouse_id.id
                data['po_warehouse_id'] = pickup_obj.po_warehouse_id.id
                data['po_warehouse'] = pickup_obj.po_warehouse_id.id
                data['partner_id'] = pickup_obj.partner_id.id
                data['partner'] = pickup_obj.partner_id.id
                data['partner_address'] = pickup_obj.partner_address
                data['area'] = pickup_obj.area
                data['npr'] = pickup_obj.npr
                data['remark'] = pickup_obj.remark
                data['date_pickup'] = pickup_obj.reattempt_date_pickup
                data['pickup_type'] = pickup_obj.pickup_type
                data['reattempted'] = True
                data['reattempt_number'] = reattempt_number
                data['partial_pickup_ref_id'] = pickup_obj.id
                data['state'] = 'confirmed'

                save_the_data = pickup_req.create(cr, uid, data, context=context)
                state = 'done'
            else:
                state= 'confirmed'
                pickup_req.write(cr, uid, [single_id], {'date_pickup': pickup_obj.reattempt_date_pickup}, context=context)

            confirm_update_query = "UPDATE pickup_request SET reattempt_time='{0}', reattempt_by='{1}',reattempt_number={2},state = '{3}'," \
                                   "reattempted=TRUE WHERE id={4}".format(
                str(fields.datetime.now()), uid, reattempt_number, state, single_id)
            cr.execute(confirm_update_query)
            cr.commit()

            confirm_update_query_line = "UPDATE pickup_request_line SET reattempt_time='{0}', reattempt_by='{1}',state = '{2}' WHERE pickup_request_line_id={3}".format(
                str(fields.datetime.now()), uid, state, single_id)
            cr.execute(confirm_update_query_line)
            cr.commit()

            # email
            if reattempt_number > 2:
                self.pool.get('pickup.request').send_email(cr, 1, 'Thrice Reattempt Pickup Notification', int(single_id),
                                                       context=None)
            else:
                self.pool.get('pickup.request').send_email(cr, 1, 'Reattempt Pickup Notification', int(single_id),
                                                       context=None)
            # -------------
        return True


    def confirm_wms_inbound_pickup_request(self, cr, uid, ids, context=None):
        pickup_request_env = self.pool.get('pickup.request')
        pickup_obj = pickup_request_env.browse(cr, uid, ids, context=context)

        for single_id in ids:
            confirm_update_query = "UPDATE pickup_request SET state ='confirmed', confirm_time='{0}', confirm_by='{1}' WHERE id={2}".format(
                str(fields.datetime.now()), uid, single_id)
            cr.execute(confirm_update_query)
            cr.commit()

            confirm_update_query_line = "UPDATE pickup_request_line SET state='confirmed', confirm_time='{0}', confirm_by='{1}' WHERE pickup_request_line_id={2}".format(
                str(fields.datetime.now()), uid, single_id)
            cr.execute(confirm_update_query_line)
            cr.commit()

        self.pool.get('inb.end.to.end').pickup_request_confirm_update(cr, uid, str(pickup_obj.po_number), pickup_obj.po_id,
                                                           str(fields.datetime.now()), context=context)

        return True

    def cancel_wms_inbound_pickup_request(self, cr, uid, ids, context=None):

        request_env = self.pool.get('pickup.request')

        for single_id in ids:
            pickup_req_obj = request_env.browse(cr, uid, single_id, context=context)
            for pickup_req_line in pickup_req_obj.pickup_request_line:
                cancel_quantity = pickup_req_line.request_quantity

                cancel_update_query_line = "UPDATE pickup_request_line SET state='cancel', cancel_time='{0}'," \
                                           " cancel_by='{1}',cancel_quantity={2}, request_quantity={3} WHERE pickup_request_line_id={4} and id={5} ".format(
                    str(fields.datetime.now()), uid, cancel_quantity,pickup_req_line.request_quantity, single_id,pickup_req_line.id)
                cr.execute(cancel_update_query_line)
                cr.commit()

            cancel_update_query = "UPDATE pickup_request SET state ='cancel', cancel_time='{0}', cancel_by='{1}' WHERE id={2}".format(
                str(fields.datetime.now()), uid, single_id)
            cr.execute(cancel_update_query)
            cr.commit()

        return True


    def _check_if_already_pickup_request(self, self_env, po_number):

        # need to check on the po already Pickup Reqed
        req_type_list=[]
        pickup_req_type = 'new'

        pickup_req_obj = self_env.search([('po_number', '=', po_number)])

        for pickup in pickup_req_obj:
            if str(pickup.request_type) == 'full_received' and pickup.state!='cancel':
                if pickup.reattempted_cancel==True:
                    pickup_req_type = 'partial_received'
                else:
                    pickup_req_type = 'full_received'

                req_type_list.append(pickup_req_type)

            elif str(pickup.request_type) == 'full_received' and pickup.state=='cancel' :
                if pickup.reattempted_cancel==False:
                    pickup_req_type = 'partial_received'
                else:
                    pickup_req_type = 'full_received'
                req_type_list.append(pickup_req_type)

            elif str(pickup.request_type) == 'partial_received':
                pickup_req_type = 'partial_received'
                req_type_list.append(pickup_req_type)
            else:
                pickup_req_type = 'new'
                req_type_list.append(pickup_req_type)

        req_type = list(dict.fromkeys(req_type_list))

        if req_type:
            if 'partial_received' in req_type:
                pickup_req_type = 'partial_received'

            elif 'new' in req_type:
                pickup_req_type = 'new'

            else:
                pickup_req_type = 'full_received'

        return pickup_req_type

    def _check_if_pickup_received(self, self_env, po_number):

        pickup_type_state = 'new'

        pickup_obj = self_env.search([('po_number', '=', po_number)])

        for pickup in pickup_obj:
            if str(pickup.pickup_type) == 'full_received' and pickup.state!='cancel':
                pickup_type_state = 'full_received'
                break
            elif str(pickup.pickup_type) == 'partial_received':
                pickup_type_state = 'partial_received'
                break
            elif str(pickup.pickup_type) == 'not_received':
                pickup_type_state = 'not_received'
            else:
                pickup_type_state = 'new'

        return pickup_type_state

    def _check_full_or_partial_received(self, record, po_number):
        full_or_partial_rec = False

        for line_item in record.pickup_request_line:

            line_received_qty = self._check_pickup_request_line_received_quantity(po_number, line_item.product_id.id)

            if line_item.ordered_quantity != line_received_qty:
                full_or_partial_rec = False
                break
            else:
                full_or_partial_rec = True

        return full_or_partial_rec

    def _process_partial_received_order(self, self_env, po_number):

        partial_received_product_id_list = list()
        partial_rec_prod_list = list()

        pickup_req_obj = self_env.search([('po_number', '=', po_number)])

        for pickup in pickup_req_obj:
            for product in pickup.pickup_request_line:
                # if (product.request_type == 'partially_received' or product.pickup_type == 'partial_received'
                #                     or product.pickup_type == 'not_received') and product.product_id not in partial_received_product_id_list:

                if product.request_type == 'partially_received'and product.product_id.id not in partial_received_product_id_list :
                    partial_received_product_id_list.append(product.product_id.id)

                elif (product.request_type == 'fully_received' and product.state=='cancel')\
                        and product.product_id.id not in partial_received_product_id_list:
                    partial_received_product_id_list.append(product.product_id.id)

                elif (product.request_type == 'fully_received' and product.reattempted_cancel==True)\
                        and product.product_id.id not in partial_received_product_id_list:
                    partial_received_product_id_list.append(product.product_id.id)


        for pr_product_id in partial_received_product_id_list:
            pr_product = self._process_partial_received_product(self_env, po_number, pr_product_id)
            if len(pr_product.keys()) > 0:
                partial_rec_prod_list.append(pr_product)

        # return list of dictionary
        return partial_rec_prod_list

    def _process_partial_received_product(self, self_env, po_number, product_id):

        pickup_req_obj = self_env.search([('po_number', '=', po_number)])
        pickup_request_line_env = self.env['pickup.request.line']
        product_requested_quantity = self._check_pickup_request_line_received_quantity(po_number, product_id)
        product_name = ''
        ordered_quantity = 0

        for pickup in pickup_req_obj:
            pickup_request_line_obj = pickup_request_line_env.search(
                [('pickup_request_line_id', '=', pickup.id), ('product_id', '=', product_id)])
            for product in pickup_request_line_obj:
                product_name = product.product
                product_id = product_id
                ordered_quantity = product.ordered_quantity

        if ordered_quantity > product_requested_quantity :
            product_dict = {
                'product': product_name,
                'product_id': product_id,
                'ordered_quantity': ordered_quantity,
                'request_quantity': ordered_quantity - product_requested_quantity,
                'updated_ordered_quantity': ordered_quantity - product_requested_quantity,
                'remark': ''
            }
            return product_dict
        else:
            return {}

    def send_email(self, cr, uid,template_name, ids, context=None):

        email_template_obj = self.pool.get('email.template')

        template_ids = email_template_obj.search(cr, uid, [('name', '=',template_name )], context=context)

        if template_ids:
            email_template_obj.send_mail(cr, uid, template_ids[0], ids, force_send=True,context=context)
            # values = email_template_obj.generate_email(cr, uid, template_ids[0], ids, context=context)
            # uid_email = self.pool.get('res.users').browse(cr, uid, uid, context=context).email if self.pool.get('res.users').browse(cr, uid, uid, context=context) else ''
            # values['email_to'] = values['email_to'] if values['email_to'] else uid_email
            # values['res_id'] = False
            # mail_mail_obj = self.pool.get('mail.mail')
            # msg_id = mail_mail_obj.create(cr, uid, values, context=context)
            # if msg_id:
            #     mail_mail_obj.send(cr, uid, [msg_id], context=context)
        return True


    @api.onchange('scan')
    def po_pickup_req_barcode_onchange(self):

        try:
            po_number = str(self.scan)

            if po_number != 'False':

                pickup_req_state = self._check_if_already_pickup_request(self, po_number)
                # pickup_type_state = self._check_if_pickup_received(self, po_number)

                po_env = self.env['purchase.order']
                po_obj = po_env.search([('name', '=', po_number), ('state', '=', 'approved'),('vendor_pickup', '=', 'yes')])

                self.po_number = po_number
                self.po = po_number
                self.po_warehouse_id = po_obj.picking_type_id.warehouse_id.id
                self.po_warehouse = self.po_warehouse_id
                self.warehouse_id = self.po_warehouse_id
                self.warehouse = self.po_warehouse
                self.partner_id = po_obj.partner_id.id
                self.partner = self.partner_id

                # vendor address
                self.partner_address = ''
                if po_obj.partner_id.street:
                    self.partner_address = self.partner_address + po_obj.partner_id.street + ", "
                if po_obj.partner_id.street2:
                    self.partner_address = self.partner_address + po_obj.partner_id.street2 + ", "
                if po_obj.partner_id.city:
                    self.partner_address = self.partner_address + po_obj.partner_id.city + ", "
                if po_obj.partner_id.state_id:
                    self.partner_address = self.partner_address + po_obj.partner_id.state_id.name + ", "
                if po_obj.partner_id.zip:
                    self.partner_address = self.partner_address + po_obj.partner_id.zip + ", "
                if po_obj.partner_id.country_id:
                    self.partner_address = self.partner_address + po_obj.partner_id.country_id.name
                # end

                # self.area = po_obj.partner_id.state_id.name

                # if pickup_req_state == 'full_received' and pickup_type_state == 'full_received':
                #     # raise exception
                #     raise osv.except_osv(_('Already Pickup request done!'),
                #                          _('Pickup request already done for this PO!!!'))
                #
                # elif (pickup_req_state == 'partial_received' and (pickup_type_state == 'new' or pickup_type_state == 'partial_received' or pickup_type_state == 'full_received'
                #                     or pickup_type_state == 'not_received')) \
                #         or (pickup_req_state == 'full_received' and (pickup_type_state == 'new' or pickup_type_state == 'partial_received' or pickup_type_state == 'full_received'
                #                     or pickup_type_state == 'not_received')) :

                if pickup_req_state == 'full_received':
                    # raise exception
                    raise osv.except_osv(_('Already Pickup request done!'),
                                         _('Pickup request already done for this PO!!!'))

                elif (pickup_req_state == 'partial_received' ):

                    self.pickup_request_line = self._process_partial_received_order(self, po_number)
                    self.scan = ""

                else:

                    po_line_env = self.env['purchase.order.line']
                    po_line_obj = po_line_env.search([('order_id', '=', po_obj.id)])

                    po_line_list = list()

                    for po_line in po_line_obj:
                        po_line_list.append({
                            'product': str(po_line.name),
                            'product_id': int(po_line.product_id.id),
                            'product_description': str(po_line.product_id.name),
                            'product_uom': str(po_line.product_id.prod_uom),
                            'ordered_quantity': float(po_line.product_qty),
                            'request_quantity': float(po_line.product_qty),
                            'updated_ordered_quantity': float(po_line.product_qty),
                            'remark': ''
                        })

                    self.pickup_request_line = po_line_list
                    self.scan = ""

        except:
            self.scan = ""

        return "xXxXxXxXxX"


    # @api.onchange('area')
    # def pickup_warehouse_onchange(self):
    #     try:
    #         state_name = (self.area)
    #         warehouse_dict = {}
    #
    #
    #
    #         wh_area = self.pool.get('warehouse.area')
    #         wh_nd_area_search = wh_area.search(self._cr, self._uid, [('code', '=', 'NODDA')], context=self._context)
    #
    #         wh_nd_area_browse = wh_area.browse(self._cr, self._uid, wh_nd_area_search, context=self._context)
    #
    #         nd_areas = str(wh_nd_area_browse.areas)
    #
    #         wh_ut_area_search = wh_area.search(self._cr, self._uid, [('code', '=', 'UttWH')], context=self._context)
    #         wh_ut_area_browse = wh_area.browse(self._cr, self._uid, wh_ut_area_search, context=self._context)
    #
    #         ut_areas = str(wh_ut_area_browse.areas)
    #
    #
    #         nodda_list = nd_areas.split(',')
    #         nodda_list = [a.lstrip() for a in nodda_list]
    #         uttara_list = ut_areas.split(',')
    #         uttara_list = [a.lstrip() for a in uttara_list]
    #
    #
    #
    #
    #         """
    #         uttara_list = ["Abdullahpur", "Adabor", "Aditmari", "AEPZ (Adamjee Export Processing Zone)", "Agailzhara", "Agargaon", "Ahsanganj", "Airport", "Akhaura", "Akkelpur", "Alaipur", "Alamdanga", "Alamdighi",
    #                        "Alfadanga", "Alikadam", "Amtali", "Anawara", "Araihazar", "Arpara", "Ashashuni", "Ashulia",
    #                        "Atpara", "Azampur", "Azimpur", "Azmireeganj", "Babuganj", "Badalgachhi", "Bagerhat Sadar",
    #                        "Bagha", "Bagharpara", "Bahubal", "Baidder Bazar", "Baiddya Jam Toil", "Bajitpur",
    #                        "Bakshigonj", "Balaganj", "Baliadangi", "Baliakandi", "Bamna", "Banaripara", "Banchharampur",
    #                        "Bandar", "Bandarban Sadar", "Bangabhaban", "Bangla Hili", "Baniachang", "Banwarinagar",
    #                        "Barajalia", "Barakal", "Baralekha", "Barguna Sadar", "Barhamganj", "Barhatta",
    #                        "Barishal Sadar", "Barura", "Basail", "Basurhat", "Batiaghat", "Bauphal", "Begumganj",
    #                        "Belabo", "Belkuchi", "Bera", "Betagi", "Bhabaniganj", "Bhairob", "Bhaluka", "Bhandaria",
    #                        "Bhanga", "Bhangura", "Bhedorganj", "Bheramara", "Bhola Sadar", "Bholahat", "Bhuapur",
    #                        "Bhurungamari", "Bianibazar", "Bilaichhari", "Biral", "Birampur", "Birganj", "Bishamsarpur",
    #                        "Bishwanath", "Boalkhali", "Boalmari", "Boda", "Bogra Sadar", "Bonarpara", "Borhanuddin Upo",
    #                        "Brahamanbaria Sadar", "Brahmanpara", "Burichang", "Cantonment (Dhaka)", "Chalna Ankorage",
    #                        "Chalna Bazar", "Chandina", "Chandpur Sadar", "Chapinawabganj Sadar", "Char Alexgander",
    #                        "Charbhadrasan", "Charfashion", "Charghat", "Chatkhil", "Chatmohar", "Chaugachha",
    #                        "Chhagalnaia", "Chhatak", "Chilmari", "Chiringga", "Chitalmari", "Chittagong Sadar",
    #                        "Chotto Dab", "Chouddagram", "Chrirbandar", "Chuadanga Sadar", "Chunarughat",
    #                        "Comilla Sadar", "Coxs Bazar Sadar", "Dabiganj", "Dagonbhuia", "Dakshin Kafrul",
    #                        "Dakshinkhan", "Damudhya", "Damurhuda", "Darus-Salam", "Dashmina", "Daudkandi", "Davidhar",
    #                        "Debbhata", "Debottar", "Delduar", "Demra", "DEPZ (Dhaka Export Processing Zone)",
    #                        "Dewangonj", "Dhamrai", "Dhamuirhat", "Dhangora", "Dhanmondi", "Dharmapasha", "Dhaur",
    #                        "Dhirai Chandpur", "Dhobaura", "Dhunat", "Digalia", "Diginala", "Dimla", "Dinajpur Sadar",
    #                        "Dip Nagar", "Dokkhin Khan", "Domar", "Doulatganj", "Doulatkhan", "Doulatpur", "Duara Bazar",
    #                        "Duaripara", "Dupchachia", "Durgapur", "East Joara", "Elephant Road", "Estern Housing",
    #                        "Fakirhat", "Faridganj", "Faridpur Sadar", "Fatikchhari", "Fatullah", "Fenchuganj",
    #                        "Feni Sadar", "Firmgate", "Fulbaria", "Gabtoli", "Gaforgaon", "Gaibandha Sadar", "Gajaria",
    #                        "Galachipa", "Gangachara", "Gangni", "Gazipur Sadar", "Ghatail", "Gheor", "Ghungiar",
    #                        "Goainhat", "Gobindaganj", "Godagari", "Gopalganj", "Gopalganj Sadar", "Gopalpur",
    #                        "Gopalpur Upo", "Gorakghat", "Gosairhat", "Gouranadi", "Gouripur", "Green Road", "Hajiganj",
    #                        "Hajirhat", "Haluaghat", "Harinakundu", "Harua", "Hatgurudaspur", "Hathazari", "Hatibandha",
    #                        "Hatiya", "Hatshoshiganj", "Hayemchar", "Hazaribagh", "Hobiganj Sadar", "Homna", "Hossenpur",
    #                        "Ishwardi", "Islambagh", "Islampur", "Isshwargonj", "Itna", "Jagnnathpur", "Jaintapur",
    #                        "Jajira", "Jakiganj", "Jaldhaka", "Jaldi", "Jamalpur", "Janipur", "Jarachhari",
    #                        "Jessore Sadar", "Jhalokathi Sadar", "Jhikargachha", "Jhinaigati", "Jibanpur", "Jigatala",
    #                        "Jinaidaha Sadar", "Joypurhat Sadar", "Kachua", "Kachua Upo", "Kafrul", "Kahalu",
    #                        "Kalabagan", "Kalai", "Kalampati", "Kalaroa", "Kalauk", "Kalia", "Kaliakaar", "Kaliganj",
    #                        "Kaliganj Upo", "Kalihati", "Kalkini", "Kallyanpur", "Kalmakanda", "Kalshi", "Kamalganj", "Kamrangirchar", "Kanaighat", "Kapashia", "Kaptai", "Karimganj", "Kasba", "Kashiani", "Kashkaolia",
    #                        "Kathalbagan", "Kathalia", "Katiadi", "Kaukhali", "Kaunia", "Kazipara", "Kazipur", "Kendua",
    #                        "Keshabpur", "Khagrachari Sadar", "Khaliajuri", "Khansama", "Khepupara", "Khetlal",
    #                        "Khod Mohanpur", "Khulna Sadar", "Kishoreganj Sadar", "Kishoriganj", "Kompanyganj",
    #                        "Kotalipara", "Kotchandpur", "Kulaura", "Kuliarchar", "Kumarkhali", "Kurigram Sadar",
    #                        "Kustia Sadar", "Kutubdia", "Lakshimpur Sadar", "Lalbagh", "Lalitganj", "Lalmatia",
    #                        "Lalmohan Upo", "Lalmonirhat Sadar", "Laxman", "Laxmichhari", "Laxmipasha", "Lechhraganj",
    #                        "Lohagara", "Lohajong", "Longachh", "Madan", "Madaripur Sadar", "Madhabpur", "Madhupur",
    #                        "Madinabad", "Madukhali", "Magura Sadar", "Mahadebpur", "Mahalchhari", "Maharajganj",
    #                        "Mahendiganj", "Maheshpur", "Maksudpur", "Malandah", "Manikchhari", "Manikganj Sadar",
    #                        "Marishya", "Mathargonj", "Mathbaria", "Matiranga", "Matlab North", "Matlab South",
    #                        "Meherpur Sadar", "Mirpur", "Mirpur", "Mirpur 1", "Mirpur 10", "Mirpur 11", "Mirpur 12",
    #                        "Mirpur 13", "Mirpur 14", "Mirpur 2", "Mirpur 6", "Mirpur 7", "Mirpur DOHS", "Mirsharai",
    #                        "Mirzapur", "Mithamoin", "Mithapukur", "Mohajan", "Mohammadpur", "Mohammadpur", "Mollahat",
    #                        "Monirampur", "Monnunagar", "Monohordi", "Morelganj", "Moulvibazar Sadar", "Muktagachha",
    #                        "Muladi", "Munshiganj Sadar", "Mymensingh Sadar", "Nababganj", "Nabiganj", "Nabinagar",
    #                        "Nachol", "Nagarkanda", "Nagarpur", "Nageshwar", "Naikhong", "Nakipur", "Nakla", "Nalchhiti",
    #                        "Naldanga", "Nalitabari", "Nandail", "Nandigram", "Naniachhar", "Naogaon Sadar",
    #                        "Narail Sadar", "Narayanganj Sadar", "Naria", "Narshingdi Sadar", "Nasirnagar",
    #                        "Natore Sadar", "Nazirpur", "New Market", "Niamatpur", "Nilphamari Sadar", "Nitpur", "Noakhali Sadar",
    #                        "Noapara", "Pabna Sadar", "Paikgachha", "Palash", "Palashbari", "Panchagra Sadar",
    #                        "Panchbibi", "Panchhari", "Pangsha", "Paribag", "Pashurampur", "Panthapath", "Patgram", "Patharghata",
    #                        "Patia", "Patnitala", "Patuakhali Sadar", "Phulchhari", "Phulpur", "Phultala", "Pirgachha",
    #                        "Pirganj", "Pirojpur Sadar", "Poschim Kafrul", "Posta", "Prasadpur", "Putia", "Rafayetpur",
    #                        "Rajapur", "Rajarhat", "Rajbari Sadar", "Rajibpur", "Rajnagar", "Rajoir", "Rajshahi Sadar",
    #                        "Rajsthali", "Ramganj", "Ramghar Head Office", "Rampal", "Ramu", "Rangpur Sadar", "Rangunia",
    #                        "Rani Sankail", "Rayenda", "Raypur", "Raypura", "Roanchhari", "Rohanpur", "Roumari",
    #                        "Rouzan", "Ruma", "Rupganj", "Rupnagar", "Saadullapur", "Sachna", "Sadarpur", "Sahebganj",
    #                        "Sajiara", "Sandwip", "Sangsad Bhaban", "Sarail", "Sariakandi", "Sarsa", "Sathia",
    #                        "Satkania", "Satkhira Sadar", "Saturia", "Savar", "Science Laboratory", "Senbag",
    #                        "Shahjadpur", "Shahrasti", "Shailakupa", "Shakhipur", "Shariatpur Sadar", "Shaymoli", "Sher e Bangla Nagar", "Sherpur", "Sherpur Shadar", "Shewrapara", "Shibganj", "Shibganj U.P.O", "Shibloya",
    #                        "Shibpur", "Shorishabari", "Shriangan", "Shribardi", "Shripur", "Shukrabad", "Shyampur",
    #                        "Siddirganj", "Singari", "Singra", "Sirajdikhan", "Sirajganj Sadar", "Sitakunda", "Sobhanbag", "Sonagazi", "Sonargaon", "Sreepur", "Srimangal", "Srinagar", "Sripur", "Subidkhali", "Sujanagar", "Sunamganj Sadar",
    #                        "Sundarganj", "Susung Durgapur", "Swarupkathi", "Syedpur", "Sylhet Sadar", "Taala",
    #                        "Tahirpur", "Tangail Sadar", "Tangibari", "Tanor", "Taraganj", "Tarash", "Technical",
    #                        "Teknaf", "Terakhada", "Tetulia", "Thakurgaon Sadar", "Thanchi", "Tongi", "Trishal",
    #                        "Tungipara", "Turag", "Tushbhandar", "Ukhia", "Ulipur", "Ullapara", "Uttar Khan",
    #                        "Uttara Sector-1", "Uttara Sector-10", "Uttara Sector-11", "Uttara Sector-12",
    #                        "Uttara Sector-13", "Uttara Sector-14", "Uttara Sector-2", "Uttara Sector-3",
    #                        "Uttara Sector-4", "Uttara Sector-5", "Uttara Sector-6", "Uttara Sector-7",
    #                        "Uttara Sector-8", "Uttara Sector-9", "Uzirpur"]
    #
    #
    #
    #
    #         nodda_list = ["Arambagh", "Babu Bazar", "Badda", "Bakshi Bazar", "Banani", "Banani Cantonment",
    #                       "Banani Dohs", "Banasree", "Bangla Bazar", "Bangla Motor", "Bangshal", "Baridhara",
    #                       "Baridhara Dohs", "Basabo", "Bashundhara R/A", "Chankharpool", "Chawk Bazar", "Dilkusha",
    #                       "Dohar", "Eskaton", "Fulbaria", "Gendaria", "Goran", "Gulistan", "Gulshan 1", "Gulshan 2",
    #                       "Hatirjheel", "Hatirpool", "Islampur", "Jatrabari", "Jurain", "Kadamtali", "Kakrail",
    #                       "Kalachadpur", "Kamalapur", "Karwan Bazar", "Keraniganj", "Khilgaon",
    #                       "Khilkhet", "Kuril", "Malibag", "Meradia", "Mitford", "Moghbazar", "Mohakhali",
    #                       "Mohakhali Dohs", "Motijheel", "Mouchak", "Mugdapara", "Nadda", "Narinda", "Nawabganj",
    #                       "Nawabpur", "Naya Bazar", "Niketon", "Nikunja-1", "Nikunja-2", "Nilkhet",
    #                       "Noya Paltan", "Palashy", "Postagola", "Purana Paltan", "Purbachal", "Rajarbag",
    #                       "Ramna", "Rampura", "Sadarghat", "Sayedabad", "Segun Bagicha", "Shahbagh", "Shahjahanpur",
    #                       "Shantibag", "Shantinagar", "Siddeswary", "Sutrapur",
    #                       "Tejgaon", "Tejkunipara", "Tejturi Bazar", "Vatara", "Wari"]
    #
    #
    #
    #         uttara_list = ["Dhanmondi", "Firmgate", "Jigatala", "Kalabagan", "Kallyanpur", "Kathalbagan",
    #                       "Khilkhet", "Lalmatia", "Mirpur", "Mirpur 1", "Mirpur 10", "Mirpur 11", "Mirpur 12",
    #                       "Mirpur 14", "Mirpur 2", "Mirpur 6", "Mirpur 7", "Mohammadpur", "New Market","Nikunja-1", "Nikunja-2",
    #                       "Paribag", "Shaymoli", "Shukrabad", "Sobhanbag","Uttara Sector-1","Uttara Sector-2","Turag",
    #                       "Dhamrai", "Dohar", "Uttara Sector-3", "Uttara Sector-4","Uttara Sector-5","Uttara Sector-6",
    #                       "Uttara Sector-7", "Uttara Sector-8", "Uttara Sector-9", "Uttara Sector-10","Uttara Sector-11","Uttara Sector-12",
    #                       "Uttara Sector-12", "Uttara Sector-14", "Mirpur 13", "Mirpur DOHS","Dokkhin Khan","Uttar Khan","Azampur",
    #                       "Abdullahpur", "Tongi","Savar"]
    #
    #         nodda_list = ["Badda", "Banani", "Banani Cantonment", "Banani Dohs", "Baridhara", "Baridhara Dohs",
    #                       "Bashundhara R/A", "Eskaton", "Firmgate", "Gulshan 1", "Gulshan 2", "Karwan Bazar", "Khilgaon",
    #                       "Moghbazar", "Mohakhali", "Mohakhali Dohs", "Niketon","Rampura", "Tejgaon", "Nadda", "Kuril"]
    #
    #         motijheel_list = ["Basabo","Bangabhaban", "Dilkusha", "Gendaria", "Gulistan", "Eskaton", "Elephant Road", "Green Road", "Hazaribagh",
    #                           "Kakrail","Motijheel", "Noya Paltan", "Panthapath", "Posta", "Purana Paltan", "Shantinagar",
    #                           "Wari", "Keraniganj", "Nawabganj", "Jatrabari", "Sayedabad", "Araihazar","Rajarbag",
    #                           "Shantibag", "Malibag", "Mouchak", "Shahjahanpur", "Bangshal"]
    #          """
    #
    #
    #
    #
    #         warehouse_dict[1] = uttara_list # Here key is the uttara warehouse ID
    #         warehouse_dict[2] = nodda_list  # Here key is the nodda warehouse ID
    #
    #         # warehouse_dict[4] = motijheel_list  # Here key is the Motijeel warehouse ID
    #
    #         # ----------------------------------------------
    #
    #         wh_id = None
    #         # The following code for Area wise Warhouse change Auto
    #         if state_name != False:
    #             for k, v in warehouse_dict.iteritems():
    #
    #                 for item in v:
    #
    #                     if state_name.upper() == item.upper():
    #                         warehouse_id = k
    #                         self.warehouse_id = warehouse_id
    #                         self.warehouse = warehouse_id
    #                         self.area = self.area
    #
    #                         return "xXxXxXxXxX"
    #
    #                     else:
    #                         self.warehouse_id = ''
    #
    #
    #     except:
    #         self.area = ""
    #
    #     return "xXxXxXxXxX"
    #
    #
    #     ### Ends here area wise warehouse selection





    def _check_pickup_request_line_received_quantity(self, po_number, product_id):

        request_quantity = 0
        remaining_quantity = 0
        cancel_quantity = 0

        pickup_req_env = self.env['pickup.request']
        pickup_req_obj = pickup_req_env.search([('po_number', '=', po_number)])

        pickup_request_line_env = self.env['pickup.request.line']

        for pickup in pickup_req_obj:
            pickup_request_line_obj = pickup_request_line_env.search(
                [('pickup_request_line_id', '=', pickup.id), ('product_id', '=', product_id)])
            for pickup_request_line in pickup_request_line_obj:
                request_quantity += pickup_request_line.request_quantity
                remaining_quantity += pickup_request_line.remaining_quantity if pickup_request_line.remaining_quantity and pickup_request_line.state!='unsuccessful' else 0.00
                cancel_quantity += pickup_request_line.cancel_quantity if pickup_request_line.cancel_quantity and pickup_request_line.state=='cancel' else 0.00

        quantity = request_quantity - remaining_quantity - cancel_quantity
        # quantity = request_quantity
        return quantity

    def _set_pickup_request_line_state(self, record, po_number):

        for line_item in record.pickup_request_line:
            line_received_qty = self._check_pickup_request_line_received_quantity(po_number, line_item.product_id.id)
            if line_item.ordered_quantity == line_received_qty:
                line_item.request_type = 'fully_received'
            else:
                line_item.request_type = 'partially_received'

        return record

    def _set_po_id_in_line(self, record, po_id):

        for line_item in record.pickup_request_line:
            line_item.po_id = po_id

        return record

    @api.model
    def create(self, vals):

        # generate Pickup number
        vals['warehouse_id'] = vals['warehouse']
        vals['po_warehouse_id'] = vals['po_warehouse']
        vals['partner_id'] = vals['partner']
        vals['po_number'] = vals['po']

        update_permission = list()

        if vals.has_key('pickup_request_line'):
            for single_line in vals['pickup_request_line']:
                if not not single_line[2]:
                    request_quantity = single_line[2]['request_quantity'] if single_line[2].has_key(
                        'request_quantity') else single_line[2]['updated_ordered_quantity']

                    if request_quantity > single_line[2]['updated_ordered_quantity'] and single_line[2].has_key('updated_ordered_quantity'):
                        update_permission.append(False)
                    else:
                        update_permission.append(True)

        if False in update_permission:

            raise osv.except_osv(_('Pickup Request line adjustment ERROR!'),
                                 _('Request quantity should be less then or equal to (available quantity)!!!'))
        else:
            record = super(pickup_request, self).create(vals)

        po_env = self.env['purchase.order']
        po_obj = po_env.search([('name', '=', str(record.po_number))])

        pickup_number = "PICK0" + str(record.id)
        po_id = po_obj.id

        record.name = pickup_number
        record.po_id = po_id
        record.po_id_ref = po_id
        record.request_type = 'full_received' if self._check_full_or_partial_received(record, str(
            record.po_number)) else 'partial_received'

        record = self._set_pickup_request_line_state(record, str(record.po_number))
        record = self._set_po_id_in_line(record, po_id)

        self.pool.get('inb.end.to.end').pickup_data_create(self._cr, self._uid, record.po_number, po_id, record.id, pickup_number, record.create_date, context=self._context)

        return record

    # calls at the time of update record
    @api.multi
    def write(self, vals):
        if vals.has_key('warehouse'):
            vals['warehouse_id'] = vals['warehouse']

        update_permission = list()

        if vals.has_key('pickup_request_line'):
            for single_line in vals['pickup_request_line']:
                if not not single_line[2]:
                    for req_l in self.pickup_request_line:

                        if req_l.id == single_line[1]:

                            request_quantity = single_line[2]['request_quantity'] if single_line[2].has_key(
                                'request_quantity') else req_l.request_quantity

                            if request_quantity > req_l.updated_ordered_quantity:
                                update_permission.append(False)
                            else:
                                update_permission.append(True)

        if False in update_permission:

            raise osv.except_osv(_('Pickup Request line adjustment ERROR!'),
                                 _('Request quantity should be less then or equal to (available quantity)!!!'))

        else:
            record = super(pickup_request, self).write(vals)

            return record


class pickup_request_line(osv.osv):
    _name = "pickup.request.line"
    _description = "Pickup Request Line"

    _columns = {
        'pickup_request_line_id': fields.many2one('pickup.request', 'pickup Request Line ID', required=True,
                                                  ondelete='cascade', select=True, readonly=True),
        'po_id': fields.many2one('purchase.order', 'PO ID', required=False,
                                 ondelete='cascade', select=True, readonly=True),
        'product': fields.char('Product'),
        'product_description': fields.char('Product Description'),
        'product_uom': fields.char('product uom'),
        # 'product_id': fields.integer('Product ID'),
        'product_id': fields.many2one('product.product', string='Product ID'),
        'ordered_quantity': fields.float('Ordered Qty'),
        'cancel_quantity': fields.float('Cancel Qty'),
        'updated_ordered_quantity': fields.float('Available Qty'),
        'request_quantity': fields.float('Request Qty', required=True, states={'confirmed': [('readonly', True)],
                                                                               'done': [('readonly', True)]}),
        'received_quantity': fields.float('Received Qty'),
        'remaining_quantity': fields.float('Remaining Qty'),
        'remark': fields.char('Remark'),

        'cancel_roll_back_pending_time': fields.datetime('Cancel Roll Back Pending Time'),

        'confirm_by': fields.many2one('res.users', 'Confirm By'),
        'confirm_time': fields.datetime('Confirmation Time'),

        'reattempt_by': fields.many2one('res.users', 'Reattempt By'),
        'reattempt_time': fields.datetime('Reattempt Time'),

        'cancel_by': fields.many2one('res.users', 'Cancel By'),
        'cancel_time': fields.datetime('Cancelled Time'),

        'create_manifest_time': fields.datetime('Create Manifest Time'),
        'reattempted_cancel': fields.boolean('Reattempted Cancel'),

        'request_type': fields.selection([
            ('fully_received', 'Fully Received'),
            ('partially_received', 'Partially Received')
        ], 'Request Type', help="Gives the status of the pickup request line", select=True),
        'pickup_type': fields.selection([
            ('full_received', 'Full Received'),
            ('partial_received', 'Partial Received'),
            ('not_received', 'Not Received'),

        ], 'Pickup Type', readonly=True, copy=False, help="Gives the status of the Pick up request", select=True),
        'state': fields.selection([
            ('pending', 'Pending'),
            ('confirmed', 'Confirmed'),
            ('create_manifest', 'Create Manifest'),
            ('unsuccessful', 'Unsuccessful'),
            ('done', 'Done'),
            ('cancel', 'Cancel'),

        ], 'Status', help="Gives the status of the pickup request line", select=True)
    }

    _defaults = {
        'state': 'pending',
    }


class PickupRequestCancelReason(osv.osv):
    _name = "pickup.request.cancel.reason"
    _description = "Pickup Request Cancel Reason"

    _columns = {
        'pickup_request_cancel_date': fields.datetime('Cancel Pickup Request Date'),
        'pickup_request_cancel_by': fields.many2one('res.users', 'Cancel Pickup Request By'),
        'pickup_request_cancel_reason': fields.text('Cancel Pickup Request Reason', required=True),
    }

    def pickup_request_cancel_reason_def(self, cr, uid, ids, context=None):
        ids = context['active_ids']
        pickup_request_cancel_reason = str(context['pickup_request_cancel_reason'])
        pickup_request_cancel_by = uid
        pickup_request_cancel_date = str(fields.datetime.now())

        pickup_obj = self.pool.get('pickup.request')

        for s_id in ids:

            if pickup_request_cancel_reason != ' ':

                pickup_request_cancel_query = "UPDATE pickup_request SET pickup_request_cancel_reason='{0}', pickup_request_cancel_by={1}, pickup_request_cancel_date='{2}' WHERE id={3}".format(
                    pickup_request_cancel_reason, pickup_request_cancel_by, pickup_request_cancel_date, s_id)

                cr.execute(pickup_request_cancel_query)

                cr.commit()

                pickup_obj.cancel_wms_inbound_pickup_request(cr, uid, [s_id], context)
            else:
                raise osv.except_osv(_('Warning!'),
                                     _('Please, Given actual reason for Cancel Pickup Request.'))

        return True

class UnsuccessfulPickupRequestCancelReason(osv.osv):
    _name = "unsuccessful.pickup.request.cancel.reason"
    _description = "Unsuccessful Pickup Request Cancel Reason"

    _columns = {
        'pickup_request_cancel_date': fields.datetime('Cancel Pickup Request Date'),
        'pickup_request_cancel_by': fields.many2one('res.users', 'Cancel Pickup Request By'),
        'pickup_request_cancel_reason': fields.text('Cancel Pickup Request Reason', required=True),
    }

    def cancel_unsuccessful_pickup_request(self, cr, uid, ids, context=None):

        ids = context['active_ids']
        pickup_request_cancel_reason = str(context['pickup_request_cancel_reason'])
        pickup_request_cancel_by = uid
        pickup_request_cancel_date = str(fields.datetime.now())

        request_env = self.pool.get('pickup.request')

        for single_id in ids:
            pickup_req_obj = request_env.browse(cr, uid, single_id, context=context)

            for pickup_req_line in pickup_req_obj.pickup_request_line:
                cancel_quantity = pickup_req_line.remaining_quantity

                cancel_update_query_line = "UPDATE pickup_request_line SET state='done',reattempted_cancel=True, cancel_time='{0}'," \
                                           " cancel_by='{1}',cancel_quantity={2}, request_quantity={3} WHERE pickup_request_line_id={4} and id={5}".format(
                    str(fields.datetime.now()), uid, cancel_quantity,pickup_req_line.request_quantity, single_id,pickup_req_line.id)
                cr.execute(cancel_update_query_line)
                cr.commit()

            cancel_update_query = "UPDATE pickup_request SET state ='done',reattempted_cancel=True, cancel_time='{0}', " \
                                  "cancel_by='{1}',pickup_request_cancel_reason='{2}', pickup_request_cancel_by={3}," \
                                  " pickup_request_cancel_date='{4}' WHERE id={5}".format(
                str(fields.datetime.now()), uid,pickup_request_cancel_reason, pickup_request_cancel_by, pickup_request_cancel_date, single_id)
            cr.execute(cancel_update_query)
            cr.commit()

        return True


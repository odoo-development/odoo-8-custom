from openerp.osv import fields, osv
from datetime import date, datetime
from openerp.tools.translate import _


class pickup_make_manifest(osv.osv_memory):
    _name = "pickup.manifest.wizard"
    _description = "Picking Manifest"
    _columns = {
        # 'vehicle_type': fields.char(string='Vehicle Type', required=True),
        'vehicle_type': fields.selection([

            ("SV - 1", "SV - 1"),
            ("SV - 2", "SV - 2"),
            ("SV - 3", "SV - 3"),
            ("SV - 4", "SV - 4"),
            ("SV - 5", "SV - 5"),
            ("SV - 6", "SV - 6"),
            ("SV - 7", "SV - 7"),
            ("SV - 8", "SV - 8"),
            ("SV - 9", "SV - 9"),
            ("CNG", "CNG"),
            ("Rickshaw", "Rickshaw"),
            ("Paddle Van", "Paddle Van"),
            ("Bike", "Bike"),
            ("Paddle", "Paddle"),
            ("Bus", "Bus",),
            ("Hired Vehicle", "Hired Vehicle")

        ], 'Vehicle Type', copy=False, help="Vehicle Type", select=True, required=True),

        'pickup_boy': fields.char(string='Pickup Boy', required=True),
        'warehouse_id': fields.many2one('stock.warehouse', string='Warehouse Location', required=True),
        'date_pickup': fields.date('Pick Up Date', required=True),
        'pickup_line': fields.one2many('pickup.manifest.line', 'pickup_manifest_id', 'Assign to Courier',
                                       required=True),
    }

    def _get_warehouse(self, cr, uid, context=None):
        record_id = context and context.get('active_ids')
        warehouse = []
        for req_id in record_id:
            pickup_obj = self.pool.get('pickup.request').browse(cr, uid, req_id, context=context)
            warehouse.append(pickup_obj.warehouse_id)
            if warehouse.count(warehouse[0]) == len(warehouse):
                return pickup_obj.warehouse_id.id
            else:
                raise osv.except_osv(_('Warning!'),
                                     _('Please select the same warehouse pick up request'))

    def _get_pickup_date(self, cr, uid, context=None):
        record_id = context and context.get('active_ids')
        date_pickup = []
        for req_id in record_id:
            pickup_obj = self.pool.get('pickup.request').browse(cr, uid, req_id, context=context)
            date_pickup.append(pickup_obj.date_pickup)
            if date_pickup.count(date_pickup[0]) == len(date_pickup):
                return pickup_obj.date_pickup
            else:
                raise osv.except_osv(_('Warning!'),
                                     _('Please select the same pick up date request'))

    _defaults = {
        'user_id': lambda obj, cr, uid, context: uid,
        'warehouse_id': _get_warehouse,
        'date_pickup': _get_pickup_date

    }

    def view_init(self, cr, uid, fields_list, context=None):
        if context is None:
            context = {}
        record_id = context and context.get('active_id', False)
        pickup_req = self.pool.get('pickup.request').browse(cr, uid, record_id, context=context)
        if pickup_req.state != 'confirmed':
            raise osv.except_osv(_('Warning!'), _('You cannot create manifest when pickup request is not confirmed.'))
        pick_list_date = []
        pick_warehouse = []
        for req_id in context.get('active_ids'):
            pickup_obj = self.pool.get('pickup.request').browse(cr, uid, req_id, context=context)
            pick_list_date.append(pickup_obj.date_pickup)

            if pick_list_date.count(pick_list_date[0]) != len(pick_list_date):
                raise osv.except_osv(_('Warning!'),
                                     _('Please select the same pick up date request'))
            pick_warehouse.append(pickup_obj.warehouse_id)
            if pick_warehouse.count(pick_warehouse[0]) != len(pick_warehouse):
                raise osv.except_osv(_('Warning!'),
                                     _('Please select the same warehouse pick up request'))

        return False

    def create_manifest(self, cr, uid, ids, context=None):

        manifest_obj = self.pool.get('pickup.manifest')

        if context is None:
            context = {}
        get_data = self.read(cr, uid, ids)[0]
        data = {}
        manifest_line = []

        data['vehicle_type'] = get_data.get('vehicle_type')
        data['pickup_boy'] = get_data.get('pickup_boy')
        data['warehouse_id'] = get_data.get('warehouse_id')[0]
        data['date_pickup'] = get_data.get('date_pickup')

        for req_id in context.get('active_ids'):
            pickup_obj = self.pool.get('pickup.request').browse(cr, uid, req_id, context=context)

            manifest_line.append([0, False, {
                'pickup_number': str(pickup_obj.name),
                'pickup_id': int(req_id),
                'po_number': str(pickup_obj.po_number),
                'po_id': int(pickup_obj.po_id),
                'partner_id': int(pickup_obj.partner_id.id),
                'partner_address': str(pickup_obj.partner_address),
                'area': str(pickup_obj.area),
                'warehouse_id': int(pickup_obj.warehouse_id.id),
                'state': 'pending'
            }])

        data['pickup_line'] = manifest_line

        save_the_data = manifest_obj.create(cr, uid, data, context=context)

        for i in context.get('active_ids'):
            update_query = "UPDATE pickup_request SET state='create_manifest', create_manifest_time='{0}' WHERE id ={1}".format(str(fields.datetime.now()), i)
            cr.execute(update_query)
            cr.commit()

            update_query_pickup_req_line = "UPDATE pickup_request_line SET state='create_manifest', create_manifest_time='{0}' WHERE pickup_request_line_id = {1}".format(str(fields.datetime.now()), i)
            cr.execute(update_query_pickup_req_line)
            cr.commit()

        return save_the_data


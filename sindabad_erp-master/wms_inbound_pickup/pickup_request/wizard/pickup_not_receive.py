from openerp.osv import fields, osv
from openerp.tools.translate import _
import datetime

class pickup_not_receive_reson(osv.osv):
    _name = "pickup.not.receive.reason"
    _description = "Pickup Not Receive Reason"

    _columns = {
        'pickup_not_receive_date': fields.datetime('Cancel Pickup Request Date'),
        'pickup_not_receive_by': fields.many2one('res.users', 'Cancel Pickup Request By'),
        # 'npr': fields.text('No Pickup Reson', required=True),
        'npr': fields.selection([

            ("Vendor was unreachable", "Vendor was unreachable"),
            ("Vendor refused to provide", "Vendor refused to provide"),
            ("Unable to reach", "Unable to reach"),
            ("Vendors place was closed", "Vendors place was closed"),
            ("Cash problem", "Cash problem"),
            ("Vendor rescheduled", "Vendor rescheduled"),
            ("Wrong address given", "Wrong address given"),
            ("Unable to reach on Time", "Unable to reach on Time"),
            ("Quality issue", "Quality issue"),
            ("Due to traffic issue", "Due to traffic issue"),
            ("Invoice/Cash Memo not provided", "Invoice/Cash Memo not provided")

        ], 'No Pick UP Reason', copy=False, help="Reason", select=True, required=True),

        'pickup_req_id': fields.char('Pickup Request ID'),
        'po_id': fields.integer('Purchase ID'),
        'pickup_manifest_id': fields.integer('Pickup Manifest ID'),
        'pickup_manifest_line_id': fields.integer('Pickup Manifest Line ID'),
    }


    def default_get(self, cr, uid, fields, context=None):
        if context is None:
            context = {}
        res = super(pickup_not_receive_reson, self).default_get(cr, uid, fields, context=context)
        manifest_pickup = self.pool.get('pickup.manifest.line').browse(cr, uid, context['active_id'], context=context)
        pickup_req = self.pool.get('pickup.request').browse(cr, uid, manifest_pickup.pickup_id, context=context)

        res['po_id'] = int(pickup_req.po_id)
        res['pickup_req_id'] = manifest_pickup.pickup_id
        res['pickup_manifest_id'] = int(manifest_pickup.pickup_manifest_id)
        res['pickup_manifest_line_id'] = context['active_id']
        return res


    def pickup_not_receive(self, cr, uid, ids, fields, context=None):

        if context is None:
            context = {}
        get_data = self.read(cr, uid, ids)[0]
        npr = str(get_data['npr'])
        pickup_not_receive_by = uid


        pickup_obj = self.pool.get('pickup.request').browse(cr, uid, int(get_data['pickup_req_id']), context=context)

        if npr != ' ':
            state = 'unsuccessful'
            pickup_type = 'not_received'
            # reattempt = self.pool.get('pickup.success')._check_pickup_request_reattempt(cr, uid, pickup_obj.po_id, context=context)
            #
            # update_query_po_reattempt_count = "UPDATE purchase_order SET reattempt_number={0} WHERE id={1}". \
            #     format(reattempt, pickup_obj.po_id)
            # cr.execute(update_query_po_reattempt_count)
            # cr.commit()

            for i in pickup_obj.pickup_request_line:
                requested_quantity = i.request_quantity
                update_query_pickup_req_line = "UPDATE pickup_request_line SET state='{0}', pickup_type='{1}',received_quantity=0.00, " \
                                               "remaining_quantity={2} WHERE pickup_request_line_id = {3} and id ={4} ".format(state,
                                                                                                        pickup_type,
                                                                                                        requested_quantity,
                                                                                                        int(get_data[
                                                                                                                'pickup_req_id']),
                                                                                                        int(i.id))
                cr.execute(update_query_pickup_req_line)
                cr.commit()

                update_query_manifest_line = "UPDATE pickup_manifest_line SET state='done'" \
                                             "WHERE pickup_manifest_id = {0} and  pickup_id = {1} and id ={2}".format(
                    int(get_data['pickup_manifest_id']), int(get_data['pickup_req_id']),
                    int(get_data['pickup_manifest_line_id']))

                cr.execute(update_query_manifest_line)
                cr.commit()

            manifest_line_obj = self.pool.get('pickup.manifest').browse(cr, uid, int(get_data['pickup_manifest_id']),
                                                                        context=context)
            res = []
            for manifest_line in manifest_line_obj.pickup_line:
                res.append(manifest_line.state)

            if len(list(set(res))) == 1:
                update_query_manifest = "UPDATE pickup_manifest SET state='done'" \
                                        "WHERE id ={0}".format(
                                                               int(get_data['pickup_manifest_id']))

                cr.execute(update_query_manifest)
                cr.commit()

            update_query = "UPDATE pickup_request SET state='{0}', pickup_type='{1}' , npr='{2}', " \
                           "received_by={3},pickup_not_receive_date='{4}' WHERE id ={5}".format(
                state, pickup_type, npr, pickup_not_receive_by, (datetime.datetime.now()), int(get_data['pickup_req_id']))
            cr.execute(update_query)
            cr.commit()

            try:
                self.pool.get('pickup.request').send_email(cr, 1, 'NPR - Pickup Process',int(get_data['pickup_req_id']), context=None)
            except:
                pass

        else:
            raise osv.except_osv(_('Warning!'),
                                 _('Please, Given actual reason for Cancel Pickup Request.'))
        return True

import pytz
from openerp import SUPERUSER_ID, workflow
from datetime import datetime
from dateutil.relativedelta import relativedelta
from operator import attrgetter
from openerp.tools.safe_eval import safe_eval as eval
from openerp.osv import fields, osv
from openerp.tools.translate import _
import openerp.addons.decimal_precision as dp
from openerp.osv.orm import browse_record_list, browse_record, browse_null
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT, DATETIME_FORMATS_MAP
from openerp.tools.float_utils import float_compare


class purchase_order(osv.osv):
    _inherit = 'purchase.order'

    _columns = {
        'pickup_request_line': fields.one2many('pickup.request.line', 'po_id', 'Pickup Request', required=False),

        'reattempt_number': fields.integer(string="Re-attempt Count"),

        'vendor_pickup': fields.selection([
            ('yes', 'Yes'),
            ('no', 'No')
        ], 'Vendor Pickup'),
    }

    _defaults = {
        'vendor_pickup': 'no',
    }


class back_to_back_order(osv.osv_memory):
    _inherit = "back.to.back.order"

    _columns = {
        'vendor_pickup': fields.selection([
            ('yes', 'Yes'),
            ('no', 'No')
        ], 'Vendor Pickup'),
    }

    _defaults = {
        'vendor_pickup': 'no',
    }


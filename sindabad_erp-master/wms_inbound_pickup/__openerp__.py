{
    'name': 'WMS Inbound Pickup Process',
    'version': '1.0',
    'category': 'WMS',
    'author': 'Shuvarthi & Rocky',
    'summary': 'The WMS Inbound Pick UP process',
    'description': 'The WMS Inbound Pick UP process',
    'depends': ['base', 'purchase','product', 'product_simple_sync','wms_inbound'],
    'data': [
        'security/wms_inbound_pickup_security.xml',
        'security/ir.model.access.csv',

        'pickup_request/wizard/select_pickup_request.xml',
        'pickup_request/wizard/pickup_receive.xml',
        'pickup_request/wizard/pickup_not_receive.xml',
        'pickup_request/wizard/cancel_pickup_request.xml',
        'pickup_request/wizard/unsuccessful_cancel_pickup_reson.xml',

        'purchase/purchase_view.xml',
        'purchase/pickup_po_view.xml',

        'pickup_end_to_end/pickup_end_to_end_view.xml',

        'manifest/manifest_pending_view.xml',
        'manifest/manifest_approve_view.xml',
        'manifest/manifest_dispatch_view.xml',
        'manifest/manifest_close_view.xml',

        'manifest/manifest_view.xml',

        'pickup_request/pickup_request_pending_view.xml',
        'pickup_request/pickup_request_confirm_view.xml',
        'pickup_request/pickup_request_completed_view.xml',
        'pickup_request/pickup_request_unsuccessful_view.xml',
        'pickup_request/pickup_request_cancel_view.xml',

        'pickup_request/pickup_request_view.xml',

        'pickup_menu.xml',

        'manifest/report/report_pickup_manifest_layout.xml',
        'manifest/report/pickup_manifest_report_menu.xml',
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}

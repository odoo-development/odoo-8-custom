from datetime import datetime, timedelta
import time
from datetime import date
from openerp import SUPERUSER_ID
from openerp.osv import fields, osv
from openerp.tools.translate import _
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT
import openerp.addons.decimal_precision as dp
from openerp import workflow
from openerp import api
import datetime
from openerp.exceptions import except_orm, Warning, RedirectWarning

# "2019-01-15 07:20:15.351546"


class inb_end_to_end(osv.osv):
    _inherit = "inb.end.to.end"

    _columns = {
        'pickup_id': fields.char('Pickup IDs'),
        'pickup_number': fields.char('Pickup Number'),
        'pickup_request_date_create': fields.text('Pickup Requst Created Date'),
        'pickup_request_date_confirm': fields.text('Pickup Request Confirm Date'),
        'manifest_id': fields.char('Pickup IDs'),
        'manifest_number': fields.char('Manifest Number'),
        'manifest_date_create': fields.text('Manifest Created Date'),
        'manifest_date_confirm': fields.text('Manifest Confirm Date'),
        'manifest_date_dispatch': fields.text('Manifest Dispatch Date')
    }



    def pickup_data_create(self, cr, uid, po_number, po_id, pickup_id, pickup_number, pickup_request_date_create, context=None):

        po_count = self.search_count(cr, uid, [('po_number', '=', po_number), ('po_id', '=', po_id)])

        if int(po_count) == 0:
            created_id = self.create(cr, uid, {
                'po_number': po_number,
                'po_id': po_id,
                'pickup_id': pickup_id,
                'pickup_number': pickup_number,
                'pickup_request_date_create': pickup_request_date_create,
            }, context=context)
        else:

            inb_etoe_id = self.search(cr, uid, [('po_number', '=', po_number), ('po_id', '=', po_id)])

            # browse and then update
            for etoe in self.browse(cr, uid, inb_etoe_id, context):
                self.write(cr, uid, inb_etoe_id, {
                    'pickup_id': str(pickup_id) if str(etoe.pickup_id) == 'False' else str(etoe.pickup_id) + "," + str(
                        pickup_id),
                    'pickup_number': str(pickup_number) if str(etoe.pickup_number) == 'False' else str(etoe.pickup_number) + "," + str(
                        pickup_number),
                    'pickup_request_date_create': str(pickup_request_date_create) if str(etoe.pickup_request_date_create) == 'False' else str(etoe.pickup_request_date_create) + "," + str(
                        pickup_request_date_create),
                })

        return True


    def pickup_request_confirm_update(self, cr, uid, po_number, po_id, pickup_request_date_confirm, context=None):

        inb_etoe_id = self.search(cr, uid, [('po_number', '=', po_number), ('po_id', '=', po_id)])

        # browse and then update
        for etoe in self.browse(cr, uid, inb_etoe_id, context):

            self.write(cr, uid, inb_etoe_id, {
                'pickup_request_date_confirm': str(pickup_request_date_confirm) if str(etoe.pickup_request_date_confirm) == 'False' else str(etoe.pickup_request_date_confirm) + "," + str(pickup_request_date_confirm),
            })

        return True


    def manifest_create(self, cr, uid, po_number, po_id, manifest_id, manifest_number, manifest_date_create, context=None):

        inb_etoe_id = self.search(cr, uid, [('po_number', '=', po_number), ('po_id', '=', po_id)])

        # browse and then update
        for etoe in self.browse(cr, uid, inb_etoe_id, context):
            self.write(cr, uid, inb_etoe_id, {
                'manifest_id': str(manifest_id) if str(etoe.manifest_id) == 'False' else str(etoe.manifest_id) + "," + str(manifest_id),
                'manifest_number': str(manifest_number) if str(etoe.manifest_number) == 'False' else str(etoe.manifest_number) + "," + str(manifest_number),
                'manifest_date_create': str(manifest_date_create) if str(etoe.manifest_date_create) == 'False' else str(etoe.manifest_date_create) + "," + str(manifest_date_create),
            })

        return True

    def manifest_confirm_update(self, cr, uid, po_number, po_id, manifest_date_confirm, context=None):

        inb_etoe_id = self.search(cr, uid, [('po_number', '=', po_number), ('po_id', '=', po_id)])

        # browse and then update
        for etoe in self.browse(cr, uid, inb_etoe_id, context):
            self.write(cr, uid, inb_etoe_id, {
                'manifest_date_confirm': str(manifest_date_confirm) if str(etoe.manifest_date_confirm) == 'False' else str(etoe.manifest_date_confirm) + "," + str(manifest_date_confirm),
            })

        return True

    def manifest_dispatch_update(self, cr, uid, po_number, po_id, manifest_date_dispatch, context=None):

        inb_etoe_id = self.search(cr, uid, [('po_number', '=', po_number), ('po_id', '=', po_id)])

        # browse and then update
        for etoe in self.browse(cr, uid, inb_etoe_id, context):
            self.write(cr, uid, inb_etoe_id, {
                'manifest_date_dispatch': str(manifest_date_dispatch) if str(etoe.manifest_date_dispatch) == 'False' else str(etoe.manifest_date_dispatch) + "," + str(manifest_date_dispatch),
            })

        return True
import time
from openerp.report import report_sxw
from openerp.osv import osv

class manifestpickupdata(report_sxw.rml_parse):

    def __init__(self, cr, uid, name, context):
        super(manifestpickupdata, self).__init__(cr, uid, name, context)
        self.localcontext.update({
            'get_pickup_data': self.get_pickup_data

        })

    def get_pickup_data(self, obj):
        report_data = []
        manifest_number = [str(obj)]
        self.cr.execute("SELECT id FROM pickup_manifest WHERE name=%s", (manifest_number))
        manifest_id = self.cr.fetchall()[0]

        self.cr.execute("SELECT pickup_id FROM pickup_manifest_line WHERE pickup_manifest_id=%s", (manifest_id))

        for item in self.cr.fetchall() :

            pick_id = item[0]
            pickup_data = self.pool.get('pickup.request').browse(self.cr, self.uid, pick_id)

            data = {
                'pickup_number':pickup_data.name,
                'po_number':pickup_data.po_number,
                'vendor_name':pickup_data.partner_id.name,
                'vendor_contact':pickup_data.partner_id.mobile,
                'vendor_address':pickup_data.partner_address,
                'po_warehouse':pickup_data.po_warehouse_id.name,
                'npr':pickup_data.npr,
                'remark':pickup_data.remark,
                'pickup_product':[]
            }
            for it in pickup_data.pickup_request_line:
                data['pickup_product'].append({
                    'product_name':it.product,
                    'product_description':it.product_description,
                    'product_uom':it.product_uom,
                    'ordered_quantity':it.ordered_quantity,
                    'request_quantity':it.request_quantity
                })
            report_data.append(data)

        return report_data

class report_pickup_manifest_layout(osv.AbstractModel):
    _name = 'report.wms_inbound_pickup.report_pickup_manifest_layout'
    _inherit = 'report.abstract_report'
    _template = 'wms_inbound_pickup.report_pickup_manifest_layout'
    _wrapped_report_class = manifestpickupdata
from datetime import datetime, timedelta
from openerp.osv import fields, osv
from openerp import api


class pickup_manifest(osv.osv):
    _name = "pickup.manifest"
    _description = "Pickup Manifest Process"


    # def _close_status_update(self, cr, uid, ids, fields, arg, context=None):
    #     res = {}
    #     for single_id in ids:
    #         manifest_obj = self.pool.get('pickup.manifest').browse(cr, uid, single_id, context=context)
    #         manifest_create_date = datetime.strptime(manifest_obj.create_date,'%Y-%m-%d %H:%M:%S')
    #         manifest_close_date = str(manifest_create_date+timedelta(hours=36))
    #         current_datetime = str(datetime.now())
    #         if current_datetime > manifest_close_date and manifest_obj.status=='open':
    #             dispatch_pickup_query = "UPDATE pickup_manifest SET status='close', close_time='{0}' WHERE id='{1}'".format(
    #                 manifest_close_date, single_id)
    #             cr.execute(dispatch_pickup_query)
    #             cr.commit()
    #     return res

    _columns = {
        # 'manifest_close_status': fields.function(_close_status_update, string="Manifest", type="many2many", relation='pickup.manifest'),
        'vehicle_type': fields.char(string='Vehicle Type', required=True),
        'name': fields.char(string='Manifest Number'),
        'pickup_boy': fields.char(string='Pickup Boy', required=True),
        'warehouse_id': fields.many2one('stock.warehouse', string='Warehouse Location', required=True),
        'date_pickup': fields.date('Pick Up Date', required=True),
        'remark': fields.text('Remark'),
        'scan': fields.char('Scan'),
        'pickup_line': fields.one2many('pickup.manifest.line', 'pickup_manifest_id', 'Assign to Courier', required=True),

        'approved_time': fields.datetime('Approved Date', help="Date on which manifest is approved."),
        'approved_by': fields.many2one('res.users', 'Approved By'),

        'dispatch_time': fields.datetime('Dispatch Date', help="Date on which manifest is dispatch."),
        'dispatch_by': fields.many2one('res.users', 'Dispatch By'),

        'close_time': fields.datetime('Close Date', help="Date on which manifest is closeled."),
        'close_by': fields.many2one('res.users', 'Close By'),

        'cancel_time': fields.datetime('Cancel Date', help="Date on which manifest is closeled."),
        'cancel_by': fields.many2one('res.users', 'Cancel By'),

        # 'npr': fields.char('npr'),
        'npr': fields.selection([

            ("Vendor was unreachable", "Vendor was unreachable"),
            ("Vendor refused to provide", "Vendor refused to provide"),
            ("Unable to reach", "Unable to reach"),
            ("Vendors place was closed", "Vendors place was closed"),
            ("Cash problem", "Cash problem"),
            ("Vendor rescheduled", "Vendor rescheduled"),
            ("Wrong address given", "Wrong address given"),
            ("Unable to reach on Time", "Unable to reach on Time"),
            ("Quality issue", "Quality issue"),
            ("Due to traffic issue", "Due to traffic issue")

        ], 'No Pick UP Reason', copy=False, help="Reason", select=True),

        'state': fields.selection([
            ('pending', 'Pending'),
            ('approved', 'Approve'),
            ('dispatch', 'Dispatch'),
            ('done', 'Done'),
            ('cancel', 'Cancel'),

        ], 'State', readonly=True, copy=False, help="Gives the status of the Pickup Manifest", select=True)
    }

    _defaults = {
        'user_id': lambda obj, cr, uid, context: uid,
        'state': 'pending'
    }

    _order = 'id desc'

    def print_report(self, cr, uid, ids, context=None):
        '''This function prints the pickup manifest'''
        context = dict(context or {}, active_ids=ids)
        return self.pool.get("report").get_action(cr, uid, ids, 'wms_inbound_pickup.report_pickup_manifest_layout', context=context)
        

    def approved_pickup_manifest(self, cr, uid, ids, context=None):

        try:
            manifest_env = self.pool.get('pickup.manifest')
            manifest_obj = manifest_env.browse(cr, uid, ids, context=context)

            for single_id in ids:
                approved_pickup_query = "UPDATE pickup_manifest SET state='approved', approved_by='{0}', approved_time='{1}' WHERE id='{2}'".format(
                    uid, str(fields.datetime.now()), single_id)
                cr.execute(approved_pickup_query)
                cr.commit()

                approved_pickup_query_line = "UPDATE pickup_manifest_line SET state='approved', approved_by='{0}', approved_time='{1}' WHERE pickup_manifest_id = {2}".format(
                    uid, str(fields.datetime.now()), single_id)
                cr.execute(approved_pickup_query_line)
                cr.commit()
            for row in manifest_obj.pickup_line:
                self.pool.get('inb.end.to.end').manifest_confirm_update(cr, uid, str(row.po_number), row.po_id,
                                                               str(fields.datetime.now()), context=context)
        except:
            pass

        return True

    def dispatch_pickup_manifest(self, cr, uid, ids, context=None):

        try:
            manifest_env = self.pool.get('pickup.manifest')
            manifest_obj = manifest_env.browse(cr, uid, ids, context=context)
            for single_id in ids:
                dispatch_pickup_query = "UPDATE pickup_manifest SET state='dispatch', dispatch_by='{0}', dispatch_time='{1}' WHERE id='{2}'".format(
                    uid, str(fields.datetime.now()), single_id)
                cr.execute(dispatch_pickup_query)
                cr.commit()

                dispatch_pickup_query_line = "UPDATE pickup_manifest_line SET state='dispatch', dispatch_by='{0}', dispatch_time='{1}' WHERE pickup_manifest_id = {2}".format(
                    uid, str(fields.datetime.now()), single_id)
                cr.execute(dispatch_pickup_query_line)
                cr.commit()

            for row in manifest_obj.pickup_line:
                self.pool.get('inb.end.to.end').manifest_dispatch_update(cr, uid, str(row.po_number), row.po_id,
                                                               str(fields.datetime.now()), context=context)
        except:
            pass

        return True

    def cancel_pickup_manifest(self, cr, uid, ids, context=None):

        try:
            for single_id in ids:
                close_pickup_query = "UPDATE pickup_manifest SET state='cancel', cancel_by='{0}', cancel_time='{1}' WHERE id='{2}'".format(
                    uid, str(fields.datetime.now()), single_id)
                cr.execute(close_pickup_query)
                cr.commit()

                close_pickup_query_line = "UPDATE pickup_manifest_line SET state='cancel', cancel_by='{0}', cancel_time='{1}' WHERE pickup_manifest_id = {2}".format(
                    uid, str(fields.datetime.now()), single_id)
                cr.execute(close_pickup_query_line)
                cr.commit()

                pickup_manifest = self.pool.get('pickup.manifest').browse(cr, uid, single_id, context=context)

                for row in pickup_manifest.pickup_line:
                    update_query = "UPDATE pickup_request SET state ='pending', cancel_roll_back_pending_time ='{0}' WHERE id={1}".format(
                        str(fields.datetime.now()), row.pickup_id)
                    cr.execute(update_query)
                    cr.commit()

                    update_query_pickup_req_line = "UPDATE pickup_request_line SET state='pending', cancel_roll_back_pending_time ='{0}' WHERE pickup_request_line_id={1}".format(
                        str(fields.datetime.now()), row.pickup_id)
                    cr.execute(update_query_pickup_req_line)
                    cr.commit()
        except:
            pass

        return True

    @api.model
    def create(self, vals):

        # generate manifest number
        record = super(pickup_manifest, self).create(vals)

        manifest_number = "MF0" + str(record.id)
        record.name = manifest_number
        pickup_line = vals['pickup_line'][0][2]

        self.pool.get('inb.end.to.end').manifest_create(self._cr, self._uid, pickup_line['po_number'], pickup_line['po_id'], record.id, manifest_number, record.create_date, context=self._context)

        return record


class pickup_manifest_line(osv.osv):
    _name = "pickup.manifest.line"
    _description = "Manifest Line"

    _columns = {
        'pickup_manifest_id': fields.many2one('pickup.manifest', 'Manifest Line ID', required=True, ondelete='cascade',
                                              select=True, readonly=True),
        'pickup_number': fields.char('Pickup Number'),
        'pickup_id': fields.integer('PIckup ID'),
        'po_number': fields.char('PO Number'),
        # 'npr': fields.char('npr'),
        'npr': fields.selection([

            ("Vendor was unreachable", "Vendor was unreachable"),
            ("Vendor refused to provide", "Vendor refused to provide"),
            ("Unable to reach", "Unable to reach"),
            ("Vendors place was closed", "Vendors place was closed"),
            ("Cash problem", "Cash problem"),
            ("Vendor rescheduled", "Vendor rescheduled"),
            ("Wrong address given", "Wrong address given"),
            ("Unable to reach on Time", "Unable to reach on Time"),
            ("Quality issue", "Quality issue"),
            ("Due to traffic issue", "Due to traffic issue")

        ], 'No Pick UP Reason', copy=False, help="Reason", select=True),

        'po_id': fields.integer('Purchase ID'),
        'partner_id': fields.many2one('res.partner', 'Vendor', required=True,
                                      change_default=True, track_visibility='always'),
        'partner_address': fields.char('Vendor Address', required=True),
        'area': fields.char('Area', required=True),
        'warehouse_id': fields.many2one('stock.warehouse', string='Warehouse Location', required=True),

        'approved_time': fields.datetime('Approved Date', help="Date on which manifest is approved."),
        'approved_by': fields.many2one('res.users', 'Approved By'),

        'dispatch_time': fields.datetime('Dispatch Date', help="Date on which manifest is dispatch."),
        'dispatch_by': fields.many2one('res.users', 'Dispatch By'),

        'close_time': fields.datetime('Close Date', help="Date on which manifest is closeled."),
        'close_by': fields.many2one('res.users', 'Close By'),

        'cancel_time': fields.datetime('Cancel Date', help="Date on which manifest is closeled."),
        'cancel_by': fields.many2one('res.users', 'Cancel By'),

        'state': fields.selection([
            ('pending', 'Pending'),
            ('approved', 'Approve'),
            ('dispatch', 'Dispatch'),
            ('close', 'Close'),

        ], 'Status', readonly=True, copy=False, help="Gives the status of the Pickup Manifest", select=True),

    }

{
    'name': 'Partial Delivery Restricted for Retail Customer Orders',
    'version': '1.0',
    'category': 'Stock',
    'summary': """Retail classification orders can not be delivered partially warning module.""",
    'description': """Retail classification orders can not be delivered partially warning module.""",
    'author': 'Md Rockibul Alam',
    'website': 'http://www.sindabad.com',
    'depends': ['odoo_magento_connect', 'stock','partial_order_delivery'],
    'init_xml': [],
    'update_xml': [

        'stock_view.xml',
         
    ],
    'demo_xml': [],
    'installable': True,
    'active': False,

}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:

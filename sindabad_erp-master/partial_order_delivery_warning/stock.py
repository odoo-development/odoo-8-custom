import new

from openerp.osv import fields, osv
from openerp import api
from openerp.tools.translate import _


class stock_picking(osv.osv):
    _inherit = "stock.picking"

    def partial_so_delivery_challan(self, cr, uid, origin, context=None):

        order_data = self.pool.get('sale.order').browse(cr, uid, context['active_id'], context=context)

        if order_data.customer_classification == 'Retail':
            raise osv.except_osv(_('Warning'), _(
                "Sorry, Retail classification orders can not be delivered partially."))

        else:
            return {
                'type': 'ir.actions.act_window',
                'name': 'Partial Order Delivery Challan',
                'res_model': 'partial.delivery.create',
                'view_type': 'form',
                'view_mode': 'form',
                'target': 'new',
            }

    def _get_retail_customer_classification(self, cursor, user, ids, name, arg, context=None):

        res = {}

        for items in self.browse(cursor, user, ids, context=context):
            # import pdb
            # pdb.set_trace()
            res[items.id] = items.partner_id.parent_id.x_classification if items.partner_id.parent_id.x_classification else items.partner_id.x_classification
        return res

    _columns = {

        'retail_customer_classification': fields.function(_get_retail_customer_classification,string='Retail Customer Classification', type='char'),

    }

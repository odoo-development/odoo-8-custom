{
    'name': 'Customer Star Category',

    'version': '1.0',

    'category': 'Information Technology',

    'author': "Md Rockibul Alam Babu",

    'description': """
    Customer review category wise star rating in sale module.
        """,

    'website': 'http://www.sindabad.com',

    'depends': [
        'sale'
    ],
    'data': [

    ],
}
from openerp.osv import fields, osv


class res_partner(osv.osv):
    _inherit = "res.partner"

    def _get_customer_star_category(self, cursor, user, ids, name, arg, context=None):
        res = False
        res = {}

        for cus in self.browse(cursor, user, ids, context=context):
            str = '* '
            cus_star_rating = ''
            for i in range(cus.x_star_category):
                if i < 5:
                    cus_star_rating += str
                else:
                    cus_star_rating = ''

            res[cus.id] = cus_star_rating

        return res


    _columns = {

        'customer_star_category': fields.function(_get_customer_star_category, string='Star Category', type='char'),


    }

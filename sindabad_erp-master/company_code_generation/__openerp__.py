{
    'name': "Company code generation",
    'version': '1.0',
    'category': 'Sale',
    'author': "Md Rockibul Alam Babu",
    'description': """
Company-code generation in customer form
    """,
    'website': "http://www.zerogravity.com.bd",
    'data': [
        'company_code_view.xml',
    ],
    'depends': [
        'sale', 'order_approval_process'
    ],
    'installable': True,
    'auto_install': False,
}

from openerp.osv import fields, osv


class res_partner(osv.osv):
    _inherit = "res.partner"

    def _get_parent_code(self, cursor, user, ids, name, arg, context=None):

        res = {}

        for cus in self.browse(cursor, user, ids, context=context):

            res[cus.id] = cus.parent_id.company_code if cus.parent_id.company_code else cus.company_code

        return res

    _columns = {

        'parent_code': fields.function(_get_parent_code, string='Company Code', type='char', store=True),
        'company_code': fields.char(string='Company Code (only Company)'),

    }


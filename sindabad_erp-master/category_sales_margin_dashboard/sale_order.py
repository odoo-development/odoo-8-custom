from openerp import models, api
import datetime
from datetime import datetime


class sales_margin(models.Model):
    _inherit = 'sale.order'

    @api.model
    def _report_xls_fields_category(self):
        return [
            'category', 'ananta_order_value', 'general_order_value', 'corporate_order_value', 'retail_order_value','total_order_value', 'comparison_period_total_order_value',
            'ananta_sale_value', 'general_sale_value','corporate_sale_value', 'retail_sale_value','total_sale_value', 'comparison_period_total_sale_value',
            'ananta_margin_value', 'general_margin_value','corporate_margin_value', 'retail_margin_value','total_margin_value', 'comparison_period_total_margin_value',
            'ananta_reserve_tat','corporate_reserve_tat','general_reserve_tat','retail_reserve_tat','total_reserve_tat','comparison_period_total_reserve_tat_value',
            'ananta_delivery_tat','corporate_delivery_tat','general_delivery_tat','retail_delivery_tat','total_delivery_tat','comparison_period_total_delivery_tat_value'
        ]

    # Change/Add Template entries
    @api.model
    def _report_xls_template(self):

        return {}


class sales_tat(models.Model):
    _inherit = 'sale.order'

    @api.model
    def _report_xls_fields_tat(self):
        return [
            'order','magento_order','customer_name', 'classification','product_name', 'product_sku','product_category',
            'order_date','confirm_order_date','reserve_date', 'date_done', 'invoice_date','order_to_confirmation',
            'confirmation_to_reserve','reserve_to_delivery','delivery_to_invoice','total_tat'
        ]

    # Change/Add Template entries
    @api.model
    def _report_xls_template(self):

        return {}
import xlwt
import datetime
from openerp.osv import orm
from openerp.report import report_sxw
from openerp.addons.report_xls.report_xls import report_xls
from openerp.addons.report_xls.utils import rowcol_to_cell, _render
from openerp.tools.translate import translate, _
import logging
from itertools import groupby
from operator import itemgetter
from datetime import datetime,timedelta,date

_logger = logging.getLogger(__name__)


_ir_translation_name = 'tat.breakdown.xls'


class tat_breakdown_xls_parser(report_sxw.rml_parse):

    def __init__(self, cr, uid, name, context):

        super(tat_breakdown_xls_parser, self).__init__(
            cr, uid, name, context=context)
        move_obj = self.pool.get('sale.order')
        self.context = context
        wanted_list = move_obj._report_xls_fields_tat(cr, uid, context)
        template_changes = move_obj._report_xls_template(cr, uid, context)
        self.localcontext.update({
            'datetime': datetime,
            'wanted_list': wanted_list,
            'template_changes': template_changes,
            '_': self._,
        })

    def _(self, src):
        lang = self.context.get('lang', 'en_US')
        return translate(self.cr, _ir_translation_name, 'report', lang, src) \
            or src


class tat_breakdown_xls(report_xls):

    def __init__(self, name, table, rml=False, parser=False, header=True,
                 store=False):

        super(tat_breakdown_xls, self).__init__(
            name, table, rml, parser, header, store)

        # Cell Styles
        _xs = self.xls_styles
        # header
        rh_cell_format = _xs['bold'] + _xs['fill'] + _xs['borders_all']
        self.rh_cell_style = xlwt.easyxf(rh_cell_format)
        self.rh_cell_style_center = xlwt.easyxf(rh_cell_format + _xs['center'])
        self.rh_cell_style_right = xlwt.easyxf(rh_cell_format + _xs['right'])
        # lines
        aml_cell_format = _xs['borders_all']
        self.aml_cell_style = xlwt.easyxf(aml_cell_format)
        self.aml_cell_style_center = xlwt.easyxf(
            aml_cell_format + _xs['center'])
        self.aml_cell_style_date = xlwt.easyxf(
            aml_cell_format + _xs['left'],
            num_format_str=report_xls.date_format)
        self.aml_cell_style_decimal = xlwt.easyxf(
            aml_cell_format + _xs['right'],
            num_format_str=report_xls.decimal_format)
        # totals
        rt_cell_format = _xs['bold'] + _xs['fill'] + _xs['borders_all']
        self.rt_cell_style = xlwt.easyxf(rt_cell_format)
        self.rt_cell_style_right = xlwt.easyxf(rt_cell_format + _xs['right'])
        self.rt_cell_style_decimal = xlwt.easyxf(
            rt_cell_format + _xs['right'],
            num_format_str=report_xls.decimal_format)

        # XLS Template
        self.col_specs_template = {

            'order': {
                'header': [1, 20, 'text', _render("_('Odoo Order Number')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'magento_order': {
                'header': [1, 20, 'text', _render("_('Magento Order Number')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'customer_name': {
                'header': [1, 20, 'text', _render("_('Customer Name')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'classification': {
                'header': [1, 20, 'text', _render("_('Classification')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'product_name': {
                'header': [1, 20, 'text', _render("_('Product Name')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'product_sku': {
                'header': [1, 20, 'text', _render("_('Product sku')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'product_category': {
                'header': [1, 20, 'text', _render("_('Category')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'order_date': {
                'header': [1, 20, 'text', _render("_('Order Date')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'confirm_order_date': {
                'header': [1, 20, 'text', _render("_('Order Confirmation Date')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'reserve_date': {
                'header': [1, 20, 'text', _render("_('Reserve Date')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'date_done': {
                'header': [1, 20, 'text', _render("_('Delivery Date')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'invoice_date': {
                'header': [1, 20, 'text', _render("_('Invoice Date')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'order_to_confirmation': {
                'header': [1, 20, 'text', _render("_('order_to_confirmation')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'confirmation_to_reserve': {
                'header': [1, 20, 'text', _render("_('confirmation_to_reserve')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'reserve_to_delivery': {
                'header': [1, 20, 'text', _render("_('reserve_to_delivery')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'delivery_to_invoice': {
                'header': [1, 20, 'text', _render("_('delivery_to_invoice')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'total_tat': {
                'header': [1, 20, 'text', _render("_('total_tat')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

        }

    def daterange(self,date1, date2):
        for n in range(int((date2 - date1).days) + 1):
            yield date1 + timedelta(n)

    def generate_xls_report(self, _p, _xs, data, objects, wb):

        wanted_list = _p.wanted_list
        self.col_specs_template.update(_p.template_changes)
        _ = _p._


        context = self.context
        st_date = data.get('form').get('date_from')
        end_date = data.get('form').get('date_to')

        st_date_time = st_date + ' 00:00:00'
        end_date_time = end_date + ' 23:59:59'


        context['start_date'] = st_date
        context['end_date'] = end_date
        self.context = context
        date_range = _("Date: %s to %s" % (st_date, end_date))

        # report_name = objects[0]._description or objects[0]._name
        report_name = _("Turn Over Time Report")
        ws = wb.add_sheet(report_name[:31])
        ws.panes_frozen = True
        ws.remove_splits = True
        ws.portrait = 0  # Landscape
        ws.fit_width_to_pages = 1
        row_pos = 0

        # set print header/footer
        ws.header_str = self.xls_headers['standard']
        ws.footer_str = self.xls_footers['standard']

        # Title
        cell_style = xlwt.easyxf(_xs['xls_title'])
        c_specs = [
            ('report_name', 2, 2, 'text', report_name),
            ('date_range', 1, 1, 'text', date_range),
        ]
        row_data = self.xls_row_template(c_specs, ['report_name', 'date_range'])
        row_pos = self.xls_write_row(
            ws, row_pos, row_data, row_style=cell_style)
        row_pos += 1

        c_specs = map(lambda x: self.render(x, self.col_specs_template, 'header', render_space={'_': _p._}),wanted_list)
        row_data = self.xls_row_template(c_specs, [x[0] for x in c_specs])
        row_pos = self.xls_write_row(
            ws, row_pos, row_data, row_style=self.rh_cell_style,
            set_column_size=True)
        ws.set_horz_split_pos(row_pos)



        in_query = "select id from  stock_picking_type where code='incoming'"
        self.cr.execute(in_query)
        in_objects = self.cr.dictfetchall()

        incoming_ids = [in_item.get('id') for in_item in in_objects]


        tat_query = "SELECT sale_order.id AS sale_order_id,sale_order.name AS order_number,sale_order.client_order_ref AS magento_order_number, sale_order.date_order AS order_date, sale_order.date_confirm,mail_message.create_date AS order_confirm_date,sale_order.partner_id,res_partner.name as customer_name, sale_order_line.id AS sales_line_id,stock_picking.date_done,stock_move.picking_type_id,(SELECT reserved_date FROM stock_move_details WHERE stock_move_details.picking_id = stock_picking.id ORDER BY stock_move_details.id DESC LIMIT 1) AS reserve_date,account_invoice.date_invoice as invoice_date, product_category.id  AS product_category_id,  product_category.name AS product_category_name, product_product.name_template as product_name, product_product.default_code as product_sku FROM stock_picking, stock_move, sale_order, sale_order_line, product_category, product_template, product_product,account_invoice,res_partner,mail_message WHERE stock_picking.id = stock_move.picking_id AND stock_picking.group_id = sale_order.procurement_group_id AND sale_order.partner_id= res_partner.id AND product_category.id = product_template.categ_id AND product_template.id = product_product.product_tmpl_id AND product_product.id = stock_move.product_id AND stock_move.product_id = sale_order_line.product_id AND sale_order.id = sale_order_line.order_id AND stock_picking.name = account_invoice.origin AND mail_message.model='sale.order' AND mail_message.record_name=sale_order.name AND mail_message.body LIKE '%Quotation confirmed%' AND sale_order.state NOT IN ('draft', 'sent', 'cancel', 'new_cus_approval_pending', 'approval_pending', 'check_pending') AND stock_picking.date_done >= '{0}' AND stock_picking.date_done <= '{1}'".format(st_date_time, end_date_time)
        self.cr.execute(tat_query)
        tat_query_objects = self.cr.dictfetchall()

        tat_partner_list = []
        tat_company_data_list = {}

        for tat_partner in tat_query_objects:
            tat_partner_list.append(tat_partner.get('partner_id'))
        partner_obj = self.pool("res.partner")

        for tat_p_items in partner_obj.browse(self.cr, self.uid, tat_partner_list, context=context):
            if tat_p_items.parent_id:
                tat_company_data_list[tat_p_items.id] = {'classification': tat_p_items.x_classification}
            else:
                tat_company_data_list[tat_p_items.id] = {'classification': tat_p_items.parent_id.x_classification if tat_p_items.parent_id.x_classification else tat_p_items.x_classification }

        objects=[]

        for tat_data_row in tat_query_objects:


            tat_partner_id = tat_data_row.get('partner_id')
            tat_company_list = tat_company_data_list.get(tat_partner_id)
            tat_data_row['classification'] = tat_company_list.get('classification')

            if tat_data_row.get('picking_type_id') not in incoming_ids:

                delivery_date = tat_data_row.get('date_done')
                reserve_date = tat_data_row.get('reserve_date')
                order_confirm_date = tat_data_row.get('order_confirm_date')
                order_date = tat_data_row.get('order_date')
                invoice_date = tat_data_row.get('invoice_date')

                order_date_obj = datetime.strptime(order_date.split(".")[0], '%Y-%m-%d %H:%M:%S')
                order_confirm_date_obj = datetime.strptime(order_confirm_date.split(".")[0], '%Y-%m-%d %H:%M:%S')

                # order_confirm_date_obj = datetime.strptime(order_confirm_date, "%Y-%m-%d %H:%M:%S")
                if order_confirm_date and order_date_obj:

                    order_to_confirmation_duration = order_confirm_date_obj - order_date_obj
                    days, seconds = order_to_confirmation_duration.days, order_to_confirmation_duration.seconds
                    order_to_confirmation_duration_hours = days * 24 + seconds // 3600
                    if order_to_confirmation_duration_hours > 0:
                        order_to_confirmation = order_to_confirmation_duration_hours
                    else:
                        order_to_confirmation = 0
                    tat_data_row['order_to_confirmation']=order_to_confirmation
                else:
                    tat_data_row['order_to_confirmation'] = '-'

                if reserve_date and order_confirm_date:

                    reserve_date_obj = datetime.strptime(reserve_date.split(".")[0], '%Y-%m-%d %H:%M:%S')
                    order_confirm_to_reserve_duration = reserve_date_obj - order_confirm_date_obj

                    days, seconds = order_confirm_to_reserve_duration.days, order_confirm_to_reserve_duration.seconds
                    order_confirm_to_reserve_duration_hours = days * 24 + seconds // 3600
                    if order_confirm_to_reserve_duration_hours > 0:
                        confirmation_to_reserve = order_confirm_to_reserve_duration_hours
                    else:
                        confirmation_to_reserve = 0
                    tat_data_row['confirmation_to_reserve']=confirmation_to_reserve
                else:
                    tat_data_row['confirmation_to_reserve'] = '-'


                if delivery_date  and reserve_date:
                    reserve_date_obj = datetime.strptime(reserve_date.split(".")[0], '%Y-%m-%d %H:%M:%S')
                    delivery_date_obj = datetime.strptime(delivery_date.split(".")[0], '%Y-%m-%d %H:%M:%S')
                    reserve_to_delivery_duration = delivery_date_obj - reserve_date_obj

                    days, seconds = reserve_to_delivery_duration.days, reserve_to_delivery_duration.seconds
                    reserve_to_delivery_duration_hours = days * 24 + seconds // 3600
                    if reserve_to_delivery_duration_hours > 0:
                        reserve_to_delivery = reserve_to_delivery_duration_hours
                    else:
                        reserve_to_delivery = 0

                    tat_data_row['reserve_to_delivery'] = reserve_to_delivery
                else:
                    tat_data_row['reserve_to_delivery'] = '-'

                if invoice_date  and delivery_date:
                    delivery_date_obj = datetime.strptime(delivery_date.split(".")[0], '%Y-%m-%d %H:%M:%S')
                    invoice_date_obj = datetime.strptime(invoice_date+' 00:00:00', '%Y-%m-%d %H:%M:%S')
                    delivery_to_invoice_duration = invoice_date_obj - delivery_date_obj

                    days, seconds = delivery_to_invoice_duration.days, delivery_to_invoice_duration.seconds
                    delivery_to_invoice_duration_hours = days * 24 + seconds // 3600
                    if delivery_to_invoice_duration_hours > 0:
                        delivery_to_invoice = delivery_to_invoice_duration_hours
                    else:
                        delivery_to_invoice = 0

                    tat_data_row['delivery_to_invoice'] = delivery_to_invoice
                else:
                    tat_data_row['delivery_to_invoice'] = '-'

                total_tat = 0


                if tat_data_row['order_to_confirmation']!='-':
                    total_tat += tat_data_row['order_to_confirmation']

                if tat_data_row['confirmation_to_reserve']!='-':
                    total_tat += tat_data_row['confirmation_to_reserve']

                if tat_data_row['reserve_to_delivery']!='-':
                    total_tat += tat_data_row['reserve_to_delivery']

                if tat_data_row['delivery_to_invoice']!='-':
                    total_tat += tat_data_row['delivery_to_invoice']

                tat_data_row['total_tat'] = total_tat

                objects.append(tat_data_row)


        
        for order in objects:

            c_specs = map(
                lambda x: self.render(x, self.col_specs_template, 'lines'),
                wanted_list)

            for list_data in c_specs:

                if str(list_data[0]) == str('order'):
                    list_data[4] = str(order['order_number'])

                if str(list_data[0]) == str('magento_order'):
                    list_data[4] = str(order['magento_order_number'])

                if str(list_data[0]) == str('customer_name'):
                     list_data[4] = str(order['customer_name'])

                if str(list_data[0]) == str('classification'):
                    list_data[4] = str(order['classification'])

                if str(list_data[0]) == str('product_name'):
                    list_data[4] = str(order['product_name'])

                if str(list_data[0]) == str('product_sku'):
                    list_data[4] = str(order['product_sku'])

                if str(list_data[0]) == str('product_category'):
                    list_data[4] = str(order['product_category_name'])

                if str(list_data[0]) == str('order_date'):
                     list_data[4] = str(order['order_date'])

                if str(list_data[0]) == str('confirm_order_date'):
                    list_data[4] = str(order['order_confirm_date'])

                if str(list_data[0]) == str('reserve_date'):
                    list_data[4] = str(order['reserve_date'])

                if str(list_data[0]) == str('date_done'):
                    list_data[4] = str(order['date_done'])

                if str(list_data[0]) == str('invoice_date'):
                     list_data[4] = str(order['invoice_date'])

                if str(list_data[0]) == str('order_to_confirmation'):
                    list_data[4] = str(order['order_to_confirmation'])

                if str(list_data[0]) == str('confirmation_to_reserve'):
                    list_data[4] = str(order['confirmation_to_reserve'])

                if str(list_data[0]) == str('reserve_to_delivery'):
                    list_data[4] = str(order['reserve_to_delivery'])

                if str(list_data[0]) == str('delivery_to_invoice'):
                    list_data[4] = str(order['delivery_to_invoice'])

                if str(list_data[0]) == str('total_tat'):
                    list_data[4] = str(order['total_tat'])


            row_data = self.xls_row_template(c_specs, [x[0] for x in c_specs])
            row_pos = self.xls_write_row(
                ws, row_pos, row_data, row_style=self.aml_cell_style)


tat_breakdown_xls('report.tat.breakdown.xls',
                    'sale.order',
                    parser=tat_breakdown_xls_parser)








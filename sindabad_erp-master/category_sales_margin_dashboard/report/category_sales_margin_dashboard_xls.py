import xlwt
import datetime
from openerp.osv import orm
from openerp.report import report_sxw
from openerp.addons.report_xls.report_xls import report_xls
from openerp.addons.report_xls.utils import rowcol_to_cell, _render
from openerp.tools.translate import translate, _
import logging
from itertools import groupby
from operator import itemgetter
from datetime import datetime,timedelta,date

_logger = logging.getLogger(__name__)


_ir_translation_name = 'category.sales.margin.dashboard.xls'


class category_sales_margin_dashboard_xls_parser(report_sxw.rml_parse):

    def __init__(self, cr, uid, name, context):

        super(category_sales_margin_dashboard_xls_parser, self).__init__(
            cr, uid, name, context=context)
        move_obj = self.pool.get('sale.order')
        self.context = context
        wanted_list = move_obj._report_xls_fields_category(cr, uid, context)
        template_changes = move_obj._report_xls_template(cr, uid, context)
        self.localcontext.update({
            'datetime': datetime,
            'wanted_list': wanted_list,
            'template_changes': template_changes,
            '_': self._,
        })

    def _(self, src):
        lang = self.context.get('lang', 'en_US')
        return translate(self.cr, _ir_translation_name, 'report', lang, src) \
            or src


class category_sales_margin_dashboard_xls(report_xls):

    def __init__(self, name, table, rml=False, parser=False, header=True,
                 store=False):

        super(category_sales_margin_dashboard_xls, self).__init__(
            name, table, rml, parser, header, store)

        # Cell Styles
        _xs = self.xls_styles
        # header
        rh_cell_format = _xs['bold'] + _xs['fill'] + _xs['borders_all']
        self.rh_cell_style = xlwt.easyxf(rh_cell_format)
        self.rh_cell_style_center = xlwt.easyxf(rh_cell_format + _xs['center'])
        self.rh_cell_style_right = xlwt.easyxf(rh_cell_format + _xs['right'])
        # lines
        aml_cell_format = _xs['borders_all']
        self.aml_cell_style = xlwt.easyxf(aml_cell_format)
        self.aml_cell_style_center = xlwt.easyxf(
            aml_cell_format + _xs['center'])
        self.aml_cell_style_date = xlwt.easyxf(
            aml_cell_format + _xs['left'],
            num_format_str=report_xls.date_format)
        self.aml_cell_style_decimal = xlwt.easyxf(
            aml_cell_format + _xs['right'],
            num_format_str=report_xls.decimal_format)
        # totals
        rt_cell_format = _xs['bold'] + _xs['fill'] + _xs['borders_all']
        self.rt_cell_style = xlwt.easyxf(rt_cell_format)
        self.rt_cell_style_right = xlwt.easyxf(rt_cell_format + _xs['right'])
        self.rt_cell_style_decimal = xlwt.easyxf(
            rt_cell_format + _xs['right'],
            num_format_str=report_xls.decimal_format)

        # XLS Template
        self.col_specs_template = {

            'category': {
                'header': [1, 20, 'text', _render("_('Category')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'ananta_order_value': {
                'header': [1, 20, 'text', _render("_('Ananta Order Value')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'general_order_value': {
                'header': [1, 20, 'text', _render("_('General Order value')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'corporate_order_value': {
                'header': [1, 20, 'text', _render("_('Corporate Order Value')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'retail_order_value': {
                'header': [1, 20, 'text', _render("_('Retail Order value')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'total_order_value': {
                'header': [1, 20, 'text', _render("_('Total Order value')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'comparison_period_total_order_value': {
                'header': [1, 20, 'text', _render("_('Comparison period Total Order Value')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'ananta_sale_value': {
                'header': [1, 20, 'text', _render("_('Ananta Sale Value')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'general_sale_value': {
                'header': [1, 20, 'text', _render("_('General Sale value')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'corporate_sale_value': {
                'header': [1, 20, 'text', _render("_('Corporate Sale Value')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'retail_sale_value': {
                'header': [1, 20, 'text', _render("_('Retail Sale value')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'total_sale_value': {
                'header': [1, 20, 'text', _render("_('Total Sale value')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'comparison_period_total_sale_value': {
                'header': [1, 20, 'text', _render("_('Comparison period Total Sale Value')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'ananta_margin_value': {
                'header': [1, 20, 'text', _render("_('Ananta Margin Value')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'general_margin_value': {
                'header': [1, 20, 'text', _render("_('General Margin value')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'corporate_margin_value': {
                'header': [1, 20, 'text', _render("_('Corporate Margin Value')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'retail_margin_value': {
                'header': [1, 20, 'text', _render("_('Retail Margin value')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'total_margin_value': {
                'header': [1, 20, 'text', _render("_('Total Margin value')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'comparison_period_total_margin_value': {
                'header': [1, 20, 'text', _render("_('Comparison period Total Margin Value')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'ananta_reserve_tat': {
                'header': [1, 20, 'text', _render("_('Ananta Reserve TAT')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'general_reserve_tat': {
                'header': [1, 20, 'text', _render("_('General Reserve TAT')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'corporate_reserve_tat': {
                'header': [1, 20, 'text', _render("_('Corporate Reserve TAT')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'retail_reserve_tat': {
                'header': [1, 20, 'text', _render("_('Retail Reserve TAT')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'total_reserve_tat': {
                'header': [1, 20, 'text', _render("_('Total Reserve TAT')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'comparison_period_total_reserve_tat_value': {
                'header': [1, 20, 'text', _render("_('Comparison period Total Reserve TAT')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'ananta_delivery_tat': {
                'header': [1, 20, 'text', _render("_('Ananta Delivery TAT')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'general_delivery_tat': {
                'header': [1, 20, 'text', _render("_('General Delivery TAT')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'corporate_delivery_tat': {
                'header': [1, 20, 'text', _render("_('Corporate Delivery TAT')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'retail_delivery_tat': {
                'header': [1, 20, 'text', _render("_('Retail Delivery TAT')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'total_delivery_tat': {
                'header': [1, 20, 'text', _render("_('Total Delivery TAT')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'comparison_period_total_delivery_tat_value': {
                'header': [1, 20, 'text', _render("_('Comparison period Total Delivery TAT')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

        }

    def daterange(self,date1, date2):
        for n in range(int((date2 - date1).days) + 1):
            yield date1 + timedelta(n)

    def generate_xls_report(self, _p, _xs, data, objects, wb):

        wanted_list = _p.wanted_list
        self.col_specs_template.update(_p.template_changes)
        _ = _p._


        context = self.context
        st_date = data.get('form').get('date_from')
        end_date = data.get('form').get('date_to')

        st_date_time = st_date + ' 00:00:00'
        end_date_time = end_date + ' 23:59:59'

        com_date_from = data.get('form').get('com_date_from')
        com_date_to = data.get('form').get('com_date_to')

        com_date_time_from = com_date_from + ' 00:00:00'
        com_date_time_to = com_date_to + ' 23:59:59'

        context['start_date'] = st_date
        context['end_date'] = end_date

        context['com_date_from'] = com_date_from
        context['com_date_to'] = com_date_to

        self.context = context
        date_range = _("Date: %s to %s" % (st_date, end_date))

        # report_name = objects[0]._description or objects[0]._name
        report_name = _("Sales Margin Dashboard")
        ws = wb.add_sheet(report_name[:31])
        ws.panes_frozen = True
        ws.remove_splits = True
        ws.portrait = 0  # Landscape
        ws.fit_width_to_pages = 1
        row_pos = 0

        # set print header/footer
        ws.header_str = self.xls_headers['standard']
        ws.footer_str = self.xls_footers['standard']

        # Title
        cell_style = xlwt.easyxf(_xs['xls_title'])
        c_specs = [
            ('report_name', 2, 2, 'text', report_name),
            ('date_range', 1, 1, 'text', date_range),
        ]
        row_data = self.xls_row_template(c_specs, ['report_name', 'date_range'])
        row_pos = self.xls_write_row(
            ws, row_pos, row_data, row_style=cell_style)
        row_pos += 1

        c_specs = map(lambda x: self.render(x, self.col_specs_template, 'header', render_space={'_': _p._}),wanted_list)
        row_data = self.xls_row_template(c_specs, [x[0] for x in c_specs])
        row_pos = self.xls_write_row(
            ws, row_pos, row_data, row_style=self.rh_cell_style,
            set_column_size=True)
        ws.set_horz_split_pos(row_pos)

        product_category_query = "SELECT id,name FROM product_category"

        self.cr.execute(product_category_query)

        product_category = []
        product_category_dict = {}

        for categ in self.cr.dictfetchall():
            product_category.append(categ['id'])
            product_category_dict.update({categ['id']:categ['name']})

        in_query = "select id from  stock_picking_type where code='incoming'"
        self.cr.execute(in_query)
        in_objects = self.cr.dictfetchall()

        incoming_ids = [in_item.get('id') for in_item in in_objects]

        comparison_period_sales_query = "SELECT so.amount_total, product_category.id as product_category_id, product_category.name as product_category_name,so.partner_id FROM sale_order so, sale_order_line,product_category, product_template, product_product WHERE so.id = sale_order_line.order_id AND product_category.id = product_template.categ_id AND product_template.id = product_product.product_tmpl_id AND product_product.id = sale_order_line.product_id AND so.state NOT IN ('draft', 'sent', 'cancel', 'new_cus_approval_pending', 'approval_pending', 'check_pending') " \
                      "AND so.date_confirm >= %s and so.date_confirm <= %s"

        self.cr.execute(comparison_period_sales_query, (com_date_from, com_date_to))
        comparison_period_sales_object = self.cr.dictfetchall()

        comparison_period_result = {}
        for k, g in groupby(sorted(comparison_period_sales_object, key=lambda x: (x['product_category_id'])), lambda x: (x['product_category_id'])):
            temp_res = dict()
            temp_res["comparison_period_total_order_value"] = sum(itm["amount_total"] for itm in g)
            comparison_period_result.update({k:temp_res})

        comparison_period_delivery_query = "SELECT stock_picking.picking_type_id,sale_order.partner_id,(SELECT sum((sq.qty * sm.price_unit)) AS total FROM stock_quant_move_rel AS sqm, stock_move AS sm, stock_quant AS sq WHERE sqm.quant_id IN (SELECT sq2.id FROM stock_quant_move_rel AS sqmr2, stock_quant AS sq2 WHERE sqmr2.move_id = stock_move.id AND sqmr2.quant_id = sq2.id) AND sqm.move_id = sm.id AND sm.purchase_line_id > 0 AND sq.id = sqm.quant_id GROUP BY sq.product_id) AS total_price,(stock_move.product_uom_qty * sale_order_line.price_unit) AS total_sale,product_category.id as product_category_id,product_category.name as product_category_name FROM stock_move, stock_picking, sale_order_line, sale_order, product_category, product_template, product_product WHERE stock_move.picking_id = stock_picking.id AND stock_move.sale_line_id = sale_order_line.id AND product_category.id = product_template.categ_id AND product_template.id = product_product.product_tmpl_id AND product_product.id = stock_move.product_id AND sale_order.id = sale_order_line.order_id AND stock_picking.state = 'done' AND stock_picking.date_done >= %s AND stock_picking.date_done <= %s"
        self.cr.execute(comparison_period_delivery_query, (com_date_time_from, com_date_time_to))
        comparison_period_delivery_objects = self.cr.dictfetchall()

        for com_delivery_data_row in comparison_period_delivery_objects:

            if com_delivery_data_row.get('picking_type_id') in incoming_ids:

                com_delivery_data_row['total_price'] = (
                                                        com_delivery_data_row.get(
                                                          'total_price') if com_delivery_data_row.get(
                                                          'total_price') is not None else 0) * (-1)
                com_delivery_data_row['total_sale'] = com_delivery_data_row.get('total_sale') * (-1)

            else:

                com_delivery_data_row['total_price'] = com_delivery_data_row.get(
                                                          'total_price') if com_delivery_data_row.get(
                                                          'total_price') is not None else 0
                com_delivery_data_row['total_sale'] = com_delivery_data_row.get('total_sale')

        comparison_period_delivery_result = {}

        for k1, g1 in groupby(sorted(comparison_period_delivery_objects, key=lambda x: (x['product_category_id'])), lambda x: (x['product_category_id'])):
            temp_res1 = dict()
            total_sale_val = 0
            total_price_val = 0
            for itm1 in g1:
                total_sale_val += itm1["total_sale"]
                total_price_val += itm1["total_price"]

            temp_res1["total_sale"] = total_sale_val
            temp_res1["comparison_period_total_sale_value"] = total_sale_val
            temp_res1["total_price"] = total_price_val
            total_profict_sale = round((temp_res1["total_sale"] - round(temp_res1["total_price"], 2)), 2)

            if total_profict_sale > 0 and temp_res1["total_sale"] > 0:
                comparison_period_margin = round(((total_profict_sale / temp_res1["total_sale"]) * 100), 2)
            else:
                comparison_period_margin = 0

            temp_res1["comparison_period_total_margin_value"]= comparison_period_margin

            comparison_period_delivery_result.update({k1:temp_res1})

        comparison_period_tat_query = "SELECT sale_order.id AS sale_order_id,sale_order.name AS sale_order_number,sale_order.date_order AS order_date,sale_order.date_confirm,mail_message.create_date AS order_confirm_date, sale_order.partner_id, sale_order_line.id AS sales_line_id,stock_picking.date_done, stock_move.picking_type_id, (SELECT reserved_date FROM stock_move_details WHERE stock_move_details.picking_id = stock_picking.id ORDER BY stock_move_details.id DESC LIMIT 1) AS reserve_date,product_category.id AS product_category_id,product_category.name AS product_category_name FROM stock_picking, stock_move, sale_order, sale_order_line,product_category, product_template, product_product,mail_message WHERE stock_picking.id = stock_move.picking_id AND stock_picking.group_id = sale_order.procurement_group_id AND product_category.id = product_template.categ_id AND product_template.id = product_product.product_tmpl_id AND product_product.id = stock_move.product_id AND stock_move.product_id = sale_order_line.product_id AND sale_order.id = sale_order_line.order_id AND mail_message.model='sale.order' AND mail_message.record_name=sale_order.name AND mail_message.body LIKE ('%Quotation confirmed%') AND sale_order.state NOT IN ('draft', 'sent', 'cancel', 'new_cus_approval_pending', 'approval_pending', 'check_pending') AND stock_picking.date_done >= '{0}' AND stock_picking.date_done <= '{1}'".format(com_date_time_from, com_date_time_to)

        self.cr.execute(comparison_period_tat_query)
        comparison_period_tat_query_object = self.cr.dictfetchall()


        comparison_period_tat_data_list=[]

        for comparison_period_tat_data_row in comparison_period_tat_query_object:

            if comparison_period_tat_data_row.get('picking_type_id') not in incoming_ids:

                deli_date = comparison_period_tat_data_row.get('date_done')
                rese_date = comparison_period_tat_data_row.get('reserve_date')
                ord_confirm_date = comparison_period_tat_data_row.get('order_confirm_date')


                if ord_confirm_date:

                    ord_confirm_date_obj = datetime.strptime(ord_confirm_date.split(".")[0],
                                                               "%Y-%m-%d %H:%M:%S")

                    if rese_date :

                        rese_date_obj = datetime.strptime(rese_date.split(".")[0],"%Y-%m-%d %H:%M:%S")
                        reserve_duration = rese_date_obj - ord_confirm_date_obj

                        days, seconds = reserve_duration.days, reserve_duration.seconds
                        reserve_hours = days * 24 + seconds // 3600
                        if reserve_hours > 0:
                            rese_tat = reserve_hours
                        else:
                            rese_tat= 0
                        comparison_period_tat_data_row['rese_tat_count'] = 1
                    else:
                        rese_tat = 0
                        comparison_period_tat_data_row['rese_tat_count'] = 0


                    if deli_date:
                        deli_date_obj = datetime.strptime(deli_date.split(".")[0],"%Y-%m-%d %H:%M:%S")
                        delivery_duration = deli_date_obj - ord_confirm_date_obj
                        days, seconds = delivery_duration.days, delivery_duration.seconds
                        delivery_hours = days * 24 + seconds // 3600
                        if delivery_hours > 0:
                            deli_tat = delivery_hours
                        else:
                            deli_tat= 0
                        comparison_period_tat_data_row['deli_tat_count'] = 1
                    else:
                        deli_tat = 0
                        comparison_period_tat_data_row['deli_tat_count'] = 0
                else:
                    rese_tat = 0
                    comparison_period_tat_data_row['rese_tat_count'] = 0
                    deli_tat = 0
                    comparison_period_tat_data_row['deli_tat_count'] = 0

                comparison_period_tat_data_row['reserve_tat'] = rese_tat
                comparison_period_tat_data_row['delivery_tat'] = deli_tat

                comparison_period_tat_data_list.append(comparison_period_tat_data_row)

        comparison_period_tat_result = {}

        for k2, g2 in groupby(sorted(comparison_period_tat_data_list, key=lambda x4: (x4['product_category_id'])), lambda x4: (x4['product_category_id'])):
            temp_res2 = dict()
            rese_tat_sum = 0
            deliv_tat_sum = 0
            com_rese_tat_len_sum = 0
            com_deli_tat_len_sum = 0

            for it2 in g2:
                rese_tat_sum+= it2["reserve_tat"]
                deliv_tat_sum+= it2["delivery_tat"]
                com_rese_tat_len_sum += it2["rese_tat_count"]
                com_deli_tat_len_sum += it2["deli_tat_count"]

            # temp_res2["delivery_tat"] = deliv_tat_sum
            # temp_res2["reserve_tat"] = rese_tat_sum
            # temp_res2["tat_len"] = com_tat_len

            temp_res2["comparison_period_total_reserve_tat_value"] = rese_tat_sum / com_rese_tat_len_sum if rese_tat_sum > 0 and com_rese_tat_len_sum > 0 else 0
            temp_res2["comparison_period_total_delivery_tat_value"] = deliv_tat_sum / com_deli_tat_len_sum if deliv_tat_sum > 0 and com_deli_tat_len_sum > 0 else 0

            comparison_period_tat_result.update({k2:temp_res2})


        sales_query = "SELECT so.amount_total, product_category.id as product_category_id, product_category.name as product_category_name,so.partner_id FROM sale_order so, sale_order_line,product_category, product_template, product_product WHERE so.id = sale_order_line.order_id AND product_category.id = product_template.categ_id AND product_template.id = product_product.product_tmpl_id AND product_product.id = sale_order_line.product_id AND so.state NOT IN ('draft', 'sent', 'cancel', 'new_cus_approval_pending', 'approval_pending', 'check_pending') " \
                      "AND so.date_confirm >= %s and so.date_confirm <= %s "

        self.cr.execute(sales_query, (st_date,end_date))
        sale_order_object = self.cr.dictfetchall()

        final_company_list = {}

        sale_partner_list=[]

        partner_obj = self.pool("res.partner")

        for sale_partner_id in sale_order_object:
            sale_partner_list.append(sale_partner_id.get('partner_id'))

        p_obj = partner_obj.browse(self.cr, self.uid, sale_partner_list, context=context)

        for p_items in p_obj:
            if p_items.parent_id:
                final_company_list[p_items.id] = {
                    'classification': p_items.x_classification
                }
            else:
                final_company_list[p_items.id] = {'classification': p_items.parent_id.x_classification if p_items.parent_id.x_classification else p_items.x_classification,}

        objects = []


        for so_item in sale_order_object:

            partner_id = so_item.get('partner_id')
            so_classification = final_company_list.get(partner_id)


            if so_classification.get('classification') is not False and so_classification.get('classification') is not None:

                got_class = so_classification.get('classification').lower()

                if 'ananta' in got_class:
                    so_item['classification']= 'ananta'

                elif 'corporate' in got_class:
                    so_item['classification'] = 'corporate'

                elif 'general' in got_class:
                    so_item['classification'] = 'general'

                else:
                    so_item['classification'] = 'retail'

            else:
                so_item['classification'] = 'retail'

        order_grouper = itemgetter('product_category_id','product_category_name','classification')
        result = []

        for key, grp in groupby(sorted(sale_order_object, key=order_grouper), order_grouper):

            temp_dict = dict(zip(["product_category_id","product_category_name","classification"], key))
            temp_dict["amount_total"] = sum(item["amount_total"] for item in grp)
            result.append(temp_dict)

        order_data_dict={}
        for i in result:
            if order_data_dict.get(i['product_category_id']) is not None and order_data_dict.get(i['product_category_id'])  is not False:
                data = order_data_dict[i['product_category_id']]
                if i['classification'] == 'ananta':
                    data['ananta_order_value'] = i['amount_total']
                if i['classification'] == 'general':
                    data['general_order_value'] = i['amount_total']
                if i['classification'] == 'corporate':
                    data['corporate_order_value'] = i['amount_total']
                if i['classification'] == 'retail':
                    data['retail_order_value'] = i['amount_total']

            else:
                order_data_dict.update({
                    i['product_category_id']: {
                        'product_category_name': i['product_category_name'],
                        'ananta_order_value': i['amount_total'] if i['classification'] == 'ananta' else 0.0,
                        'general_order_value': i['amount_total'] if i['classification'] == 'general' else 0.0,
                        'corporate_order_value': i['amount_total'] if i['classification'] == 'corporate' else 0.0,
                        'retail_order_value': i['amount_total'] if i['classification'] == 'retail' else 0.0,
                    }
                })

        delivery_query ="SELECT stock_picking.picking_type_id,sale_order.partner_id,(SELECT sum((sq.qty * sm.price_unit)) AS total FROM stock_quant_move_rel AS sqm, stock_move AS sm, stock_quant AS sq WHERE sqm.quant_id IN (SELECT sq2.id FROM stock_quant_move_rel AS sqmr2, stock_quant AS sq2 WHERE sqmr2.move_id = stock_move.id AND sqmr2.quant_id = sq2.id) AND sqm.move_id = sm.id AND sm.purchase_line_id > 0 AND sq.id = sqm.quant_id GROUP BY sq.product_id) AS total_price,(stock_move.product_uom_qty * sale_order_line.price_unit) AS total_sale,product_category.id as product_category_id,product_category.name as product_category_name FROM stock_move, stock_picking, sale_order_line, sale_order, product_category, product_template, product_product WHERE stock_move.picking_id = stock_picking.id AND stock_move.sale_line_id = sale_order_line.id AND product_category.id = product_template.categ_id AND product_template.id = product_product.product_tmpl_id AND product_product.id = stock_move.product_id AND sale_order.id = sale_order_line.order_id AND stock_picking.state = 'done' AND stock_picking.date_done >= %s AND stock_picking.date_done <= %s"
        self.cr.execute(delivery_query, (st_date_time, end_date_time))
        delivery_objects = self.cr.dictfetchall()

        delivery_partner_list = []
        delivery_final_company_list = {}

        for delivery_partner in delivery_objects:
            delivery_partner_list.append(delivery_partner.get('partner_id'))

        for delivery_p_items in partner_obj.browse(self.cr, self.uid, delivery_partner_list, context=context):
            if delivery_p_items.parent_id:
                delivery_final_company_list[delivery_p_items.id] = {'classification': delivery_p_items.x_classification}
            else:
                delivery_final_company_list[delivery_p_items.id] = {'classification': delivery_p_items.parent_id.x_classification if delivery_p_items.parent_id.x_classification else delivery_p_items.x_classification }

        for delivery_data_row in delivery_objects:

            delivery_partner_id = delivery_data_row.get('partner_id')
            delivery_company_list = delivery_final_company_list.get(delivery_partner_id)
            total_sale = delivery_data_row.get('total_sale')

            if delivery_company_list.get('classification') is not False and delivery_company_list.get('classification') is not None:

                got_classification = delivery_company_list.get('classification').lower()

                if 'ananta' in got_classification:
                    delivery_data_row['classification'] = 'ananta'
                    if delivery_data_row.get('picking_type_id') in incoming_ids:
                        delivery_data_row['total_price'] = (
                                                              delivery_data_row.get(
                                                                  'total_price') if delivery_data_row.get(
                                                                  'total_price') is not None else 0) * (-1)
                        delivery_data_row['total_sale'] = delivery_data_row.get('total_sale') * (-1)

                    else:

                        delivery_data_row['total_price'] = delivery_data_row.get(
                                                                  'total_price') if delivery_data_row.get(
                                                                  'total_price') is not None else 0
                        delivery_data_row['total_sale'] = delivery_data_row.get('total_sale')

                elif 'corporate' in got_classification:
                    delivery_data_row['classification'] = 'corporate'
                    if delivery_data_row.get('picking_type_id') in incoming_ids:

                        delivery_data_row['total_price'] = (
                            delivery_data_row.get('total_price') if delivery_data_row.get(
                                'total_price') is not None else 0) *(-1)
                        delivery_data_row['total_sale']= delivery_data_row.get('total_sale') *(-1)
                    else:

                        delivery_data_row['total_price'] = delivery_data_row.get(
                            'total_price') if delivery_data_row.get(
                            'total_price') is not None else 0
                        delivery_data_row['total_sale'] = delivery_data_row.get('total_sale')

                elif 'general' in got_classification:
                    delivery_data_row['classification'] = 'general'

                    if delivery_data_row.get('picking_type_id') in incoming_ids:

                        delivery_data_row['total_price'] = (
                                                              delivery_data_row.get(
                                                                  'total_price') if delivery_data_row.get(
                                                                  'total_price') is not None else 0) * (-1)
                        delivery_data_row['total_sale'] = delivery_data_row.get('total_sale') * (-1)
                    else:
                        delivery_data_row['total_price'] = delivery_data_row.get(
                            'total_price') if delivery_data_row.get(
                            'total_price') is not None else 0
                        delivery_data_row['total_sale'] = delivery_data_row.get('total_sale')

                else:
                    delivery_data_row['classification'] = 'retail'
                    if delivery_data_row.get('picking_type_id') in incoming_ids:
                        delivery_data_row['total_price'] = (
                                                              delivery_data_row.get(
                                                                  'total_price') if delivery_data_row.get(
                                                                  'total_price') is not None else 0) * (-1)
                        delivery_data_row['total_sale'] = delivery_data_row.get('total_sale') * (-1)
                    else:
                        delivery_data_row['total_price'] = delivery_data_row.get(
                            'total_price') if delivery_data_row.get(
                            'total_price') is not None else 0
                        delivery_data_row['total_sale'] = delivery_data_row.get('total_sale')
            else:
                delivery_data_row['classification'] = 'retail'
                if delivery_data_row.get('picking_type_id') in incoming_ids:
                    delivery_data_row['total_price'] = (
                                                           delivery_data_row.get(
                                                               'total_price') if delivery_data_row.get(
                                                               'total_price') is not None else 0) * (-1)
                    delivery_data_row['total_sale'] = delivery_data_row.get('total_sale') * (-1)
                else:
                    delivery_data_row['total_price'] = delivery_data_row.get(
                        'total_price') if delivery_data_row.get(
                        'total_price') is not None else 0
                    delivery_data_row['total_sale'] = delivery_data_row.get('total_sale')


        # delivery_grouper = itemgetter("product_category_id", "product_category_name", "classification")
        delivery_grouper = lambda x: (x['product_category_id'],x['product_category_name'],x['classification'])
        delivery_result = []

        for key1, grp1 in groupby(sorted(delivery_objects, key=delivery_grouper), delivery_grouper):
            temp_dict1 = dict(zip(["product_category_id", "product_category_name", "classification"], key1))
            total_sale_val = 0
            total_price_val = 0
            for item1 in grp1:
                total_sale_val+= item1["total_sale"]
                total_price_val+= item1["total_price"]

            temp_dict1["total_sale"] = total_sale_val
            temp_dict1["total_price"] = total_price_val
            delivery_result.append(temp_dict1)

        delivery_data_dict = {}

        for delivery_row in delivery_result:

            ananta_sale_value = 0.0
            ananta_cost_value = 0.0
            general_sale_value = 0.0
            general_cost_value = 0.0
            corporate_sale_value = 0.0
            corporate_cost_value = 0.0
            retail_sale_value = 0.0
            retail_cost_value = 0.0
            ananta_margin = 0.0
            general_margin = 0.0
            corporate_margin = 0.0
            retail_margin = 0.0

            if delivery_row['classification'] == 'ananta':
                ananta_sale_value = delivery_row['total_sale']
                ananta_cost_value = delivery_row['total_price']
                total_ananta_sale = round(ananta_sale_value, 2)
                total_profict_ananta_sale = round((total_ananta_sale - round(ananta_cost_value, 2)), 2)


                if total_profict_ananta_sale > 0 and total_ananta_sale > 0:
                    ananta_margin = round(((total_profict_ananta_sale / total_ananta_sale) * 100), 2)

            if delivery_row['classification'] == 'general':
                general_sale_value = delivery_row['total_sale']
                general_cost_value = delivery_row['total_price']
                total_general_sale = round(general_sale_value, 2)
                total_profict_general_sale = round((total_general_sale - round(general_cost_value, 2)), 2)


                if total_profict_general_sale > 0 and total_general_sale > 0:
                    general_margin = round(((total_profict_general_sale / total_general_sale) * 100), 2)

            if delivery_row['classification'] == 'corporate':
                corporate_sale_value = delivery_row['total_sale']
                corporate_cost_value = delivery_row['total_price']
                total_corporate_sale = round(corporate_sale_value, 2)
                total_profict_corporate_sale = round((total_corporate_sale - round(corporate_cost_value, 2)), 2)

                if total_profict_corporate_sale > 0 and total_corporate_sale > 0:
                    corporate_margin = round(((total_profict_corporate_sale / total_corporate_sale) * 100), 2)

            if delivery_row['classification'] == 'retail':
                retail_sale_value = delivery_row['total_sale']
                retail_cost_value = delivery_row['total_price']
                total_retail_sale = round(retail_sale_value, 2)
                total_profict_retail_sale = round((total_retail_sale - round(retail_cost_value, 2)), 2)

                if total_profict_retail_sale > 0 and total_retail_sale > 0:
                    retail_margin = round(((total_profict_retail_sale / total_retail_sale) * 100), 2)


            if delivery_data_dict.get(delivery_row['product_category_id']) is not None and delivery_data_dict.get(
                    delivery_row['product_category_id']) is not False:
                data = delivery_data_dict[delivery_row['product_category_id']]
                if delivery_row['classification'] == 'ananta':
                    data['ananta_sale_value'] = ananta_sale_value
                    data['ananta_cost_value'] = ananta_cost_value
                    data['ananta_margin'] = ananta_margin
                if delivery_row['classification'] == 'general':
                    data['general_sale_value'] = general_sale_value
                    data['general_cost_value'] = general_cost_value
                    data['general_margin'] = general_margin
                if delivery_row['classification'] == 'corporate':
                    data['corporate_sale_value'] = corporate_sale_value
                    data['corporate_cost_value'] = corporate_cost_value
                    data['corporate_margin'] = corporate_margin
                if delivery_row['classification'] == 'retail':
                    data['retail_sale_value'] = retail_sale_value
                    data['retail_cost_value'] = retail_cost_value
                    data['retail_margin'] = retail_margin

            else:

                delivery_data_dict.update({
                    delivery_row['product_category_id']: {
                    'ananta_sale_value': ananta_sale_value ,
                    'ananta_cost_value': ananta_cost_value ,
                    'ananta_margin': ananta_margin ,
                    'general_sale_value': general_sale_value,
                    'general_cost_value': general_cost_value ,
                    'general_margin': general_margin,
                    'corporate_sale_value':corporate_sale_value ,
                    'corporate_cost_value': corporate_cost_value ,
                    'corporate_margin': corporate_margin ,
                    'retail_sale_value': retail_sale_value ,
                    'retail_cost_value':retail_cost_value,
                    'retail_margin':retail_margin
                }

                })


        tat_query = "SELECT sale_order.id AS sale_order_id,sale_order.date_order AS order_date,sale_order.date_confirm,mail_message.create_date AS order_confirm_date, sale_order.partner_id, sale_order_line.id AS sales_line_id,stock_picking.date_done, stock_move.picking_type_id, (SELECT reserved_date FROM stock_move_details WHERE stock_move_details.picking_id = stock_picking.id ORDER BY stock_move_details.id DESC LIMIT 1) AS reserve_date,product_category.id AS product_category_id,product_category.name AS product_category_name FROM stock_picking, stock_move, sale_order, sale_order_line,product_category, product_template, product_product,mail_message WHERE stock_picking.id = stock_move.picking_id AND stock_picking.group_id = sale_order.procurement_group_id AND product_category.id = product_template.categ_id AND product_template.id = product_product.product_tmpl_id AND product_product.id = stock_move.product_id AND stock_move.product_id = sale_order_line.product_id AND sale_order.id = sale_order_line.order_id AND mail_message.model='sale.order' AND mail_message.record_name=sale_order.name AND mail_message.body LIKE '%Quotation confirmed%' AND sale_order.state NOT IN ('draft', 'sent', 'cancel', 'new_cus_approval_pending', 'approval_pending', 'check_pending') AND stock_picking.date_done >= '{0}' AND stock_picking.date_done <= '{1}'".format(st_date_time, end_date_time)
        self.cr.execute(tat_query)
        tat_query_objects = self.cr.dictfetchall()

        tat_partner_list = []
        tat_company_data_list = {}

        for tat_partner in tat_query_objects:
            tat_partner_list.append(tat_partner.get('partner_id'))

        for tat_p_items in partner_obj.browse(self.cr, self.uid, tat_partner_list, context=context):
            if tat_p_items.parent_id:
                tat_company_data_list[tat_p_items.id] = {'classification': tat_p_items.x_classification}
            else:
                tat_company_data_list[tat_p_items.id] = {'classification': tat_p_items.parent_id.x_classification if tat_p_items.parent_id.x_classification else tat_p_items.x_classification }

        tat_data_list=[]

        for tat_data_row in tat_query_objects:

            tat_partner_id = tat_data_row.get('partner_id')
            tat_company_list = tat_company_data_list.get(tat_partner_id)

            if tat_data_row.get('picking_type_id') not in incoming_ids:

                delivery_date = tat_data_row.get('date_done')
                reserve_date = tat_data_row.get('reserve_date')
                order_confirm_date = tat_data_row.get('order_confirm_date')
                if order_confirm_date:
                    order_confirm_date_obj = datetime.strptime(order_confirm_date.split(".")[0], "%Y-%m-%d %H:%M:%S")

                    if reserve_date :

                        reserve_date_obj = datetime.strptime(reserve_date.split(".")[0], "%Y-%m-%d %H:%M:%S")
                        order_confirm_to_reserve_duration = reserve_date_obj - order_confirm_date_obj

                        days, seconds = order_confirm_to_reserve_duration.days, order_confirm_to_reserve_duration.seconds
                        hours = days * 24 + seconds // 3600
                        if hours > 0:
                            reserve_tat = hours
                        else:
                            reserve_tat = 0

                        tat_data_row['reserve_date_count']=1
                    else:
                        reserve_tat = 0
                        tat_data_row['reserve_date_count'] = 0

                    if delivery_date:
                        delivery_date_obj = datetime.strptime(delivery_date.split(".")[0], "%Y-%m-%d %H:%M:%S")
                        order_confirm_to_delivery_duration = delivery_date_obj - order_confirm_date_obj
                        days, seconds = order_confirm_to_delivery_duration.days, order_confirm_to_delivery_duration.seconds
                        hours = days * 24 + seconds// 3600
                        if hours > 0:
                            delivery_tat = hours
                        else:
                            delivery_tat = 0
                        tat_data_row['delivery_date_count'] =1
                    else:
                        delivery_tat = 0
                        tat_data_row['delivery_date_count'] =0

                else:
                    reserve_tat = 0
                    tat_data_row['reserve_date_count'] = 0
                    delivery_tat = 0
                    tat_data_row['delivery_date_count'] = 0

                if tat_company_list.get('classification') is not False and tat_company_list.get(
                    'classification') is not None:
                    tat_partner_classification = tat_company_list.get('classification').lower()


                    if 'ananta' in tat_partner_classification:
                        tat_data_row['classification'] = 'ananta'

                    elif 'corporate' in tat_partner_classification:
                        tat_data_row['classification'] = 'corporate'

                    elif 'general' in tat_partner_classification:
                        tat_data_row['classification'] = 'general'

                    else:
                        tat_data_row['classification'] = 'retail'

                    # tat_data_row['reserve_tat'] = reserve_tat
                    # tat_data_row['delivery_tat'] = delivery_tat

                else:
                    tat_data_row['classification'] = 'retail'

                tat_data_row['reserve_tat'] = reserve_tat
                tat_data_row['delivery_tat'] = delivery_tat


                tat_data_list.append(tat_data_row)

        tat_grouper = lambda x2: (x2['product_category_id'],x2['product_category_name'],x2['classification'])
        tat_result = []

        for key2, grp2 in groupby(sorted(tat_data_list, key=tat_grouper), tat_grouper):
            temp_dict2 = dict(zip(["product_category_id", "product_category_name", "classification"], key2))
            reserve_tat_sum = 0
            delivery_tat_sum = 0
            delivery_date_len_sum = 0
            reserve_date_len_sum = 0

            for item2 in grp2:
                reserve_tat_sum+= item2["reserve_tat"]
                delivery_tat_sum+= item2["delivery_tat"]
                delivery_date_len_sum += item2["delivery_date_count"]
                reserve_date_len_sum +=  item2["reserve_date_count"]


            temp_dict2["delivery_tat"] = delivery_tat_sum
            temp_dict2["reserve_tat"] = reserve_tat_sum
            temp_dict2["delivery_date_len"] = delivery_date_len_sum
            temp_dict2["reserve_date_len"] = reserve_date_len_sum
            tat_result.append(temp_dict2)


        tat_data_dict = {}

        for tat_row in tat_result:

            ananta_reserve_tat = 0
            ananta_delivery_tat = 0
            general_reserve_tat = 0
            general_delivery_tat = 0
            corporate_reserve_tat = 0
            corporate_delivery_tat = 0
            retail_reserve_tat = 0
            retail_delivery_tat = 0


            if tat_row['classification'] == 'ananta':
                ananta_reserve_tat = tat_row['reserve_tat']/tat_row['reserve_date_len'] if tat_row['reserve_tat'] >0 and tat_row['reserve_date_len'] >0 else 0
                ananta_delivery_tat = tat_row['delivery_tat']/tat_row['delivery_date_len'] if tat_row['delivery_tat'] >0 and tat_row['delivery_date_len'] >0 else 0


            if tat_row['classification'] == 'general':
                general_reserve_tat = tat_row['reserve_tat']/tat_row['reserve_date_len'] if tat_row['reserve_tat'] >0 and tat_row['reserve_date_len'] >0 else 0
                general_delivery_tat = tat_row['delivery_tat']/tat_row['delivery_date_len'] if tat_row['delivery_tat'] >0 and tat_row['delivery_date_len'] >0 else 0


            if tat_row['classification'] == 'corporate':
                corporate_reserve_tat = tat_row['reserve_tat']/tat_row['reserve_date_len'] if tat_row['reserve_tat'] >0 and tat_row['reserve_date_len'] >0 else 0
                corporate_delivery_tat = tat_row['delivery_tat']/tat_row['delivery_date_len'] if tat_row['delivery_tat'] >0 and tat_row['delivery_date_len'] >0 else 0


            if tat_row['classification'] == 'retail':
                retail_reserve_tat = tat_row['reserve_tat']/tat_row['reserve_date_len'] if tat_row['reserve_tat'] >0 and tat_row['reserve_date_len'] >0 else 0
                retail_delivery_tat = tat_row['delivery_tat']/tat_row['delivery_date_len'] if tat_row['delivery_tat'] >0 and tat_row['delivery_date_len'] >0 else 0


            if tat_data_dict.get(tat_row['product_category_id']) is not None and tat_data_dict.get(
                    tat_row['product_category_id']) is not False:
                data = tat_data_dict[tat_row['product_category_id']]
                if tat_row['classification'] == 'ananta':
                    data['ananta_reserve_tat'] = ananta_reserve_tat
                    data['ananta_delivery_tat'] = ananta_delivery_tat
                if tat_row['classification'] == 'general':
                    data['general_reserve_tat'] = general_reserve_tat
                    data['general_delivery_tat'] = general_delivery_tat
                if tat_row['classification'] == 'corporate':
                    data['corporate_reserve_tat'] = corporate_reserve_tat
                    data['corporate_delivery_tat'] = corporate_delivery_tat
                if tat_row['classification'] == 'retail':
                    data['retail_reserve_tat'] = retail_reserve_tat
                    data['retail_delivery_tat'] = retail_delivery_tat

            else:

                tat_data_dict.update({
                    tat_row['product_category_id']: {
                    'ananta_reserve_tat': ananta_reserve_tat ,
                    'ananta_delivery_tat': ananta_delivery_tat ,
                    'general_reserve_tat': general_reserve_tat,
                    'general_delivery_tat': general_delivery_tat ,
                    'corporate_reserve_tat':corporate_reserve_tat ,
                    'corporate_delivery_tat': corporate_delivery_tat ,
                    'retail_reserve_tat': retail_reserve_tat ,
                    'retail_delivery_tat':retail_delivery_tat
                }})

        res={}

        for cate_id in product_category:
            res[cate_id] = {
             'ananta_cost_value': 0,
             'ananta_margin':0,
             'ananta_order_value': 0,
             'ananta_sale_value': 0,
             'ananta_reserve_tat': 0,
             'ananta_delivery_tat': 0,
             'comparison_period_total_margin_value': 0,
             'comparison_period_total_order_value': 0,
             'comparison_period_total_sale_value': 0,
             'comparison_period_total_reserve_tat_value': 0,
             'comparison_period_total_delivery_tat_value': 0,
             'corporate_cost_value': 0,
             'corporate_margin':0,
             'corporate_order_value': 0,
             'corporate_sale_value':0,
             'corporate_reserve_tat':0,
             'corporate_delivery_tat':0,
             'general_cost_value': 0,
             'general_margin':0,
             'general_order_value': 0,
             'general_sale_value': 0,
             'general_reserve_tat': 0,
             'general_delivery_tat': 0,
             'product_category_name': product_category_dict[cate_id],
             'retail_cost_value': 0,
             'retail_margin': 0,
             'retail_order_value':0,
             'retail_sale_value': 0,
             'retail_reserve_tat': 0,
             'retail_delivery_tat': 0,
             'total_price': 0,
             'total_sale': 0,
             'total_reserve_tat': 0,
             'total_delivery_tat': 0
                            }

            # if cate_id in comparison_period_result and cate_id in comparison_period_delivery_result:

            if cate_id in order_data_dict and cate_id in delivery_data_dict:

                res[cate_id].update(order_data_dict[cate_id])
                res[cate_id].update(delivery_data_dict[cate_id])
                if comparison_period_result.has_key(cate_id):
                    res[cate_id].update(comparison_period_result.get(cate_id))
                if comparison_period_delivery_result.has_key(cate_id):
                    res[cate_id].update(comparison_period_delivery_result.get(cate_id))
                if tat_data_dict.has_key(cate_id):
                    res[cate_id].update(tat_data_dict.get(cate_id))
                if comparison_period_tat_result.has_key(cate_id):
                    res[cate_id].update(comparison_period_tat_result.get(cate_id))
                objects.append(res[cate_id])

        
        for order in objects:

            c_specs = map(
                lambda x: self.render(x, self.col_specs_template, 'lines'),
                wanted_list)

            for list_data in c_specs:

                if str(list_data[0]) == str('category'):
                    list_data[4] = str(order['product_category_name'])

                if str(list_data[0]) == str('ananta_order_value'):
                    list_data[4] = str(order['ananta_order_value'])

                if str(list_data[0]) == str('general_order_value'):
                     list_data[4] = str(order['general_order_value'])

                if str(list_data[0]) == str('corporate_order_value'):
                    list_data[4] = str(order['corporate_order_value'])

                if str(list_data[0]) == str('retail_order_value'):
                    list_data[4] = str(order['retail_order_value'])

                if str(list_data[0]) == str('total_order_value'):
                    list_data[4] = str(float(order['ananta_order_value'])+float(order['general_order_value'])+float(order['corporate_order_value'])+float(order['retail_order_value']))

                if str(list_data[0]) == str('comparison_period_total_order_value'):
                     list_data[4] = str(order['comparison_period_total_order_value'])

                if str(list_data[0]) == str('ananta_sale_value'):
                    list_data[4] = str(order['ananta_sale_value'])

                if str(list_data[0]) == str('general_sale_value'):
                    list_data[4] = str(order['general_sale_value'])

                if str(list_data[0]) == str('corporate_sale_value'):
                    list_data[4] = str(order['corporate_sale_value'])

                if str(list_data[0]) == str('retail_sale_value'):
                     list_data[4] = str(order['retail_sale_value'])

                if str(list_data[0]) == str('total_sale_value'):
                    list_data[4] = str(float(order['ananta_sale_value'])+float(order['general_sale_value'])+float(order['corporate_sale_value'])+float(order['retail_sale_value']))

                if str(list_data[0]) == str('comparison_period_total_sale_value'):
                    list_data[4] = str(order['comparison_period_total_sale_value'])

                if str(list_data[0]) == str('ananta_margin_value'):
                    list_data[4] = str(order['ananta_margin'])

                if str(list_data[0]) == str('general_margin_value'):
                    list_data[4] = str(order['general_margin'])

                if str(list_data[0]) == str('corporate_margin_value'):
                    list_data[4] = str(order['corporate_margin'])

                if str(list_data[0]) == str('retail_margin_value'):
                    list_data[4] = str(order['retail_margin'])

                if str(list_data[0]) == str('total_margin_value'):
                    list_data[4] = str(float(order['ananta_margin'])+float(order['general_margin'])+float(order['corporate_margin'])+float(order['retail_margin']))

                if str(list_data[0]) == str('comparison_period_total_margin_value'):
                    list_data[4] = str(order['comparison_period_total_margin_value'])

                if str(list_data[0]) == str('ananta_reserve_tat'):
                    list_data[4] = str(order['ananta_reserve_tat'])

                if str(list_data[0]) == str('general_reserve_tat'):
                    list_data[4] = str(order['general_reserve_tat'])

                if str(list_data[0]) == str('corporate_reserve_tat'):
                    list_data[4] = str(order['corporate_reserve_tat'])

                if str(list_data[0]) == str('retail_reserve_tat'):
                    list_data[4] = str(order['retail_reserve_tat'])

                if str(list_data[0]) == str('total_reserve_tat'):
                    list_data[4] = str(int(order['ananta_reserve_tat'])+int(order['general_reserve_tat'])+int(order['corporate_reserve_tat'])+int(order['retail_reserve_tat']))

                if str(list_data[0]) == str('comparison_period_total_reserve_tat_value'):
                    list_data[4] = str(order['comparison_period_total_reserve_tat_value'])

                if str(list_data[0]) == str('ananta_delivery_tat'):
                    list_data[4] = str(order['ananta_delivery_tat'])

                if str(list_data[0]) == str('general_delivery_tat'):
                    list_data[4] = str(order['general_delivery_tat'])

                if str(list_data[0]) == str('corporate_delivery_tat'):
                    list_data[4] = str(order['corporate_delivery_tat'])

                if str(list_data[0]) == str('retail_delivery_tat'):
                    list_data[4] = str(order['retail_delivery_tat'])

                if str(list_data[0]) == str('total_delivery_tat'):
                    list_data[4] = str(int(order['ananta_delivery_tat'])+int(order['general_delivery_tat'])+int(order['corporate_delivery_tat'])+int(order['retail_delivery_tat']))

                if str(list_data[0]) == str('comparison_period_total_delivery_tat_value'):
                    list_data[4] = str(order['comparison_period_total_delivery_tat_value'])


            row_data = self.xls_row_template(c_specs, [x[0] for x in c_specs])
            row_pos = self.xls_write_row(
                ws, row_pos, row_data, row_style=self.aml_cell_style)


category_sales_margin_dashboard_xls('report.category.sales.margin.dashboard.xls',
                    'sale.order',
                    parser=category_sales_margin_dashboard_xls_parser)








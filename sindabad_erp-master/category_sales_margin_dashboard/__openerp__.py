{
    'name': 'Category Sales Margin Dashboard',
    'version': '8.0.1',
    'author': "Shuvarthi",
    'category': 'sale',
    'summary': 'Category Sales Margin Dashboard',
    'depends': ['sale', 'report_xls','send_sms_on_demand'],
    'data': [
        'security/category_sales_margin_security.xml',
        'security/ir.model.access.csv',

        'wizard/category_sales_margin_dashboard_filter.xml',
        'wizard/tat_report_filter.xml',
        'report/category_sales_margin_dashboard_xls.xml',
        'report/tat_breakdown_report_xls.xml',

    ],
}

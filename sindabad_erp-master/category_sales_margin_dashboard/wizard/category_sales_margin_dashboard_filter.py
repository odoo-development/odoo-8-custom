from openerp.osv import fields, osv
import datetime


class CategorySalesMarginDashboardWiz(osv.osv_memory):

    _name = 'category.sales.margin.dashboard.wiz'
    _description = 'Sales Margin Report Wiz'

    _columns = {
        'date_from': fields.date("Start Date"),
        'date_to': fields.date("End Date"),
        'com_date_from': fields.date("From Date"),
        'com_date_to': fields.date("To Date"),

    }

    _defaults = {
        'date_from': datetime.datetime.today(),
        'date_to': datetime.datetime.today(),
        'com_date_from': datetime.datetime.today(),
        'com_date_to': datetime.datetime.today()
    }


    def _build_contexts(self, cr, uid, ids, data, context=None):
        if context is None:
            context = {}

        result = {
                'date_from': data['form']['date_from'],
                'date_to': data['form']['date_to'],
                'com_date_from': data['form']['com_date_from'],
                'com_date_to': data['form']['com_date_to'],
                  }

        return result

    def check_report(self, cr, uid, ids, context=None):
        if context is None:
            context = {}

        data = {}

        data = self.read(cr, uid, ids, ['date_from', 'date_to','com_date_from','com_date_to'], context=context)[0]

        datas = {
            'ids': context.get('active_ids', []),
            'model': 'category.sales.margin.dashboard.wiz',
            'form': data
        }

        return {'type': 'ir.actions.report.xml',
                'report_name': 'category.sales.margin.dashboard.xls',
                'datas': datas
                }


from datetime import datetime, timedelta
import time
from datetime import date
from openerp import SUPERUSER_ID
from openerp.osv import fields, osv
from openerp.tools.translate import _
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT
import openerp.addons.decimal_precision as dp
from openerp import workflow
from openerp import api
import datetime
from openerp.addons.partial_delivery_in_so_list.sale import partial_waiting_process_class


class SaleOrder(osv.osv):
    _inherit = 'sale.order'




    def _computedue(self, cursor, user, ids, name, arg, context=None):
        res = {}

        for sale in self.browse(cursor, user, ids, context=context):
            total = 0

            for inv_items in sale.invoice_ids:
                if str(inv_items.state) == 'open':
                    if str(inv_items.type) == 'out_invoice':
                        total += inv_items.new_residual
                    else:
                        total -= inv_items.new_residual
            res[sale.id] = total

        return res



    _columns = {
        'due_invoiced_amount': fields.function(_computedue, string='Due Invoiced Amount', type='float')
    }


class invoice_journal_relation(osv.osv):
    _name = "invoice.journal.payment.relation"


    _columns = {
        'journal_id': fields.many2one('account.move','Journal ID'),
        'invoice_id': fields.many2one('account.invoice','Invoice ID'),
        'invoice_amount':fields.float('Invoice Amount')
    }



class account_invoice(osv.osv):
    _inherit = "account.invoice"

    def _get_residual_fnc(self, cr, uid, ids, name, arg, context=None):

        # if not ids:
        #    return []

        res = dict()

        for inv in self.browse(cr, uid, ids, context=context):
            res[inv.id] = inv.new_residual - inv.adjusted_refund_amount

        return res



    @api.one
    @api.depends(
        'state', 'currency_id', 'invoice_line.price_subtotal',
        'move_id.line_id.account_id.type',
        'move_id.line_id.amount_residual',
        # Fixes the fact that move_id.line_id.amount_residual, being not stored and old API, doesn't trigger recomputation
        'move_id.line_id.reconcile_id',
        'move_id.line_id.amount_residual_currency',
        'move_id.line_id.currency_id',
        'move_id.line_id.reconcile_partial_id.line_partial_ids.invoice.type',
    )
    # An invoice's residual amount is the sum of its unreconciled move lines and,
    # for partially reconciled move lines, their residual amount divided by the
    # number of times this reconciliation is used in an invoice (so we split
    # the residual amount between all invoice)
    def _compute_residual(self):
        self.residual = 0.0

        if self.type == 'out_invoice':
            # self.residual = self.new_residual-self.adjusted_refund_amount ## It was old code
            self.residual = self.new_residual
        elif self.type == 'out_refund':
            self.residual = self.new_residual
        else:
            partial_reconciliations_done = []
            for line in self.sudo().move_id.line_id:
                if line.account_id.type not in ('receivable', 'payable'):
                    continue
                if line.reconcile_partial_id and line.reconcile_partial_id.id in partial_reconciliations_done:
                    continue
                # Get the correct line residual amount
                if line.currency_id == self.currency_id:
                    line_amount = line.amount_residual_currency if line.currency_id else line.amount_residual
                else:
                    from_currency = line.company_id.currency_id.with_context(date=line.date)
                    line_amount = from_currency.compute(line.amount_residual, self.currency_id)
                # For partially reconciled lines, split the residual amount
                if line.reconcile_partial_id:
                    partial_reconciliation_invoices = set()
                    for pline in line.reconcile_partial_id.line_partial_ids:
                        if pline.invoice and self.type == pline.invoice.type:
                            partial_reconciliation_invoices.update([pline.invoice.id])
                    line_amount = self.currency_id.round(line_amount / len(partial_reconciliation_invoices))
                    partial_reconciliations_done.append(line.reconcile_partial_id.id)
                self.residual += line_amount
            self.residual = max(self.residual, 0.0)



        # Each partial reconciliation is considered only once for each invoice it appears into,
        # and its residual amount is divided by this number of invoices


        # self.residual = self.new_residual

    _columns = {
        'new_residual':fields.float('New Residual Amount'),
        # 'full_return':fields.boolean('Full Return'),
        'adjusted_refund_amount':fields.float('Adjusted Refund Amount'),
        'refund_invoices_number': fields.char('Adjusted refund Invoices'), ## It will be saved as comma (,) seperated ex. [INC/001,INVS652]
        'residual': fields.function(_get_residual_fnc, string='Balance'),
    }

    # set 'new_residual' at the time of validate
    def invoice_validate(self, cr, uid, ids, context=None):
        res = super(account_invoice, self).invoice_validate(cr, uid, ids, context=context)

        invoice_data = self.pool.get('account.invoice').browse(cr, uid, ids, context=context)

        for inv in invoice_data:
            inv.write({'new_residual': inv.amount_total})

        return res

class order_payment(osv.osv):
    _name = "order.payment"
    _description = "Customer Order Payment Voucher"

    _columns = {
        'name': fields.char('Name'),
        'customer_id': fields.many2one('res.partner', 'Customer', domain="['|', ('customer','=', True), ('is_company', '=', True)]", required=True),
        'customer_name': fields.char('Customer Name'),
        'customer_address': fields.char('Address'),
        'payment_date': fields.datetime('Date Of Received'),
        'period_id': fields.many2one('account.period', 'Period', required=True),
        'only_vat': fields.boolean('Only Vat'),
        'rec_amount': fields.float('Receiving Amount'),
        'bounce_count': fields.float('Bounced #'),
        'bounce_dates': fields.char('Bounced Dates'),
        'description': fields.char('Comments'),
        'invoice_amount': fields.float('Invoice Amount'),
        'scan': fields.char('Scan Invoice Here'),
        'order_scan': fields.char('Scan/Type Order Here'),
        'payment_type': fields.selection([
            ('cash', 'Cash'),
            ('bank', 'Bank'),
            ('challan', 'Challan'),

        ], 'Payment Type',select=True),

        'payment_method': fields.many2one('account.journal', 'Payment Method', required=True),
        'cheque': fields.char('Cheque/EFTN'),
        'cheque_date': fields.date('Cheque Date'),
        'mr_no': fields.char('Money Receipt No'),
        'journal_id':fields.many2one('account.move', 'Journal '),

        'confirm_time': fields.datetime('Confirmation Time'),
        'cancel_time': fields.datetime('Cancel Time'),
        'state': fields.selection([
            ('pending', 'Pending'),
            ('confirm', 'Confirmed'),
            ('cancel', 'Cancelled'),

        ], 'Status', readonly=True, copy=False, help="Gives the status of the Customer Order Payment Voucher", select=True),

        'order_payment_line': fields.one2many('order.payment.line', 'order_payment_id', 'Invoice payment line', required=True),
        'vat_payment_line': fields.one2many('vat.payment.line', 'vat_payment_id', 'VAT/AIT line'),

        'cancel_order_payment_date': fields.datetime('Cancel Order Payment Date'),
        'cancel_order_payment_by': fields.many2one('res.users', 'Cancel Order Payment By'),
        'cancel_order_payment_reason': fields.text('Cancel Order Payment Reason'),

    }


    def _get_period(self, cr, uid, context=None):
        if context is None: context = {}
        if context.get('period_id', False):
            return context.get('period_id')
        periods = self.pool.get('account.period').find(cr, uid, context=context)
        return periods and periods[0] or False

    _defaults = {
        'user_id': lambda obj, cr, uid, context: uid,
        'state': 'pending',
        'payment_date': datetime.datetime.today(),
        'period_id': _get_period,
    }


    @api.onchange('scan')
    def invoice_barcode_onchange(self):

        try:
            invoice_id = str(self.scan)
            self.scan = ''


            if invoice_id != 'False' and invoice_id != False and invoice_id != 'None' and invoice_id != None:
                invoice_line_env = self.env['account.invoice']
                invoice_line_obj = invoice_line_env.search([('id', '=', invoice_id)])
                for invoice_line in invoice_line_obj:

                    customer_id = invoice_line.partner_id.commercial_partner_id.id

                    if customer_id:
                        all_partner_ids = self.pool['res.partner'].search(self._cr, self._uid,[('id', 'child_of', customer_id)],context={})
                        invoice_ids = self.pool['account.invoice'].search(self._cr, self._uid,
                                                                          [('partner_id', 'in', all_partner_ids),
                                                                           ('state', '=', 'open'),('type', '=', 'out_invoice')], context={})
                        invoicelines = self.pool['account.invoice'].browse(self._cr, self._uid, invoice_ids)

                        order_payment_line = list()


                        for so in invoicelines:
                            paid_amount = so.amount_total - so.residual
                            order_payment_line.append({
                                'invoice_id': int(so.id),
                                'mag_no': str(so.name),
                                'delivered_amount': so.amount_total,
                                'paid_amount': paid_amount,
                                'unpaid_amount': so.residual,
                                'amount': 0,
                                'state': 'pending'
                            })

                        self.order_payment_line = order_payment_line
                        self.customer_id=customer_id


                        self.customer_name = invoice_line.partner_id.commercial_partner_id.name
                        self.customer_address = str(invoice_line.partner_id.commercial_partner_id.street) + ", " + str(invoice_line.partner_id.commercial_partner_id.city)




        except:
            pass

        return 'xxx'

    @api.onchange('order_scan')
    def order_scan_onchange(self):

        try:
            order_id = str(self.order_scan)
            self.order_scan = ''

            if order_id != 'False' and order_id != False and order_id != 'None' and order_id != None:
                order_line_env = self.env['sale.order']
                order_line_obj = order_line_env.search([('client_order_ref', '=', order_id)])
                for order_line in order_line_obj:

                    customer_id = order_line.partner_id.commercial_partner_id.id

                    if customer_id:
                        all_partner_ids = self.pool['res.partner'].search(self._cr, self._uid,
                                                                          [('id', 'child_of', customer_id)], context={})
                        invoice_ids = self.pool['account.invoice'].search(self._cr, self._uid,
                                                                          [('partner_id', 'in', all_partner_ids),
                                                                           ('state', '=', 'open'),('type', '=', 'out_invoice')], context={})
                        invoicelines = self.pool['account.invoice'].browse(self._cr, self._uid, invoice_ids)

                        order_payment_line = list()

                        for so in invoicelines:
                            paid_amount = so.amount_total - so.residual
                            order_payment_line.append({
                                 'invoice_id': int(so.id),
                                'mag_no': str(so.name),
                                'delivered_amount': so.amount_total,
                                'paid_amount': paid_amount,
                                'unpaid_amount': so.residual,
                                'amount': 0,
                                'state': 'pending'
                            })

                        self.order_payment_line = order_payment_line
                        self.customer_id = customer_id

                        self.customer_name = order_line.partner_id.commercial_partner_id.name
                        self.customer_address = str(order_line.partner_id.commercial_partner_id.street) + ", " + str(
                            order_line.partner_id.commercial_partner_id.city)




        except:
            pass

        return 'xxx'


    @api.onchange('order_payment_line')
    def test_code(self):
        total_sum = 0
        for line_item in self.order_payment_line:
            if line_item.select_invoice == True:
                total_sum = total_sum + line_item.amount
        self.invoice_amount = total_sum
        return 'ss'


    @api.onchange('customer_id')
    def customer_on_select(self):
    
        if self.customer_id:


            all_partner_ids = self.pool['res.partner'].search(self._cr, self._uid, [('id', 'child_of', self.customer_id.id)],context={})
            invoice_ids = self.pool['account.invoice'].search(self._cr, self._uid, [('partner_id', 'in', all_partner_ids),
                                                                        ('state', '=', 'open')], context={})
            invoicelines = self.pool['account.invoice'].browse(self._cr, self._uid, invoice_ids)

            order_payment_line = list()

            for so in invoicelines:
                paid_amount= so.amount_total - so.residual
                order_payment_line.append({
                    'invoice_id': int(so.id),
                    'mag_no': str(so.name),
                    'delivered_amount': so.amount_total,
                    'paid_amount': paid_amount,
                    'unpaid_amount': so.residual,
                    'amount': 0,
                    'state': 'pending'
                })

            self.order_payment_line = order_payment_line

            # customer info
            parent = self.customer_id
            customer = None
            for i in range(5):
                if len(parent.parent_id) == 1:
                    parent = parent.parent_id
                else:
                    customer = parent
                    break

            self.customer_name = customer.name
            self.customer_address = str(customer.street) + ", " + str(customer.city)

        return "xXxXxXxXxX"


    @api.model
    def create(self, vals):

        rec_amount = round((vals.get('rec_amount', 0)),2)
        inv_amount = round((vals.get('invoice_amount', 0)),2)

        total_rec_amount = 0
        total_vats_amount =0
        total_amount = 0


        if vals:
            new_vals_line= vals['order_payment_line']
            created_list=[]



            for v in vals['order_payment_line']:
                if v[2].get('amount') >0 and v[2].get('select_invoice') == True:
                    created_list.append(v)
                    total_rec_amount += v[2].get('amount')

            for vat in vals['vat_payment_line']:
                total_vats_amount += vat[2].get('amount')


            vals['order_payment_line'] = created_list

            total_amount = rec_amount + total_vats_amount


            empty_reason_found = False # This parameter for

            for post_items in created_list:
                if post_items[2].get('unpaid_amount') !=0 and (post_items[2].get('unpaid_reason') is None or post_items[2].get('unpaid_reason') is False):
                    empty_reason_found=True
                    break

            if empty_reason_found == True:
                raise osv.except_osv(_('Attention !!!'),
                                     _('Please Select Unpaid Reason'))



        if (rec_amount != inv_amount):

            raise osv.except_osv(_('Order payment line adjustment ERROR!'),
                                 _('Receiving amount should be equal to line amounts!!!'))
        else:
            record = super(order_payment, self).create(vals)

            rec_number = "REC-" + str(record.id)
            record.name = rec_number

            return record





    def write(self, cr, uid, ids, vals, context=None):

        ord_payment = self.browse(cr, uid, ids, context=context)

        if ord_payment.state == 'confirm':
            raise osv.except_osv(_('Already Confirmed'), _('This Payment has been completed.You can not edit.'))


        ord_pay_line = self.pool.get('order.payment.line')

        rec_amount = vals.get('rec_amount', ord_payment.rec_amount)


        total_rec_amount = 0


        if vals:
            # vals contains 'order_payment_line'
            if vals.get('order_payment_line'):
                for v in vals['order_payment_line']:
                    if v[2]:
                        total_rec_amount += v[2].get('amount', ord_pay_line.browse(cr, uid, v[1]).amount)

                    else:
                        total_rec_amount += ord_pay_line.browse(cr, uid, v[1]).amount


            # vals does not contains 'order_payment_line'
            else:
                for ord_p in ord_payment.order_payment_line:
                    total_rec_amount += ord_p.amount

            # if vals.get('order_payment_line'):
            #
            #     if rec_amount != total_rec_amount:
            #         raise osv.except_osv(_('Order payment line adjustment ERROR!'),
            #                              _('Receiving amount should be equal to line amounts!!!'))


        return super(order_payment, self).write(cr, uid, ids, vals, context=context)


    def bounch_order_payment(self, cr, uid, ids, context=None):
        if context is None:
            context= {}

        payments = self.browse(cr, uid, ids, context=context)
        bounched = payments.bounce_count
        bounched_date = str(payments.bounce_dates)

        for pay_items in payments:
            if pay_items.state != 'confirm':
                raise osv.except_osv(_('No permission'),_('Sorry this payment is not confirmed'))

            if pay_items.journal_id.id is not None and pay_items.journal_id.id is not False:

                move_id = pay_items.journal_id.id
                # Cancel the journal
                move_obj_m = self.pool.get('account.move')
                move_obj = move_obj_m.button_cancel(cr,uid,ids=[move_id],context=context)

                #Ends Here


                # Revert The Payment Amount

                for payment_line in pay_items.order_payment_line:
                    balance_amount = payment_line.invoice_id.residual + payment_line.amount
                    inv_id = payment_line.invoice_id.id

                    cr.execute("update account_invoice set new_residual='{0}',state='open' where id='{1}'".format(balance_amount,inv_id))
                    cr.commit()

                # Ends Here

                # Follwoing Code For delete the relation

                cr.execute("delete from invoice_journal_payment_relation where journal_id='{0}'".format(move_id))
                cr.commit()

                # Ends Here


                ## Following Code for update the order Payment Status
                move_obj_m.unlink(cr, uid, [move_id], context=context) ## Deletion of moved ID
                bounched = bounched +1
                bounched_date = bounched_date+ str(datetime.datetime.today()) + str(', ')

                cr.execute("update order_payment set journal_id=NULL,state='pending', bounce_count= '{0}',bounce_dates='{1} 'where id='{2}'".format(bounched,bounched_date,pay_items.id))
                cr.commit()
                # Ends Here


        return 0

    def confirm_order_payment(self, cr, uid, ids, context=None):
        magento_order_numbers = []

        if context is None:
            context= {}



        # Starts Here
        payments = self.browse(cr, uid, ids, context=context)

        line_ids= []

        for pay_items in payments:
            if pay_items.state == 'confirm':
                raise osv.except_osv(_('Already Confirmed'),_('This Payment has been completed'))


            if pay_items.customer_id.property_account_receivable.id is False:
                raise osv.except_osv(_('Please assign account'),_('Your Company Does Not Have Assigned account.!!!!!'))

            rec_amount = pay_items.rec_amount

            only_vat = pay_items.only_vat

            ref = ''
            memo = pay_items.cheque

            if memo is False or memo is None:
                memo=''


            for payment_line in pay_items.order_payment_line:
                ref = ref + ', '+ str(payment_line.mag_no)




            total_vat = 0
            for vat_line in pay_items.vat_payment_line:
                if vat_line.amount >0:
                    line_ids.append((0, 0, {
                        'name': memo,
                        'account_id': vat_line.payment_method.default_debit_account_id.id,
                        'partner_id': pay_items.customer_id.id,
                        'debit': vat_line.amount,
                    }))
                    total_vat +=vat_line.amount


            if rec_amount >0:
                if only_vat == True:
                    credit_amount = total_vat
                else:
                    credit_amount =(rec_amount+total_vat)

                line_ids.append((0, 0, {
                    'name': memo,
                    'account_id': pay_items.customer_id.property_account_receivable.id,
                    'partner_id': pay_items.customer_id.id,
                    'credit': credit_amount,
                }))


            if rec_amount >0 and only_vat !=True:

                line_ids.append((0, 0, {
                        'name': memo,
                        'account_id': pay_items.payment_method.default_debit_account_id.id,
                        'partner_id': pay_items.customer_id.id,
                        'debit': rec_amount,
                    }))





            relation_between_journal_invoice_list =[]


            journal_id=None
            if len(line_ids)>1:


                j_vals = {'name': '/',
                    'journal_id':1141,
                    'date': pay_items.payment_date,
                    'period_id': pay_items.period_id.id,
                    'ref': ref,
                    'line_id':line_ids

                }



                jv_entry = self.pool.get('account.move')
                saved_jv_id = jv_entry.create(cr, uid, j_vals, context=context)
                if saved_jv_id > 0:
                    journal_id = saved_jv_id
                jv_entry.button_validate(cr, uid, [saved_jv_id], context)



                for payment_line in pay_items.order_payment_line:
                    magento_order_numbers.append(payment_line.mag_no)

                    today_date = date.today()

                    inv_id = payment_line.invoice_id.id

                    if journal_id > 0:
                        cr.execute('INSERT INTO invoice_journal_payment_relation (journal_id, invoice_id,invoice_amount) values (%s, %s,%s)',(journal_id, inv_id, payment_line.amount))
                        cr.commit()

                    vat_ait_challan=False
                    if payment_line.unpaid_reason == 'vat' or payment_line.unpaid_reason == 'ait':
                        vat_ait_challan = True


                    if payment_line.unpaid_amount == 0:

                        new_residual=0
                        update_refund_residual_amount = "UPDATE account_invoice set new_residual='{0}',state='paid',vat_ait_challan='{1}' where id='{2}'".format(new_residual,vat_ait_challan, inv_id)

                        cr.execute(update_refund_residual_amount)
                        cr.commit()
                    else:

                        new_residual = payment_line.unpaid_amount
                        update_refund_residual_amount = "UPDATE account_invoice set new_residual='{0}',state='open',vat_ait_challan='{1}',ait_date='{2}' where id='{3}'".format(
                            new_residual,vat_ait_challan,today_date, inv_id)



                        cr.execute(update_refund_residual_amount)
                        cr.commit()






                ## Make relation between invoice and journals







        for single_id in ids:

            confirm_order_payment_query = "UPDATE order_payment SET state='confirm',journal_id={0}, confirm_time='{1}' WHERE id={2}".format(
                journal_id,str(fields.datetime.now()), single_id)
            cr.execute(confirm_order_payment_query)
            cr.commit()

            confirm_order_payment_line = "UPDATE order_payment_line SET state='confirm', confirm_time='{0}' WHERE order_payment_id={1}".format(
                str(fields.datetime.now()), single_id)
            cr.execute(confirm_order_payment_line)
            cr.commit()

            ### Call Status Synch Funciotn
            for mag_order in magento_order_numbers:
                query = "select id from sale_order where client_order_ref=%s"
                cr.execute(query,(mag_order,))
                so_obj = cr.fetchall()

                so_ids = []

                if len(so_obj) >0:
                    so_ids.append(so_obj[0][0])

                    so_obj = self.pool.get('sale.order')

                    for so in so_obj.browse(cr, uid, so_ids):

                        if so.invoice_ids:
                            invoices = [inv.state for inv in so.invoice_ids if
                                        inv.type == 'out_invoice' and inv.state != 'cancel']
                            invoices_state = [inv_state for inv_state in invoices if inv_state != 'paid']

                            try:
                                s_uid = 1
                                if not invoices_state and so.state == 'done':
                                    data = {
                                        'odoo_order_id': so.id,
                                        'magento_id': str(so.client_order_ref),
                                        'order_state': str('complete'),
                                        'state_time': fields.datetime.now(),
                                    }
                                    new_r = self.pool["order.status.synch.log"].create(cr, s_uid, data)
                            except:
                                pass



                    # self.pool.get('order.item.wise.close').complete_order_status_sync(cr, uid, so_ids,
                    #                                                                 context=None)  ## Ids means Sales Order ID

            ## Ends Here Status Synch Function



        return True

    def cancel_order_payment(self, cr, uid, ids, context=None):

        ord_payment = self.browse(cr, uid, ids, context=context)

        if ord_payment.state == 'confirm':
            raise osv.except_osv(_('Already Confirmed'), _('This Payment has been completed.You can not Cancel.'))

        for single_id in ids:
            cancel_order_payment_query = "UPDATE order_payment SET state='cancel', cancel_time='{0}' WHERE id={1}".format(
                str(fields.datetime.now()), single_id)
            cr.execute(cancel_order_payment_query)
            cr.commit()

            cancel_order_payment_line = "UPDATE order_payment_line SET state='cancel', cancel_time='{0}' WHERE order_payment_id={1}".format(
                str(fields.datetime.now()), single_id)
            cr.execute(cancel_order_payment_line)
            cr.commit()

        return True


class order_payment_line(osv.osv):
    _name = "order.payment.line"
    _description = "order payment line"

    _columns = {
        'order_payment_id': fields.many2one('order.payment', 'Order Payment ID', required=True,
                                              ondelete='cascade', select=True, readonly=True),





        'invoice_id': fields.many2one('account.invoice', string="Invoice No"),
        'mag_no': fields.char('Magento No'),
        'order_id': fields.many2one('sale.order', string="Order No."),
        'delivered_amount': fields.float('Invoiced Amount'),
        'paid_amount': fields.float('Paid Till Today'),
        'unpaid_amount': fields.float('Unpaid Amount'),
        'amount': fields.float('Receiving Amount'),
        'unpaid_reason': fields.selection([
            ('vat', 'VAT'),
            ('ait', 'AIT'),
            ('unpaid', 'Unpaid'),
        ], 'Why Unpaid?',select=True),


        'remarks': fields.char('Remarks'),
        'select_invoice': fields.boolean('On Select'),

        'confirm_time': fields.datetime('Confirmation Time'),
        'cancel_time': fields.datetime('Cancel Time'),

        'state': fields.selection([
            ('pending', 'Pending'),
            ('confirm', 'Confirmed'),
            ('cancel', 'Cancelled'),
        ], 'Status', help="Gives the status of the order payment line", select=True),
    }

    _defaults = {
        'amount': 0,
    }

    @api.onchange('amount')
    def mag_on_select(self):

        if self.amount:


            self.unpaid_amount = float(self.delivered_amount) - float(self.paid_amount) - float(self.amount)


        return "xXxXxXxXxX"

    @api.onchange('select_invoice')
    def mark_invoice_select(self):
        self.amount=self.unpaid_amount

        return "xXxXxXxXxX"

    # @api.onchange('order_id')
    # def order_on_select(self):
    #
    #     if self.order_id:
    #         self.mag_no = self.order_id.client_order_ref
    #         self.order_amount = self.order_id.amount_total
    #         self.delivered_amount = self.order_id.delivered_amount if self.order_id.delivered_amount else 0
    #         self.state = 'pending'
    #
    #     return "xXxXxXxXxX"


class CancelOrderPaymentReason(osv.osv):
    _name = "cancel.order.payment.reason"
    _description = "Cancel Order Payment Reason"

    _columns = {
        'cancel_order_payment_date': fields.datetime('Cancel Order Payment Date'),
        'cancel_order_payment_by': fields.many2one('res.users', 'Cancel Order Payment By'),
        'cancel_order_payment_reason': fields.text('Cancel Order Payment Reason', required=True),
    }

    def cancel_order_payment_reason_def(self, cr, uid, ids, context=None):
        ids = context['active_ids']
        cancel_order_payment_reason = str(context['cancel_order_payment_reason'])
        cancel_order_payment_by = uid
        cancel_order_payment_date = str(fields.datetime.now())

        order_payment_obj = self.pool.get('order.payment')

        for s_id in ids:
            cancel_order_payment_query = "UPDATE order_payment SET cancel_order_payment_reason='{0}', cancel_order_payment_by={1}, cancel_order_payment_date='{2}' WHERE id={3}".format(
                cancel_order_payment_reason, cancel_order_payment_by, cancel_order_payment_date, s_id)
            cr.execute(cancel_order_payment_query)
            cr.commit()

            order_payment_obj.cancel_order_payment(cr, uid, [s_id], context)

        return True





class vat_payment_line(osv.osv):
    _name = "vat.payment.line"
    _description = "VAT/AIT line"

    _columns = {
        'vat_payment_id': fields.many2one('order.payment', 'Order Payment ID', required=True,
                                              ondelete='cascade', select=True, readonly=True),
        'reason': fields.selection([
            ('ait', 'AIT'),
            ('vat', 'VAT'),
            ('eftn', 'EFTN'),
            ('others', 'Others'),
        ], 'Reason ', select=True),
        'payment_method': fields.many2one('account.journal', 'Account'),
        'amount': fields.float('Amount'),
        'remark': fields.char('Remarks'),
    }


class stock_invoice_onshipping(osv.osv_memory):
    _inherit = "stock.invoice.onshipping"
    _description = "Stock Invoice Onshipping"
    _columns = {

        'full_return':fields.boolean('Full Return'),
        'invoice_number': fields.char('Invoice Number'),
        'invoice_scan': fields.char('Invoice Scan'),
        'invoice_id': fields.many2one('account.invoice', 'Refund Invoice'),
    }


    @api.onchange('invoice_scan')
    def _invoice_scan_change(self):
        if self.invoice_scan:

            inv_ids = [int(self.invoice_scan)]

            invoicelines = self.pool['account.invoice'].browse(self._cr, self._uid, inv_ids)
            for inv_items in invoicelines:
                inv_number = inv_items.number
                self.invoice_number = inv_number
                self.invoice_scan=''


        return "xXxXxXxXxX"


    def open_invoice(self, cr, uid, ids, context=None):

        if context is None:
            context = {}

        invoice_ids = self.create_invoice(cr, uid, ids, context=context)

        ### Send SMS when we validate the refun  invoice
        invoice_data = self.pool.get('account.invoice').browse(cr, uid, invoice_ids, context=context)


        try:
            inv_create_date = invoice_data.create_date
            if invoice_data.type == 'out_refund':
                cr.execute("update account_invoice set delivery_date=%s where id=%s",([inv_create_date, invoice_data.id]))
                cr.commit()

        except:
            pass



        try:

            if invoice_data.type == 'out_refund':
                sms_obj = self.pool.get("send.sms.on.demand")
                mobile_numbers = invoice_data.partner_id.phone
                # mobile_numbers='01716520313'

                returned_amount=invoice_data.amount_total
                order = str(invoice_data.name)
                name = "wms.manifest.process." + str(invoice_data.id)

                customer_name = ''
                rm_mobile_numbers = ''
                rm_name = ''
                parent = invoice_data.partner_id

                for i in range(5):
                    if len(parent.parent_id) == 1:
                        parent = parent.parent_id
                    else:
                        rm_mobile_numbers = parent.user_id.partner_id.phone
                        rm_name = parent.user_id.partner_id.name
                        customer_name = parent.name
                        # company_id = parent.id
                        break

                sms_text_return = "Your shipment of order #{0} has been returned. For any query please call us on Helpline: 09612002244".format(order)

                sms_text_return_rm = "Dear {4}, Order #{0} of your client {1} - Mob- {2} has been returned with Value Tk {3}.".format(
                    order, customer_name, mobile_numbers, format(int(round(returned_amount)), ','), rm_name)




                # only retail customer will get the SMS
                # if returned_amount > 0 and "Retail" in str(so.customer_classification):
                if returned_amount > 0:
                    sms_obj.send_sms_on_demand(cr, uid, ids, sms_text_return, mobile_numbers, name, context=context)
                    if rm_mobile_numbers:
                        sms_obj.send_sms_on_demand(cr, uid, ids, sms_text_return_rm, rm_mobile_numbers, name, context=context)

        except:

            pass



        ### ENds here



        if not invoice_ids:
            raise osv.except_osv(_('Error!'), _('No invoice created!'))

        data = self.browse(cr, uid, ids[0], context=context)

        action_model = False
        action = {}

        journal2type = {'sale': 'out_invoice', 'purchase': 'in_invoice', 'sale_refund': 'out_refund',
                        'purchase_refund': 'in_refund'}
        inv_type = journal2type.get(data.journal_type) or 'out_invoice'
        data_pool = self.pool.get('ir.model.data')
        if inv_type == "out_invoice":
            action_id = data_pool.xmlid_to_res_id(cr, uid, 'account.action_invoice_tree1')
        elif inv_type == "in_invoice":
            action_id = data_pool.xmlid_to_res_id(cr, uid, 'account.action_invoice_tree2')
        elif inv_type == "out_refund":
            action_id = data_pool.xmlid_to_res_id(cr, uid, 'account.action_invoice_tree3')
        elif inv_type == "in_refund":
            action_id = data_pool.xmlid_to_res_id(cr, uid, 'account.action_invoice_tree4')

        # get sale order info
        invoice_pool = self.pool.get('account.invoice')
        invoice_data = invoice_pool.browse(cr, uid, invoice_ids[0], context=context)

        ## Update the Adjusted Refund Invoice

        if inv_type == "out_refund":

            invoice_data.signal_workflow('invoice_open')
            if data.invoice_number:
                invoice_number = data.invoice_number


                invoice_ids = invoice_pool.search(cr, uid, [('number', '=', invoice_number)], limit=1, context=context)


                if len(invoice_ids)>0:
                    main_inv_obj = invoice_pool.browse(cr, uid, invoice_ids[0], context=context)
                    refund_amount = invoice_data.amount_total
                    current_balance = main_inv_obj.residual - refund_amount

                    cr.execute("update account_invoice set adjusted_refund_amount=%s,new_residual=%s where id=%s",([refund_amount, current_balance,invoice_ids[0]]))
                    cr.commit()

                    if main_inv_obj.state!='paid':
                        if main_inv_obj.state!='cancel':
                            full_refund = self.full_refund_paid(cr,uid,invoice_ids[0],invoice_data.id,context)

                            if full_refund:
                                cr.execute("update account_invoice set state=%s where id=%s",
                                    (['paid', invoice_ids[0]]))
                                cr.execute("update account_invoice set state=%s where id=%s",
                                           (['paid', invoice_data.id]))
                                cr.commit()


        ## Ends Here

        if not str(invoice_data.name).startswith("PO"):

            # waiting availability
            pwp = partial_waiting_process_class()
            pwp.waiting_availability_process(cr, uid, str(invoice_data.name), context=context)

            # partially delivered
            sale_order_pool = self.pool.get('sale.order')
            pwp.partially_delivered_prcess(sale_order_pool, cr, uid, str(invoice_data.name), context=context)

            # partially available
            pwp.partially_available_process(cr, uid, str(invoice_data.name), context=context)

        if context.has_key('refund'):
            return invoice_data.id

        if action_id:
            action_pool = self.pool['ir.actions.act_window']
            action = action_pool.read(cr, uid, action_id, context=context)
            action['domain'] = "[('id','in', [" + ','.join(map(str, invoice_ids)) + "])]"
            return action
        return True




    def full_refund_paid(self,cr,uid,invoice_id,refund_invoice_id,context):
        invoice_pool = self.pool.get('account.invoice')

        main_inv_obj = invoice_pool.browse(cr, uid, invoice_id, context=context)
        refund_inv_obj = invoice_pool.browse(cr, uid, refund_invoice_id, context=context)

        price_subtotal = 0.0
        refund_price_subtotal = 0.0
        for item in main_inv_obj.invoice_line:
            if item.product_id.product_tmpl_id.type != 'service':
                price_subtotal+= item.price_subtotal

        for refund_item in refund_inv_obj.invoice_line:
            if refund_item.product_id.product_tmpl_id.type != 'service':
                refund_price_subtotal+= refund_item.price_subtotal

        if price_subtotal == refund_price_subtotal:
            full_refund = True
        else:
            full_refund = False

        return full_refund




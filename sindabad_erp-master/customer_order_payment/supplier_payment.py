from datetime import datetime, timedelta
import time
from datetime import date
from openerp import SUPERUSER_ID
from openerp.osv import fields, osv
from openerp.tools.translate import _
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT
import openerp.addons.decimal_precision as dp
from openerp import workflow
from openerp import api
import datetime
from openerp.addons.partial_delivery_in_so_list.sale import partial_waiting_process_class





class supplier_payment(osv.osv):
    _name = "supplier.payment"
    _description = "Customer supplier payment Voucher"

    _columns = {
        'name': fields.char('Name'),
        'customer_id': fields.many2one('res.partner', 'Supplier', domain="['|', ('supplier','=', True), ('is_company', '=', True)]", required=True),
        'customer_name': fields.char('Customer Name'),
        'customer_address': fields.char('Address'),
        'payment_date': fields.datetime('Date Of Received'),
        'period_id': fields.many2one('account.period', 'Period', required=True),
        'only_vat': fields.boolean('Only Vat'),
        'rec_amount': fields.float('Receiving Amount'),
        'bounce_count': fields.float('Bounced #'),
        'bounce_dates': fields.char('Bounced Dates'),
        'description': fields.char('Comments'),
        'invoice_amount': fields.float('Invoice Amount'),
        'scan': fields.char('Scan Invoice Here'),
        'order_scan': fields.char('Scan/Type Order Here'),
        'payment_type': fields.selection([
            ('cash', 'Cash'),
            ('bank', 'Bank'),
            ('challan', 'Challan'),

        ], 'Payment Type',select=True),

        'payment_method': fields.many2one('account.journal', 'Payment Method', required=True),
        'cheque': fields.char('Cheque/EFTN'),
        'cheque_date': fields.date('Cheque Date'),
        'mr_no': fields.char('Money Receipt No'),
        'journal_id':fields.many2one('account.move', 'Journal '),

        'confirm_time': fields.datetime('Confirmation Time'),
        'cancel_time': fields.datetime('Cancel Time'),
        'state': fields.selection([
            ('pending', 'Pending'),
            ('confirm', 'Confirmed'),
            ('cancel', 'Cancelled'),

        ], 'Status', readonly=True, copy=False, help="Gives the status of the Customer supplier payment Voucher", select=True),

        'supplier_payment_line': fields.one2many('supplier.payment.line', 'supplier_payment_id', 'Invoice payment line', required=True),
        'vat_payment_line': fields.one2many('vat.payment.line', 'vat_payment_id', 'VAT/AIT line'),

        'cancel_supplier_payment_date': fields.datetime('Cancel supplier payment Date'),
        'cancel_supplier_payment_by': fields.many2one('res.users', 'Cancel supplier payment By'),
        'cancel_supplier_payment_reason': fields.text('Cancel supplier payment Reason'),

    }


    def _get_period(self, cr, uid, context=None):
        if context is None: context = {}
        if context.get('period_id', False):
            return context.get('period_id')
        periods = self.pool.get('account.period').find(cr, uid, context=context)
        return periods and periods[0] or False

    _defaults = {
        'user_id': lambda obj, cr, uid, context: uid,
        'state': 'pending',
        'payment_date': datetime.datetime.today(),
        'period_id': _get_period,
    }


class supplier_payment_line(osv.osv):
    _name = "supplier.payment.line"
    _description = "supplier payment line"

    _columns = {
        'supplier_payment_id': fields.many2one('supplier.payment', 'supplier payment ID', required=True,
                                              ondelete='cascade', select=True, readonly=True),

        'invoice_id': fields.many2one('account.invoice', string="Invoice No"),
        'mag_no': fields.char('Magento No'),
        'order_id': fields.many2one('sale.order', string="Order No."),
        'delivered_amount': fields.float('Invoiced Amount'),
        'paid_amount': fields.float('Paid Till Today'),
        'unpaid_amount': fields.float('Unpaid Amount'),
        'amount': fields.float('Receiving Amount'),
        'unpaid_reason': fields.selection([
            ('vat', 'VAT'),
            ('ait', 'AIT'),
            ('unpaid', 'Unpaid'),
        ], 'Why Unpaid?',select=True),


        'remarks': fields.char('Remarks'),
        'select_invoice': fields.boolean('On Select'),

        'confirm_time': fields.datetime('Confirmation Time'),
        'cancel_time': fields.datetime('Cancel Time'),

        'state': fields.selection([
            ('pending', 'Pending'),
            ('confirm', 'Confirmed'),
            ('cancel', 'Cancelled'),
        ], 'Status', help="Gives the status of the supplier payment line", select=True),
    }

    _defaults = {
        'amount': 0,
    }

    @api.onchange('amount')
    def mag_on_select(self):

        if self.amount:


            self.unpaid_amount = float(self.delivered_amount) - float(self.paid_amount) - float(self.amount)


        return "xXxXxXxXxX"

    @api.onchange('select_invoice')
    def mark_invoice_select(self):
        self.amount=self.unpaid_amount

        return "xXxXxXxXxX"

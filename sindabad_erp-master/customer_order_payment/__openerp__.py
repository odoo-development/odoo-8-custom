# Author Mufti Muntasir Ahmed 11-04-2016

{
    'name': 'Customer Order Payment',
    'version': '1.0',
    'category': 'Account',
    'author': 'Mufti Muntasir Ahmed',
    'summary': 'Auto Customer Payment order wise',
    'description': 'All Expenses records are assigned in this module.',
    'depends': ['base','sale', 'account','invoice_ageing_report', 'delivery_customer_name'],
    'data': [
        'security/order_payment_security.xml',
        'security/ir.model.access.csv',

        'report/report_customer_order_payment.xml',
        'report/report_menu.xml',

        'wizard/cancel_order_payment_reason_view.xml',
        'views/order_payment_view.xml',
        'views/inherit_account_invoice_view.xml',
        'views/cancel_order_payment_button.xml',
        'order_payment_menu.xml',
        'stock_invoice_onshopping_extend_view.xml'
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}

from datetime import datetime, timedelta
import time
from openerp import SUPERUSER_ID
from openerp.osv import fields, osv
from openerp.tools.translate import _
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT
import openerp.addons.decimal_precision as dp
from openerp import workflow
import datetime


class sale_order(osv.osv):
    _inherit = "sale.order"

    _columns = {
        'cancel_quotation_date': fields.datetime('Quotation Cancel Date'),
        'cancel_quotation_by': fields.many2one('res.users', 'Quotation Cancel By'),
        'cancel_quotation_reason': fields.selection([

            ("product_unavailability", "Product unavailability"),
            ("delay_delivery", "Delay delivery"),
            ("pricing_issue", "Pricing issue"),
            ("wrong_order", "Wrong order"),
            ("cash_problem", "Cash problem"),
            ("damage_product", "Damage product"),
            ("double_ordered", "Double ordered"),
            ("sample_not_approved", "Sample not approved"),
            ("wrong_sku", "Wrong SKU"),
            ("not_interested", "Not interested"),
            ("wrong_product", "Wrong product"),
            ("quality_issue", "Quality issue"),
            ("test_order", "Test Order"),
            ("payment_not_done", "Payment not done"),
            ("apnd", "Advance payment not done"),
            ("invalid_information", "Invalid Information"),
            ("customer_unreachable", "Customer unreachable"),
            ("same_day_delivery", "Same day delivery"),
            ("fake_order", "Fake Order"),
            ("sourcing_delay","Sourcing Delay"),
            ("reorder","Reorder"),
            ("discount_issue","Discount Issue"),
            ("product_not_available_customer_will_take_alternative_product", "Product not available, Customer will take alternative product"),

        ], 'Quotation Cancel Reason', copy=False, help="Reasons", select=True)

    }


class so_cancel_quotation_process(osv.osv):
    _name = "so.cancel.quotation.process"
    _description = "SO Cancel Quotation Process"

    _columns = {
        'cancel_quotation_date': fields.datetime('Quotation Cancel Date'),
        'cancel_quotation_by': fields.many2one('res.users', 'Quotation Cancel By'),
        'cancel_quotation_reason': fields.selection([

            ("product_unavailability", "Product unavailability"),
            ("delay_delivery", "Delay delivery"),
            ("pricing_issue", "Pricing issue"),
            ("wrong_order", "Wrong order"),
            ("cash_problem", "Cash problem"),
            ("damage_product", "Damage product"),
            ("double_ordered", "Double ordered"),
            ("sample_not_approved", "Sample not approved"),
            ("wrong_sku", "Wrong SKU"),
            ("not_interested", "Not interested"),
            ("wrong_product", "Wrong product"),
            ("quality_issue", "Quality issue"),
            ("test_order", "Test Order"),
            ("payment_not_done", "Payment not done"),
            ("apnd", "Advance payment not done"),
            ("invalid_information", "Invalid Information"),
            ("customer_unreachable", "Customer unreachable"),
            ("same_day_delivery", "Same day delivery"),
            ("fake_order", "Fake Order"),
            ("sourcing_delay","Sourcing Delay"),
            ("reorder","Reorder"),
            ("discount_issue","Discount Issue"),
            ("product_not_available_customer_will_take_alternative_product", "Product not available, Customer will take alternative product"),

        ], 'Quotation Cancel Reason', copy=False, help="Reasons", select=True)

    }

    def so_cancel_quotation(self, cr, uid, ids, context=None):

        ids = context['active_ids']
        cancel_quotation_reason = context['cancel_quotation_reason']
        cancel_quotation_by = uid
        cancel_quotation_date = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')

        so_obj = self.pool.get('sale.order')

        try:

            for id in ids:
                cancel_quotation_query = "UPDATE sale_order SET cancel_quotation_reason='{0}', cancel_quotation_by={1}, cancel_quotation_date='{2}' WHERE id={3}".format(str(cancel_quotation_reason), int(cancel_quotation_by), str(cancel_quotation_date), id)
                cr.execute(cancel_quotation_query)
                cr.commit()

                so_obj.action_cancel(cr, uid, [id], context)
        except:
            pass

        return True

{
    'name': 'SO Cancel Quotation',
    'version': '1.0',
    'category': 'SO Cancel Quotation',
    'author': 'Shahjalal',
    'summary': 'Sales Order, Cancel Quotation',
    'description': 'SO Cancel Quotation',
    'depends': ['odoo_magento_connect', 'base', 'sale'],
    'data': [
        'security/so_cancel_quatation_security.xml',
        'security/ir.model.access.csv',
        'wizard/order_cancel.xml',
        'cancel_quotation.xml',
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}

import math
import re
import time

from openerp import api, tools, SUPERUSER_ID
from openerp.osv import osv, fields, expression
from openerp.tools.translate import _
from openerp.tools import DEFAULT_SERVER_DATETIME_FORMAT
import psycopg2

import openerp.addons.decimal_precision as dp
from openerp.tools.float_utils import float_round, float_compare

from openerp.report import report_sxw
from openerp.osv import fields, osv
from datetime import datetime, timedelta


class ReorderLevelNoti(report_sxw.rml_parse):

    def __init__(self, cr, uid, name, context):
        super(ReorderLevelNoti, self).__init__(cr, uid, name, context)

        self.context = context

    def set_context(self, objects, data, ids, report_type=None):

        ids = self.context.get('active_ids')

        # import pdb;pdb.set_trace()

        product_ids = []

        return super(ReorderLevelNoti, self).set_context(objects, data, self.product_ids, report_type)


class report_customerstatement(osv.AbstractModel):
    _name = 'report.re_order_level_notifications.report_reorderlevelnoti'
    _inherit = 'report.abstract_report'
    _template = 're_order_level_notifications.report_reorderlevelnoti'
    _wrapped_report_class = ReorderLevelNoti

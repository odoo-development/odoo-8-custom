{
    'name': 'Re order level notifications',
    'version': '1.0',
    'category': 'Generic Modules',
    'author': 'Shahjalal Hossain',
    'summary': 'Vendor PO to Magento',
    'description': 'Vendor PO to Magento',
    'depends': ['base', 'purchase', 'po_approval_process', 'purchase_order_line_state', 'product'],
    'data': [
        'reoln_menu.xml',
        're_order_level.xml',
        'reorder_level_email_template.xml',
        'views/report_roqn.xml',
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}

# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2009 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import time
from openerp.report import report_sxw
from openerp.osv import osv


class salesmissingdata(report_sxw.rml_parse):

    def __init__(self, cr, uid, name, context):
        super(salesmissingdata, self).__init__(cr, uid, name, context)
        self.localcontext.update({
            'get_client_order_ref': self.get_client_order_ref,


        })
    def get_client_order_ref(self, obj):

        return '-'


class report_jv(osv.AbstractModel):
    _name = 'report.journal_voucher_report.report_jv'
    _inherit = 'report.abstract_report'
    _template = 'journal_voucher_report.report_jv'
    _wrapped_report_class = salesmissingdata

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:

## Author Mufti Muntasir Ahmed 01-04-2018

{
    'name': 'Journal Voucher Print',
    'version': '8.0.0',
    'category': 'Accounts',
    'description': """
Allows you to print delivery challan    
==============================================================

It is a sample delivery challan report with custom fields which 
are exported from magento version 1.

""",
    'author': 'Mufti Muntasir Ahmed',
    'depends': ['base', 'report'],
    'data': [

        'jv_menu.xml',
        'views/report_jv.xml',

    ],

    'installable': True,
    'auto_install': False,
}

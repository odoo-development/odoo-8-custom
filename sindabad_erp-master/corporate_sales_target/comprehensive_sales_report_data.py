from openerp import models, api
from openerp.osv import osv, fields
from openerp import api, fields, models


class ComprehensiveSalesReport(models.Model):
    _inherit = 'stock.picking'

    @api.model
    def _report_xls_fields_comprehensive(self):

        return [
            'order_date','order_create_date','shipment_date','purchase_ref','order','return','odoo_order','customer_name','company_name','company_code','classification','email','company_email','product_name','product_sku','product_category','delivered_qty','cost_rate_avg','total_cost','sales_rate','total_sales','profit','salesperson','invoice_number','inv_quantity','creation_date','invoice_value', 'return_invoice_number','return_invoice_value','return_invoice_creation_date','return_inv_quantity',
  'order_close_reason','close_value','closed_quantity','payment'
# 'x_custom_discount','x_discount','status','invoice_state',
        ]
    # Change/Add Template entries
    @api.model
    def _report_xls_template_comprehensive(self):

        return {}

# 1. Magento Order No
# 2. Payment Option
# 3. Order Placing Date
# 4. Purchase Order Ref.
# 5. Confirmation Date
# 6. Expected Shipping Date
# 7. Customer entity Name
# 8. Company Admin Email
# 9. Customer Email
# 10. Company Code
# 11. Customer Classification
# 12. RM Name
# 13. SKU
# 14. Product Name
# 15. Product Category
# 16. Unite of measure
# 17. Sale Rate
# 18. Order Qty
# 19. Closed Qty
# 20. Closed Value
# 21. Closing Date
# 22. Closing Reason
# 23. Invoice No
# 24. Invoice Date
# 25. Invoiced Qty
# 26. Invoice Value
# 27. Return Qty
# 28. Returned Value
# 29. Return Invoice No
# 30. Return Date
# 31. Returned Reason
# 32. Actual Delivery Date
# 33. Actual Delivered Qty
# 34. Actual Delivered Value
# 35. Payment Method
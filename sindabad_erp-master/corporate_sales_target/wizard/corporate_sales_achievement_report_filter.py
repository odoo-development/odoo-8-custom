from openerp.osv import fields, osv
import datetime
from openerp import api



class CorporateSalesAchievementReportWiz(osv.osv_memory):

    _name = 'corporate.sales.achievement.report.wiz'
    _description = 'Corporate Sales Achievement Report Wiz'

    def _target_rm_field_get(self, cr, uid, context=None):

        target_rm = self.pool.get('corporate.sales.target.rm')
        target = self.pool.get('corporate.sales.target')

        target_id = target.search(cr, uid, [], context=context)

        ids = target_rm.search(cr, uid,[('corporate_sales_target_rm_id','=',target_id )],context=context)

        res = []

        for field in target_rm.browse(cr, uid, ids, context=context):

            res.append((field.id, field.emp_name.name+" (Target - "+field.corporate_sales_target_rm_id.name+" )"))

        return res

    _columns = {
        'date_from': fields.date("From",default = lambda self: self.pool.get('corporate.sales.target').browse(self._cr, self._uid,self.pool.get('corporate.sales.target').search(self._cr, self._uid,[],context=self._context),context=self._context)[-1].date_from),
        'date_to': fields.date("To", default =  lambda self: self.pool.get('corporate.sales.target').browse(self._cr, self._uid,self.pool.get('corporate.sales.target').search(self._cr, self._uid,[],context=self._context),context=self._context)[-1].date_to),
        'type': fields.selection([('corporate', 'Corporate'),('retail', 'Retail')], 'Status',default = 'corporate'),
        'corporate_target': fields.many2one('corporate.sales.target', 'Corporate Target',default = lambda self: self.pool.get('corporate.sales.target').browse(self._cr, self._uid,self.pool.get('corporate.sales.target').search(self._cr, self._uid,[],context=self._context),context=self._context)[-1].id),
        'corporate_target_rm': fields.selection(_target_rm_field_get, "RM")

    }

    @api.onchange('corporate_target')
    def target_onchange(self):
        target_obj = self.pool.get('corporate.sales.target')
        corporate_target_id = self.corporate_target.id
        target_data = target_obj.browse(self._cr, self._uid,corporate_target_id,self._context)[0]

        self.date_from = target_data.date_from
        self.date_to = target_data.date_to

        return "---------"

    def _build_contexts(self, cr, uid, ids, data, context=None):
        if context is None:
            context = {}
        result = {'date_from': data['form']['date_from'],
                  'date_to': data['form']['date_to'],
                  'type': data['form']['type'],
                  'corporate_target': data['form']['corporate_target'],
                  'corporate_target_rm': data['form']['corporate_target_rm']}

        return result

    def check_report(self, cr, uid, ids, context=None):
        if context is None:
            context = {}

        data = self.read(cr, uid, ids, ['date_from','date_to','type','corporate_target','corporate_target_rm'], context=context)[0]

        datas = {
            'ids': context.get('active_ids', []),
            'model': 'corporate.sales.achievement.report.wiz',
            'form': data
        }
        return {'type': 'ir.actions.report.xml',
                'report_name': 'corporate.sales.achievement.xls',
                'datas': datas
                }

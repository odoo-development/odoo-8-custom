from openerp.osv import fields, osv
import datetime
from openerp import api



class CorporateSalesAchievementReportWiz(osv.osv_memory):

    _name = 'comprehensive.sales.report.wiz'
    _description = 'Comprehensive Sales Report Wiz'


    _columns = {
        'order_date_from': fields.datetime("Order Confirm From Date"),
        'order_date_to': fields.datetime("Order Confirm To Date"),
        'invoice_date_from': fields.datetime("Invoice From Date"),
        'invoice_date_to': fields.datetime("Invoice To Date"),

        'classification': fields.selection([
                                     ('Ananta', 'Ananta'),
                                     ('Corporate', 'Corporate'),
                                     ('Retail', 'Retail'),
                                     ('Employee', 'Employee'),
                                     ('General', 'General'),
                                     ('SOHO', 'SOHO')],
                                    'Customer Classification'),

        'company_name': fields.many2one('res.partner', 'Company'),
        'rm_name': fields.many2one('res.users', 'Sales Person')

    }
    _defaults = {
        'invoice_date_from': datetime.date.today(),
        'invoice_date_to': datetime.date.today()
    }


    def _build_contexts(self, cr, uid, ids, data, context=None):
        if context is None:
            context = {}
        result = {'order_date_from': data['form']['order_date_from'],
                  'order_date_to': data['form']['order_date_to'],
                  'invoice_date_from': data['form']['invoice_date_from'],
                  'invoice_date_to': data['form']['invoice_date_to'],
                  'classification': data['form']['classification'],
                  'company_name': data['form']['company_name'],
                  'rm_name': data['form']['rm_name']
                  }

        return result

    def check_report(self, cr, uid, ids, context=None):
        if context is None:
            context = {}

        data = self.read(cr, uid, ids, ['order_date_from','order_date_to','invoice_date_from','invoice_date_to',
                                        'classification','company_name','rm_name'], context=context)[0]
        datas = {
            'ids': context.get('active_ids', []),
            'model': 'comprehensive.sales.report.wiz',
            'form': data
        }
        return {'type': 'ir.actions.report.xml',
                'report_name': 'comprehensive.sales.xls',
                'datas': datas
                }

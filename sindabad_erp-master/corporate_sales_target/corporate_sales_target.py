from openerp.osv import fields, osv
from openerp import api
from openerp.tools.translate import _
from datetime import datetime,timedelta,date


class corporate_sales_target(osv.osv):

    _name = "corporate.sales.target"

    _columns = {
        'name': fields.char('Name'),
        'date_from': fields.date('Date From'),
        'date_to': fields.date('Date To'),
        'corporate_sales_amount': fields.float('Sales Amount'),
        'corporate_delivery_amount': fields.float('Delivery Amount'),

        'approve': fields.boolean('Approve'),
        'approve_by': fields.many2one('res.users', 'Approve By'),
        'approve_time': fields.datetime('Approve Date'),

        'cancel': fields.boolean('Cancel'),
        'cancel_by': fields.many2one('res.users', 'Cancel By'),
        'cancel_time': fields.datetime('Cancel Date'),

        'corporate_sales_target_rm_line': fields.one2many('corporate.sales.target.rm', 'corporate_sales_target_rm_id', 'Corporate Sales Target RM Line', required=True),

        'state': fields.selection([
            ('pending', 'Pending'),
            ('approve', 'Approve'),
            ('cancel', 'Cancel'),

        ], 'Status', help="Gives the status of sales target rm", select=True),

        'corporate_sales_target_holiday_line': fields.one2many('corporate.sales.target.holiday', 'corporate_sales_target_holiday_id', 'Sales Target Holiday Line'),

    }

    _defaults = {
        'date_from': fields.datetime.now,
        'date_to': fields.datetime.now,
        'user_id': lambda obj, cr, uid, context: uid,
        'state': 'pending',

    }

    def approve_corporate_sales_target(self, cr, uid, ids, context=None):

        for single_id in ids:
            approve_corporate_sales_target_query = "UPDATE corporate_sales_target SET state='approve', approve=TRUE, approve_by='{0}', approve_time='{1}' WHERE id={2}".format(
                uid, str(fields.datetime.now()), single_id)
            cr.execute(approve_corporate_sales_target_query)
            cr.commit()

            approve_corporate_sales_target_rm_query = "UPDATE corporate_sales_target_rm SET state='approve', approve=TRUE, approve_by='{0}', approve_time='{1}' WHERE corporate_sales_target_rm_id={2}".format(
                uid, str(fields.datetime.now()), single_id)
            cr.execute(approve_corporate_sales_target_rm_query)
            cr.commit()

        return True

    def cancel_corporate_sales_target(self, cr, uid, ids, context=None):

        for single_id in ids:
            cancel_corporate_sales_target_query = "UPDATE corporate_sales_target SET state='cancel', cancel=TRUE, cancel_by='{0}', cancel_time='{1}' WHERE id={2}".format(
                uid, str(fields.datetime.now()), single_id)
            cr.execute(cancel_corporate_sales_target_query)
            cr.commit()

            cancel_corporate_sales_target_rm_query = "UPDATE corporate_sales_target_rm SET state='cancel', cancel=TRUE, cancel_by='{0}', cancel_time='{1}' WHERE corporate_sales_target_rm_id={2}".format(
                uid, str(fields.datetime.now()), single_id)
            cr.execute(cancel_corporate_sales_target_rm_query)
            cr.commit()

        return True



    @api.model
    def create(self, vals):

        record = None

        amount = vals.get("corporate_sales_amount")
        total_amount = 0

        if vals.has_key('corporate_sales_target_rm_line'):
            for single_line in vals['corporate_sales_target_rm_line']:

                if not not single_line[2]:
                    total_amount = total_amount + single_line[2]["target_amount"]

        if amount != total_amount:
            raise osv.except_osv(_('Warning!!!'),
                                 _('Employees total target amount are must be equal as Sales target amount!!!'))
        else:
            record = super(corporate_sales_target, self).create(vals)
        target_from_date = datetime.strptime(record.date_from, '%Y-%m-%d')
        name = target_from_date.strftime("%b")+"-"+record.date_from.split('-')[0]

        record.name = name
        return record



class corporate_sales_target_rm(osv.osv):

    _name = "corporate.sales.target.rm"

    _columns = {

        'corporate_sales_target_rm_id': fields.many2one('corporate.sales.target', 'Sales Target RM ID', required=True, ondelete='cascade', select=True, readonly=True),

        'amount': fields.float('Amount'),

        'emp_name': fields.many2one('res.users', 'Employee Name'),
        'target_amount': fields.float('Target Sales Amount'),
        'target_delivery_amount': fields.float('Target Delivery Amount'),

        'approve': fields.boolean('Approve'),
        'approve_by': fields.many2one('res.users', 'Approve By'),
        'approve_time': fields.datetime('Approve Date'),

        'cancel': fields.boolean('Cancel'),
        'cancel_by': fields.many2one('res.users', 'Cancel By'),
        'cancel_time': fields.datetime('Cancel Date'),

        'state': fields.selection([
            ('pending', 'Pending'),
            ('approve', 'Approve'),
            ('cancel', 'Cancel'),

        ], 'Status', help="Gives the status of sales target rm", select=True),

    }


class corporate_sales_target_holiday(osv.osv):

    _name = "corporate.sales.target.holiday"

    _columns = {

        'corporate_sales_target_holiday_id': fields.many2one('corporate.sales.target', 'Sales Target Holiday ID', required=True, ondelete='cascade', select=True, readonly=True),

        'holiday': fields.date('Holiday'),

    }

from openerp import models, api
from openerp.osv import osv, fields
from openerp import api, fields, models


class CorporateSalesAchievementReport(models.Model):
    _inherit = 'sale.order'

    @api.model
    def _report_xls_fields_sales(self):

        return [
            'rm_name', 'assign_customer_count', 'customer_order_count',
            'current_run_rate','required_run_rate', 'mtd_sale', 'sales_target',
            'delivery_target', 'total_invoice_amount','total_return_amount','net_delivery_amount',
            'achievement_delivery','profit_margin'
        ]

    # Change/Add Template entries
    @api.model
    def _report_xls_template_sales(self):

        return {}


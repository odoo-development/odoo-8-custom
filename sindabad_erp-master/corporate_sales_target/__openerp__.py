{
    'name': 'Corporate Target',
    'version': '8.0.0.6.0',
    'author': "Shuvarthi",
    'category': 'Sales',
    'summary': 'Corporate Target',
    'depends': ['sale','stock', 'product','report_xls'],

    'data': [

        'security/corporate_sales_target_security.xml',
        'security/ir.model.access.csv',

        'wizard/corporate_sales_achievement_report_filter.xml',
        'wizard/comprehensive_sales_report_filter.xml',

        'report/corporate_sales_achievement_report_xls_view.xml',
        'report/comprehensive_sales_report_xls_view.xml',

        'views/corporate_sales_target_view.xml',

        'corporate_sales_target_menu.xml',

    ],

    'installable': True,
    'auto_install': False,
}

import xlwt
from datetime import datetime,timedelta
from openerp.osv import orm
from openerp.report import report_sxw
from openerp.addons.report_xls.report_xls import report_xls
from openerp.addons.report_xls.utils import rowcol_to_cell, _render
from openerp.tools.translate import translate, _
import logging
_logger = logging.getLogger(__name__)


_ir_translation_name = 'comprehensive.sales.xls'


class comprehensive_sales_xls_parser(report_sxw.rml_parse):

    def __init__(self, cr, uid, name, context):
        super(comprehensive_sales_xls_parser, self).__init__(
            cr, uid, name, context=context)
        move_obj = self.pool.get('stock.picking')
        self.context = context
        wanted_list = move_obj._report_xls_fields_comprehensive(cr, uid, context)
        template_changes = move_obj._report_xls_template_comprehensive(cr, uid, context)
        self.localcontext.update({
            'datetime': datetime,
            'wanted_list': wanted_list,
            'template_changes': template_changes,
            '_': self._,
        })

    def _(self, src):
        lang = self.context.get('lang', 'en_US')
        return translate(self.cr, _ir_translation_name, 'report', lang, src) \
            or src


class comprehensive_sales_xls(report_xls):

    def __init__(self, name, table, rml=False, parser=False, header=True,
                 store=False):
        super(comprehensive_sales_xls, self).__init__(
            name, table, rml, parser, header, store)

        # Cell Styles
        _xs = self.xls_styles
        # header
        rh_cell_format = _xs['bold'] + _xs['fill'] + _xs['borders_all']
        self.rh_cell_style = xlwt.easyxf(rh_cell_format)
        self.rh_cell_style_center = xlwt.easyxf(rh_cell_format + _xs['center'])
        self.rh_cell_style_right = xlwt.easyxf(rh_cell_format + _xs['right'])
        # lines
        aml_cell_format = _xs['borders_all']
        self.aml_cell_style = xlwt.easyxf(aml_cell_format)
        self.aml_cell_style_center = xlwt.easyxf(
            aml_cell_format + _xs['center'])
        self.aml_cell_style_date = xlwt.easyxf(
            aml_cell_format + _xs['left'],
            num_format_str=report_xls.date_format)
        self.aml_cell_style_decimal = xlwt.easyxf(
            aml_cell_format + _xs['right'],
            num_format_str=report_xls.decimal_format)
        # totals
        rt_cell_format = _xs['bold'] + _xs['fill'] + _xs['borders_all']
        self.rt_cell_style = xlwt.easyxf(rt_cell_format)
        self.rt_cell_style_right = xlwt.easyxf(rt_cell_format + _xs['right'])
        self.rt_cell_style_decimal = xlwt.easyxf(
            rt_cell_format + _xs['right'],
            num_format_str=report_xls.decimal_format)

        # XLS Template
        self.col_specs_template = {

            'order_date': {
                'header': [1, 20, 'text', _render("_('Confirmation Date')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'order_create_date': {
                'header': [1, 20, 'text', _render("_('Order Placing Date')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'shipment_date': {
                'header': [1, 20, 'text', _render("_('Expected shipping Date')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'purchase_ref': {
                'header': [1, 20, 'text', _render("_('Purchase Order Ref')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'order': {
                'header': [1, 20, 'text', _render("_('Magento Order No')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            # 'warehouse': {
            #     'header': [1, 20, 'text', _render("_('Warehouse')")],
            #     'lines': [1, 0, 'text', _render("''")],
            #     'totals': [1, 0, 'text', None]},

            'return': {
                'header': [1, 20, 'text', _render("_('Return')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'odoo_order': {
                'header': [1, 20, 'text', _render("_('Odoo Number')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'customer_name': {
                'header': [1, 20, 'text', _render("_('Customer Name')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},
            'company_name': {
                'header': [1, 20, 'text', _render("_('Company Name')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'company_code': {
                'header': [1, 20, 'text', _render("_('Company Code')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'company_email': {
                            'header': [1, 20, 'text', _render("_('Company Email')")],
                            'lines': [1, 0, 'text', _render("''")],
                            'totals': [1, 0, 'text', None]},

            'email': {
                'header': [1, 20, 'text', _render("_('Email')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'classification': {
                'header': [1, 20, 'text', _render("_('Classification')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'salesperson': {
                'header': [1, 20, 'text', _render("_('RM Name')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'product_name': {
                'header': [1, 20, 'text', _render("_('Product Name')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},
            'product_sku': {
                'header': [1, 20, 'text', _render("_('Product SKU')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'product_category': {
                'header': [1, 20, 'text', _render("_('Product Category')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'delivered_qty': {
                'header': [1, 20, 'text', _render("_('Quantity')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'cost_rate_avg': {
                'header': [1, 20, 'text', _render("_('Cost Rate')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'total_cost': {
                'header': [1, 20, 'text', _render("_('Total Cost')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'sales_rate': {
                'header': [1, 20, 'text', _render("_('Sale Rate')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'total_sales': {
                'header': [1, 20, 'text', _render("_('Total Sale')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'profit': {
                'header': [1, 20, 'text', _render("_('Profit/Loss')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'payment': {
                'header': [1, 20, 'text', _render("_('Payment Method')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'order_qty': {
                'header': [1, 20, 'text', _render("_('Order Quantity')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            # 'x_discount': {
            #     'header': [1, 20, 'text', _render("_('Campaign/Promotoion Discount')")],
            #     'lines': [1, 0, 'text', _render("''")],
            #     'totals': [1, 0, 'text', None]},

            'invoice_number': {
                'header': [1, 20, 'text', _render("_('Invoice Number')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'invoice_value': {
                'header': [1, 20, 'text', _render("_('Invoice Value')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'inv_quantity': {
                'header': [1, 20, 'text', _render("_('Invoice Quantity')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'creation_date': {
                'header': [1, 20, 'text', _render("_('Creation Date')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'order_close_reason': {
                'header': [1, 20, 'text', _render("_('Close Order Reason')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'close_value': {
                'header': [1, 20, 'text', _render("_('Invoice Value')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'closed_quantity': {
                'header': [1, 20, 'text', _render("_('Close Quantity')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'return_invoice_number': {
                'header': [1, 20, 'text', _render("_('Return Invoice Number')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'return_invoice_value': {
                'header': [1, 20, 'text', _render("_('Return Invoice Value')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'return_invoice_creation_date': {
                'header': [1, 20, 'text', _render("_('Return Creation Date')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'return_inv_quantity': {
                'header': [1, 20, 'text', _render("_('Return Quantity')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]}
        }


    def generate_xls_report(self, _p, _xs, data, objects, wb):

        wanted_list = _p.wanted_list
        self.col_specs_template.update(_p.template_changes)
        _ = _p._
        context = self.context
        today= datetime.today()
        today_from_date = today.strftime("%m-%d-%Y, 00:00:00")
        today_to_date = today.strftime("%m-%d-%Y, 23:59:59")
        if data.get('form').get('order_date_from') and data.get('form').get('order_date_to'):
            st_date = data.get('form').get('order_date_from')
            end_date = data.get('form').get('order_date_to')
        else:

            st_date = data.get('form').get('invoice_date_from') if data.get('form').get('invoice_date_from') else today_from_date
            end_date = data.get('form').get('invoice_date_to') if data.get('form').get('invoice_date_from') else today_to_date

        customer_classification = data.get('form').get('classification')
        company_id = data.get('form').get('company')
        rm_name = data.get('form').get('rm_name')

        DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"
        st_date = datetime.strptime(st_date, DATETIME_FORMAT)
        end_date = datetime.strptime(end_date, DATETIME_FORMAT)

        st_date = st_date+ timedelta(hours=6)
        end_date = end_date+ timedelta(hours=6)
        st_date = st_date.strftime("%m-%d-%Y, %H:%M:%S")
        end_date = end_date.strftime("%m-%d-%Y, %H:%M:%S")
        context['start_date'] = st_date
        context['end_date'] = end_date
        self.context = context
        date_range = _("Date: %s to %s" % (st_date, end_date))

        # report_name = objects[0]._description or objects[0]._name
        report_name = _("Comprehensive Sales Report ")
        ws = wb.add_sheet(report_name[:31])
        ws.panes_frozen = True
        ws.remove_splits = True
        ws.portrait = 0  # Landscape
        ws.fit_width_to_pages = 1
        row_pos = 0

        # set print header/footer
        ws.header_str = self.xls_headers['standard']
        ws.footer_str = self.xls_footers['standard']

        # Title
        cell_style = xlwt.easyxf(_xs['xls_title'])
        c_specs = [
            ('report_name', 4, 3, 'text', report_name),
        ]
        row_data = self.xls_row_template(c_specs, ['report_name'])
        row_pos = self.xls_write_row(
            ws, row_pos, row_data, row_style=cell_style)
        row_pos += 1

        # Date Ranges
        cell_style = xlwt.easyxf(_xs['xls_title'])
        c_specs = [
            ('date_range', 4, 3, 'text', date_range),
        ]
        row_data = self.xls_row_template(c_specs, ['date_range'])
        row_pos = self.xls_write_row(
            ws, row_pos, row_data, row_style=cell_style)
        row_pos += 1

        # Column headers
        c_specs = map(lambda x: self.render(
            x, self.col_specs_template, 'header', render_space={'_': _p._}),
            wanted_list)
        row_data = self.xls_row_template(c_specs, [x[0] for x in c_specs])
        row_pos = self.xls_write_row(
            ws, row_pos, row_data, row_style=self.rh_cell_style,
            set_column_size=True)
        ws.set_horz_split_pos(row_pos)
        sale_discount =0
        uid=1

        incoming_ids =[]
        in_query = "select id from  stock_picking_type where code='incoming'"
        self.cr.execute(in_query)
        in_objects = self.cr.dictfetchall()

        incoming_ids = [in_item.get('id') for in_item in in_objects]


        ### Following code has been added for 0 cost value update

        list_of_zero_cost_value_query='select product_id,count(*) as total_roq from stock_quant where cost =0 group by product_id'
        self.cr.execute(list_of_zero_cost_value_query)
        zero_data = self.cr.dictfetchall()

        for items in zero_data:
            p_id=items.get('product_id')
            partner_obj = self.pool("product.product")
            p_obj = partner_obj.browse(self.cr, uid, [p_id], context=context)[0]

            current_cost_price=p_obj.standard_price

            if current_cost_price == 0:
                current_cost_price = p_obj.average_purchaes_price


            query_for_product = "update stock_quant set cost=%s where cost=0 and product_id=%s "
            self.cr.execute(query_for_product, (current_cost_price, p_id))
            self.cr.commit()

        ### Ends code here for 0 cost value update

        if data.get('form').get('invoice_date_from'):
            query = "select stock_move.id as moved_id,stock_move.product_id,stock_picking.date_done,stock_picking.picking_type_id,stock_move.product_uom_qty,sale_order_line.price_unit,sale_order.order_close_reason AS order_close_reason,sale_order_line.closed_quantity AS closed_quantity,sale_order_line.x_custom_discount,account_invoice.amount_total AS invoice_value,sale_order_line.order_partner_id,sale_order.client_order_ref,sale_order.date_confirm,sale_order.create_date as order_create_date,sale_order_line.name, sale_order.name as odoo_no,sale_order.note AS payment,sale_order.x_purchase_ref_cus  AS purchase_ref,sale_order.magetno_delivery_at  AS shipment_date,sale_order.custom_salesperson,sale_order.customer_classification  AS classification,(select product_category.name from product_category,product_template,product_product where product_category.id = product_template.categ_id and product_template.id = product_product.product_tmpl_id and product_product.id = stock_move.product_id ) as category,(select sum((sq.qty*sm.price_unit)) as total from stock_quant_move_rel as sqm,stock_move as sm, stock_quant as sq where sqm.quant_id in ((select sq2.id from stock_quant_move_rel as sqmr2, stock_quant as sq2 where sqmr2.move_id=stock_move.id and sqmr2.quant_id = sq2.id)) and sqm.move_id = sm.id and sm.purchase_line_id >0 and sq.id=sqm.quant_id group by sq.product_id)as total_price ,sale_order_line.product_uom_qty as order_qty ,account_invoice.number as invoice_number, account_invoice.date_invoice as invoice_date, account_invoice.create_date as create_date,account_invoice.delivery_date,sale_order_line.x_discount,(SELECT res_partner.name FROM res_partner,res_users WHERE res_partner.id = res_users.partner_id and res_users.id=sale_order.custom_salesperson) AS salesperson from stock_move,stock_picking,sale_order_line,sale_order,account_invoice where stock_move.picking_id=stock_picking.id and stock_move.sale_line_id = sale_order_line.id and sale_order_line.order_id = sale_order.id  and stock_picking.name = account_invoice.origin and account_invoice.state != 'cancel'  and account_invoice.state != 'draft' and type in ('out_invoice', 'out_refund') and account_invoice.create_date >= %s and account_invoice.create_date <= %s"

        else:
            query = "select stock_move.id as moved_id,stock_move.product_id,stock_picking.date_done,stock_picking.picking_type_id,stock_move.product_uom_qty,sale_order_line.price_unit,sale_order.order_close_reason AS order_close_reason,sale_order_line.closed_quantity AS closed_quantity,sale_order_line.x_custom_discount,sale_order_line.order_partner_id,sale_order.client_order_ref,sale_order.date_confirm,sale_order.create_date as order_create_date,sale_order_line.name, sale_order.name as odoo_no,sale_order.note AS payment,sale_order.x_purchase_ref_cus  AS purchase_ref,sale_order.magetno_delivery_at  AS shipment_date,sale_order.custom_salesperson,sale_order.customer_classification  AS classification,account_invoice.amount_total AS invoice_value,(select product_category.name from product_category,product_template,product_product where product_category.id = product_template.categ_id and product_template.id = product_product.product_tmpl_id and product_product.id = stock_move.product_id ) as category,(select sum((sq.qty*sm.price_unit)) as total from stock_quant_move_rel as sqm,stock_move as sm, stock_quant as sq where sqm.quant_id in ((select sq2.id from stock_quant_move_rel as sqmr2, stock_quant as sq2 where sqmr2.move_id=stock_move.id and sqmr2.quant_id = sq2.id)) and sqm.move_id = sm.id and sm.purchase_line_id >0 and sq.id=sqm.quant_id group by sq.product_id)as total_price,sale_order_line.product_uom_qty as order_qty,account_invoice.number as invoice_number, account_invoice.date_invoice as invoice_date, account_invoice.create_date as create_date, account_invoice.delivery_date,sale_order_line.x_discount,(SELECT res_partner.name FROM res_partner,res_users WHERE res_partner.id = res_users.partner_id and res_users.id=sale_order.custom_salesperson) AS salesperson from stock_move,stock_picking,sale_order_line,sale_order,account_invoice where stock_move.picking_id=stock_picking.id and stock_move.sale_line_id = sale_order_line.id and sale_order_line.order_id = sale_order.id  and stock_picking.name = account_invoice.origin and account_invoice.state != 'cancel'  and account_invoice.state != 'draft' and type in ('out_invoice', 'out_refund') and sale_order.date_confirm >= %s and sale_order.date_confirm <= %s"


        query_parameters = (st_date, end_date)

        found_classificatoion=0

        if customer_classification is not None and customer_classification is not False:
            found_classificatoion=1
            customer_classification = customer_classification + str('%')
            query = query + "and sale_order.customer_classification like %s "
            query_parameters = (st_date, end_date,customer_classification)

        if rm_name is not None and rm_name is not False:
            rm_name = rm_name + str('%')
            query = query + "and sale_order.custom_salesperson like %s "
            query_parameters = (st_date, end_date,rm_name)

        if company_id is not None and company_id is not False:
            p_ids=[]
            company_id=company_id[0]
            p_ids.append(company_id)
            partner_obj = self.pool("res.partner").browse(self.cr, uid, [company_id], context=context)

            for p_obj in partner_obj.commercial_partner_id.child_ids:
                p_ids.append(p_obj.id)

            query = query + "and sale_order_line.order_partner_id in %s "


            if found_classificatoion == 1:
                query_parameters = (st_date, end_date, customer_classification,tuple(p_ids))
            else:
                query_parameters = (st_date, end_date, tuple(p_ids))

        self.cr.execute(query,query_parameters)
        objects = self.cr.dictfetchall()

        partner_list =[]
        product_id_list =[]
        order_num_list =[]
        final_product_list ={}
        final_company_list ={}

        partner_obj = self.pool("res.partner")

        # user_ids = partner_obj.search(cr, uid, domain, context=context)
        for items in objects:
            partner_list.append(items.get('order_partner_id'))
            product_id_list.append(items.get('product_id'))

        product_id_list = list(dict.fromkeys(product_id_list))
        partner_list = list(dict.fromkeys(partner_list))

        query_for_product = "select id,name_template,default_code from product_product where id in  %s"

        self.cr.execute(query_for_product, (tuple(product_id_list),))
        products = self.cr.dictfetchall()

        for p_items in products:
            final_product_list[p_items.get('id')] = p_items

        p_obj = partner_obj.browse(self.cr, uid, partner_list, context=context)

        for p_items in p_obj:
            if p_items.parent_id:
                final_company_list[p_items.id] = {
                    'customer_name':p_items.name,
                    'company_name':p_items.parent_id.name,
                    'company_code':p_items.parent_code,
                    'email':p_items.email,
                    'company_email':p_items.parent_id.email,
                }
            else:
                final_company_list[p_items.id] = {
                    'customer_name': p_items.name,
                    'company_name': p_items.parent_id.name,
                    'email': p_items.parent_id.email if p_items.parent_id.email else p_items.email,
                    'company_code': p_items.parent_id.parent_code,
                    'company_email': p_items.parent_id.email,
                }


        # Sale Order lines
        for line in objects:


            c_specs = map(
                lambda x: self.render(x, self.col_specs_template, 'lines'),
                wanted_list)
            cal_cost=0

            if line.get('total_price') is not None:
                cal_cost  = line.get('total_price')

            if cal_cost ==0:
                get_adjusted_cost_query = "select sum((sq.qty * sm.price_unit)) as total from stock_quant_move_rel as sqm,stock_move as sm, stock_quant as sq where sqm.quant_id in ((select sq2.id from stock_quant_move_rel as sqmr2, stock_quant as sq2 where sqmr2.move_id=%s and sqmr2.quant_id = sq2.id)) and sqm.move_id = sm.id and sm.inventory_id >0 and sq.id=sqm.quant_id group by sq.product_id"
                moved_id = line.get('moved_id')
                self.cr.execute(get_adjusted_cost_query, ([moved_id]))
                get_adjusted_cost = self.cr.dictfetchall()
                for a_cost in get_adjusted_cost:
                    if a_cost.get('total') is not None:
                        cal_cost = a_cost.get('total')


            #  Floowing chekup for sq qty is zero then
            if cal_cost ==0:

                get_adjusted_query="select sq.cost,sm.price_unit from stock_quant_move_rel as sqm,stock_move as sm, stock_quant as sq where sqm.quant_id in (select sq2.id from stock_quant_move_rel as sqmr2, stock_quant as sq2 where sqmr2.move_id=%s and sqmr2.quant_id = sq2.id) and sqm.move_id = sm.id  and sq.id=sqm.quant_id and (sm.purchase_line_id >0 or sm.inventory_id >0) ORDER BY sm.id desc limit 1"
                moved_id = line.get('moved_id')
                self.cr.execute(get_adjusted_query, ([moved_id]))
                get_adjusted_cost = self.cr.dictfetchall()
                for a_cost in get_adjusted_cost:
                    if a_cost.get('price_unit') is not None:
                        cal_cost = a_cost.get('price_unit') * line.get('product_uom_qty')
                    elif a_cost.get('cost') is not None:
                        cal_cost = a_cost.get('cost') * line.get('product_uom_qty')


            for list_data in c_specs:

                if str(list_data[0]) == str('order'):

                    list_data[4] = str(line.get('client_order_ref'))


                if str(list_data[0]) == str('order_date'):
                    list_data[4] = str(line.get('date_confirm'))

                if str(list_data[0]) == str('order_create_date'):
                    list_data[4] = str(line.get('order_create_date'))

                if str(list_data[0]) == str('shipment_date'):
                    list_data[4] = str(line.get('shipment_date'))

                if str(list_data[0]) == str('invoice_number'):
                    if line.get('picking_type_id') not in incoming_ids:
                        invoice_number = str(line.get('invoice_number'))
                        list_data[4] = str(invoice_number.replace("None", ""))

                """
                if str(list_data[0]) == str('invoice_date'):
                    invoice_date = str(line.get('invoice_date'))
                    list_data[4] = str(invoice_date.replace("None", ""))
                """

                if str(list_data[0]) == str('creation_date'):
                    if line.get('picking_type_id') not in incoming_ids:
                        creation_date = str(line.get('create_date'))
                        list_data[4] = str(creation_date.replace("None", ""))

                if str(list_data[0]) == str('invoice_value'):
                    if line.get('picking_type_id') not in incoming_ids:
                        invoice_value = str(line.get('invoice_value'))
                        list_data[4] = str(invoice_value.replace("None", ""))

                if str(list_data[0]) == str('inv_quantity'):
                    if line.get('picking_type_id') not in incoming_ids:
                        inv_quantity = str(line.get('product_uom_qty'))
                        list_data[4] = str(inv_quantity.replace("None", ""))

                if str(list_data[0]) == str('return'):
                    if line.get('picking_type_id') in incoming_ids:
                        list_data[4] = str('Return')

                if str(list_data[0]) == str('return_invoice_number'):
                    if line.get('picking_type_id') in incoming_ids:
                        invoice_number = str(line.get('invoice_number'))
                        list_data[4] = str(invoice_number.replace("None", ""))

                if str(list_data[0]) == str('return_invoice_creation_date'):
                    if line.get('picking_type_id') in incoming_ids:
                        creation_date = str(line.get('create_date'))
                        list_data[4] = str(creation_date.replace("None", ""))

                if str(list_data[0]) == str('return_invoice_value'):
                    if line.get('picking_type_id') in incoming_ids:
                        invoice_value = str(line.get('invoice_value'))
                        list_data[4] = str(invoice_value.replace("None", ""))

                if str(list_data[0]) == str('return_inv_quantity'):
                    if line.get('picking_type_id') in incoming_ids:
                        inv_quantity = str(line.get('product_uom_qty'))
                        list_data[4] = str(inv_quantity.replace("None", ""))

                if str(list_data[0]) == str('close_value'):
                    if line.get('closed_quantity'):
                        close_value = str(line.get('closed_quantity') * line.get('price_unit'))
                        list_data[4] = str(close_value.replace("None", ""))

                if str(list_data[0]) == str('closed_quantity'):
                    closed_quantity = str(line.get('closed_quantity'))
                    list_data[4] = str(closed_quantity.replace("None", ""))

                if str(list_data[0]) == str('order_close_reason'):
                    if line.get('closed_quantity'):
                        order_close_reason = str(line.get('order_close_reason'))
                        list_data[4] = str(order_close_reason.replace("None", ""))
                    else:
                        list_data[4] = ''


                if str(list_data[0]) == str('product_name'):
                    product_id = line.get('product_id')
                    abc = final_product_list.get(product_id)
                    list_data[4] = str(abc.get('name_template'))

                if str(list_data[0]) == str('product_sku'):
                    product_id = line.get('product_id')
                    abc = final_product_list.get(product_id)
                    list_data[4] = str(abc.get('default_code'))


                if str(list_data[0]) == str('product_category'):

                    list_data[4] = str(line.get('category'))


                if str(list_data[0]) == str('purchase_ref'):

                    list_data[4] = str(line.get('purchase_ref'))


                # if str(list_data[0]) == str('warehouse'):
                #
                #     list_data[4] = str(line.get('warhouse_name'))

                if str(list_data[0]) == str('customer_name'):
                    product_id = line.get('order_partner_id')
                    abc = final_company_list.get(product_id)
                    list_data[4] = str(abc.get('customer_name'))

                if str(list_data[0]) == str('email'):
                    product_id = line.get('order_partner_id')
                    abc = final_company_list.get(product_id)
                    email = str(abc.get('email'))
                    list_data[4] = str(email.replace("False", ""))

                if str(list_data[0]) == str('company_name'):
                    product_id = line.get('order_partner_id')
                    abc = final_company_list.get(product_id)
                    company_name = str(abc.get('company_name'))
                    list_data[4] = str(company_name.replace("False", ""))

                if str(list_data[0]) == str('company_email'):
                    partner_id = line.get('order_partner_id')
                    abc = final_company_list.get(partner_id)
                    company_email = str(abc.get('company_email'))
                    list_data[4] = str(company_email.replace("None", ""))
                    list_data[4] = str(company_email.replace("False", ""))

                if str(list_data[0]) == str('company_code'):
                    product_id = line.get('order_partner_id')
                    abc = final_company_list.get(product_id)
                    company_code = str(abc.get('company_code'))
                    list_data[4] = str(company_code.replace("False", ""))

                if str(list_data[0]) == str('classification'):
                    classification = str(line.get('classification'))
                    list_data[4] = str(classification.replace("False", ""))

                if str(list_data[0]) == str('salesperson'):
                    salesperson = str(line.get('salesperson'))
                    list_data[4] = str(salesperson.replace("False", ""))

                if str(list_data[0]) == str('delivered_qty'):
                    list_data[4] = str(line.get('product_uom_qty'))

                if str(list_data[0]) == str('purchase_ref'):
                    list_data[4] = str(line.get('purchase_ref'))

                if str(list_data[0]) == str('payment'):
                    list_data[4] = str(line.get('payment'))

                if str(list_data[0]) == str('order_qty'):
                    list_data[4] = str(line.get('order_qty'))

                # if str(list_data[0]) == str('x_custom_discount'):
                #     try:
                #         if line.get('product_uom_qty') > 0:
                #             list_data[4] = str(round((line.get('x_custom_discount')*line.get('product_uom_qty')),2))
                #         else:
                #             list_data[4] = str('0')
                #     except:
                #         list_data[4] = str('0')


                if str(list_data[0]) == str('cost_rate_avg'):
                    if line.get('product_uom_qty') >0:
                        calculation_of_rate = cal_cost / line.get('product_uom_qty')
                    else:
                        calculation_of_rate = cal_cost
                    list_data[4] = str(calculation_of_rate)




                if str(list_data[0]) == str('total_cost'):
                    if line.get('picking_type_id') in incoming_ids:
                        list_data[4] = str(cal_cost*-1)
                    else:
                        list_data[4] = str(cal_cost)


                if str(list_data[0]) == str('sales_rate'):
                    list_data[4] = str(line.get('price_unit'))


                if str(list_data[0]) == str('total_sales'):
                    if line.get('picking_type_id') in incoming_ids:
                        total_sale = str(line.get('price_unit') * line.get('product_uom_qty') *-1)
                    else:
                        total_sale = str(line.get('price_unit') * line.get('product_uom_qty'))
                    list_data[4] = str(total_sale)

                if str(list_data[0]) == str('order_date'):
                    list_data[4] = str(line.get('date_confirm'))

                if str(list_data[0]) == str('odoo_order'):
                    list_data[4] = str(line.get('odoo_no'))


                if str(list_data[0]) == str('profit'):
                    profit = 0
                    total_sale = line.get('price_unit') * line.get('product_uom_qty')
                    total_cost = cal_cost
                    profit = total_sale - total_cost
                    if line.get('picking_type_id') in incoming_ids:
                        profit = profit * -1

                    list_data[4] = str(round(profit,2))


            row_data = self.xls_row_template(c_specs, [x[0] for x in c_specs])
            row_pos = self.xls_write_row(
                ws, row_pos, row_data, row_style=self.aml_cell_style)


        # For discount Write
        discount_text = 'Total Discount : '+str(sale_discount)

        cell_style = xlwt.easyxf(_xs['xls_title'])
        c_specs = [
            ('sale_discount', 4, 3, 'text', str(discount_text)),
        ]
        row_data = self.xls_row_template(c_specs, ['sale_discount'])
        row_pos = self.xls_write_row(
            ws, row_pos, row_data, row_style=cell_style)
        row_pos += 1




comprehensive_sales_xls('report.comprehensive.sales.xls',
                    'stock.picking',
                    parser=comprehensive_sales_xls_parser)

import xlwt
from datetime import datetime,timedelta,date
from openerp.osv import orm,osv
from openerp.report import report_sxw
from openerp.addons.report_xls.report_xls import report_xls
from openerp.addons.report_xls.utils import rowcol_to_cell, _render
from openerp.tools.translate import translate, _
from dateutil.relativedelta import relativedelta
import logging
from itertools import groupby
from operator import itemgetter


_logger = logging.getLogger(__name__)


_ir_translation_name = 'corporate.sales.achievement.xls'


class corporate_sales_achievement_xls_parser(report_sxw.rml_parse):

    def __init__(self, cr, uid, name, context):
        super(corporate_sales_achievement_xls_parser, self).__init__(
            cr, uid, name, context=context)
        move_obj = self.pool.get('sale.order')
        self.context = context
        wanted_list = move_obj._report_xls_fields_sales(cr, uid, context)
        template_changes = move_obj._report_xls_template_sales(cr, uid, context)
        self.localcontext.update({
            'datetime': datetime,
            'wanted_list': wanted_list,
            'template_changes': template_changes,
            '_': self._,
        })

    def _(self, src):
        lang = self.context.get('lang', 'en_US')
        return translate(self.cr, _ir_translation_name, 'report', lang, src) \
            or src


class corporate_sales_achievement_xls(report_xls):

    def __init__(self, name, table, rml=False, parser=False, header=True,
                 store=False):
        super(corporate_sales_achievement_xls, self).__init__(
            name, table, rml, parser, header, store)

        # Cell Styles
        _xs = self.xls_styles
        # header
        rh_cell_format = _xs['bold'] + _xs['fill'] + _xs['borders_all']
        self.rh_cell_style = xlwt.easyxf(rh_cell_format)
        self.rh_cell_style_center = xlwt.easyxf(rh_cell_format + _xs['center'])
        self.rh_cell_style_right = xlwt.easyxf(rh_cell_format + _xs['right'])
        # lines
        aml_cell_format = _xs['borders_all']
        self.aml_cell_style = xlwt.easyxf(aml_cell_format)
        self.aml_cell_style_center = xlwt.easyxf(
            aml_cell_format + _xs['center'])
        self.aml_cell_style_date = xlwt.easyxf(
            aml_cell_format + _xs['left'],
            num_format_str=report_xls.date_format)
        self.aml_cell_style_decimal = xlwt.easyxf(
            aml_cell_format + _xs['right'],
            num_format_str=report_xls.decimal_format)
        # totals
        rt_cell_format = _xs['bold'] + _xs['fill'] + _xs['borders_all']
        self.rt_cell_style = xlwt.easyxf(rt_cell_format)
        self.rt_cell_style_right = xlwt.easyxf(rt_cell_format + _xs['right'])
        self.rt_cell_style_decimal = xlwt.easyxf(
            rt_cell_format + _xs['right'],
            num_format_str=report_xls.decimal_format)

        # XLS Template
        self.col_specs_template = {
            'rm_name': {
                'header': [1, 20, 'text', _render("_('RM Name')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},
            'assign_customer_count': {
                'header': [1, 20, 'text', _render("_('No of Assigned Customers')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},
            'customer_order_count': {
                'header': [1, 20, 'text', _render("_('No of Customers Given Order')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},
            'current_run_rate': {
                'header': [1, 20, 'text', _render("_('Current Run Rate (Per Day)')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},
            'required_run_rate': {
                'header': [1, 20, 'text', _render("_('Required Run Rate (Per Day)')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},
            'mtd_sale': {
                'header': [1, 20, 'text', _render("_('Sales Order')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},
            'sales_target': {
                'header': [1, 20, 'text', _render("_('Sales Target')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},
            'delivery_target': {
                'header': [1, 20, 'text', _render("_('Delivery Target')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},
            'total_invoice_amount': {
                'header': [1, 20, 'text', _render("_('Invoiced Amount')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},
            'total_return_amount': {
                'header': [1, 20, 'text', _render("_('Returned Amount ')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},
            'net_delivery_amount': {
                'header': [1, 20, 'text', _render("_('Net Delivery')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},
            'achievement_delivery': {
                'header': [1, 20, 'text', _render("_('Achievement (Delivery)(%) ')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},
            'profit_margin': {
                'header': [1, 20, 'text', _render("_('Profit Margin (%) (Estimated)')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},


        }

    def daterange(self,date1, date2):
        for n in range(int((date2 - date1).days) + 1):
            yield date1 + timedelta(n)

    def generate_xls_report(self, _p, _xs, data, objects, wb):

        wanted_list = _p.wanted_list
        self.col_specs_template.update(_p.template_changes)
        _ = _p._

        context = self.context
        from_date= data.get('form').get('date_from')
        st_date = data.get('form').get('date_to')
        type = data.get('form').get('type')
        corporate_target_id = data.get('form').get('corporate_target')
        corporate_target_rm = data.get('form').get('corporate_target_rm')
        context['start_date'] = from_date
        context['end_date'] = st_date

        to_date = datetime.strptime(st_date, '%Y-%m-%d')
        from_date_obj = datetime.strptime(from_date, '%Y-%m-%d')

        from_date_time = from_date + ' 00:00:00'
        st_date_time = st_date + ' 23:59:59'

        self.context = context
        date_range = _("Date: %s to %s" % (from_date,st_date))

        # report_name = objects[0]._description or objects[0]._name
        report_name = _("Sales Corporate Vs Achievement Report")
        ws = wb.add_sheet(report_name[:31])
        ws.panes_frozen = True
        ws.remove_splits = True
        ws.portrait = 0  # Landscape
        ws.fit_width_to_pages = 1
        row_pos = 0

        # set print header/footer
        ws.header_str = self.xls_headers['standard']
        ws.footer_str = self.xls_footers['standard']

        # Title
        cell_style = xlwt.easyxf(_xs['xls_title'])
        c_specs = [
            ('report_name', 2, 2, 'text', report_name),
            ('date_range', 1, 1, 'text', date_range),
        ]
        row_data = self.xls_row_template(c_specs, ['report_name','date_range'])
        row_pos = self.xls_write_row(
            ws, row_pos, row_data, row_style=cell_style)
        row_pos += 1

        c_specs = map(lambda x: self.render(
            x, self.col_specs_template, 'header', render_space={'_': _p._}),
                      wanted_list)
        row_data = self.xls_row_template(c_specs, [x[0] for x in c_specs])
        row_pos = self.xls_write_row(
            ws, row_pos, row_data, row_style=self.rh_cell_style,
            set_column_size=True)
        ws.set_horz_split_pos(row_pos)

        traget_date_from = datetime.today()
        traget_date_to = datetime.today()

        traget_query = "select * from corporate_sales_target WHERE id = %s"
        self.cr.execute(traget_query, (corporate_target_id[0],))
        target_list = self.cr.dictfetchall()
        if target_list:
            traget_date_from = target_list[0]['date_from']
            traget_date_to = target_list[0]['date_to']

            traget_date_from = datetime.strptime(traget_date_from, '%Y-%m-%d')
            traget_date_to = datetime.strptime(traget_date_to, '%Y-%m-%d')


        date_duration = to_date - from_date_obj

        if date_duration.days != 0:
            date_duration_count = date_duration.days
        else:
            date_duration_count = 1

        if traget_date_from.month == traget_date_to.month == to_date.month == from_date_obj.month:
            last_date_duration_count = 1
            last_date_duration = traget_date_to - to_date

            if last_date_duration.days != 0:
                last_date_duration = last_date_duration.days
                if last_date_duration == 0:
                    last_date_duration_count = 1

            else:
                last_date_duration_count = 1
        else:
            raise osv.except_osv(_('warning!'), _('Please select the correct date'))

        final_data = {}


        #### target amount ######
        if corporate_target_rm:
            target_amount_query = "SELECT sum(corporate_sales_target_rm.target_amount) AS target_sales_amount,sum(corporate_sales_target_rm.target_delivery_amount) AS target_delivery_amount,corporate_sales_target_rm.emp_name FROM corporate_sales_target,corporate_sales_target_rm WHERE corporate_sales_target.id = corporate_sales_target_rm.corporate_sales_target_rm_id and corporate_sales_target.id = %s AND corporate_sales_target_rm.id = %s GROUP BY corporate_sales_target_rm.emp_name"

            self.cr.execute(target_amount_query, (corporate_target_id[0],corporate_target_rm))

        else:
            target_amount_query = "SELECT sum(corporate_sales_target_rm.target_amount) AS target_sales_amount,sum(corporate_sales_target_rm.target_delivery_amount) AS target_delivery_amount,corporate_sales_target_rm.emp_name FROM corporate_sales_target,corporate_sales_target_rm WHERE corporate_sales_target.id = corporate_sales_target_rm.corporate_sales_target_rm_id and corporate_sales_target.id = %s GROUP BY corporate_sales_target_rm.emp_name"

            self.cr.execute(target_amount_query, (corporate_target_id[0],))
        target_data_list = self.cr.dictfetchall()

        if len(target_data_list)== 0:
            raise osv.except_osv(_('warning!'), _('Please select the correct date'))

        target_data_dict={}

        for target_data in target_data_list:
            target_data_dict[target_data['emp_name']] = {
                'target_sales_amount':target_data['target_sales_amount'],
                'target_delivery_amount':target_data['target_delivery_amount']
            }

        ##########################

        #####  RM wise Get classification partner ID Lists  #####
        rm_partner_list = []
        corporate_partner_id_list = []
        corporate_partner_dict = {}
        retail_partner_id_list = []
        general_partner_id_list = []
        corporate_company_id_list = []
        retail_company_id_list = []

        partner_obj = self.pool.get("res.partner")
        for items in target_data_list:
            rm_partner_list.append(items.get('emp_name'))

        p_obj = partner_obj.browse(self.cr, self.uid,
                                   partner_obj.search(self.cr,self.uid, [('user_id','in',rm_partner_list),
                                                                         ('customer','=',True),('is_company','=',True)],context=self.context),context=context)

        assign_company_list=[{'company_id':p_items.id,'company_rm_id':p_items.user_id.id,'rm_name':p_items.user_id.name} for p_items in p_obj]

        rm_name = None
        assign_company_list = sorted(assign_company_list,
                                           key=itemgetter('company_rm_id'))
        for key2, value2 in groupby(assign_company_list, key=itemgetter('company_rm_id')):

            assign_company = []
            for k2 in value2:
                assign_company.append(k2['company_id'])
                rm_name = k2['rm_name']

            corporate_partner_dict[key2] = {'assign_company_count': len(list(set(assign_company))),
                               'rm_name': rm_name}

        ##############################################################

        query = "SELECT count(*) as order_count, sum(sale_order.amount_total) AS order_value, sale_order.custom_salesperson , sale_order.partner_id FROM sale_order WHERE sale_order.state not IN ('draft', 'sent', 'cancel', 'new_cus_approval_pending','approval_pending','check_pending') AND sale_order.date_confirm >= %s AND sale_order.date_confirm <= %s AND sale_order.custom_salesperson IN %s GROUP BY custom_salesperson, partner_id"


        # if type == 'corporate':
        self.cr.execute(query, (from_date, st_date, tuple(rm_partner_list)))
        sale_order_list = self.cr.dictfetchall()

        # else:
        #     self.cr.execute(query, (from_date, st_date, tuple(retail_partner_id_list)))
        #     sale_order_list = self.cr.dictfetchall()


        sale_order_list = sorted(sale_order_list,
                                 key=itemgetter('custom_salesperson'))


        for key, value in groupby(sale_order_list, key=itemgetter('custom_salesperson')):
            mtd_sales = 0.0
            customer_order_list = []
            for k in value:
                mtd_sales = mtd_sales + k['order_value']
                customer_order_list.append(k['partner_id'])


            final_data[key] = {'mtd_sale':mtd_sales,
                               'customer_order_count': len(list(set(customer_order_list))),
                               'rm_id': key}

        in_query = "select id from  stock_picking_type where code='incoming'"
        self.cr.execute(in_query)
        incoming_objects = self.cr.dictfetchall()

        incoming_ids = [in_item.get('id') for in_item in incoming_objects]

        delivered_sales="SELECT stock_picking.picking_type_id,sale_order.custom_salesperson as rm_id,(sale_order_line.price_unit)as price_unit,(stock_move.product_uom_qty) as product_uom_qty,(SELECT sum((sq.qty * sm.price_unit)) AS total FROM stock_quant_move_rel AS sqm, stock_move AS sm, stock_quant AS sq WHERE sqm.quant_id IN ((SELECT sq2.id FROM stock_quant_move_rel AS sqmr2, stock_quant AS sq2 WHERE sqmr2.move_id = stock_move.id AND sqmr2.quant_id = sq2.id)) AND sqm.move_id = sm.id AND sm.purchase_line_id > 0 AND sq.id = sqm.quant_id GROUP BY sq.product_id) AS total_price FROM stock_move, stock_picking, sale_order_line, sale_order, account_invoice WHERE stock_move.picking_id = stock_picking.id AND stock_move.sale_line_id = sale_order_line.id AND sale_order_line.order_id = sale_order.id AND stock_picking.name = account_invoice.origin AND account_invoice.state != 'cancel' AND account_invoice.state != 'draft' AND type IN ('out_invoice', 'out_refund') AND account_invoice.create_date >= %s AND account_invoice.create_date <= %s AND sale_order.custom_salesperson IN %s"

        self.cr.execute(delivered_sales, (from_date_time, st_date_time,tuple(rm_partner_list)))
        delivered_sales_data_list = self.cr.dictfetchall()



        for delivered_sales_data in delivered_sales_data_list:

            cal_cost = delivered_sales_data.get('total_price')
            moved_id = delivered_sales_data.get('moved_id')

            if cal_cost == 0:

                get_adjusted_cost_query = "select sum((sq.qty * sm.price_unit)) as total from stock_quant_move_rel as sqm,stock_move as sm, stock_quant as sq where sqm.quant_id in ((select sq2.id from stock_quant_move_rel as sqmr2, stock_quant as sq2 where sqmr2.move_id=%s and sqmr2.quant_id = sq2.id)) and sqm.move_id = sm.id and sm.inventory_id >0 and sq.id=sqm.quant_id group by sq.product_id"

                self.cr.execute(get_adjusted_cost_query, ([moved_id]))
                get_adjusted_cost = self.cr.dictfetchall()
                for a_cost in get_adjusted_cost:
                    if a_cost.get('total') is not None:
                        cal_cost = a_cost.get('total')

            #  Floowing chekup for sq qty is zero then
            if cal_cost == 0:

                get_adjusted_query = "select sq.cost,sm.price_unit from stock_quant_move_rel as sqm,stock_move as sm, stock_quant as sq where sqm.quant_id in (select sq2.id from stock_quant_move_rel as sqmr2, stock_quant as sq2 where sqmr2.move_id=%s and sqmr2.quant_id = sq2.id) and sqm.move_id = sm.id  and sq.id=sqm.quant_id and (sm.purchase_line_id >0 or sm.inventory_id >0) ORDER BY sm.id desc limit 1"

                self.cr.execute(get_adjusted_query, ([moved_id]))
                get_adjusted_cost = self.cr.dictfetchall()
                for a_cost in get_adjusted_cost:
                    if a_cost.get('price_unit') is not None:
                        cal_cost = a_cost.get('price_unit') * delivered_sales_data.get('product_uom_qty')
                    elif a_cost.get('cost') is not None:
                        cal_cost = a_cost.get('cost') * delivered_sales_data.get('product_uom_qty')

            total_cost = cal_cost
            if total_cost:
                total_cost=total_cost
            else:
                total_cost=0
            delivered_sales_data['total_refund'] = 0.0
            delivered_sales_data['total_sale'] = 0.0

            if delivered_sales_data.get('picking_type_id') in incoming_ids:

                total_sale = round((delivered_sales_data.get('price_unit') * delivered_sales_data.get('product_uom_qty')),2)
                sale_profit = round(((total_sale - total_cost) * (-1)), 2)

                delivered_sales_data['total_refund']= total_sale

            else:
                total_sale = round((delivered_sales_data.get('price_unit') * delivered_sales_data.get('product_uom_qty')),2)

                sale_profit = round((total_sale - total_cost),2)

                # total_delivery_sale = total_delivery_sale + total_sale

                delivered_sales_data['total_sale'] = total_sale

            delivered_sales_data['profit'] = sale_profit

        delivered_sales_data_list = sorted(delivered_sales_data_list,
                                           key=itemgetter('rm_id'))


        ###########################################

        ############# sales order #########
        delivery_data ={}

        for key1, value1 in groupby(delivered_sales_data_list, key=itemgetter('rm_id')):
            mtd_total_sale = 0.0
            mtd_total_refund = 0.0
            mtd_total_profit = 0.0
            for k1 in value1:
                mtd_total_sale = mtd_total_sale + k1['total_sale']
                mtd_total_refund = mtd_total_refund + k1['total_refund']
                mtd_total_profit = mtd_total_profit + k1['profit']

            delivery_data[key1]={'mtd_total_sale':mtd_total_sale,
                                 'mtd_total_refund': mtd_total_refund,
                                 'net_delivery': mtd_total_sale - mtd_total_refund,
                                 'profit_margin': mtd_total_profit
                                 }

        data_list = []

        for row in rm_partner_list:

            result = {'rm_name':corporate_partner_dict[row]['rm_name'],
                      'assign_customer_count':corporate_partner_dict[row]['assign_company_count'],
                      'customer_order_count':final_data[row]['customer_order_count'] if final_data.get(row) else 0,
                      # 'current_run_rate':str(round(final_data[row]['mtd_sale'] /date_duration_count)) if final_data.get(row) else 0,
                      # 'required_run_rate':str(round((target_data_dict[row]['target_sales_amount'] - final_data[row]['mtd_sale'])/last_date_duration_count)) if final_data.get(row) else 0,
                      'current_run_rate':0.00,
                      'required_run_rate':0.00,
                      'mtd_sale':final_data[row]['mtd_sale'] if final_data.get(row) else 0,
                      'sales_target':target_data_dict[row]['target_sales_amount'],
                      'delivery_target':target_data_dict[row]['target_delivery_amount'],
                      'total_invoice_amount':0.00,
                      'total_return_amount':0.00,
                      'net_delivery_amount':0.00,
                      'achievement_delivery':0.00,
                      'profit_margin':0.00
                      }

            if delivery_data.has_key(row):
                net_delivery = round(delivery_data[row]['mtd_total_sale'] - delivery_data[row]['mtd_total_refund'])
                result.update({'total_invoice_amount':str(round(delivery_data[row]['mtd_total_sale'])),
                      'total_return_amount':str(round(delivery_data[row]['mtd_total_refund'])),
                      'net_delivery_amount':str(round(delivery_data[row]['mtd_total_sale'] - delivery_data[row]['mtd_total_refund'])),
                      'achievement_delivery':str(round(((delivery_data[row]['mtd_total_sale'] - delivery_data[row]['mtd_total_refund'])/target_data_dict[row]['target_delivery_amount'])*100))+'%',
                      'profit_margin':str(round((delivery_data[row]['profit_margin']/(delivery_data[row]['mtd_total_sale'] - delivery_data[row]['mtd_total_refund']))*100))+'%',
                      'current_run_rate': str(round(net_delivery / date_duration_count)) if net_delivery else 0,
                      'required_run_rate': str(
                                   round((target_data_dict[row]['target_sales_amount'] - net_delivery) / last_date_duration_count)) if final_data.get(row) else 0,
                               })

            data_list.append(result)

        ##################################



        for data_items in data_list:
            c_specs = map(
                lambda x: self.render(x, self.col_specs_template, 'lines'),
                wanted_list)

            for list_data in c_specs:

                if str(list_data[0]) == str('rm_name'):
                    list_data[4] = str(data_items.get('rm_name'))

                if str(list_data[0]) == str('assign_customer_count'):
                    list_data[4] = str(data_items.get('assign_customer_count'))

                if str(list_data[0]) == str('customer_order_count'):
                    list_data[4] = str(data_items.get('customer_order_count'))

                if str(list_data[0]) == str('current_run_rate'):
                    list_data[4] = str(data_items.get('current_run_rate'))

                if str(list_data[0]) == str('required_run_rate'):
                    list_data[4] = str(data_items.get('required_run_rate'))

                if str(list_data[0]) == str('mtd_sale'):
                    list_data[4] = str(data_items.get('mtd_sale'))

                if str(list_data[0]) == str('sales_target'):
                    list_data[4] = str(data_items.get('sales_target'))

                if str(list_data[0]) == str('delivery_target'):
                    list_data[4] = str(data_items.get('delivery_target'))

                if str(list_data[0]) == str('total_invoice_amount'):
                    list_data[4] = str(data_items.get('total_invoice_amount'))

                if str(list_data[0]) == str('total_return_amount'):
                    list_data[4] = str(data_items.get('total_return_amount'))

                if str(list_data[0]) == str('net_delivery_amount'):
                    list_data[4] = str(data_items.get('net_delivery_amount'))

                if str(list_data[0]) == str('achievement_delivery'):
                    list_data[4] = str(data_items.get('achievement_delivery'))

                if str(list_data[0]) == str('profit_margin'):
                    list_data[4] = str(data_items.get('profit_margin'))

            row_data = self.xls_row_template(c_specs, [x[0] for x in c_specs])
            row_pos = self.xls_write_row(
                ws, row_pos, row_data, row_style=self.aml_cell_style)


corporate_sales_achievement_xls('report.corporate.sales.achievement.xls',
                    'sale.order',
                    parser=corporate_sales_achievement_xls_parser)

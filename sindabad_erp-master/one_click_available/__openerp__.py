{
    'name': 'One Click Available SO',
    'version': '1.0',
    'category': 'One Click Available SO',
    'author': 'Shahjalal',
    'summary': 'Stock',
    'description': 'One Click Available SO',
    'depends': ['base', 'sale', 'sale_stock', 'stock'],
    'data': [
        'wizard/refresh_wizard.xml',
        'wizard/force_calculation.xml'
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}

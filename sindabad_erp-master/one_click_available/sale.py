from datetime import datetime, timedelta, date
from dateutil import relativedelta
import json
import time
import datetime

from openerp import SUPERUSER_ID, models, fields, api
from openerp.osv import fields, osv
from openerp.tools.translate import _
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT
import openerp.addons.decimal_precision as dp
from openerp import workflow

from openerp.tools.float_utils import float_compare, float_round
from openerp.exceptions import Warning
from openerp import SUPERUSER_ID, api
from openerp.addons.procurement import procurement
import logging


class sale_order(osv.osv):

    _inherit = ["sale.order"]

    def action_button_confirm(self, cr, uid, ids, context=None):

        if not context:
            context = {}
        assert len(ids) == 1, 'This option should only be used for a single id at a time.'
        self.signal_workflow(cr, uid, ids, 'order_confirm')
        if context.get('send_email'):
            self.force_quotation_send(cr, uid, ids, context=context)

        self.action_view_delivery(cr, uid, ids, context)

        ## Insert Confirmation Log



        # try:
        #     s_uid=1
        #     abc=self.browse(cr, uid, ids[0], context=context)
        #     data = {
        #         'odoo_order_id': ids[0],
        #         'magento_id': str(abc.client_order_ref),
        #         'order_state': str('processing'),
        #         'state_time': fields.datetime.now(),
        #     }
        #     new_r = self.pool["order.status.synch.log"].create(cr, s_uid, data)
        # except:
        #     pass

        ## Ends Here

        return True

    def action_view_delivery(self, cr, uid, ids, context=None):
        '''
        This function returns an action that display existing delivery orders
        of given sales order ids. It can either be a in a list or in a form
        view, if there is only one delivery order to show.
        '''

        mod_obj = self.pool.get('ir.model.data')
        act_obj = self.pool.get('ir.actions.act_window')

        result = mod_obj.get_object_reference(cr, uid, 'stock', 'action_picking_tree_all')
        id = result and result[1] or False
        result = act_obj.read(cr, uid, [id], context=context)[0]

        # compute the number of delivery orders to display
        pick_ids = []
        for so in self.browse(cr, uid, ids, context=context):
            pick_ids += [picking.id for picking in so.picking_ids]

        # choose the view_mode accordingly
        if len(pick_ids) > 1:
            result['domain'] = "[('id','in',[" + ','.join(map(str, pick_ids)) + "])]"
        else:
            res = mod_obj.get_object_reference(cr, uid, 'stock', 'view_picking_form')
            result['views'] = [(res and res[1] or False, 'form')]
            result['res_id'] = pick_ids and pick_ids[0] or False

        if len(pick_ids) == 1 and (str(so.state) == "draft" or str(so.state) == "progress"):
            self.pool.get('stock.picking').action_assign(cr, uid, pick_ids, context=context)

        return result


class stock_picking(osv.osv):
    _inherit = "stock.picking"

    def waiting_refresh_action(self, cr, uid, ids=[], context=None):

        last_few_days_order_query = "select id,origin,state from stock_picking where state='waiting'"

        cr.execute(last_few_days_order_query)
        all_order_origin_data = cr.fetchall()



        if len(all_order_origin_data) > 0:

            all_so_id_list = list()
            for all_order_origin in all_order_origin_data:

                if str(all_order_origin[1]).startswith('SO'):
                    all_so_id_list.append(all_order_origin)

            context = {
                'lang': 'en_US',
                'tz': 'Asia/Dhaka',
                'uid': uid,
                'active_model': 'stock.picking.type',
                'search_default_picking_type_id': [2],
                'default_picking_type_id': 2,
                'search_default_waiting': 1,
                'params': {'action': 379},
                'search_disable_custom_filters': True,
                'contact_display': 'partner_address',
                'active_ids': [2],
                'active_id': 2
            }


            for so_id in all_so_id_list:
                stock_picking_id = int(so_id[0])


                if str(so_id[2]) == 'partially_available' or str(so_id[2]) == 'confirmed':
                    #     # unreserve
                    self.pool.get('stock.picking').do_unreserve(cr, uid, int(stock_picking_id), context=context)

                    # action cancel
                    self.pool.get('stock.picking').action_assign(cr, uid, int(stock_picking_id), context=context)

                if str(so_id[2]) == 'waiting':
                    # Rereservation
                    self.pool.get('stock.picking').rereserve_pick(cr, uid, [stock_picking_id], context=context)
        return True


    def refresh_action(self, cr, uid, ids, context=None):

        try:
            with open('/var/log/odoo/cron-run-time.log', 'a+') as f:
                right_now = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
                f.write("Auto Refresh Action: Start time: " + right_now + "\n")
        except:
            pass

        # 90 days but not before 1st July
        date_threshold = 90

        now = date.today()
        first_july = date(2018, 7, 1)
        date_delta = now - first_july

        if int(date_delta.days) > date_threshold:

            last_few_days_order_query = "SELECT DISTINCT ON (origin) id, origin, state FROM stock_picking WHERE date > current_date - interval '"+str(date_threshold)+"' day AND state != 'done' AND state != 'cancel';"
        else:
            last_few_days_order_query = "SELECT DISTINCT ON (origin) id, origin, state FROM stock_picking WHERE date > '"+str(first_july)+"' AND state != 'done' AND state != 'cancel';"

        cr.execute(last_few_days_order_query)
        all_order_origin_data = cr.fetchall()

        if len(all_order_origin_data) > 0:

            all_so_id_list = list()
            for all_order_origin in all_order_origin_data:

                if str(all_order_origin[1]).startswith('SO'):
                    all_so_id_list.append(all_order_origin)

            context = {
                'lang': 'en_US',
                'tz': 'Asia/Dhaka',
                'uid': uid,
                'active_model': 'stock.picking.type',
                'search_default_picking_type_id': [2],
                'default_picking_type_id': 2,
                'search_default_waiting': 1,
                'params': {'action': 379},
                'search_disable_custom_filters': True,
                'contact_display': 'partner_address',
                'active_ids': [2],
                'active_id': 2
            }


            for so_id in all_so_id_list:
                stock_picking_id = int(so_id[0])


                if str(so_id[2]) == 'partially_available' or str(so_id[2]) == 'confirmed':
                    #     # unreserve
                    self.pool.get('stock.picking').do_unreserve(cr, uid, int(stock_picking_id), context=context)

                    # action cancel
                    self.pool.get('stock.picking').action_assign(cr, uid, int(stock_picking_id), context=context)

                if str(so_id[2]) == 'waiting':
                    # Rereservation
                    self.pool.get('stock.picking').rereserve_pick(cr, uid, [stock_picking_id], context=context)

        try:
            with open('/var/log/odoo/cron-run-time.log', 'a+') as f:
                right_now = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
                f.write("Auto Refresh Action: End time: " + right_now + "\n")
        except:
            pass

        return True


    def force_calculation(self, cr, uid, ids, context=None):

        if uid != 1:
            raise osv.except_osv(_('Warning!'),
                                 _('You do not have any access. Only IT team can do it'))


        active_id_list = context.get('active_ids')
        stock_list = self.browse(cr, uid, active_id_list, context=context)

        quant_ids =[]
        for abc in stock_list:
            for move_line in abc.move_lines:
                for quant in move_line.quant_ids:
                    quant_ids.append(quant.id)

        for quant_id in quant_ids:
            cancel_expense_query_line = "UPDATE stock_quant SET total_calculated_cost=0 WHERE id={0}".format(quant_id)
            cr.execute(cancel_expense_query_line)
            cr.commit()


        for abc in stock_list:
            for move_line in abc.move_lines:
                for quant in move_line.quant_ids:
                    data =quant.inventory_value




        return True
{
    'name': 'Default Invoice Date',
    'version': '1.0',
    'category': 'Default Invoice Date',
    'author': 'Shahjalal',
    'summary': 'Stock',
    'description': 'Default Invoice Date',
    'depends': ['base', 'sale', 'sale_stock', 'stock', 'stock_account'],
    'installable': True,
    'application': True,
    'auto_install': False,
}

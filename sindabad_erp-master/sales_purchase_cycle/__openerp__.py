# Author Mufti Muntasir Ahmed 25/03/2018


{
    'name': 'Sales and purchase Cycle',
    'version': '1.0.0',
    'category': 'Sales',
    'description': """
Order Process Cycle from shipment to dispatch

""",
    'author': 'Mufti Muntasir Ahmed',
    'depends': ['sale'],
    'data': [

        'sales_menu.xml',
        'wizard/sales_purchase_report_cycle_view.xml',
        'views/report_salespurchasecycle.xml',




    ],

    'installable': True,
    'auto_install': False,
}
#Author Mufti Muntasir Ahmed 25/03/2018


import time
from openerp.report import report_sxw
from openerp.osv import osv
from datetime import datetime


class HistoryData(report_sxw.rml_parse):

    def __init__(self, cr, uid, name, context):
        super(HistoryData, self).__init__(cr, uid, name, context)
        self.localcontext.update({
            'get_client_order_ref': '',


        })


    def set_context(self, objects, data, ids, report_type=None):
        data_list=[]

        test_data={1:'100'}
        ananta_group_ids = [2444,3599,2972,3623,2803,1092,2730,2425,4229,1724,4386,480,2843,5710,649,4746,948]


        start_date = data.get('form').get('date_from')
        end_date = data.get('form').get('date_to')
        sale_order = self.pool['sale.order']
        sale_orders = sale_order.search(self.cr,self.uid,
                                        [('date_order', '<=', end_date),
                                         ('date_order', '>=', start_date), ('partner_id', 'in', ananta_group_ids)])
        sales_list = sale_order.browse(self.cr,self.uid,sale_orders)
        count_data=0



        for so_items in sales_list:
            count_data = count_data + 1
            temp_dict = {}
            temp_dict['sl']= count_data
            temp_dict['so_date']= so_items.date_order
            temp_dict['order_number'] = so_items.name
            temp_dict['customer'] = so_items.partner_id.name
            temp_dict['customer_address'] = so_items.partner_invoice_id.city

            so_date = so_items.date_order

            delivery_order = self.pool['stock.picking']
            query_dispatched = delivery_order.search(self.cr,self.uid,[('origin','=',so_items.name)])

            dispatched_list = delivery_order.browse(self.cr,self.uid,query_dispatched)

            distpatch_ids = [ items.id for items in dispatched_list ]
            distpatch_name = [ items.name for items in dispatched_list ]

            ### History for PO

            po_order_lines = so_items.purchase_line_ids

            po_name_list=[]
            po_id_list=[]
            po_vendor_name = ''
            po_number = ''

            for po_line in po_order_lines:

                po_name = po_line.order_id.name


                if po_name not in po_name_list:
                    po_name_list.append(po_name)
                    po_id_list = po_line.order_id.id
                    po_vendor_name += str(po_line.order_id.partner_id.name) + ', '
                    po_number += str(po_line.order_id.name) + ', '


            mail_message = self.pool['mail.message']
            po_histories = mail_message.search(self.cr, self.uid,
                                                 [('model', '=', 'purchase.order'), ('res_id', '=', po_id_list),
                                                  ('record_name', 'in', po_name_list)])

            po_history_lists = mail_message.browse(self.cr, self.uid, po_histories)

            rfq_created = ''
            rfq_confirm = ''
            inv_paid = ''
            grn_rec = ''
            inv_rec = ''
            temp_dict['vendor_name'] = po_vendor_name
            temp_dict['po_number'] = po_number

            po_place_date=None
            po_place_date_flag=0

            grn_receive_date =None
            grn_receive_date_flag =0

            for items in po_history_lists:

                if 'Invoice paid' in items.body:
                    inv_paid += str(items.write_date) + ', '

                if 'RFQ created' in items.body:
                    rfq_created += str(items.write_date) + ', '

                    if po_place_date_flag ==0:
                        po_place_date=items.write_date
                        po_place_date_flag=1

                if 'RFQ Confirmed' in items.body:
                    rfq_confirm += str(items.write_date) + ', '

                if 'Products received' in items.body:
                    grn_rec += str(items.write_date) + ', '

                if 'Invoice received' in items.body:
                    inv_rec += str(items.write_date) + ', '

                    if grn_receive_date_flag ==0:
                        grn_receive_date=items.write_date
                        grn_receive_date_flag=1

            temp_dict['rfq_created'] = rfq_created
            temp_dict['rfq_confirm'] = rfq_confirm
            temp_dict['inv_paid'] = inv_paid
            temp_dict['grn_rec'] = grn_rec
            temp_dict['inv_rec'] = inv_rec

            ## History for PO Ends here

            ### History For Delivery

            mail_message_list = self.pool['mail.message']
            delivery_histories = mail_message_list.search(self.cr, self.uid,
                                                 [('model', '=', 'stock.picking'), ('res_id', 'in', distpatch_ids),
                                                  ('record_name', 'in', distpatch_name)])

            delivery_histories = mail_message_list.browse(self.cr, self.uid, delivery_histories)

            final_distpatch_date = ''
            first_distpatch_date = ''


            for picking in delivery_histories:

                if 'Ready to Transfer' in picking.body:
                    final_distpatch_date +=str(picking.write_date) + ', '

                if 'Picking List created' in picking.body:
                    first_distpatch_date +=str(picking.write_date) + ', '

            temp_dict['delivery_creation'] = first_distpatch_date
            temp_dict['delivery_transfer'] = final_distpatch_date

            #### History For Invoice

            categories_name =''
            cat_ids=[]


            for order_line in so_items.order_line:
                if order_line.product_id.categ_id not in cat_ids:
                    cat_ids.append(order_line.product_id.categ_id)
                    categories_name += str(order_line.product_id.categ_id.name) + ', '

            temp_dict['product_categories'] = categories_name


            invoice_ids=[inv.id for inv in so_items.invoice_ids]

            invoice_creaed = ''
            invoice_paid =''

            mail_message_list = self.pool['mail.message']
            invoice_histories = mail_message_list.search(self.cr, self.uid,
                                                          [('model', '=', 'account.invoice'), ('res_id', 'in', invoice_ids)])

            invoice_his_list = mail_message_list.browse(self.cr, self.uid, invoice_histories)

            invoice_date = None
            invoice_date_flag=0


            for items in invoice_his_list:

                if 'Invoice created' in items.body:
                    invoice_creaed +=str(items.write_date) + ', '

                if 'Invoice paid' in items.body:
                    invoice_paid += str(items.write_date) + ', '

                    if invoice_date_flag ==0:
                        invoice_date=items.write_date
                        invoice_date_flag=1

                temp_dict['invoice_created'] = invoice_creaed
                temp_dict['invoice_paid'] = invoice_paid



            if so_date and invoice_date:
                date_diff = datetime.strptime(invoice_date, '%Y-%m-%d %H:%M:%S') - datetime.strptime(so_date, '%Y-%m-%d %H:%M:%S')
                days, hours, minutes = date_diff.days, date_diff.seconds // 3600, date_diff.seconds // 60 % 60
                temp_dict['so_to_dis'] = str(days) + ' d ' + str(hours) + ' h '+ str(minutes) +' min'

            if po_place_date and grn_receive_date:
                date_diff = datetime.strptime(grn_receive_date, '%Y-%m-%d %H:%M:%S') - datetime.strptime(po_place_date, '%Y-%m-%d %H:%M:%S')
                days, hours, minutes = date_diff.days, date_diff.seconds // 3600, date_diff.seconds // 60 % 60
                temp_dict['po_to_grn'] = str(days) + ' d ' + str(hours) + ' h '+ str(minutes) +' min'

            if invoice_date and grn_receive_date:
                date_diff = datetime.strptime(invoice_date, '%Y-%m-%d %H:%M:%S') - datetime.strptime(grn_receive_date, '%Y-%m-%d %H:%M:%S')
                days, hours, minutes = date_diff.days, date_diff.seconds // 3600, date_diff.seconds // 60 % 60

                temp_dict['grn_to_dis'] = str(days) + ' d ' + str(hours) + ' h '+ str(minutes) +' min'

            if invoice_date and grn_receive_date:
                temp_dict['o_status']='Full Cycle Complete'
            elif invoice_date:
                temp_dict['o_status'] = 'Delivered'
            else:
                temp_dict['o_status'] = 'Pending'



            data_list.append(temp_dict)







        self.localcontext.update({

            'getLines': {1:data_list},
            'start_date': start_date,
            'end_date': end_date,

        })


        return super(HistoryData, self).set_context(objects, data, ids, report_type)



class report_salespurchasecycle(osv.AbstractModel):
    _name = 'report.sales_purchase_cycle.report_salespurchasecycle'
    _inherit = 'report.abstract_report'
    _template = 'sales_purchase_cycle.report_salespurchasecycle'
    _wrapped_report_class = HistoryData

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:

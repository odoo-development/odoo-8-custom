## Author Mufti Muntasir Ahmed 06-07-2019




import re
import xmlrpclib
import json
import requests
import logging
from openerp.osv import fields, osv
from openerp.tools.translate import _
from openerp import SUPERUSER_ID, api
from openerp.addons.base.res.res_partner import _lang_get
from openerp.exceptions import Warning
from openerp.http import request
from ..odoo_to_magento_api_connect.api_connect import get_magento_token, submit_request
from datetime import datetime, timedelta, date
import datetime


XMLRPC_API = '/index.php/api/xmlrpc'

_logger = logging.getLogger(__name__)

class synch_log(osv.osv):
    _name="stock.synch.log"

    _columns = {
        'entry_log_time': fields.datetime("Entry Log Time"),
        'stock_sync_log': fields.text("Stock Synchronization Log"),
        'response_from_magento': fields.text("Failed Response List"),
        'total_sku_from_odoo': fields.text("Total # of SKU sent from Odoo"),
    }

class product_product(osv.osv):
    _inherit = "product.product"


    def _get_pending_qty(self, cr, uid, ids, name, arg, context=None):
        res = {}
        for record in ids:

            order_ids = self.pool.get('sale.order.line').search(cr, uid, [('product_id','=',record),('state', 'in', ['draft','approval_pending','check_pending','new_cus_approval_pending'])])
            order_ids_obj = self.pool.get('sale.order.line').browse(cr, uid, order_ids, context=context)
            total_pending_qty=0
            for items in order_ids_obj:
                total_pending_qty += items.product_uom_qty

            res[record] = total_pending_qty
        return res



    def _get_synch_qty(self, cr, uid, ids, name, arg, context=None):
        res={}

        p_objs = self.browse(cr, uid, ids, context)

        synch_qty = 0
        for prod_item in p_objs:
            synch_qty =  prod_item.qty_available - prod_item.pending_qty - prod_item.outgoing_qty - prod_item.damage_quantity
            if synch_qty <=0:
                synch_qty=0
            res[prod_item.id]=synch_qty

        return res

    def chunks(self, l, n):
        """Yield successive n-sized chunks from l."""
        for i in xrange(0, len(l), n):
            yield l[i:i + n]

    def stock_synch(self, cr, uid,all_data=False,chunk=1, context=None):
        try:
            with open('/var/log/odoo/cron-run-time.log', 'a+') as f:
                right_now = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
                f.write("Auto Stock Sync: Start time: " + right_now + "\n")
        except:
            pass

        # If chunk 1 then range would be (1 to 1000)
        # If chunk 2 then range would be (1001 to 2000)
        chunk=chunk



        # Start product list query From here
        # tomorrow = datetime.datetime.now() + datetime.timedelta(days=1)
        # cron_id = 29  ## It is for night
        # update_query = "UPDATE ir_cron SET nextcall='{0}' WHERE id={1}".format(tomorrow.strftime('%Y-%m-%d %H:%M:%S'), cron_id)
        # cr.execute(update_query)
        # cr.commit()



        product_obj = self.pool.get('product.product')

        product_id_lists_all = list()
        if not all_data:
            # product_id_lists = product_obj.search(cr, uid, [('stock_sync_req', '=', True)])
            product_id_lists_all = product_obj.search(cr, uid,
                                                      ['&', ('qty_available', '>', 0), ('stock_sync_req', '=', True)])

            # product_id_query = "SELECT id FROM product_product WHERE stock_sync_req = TRUE"
            # cr.execute(product_id_query)
            # for p_id in cr.fetchall():
            #     product_id_lists_all.append(p_id[0])
            
        else:
            # product_id_lists = product_obj.search(cr, uid, [('id', '=', all_data['ids'][0])])
            product_id_lists_all.append(all_data['ids'][0])

        if chunk == 1:
            product_id_lists_all = product_id_lists_all[0:1000]

        else:
            st_fm = (chunk * 1000)-1000
            end_fm = (chunk * 1000)
            product_id_lists_all = product_id_lists_all[st_fm:end_fm]

        sms_text = "[Before ]Counting 1000 chunk({0}) : Starts (in Live)".format(chunk)

        name = "stock.sync"
        # self.pool.get("send.sms.on.demand").send_sms_on_demand(cr, uid, product_id_lists_all, sms_text, "+8801817535299, +8801716520313", name, context=context)


        if len(product_id_lists_all) == 0:
            return True


        product_id_lists_chunks = [x for x in self.chunks(product_id_lists_all, 1000)]


        sms_text = "Counting 1000 chunk({0}) : Starts (in Live)".format(chunk)

        name = "stock.sync"
        # self.pool.get("send.sms.on.demand").send_sms_on_demand(cr, uid, product_id_lists_all, sms_text, "+8801817535299, +8801716520313", name, context=context)

        counter = 1
        for product_id_lists in product_id_lists_chunks:
            try:

                products_obj_line = product_obj.browse(cr, uid, product_id_lists)

                mapping_ids = self.pool.get('magento.product').search(
                    cr, uid, [('oe_product_id', 'in', product_id_lists)])

                mapping_objects = self.pool.get(
                    'magento.product').browse(cr, uid, mapping_ids)

                odoo_magento_dict ={} # keys are odoo id and values are magento ID

                for items in mapping_objects:
                    odoo_magento_dict[items.oe_product_id] = items.mag_product_id

                magento_updated_qty = {} # keys are magento id and values are qty to update

                stored_list =[]


                for prod_item in products_obj_line:
                    qty=0

                    if odoo_magento_dict.get(prod_item.id) is not None:
                        tmp={}
                        # qty = prod_item.qty_available - prod_item.pending_qty - prod_item.outgoing_qty
                        qty = (prod_item.qty_available - prod_item.pending_qty - prod_item.outgoing_qty) - prod_item.damage_quantity
                        qty =qty if qty >0 else 0

                        tmp['qty']=qty
                        tmp['threshold_qty']=prod_item.reorder_qty_level

                        stored_list.append({
                            'sku':prod_item.default_code,
                            'qty':qty,
                            'threshold_qty':prod_item.reorder_qty
                        })

                        magento_updated_qty[odoo_magento_dict.get(prod_item.id)] = tmp




                qty=0
                stock=True
                count=0


                success_story = []
                success_story_2 = []
                sale_order_ids = []
                token, url_root = get_magento_token(self, cr, uid, sale_order_ids)

                token = token.replace('"', "")
                headers = {'Authorization': token, 'Content-Type': 'application/json'}
                stock_list =[]

                for mage_product_id,tmp in magento_updated_qty.items():

                    ## Calling server starts from
                    sale_order_ids=[]

                    if int(tmp.get('qty')) > 0:
                        stock_list.append({
                            "product_id": mage_product_id,
                            "stock_item": {

                                        "qty": tmp.get('qty'),
                                        "is_in_stock": stock,
                                        "extension_attributes": {
                                            "threshold_qty": tmp.get('threshold_qty')
                                        }
                                    }
                        })
                    else:
                        stock_list.append({
                            "product_id": mage_product_id,
                            "stock_item": {

                                        "qty": tmp.get('qty'),
                                        "extension_attributes": {
                                            "threshold_qty": tmp.get('threshold_qty')
                                        }
                                    }
                        })


                product_stock_data = {"stockItems": stock_list}


                url = url_root + "/index.php/rest/V1/odoomagentoconnect/productStockBulk/"
                data = json.dumps(product_stock_data)

                resp = ''
                try:
                    resp = requests.post(url, data=data, headers=headers)

                    sms_text = "Count {0} ({1}, {2}) : sucess (in Live)".format(counter, len(product_id_lists_all), len(product_id_lists))
                    name = "stock.sync"
                    # self.pool.get("send.sms.on.demand").send_sms_on_demand(cr, uid, product_id_lists, sms_text, "+8801817535299, +8801716520313", name, context=context)
                except:
                    sms_text = "Count {0} ({1}, {2}) : fail (in Live)".format(counter, len(product_id_lists_all), len(product_id_lists))
                    name = "stock.sync"
                    # self.pool.get("send.sms.on.demand").send_sms_on_demand(cr, uid, product_id_lists, sms_text, "+8801817535299, +8801716520313", name, context=context)

                    pass
                # SMS
                counter += 1

                log_data ={
                    'entry_log_time':fields.datetime.now(),
                    'stock_sync_log': str(stored_list),
                    'response_from_magento': str(resp),
                    'total_sku_from_odoo': str(len(stored_list))
                }

                try:
                    stock_log = self.pool['stock.synch.log'].create(cr,uid,log_data)
                except:
                    sms_text = "Log entry error (in Live)"
                    name = "stock.sync"
                    # self.pool.get("send.sms.on.demand").send_sms_on_demand(cr, uid, product_id_lists, sms_text, "+8801817535299, +8801716520313", name, context=context)
                    pass
                    # success_story.append(mage_product_id)
                    # success_story_2.append(resp)


                    # By getting rsponses data ends here
            except Exception:
                sms_text = "Failed Chunk {0}".format(len(product_id_lists_all))
                name = "stock.sync"
                # self.pool.get("send.sms.on.demand").send_sms_on_demand(cr, uid, product_id_lists_all, sms_text, "+8801817535299, +8801716520313", name, context=context)
            finally:
                sms_text = "It has entered the Final Method and called the data"
                name = "stock.sync"
                # self.pool.get("send.sms.on.demand").send_sms_on_demand(cr, uid, product_id_lists_all, sms_text, "+8801817535299, +8801716520313", name, context=context)
                return True

        try:
            with open('/var/log/odoo/cron-run-time.log', 'a+') as f:
                right_now = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
                f.write("Auto Stock Sync: End time: " + right_now + "\n")
        except:
            pass

        sms_text = "Counting ({0}) : Ends (in Live)".format(len(product_id_lists_all))
        name = "stock.sync"
        # self.pool.get("send.sms.on.demand").send_sms_on_demand(cr, uid, product_id_lists_all, sms_text, "+8801817535299, +8801716520313", name, context=context)


        return True


    def core_sync(self, cr, uid, ids, context=None):

        all_data = dict()
        all_data['ids'] = ids

        self.stock_synch(cr, uid, all_data, context=context)

        return True

    def stock_synch_250(self, cr, uid,all_data=False, context=None):

        # Start product list query From here

        product_obj = self.pool.get('product.product')

        # product_id_lists_all = list()
        # if not all_data:
            # product_id_lists = product_obj.search(cr, uid, [('stock_sync_req', '=', True)])
        # product_id_query = "SELECT id FROM product_product WHERE stock_sync_req = TRUE"
        # cr.execute(product_id_query)
        #for p_id in cr.fetchall():
        #    product_id_lists_all.append(p_id[0])
            
        # else:
            # product_id_lists = product_obj.search(cr, uid, [('id', '=', all_data['ids'][0])])
        #     product_id_lists_all.append(all_data['ids'][0])

        product_id_lists_all = product_obj.search(cr, uid, ['&', ('qty_available', '>', 0), ('stock_sync_req', '=', True)])
        product_id_lists_chunks = [x for x in self.chunks(product_id_lists_all, 1000)]

        sms_text = "Counting ({0}) : Starts with Button".format(len(product_id_lists_all))
        name = "stock.sync"
        # self.pool.get("send.sms.on.demand").send_sms_on_demand(cr, uid, product_id_lists_all, sms_text, "+8801817535299, +8801716520313", name, context=context)

        counter = 1
        for product_id_lists in product_id_lists_chunks:

            products_obj_line = product_obj.browse(cr, uid, product_id_lists)

            mapping_ids = self.pool.get('magento.product').search(
                cr, uid, [('oe_product_id', 'in', product_id_lists)])

            mapping_objects = self.pool.get(
                'magento.product').browse(cr, uid, mapping_ids)

            odoo_magento_dict ={} # keys are odoo id and values are magento ID

            for items in mapping_objects:
                odoo_magento_dict[items.oe_product_id] = items.mag_product_id

            magento_updated_qty = {} # keys are magento id and values are qty to update

            stored_list =[]

            for prod_item in products_obj_line:
                qty=0

                if odoo_magento_dict.get(prod_item.id) is not None:
                    tmp={}
                    # qty = prod_item.qty_available - prod_item.pending_qty - prod_item.outgoing_qty
                    qty = (prod_item.qty_available - prod_item.pending_qty - prod_item.outgoing_qty) - prod_item.damage_quantity
                    qty =qty if qty >0 else 0

                    tmp['qty']=qty
                    tmp['threshold_qty']=prod_item.reorder_qty_level

                    stored_list.append({
                        'sku':prod_item.default_code,
                        'qty':qty,
                        'threshold_qty':prod_item.reorder_qty
                    })

                    magento_updated_qty[odoo_magento_dict.get(prod_item.id)] = tmp

            qty=0
            stock=True
            count=0

            success_story = []
            success_story_2 = []
            sale_order_ids = []
            token, url_root = get_magento_token(self, cr, uid, sale_order_ids)

            token = token.replace('"', "")
            headers = {'Authorization': token, 'Content-Type': 'application/json'}
            stock_list =[]

            for mage_product_id,tmp in magento_updated_qty.items():

                ## Calling server starts from
                sale_order_ids=[]

                if int(tmp.get('qty')) > 0:
                    stock_list.append({
                        "product_id": mage_product_id,
                        "stock_item": {

                                    "qty": tmp.get('qty'),
                                    "is_in_stock": stock,
                                    "extension_attributes": {
                                        "threshold_qty": tmp.get('threshold_qty')
                                    }
                                }
                    })
                else:
                    stock_list.append({
                        "product_id": mage_product_id,
                        "stock_item": {

                                    "qty": tmp.get('qty'),
                                    "extension_attributes": {
                                        "threshold_qty": tmp.get('threshold_qty')
                                    }
                                }
                    })

            product_stock_data = {"stockItems": stock_list}

            url = url_root + "/index.php/rest/V1/odoomagentoconnect/productStockBulk/"
            data = json.dumps(product_stock_data)

            try:
                resp = requests.post(url, data=data, headers=headers)

                sms_text = "Count {0} ({1}, {2}) : sucess with Button".format(counter, len(product_id_lists_all), len(product_id_lists))
                name = "stock.sync"
                # self.pool.get("send.sms.on.demand").send_sms_on_demand(cr, uid, product_id_lists, sms_text, "+8801817535299, +8801716520313", name, context=context)
            except:
                sms_text = "Count {0} ({1}, {2}) : fail with Button".format(counter, len(product_id_lists_all), len(product_id_lists))
                name = "stock.sync"
                # self.pool.get("send.sms.on.demand").send_sms_on_demand(cr, uid, product_id_lists, sms_text, "+8801817535299, +8801716520313", name, context=context)

                pass
            
            # SMS
            counter += 1
            
            # print("-------------------------")
            # print("-------------------------")
            # print("250 products syncing response: "+str(resp))
            # print("-------------------------")
            # print("-------------------------")

            log_data ={
                'entry_log_time':fields.datetime.now(),
                'stock_sync_log': str(stored_list),
                'response_from_magento': str(resp),
                'total_sku_from_odoo': str(len(stored_list))
            }

            try:
                stock_log = self.pool['stock.synch.log'].create(cr,uid,log_data)
            except:
                sms_text = "Log entry error with Button"
                name = "stock.sync"
                # self.pool.get("send.sms.on.demand").send_sms_on_demand(cr, uid, product_id_lists, sms_text, "+8801817535299, +8801716520313", name, context=context)

                pass
                # success_story.append(mage_product_id)
                # success_story_2.append(resp)


                # By getting rsponses data ends here

            # break
        
        sms_text = "Counting ({0}) : Ends with Button".format(len(product_id_lists_all))
        name = "stock.sync"
        # self.pool.get("send.sms.on.demand").send_sms_on_demand(cr, uid, product_id_lists_all, sms_text, "+8801817535299, +8801716520313", name, context=context)

        return True

    _columns = {

        'pending_qty': fields.function(_get_pending_qty, string='Pending quantity for approval/confirmation', type='float'),
        'synch_qty': fields.function(_get_synch_qty, string='Synchronization  QTY', type='float'),
        'stock_sync_req': fields.boolean('Stock Sync Required'),
    }



class stock_move(osv.osv):
    _inherit = 'stock.move'


    def stock_adjustment_product_wise(self, cr, uid, ids=[], context=None):

        stock_move_obj = self.pool.get('stock.move')
        stock_move_id_lists = stock_move_obj.search(cr, uid, [('state', '=', 'done'),('date','>','2019-09-06')])
        stock_move_obj_line = stock_move_obj.browse(cr, uid, stock_move_id_lists)


        ## Internal Location Id

        internal_location_ids = self.pool.get('stock.location').search(cr, uid, [('usage', '=', 'internal')])


        excess_move_list = []
        product_dict = {}
        new_product_id_list = []



        for items in stock_move_obj_line:
            tmp_dict ={}
            moved_qty = items.moved_qty

            if moved_qty > items.product_uom_qty  and items.already_overqty_calculated != True:
                extra_qty = items.product_uom_qty - moved_qty
                excess_move_list.append(items.id)

                common_quant_id_list =[]


                #### Quant_id check

                move_quant_ids = [q_id.id for q_id in items.quant_ids]


                #### current quant ids

                quant_ids = self.pool.get('stock.quant').search(cr, uid, [('product_id', '=', items.product_id.id),
                                                                          ('location_id.usage', '=', 'internal')])
                quant_ids_obj = self.pool.get('stock.quant').browse(cr, uid, quant_ids, context=context)



                common_quant_id_list = list(set(move_quant_ids) & set(quant_ids))






                if items.product_id.id not in new_product_id_list:
                    new_product_id_list.append(items.product_id.id)


                if product_dict.get(items.product_id.id) is not None:
                    tmp_dict = product_dict.get(items.product_id.id)

                    if tmp_dict.get(items.location_dest_id.id) is not None:
                        tmp_dict[items.location_dest_id.id] = tmp_dict.get(items.location_dest_id.id) + extra_qty
                    else:
                        tmp_dict[items.location_dest_id.id] = extra_qty
                    product_dict[items.product_id.id] = tmp_dict
                else:
                    if tmp_dict.get(items.location_dest_id.id) is not None:
                        tmp_dict[items.location_dest_id.id] = tmp_dict.get(items.location_dest_id.id) + extra_qty
                    else:
                        tmp_dict[items.location_dest_id.id] = extra_qty
                    product_dict[items.product_id.id] = tmp_dict


                # ## Add qty for STN
                #
                # if items.location_id.id in internal_location_ids and items.location_dest_id.id in internal_location_ids:
                #
                #     if product_dict.get(items.product_id.id) is not None:
                #         tmp_dict = product_dict.get(items.product_id.id)
                #
                #         if tmp_dict.get(items.location_id.id) is not None:
                #             tmp_dict[items.location_id.id] = tmp_dict.get(items.location_id.id) - extra_qty
                #         else:
                #             tmp_dict[items.location_id.id] = abs(extra_qty)
                #         product_dict[items.product_id.id] = tmp_dict
                #     else:
                #         if tmp_dict.get(items.location_id.id) is not None:
                #             tmp_dict[items.location_id.id] = tmp_dict.get(items.location_id.id) - extra_qty
                #         else:
                #             tmp_dict[items.location_id.id] = abs(extra_qty)
                #         product_dict[items.product_id.id] = tmp_dict



        res = {}
        for id in ids:
            res[id]=product_dict


        # Following code for before changin the qty from move line (Pre 001)
        before_change_p_id ={}

        for new_p_id in new_product_id_list:

            quant_ids = self.pool.get('stock.quant').search(cr, uid, [('product_id', '=', new_p_id),
                                                                      ('location_id.usage', '=', 'internal')])
            quant_ids_obj = self.pool.get('stock.quant').browse(cr, uid, quant_ids, context=context)

            t_warehouses = {} # wh location wise qty and befor modification
            total_qty = 0

            for quant in quant_ids_obj:

                if quant.location_id:
                    if quant.location_id.id not in t_warehouses:
                        t_warehouses.update({quant.location_id.id: 0})
                    t_warehouses[quant.location_id.id] += quant.qty
                    total_qty += quant.qty
            before_change_p_id[new_p_id]=t_warehouses

        # Ends Here (Pre 001)


        # Correct the excess move from stock move line (Don 001)


        for k,v in product_dict.iteritems():
            new_quant_ids = self.pool.get('stock.quant').search(cr, uid, [('product_id', '=', k),
                                                                      ('location_id.usage', '=', 'internal')])
            new_quant_ids_obj = self.pool.get('stock.quant').browse(cr, uid, new_quant_ids, context=context)
            reminder =0
            update_value=0


            for location , qty in v.iteritems():
                for new_items in new_quant_ids_obj:
                    if new_items.location_id.id == location:
                        if qty == 0:
                            break

                        if abs(qty) > new_items.qty:
                            reminder = abs(qty) - new_items.qty
                            update_value=0

                        elif new_items.qty > abs(qty) :
                            update_value = new_items.qty - abs(qty)
                            reminder=0
                        else:
                            reminder = new_items.qty - abs(qty)
                            update_value = reminder



                        update_query = "UPDATE stock_quant SET qty='{0}' WHERE id ={1}".format(update_value, new_items.id)
                        cr.execute(update_query)
                        cr.commit()

                        qty = reminder



        # Ends Here (Don 001)


        dic={
            'updated_product_dict':product_dict,
            'updated_move_list':excess_move_list,
            'total_updated_move_list':len(excess_move_list),
            'total_updated_product_list':len(product_dict),
        }



        for m_id in excess_move_list:

            update_excess_move_query = "UPDATE stock_move SET already_overqty_calculated = TRUE WHERE id = {0}".format(m_id)
            cr.execute(update_excess_move_query)
            cr.commit()




        try:
            qty_change_log_obj = self.pool.get('over.qty.calculation.log')
            registration_id = qty_change_log_obj.create(cr, uid, dic, context=context)
        except:
            pass

        # Re assigning the moves

        self.refresh_action_for_reassignment(cr,uid,ids,context)




        return res


    def refresh_action_for_reassignment(self, cr, uid, ids, context=None):



        # 90 days but not before 1st July
        date_threshold = 90

        now = date.today()
        first_july = date(2018, 7, 1)
        date_delta = now - first_july

        if int(date_delta.days) > date_threshold:

            last_few_days_order_query = "SELECT DISTINCT ON (origin) id, origin, state FROM stock_picking WHERE date > current_date - interval '"+str(date_threshold)+"' day AND state != 'done' AND state != 'cancel';"
        else:
            last_few_days_order_query = "SELECT DISTINCT ON (origin) id, origin, state FROM stock_picking WHERE date > '"+str(first_july)+"' AND state != 'done' AND state != 'cancel';"

        cr.execute(last_few_days_order_query)
        all_order_origin_data = cr.fetchall()

        if len(all_order_origin_data) > 0:

            all_so_id_list = list()
            for all_order_origin in all_order_origin_data:

                if str(all_order_origin[1]).startswith('SO'):
                    all_so_id_list.append(all_order_origin)

            context = {
                'lang': 'en_US',
                'tz': 'Asia/Dhaka',
                'uid': uid,
                'active_model': 'stock.picking.type',
                'search_default_picking_type_id': [2],
                'default_picking_type_id': 2,
                'search_default_waiting': 1,
                'params': {'action': 379},
                'search_disable_custom_filters': True,
                'contact_display': 'partner_address',
                'active_ids': [2],
                'active_id': 2
            }

            for so_id in all_so_id_list:
                stock_picking_id = int(so_id[0])



                if str(so_id[2]) == 'partially_available' or str(so_id[2]) == 'confirmed' or str(so_id[2]) == 'assigned':
                    #     # unreserve
                    self.pool.get('stock.picking').do_unreserve(cr, uid, int(stock_picking_id), context=context)

                    # action cancel
                    self.pool.get('stock.picking').action_assign(cr, uid, int(stock_picking_id), context=context)

                if str(so_id[2]) == 'waiting':
                    # Rereservation
                    self.pool.get('stock.picking').rereserve_pick(cr, uid, [stock_picking_id], context=context)
        return True




    def _get_moved_qty(self, cr, uid, ids, name, arg, context=None):
        res = {}
        for record in self.browse(cr, uid, ids, context=context):




            total_moved_qty=0
            for items in record.quant_ids:
                total_moved_qty += items.qty


            res[record.id] = total_moved_qty
        return res

    def _get_moved_qty_list(self, cr, uid, ids, name, arg, context=None):
        res = {}
        for record in self.browse(cr, uid, ids, context=context):




            total_moved_qty=[]
            for items in record.quant_ids:
                total_moved_qty.append(items.id)




            res[record.id] = total_moved_qty
        return res


    _columns = {
        'moved_qty': fields.function(_get_moved_qty, string='Moved QTY', type='float'),
        'quanit_id_list': fields.function(stock_adjustment_product_wise, string='Changes list QTY', type='char'),
        'already_overqty_calculated':fields.boolean('Overdue Calculated'),
    }



    class over_qty_calculation_log(osv.osv):

        _name = 'over.qty.calculation.log'

        _columns = {
            'updated_product_dict': fields.text('Updated Product Dict'),
            'updated_move_list': fields.text('Updated Move List'),
            'total_updated_move_list': fields.float('Total Updated Move List'),
            'total_updated_product_list': fields.float('Total Updated Product List'),
        }

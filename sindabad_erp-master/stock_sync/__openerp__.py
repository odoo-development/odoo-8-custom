{
    'name': 'Product Stock Sync',
    'version': '1.0',
    'category': 'Product Stock Sync',
    'author': 'Mufti Muntasir Ahmed',
    'summary': 'Product Stock Sync with Magento',
    'description': 'Product Stock Sync with Magento',
    'depends': ['product', 'base', 'odoo_magento_connect', 'sale', 'stock', 'wms_manifest', 'order_status_sync', 'odoo_to_magento_api_connect'],
    'data': [
        'security/stock_sync_security.xml',
        'security/ir.model.access.csv',

        'views/stock_sync_log_view.xml',

        'core_sync_view.xml',
        'product_view.xml'
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}

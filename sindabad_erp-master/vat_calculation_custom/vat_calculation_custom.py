import re, time, random
from openerp import api
from openerp.osv import fields, osv
from openerp.tools.translate import _
import logging
_logger = logging.getLogger(__name__)


class account_invoice_refund(osv.osv_memory):

    """Refunds invoice"""

    _inherit = "account.invoice.refund"

    def compute_refund(self, cr, uid, ids, mode='refund', context=None):

        res = super(account_invoice_refund, self).compute_refund(cr, uid, ids, mode='refund', context=context)

        if res:

            res_id = res['domain'][1][2]

            account_type = self.pool.get('account.invoice').browse(cr, uid, res_id, context=context)[0].type

            if account_type == 'out_refund':
                inv_id = res_id[0]
                cr.execute("DELETE FROM account_invoice_tax WHERE invoice_id=%s", ([res_id[0]]))
                cr.commit()

                tax_obj = self.pool.get('account.tax')
                tax_data = tax_obj.browse(cr, uid, [5], context=context)[0]

                cr.execute(
                    "select sum(five_vat) as total_vat from account_invoice_line where invoice_id=%s group by invoice_id",
                    ([res_id[0]]))
                total_vat_amount = cr.fetchall()
                if len(total_vat_amount) > 0:
                    total_vat_amount = total_vat_amount[0][0]

                    cr.execute("select amount_untaxed from account_invoice where id=%s", ([res_id[0]]))
                    base_amount = cr.fetchall()[0][0]

                    tax_line_data = {
                        'account_id': tax_data.account_collected_id.id,
                        'sequence': 1,
                        'company_id': 1,
                        'manual': False,
                        'base_amount': base_amount,
                        'amount': total_vat_amount,
                        'base': base_amount,
                        'tax_amount': total_vat_amount,
                        'name': tax_data.name,
                    }

                    account_invoice = self.pool['account.invoice']
                    account_invoice.write(cr, uid, inv_id, {'tax_line': [(0, 0, tax_line_data)]},
                                          context=context)

                    cr.commit()

        return res

class stock_picking(osv.osv):
    _inherit = "stock.picking"

    def action_invoice_create(self, cr, uid, ids, journal_id=False, group=False, type='out_invoice', context=None):
        '''Return ids of created invoices for the pickings'''
        res = super(stock_picking,self).action_invoice_create(cr, uid, ids, journal_id, group, type, context=context)

        if res:

            res_id = res

            account_type = self.pool.get('account.invoice').browse(cr, uid, res_id, context=context)[0].type

            if account_type == 'out_refund' or account_type == 'in_refund':
                inv_id = res_id[0]
                cr.execute("DELETE FROM account_invoice_tax WHERE invoice_id=%s", ([res_id[0]]))
                cr.commit()

                tax_obj = self.pool.get('account.tax')
                tax_data = tax_obj.browse(cr, uid, [5], context=context)[0]

                cr.execute(
                    "select sum(five_vat) as total_vat from account_invoice_line where invoice_id=%s group by invoice_id",
                    ([res_id[0]]))
                total_vat_amount = cr.fetchall()
                if len(total_vat_amount) > 0:
                    total_vat_amount = total_vat_amount[0][0]

                    cr.execute("select amount_untaxed from account_invoice where id=%s", ([res_id[0]]))
                    base_amount = cr.fetchall()[0][0]

                    tax_line_data = {
                        'account_id': tax_data.account_collected_id.id,
                        'sequence': 1,
                        'company_id': 1,
                        'manual': False,
                        'base_amount': base_amount,
                        'amount': total_vat_amount,
                        'base': base_amount,
                        'tax_amount': total_vat_amount,
                        'name': tax_data.name,
                    }

                    account_invoice = self.pool['account.invoice']
                    account_invoice.write(cr, uid, inv_id, {'tax_line': [(0, 0, tax_line_data)]},
                                          context=context)

                    cr.commit()

        return res

class stock_invoice_onshipping(osv.osv_memory):
    _inherit = "stock.invoice.onshipping"

    def create_invoice(self, cr, uid, ids, context=None):
        uid=1

        res = super(stock_invoice_onshipping, self).create_invoice(cr, uid, ids, context=context)

        if res:

            account_type = self.pool.get('account.invoice').browse(cr, uid, res, context=context)[0].type

            if account_type == 'out_invoice' or account_type == 'out_refund':
                inv_id = res[0]
                cr.execute("DELETE FROM account_invoice_tax WHERE invoice_id=%s", ([res[0]]))
                cr.commit()

                tax_obj = self.pool.get('account.tax')
                tax_data = tax_obj.browse(cr, uid, [5], context=context)[0]

                cr.execute(
                    "select sum(five_vat) as total_vat from account_invoice_line where invoice_id=%s group by invoice_id",
                    ([res[0]]))
                total_vat_amount = cr.fetchall()
                if len(total_vat_amount) > 0:
                    total_vat_amount = total_vat_amount[0][0]

                    cr.execute("select amount_untaxed from account_invoice where id=%s", ([res[0]]))
                    base_amount = cr.fetchall()[0][0]

                    tax_line_data = {
                        'account_id': tax_data.account_collected_id.id,
                        'sequence': 1,
                        'company_id': 1,
                        'manual': False,
                        'base_amount': base_amount,
                        'amount': total_vat_amount,
                        'base': base_amount,
                        'tax_amount': total_vat_amount,
                        'name': tax_data.name,
                    }

                    account_invoice = self.pool['account.invoice']
                    account_invoice.write(cr, uid, inv_id, {'tax_line': [(0, 0, tax_line_data)]},
                                          context=context)

                    cr.commit()

        return res


class account_invoice_line(osv.osv):
    _inherit = 'account.invoice.line'

    def create(self, cr, uid, vals, context=None):
        uid=1

        vals['invoice_line_tax_id'][0][2].append(5)
        rec = super(account_invoice_line, self).create(cr, uid, vals, context=context)

        inv_line = self.browse(cr, uid, rec, context=context)
        origin = inv_line.invoice_id.origin
        total_unit_cost=0
        unit_cost=0
        des_name = vals.get('name') if vals.get('name') else ''
        result = des_name.startswith('FREE -')

        if origin is not False and ('OUT' in origin or 'IN' in origin) and result is False:
            purchase_query = "select sum((sq.qty*sm.price_unit)) as total from stock_quant_move_rel as sqm,stock_move as sm, stock_quant as sq where sqm.quant_id in ((select sq2.id from stock_quant_move_rel as sqmr2, stock_quant as sq2 where sqmr2.move_id in (select stock_move.id from stock_picking, stock_move where stock_picking.id = stock_move.picking_id and stock_picking.name=%s and stock_move.product_id=%s and stock_move.name NOT LIKE %s limit 1) and sqmr2.quant_id = sq2.id)) and sqm.move_id = sm.id and sm.purchase_line_id >0 and sq.id=sqm.quant_id group by sq.product_id"

            cr.execute(purchase_query, (origin, inv_line.product_id.id,'FREE %'))
            objects = cr.dictfetchall()
            for items in objects:
                total_unit_cost =items.get('total')
            if total_unit_cost == 0:
                unit_cost = inv_line.product_id.standard_price

        if total_unit_cost >0 and inv_line.quantity > 0:
            unit_cost = total_unit_cost/inv_line.quantity

        if unit_cost == 0:
            unit_cost = inv_line.product_id.standard_price

        # unit_cost = inv_line.product_id.standard_price
        total_quantity = inv_line.quantity
        unit_so_price = inv_line.price_unit

        total_profit_loss = (unit_so_price * total_quantity) - (unit_cost * total_quantity)

        # cr.execute("update account_invoice_line set unit_cost=%s where id=%s", ([unit_cost, inv_line.id]))
        # cr.commit()

        # import pdb;pdb.set_trace()

        if total_profit_loss > 0:

            tax_obj = self.pool.get('account.tax')
            tax_data = tax_obj.browse(cr, uid, [5], context=context)[0]

            inv_line.unit_cost = unit_cost
            inv_line.unit_cost_profit_loss = unit_so_price - unit_cost
            per_unit_vat = (unit_so_price - unit_cost) * 0.05
            # inv_line.price_unit = inv_line.price_unit - per_unit_vat
            inv_line.vat_on_unit_profit_loss = (unit_so_price - unit_cost) * 0.05
            inv_line.total_profit_loss = total_profit_loss
            inv_line.five_vat = total_profit_loss * 0.05
            inv_line.unit_price_with_vat = inv_line.price_unit + per_unit_vat

            tax_line_obj = self.pool.get('account.invoice.tax')
            tax_id = tax_line_obj.search(cr, uid, [('invoice_id', '=', inv_line.invoice_id.id)], context=context)
            if tax_id:
                # add the previous amounts
                tax_info = tax_line_obj.browse(cr, uid, tax_id, context=context)

                tax_line_data = {
                    # 'account_id': tax_data.account_collected_id.id,
                    # 'sequence': 1,
                    # 'company_id': 1,
                    # 'invoice_id': inv_line.invoice_id.id,
                    # 'manual': False,
                    'base_amount': inv_line.price_subtotal + tax_info.base_amount,
                    'amount': (total_profit_loss + tax_info.amount) * 0.05,
                    'base': inv_line.price_subtotal + tax_info.base_amount,
                    'tax_amount': (total_profit_loss + tax_info.amount) * 0.05,
                }

                tax_line = tax_line_obj.write(cr, uid, tax_id, tax_line_data, context=context)

            else:
                tax_line_data = {
                    'account_id': tax_data.account_collected_id.id,
                    'sequence': 1,
                    'company_id': 1,
                    'invoice_id': inv_line.invoice_id.id,
                    'manual': False,
                    'base_amount': inv_line.price_subtotal,
                    'amount': total_profit_loss * 0.05,
                    'base': inv_line.price_subtotal+inv_line.five_vat,
                    'tax_amount': total_profit_loss * 0.05,
                    'name': tax_data.name,
                }

                tax_line_id = tax_line_obj.create(cr, uid, tax_line_data, context=context)
                inv_line.tax_line = tax_line_id

        else:
            inv_line.vat_on_unit_profit_loss = 0
            inv_line.total_profit_loss = 0
            inv_line.five_vat = 0


        return inv_line.id

    def _get_unit_cost(self, cr, uid, ids, name="", args={}, context=None):
        res = {}

        for inv_line in self.browse(cr, uid, ids, context):
            unit_cost = inv_line.product_id.standard_price
            res[inv_line.id] = str(unit_cost)

        return res

    def _get_unit_cost_profit_loss(self, cr, uid, ids, name="", args={}, context=None):
        res = {}

        for inv_line in self.browse(cr, uid, ids, context):
            unit_cost = inv_line.product_id.standard_price
            unit_so_price = inv_line.price_unit
            res[inv_line.id] = str(unit_so_price - unit_cost)

        return res

    def vat_on_unit_profit_loss(self, cr, uid, ids, name="", args={}, context=None):
        res = {}

        for inv_line in self.browse(cr, uid, ids, context):
            unit_cost = inv_line.product_id.standard_price
            unit_so_price = inv_line.price_unit
            p_l = unit_so_price - unit_cost

            if p_l > 0:
                res[inv_line.id] = str(p_l * 0.05)
            else:
                res[inv_line.id] = "0"

        return res

    def _get_total_profit_loss(self, cr, uid, ids, name="", args={}, context=None):
        res = {}

        for inv_line in self.browse(cr, uid, ids, context):
            unit_cost = inv_line.product_id.standard_price
            total_quantity = inv_line.quantity
            unit_so_price = inv_line.price_unit

            res[inv_line.id] = str((unit_so_price * total_quantity) - (unit_cost * total_quantity))

        return res

    def _get_total_vat(self, cr, uid, ids, name="", args={}, context=None):
        res = {}

        for inv_line in self.browse(cr, uid, ids, context):
            unit_cost = inv_line.product_id.standard_price
            total_quantity = inv_line.quantity
            unit_so_price = inv_line.price_unit
            total_profit = (unit_so_price * total_quantity) - (unit_cost * total_quantity)

            if total_profit > 0:
                res[inv_line.id] = str(total_profit * 0.05)
            else:
                res[inv_line.id] = "0"

        return res


    def _total_price_with_vat(self, cr, uid, ids, name, arg, context=None):
        res = {}
        for inv_line in self.browse(cr, uid, ids, context):
            res[inv_line.id]= inv_line.price_subtotal + inv_line.five_vat


        return res


    _columns = {
        'unit_cost': fields.float(string='Unit Cost'),
        'unit_cost_profit_loss': fields.float(string='Cost P/L'),
        'vat_on_unit_profit_loss': fields.float(string='VAT Per Unit'),
        # 'total_unit_cost': fields.float('Total Unit Cost'),
        # 'total_profit_loss': fields.function(_get_total_profit_loss, string='Total P/L', type='text', store=True),
        'total_profit_loss': fields.float(string='Total P/L'),
        'five_vat': fields.float(string='5% Vat'),
        'unit_price_with_vat': fields.float(string='Unit Price with VAT'),
        'total_price_with_vat': fields.function(_total_price_with_vat, type='float', string='Total Price With VAT'),

    }

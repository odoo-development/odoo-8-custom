# Author Mufti Muntasir Ahmed 16-01-2021


{
    'name': 'Loading Master Generation Process',
    'version': '8.0.0',
    'category': 'Warehouse (WMS)',
    'description': """

""",
    'author': 'Mufti Muntasir Ahmed',
    'depends': ['account'],
    'data': [
        'security/loading_master_security.xml',
        'security/ir.model.access.csv',

        'loading_view.xml',
        'views/report_loading.xml',
        'report_menu.xml',
        'loading_menu.xml',

    ],

    'installable': True,
    'auto_install': False,
}
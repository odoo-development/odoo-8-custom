# Author Mufti Muntasir Ahmed 16-01-2021


from openerp.osv import osv, fields
from openerp import SUPERUSER_ID, api
from openerp.tools.translate import _
from datetime import datetime



class LoadingList(osv.osv):
    _name = 'loading.master'
    _description = "Loading List"

    _columns = {
        'name': fields.char('Order Reference', required=False, copy=False,
                            readonly=True, states={'draft': [('readonly', False)], 'sent': [('readonly', False)]},
                            select=True),
        'loading_date': fields.datetime('Date', required=False, readonly=True, select=True, copy=False),
        'date_confirm': fields.datetime('Confirmation Date', readonly=True, select=True,
                                        help="Date on which Loading is confirmed.", copy=False),
        'vehicle_number': fields.char('Vehicle Number'),
        'assign_to': fields.char('Assign To'),
        'description': fields.char('Remarks'),
        'total_amount': fields.float('Total loading Amount'),
        'invoice_scan': fields.char('Invoice Scan'),
        'manifest_id': fields.many2one('wms.manifest.process', 'Manifest ID', readonly=True),

        'loading_line': fields.one2many('loading.master.line', 'loading_id', 'Loading List Lines', required=False),
        'loading_products_line': fields.one2many('loading.products.line', 'loading_id', 'Loading Products Line', required=False),

        'state': fields.selection([
            ('pending', 'Waiting for Loading'),
            ('done', 'Loaded and Manifested'),
            ('cancel', 'Cancelled'),

        ], 'Status', readonly=True, copy=False, help="Gives the status of the Loading Master", select=True),

    }

    _defaults = {
        'loading_date': fields.datetime.now,
        'state': 'pending',
        'name': lambda obj, cr, uid, context: '/',

    }

    _order = 'id desc'

    def make_confirm(self, cr, uid, ids, context=None):

        id_list = ids
        stock_loading_ids = []
        load_obj = self.browse(cr, uid, ids, context=context)[0]

        if str(load_obj.state) == str('done'):
            raise osv.except_osv(_('Already Confirmed !!!'),
                                 _('This sheet has been confirmed and manifest has been generated.'))

        if str(load_obj.state) == str('cancel'):
            raise osv.except_osv(_('Already Cancelled !!!'),
                                 _('This sheet can not  confirm.'))

        ## Generate Manifiest Process
        manifest_id=''

        inv_lines = []

        for inv_items in load_obj.loading_line:
            tmp = {'status': inv_items.status, 'area': inv_items.area, 'invoice_id': inv_items.invoice_id.id, 'phone': inv_items.phone, 'amount': inv_items.amount,
             'payment_type': inv_items.payment_type, 'address': inv_items.address,
             'magento_no': inv_items.magento_no, 'customer_name': inv_items.customer_name}

            inv_lines.append([0,False,tmp])

        manifest_vals = {
            'remark': load_obj.description,
            'vehicle_from': 0,
            'vehicle_number': load_obj.vehicle_number,
            'scan': False,
            'courier_name': load_obj.name,
            'vehicle_to': 0,
            'assigned_to': load_obj.assign_to,
            'picking_line': inv_lines,


                }


        manifest_id =self.pool["wms.manifest.process"].create(cr, uid, manifest_vals)



        ## Ends here Manifest Process

        for element in id_list:
            ids = [element]

            if ids is not None:
                for id in ids:
                    cr.execute("update loading_master set state='done',manifest_id=%s where id=%s", ([manifest_id,id]))
                    cr.commit()

        return 'True'

    def make_cancel(self, cr, uid, ids, context=None):
        load_obj = self.browse(cr, uid, ids, context=context)[0]

        if str(load_obj.state) == str('done'):
            raise osv.except_osv(_('Already Confirmed !!!'),
                                 _('This sheet has been confirmed and manifest has been generated.'))

        if ids is not None:
            for id in ids:
                loading_line = self.pool.get('loading.master.line').search(cr, uid, [('loading_id', '=', id)])
                invoices = self.pool.get('loading.master.line').browse(cr, uid,loading_line,context=context)
                inv_ids = [inv_id.invoice_id.id for inv_id in invoices]

                cr.execute("update loading_master set state='cancel' where id=%s", ([id]))
                cr.commit()

                cr.execute("UPDATE account_invoice SET x_loading_assign=FALSE WHERE id in %s",
                                 (tuple(inv_ids),))
                cr.commit()

        return 'True'

    def _check_packing_line_for_same_invoice(self, packing_line, invoice_id):
        same_invoice = False

        for line in packing_line:
            try:

                if line['invoice_id'].id == invoice_id:
                    same_invoice = True
            except:
                pass

        return same_invoice

    def check_invoice_delivered(self, invoice_id):
        delivered = False

        check_delivered_query = "SELECT delivered FROM account_invoice WHERE id={0}".format(invoice_id)
        self._cr.execute(check_delivered_query)

        for a in self._cr.fetchall():
            inv_delivered = a[0]
            if inv_delivered == True:
                delivered = True
            else:
                delivered = False

        return delivered

    def check_invoice_return(self, invoice_id):
        returned = False

        check_return_query = "SELECT type FROM account_invoice WHERE id={0}".format(invoice_id)
        self._cr.execute(check_return_query)

        for a in self._cr.fetchall():
            inv_returned = a[0]
            if inv_returned == 'out_refund':
                returned = True
            else:
                returned = False

        return returned

    def check_invoice_assigned(self, invoice_id):
        assigned = False

        check_assigned_query = "SELECT assigned FROM account_invoice WHERE id={0}".format(invoice_id)
        self._cr.execute(check_assigned_query)

        for a in self._cr.fetchall():
            inv_assigned = a[0]
            if inv_assigned == True:
                assigned = True
            else:
                assigned = False

        return assigned
    def check_load_assign(self, invoice_id):
        x_loading_assign = False
        check_loading_assigened_query = "SELECT x_loading_assign FROM account_invoice WHERE id ={0}".format(invoice_id)
        self._cr.execute(check_loading_assigened_query)
        for a in self._cr.fetchall():
            inv_x_loading_assign= a[0]
            if inv_x_loading_assign == True:
                x_loading_assign = True
            else:
                x_loading_assign= False
        return x_loading_assign

    @api.onchange('invoice_scan')
    def loading_invoice_scan(self):
        outbound_line_list = list()
        invoice_id = str(self.invoice_scan)

        try:

            # outbound_line_list=self.loading_products_line

            if invoice_id != 'False':

                if self.check_invoice_delivered(invoice_id):
                    self.invoice_scan = ""
                    raise osv.except_osv(_('Warning!'), _('This order is already delivered.'))
                elif self.check_invoice_return(invoice_id):
                    self.invoice_scan = ""
                    raise osv.except_osv(_('Warning!'), _('This order is already returned.'))
                elif self.check_invoice_assigned(invoice_id):
                    # raise message. invoice already assigned
                    self.invoice_scan = ""
                    raise osv.except_osv(_('Warning!'), _('This order is already assigned for manifest'))
                if self.check_load_assign(invoice_id):
                    self.invoice_scan=""
                    raise osv.except_osv(_('Warning'), _('This order already assigened another sheet'))

                invoice_id = int(invoice_id)
                invoice_env = self.env['account.invoice']
                invoice_obj = invoice_env.search([('id', '=', invoice_id)])

                new_picking_line=[]


                if invoice_obj:
                    # invoice_obj = invoice_env.search([('id', '=', invoice_id)])
                    invoice_line_env = self.env['account.invoice.line']
                    invoice_line_obj = invoice_line_env.search([('invoice_id', '=', invoice_id)])

                    if self._check_packing_line_for_same_invoice(self.loading_line, invoice_id):
                        self.invoice_scan = ""
                    else:

                        if self.loading_products_line:

                            for items in self.loading_products_line:
                                if not items.id:
                                    outbound_line_list.append({
                                        'invoice_id': items['invoice_id'],
                                        'product_id': items['product_id'],
                                        'loading_qty': items['loading_qty'],

                                    })




                        # for invoice_line in invoice_line_obj:
                        #     prod_name = str(invoice_line.product_id.name)
                        #     if prod_name != 'Shipping' and prod_name != 'shipping':
                        #         outbound_line_list.append({
                        #             'invoice_id': invoice_id,
                        #             'product_id': invoice_line.product_id.id,
                        #             'loading_qty': invoice_line.quantity,
                        #
                        #         })
                        #
                        # self.loading_products_line = outbound_line_list

                        so_env = self.env['sale.order']
                        deli_area = ""
                        deli_address = ""
                        deli_phone = ""
                        if invoice_obj:
                            so = so_env.search([('client_order_ref', '=', str(invoice_obj.name))])
                            deli_area = so.partner_shipping_id.state_id.name
                            deli_address = so.partner_shipping_id.street
                            deli_phone = so.partner_shipping_id.mobile

                        if self.loading_line:
                            for items in self.loading_line:
                                if not items.id:
                                    new_picking_line.append({
                                        'invoice_id': items['invoice_id'],
                                        'magento_no': items['magento_no'],
                                        'customer_name': items['customer_name'],
                                        'payment_type': items['payment_type'].replace("Payment Information:-", ""),
                                        'amount': items['amount'],
                                        'status': items['status'],
                                        'area': items['area'],
                                        'address': items['address'],
                                    })


                        new_picking_line.append({
                            'invoice_id': invoice_id,
                            'magento_no': str(invoice_obj.name),
                            'priority_customer': invoice_obj.x_priority_customer,
                            'customer_name': str(invoice_obj.partner_id.name),
                            # 'phone': str(invoice_obj.partner_id.phone),
                            'phone': str(deli_phone),
                            # 'address': str(invoice_obj.commercial_partner_id.street),
                            'address': str(deli_address),
                            'payment_type': str(invoice_obj.comment).replace("Payment Information:-", ""),
                            'amount': float(invoice_obj.amount_total),
                            'status': str(invoice_obj.state),
                            # 'area': str(invoice_obj.commercial_partner_id.city),
                            'area': str(deli_area)
                        })
                        self.loading_line = new_picking_line



                        self.invoice_scan = ""
                else:
                    self.invoice_scan = ""

        except:
            self.invoice_scan = ""
            if self.check_invoice_delivered(invoice_id):
                self.invoice_scan = ""
                raise osv.except_osv(_('Warning!'), _('This order is already delivered.'))
            elif self.check_invoice_return(invoice_id):
                self.invoice_scan = ""
                raise osv.except_osv(_('Warning!'), _('This order is already returned.'))
            elif self.check_invoice_assigned(invoice_id):
                # raise message. invoice already assigned
                self.invoice_scan = ""
                raise osv.except_osv(_('Warning!'), _('This order is already assigned for manifest'))
            if self.check_load_assign(invoice_id):
                self.invoice_scan=""
                raise osv.except_osv(_('Warning!'),_('This order already assigened another sheet'))

        return "xXxXxXxXxX"

    @api.model
    def create(self, vals):

        outbound_line_list=[]

        for inv in vals['loading_line']:
            invoice_id = inv[2].get('invoice_id')
            invoice_line_env = self.env['account.invoice.line']
            invoice_line_obj = invoice_line_env.search([('invoice_id', '=', invoice_id)])
            for invoice_line in invoice_line_obj:
                prod_name = str(invoice_line.product_id.name)
                if prod_name != 'Shipping' and prod_name != 'shipping':
                    outbound_line_list.append([0,False,{
                        'invoice_id': invoice_id,
                        'product_id': invoice_line.product_id.id,
                        'loading_qty': invoice_line.quantity,

                    }])

            loading_assigened_query = "UPDATE account_invoice SET x_loading_assign=TRUE WHERE id='{0}'".format(
                int(invoice_id))
            self._cr.execute(loading_assigened_query)
            self._cr.commit()
        vals['loading_products_line'] = outbound_line_list

        record = super(LoadingList, self).create(vals)
        loading_number = 'LoadSheet-0' + str(record.id)
        record.name = loading_number
        
        return record

    @api.multi
    def write(self, vals):

        if str(self.state) == str('done'):
            raise osv.except_osv(_('Already Confirmed !!!'),
                                 _('This sheet has been confirmed and manifest has been generated.'))

        get_deleted_line_ids = []
        get_deleted_invs_ids = []
        outbound_line_list=[]

        vals['loading_products_line'] = outbound_line_list

        if vals.get('loading_line'):

            for items in vals.get('loading_line'):
                if items[0] == 0:

                    invoice_id = items[2].get('invoice_id')
                    invoice_line_env = self.env['account.invoice.line']
                    invoice_line_obj = invoice_line_env.search([('invoice_id', '=', invoice_id)])
                    for invoice_line in invoice_line_obj:
                        prod_name = str(invoice_line.product_id.name)
                        if prod_name != 'Shipping' and prod_name != 'shipping':
                            outbound_line_list.append([0, False, {
                                'invoice_id': invoice_id,
                                'product_id': invoice_line.product_id.id,
                                'loading_qty': invoice_line.quantity,

                            }])

                    loading_assigened_query = "UPDATE account_invoice SET x_loading_assign=TRUE WHERE id='{0}'".format(
                        int(invoice_id))
                    self._cr.execute(loading_assigened_query)
                    self._cr.commit()

                if items[0]==2:
                    get_deleted_line_ids.append(items[1])
                    for sav_items in self.loading_line:
                        if sav_items.id == items[1]:
                            get_deleted_invs_ids.append(sav_items.invoice_id.id)
                            break



        record = super(LoadingList, self).write(vals)

        if len(get_deleted_invs_ids)>0:
            self._cr.execute("DELETE FROM loading_products_line WHERE invoice_id in %s", (tuple(get_deleted_invs_ids),))
            self._cr.commit()

            self._cr.execute("UPDATE account_invoice SET x_loading_assign=FALSE WHERE id in %s", (tuple(get_deleted_invs_ids),))
            self._cr.commit()

        return record




class LoadingListLine(osv.osv):
    _name = 'loading.master.line'
    _description = "Loading Line List"

    _columns = {
        'loading_id': fields.many2one('loading.master', 'Loading Reference', required=False, ondelete='cascade', select=True,
                                      readonly=True),
        'invoice_id': fields.many2one('account.invoice', string='Invoice Number'),
        'invoice_number': fields.char('Invoice Number'),
        'magento_no': fields.char('Order No'),
        'customer_name': fields.char('Customer Name'),
        'phone': fields.char('Phone'),
        'address': fields.char('Address'),
        'payment_type': fields.char('Payment Type'),
        'amount': fields.float('Amount'),
        'status': fields.char('Status'),
        'area': fields.char('Area'),
        'priority_customer': fields.boolean('Priority Customer'),


    }




class loading_products_line(osv.osv):
    _name = 'loading.products.line'
    _description = "Loading Products Line"

    _columns = {
        'load_prod_id': fields.many2one('loading.products', 'Loading products', required=False, ondelete='cascade', select=True, readonly=True),
        'loading_id': fields.many2one('loading.master', 'Loading ID', required=False,
                                 ondelete='cascade', select=True, readonly=True),
        'invoice_id': fields.many2one('account.invoice', 'Invoice Number', readonly=True),
        'product_id': fields.many2one('product.product', 'Product', readonly=True),
        'product': fields.char('Product Name', readonly=True),
        'product_ean': fields.char('Product EAN', readonly=True),
        'loading_qty': fields.float('Qty'),
        'state': fields.selection([
            ('pending', 'Waiting for Loading'),
            ('done', 'Done'),
            ('cancel', 'Cancelled'),

        ], 'Status', readonly=True, copy=False, help="Gives the status of the quotation or sales order", select=True),

    }

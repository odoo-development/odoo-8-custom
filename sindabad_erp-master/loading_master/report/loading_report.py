# Author Mufti Muntasir Ahmed 18-01-2021
import time
from openerp.report import report_sxw
from openerp.osv import osv


class LoadingListReport(report_sxw.rml_parse):

    def __init__(self, cr, uid, name, context):
        super(LoadingListReport, self).__init__(cr, uid, name, context)
        self.localcontext.update({
            'get_magento_order_numbers': self.get_magento_order_numbers,
            'get_product_list': self.get_product_list,

        })


    def get_magento_order_numbers(self, obj):



        magento_numbers = ''

        for items in obj.loading_line:
            if items.magento_no:
                magento_numbers = magento_numbers + str(items.magento_no) + ', '

        

        return magento_numbers

    def get_product_list(self, obj):


        total_moveline_list = [items for items in obj.loading_products_line]
        product_id_lists = []
        moves_list = []
        for items in total_moveline_list:
            for att in items:
                product_id_lists.append(att.product_id.id)
                moves_list.append(att)


        product_id_lists = list(set(product_id_lists))

        p_list=[]


        for product_id in product_id_lists:
            qty_count=0
            product_info_dict ={}
            for mov in moves_list:
                if mov.product_id.id == product_id:
                    qty_count += mov.loading_qty

            for mov in moves_list:
                if mov.product_id.id == product_id:

                    product_info_dict['product_name'] = str(mov.product_id.default_code) + ' ' +str(mov.product_id.name)
                    product_info_dict['default_code'] = mov.product_id.default_code
                    product_info_dict['mov'] = mov
                    product_info_dict['qty'] = qty_count
                    break

            p_list.append(product_info_dict)

        return p_list



class report_loadinglayout(osv.AbstractModel):
    _name = 'report.loading_master.report_loadinglayout'
    _inherit = 'report.abstract_report'
    _template = 'loading_master.report_loadinglayout'
    _wrapped_report_class = LoadingListReport

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:

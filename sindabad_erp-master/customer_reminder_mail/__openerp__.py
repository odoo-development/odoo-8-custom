# Author S. M. Sazedul Haque 2019-01-01

{
    'name': 'Customer Reminder Email Send',
    'version': '1.0.0',
    'category': 'sale',
    'description': """
Allows you to Send Customer Statement    
==============================================================



To,

(Company Name and address)


Subject: Gentle reminder for Outstanding Amount until (Date);${object.name}   ;


Dear Sir/ Madam,

Greetings from Sindabad.com Ltd.

We are tremendously proud and grateful for your regular association with us. You will be glad to know that we are restlessly working to improve our services. As a part of this continuous improvement, please consider this as a gentle reminder for your outstanding amount of BDT ${object.previous_month_balance}. Please note that the data we considered is until (Date) and details of outstanding amount attached.

Please pay your bills as soon as possible. If you have any query, please communicate with your designated RM.


If you have already paid the mentioned outstanding amount within last one week, please ignore this gentle reminder. If you made any EFTN payment please let us know with payment date and reference number.


Thanks with regards,  
Sindabad Support Team
www.sindabad.com
Phone- 0961-2002244 (Saturday- Thursday. 10am- 8pm)
Email- support@sindabad.com






""",
    'author': 'Mufti Muntasir Ahmed',
    'depends': ['account','base', 'report'],
    'data': [
        'customer_reminder_mail_menu.xml',
        'customer_reminder_mail_template.xml',
        'report/reminder_layout.xml',
        'report/report_customerremindermail.xml',
        # 'wizard/customer_reminder_report_email_send_view.xml',
    ],

    'installable': True,
    'auto_install': False,
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:

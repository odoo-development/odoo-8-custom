{
    'name': 'Product COGS Correction',
    'version': '8.0.0',
    'category': 'Sales',
    'description': "Product COGS Correction",
    'author': 'Mufti Muntasir Ahmed',
    'depends': ['stock', 'stock_account'],
    'data': [

    ],
    'installable': True,
    'auto_install': False,
}

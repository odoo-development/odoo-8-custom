import time
from openerp.report import report_sxw
from openerp.osv import osv


class packingsWarrantyData(report_sxw.rml_parse):
    def __init__(self, cr, uid, name, context):
        super(packingsWarrantyData, self).__init__(cr, uid, name, context)
        self.localcontext.update({
            'get_client_shipping_address': '',
        })




class report_packings_warranty_layout(osv.AbstractModel):
    _name = 'report.warranty_service.report_packings_warranty_layout'
    _inherit = 'report.abstract_report'
    _template = 'warranty_service.report_packings_warranty_layout'
    _wrapped_report_class = packingsWarrantyData
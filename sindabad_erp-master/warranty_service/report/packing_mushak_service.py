import time
from openerp.report import report_sxw
from openerp.osv import osv
import datetime



class packingsmushakData(report_sxw.rml_parse):
    def __init__(self, cr, uid, name, context):
        super(packingsmushakData, self).__init__(cr, uid, name, context)
        self.localcontext.update({
            'get_invoices_data': self.get_invoices_data,
            'total_vat_sum': self.total_vat_sum,
            'get_basic_info': self.get_basic_info,
        })


    def get_invoices_data(self, obj):
        final_list=[]


        try:
            order_numbers = [obj.split(',')[0]]
        except:
            order_numbers = [str(obj)]

        group_id = None
        data_list = []

        self.cr.execute("SELECT id FROM account_invoice WHERE origin=%s", (order_numbers))
        for item in self.cr.fetchall():
            group_id = [item[0]]

        if group_id is not None:

            order_data = self.pool.get('account.invoice').browse(self.cr, self.uid, group_id)
            if str(order_data.type) == 'out_invoice' and str(order_data.state) != 'cancel':

                account_invoice_ids = order_data.invoice_line
                list_limit_counter = 0
                sl=0
                for items in account_invoice_ids:

                    if items.product_id.default_code:
                        list_limit_counter += 1

                        # vat_amount = ((items.price_unit - round(items.product_id.average_purchaes_price, 2))*items.quantity) * 0.05
                        # vat_amount = ((items.price_unit - items.product_id.standard_price)*items.quantity) * 0.05
                        # p_vat = vat_amount if vat_amount >= 0 else 0.0

                        # p_total_amount = items.price_subtotal - items.five_vat
                        try:
                            # sale_price_per_unit = items.price_unit + (items.five_vat / items.quantity)
                            sale_price_per_unit = items.price_unit
                        except ZeroDivisionError:

                            sale_price_per_unit= items.price_unit


                        total_amount = items.total_price_with_vat
                        service_vat = float((items.five_vat * 100)/5)
                        p_t_amount = (total_amount - service_vat)-items.five_vat
                        # p_t_amount = (p_total_amount + vat_amount) if p_vat == 0 else p_total_amount

                        estimated_cost_price = round(items.product_id.average_purchaes_price, 2)


                        sl += 1
                        data_list.append({'name': 'cool',
                                          'sl':sl,
                                          'product_name': items.product_id.name,
                                          'quantity': round(items.quantity,2),
                                          'unit_cost': round(items.price_unit,2),
                                          'sale_price_per_unit': round(sale_price_per_unit,2),
                                          'product_total_cost': round((items.price_unit*items.quantity),2),
                                          'cost_price': round(items.product_id.standard_price,2),
                                          'es_cost_price': estimated_cost_price,

                                          'vat_on_unit_profit_loss': items.vat_on_unit_profit_loss,
                                          'five_vat': round(items.five_vat,2),

                                          # 'date_invoice': it.date_invoice,
                                          'total_unit': round(items.price_unit*items.quantity,2),
                                          # 'total_cost': items.product_id.standard_price*items.quantity,
                                          'total_cost': estimated_cost_price*items.quantity,
                                          'total_amount': total_amount,
                                          'p_t_amount': p_t_amount,
                                          'service_vat' : service_vat,
                                          'subtotal': items.price_subtotal,
                                          # 'vat': round(p_vat,2),
                                          # 'total_vat': sum(round(p_vat,2)),
                                          # 'uom':items.product_id.uom_id.name,
                                          'uom': items.product_id.prod_uom,
                                          'discount': items.discount,
                                          # 'status_data': str_state

                                          })

                        # if list_limit_counter%9 ==0:
                        #     final_list.append(data_list)
                        #     data_list=[]


        final_list = data_list



        return final_list

    def total_vat_sum(self, obj):

        try:
            order_numbers = [obj.split(',')[0]]
        except:
            order_numbers = [str(obj)]

        total_vat = 0.00
        total_subtotal = 0.00
        p_total_amount = 0.00
        total_service_vat = 0.00
        group_id = None


        self.cr.execute("SELECT id FROM account_invoice WHERE origin=%s", (order_numbers))
        for item in self.cr.fetchall():
            group_id = [item[0]]

        if group_id is not None:

            order_data = self.pool.get('account.invoice').browse(self.cr, self.uid, group_id)
            if str(order_data.type) == 'out_invoice' and str(order_data.state) != 'cancel':

                account_invoice_ids = order_data.invoice_line

                for items in account_invoice_ids:
                    without_vat_amount = items.price_unit * items.quantity

                    # vat_amount = ((items.price_unit - round(items.product_id.average_purchaes_price,
                    #                                         2)) * items.quantity) * 0.05
                    # p_vat = vat_amount if vat_amount >= 0 else 0.0
                    try:
                        sale_price_per_unit = items.price_unit + (items.five_vat / items.quantity)

                    except ZeroDivisionError:

                        sale_price_per_unit = items.price_unit

                    total_amount = items.total_price_with_vat
                    service_vat = (items.five_vat * 100) / 5
                    p_t_amount = (total_amount - service_vat) - items.five_vat

                    total_vat += items.five_vat
                    total_subtotal += total_amount
                    p_total_amount += p_t_amount
                    total_service_vat += service_vat

        return [{'total_vat': round(total_vat, 2),
                 'total_subtotal': round(total_subtotal, 2),
                 'p_total_amount': round(p_total_amount, 2),
                 'total_service_vat': round(total_service_vat, 2),
                 }]

    def get_basic_info(self, obj):



        try:
            order_numbers = [obj.split(',')[0]]
        except:
            order_numbers = [str(obj)]

        data_dict = {}

        self.cr.execute("SELECT id FROM sale_order WHERE client_order_ref=%s", (order_numbers))
        so_id = [so[0] for so in self.cr.fetchall()]


        if so_id is not None:

            order_data = self.pool.get('sale.order').browse(self.cr, self.uid, so_id)

            wh_id=order_data.warehouse_id.id

            warehouse_map = {1: 'uttara',
                              2: 'nodda',
                              3: 'nodda',
                             6: 'uttara',
                             21: 'signboard',
                             27: 'badda',
                             34: 'badda',
                             37: 'savar',
                             }

            if warehouse_map[wh_id]=='uttara':
                issue_address='House No-46, Block-B, Ward-04, Noyanagor, Main Road, Turag, Dhaka-1230'
            elif warehouse_map[wh_id]=='signboard':
                issue_address = 'Hatim supermarket Sanarpar main road,SignBoard ,Shiddhirganj, Dhaka Narayanganj-1430'
            elif warehouse_map[wh_id] == 'badda':
                issue_address = 'Vatara Thana, badda main road, sayednogor Dhaka-1212'
            elif warehouse_map[wh_id]=='savar':
                issue_address = 'Boliapur, nagarkanda, Savar, Dhaka-1212'
            else:
                issue_address = 'Ka-61, 5/A, Pragati Sharani, Baridhara, Dhaka-1212'
            shipping_address = order_data.partner_shipping_id.name
            if order_data.partner_shipping_id.street:
                shipping_address = shipping_address+", " + order_data.partner_shipping_id.street+", "
            if order_data.partner_shipping_id.street2:
                shipping_address = shipping_address + order_data.partner_shipping_id.street2+", "
            if order_data.partner_shipping_id.city:
                shipping_address = shipping_address + order_data.partner_shipping_id.city+", "
            if order_data.partner_shipping_id.state_id:
                shipping_address = shipping_address + order_data.partner_shipping_id.state_id.name+", "
            if order_data.partner_shipping_id.zip:
                shipping_address = shipping_address + order_data.partner_shipping_id.zip+", "
            if order_data.partner_invoice_id.country_id:
                shipping_address = shipping_address + order_data.partner_shipping_id.country_id.name

            billing = order_data.partner_id.parent_id if order_data.partner_id.parent_id else order_data.partner_id
            billing_address = ''
            if billing.street:
                billing_address = billing.street+", "
            if billing.street2:
                billing_address = billing_address + billing.street2+", "
            if billing.city:
                billing_address = billing_address + billing.city+", "
            if billing.state_id:
                billing_address = billing_address + billing.state_id.name+", "
            if billing.zip:
                billing_address = billing_address + billing.zip+", "
            if billing.country_id:
                billing_address = billing_address + billing.country_id.name


            data_dict.update({'partner_name':order_data.partner_id.parent_id.name if order_data.partner_id.parent_id.name else order_data.partner_id.name,
                               'partner_bin_no':'',
                               'company_bin_no':'000064664-0101',
                              'shipping_address':shipping_address,
                              'company_name':'Sindabad.com Ltd.',
                              'issue_address':issue_address,
                              'billing_address':billing_address,
                              'issued_date':datetime.date.today().strftime('%d-%m-%Y'),
                              'issued_time':datetime.datetime.now().strftime('%H:%M:%S')
            })
        data_list =[data_dict]



        return data_list






class report_packings_mushak_layout(osv.AbstractModel):
    _name = 'report.warranty_service.report_packings_mushak_layout'
    _inherit = 'report.abstract_report'
    _template = 'warranty_service.report_packings_mushak_layout'
    _wrapped_report_class = packingsmushakData
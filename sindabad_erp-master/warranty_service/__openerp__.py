# Author Mufti Muntasir Ahmed

{
    'name': 'Warranty Service Print and service',
    'version': '8.0.0',
    'category': 'Sales',
    'description': """
Generate the   
==============================================================

""",
    'author': 'Mufti Muntasir Ahmed',
    'depends': ['product','packings','order_delivery_dispatch'],
    'data': [
'views/report_packings_warranty_layout.xml',
'views/report_packings_mushak_layout.xml',
'views/report_invoice_warranty_layout.xml',
        'warranty_service_serial_add.xml',
        'warranty_service_packing.xml',
'warranty_serial_report.xml',
'invoice_warranty_menu.xml',
        'account_invoice.xml',
        'packing_warranty_button.xml',
        'packing_mushak_menu.xml',
        'packing_mushak_button.xml',
    ],

    'installable': True,
    'auto_install': False,
}


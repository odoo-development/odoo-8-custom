
from openerp.osv import osv, fields, expression



class PackingList(osv.osv):
    _inherit = "packing.list"




    def print_packing_warranty_report(self, cr, uid, ids, context=None):

        context = dict(context or {}, active_ids=ids)
        return self.pool.get("report").get_action(cr, uid, ids, 'warranty_service.report_packings_warranty_layout', context=context)


    def print_packing_mushak_report(self, cr, uid, ids, context=None):

        context = dict(context or {}, active_ids=ids)
        return self.pool.get("report").get_action(cr, uid, ids, 'warranty_service.report_packings_mushak_layout', context=context)



class product_product(osv.osv):
    _inherit = 'product.product'

    _columns = {
        'warranty_applicable': fields.boolean('Warranty Applicable'),
        'warranty_duration': fields.integer('Warranty (Month)'),
        'warranty_text': fields.text('Warranty Rule'),

    }



class PackingListLine(osv.osv):
    _inherit = 'packing.list.line'


    _columns = {
        'warranty_serials': fields.boolean('Warranty Serials')
    }

# class account_invoice_line(osv.osv):
#     _inherit = 'account.invoice.line'
#
#
#     _columns = {
#         'product_serials': fields.char('Product Serials')
#     }


class warranty_service_serial_add(osv.osv):
    _name = 'warranty.service.serial.add'


    _columns = {
        'warranty_serials': fields.boolean('Warranty Serials'),
        'invoice_id': fields.many2one('account.invoice', 'Invoice Number'),
        'warranty_service_serial_add_line': fields.one2many('warranty.service.serial.add.line', 'warranty_service_serial_add_id', 'Serial Line',
                                           required=True),

    }


    def default_get(self, cr, uid, fields, context=None):
        # product_pricelist = self.pool.get('product.pricelist')
        if context is None:
            context = {}
        res = super(warranty_service_serial_add, self).default_get(cr, uid, fields, context=context)

        if context.get('active_model') == 'packing.list.line':
            packing_line_obj = self.pool.get("packing.list.line")
            packing_line = packing_line_obj.browse(cr, uid, context['active_id'], context=context)
            invoice = packing_line.invoice_id
        else:
            inv_obj = self.pool.get("account.invoice")
            inv_data = inv_obj.browse(cr, uid, context['active_id'], context=context)
            invoice = inv_data

        items = list()

        for line in invoice.invoice_line:
            if line.quantity>0.00 and line.product_id.warranty_applicable == True:
                item = {
                    'product_id': line.product_id.id,
                    'quantity': line.quantity,
                    'invoice_line_id': line.id,
                    'serial': line.x_product_serial
                }

                if line.product_id.default_code and line.product_id.default_code != '':
                    items.append(item)
        res['invoice_id'] = invoice.id

        res.update(warranty_service_serial_add_line=items)


        return res


    def serial_add(self, cr, uid, fields, context=None):

        warranty_service = self.browse(cr, uid, fields[0], context=context)

        ## update invoice Line

        for wt_items in warranty_service.warranty_service_serial_add_line:

            try:
                if wt_items.serial is not False:
                    cr.execute("UPDATE account_invoice_line set x_product_serial=%s where id = %s", (str(wt_items.serial), wt_items.invoice_line_id.id))
                    cr.commit()
            except:
                pass


        return True


class warranty_service_serial_add_line(osv.osv):
    _name = 'warranty.service.serial.add.line'


    _columns = {
        'warranty_service_serial_add_id': fields.many2one('warranty.service.serial.add', 'Warranty Service Serial Add', required=True,
                                          ondelete='cascade', select=True, readonly=True),
        'product_id': fields.many2one('product.product', 'Product', required=True),
        'invoice_line_id': fields.many2one('account.invoice.line', 'Invoice Line'),
        'quantity': fields.float('Quantity'),
        'serial': fields.text('Serial'),

    }














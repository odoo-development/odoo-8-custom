# Author Mufti Muntasir Ahmed 25-04-2018


import xlwt
from datetime import datetime
from openerp.osv import orm,fields, osv
from openerp.report import report_sxw
from openerp.addons.report_xls.report_xls import report_xls
from openerp.addons.report_xls.utils import rowcol_to_cell, _render
from openerp.tools.translate import translate, _
import logging
_logger = logging.getLogger(__name__)


_ir_translation_name = 'account.report.xls'


class account_report_xls_parser(report_sxw.rml_parse):

    def __init__(self, cr, uid, name, context):
        super(account_report_xls_parser, self).__init__(
            cr, uid, name, context=context)
        move_obj = self.pool.get('account.account')
        self.context = context
        wanted_list = move_obj._report_xls_fields(cr, uid, context)
        template_changes = move_obj._report_xls_template(cr, uid, context)
        self.localcontext.update({
            'datetime': datetime,
            'wanted_list': wanted_list,
            'template_changes': template_changes,
            '_': self._,
        })

    def _(self, src):
        lang = self.context.get('lang', 'en_US')
        return translate(self.cr, _ir_translation_name, 'report', lang, src) \
            or src


class account_report_xls(report_xls):

    def __init__(self, name, table, rml=False, parser=False, header=True,
                 store=False):
        super(account_report_xls, self).__init__(
            name, table, rml, parser, header, store)

        # Cell Styles
        _xs = self.xls_styles
        # header
        rh_cell_format = _xs['bold'] + _xs['fill'] + _xs['borders_all']
        self.rh_cell_style = xlwt.easyxf(rh_cell_format)
        self.rh_cell_style_center = xlwt.easyxf(rh_cell_format + _xs['center'])
        self.rh_cell_style_right = xlwt.easyxf(rh_cell_format + _xs['right'])
        # lines
        aml_cell_format = _xs['borders_all']
        self.aml_cell_style = xlwt.easyxf(aml_cell_format)
        self.aml_cell_style_center = xlwt.easyxf(
            aml_cell_format + _xs['center'])
        self.aml_cell_style_date = xlwt.easyxf(
            aml_cell_format + _xs['left'],
            num_format_str=report_xls.date_format)
        self.aml_cell_style_decimal = xlwt.easyxf(
            aml_cell_format + _xs['right'],
            num_format_str=report_xls.decimal_format)
        # totals
        rt_cell_format = _xs['bold'] + _xs['fill'] + _xs['borders_all']
        self.rt_cell_style = xlwt.easyxf(rt_cell_format)
        self.rt_cell_style_right = xlwt.easyxf(rt_cell_format + _xs['right'])
        self.rt_cell_style_decimal = xlwt.easyxf(
            rt_cell_format + _xs['right'],
            num_format_str=report_xls.decimal_format)

        # XLS Template
        self.col_specs_template = {
            'journal_date': {
                'header': [1, 20, 'text', _render("_('Journal Date')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},
            'period_name': {
                'header': [1, 20, 'text', _render("_('Period')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},
            'account_name': {
                'header': [1, 20, 'text', _render("_('Acount')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},
            'referance': {
                'header': [1, 20, 'text', _render("_('Reference')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},
            'debit': {
                'header': [1, 20, 'text', _render("_('Debit')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},
            'credit': {
                'header': [1, 20, 'text', _render("_('Credit')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},
            'previous_balance': {
                'header': [1, 20, 'text', _render("_('Previous Balance')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},
            'cum_balamce': {
                'header': [1, 20, 'text', _render("_('Balance')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]}


        }



    def generate_xls_report(self, _p, _xs, data, objects, wb):

        wanted_list = _p.wanted_list
        self.col_specs_template.update(_p.template_changes)
        _ = _p._

        context = self.context
        st_date = data.get('form').get('date_from')
        end_date = data.get('form').get('date_to')
        account_id = data.get('form').get('account_id')
        context['start_date'] = st_date
        context['end_date'] = end_date
        self.context = context
        date_range = _("Date: %s to %s" % (st_date, end_date))

        # report_name = objects[0]._description or objects[0]._name
        report_name = _("Ledger Details Statement")
        ws = wb.add_sheet(report_name[:31])
        ws.panes_frozen = True
        ws.remove_splits = True
        ws.portrait = 0  # Landscape
        ws.fit_width_to_pages = 1
        row_pos = 0

        # set print header/footer
        ws.header_str = self.xls_headers['standard']
        ws.footer_str = self.xls_footers['standard']

        # Title
        cell_style = xlwt.easyxf(_xs['xls_title'])
        c_specs = [
            ('report_name', 4, 3, 'text', report_name),
        ]
        row_data = self.xls_row_template(c_specs, ['report_name'])
        row_pos = self.xls_write_row(
            ws, row_pos, row_data, row_style=cell_style)
        row_pos += 1

        # Date Ranges
        cell_style = xlwt.easyxf(_xs['xls_title'])
        c_specs = [
            ('date_range', 4, 3, 'text', date_range),
        ]
        row_data = self.xls_row_template(c_specs, ['date_range'])
        row_pos = self.xls_write_row(
            ws, row_pos, row_data, row_style=cell_style)
        row_pos += 1

        # Column headers
        c_specs = map(lambda x: self.render(
            x, self.col_specs_template, 'header', render_space={'_': _p._}),
            wanted_list)
        row_data = self.xls_row_template(c_specs, [x[0] for x in c_specs])
        row_pos = self.xls_write_row(
            ws, row_pos, row_data, row_style=self.rh_cell_style,
            set_column_size=True)
        ws.set_horz_split_pos(row_pos)

        previous_balance = 0
        cum_balamce = 0
        if account_id:
            account_id=account_id[0]
        else:
            raise osv.except_osv(_('Warning!'),
                                 _('Please Select a ledger'))





        prev_query = "select sum(account_move_line.debit) as total_debit,sum(account_move_line.credit) as toa_credit ,account_move_line.account_id from account_move_line, account_move where account_move_line.move_id = account_move.id and account_move_line.state !='cancel' and account_move_line.account_id=%s and account_move.date < %s group by account_move_line.account_id"

        self.cr.execute(prev_query,(account_id,st_date))

        prev_objects = self.cr.dictfetchall()

        for p_items in prev_objects:
            previous_balance = p_items.get('total_debit') - p_items.get('toa_credit')

        cum_balamce = previous_balance

        count =0



        query = "select account_move.date as journal_date, (select name from account_period where account_period.id=account_move.period_id) as period_name,(select name from account_account where account_account.id=account_move_line.account_id) as account_name,account_move_line.ref as referance ,account_move_line.account_id,account_move_line.debit,account_move_line.credit,(select name from res_partner where res_partner.id=account_move_line.partner_id) as partner_name,account_move_line.state from account_move_line, account_move where account_move_line.move_id = account_move.id and account_move_line.state !='cancel' and account_move_line.account_id=%s and account_move.date >= %s and account_move.date <= %s"


        self.cr.execute(query, (account_id,st_date, end_date))
        objects = self.cr.dictfetchall()

        if len(objects)> 0:

            for line in objects:

                if count > 0:
                    previous_balance =''
                count = count +1



                c_specs = map(
                    lambda x: self.render(x, self.col_specs_template, 'lines'),
                    wanted_list)
                for list_data in c_specs:
                    if str(list_data[0]) == str('journal_date'):
                        list_data[4] = str(line.get('journal_date'))


                    if str(list_data[0]) == str('journal_date'):
                        list_data[4] = str(line.get('journal_date'))


                    if str(list_data[0]) == str('account_name'):
                        list_data[4] = str(line.get('account_name'))


                    if str(list_data[0]) == str('referance'):
                        list_data[4] = str(line.get('referance'))


                    if str(list_data[0]) == str('debit'):
                        cum_balamce = cum_balamce + line.get('debit')
                        list_data[4] = str(line.get('debit'))


                    if str(list_data[0]) == str('credit'):
                        cum_balamce = cum_balamce - line.get('credit')
                        list_data[4] = str(line.get('credit'))


                    if str(list_data[0]) == str('previous_balance'):
                        list_data[4] = str(previous_balance)


                    if str(list_data[0]) == str('cum_balamce'):
                        list_data[4] = str(cum_balamce)


                row_data = self.xls_row_template(c_specs, [x[0] for x in c_specs])
                row_pos = self.xls_write_row(
                    ws, row_pos, row_data, row_style=self.aml_cell_style)

        else:
            c_specs = map(
                lambda x: self.render(x, self.col_specs_template, 'lines'),
                wanted_list)
            for list_data in c_specs:
                if str(list_data[0]) == str('previous_balance'):
                    list_data[4] = str(previous_balance)

                if str(list_data[0]) == str('cum_balamce'):
                    list_data[4] = str(cum_balamce)

            row_data = self.xls_row_template(c_specs, [x[0] for x in c_specs])
            row_pos = self.xls_write_row(
                ws, row_pos, row_data, row_style=self.aml_cell_style)





account_report_xls('report.account.report.xls',
                    'account.account',
                    parser=account_report_xls_parser)

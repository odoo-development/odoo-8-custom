from openerp.osv import osv, fields



class res_partner(osv.Model):
    _inherit = 'res.partner'

    def _init_lines_value(self,  cr, uid,partner):

        parent_list = []

        if partner.parent_id:
            parent_list.append(partner.parent_id.id)

            cr.execute("select id from  res_partner where customer=True and id=%s", ([partner.parent_id.id]))

            for items in cr.fetchall():
                parent_list.append(items[0])
        else:
            parent_list.append(partner.id)


        cr.execute("select id from  res_partner where customer=True and parent_id=%s", ([partner.id]))
        for items in cr.fetchall():
            parent_list.append(items[0])

        moveline_obj = self.pool['account.move.line']
        movelines = moveline_obj.search(cr, uid,
                                        [('partner_id', 'in', parent_list),
                                         ('account_id.type', 'in', ['receivable', 'payable']),
                                            ('state', '<>', 'draft')])

        movelines = moveline_obj.browse(cr, uid, movelines)

        previous_balance = 0

        for items in movelines:
            previous_balance += (items.debit - items.credit)

        return movelines



    def _statement_balance(self, cr, uid, ids, name, arg, context=None):
        res = {}
        for partner in self.browse(cr, uid, ids, context=context):
            res[partner.id] = reduce(lambda x, y: x + (y['debit'] - y['credit']),
                                     self._init_lines_value(cr,uid,partner), 0)


        return res



    def _ledger_balance(self, cr, uid, ids, name, arg, context=None):
        res = {}
        for partner in self.browse(cr, uid, ids, context=context):
            previous_balance=0
            if partner.is_company == True and partner.property_account_receivable.id > 0:
                prev_query = "select sum(account_move_line.debit) as total_debit,sum(account_move_line.credit) as toa_credit ,account_move_line.account_id from account_move_line, account_move where account_move_line.move_id = account_move.id and account_move_line.state !='cancel' and account_move_line.account_id=%s group by account_move_line.account_id"

                cr.execute(prev_query, ([partner.property_account_receivable.id]))

                prev_objects = cr.dictfetchall()
                for p_items in prev_objects:
                    previous_balance = p_items.get('total_debit') - p_items.get('toa_credit')
                res[partner.id] = previous_balance
            else:
                res[partner.id]=0


        return res

    def company_accounting_mapping(self, cr, uid, ids, context=None):
        for partner_id in context.get('active_ids'):
            partner_pool = self.pool.get('res.partner')
            partner_object = partner_pool.browse(cr, uid, partner_id, context=context)
            partner_account_id =partner_object.property_account_receivable.id

            company_data = partner_object.is_company

            child_ids = [partner_id]


            for p_id in partner_object.child_ids:
                child_ids.append(p_id.id)
            if partner_account_id != 7074 and company_data == True and partner_account_id is not False:
                cr.execute("update account_move_line as avl set account_id=%s where avl.id in (select account_move_line.id from account_move_line where account_id =7074 and partner_id in %s)", (partner_account_id, tuple(child_ids),))
                cr.commit()

                cr.execute("update account_voucher_line as al set account_id=%s where al.id in (select account_voucher_line.id from account_voucher_line,account_voucher where account_voucher_line.voucher_id=account_voucher.id and account_voucher_line.account_id = 7074 and partner_id in %s)",(partner_account_id, tuple(child_ids),))
                cr.commit()




        return ''


    _columns = {

        'statement_balance': fields.function(_statement_balance, type='text', string='Customer Statement Balance'),
        'ledger_balance': fields.function(_ledger_balance, type='text', string='Customer Ledger Balance'),

    }



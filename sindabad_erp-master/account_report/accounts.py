# Author Mufti Muntasir Ahmed 25-04-2018

from openerp import models, api


class account_account(models.Model):
    _inherit = 'account.account'

    @api.model
    def _report_xls_fields(self):

        return [
            'journal_date','period_name','account_name','referance','debit','credit','previous_balance','cum_balamce'


        ]

    # Change/Add Template entries
    @api.model
    def _report_xls_template(self):

        return {}

# Author S. M. Sazedul Haque 2018/11/07

from openerp.osv import fields, osv
import datetime


class AccountReportWiz(osv.osv_memory):
    """
    This wizard will provide the partner Ledger report by periods, between any two dates.
    """
    _name = 'account.report.wiz'
    _description = 'Account Report Wiz'

    _columns = {
        'date_from': fields.date("Start Date"),
        'date_to': fields.date("End Date"),
        'account_id':fields.many2one('account.account', 'Account')
    }

    _defaults = {
        'date_from': datetime.datetime.today(),
        'date_to': datetime.datetime.today()
    }

    def _build_contexts(self, cr, uid, ids, data, context=None):
        if context is None:
            context = {}
        result = {'date_from': data['form']['date_from'], 'date_to': data['form']['date_to'], 'account_id': data['form']['account_id']}

        return result

    def check_report(self, cr, uid, ids, context=None):
        if context is None:
            context = {}

        data = {}

        data = self.read(cr, uid, ids, ['date_from', 'date_to','account_id'], context=context)[0]

        datas = {
            'ids': context.get('active_ids', []),
            'model': 'account.report.wiz',
            'form': data
        }
        return {'type': 'ir.actions.report.xml',
                'report_name': 'account.report.xls',
                'datas': datas
                }

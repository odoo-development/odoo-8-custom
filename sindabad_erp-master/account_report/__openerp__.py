# Author Mufti Muntasir Ahmed 25-04-2018

{
    'name': 'Account Report',
    'version': '8.0.0.6.0',
    'author': "Mufti and Shuvarthi",
    'category': 'Accounting & Finance',
    'summary': 'Accounts report',
    'depends': ['account','report_xls'],
    'data': [
        'security/account_report_security.xml',
        'security/ir.model.access.csv',
        'wizard/account_report_filter.xml',
        'report/account_report_xls.xml',
        'partner_accounting_mapping.xml',
    ],
}

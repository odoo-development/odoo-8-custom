from openerp.osv import fields, osv
import datetime

class sale_order(osv.osv):
    _inherit = "sale.order"

    _columns = {
        'invoice_created': fields.boolean(string='Invoice Created', type='boolean'),
        'full_refund_invoiced': fields.boolean(string='Refund Invoiced', type='boolean'),
    }


class account_invoice(osv.osv):
    _inherit = "account.invoice"

    # def create(self, cr, uid, vals, context=None):
    #
    #     res = super(account_invoice, self).create(cr, uid, vals, context=context)
    #     import pdb;
    #     pdb.set_trace()
    #
    #
    #     invoice_info = self.browse(cr, uid, res, context=context)
    #     if invoice_info.name:
    #         so = self.pool.get('sale.order').browse(cr, uid,
    #                                                 self.pool.get('sale.order').search(cr, uid, [('client_order_ref', '=', invoice_info.name)]),
    #
    #
    #                                                 context=context)
    #
    #         context_data={}
    #         for order in so:
    #             if order.invoice_ids:
    #                 invoice_created_query = "UPDATE sale_order SET invoice_created= TRUE WHERE id={0}".format(order.id)
    #                 cr.execute(invoice_created_query)
    #                 cr.commit()
    #             out_refund_amount = 0
    #             out_invoice_amount = 0
    #
    #             for invoice in order.invoice_ids:
    #
    #                 if invoice.type == 'out_invoice':
    #                     out_invoice_amount += invoice.amount_total
    #
    #                 if invoice.type == 'out_refund':
    #                     out_refund_amount += invoice.amount_total
    #
    #             if out_invoice_amount == out_refund_amount and out_invoice_amount > 0 and out_refund_amount > 0:
    #                 full_refund_invoiced_query = "UPDATE sale_order SET full_refund_invoiced = TRUE WHERE id={0}".format(
    #                     order.id)
    #                 cr.execute(full_refund_invoiced_query)
    #                 cr.commit()
    #
    #             if order.ass_for_bill_coll:
    #                 billing = self.pool.get('bill.collection.billing.process')
    #                 billing_col = billing.browse(cr, uid, self.pool.get('bill.collection.billing.process').search(
    #                     cr, uid, [
    #                         ('mag_name', '=',
    #                          order.client_order_ref), (
    #                             'state', 'not in',
    #                             ['pending_payment',
    #                              'partially_paid', 'paid',
    #                              'cancle'])]),
    #                                              context=context)
    #                 for s_id in billing_col:
    #                     if out_invoice_amount == out_refund_amount and out_invoice_amount > 0 and out_refund_amount > 0:
    #                         billing_collection_cancle = self.pool.get('cancel.bill.process.reason')
    #
    #                         values = {
    #                             'cancel_bill_process_date': datetime.datetime.now(),
    #                             'cancel_bill_process_by': uid,
    #                             'cancel_bill_process_reason': 'Full Refund',
    #                         }
    #
    #                         billing_collection_cancle.create(cr, uid, values)
    #
    #                         context_data['active_ids'] = [s_id.id]
    #                         context_data['cancel_bill_process_reason'] ='Full Refund'
    #
    #                         billing_collection_cancle.cancel_bill_process_reason_def(cr, uid, [s_id.id], context=context_data)
    #
    #     return res


    # def write(self, cr, uid,ids, vals, context=None):
    #     res = super(account_invoice, self).write(cr, uid, ids, vals, context=context)
    #
    #     invoice_info = self.browse(cr, uid, ids, context=context)
    #     if invoice_info.name:
    #
    #         so = self.pool.get('sale.order').browse(cr, uid,
    #                                                 self.pool.get('sale.order').search(cr, uid, [
    #                                                     ('client_order_ref', '=', invoice_info.name)]),
    #                                                 context=context)
    #         context_data = {}
    #         for order in so:
    #             if order.invoice_ids:
    #                 invoice_created_query = "UPDATE sale_order SET invoice_created= TRUE WHERE id={0}".format(order.id)
    #                 cr.execute(invoice_created_query)
    #                 cr.commit()
    #
    #             out_refund_amount = 0
    #             out_invoice_amount = 0
    #
    #             for invoice in order.invoice_ids:
    #
    #                 if invoice.type == 'out_invoice':
    #                     out_invoice_amount += invoice.amount_total
    #
    #                 if invoice.type == 'out_refund':
    #                     out_refund_amount += invoice.amount_total
    #
    #             if out_invoice_amount == out_refund_amount and out_invoice_amount > 0 and out_refund_amount > 0:
    #
    #                 full_refund_invoiced_query = "UPDATE sale_order SET full_refund_invoiced = TRUE WHERE id={0}".format(order.id)
    #                 cr.execute(full_refund_invoiced_query)
    #                 cr.commit()
    #
    #             if order.ass_for_bill_coll:
    #                 billing = self.pool.get('bill.collection.billing.process')
    #                 billing_col = billing.browse(cr, uid,self.pool.get('bill.collection.billing.process').search(
    #                                                                                           cr, uid, [
    #                                                                                               ('mag_name', '=',
    #                                                                                                order.client_order_ref), (
    #                                                                                               'state', 'not in',
    #                                                                                               ['pending_payment',
    #                                                                                                'partially_paid', 'paid',
    #                                                                                                'cancle'])]),
    #                                                                                       context=context)
    #                 for s_id in billing_col:
    #                     if out_invoice_amount == out_refund_amount and out_invoice_amount > 0 and out_refund_amount > 0:
    #
    #                         billing_collection_cancle = self.pool.get('cancel.bill.process.reason')
    #
    #                         values={
    #                             'cancel_bill_process_date': datetime.datetime.now(),
    #                             'cancel_bill_process_by': uid,
    #                             'cancel_bill_process_reason': 'Full Refund',
    #                         }
    #
    #
    #                         billing_collection_cancle.create(cr, uid, values)
    #
    #                         context_data['active_ids'] = [s_id.id]
    #                         context_data['cancel_bill_process_reason'] =  'Full Refund'
    #
    #                         billing_collection_cancle.cancel_bill_process_reason_def(cr,uid,[s_id.id],context=context_data)
    #
    #     return res

    def invoice_validate(self, cr, uid, ids, context=None):

        uid = 1
        res = super(account_invoice, self).invoice_validate(cr, uid, ids, context=context)

        try:

            invoice_data = self.browse(cr, uid, ids, context=context)
            for invoice_info in invoice_data:
                if invoice_info.name:
                    so = self.pool.get('sale.order').browse(cr, uid,
                                                            self.pool.get('sale.order').search(cr, uid, [
                                                                ('client_order_ref', '=', invoice_info.name)]),

                                                            context=context)

                    context_data = {}
                    for order in so:
                        if order.invoice_ids:
                            invoice_created_query = "UPDATE sale_order SET invoice_created= TRUE WHERE id={0}".format(order.id)
                            cr.execute(invoice_created_query)
                            cr.commit()
                        out_refund_amount = 0
                        out_invoice_amount = 0

                        for invoice in order.invoice_ids:

                            if invoice.type == 'out_invoice':
                                out_invoice_amount += invoice.amount_total

                            if invoice.type == 'out_refund':
                                out_refund_amount += invoice.amount_total

                        if out_invoice_amount == out_refund_amount and out_invoice_amount > 0 and out_refund_amount > 0:
                            full_refund_invoiced_query = "UPDATE sale_order SET full_refund_invoiced = TRUE WHERE id={0}".format(
                                order.id)
                            cr.execute(full_refund_invoiced_query)
                            cr.commit()

                        if order.ass_for_bill_coll:
                            billing = self.pool.get('bill.collection.billing.process')
                            billing_col = billing.browse(cr, uid, self.pool.get('bill.collection.billing.process').search(
                                cr, uid, [
                                    ('mag_name', '=',
                                     order.client_order_ref), (
                                        'state', 'not in',
                                        ['pending_payment',
                                         'partially_paid', 'paid',
                                         'cancle'])]),
                                                         context=context)
                            for s_id in billing_col:
                                if out_invoice_amount == out_refund_amount and out_invoice_amount > 0 and out_refund_amount > 0:
                                    billing_collection_cancle = self.pool.get('cancel.bill.process.reason')

                                    values = {
                                        'cancel_bill_process_date': datetime.datetime.now(),
                                        'cancel_bill_process_by': uid,
                                        'cancel_bill_process_reason': 'Full Refund',
                                    }

                                    billing_collection_cancle.create(cr, uid, values)

                                    context_data['active_ids'] = [s_id.id]
                                    context_data['cancel_bill_process_reason'] = 'Full Refund'

                                    billing_collection_cancle.cancel_bill_process_reason_def(cr, uid, [s_id.id],
                                                                                         context=context_data)
        except:
            pass

        return res

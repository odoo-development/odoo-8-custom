{
    'name': "Sale Order Invoice Created and Full Refund Mark",
    'version': '1.0',
    'category': 'Sale',
    'author': "Shuvarthi Dhar",
    'description': """
Invoice Create and Full Refund Sale Order Mark
    """,
    'website': "http://www.sindabad.com",
    'depends': ['base', 'odoo_magento_connect', 'account', 'sale', 'stock','order_approval_process','send_sms_on_demand'],
    'data': [
        # 'sale_view.xml',
    ],

    'installable': True,
    'application': True,
    'auto_install': False,
}

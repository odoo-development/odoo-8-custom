from openerp.osv import osv, fields, expression

class StockPicking(osv.osv):
    _inherit = 'stock.picking'

    def _get_customer_name(self, cursor, user, ids, name, arg, context=None):
        res = {}

        for items in self.browse(cursor, user, ids, context=context):

            res[items.id] = items.partner_id.parent_id.name if items.partner_id.parent_id.name else items.partner_id.name
        return res

    _columns = {

    'customer_name': fields.function(_get_customer_name, string='Customer Name', type='char')
    }




class AccountInvoice(osv.osv):
    _inherit = 'account.invoice'

    def _get_customer_name(self, cursor, user, ids, name, arg, context=None):
        res = {}

        for items in self.browse(cursor, user, ids, context=context):

            res[items.id] = items.partner_id.parent_id.name if items.partner_id.parent_id.name else items.partner_id.name
        return res

    _columns = {

    'customer_name': fields.function(_get_customer_name, string='Customer Name', type='char')
    }
# -*- coding: utf-8 -*-

from openerp.osv import fields, osv
from xml.etree import ElementTree

class XmlDictConfig(dict):

    def __init__(self, parent_element):
        if parent_element.items():
            self.update(dict(parent_element.items()))
        for element in parent_element:
            if len(element):
                # treat like dict - we assume that if the first two tags
                # in a series are different, then they are all different.
                if len(element) == 1 or element[0].tag != element[1].tag:
                    aDict = XmlDictConfig(element)
                # treat like list - we assume that if the first two tags
                # in a series are the same, then the rest are the same.
                else:
                    # here, we put the list in dictionary; the key is the
                    # tag name the list elements all share in common, and
                    # the value is the list itself
                    aDict = {element[0].tag: XmlListConfig(element)}
                # if the tag has attributes, add those to the dict
                if element.items():
                    aDict.update(dict(element.items()))
                self.update({element.tag: aDict})
            # this assumes that if you've got an attribute in a tag,
            # you won't be having any text. This may or may not be a
            # good idea -- time will tell. It works for the way we are
            # currently doing XML configuration files...
            elif element.items():
                self.update({element.tag: dict(element.items())})
            # finally, if there are no child tags and no attributes, extract
            # the text
            else:
                self.update({element.tag: element.text})

class common_sms(osv.osv):
    """COMMON SMS sending using SSL ."""
    _name = "common.sms"
    _description = "Module for Sms."

    _columns = {
        'to': fields.char("To:", required=True),
        'reference_id': fields.char("Reference ID:"),
        'sending_type': fields.selection([
            ('auto', 'Auto'),
            ('manual', 'Manual'),
        ], "Sending Type"),
        'state': fields.selection([
            ('init', 'init'),
            ('delivered', 'Delivered'),
            ('undelivered', 'Undelivered'),
        ], "SMS State"),
        'msg': fields.text("Message:"),
        'partner_id': fields.many2one('res.partner', 'Partner', ondelete='set null',
             help="Linked User By whom the sms sending is going on"),
    }

    _defaults = {
        'sending_type': 'manual',
        'state': 'init'
    }


    def create(self, cr, uid, data, context=None):
        context = dict(context or {})
        data['partner_id'] = uid

        common_sms_id = super(common_sms, self).create(cr, uid, data, context=context)
        common_sms_obj = self.browse(cr, uid, common_sms_id, context=context)
        sms_obj = self.pool.get('common.sms').send_common_sms(cr, uid, common_sms_obj.to, common_sms_obj.msg, common_sms_obj.id, context)
        return common_sms_id

    def action_resend(self, cr, uid, data, context=None):
        context = dict(context or {})
        common_sms_obj = self.pool.get('common.sms').browse(cr, uid, data[0], context=context)
        sms_obj = self.pool.get('common.sms').send_common_sms(cr, uid, common_sms_obj.to, common_sms_obj.msg, common_sms_obj.id, context)
        return sms_obj


    def send_common_sms(self, cr, uid, mobile_number, sms_text=None, id=None, context=None):
        if not context:
            context = {}
        if not mobile_number:
            return False
        ssl_config = self.pool["sms.mail.server"].search(cr, uid, [("gateway","=","ssl")], order='sequence asc', limit=1, context=context)
        ssl_config_obj = self.pool["sms.mail.server"].browse(cr, uid, ssl_config, context=context)


        new_r = self.pool["sms.sms"].send_sms_by_ssl(cr, uid, uid, body_sms=sms_text, mob_no=mobile_number,
            uniqueid=uid, context=context)
        if new_r is not None:
            root = ElementTree.XML(new_r)
            xmldict = XmlDictConfig(root)
            if xmldict['LOGIN'] == "SUCCESSFULL" and "REFERENCEID" in xmldict['SMSINFO']:
                self.pool.get('common.sms').write(cr, uid, [id], {'state': 'delivered', 'reference_id': xmldict['SMSINFO']['REFERENCEID']}, context=context)
                return True
            else:
                self.pool.get('common.sms').write(cr, uid, [id], {'state': 'undelivered'}, context=context)
                return False
        else:
            pass


    # def send_common_sms(self, cr, uid, ids, sms_text, context=None):
    # 	if not context:
    # 		context = {}
    #
    # 	partner_obj = self.pool.get('res.partner')
    #
    # 	for id in partner_obj.search(cr, uid, [('id' ,'in', ids)], context=context):
    # 		c_obj = partner_obj.browse(cr, uid, [id])
    # 				mobile_numbers = c_obj.mobile
    # 				if not mobile_numbers:
    # 					break
    # 				ssl_config = self.pool["sms.mail.server"].search(cr, uid, [("gateway","=","ssl")], order='sequence asc', limit=1, context=context)
    # 				ssl_config_obj = self.pool["sms.mail.server"].browse(cr, uid, ssl_config, context=context)
    #
    #
    # 				new_r = self.pool["sms.sms"].send_sms_by_ssl(cr, uid, ids, body_sms=sms_text, mob_no=mobile_numbers,
    # 					uniqueid=pr_obj.name, context=context)
    # 	return True


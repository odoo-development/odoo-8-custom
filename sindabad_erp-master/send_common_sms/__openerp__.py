# -*- coding: utf-8 -*-
{
    'name': "send_common_sms",

    'summary': """
        using this module user can send custom_sms  to a given number""",

    'description': """
        using this module user can send custom_sms  to a given number
    """,

    'author': "S. M. Sazedul Haque - Zero Gravity Ventures Ltd",
    'website': "http://zerogravity.com.bd/",

    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base', 'sms_notification', 'ssl_sms_gateway'],

    # always loaded
    'data': [
        'security/send_common_sms_security.xml',
        'security/ir.model.access.csv',
        'templates.xml',
    ],

}
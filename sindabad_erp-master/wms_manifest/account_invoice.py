import itertools
from lxml import etree

from openerp import models, fields, api, _
from openerp.exceptions import except_orm, Warning, RedirectWarning
from openerp.tools import float_compare
import openerp.addons.decimal_precision as dp


class account_invoice(models.Model):
    _inherit = "account.invoice"

    assigned = fields.Boolean(help="Assigned")

class sale_order(models.Model):
    _inherit = "sale.order"

    partially_invoiced_dispatch = fields.Boolean(help="Partially Dispatched")


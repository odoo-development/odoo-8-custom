import time
from openerp.report import report_sxw
from openerp.osv import osv


class WMSManifestReport(report_sxw.rml_parse):
    def __init__(self, cr, uid, name, context):
        super(WMSManifestReport, self).__init__(cr, uid, name, context)
        self.localcontext.update({
            'get_wms_manifest_detail': self.get_wms_manifest_detail,

        })

    def get_wms_manifest_detail(self, obj):

        manifest_obj = None

        return manifest_obj


class report_wms_manifest_layout(osv.AbstractModel):
    _name = 'report.wms_manifest.report_wms_manifest_layout'
    _inherit = 'report.abstract_report'
    _template = 'wms_manifest.report_wms_manifest_layout'
    _wrapped_report_class = WMSManifestReport

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:

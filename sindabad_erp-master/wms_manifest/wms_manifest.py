from datetime import datetime, timedelta
import time
from datetime import date
from openerp import SUPERUSER_ID
from openerp.osv import fields, osv
from openerp.tools.translate import _
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT
import openerp.addons.decimal_precision as dp
from openerp import workflow
from openerp import api
import datetime


class wms_manifest_process(osv.osv):
    _name = "wms.manifest.process"
    _description = "WMS Manifest Process"

    def _get_manifest_total_amount(self, cr, uid, ids, field_name, arg, context=None):

        res = {}
        if context is None:
            context = {}

        for val in self.browse(cr, uid, ids, context=context):
            total_amount = 0

            for p_line in val.picking_line:
                total_amount = total_amount + p_line.amount

            res[val.id] = total_amount

        return res

    _columns = {
        'courier_name': fields.char('Courier Name', required=True),
        'vehicle_number': fields.char('Vehicle Number'),
        'assigned_to': fields.char('Assigned To'),
        'Assignee': fields.many2one('res.users', 'Assignee'),
        'vehicle_from': fields.float('Start Km', required=False),
        'vehicle_to': fields.float('End Km', required=False),
        'remark': fields.text('Remark'),
        'scan': fields.char('Scan'),
        'picking_line': fields.one2many('wms.manifest.line', 'wms_manifest_id', 'Assign to Courier', required=True),
        'confirm_time': fields.datetime('Confirmation Date', readonly=True, select=True,
                                       help="Date on which sales order is confirmed."),
        'confirm_by': fields.many2one('res.users', 'Confirm By'),
        'cancel_time': fields.datetime('Cancelletion Date', readonly=True, select=True,
                                       help="Date on which sales order is cancelled."),
        'cancel_by': fields.many2one('res.users', 'Cancel By'),
        'manifest_total_amount': fields.function(_get_manifest_total_amount, string='Total Amount', type='float'),
        'state': fields.selection([
            ('pending', 'Pending'),
            ('confirm', 'Confirmation'),
            ('cancel', 'Cancelled'),

        ], 'Status', readonly=True, copy=False, help="Gives the status of the Mainfest", select=True),

    }

    _defaults = {
        'user_id': lambda obj, cr, uid, context: uid,
        'state': 'pending',
        'Assignee': lambda obj, cr, uid, context: uid,
    }

    _order = 'id desc'

    def _check_packing_line_for_same_invoice(self, packing_line, invoice_id):
        same_invoice = False

        for line in packing_line:
            if line['invoice_id'] == invoice_id:
                same_invoice = True

        return same_invoice

    def check_invoice_assigned(self, invoice_id):
        assigned = False

        check_assigned_query = "SELECT assigned FROM account_invoice WHERE id={0}".format(invoice_id)
        self._cr.execute(check_assigned_query)

        for a in self._cr.fetchall():
            inv_assigned = a[0]
            if inv_assigned == True:
                assigned = True
            else:
                assigned = False

        return assigned

    def check_invoice_delivered(self, invoice_id):
        delivered = False

        check_delivered_query = "SELECT delivered FROM account_invoice WHERE id={0}".format(invoice_id)
        self._cr.execute(check_delivered_query)

        for a in self._cr.fetchall():
            inv_delivered = a[0]
            if inv_delivered == True:
                delivered = True
            else:
                delivered = False

        return delivered

    def check_invoice_return(self, invoice_id):
        returned = False

        check_return_query = "SELECT type FROM account_invoice WHERE id={0}".format(invoice_id)
        self._cr.execute(check_return_query)

        for a in self._cr.fetchall():
            inv_returned = a[0]
            if inv_returned == 'out_refund':
                returned = True
            else:
                returned = False

        return returned


    @api.onchange('scan')
    def invoice_barcode_onchange(self):
        obj_scan = self.env['wms.manifest.process']
        invoice_id = str(self.scan)

        try:
            # invoice_id = str(self.scan)

            if invoice_id != 'False':

                invoice_id = int(invoice_id)

                if self.check_invoice_assigned(invoice_id):
                    # raise message. invoice already assigned

                    self.scan = ""

                    raise osv.except_osv(_('Warning!'), _('This date of birth is too old!'))
                if self.check_invoice_delivered(invoice_id):
                    self.scan = ""
                    raise osv.except_osv(_('Warning!'), _('This order is already delivered.'))
                elif self.check_invoice_return(invoice_id):
                    self.scan = ""
                    raise osv.except_osv(_('Warning!'), _('This order is already returned.'))
                else:

                    invoice_env = self.env['account.invoice']
                    invoice_obj = invoice_env.search([('id', '=', invoice_id)])

                    so_env = self.env['sale.order']
                    deli_area = ""
                    deli_address = ""
                    deli_phone = ""
                    if invoice_obj:
                        so = so_env.search([('client_order_ref', '=', str(invoice_obj.name))])
                        deli_area = so.partner_shipping_id.state_id.name
                        deli_address = so.partner_shipping_id.street
                        deli_phone = so.partner_shipping_id.mobile
                        # deli_phone = self.pool.get("send.sms.on.demand").get_customer_mobile(self._cr, 1, so, context=self._context)

                    if self.picking_line:

                        if invoice_obj:

                            if self._check_packing_line_for_same_invoice(self.picking_line, invoice_id):

                                self.scan = ""
                            else:

                                new_picking_line = []
                                for items in self.picking_line:
                                    new_picking_line.append({
                                        'invoice_id': items['invoice_id'],
                                        'magento_no': items['magento_no'],
                                        'customer_name': items['customer_name'],
                                        'phone': items['phone'],
                                        'address': items['address'],
                                        'payment_type': items['payment_type'].replace("Payment Information:-", ""),
                                        'amount': items['amount'],
                                        'status': items['status'],
                                        'area': items['area']
                                    })

                                new_picking_line.append({
                                    'invoice_id': invoice_id,
                                    'magento_no': str(invoice_obj.name),
                                    'customer_name': str(invoice_obj.partner_id.name),
                                    # 'phone': str(invoice_obj.partner_id.phone),
                                    'phone': str(deli_phone),
                                    # 'address': str(invoice_obj.commercial_partner_id.street),
                                    'address': str(deli_address),
                                    'payment_type': str(invoice_obj.comment).replace("Payment Information:-", ""),
                                    'amount': float(invoice_obj.amount_total),
                                    'status': str(invoice_obj.state),
                                    # 'area': str(invoice_obj.commercial_partner_id.city),
                                    'area': str(deli_area)
                                })
                                self.picking_line = new_picking_line
                                self.scan = ""
                        else:
                            self.scan = ""
                    else:

                        if invoice_obj:
                            # invoice_obj = invoice_env.search([('id', '=', invoice_id)])

                            self.picking_line = [{
                                'invoice_id': invoice_id,
                                'magento_no': str(invoice_obj.name),
                                'customer_name': str(invoice_obj.partner_id.name),
                                # 'phone': str(invoice_obj.partner_id.phone),
                                'phone': str(deli_phone),
                                # 'address': str(invoice_obj.commercial_partner_id.street),
                                'address': str(deli_address),
                                'payment_type': str(invoice_obj.comment).replace("Payment Information:-", ""),
                                'amount': float(invoice_obj.amount_total),
                                'status': str(invoice_obj.state),
                                # 'area': str(invoice_obj.commercial_partner_id.city),
                                'area': str(deli_area)
                            }]

                        self.scan = ""
        except:
            self.scan = ""
            if self.check_invoice_delivered(invoice_id):
                self.scan = ""
                raise osv.except_osv(_('Warning!'), _('This order is already delivered.'))
            elif self.check_invoice_return(invoice_id):
                self.scan = ""
                raise osv.except_osv(_('Warning!'), _('This order is already returned.'))
            elif self.check_invoice_assigned(invoice_id):
                # raise message. invoice already assigned
                self.scan = ""
                raise osv.except_osv(_('Warning!'), _('This order is already assigned for manifest'))

        return "xXxXxXxXxX"


    @api.model
    def create(self, vals):



        f_list=[]
        new_list=[]
        for items in vals['picking_line']:
            f_list.append(items[2])
        for it in f_list:
            if int(it.get('invoice_id')) >0:
                new_list.append(it)


        new_list =[dict(tupleized) for tupleized in set(tuple(item.items()) for item in new_list)]
        picking_line =[]

        for item in new_list:
            picking_line.append([0,False,item])

        vals['picking_line'] = picking_line


        try:

            if vals:

                for v in vals['picking_line']:

                    invoice_id = v[2].get('invoice_id')

                    account_invoice_assign_query = "UPDATE account_invoice SET assigned=TRUE WHERE id='{0}'".format(int(invoice_id))
                    self._cr.execute(account_invoice_assign_query)
                    self._cr.commit()
        except:
            pass

        return super(wms_manifest_process, self).create(vals)



    def write(self, cr, uid, ids, vals, context=None):
        if not context:
            context = {}

        deleted_picking_line = []
        if vals.get('picking_line') :
            for items in vals.get('picking_line'):
                if items[0] == 2:
                    deleted_picking_line.append(items[1])

        invoice_ids_obj = list()
        if deleted_picking_line:
            cr.execute("select invoice_id from  wms_manifest_line where id=%s", (deleted_picking_line))

            invoice_ids_obj = cr.fetchall()
        invoice_ids=[]

        for it in invoice_ids_obj:
            invoice_ids.append(it[0])

        res = super(wms_manifest_process, self).write(cr, uid, ids, vals, context=context)
        if res:
            for inv_id in invoice_ids:
                cr.execute("update account_invoice set assigned=FALSE where id=%s", ([inv_id]))
                cr.commit()

        return res

    def confirm_wms_manifest(self, cr, uid, ids, context=None):



        data = self.browse(cr, uid, ids[0], context=context)

        for items in data.picking_line:
            magento_no = str(items.magento_no)
            query_for_SO = self.pool['sale.order'].search(cr, uid,[('client_order_ref', '=', magento_no)])

            if len(query_for_SO) >0:

                sales = self.pool.get('sale.order').browse(cr, uid, query_for_SO, context=context)[0]

                for so_item in sales:
                    if so_item.state !='cancel':
                        if so_item.delivered_amount == so_item.amount_total:
                            dispatch_wms_query = "UPDATE sale_order SET invoiced_dispatch=TRUE  WHERE id='{1}'".format(uid, so_item.id)
                            cr.execute(dispatch_wms_query)
                            cr.commit()
                        else:
                            dispatch_wms_query = "UPDATE sale_order SET partially_invoiced_dispatch=TRUE  WHERE id='{1}'".format(uid, so_item.id)
                            cr.execute(dispatch_wms_query)
                            cr.commit()




        try:
            for single_id in ids:
                confirm_wms_query = "UPDATE wms_manifest_process SET state='confirm', confirm_by='{0}', confirm_time='{1}' WHERE id='{2}'".format(uid, str(fields.datetime.now()), single_id)
                cr.execute(confirm_wms_query)
                cr.commit()
                confirm_wms_line_query = "UPDATE wms_manifest_line SET status='confirm' WHERE wms_manifest_id='{0}'".format(single_id)
                cr.execute(confirm_wms_line_query)
                cr.commit()
        except:
            pass

        return True

    def cancel_wms_manifest(self, cr, uid, ids, context=None):

        try:
            for single_id in ids:
                cancel_wms_query = "UPDATE wms_manifest_process SET state='cancel', cancel_by='{0}' WHERE id='{1}'".format(uid, single_id)
                cr.execute(cancel_wms_query)
                cr.commit()

                cancel_wms_line_query = "UPDATE wms_manifest_line SET status='cancel' WHERE wms_manifest_id='{0}'".format(
                    single_id)
                cr.execute(cancel_wms_line_query)
                cr.commit()
        except:
            pass

        return True


class wms_manifest_line(osv.osv):
    _name = "wms.manifest.line"
    _description = "Manifest Line"

    def _get_invoice_number(self, cr, uid, ids, field_name, arg, context=None):

        res = {}
        if context is None:
            context = {}

        for val in self.browse(cr, uid, ids, context=context):

            account_invoice = self.pool.get('account.invoice').browse(cr, uid, [val.invoice_id], context=context)

            res[val.id] = str(account_invoice.number)

        return res

    _columns = {
        'wms_manifest_id': fields.many2one('wms.manifest.process', 'Manifest Line ID', required=True, ondelete='cascade', select=True, readonly=True),
        'invoice_id': fields.integer('Invoice ID'),
        'invoice_number': fields.function(_get_invoice_number, string='Invoice No.', type='char'),
        'magento_no': fields.char('Order No'),
        'customer_name': fields.char('Customer Name'),
        'phone': fields.char('Phone'),
        'address': fields.char('Address'),
        'payment_type': fields.char('Payment Type'),
        'amount': fields.float('Amount'),
        'status': fields.char('Status'),
        'area': fields.char('Area'),

    }


class account_invoice(osv.osv):
    _inherit ='account.invoice'

    _columns ={
        'delivery_date':fields.date('Delivery Date'),
        'delivered':fields.boolean('Delivered'),


    }



class wms_manifest_outbound(osv.osv):
    _name = "wms.manifest.outbound"
    _description = "WMS Manifest Process Outbound"

    _columns = {
        'name': fields.char('Order Number', required=True),
        'customer_name': fields.char('Customer Name'),
        'scan': fields.char('Scan'),
        'outbound_line': fields.one2many('wms.manifest.outbound.line', 'wms_manifest_outbound_id', 'WMS Outbound Line', required=True),
        'confirm': fields.boolean('Confirm'),
        'confirm_by': fields.many2one('res.users', 'Confirm By'),
        'cancel': fields.boolean('Cancel'),
        'cancel_by': fields.many2one('res.users', 'Cancel By'),
        'has_return': fields.boolean('Has Return'),
        'delivered': fields.boolean('Delivered'),
        'stock_return_id': fields.integer('Stock Return ID'),
        'invoice_return_id': fields.integer('Invoice Return ID'),
        'delivery_date': fields.date('Delivery Date', required=True),
        'state': fields.selection([
            ('pending', 'Pending'),
            ('confirm', 'Confirmation'),
            ('cancel', 'Cancelled'),

        ], 'Status', readonly=True, copy=False, help="Gives the status of the Mainfest", select=True),
    }

    _order = 'id desc'

    # def receipt_return(self, cr, uid, ids, context=None):
    #
    #     invoice_id=None
    #     stock_picking_name=None
    #     invoice_name = None
    #     return_list = self.pool.get('wms.manifest.outbound').browse(cr, uid, ids, context=context)[0]
    #
    #     if return_list.stock_return_id:
    #         raise osv.except_osv(_('Already Received!'), _('Already Received'))
    #
    #
    #     cr.execute("select invoice_id from  wms_manifest_outbound_line where wms_manifest_outbound_id=%s", (ids))
    #
    #     invoice_ids = cr.fetchall()
    #
    #     new_inv_ids = [items[0] for items in invoice_ids]
    #
    #     if len(new_inv_ids) > 0:
    #         invoice_id = new_inv_ids[0]
    #
    #         cr.execute("select origin,number from  account_invoice where id=%s", ([invoice_id]))
    #         origin = cr.fetchall()
    #
    #         for item in origin:
    #             if item:
    #                 stock_picking_name = item[0]
    #                 invoice_name = item[1]
    #
    #
    #     cr.execute("select id from  stock_picking where name=%s", ([stock_picking_name]))
    #
    #     stock_id_lists = [items[0] for items in cr.fetchall()]
    #     stock_object = self.pool.get('stock.picking').browse(cr, uid, stock_id_lists, context=context)[0]
    #
    #     product_dict ={}
    #     product_return_moves = [[5, False, False]]
    #     for items in return_list.outbound_line:
    #
    #         if items.quantity > items.delivered:
    #             product_dict={}
    #             product_dict['product_id']=items.product_id
    #             product_dict['quantity']=items.quantity - items.delivered
    #             product_dict['lot_id']=False
    #
    #             for st_items in stock_object.move_lines:
    #                 if st_items.product_id.id == items.product_id:
    #                     product_dict['move_id'] = st_items.id
    #                     product_return_moves.append([0, False, product_dict])
    #                     product_dict = {}
    #
    #     prepare_data={'product_return_moves': product_return_moves,
    #      'invoice_state': '2binvoiced'}
    #
    #     return_pool= self.pool['stock.return.picking']
    #     data = prepare_data
    #
    #     context.update({
    #         'active_model': 'stock.picking',
    #         'active_ids': [stock_object.id],
    #         'active_id': stock_object.id,
    #         'refund': True
    #     })
    #
    #     values = prepare_data
    #
    #     created_id = self.pool['stock.return.picking'].create(cr, uid, values, context=context)
    #
    #     return_stock_id=None
    #     return_inv_id=None
    #
    #
    #     id_list =[created_id]
    #     context['active_id'] = stock_object.id
    #     stock_picking_id = return_pool._create_returns(cr, uid, id_list, context=context)
    #     # 2 id is coming
    #
    #     cr.commit()
    #     picking=[]
    #
    #
    #
    #
    #
    #
    #
    #     if stock_picking_id:
    #         return_stock_id = stock_picking_id[0]
    #
    #         ###### Stock Transfer Starts from Here
    #         picking=[stock_picking_id[0]]
    #
    #         context.update({
    #
    #             'active_model': 'stock.picking',
    #             'active_ids': picking,
    #             'active_id': len(picking) and picking[0] or False
    #         })
    #
    #         created_id = self.pool['stock.transfer_details'].create(cr, uid, {
    #             'picking_id': len(picking) and picking[0] or False}, context)
    #
    #         if created_id:
    #             created_id = self.pool.get('stock.transfer_details').browse(cr, uid, created_id, context=context)
    #             if created_id.picking_id.state in ['assigned', 'partially_available']:
    #                 # raise Warning(_('You cannot transfer a picking in state \'%s\'.') % created_id.picking_id.state)
    #
    #                 processed_ids = []
    #                 # Create new and update existing pack operations
    #                 for lstits in [created_id.item_ids, created_id.packop_ids]:
    #                     for prod in lstits:
    #                         pack_datas = {
    #                             'product_id': prod.product_id.id,
    #                             'product_uom_id': prod.product_uom_id.id,
    #                             'product_qty': prod.quantity,
    #                             'package_id': prod.package_id.id,
    #                             'lot_id': prod.lot_id.id,
    #                             'location_id': prod.sourceloc_id.id,
    #                             'location_dest_id': prod.destinationloc_id.id,
    #                             'result_package_id': prod.result_package_id.id,
    #                             'date': prod.date if prod.date else datetime.now(),
    #                             'owner_id': prod.owner_id.id,
    #                         }
    #                         if prod.packop_id:
    #                             prod.packop_id.with_context(no_recompute=True).write(pack_datas)
    #                             processed_ids.append(prod.packop_id.id)
    #                         else:
    #                             pack_datas['picking_id'] = created_id.picking_id.id
    #                             packop_id = created_id.env['stock.pack.operation'].create(pack_datas)
    #                             processed_ids.append(packop_id.id)
    #                 # Delete the others
    #                 packops = created_id.env['stock.pack.operation'].search(
    #                     ['&', ('picking_id', '=', created_id.picking_id.id), '!', ('id', 'in', processed_ids)])
    #                 packops.unlink()
    #
    #                 # Execute the transfer of the picking
    #                 created_id.picking_id.do_transfer()
    #
    #                 ## Stock Transfer End Here
    #
    #
    #                 ### Invoice start Here
    #
    #             if created_id.picking_id.invoice_state != 'invoiced':
    #                 # Now Create the Invoice for this picking
    #                 res=[]
    #                 picking_pool = self.pool.get('stock.picking')
    #                 from datetime import date
    #                 context['date_inv'] = date.today()
    #                 context['inv_type'] = 'in_invoice'
    #                 active_ids = [stock_picking_id[0]]
    #
    #                 stock_invoice_onshipping_data ={
    #                     'journal_id': 1147,
    #                     'journal_type': 'sale_refund',
    #                     'invoice_date': datetime.date.today(),
    #                     'invoice_number': invoice_name
    #                 }
    #
    #
    #                 stock_invoice_onshipping_obj = self.pool.get("stock.invoice.onshipping")
    #                 stock_invoice_onshipping_id = stock_invoice_onshipping_obj.create(cr, uid, stock_invoice_onshipping_data, context=context)
    #
    #                 res = stock_invoice_onshipping_obj.open_invoice(cr, uid, [stock_invoice_onshipping_id], context)
    #
    #                 return_inv_id = res if res else None
    #
    #
    #
    #     # Do Transfer
    #     # (5233, 1)
    #
    #
    #     # Do Invoice
    #
    #     # Both ID should save with foreignkey relation (Picking ID and invoice ID)
    #     # wms.manifest.outbound Table
    #
    #     # stock.return.picking
    #
    #     stock_return_id=return_stock_id
    #     invoice_return_id=return_inv_id
    #
    #
    #     cr.commit()
    #     for wms_id in ids:
    #         cr.execute("update wms_manifest_outbound set invoice_return_id=%s,stock_return_id=%s where id=%s", ([invoice_return_id,stock_return_id,wms_id]))
    #         cr.commit()
    #
    #
    #
    #
    #     ## Dispatch Delivered Flag update
    #
    #     rst_list = self.pool.get('stock.picking').browse(cr, uid, stock_id_lists, context=context)[0]
    #
    #     so_name = rst_list.origin
    #
    #     cr.execute("select id from  sale_order where name=%s", ([so_name]))
    #     abc = cr.fetchall()
    #     sale_ids = [abc[0][0]]
    #
    #     sales = self.pool.get('sale.order').browse(cr, uid, sale_ids, context=context)[0]
    #
    #     if sales.delivered_amount <= sales.invoiced_amount:
    #         cr.execute("update sale_order set delivery_dispatch=TRUE where id=%s", ([sales.id]))
    #
    #     cr.commit()
    #
    #
    #     return True
    #
    #     # return ''


    @api.onchange('scan')
    def invoice_outbound_onchange(self):

        try:
            invoice_id = str(self.scan)

            if invoice_id != 'False':

                invoice_id = int(invoice_id)
                invoice_env = self.env['account.invoice']
                invoice_obj = invoice_env.search([('id', '=', invoice_id)])

                if self.outbound_line:
                    self.outbound_line = None
                    self.scan = ""
                    self.name = ""
                    self.customer_name = ""
                else:
                    if invoice_obj:
                        # invoice_obj = invoice_env.search([('id', '=', invoice_id)])
                        invoice_line_env = self.env['account.invoice.line']
                        invoice_line_obj = invoice_line_env.search([('invoice_id', '=', invoice_id)])
                        outbound_line_list = list()

                        for invoice_line in invoice_line_obj:
                            outbound_line_list.append({
                                'invoice_id': invoice_id,
                                'magento_no': str(invoice_obj.name),
                                'description': str(invoice_line.name),
                                'product_id': int(invoice_line.product_id),
                                'amount': float(invoice_line.price_subtotal),
                                'quantity': float(invoice_line.quantity),
                                'delivered': float(invoice_line.quantity)
                            })

                        self.outbound_line = outbound_line_list
                        self.name = str(invoice_obj.name)
                        self.customer_name = str(invoice_obj.partner_id.name)

                        """
                        self.outbound_line = [{
                            'invoice_id': invoice_id,
                            'magento_no': str(invoice_obj.name),
    
                            'customer_name': str(invoice_obj.partner_id.name),
                            'phone': str(invoice_obj.partner_id.phone),
                            'address': str(invoice_obj.commercial_partner_id.street),
                            'payment_type': str(invoice_obj.comment),
                            'amount': float(invoice_obj.amount_total),
                            'status': str(invoice_obj.state),
                            'area': str(invoice_obj.commercial_partner_id.city)
                        }]
                        """

                        self.scan = ""
                    else:
                        self.scan = ""

        except:
            self.scan = ""

        return "xXxXxXxXxX"


    def create(self, cr, uid, vals, context=None):

        order_number =None


        today_date=datetime.date.today()

        has_return = False
        inv_id=None
        for items in vals.get('outbound_line'):
            if items:
                for items2 in items:
                    if isinstance(items2, dict):
                        inv_id = items2.get('invoice_id')
                        order_number = items2.get('magento_no')
                        if float(items2.get('quantity')) > float(items2.get('delivered')):
                            items2['return_qty'] = str(float(items2.get('quantity')) - float(items2.get('delivered')))
                            has_return=True
                        else:
                            items2['return_qty'] ='0.00'

                        today_date=items2.get('delivery_date')

        vals['has_return']=has_return
        vals['name'] = order_number
        vals['state'] = 'pending'
        today_date = vals.get('delivery_date')


        inv_data = self.pool.get('account.invoice').browse(cr, uid, [inv_id], context=context)
        if inv_data:
            if inv_data.delivered:
                raise osv.except_osv(_('Already Assigned!'), _('This Order Already Delivered'))
        if context is None:
            context = {}


        # create_context = dict(context, alias_model_name='crm.lead', alias_parent_model_name=self._name)
        section_id = super(wms_manifest_outbound, self).create(cr, uid, vals, context=context)
        # if not has_return:
        #     if section_id and inv_id is not None:

        cr.execute("update account_invoice set delivery_date=%s,delivered=TRUE where id=%s", ([today_date, inv_id]))

        cr.commit()

        if order_number is not None:
            cr.execute("select id from  sale_order where client_order_ref=%s", ([str(order_number)]))
            abc = cr.fetchall()
            sale_ids = [abc[0][0]]

            sales = self.pool.get('sale.order').browse(cr, uid, sale_ids, context=context)[0]

            if sales.delivered_amount == sales.amount_total:
                cr.execute("update sale_order set delivery_dispatch=TRUE where id=%s", ([sales.id]))
                cr.commit()
                cr.execute("update sale_order set partially_invoiced_dispatch=FALSE where id=%s", ([sales.id]))
                cr.commit()
            elif sales.delivered_amount < sales.amount_total and sales.delivered_amount > 0:
                cr.execute("update sale_order set partially_invoiced_dispatch=TRUE where id=%s", ([sales.id]))
                cr.commit()


        cr.commit()
        return section_id


class wms_manifest_outbound_line(osv.osv):
    _name = "wms.manifest.outbound.line"
    _description = "Manifest Outbound Line"

    # delivered amount
    def _get_delivered_amount(self, cursor, user, ids, name, arg, context=None):
        res = False
        res = {}

        for manifest_line in self.browse(cursor, user, ids, context=context):

            total_sum = ''
            inv_id=manifest_line.invoice_id
            cursor.execute("select name from  account_invoice where id=%s", ([inv_id]))

            invoice_ids_obj = cursor.fetchall()
            if len(invoice_ids_obj)>0:
                total_sum=invoice_ids_obj[0]

            res[manifest_line.id] = total_sum
        return res

    _columns = {
        'wms_manifest_outbound_id': fields.many2one('wms.manifest.outbound', 'Manifest Outbound Line ID', required=True, ondelete='cascade', select=True, readonly=True),
        'invoice_id': fields.integer('Invoice ID', readonly=True),
        'magento_no': fields.char('Order No', readonly=True),
        'description': fields.char('Description', readonly=True),
        'product_id': fields.integer('Product ID', readonly=True),
        'amount': fields.float('Amount', readonly=True),
        'quantity': fields.float('Quantity', readonly=True),
        'delivered': fields.float('Delivered'),
        'return_qty': fields.float('Return',readonly=True)
    }





{
    'name': 'Re-calculate sales order amount',
    'version': '8.0',
    'author': "Mufti Muntasir and Rocky",
    'category': 'Sale',
    'summary': 'Re-calculate sales order amount',
    'depends': ['base', 'odoo_magento_connect', 'sale'],
    'data': [

        'security/re_calculate_so_amount_security.xml',
        're_calculate_so_amount_view.xml',

    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}

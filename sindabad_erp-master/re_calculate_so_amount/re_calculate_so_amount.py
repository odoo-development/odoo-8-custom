from datetime import datetime, timedelta
import time
from openerp import SUPERUSER_ID
from openerp.osv import fields, osv
from openerp.tools.translate import _
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT
import openerp.addons.decimal_precision as dp
from openerp import workflow
import datetime


class sale_order(osv.osv):
    _inherit = "sale.order"


    def action_re_calculate_so_amount(self, cr, uid, ids, context=None):
        so_obj = self.pool.get("sale.order")
        so = so_obj.browse(cr, uid, ids, context=context)
        so_amount = so.amount_total
        magento_amount = so.x_sin_total_magento
        gap_amount =0


        p_found = False

        already_adjusted_amount=0
        line_id=0



        gap_amount = round((magento_amount - so_amount),2)


        for so_items in so.order_line:
            if so_items.product_id.id == 20044:
                p_found=True
                already_adjusted_amount=so_items.price_unit
                line_id = so_items.id
                break



        if p_found == False and gap_amount !=0:
            vals= {'order_line':
    [
     [0, False, {'property_ids': [[6, False, []]], 'product_uos_qty': 1, 'product_id': 20044, 'product_uom': 1, 'discount': 0, 'event_id': False, 'price_unit': gap_amount, 'product_uom_qty': 1, 'name': 'Sales Discount', 'product_tmpl_id': 9341, 'route_id': False, 'delay': 7, 'event_type_id': False, 'address_allotment_id': False, 'th_weight': 0, 'product_uos': False, 'tax_id': [[6, False, []]], 'event_ok': False, 'event_ticket_id': False, 'product_packaging': False}]

    ]
    }
            record = super(sale_order, self).write(cr, uid, ids, vals, context=context)
        elif p_found == True and gap_amount !=0 and already_adjusted_amount != gap_amount:

            vals = {'order_line': [ [1, line_id, {'price_unit': gap_amount}]]}
            record = super(sale_order, self).write(cr, uid, ids, vals, context=context)

        return True




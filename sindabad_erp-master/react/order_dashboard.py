# -*- coding: utf-8 -*-
##########################################################################
#
#    Copyright (c) 2018-Present Zero Gravity ventures Limited (<https://webkul.com/>)
#
##########################################################################

from datetime import datetime, timedelta
from openerp.osv import fields, osv
from openerp.tools.translate import _
from openerp import SUPERUSER_ID, api
import openerp.addons.decimal_precision as dp
import logging
import sys

reload(sys)
sys.setdefaultencoding('utf8')

XMLRPC_API = '/index.php/api/xmlrpc'


class product_category(osv.osv):
    _inherit = "product.category"

    @api.multi
    def root_name_get(self):
        def get_root_names(cat):
            """ Return the list [cat.name, cat.parent_id.name, ...] """
            res = ''
            while cat:
                res = cat.name
                cat = cat.parent_id
            return res

        return [(cat.id, get_root_names(cat)) for cat in self]

    def _root_name_get_fnc(self, cr, uid, ids, prop, unknow_none, context=None):
        res = self.root_name_get(cr, uid, ids, context=context)
        return dict(res)

    @api.multi
    def parent_id_get(self):
        def get_ids(cat):
            """ Return the list [cat.name, cat.parent_id.name, ...] """
            res = 0
            while cat:
                res = cat.id
                cat = cat.parent_id
            return res

        return [(cat.id, get_ids(cat)) for cat in self]

    def _parent_id_get_fnc(self, cr, uid, ids, prop, unknow_none, context=None):
        res = self.parent_id_get(cr, uid, ids, context=context)
        return dict(res)

    _columns = {
        'root_cat_id': fields.function(_parent_id_get_fnc, type="char", readonly=True, string='Root Category ID'),
        'root_cat_name': fields.function(_root_name_get_fnc, type="char", readonly=True, string='Root Category Name'),
    }


class dashboard_order(osv.osv):
    _inherit = "sale.order"

    def afterWhere(self, cr, uid, params, method_name):
        sales_order_poll = self.pool.get('sale.order')

        afterWhere = ' '
        ninety_days = datetime.now() + timedelta(days=-90)
        todays = datetime.now()

        if params.has_key('partner'):
            partner = tuple(params.get('partner'))
            if len(partner) > 0:
                # if params.has_key('date_order'):
                sales_order = sales_order_poll.search(
                    cr, uid, [('partner_id', 'in', partner)])
                # else:
                #    sales_order = sales_order_poll.search(
                #        cr, uid, [('partner_id', 'in', partner), ('date_order', '&lt;=', datetime.datetime.combine(context_today(), datetime.time(23, 59, 59))), ('date_order', '&gt;=', datetime.datetime.combine(datetime.date.today() - datetime.timedelta(days=90), datetime.time(0, 0, 0)))])
                if len(sales_order) > 1:
                    afterWhere = afterWhere + ' AND so.id IN ' + str(tuple(sales_order))
                elif not len(sales_order) is 0:
                    sales_order = sales_order_poll.search(
                        cr, uid, [('partner_id', '=', partner[0])])
                    afterWhere = afterWhere + ' AND so.id = ' + str(sales_order[0])

        if params.has_key('concern'):
            concern = tuple(params.get('concern'))
            if len(concern) > 0:
                if len(concern) > 1:
                    afterWherePlus = ' AND partner.id IN ' + str(concern)
                    afterWhere = afterWhere + afterWherePlus
                elif not len(concern) is 0:
                    afterWhere = afterWhere + ' AND partner.id = ' + str(concern[0])

        if params.has_key('category'):
            category = tuple(params.get('category'))
            if len(category) > 0:
                if len(category) > 1:
                    afterWherePlus = ' AND category.root_id IN ' + str(category)
                    afterWhere = afterWhere + afterWherePlus
                elif not len(category) is 0:
                    afterWhere = afterWhere + ' AND category.root_id = ' + str(category[0])

        if params.has_key('order_aging') and params.get('order_aging') and len(params.get('order_aging')) > 0:
            order_aging = params.get('order_aging')
            if order_aging != None:
                afterWherePlus = ' AND so.date_confirm >= \'' + order_aging + '\''
                afterWhere = afterWhere + afterWherePlus

        if params.has_key('date_order'):
            date_order = params.get('date_order')

            if date_order.has_key('start_date_order') and date_order.get('start_date_order'):

                afterWhere = afterWhere + ' AND date_order >= \'' + date_order.get('start_date_order') + '\''
            else:
                ninety_days_string = ninety_days.strftime('%Y-%m-%d')
                afterWhere = afterWhere + ' AND date_order >= \'' + ninety_days_string + '\''

            if date_order.has_key('end_date_order') and date_order.get('end_date_order'):
                afterWhere = afterWhere + ' AND date_order <= \'' + date_order.get('end_date_order') + '\''
            else:
                todays_string = todays.strftime('%Y-%m-%d')
                afterWhere = afterWhere + ' AND date_order <= \'' + todays_string + '\''
        else:
            afterWhere = afterWhere + ' AND date_order >= %s AND date_order <= ' % (
            ninety_days.strftime('%Y-%m-%d'), todays.strftime('%Y-%m-%d'))

        if params.has_key('shipment_date'):
            shipment_date = params.get('shipment_date')
            # if (shipment_date.has_key('start_shipment_date') and shipment_date.get('start_shipment_date')) and (shipment_date.has_key('end_shipment_date') and shipment_date.get('end_shipment_date')):
            if shipment_date.has_key('start_shipment_date') and shipment_date.get('start_shipment_date'):
                afterWhere = afterWhere + ' AND so.message_last_post >= \'' + shipment_date.get(
                    'start_shipment_date') + '\''

            if shipment_date.has_key('end_shipment_date') and shipment_date.get('end_shipment_date'):
                afterWhere = afterWhere + ' AND so.message_last_post <= \'' + shipment_date.get(
                    'end_shipment_date') + '\''

            # --------------------------------------
            # 'fqd' = Full Quantity Delivery
            # 'pqd' = Partial Quantity Delivery
            # 'pd'  = Pending Delivery
            # --------------------------------------

        # if not method_name == 'return_buyer':
        #    afterWhere + ' AND sol.qty_delivered != 0'

        if method_name == 'line_item_snapshot':
            status = {
                'common': '',
                'done': '',
                'draft': '',
                'partial': '',
            }

            status['common'] = afterWhere + ' AND sol.price_unit != 0'

            if params.has_key('delivery_status'):
                delivery_status = params.get('delivery_status')
                if 'done' in delivery_status:
                    status['done'] = " AND so.state = 'done' AND sol.qty_delivered != 0 "
                if 'draft' in delivery_status:
                    status['draft'] = " AND so.state = 'draft' "
                if 'partial' in delivery_status:
                    status['partial'] = " AND so.partial_delivered = TRUE  AND sol.qty_delivered != 0 "
                return status
            else:
                status['done'] = " AND so.state = 'done' AND sol.qty_delivered != 0 "
                status['draft'] = " AND so.state = 'draft' "
                status['partial'] = " AND so.partial_delivered = TRUE  AND sol.qty_delivered != 0 "
                return status

        else:

            if params.has_key('delivery_status'):
                delivery_status = params.get('delivery_status')
                # if 'partial' in delivery_status:
                #   afterWhere = afterWhere + " AND so.partial_delivered = TRUE"
                #   delivery_status.remove('partial')

                if len(delivery_status) > 1:
                    afterWhere = afterWhere + ' AND so.state IN ' + str(tuple(delivery_status))
                elif not len(delivery_status) is 0:
                    afterWhere = afterWhere + " AND so.state = '%s' " % (delivery_status[0])
                elif len(delivery_status) is 0:
                    afterWhere = afterWhere + " AND so.state IN ('done', 'draft', 'partial')"
            afterWhere = afterWhere + ' AND sol.price_unit != 0 '

            return afterWhere

    def top_twenty_buyer(self, cr, uid, params, fields, offset, limit=None):

        logicBoard = self.afterWhere(cr, uid, params, 'top_twenty_buyer')

        query = 'SELECT DISTINCT SUM(sol.price_unit*sol.qty_delivered) AS Total_Buy,\
                               partner.name\
                              FROM \
                                 sale_order_line sol\
                              INNER JOIN \
                                 sale_order so ON so.id = sol.order_id \
                              INNER JOIN \
                                 product_product product ON product.id = sol.product_id \
                              INNER JOIN \
                                 product_template template ON template.id = product.product_tmpl_id \
                              INNER JOIN \
                                (WITH RECURSIVE category(id, parent_id, root_id) AS (\
                              SELECT id, parent_id, id AS root_id\
                              FROM product_category WHERE parent_id IS NULL\
                              UNION ALL\
                              SELECT t2.id, t2.parent_id, category.root_id\
                              FROM product_category t2, category WHERE t2.parent_id = category.id\
                              )\
                              SELECT (SELECT name FROM product_category WHERE id= category.root_id) as pname, id, root_id\
                              FROM category ) as category ON category.id = template.categ_id \
                              INNER JOIN \
                                 res_partner partner\
                              ON \
                                 partner.id = so.partner_id\
                              WHERE \
                                 sol.price_unit >0\
                                 %s \
                              GROUP BY \
                                 partner.name\
                              ORDER BY\
                                          Total_Buy DESC\
                              LIMIT %s' % (logicBoard, limit)
        cr.execute(query)
        result = {}
        key = 0
        for price, name in cr.fetchall():
            result[str(name)] = price
            key = key + 1
        # result = dict(cr.fetchall())
        return result

    def top_ten_sku_by_value(self, cr, uid, params, fields, offset, limit=None):

        logicBoard = self.afterWhere(cr, uid, params, 'top_ten_sku_by_value')
        limit = 10

        query = 'SELECT DISTINCT SUM(sol.price_unit*sol.qty_delivered) AS Total_Buy,\
                               product.name_template\
                              FROM \
                                 sale_order_line sol\
                              INNER JOIN \
                                 sale_order so ON so.id = sol.order_id \
                              INNER JOIN \
                                 product_product product ON product.id = sol.product_id \
                              INNER JOIN \
                                 product_template template ON template.id = product.product_tmpl_id \
                              INNER JOIN \
                                (WITH RECURSIVE category(id, parent_id, root_id) AS (\
                              SELECT id, parent_id, id AS root_id\
                              FROM product_category WHERE parent_id IS NULL\
                              UNION ALL\
                              SELECT t2.id, t2.parent_id, category.root_id\
                              FROM product_category t2, category WHERE t2.parent_id = category.id\
                              )\
                              SELECT (SELECT name FROM product_category WHERE id= category.root_id) as pname, id, root_id\
                              FROM category ) as category ON category.id = template.categ_id \
                              INNER JOIN \
                                 res_partner partner\
                              ON \
                                 partner.id = so.partner_id\
                              WHERE \
                                 sol.price_unit*sol.qty_delivered >0 \
                                 %s \
                              GROUP BY \
                                 product.name_template\
                              ORDER BY\
                                          Total_Buy DESC\
                              LIMIT %s' % (logicBoard, limit)
        cr.execute(query)
        result = {}
        key = 0
        for price, sku in cr.fetchall():
            result[str(sku)] = price
            key = key + 1
        # result = dict(cr.fetchall())
        return result

    def top_ten_sku_by_qty(self, cr, uid, params, fields, offset, limit=None):

        logicBoard = self.afterWhere(cr, uid, params, 'top_ten_sku_by_qty')
        limit = 10

        query = 'SELECT DISTINCT SUM(sol.qty_delivered) AS total_qty,\
                               product.name_template\
                              FROM \
                                 sale_order_line sol\
                              INNER JOIN \
                                 sale_order so ON so.id = sol.order_id \
                              INNER JOIN \
                                 product_product product ON product.id = sol.product_id \
                              INNER JOIN \
                                 product_template template ON template.id = product.product_tmpl_id \
                              INNER JOIN \
                                (WITH RECURSIVE category(id, parent_id, root_id) AS (\
                              SELECT id, parent_id, id AS root_id\
                              FROM product_category WHERE parent_id IS NULL\
                              UNION ALL\
                              SELECT t2.id, t2.parent_id, category.root_id\
                              FROM product_category t2, category WHERE t2.parent_id = category.id\
                              )\
                              SELECT (SELECT name FROM product_category WHERE id= category.root_id) as pname, id, root_id\
                              FROM category ) as category ON category.id = template.categ_id \
                              INNER JOIN \
                                 res_partner partner\
                              ON \
                                 partner.id = so.partner_id\
                              WHERE \
                                 sol.price_unit >0 AND sol.qty_delivered > 0 \
                                 %s \
                              GROUP BY \
                                 product.name_template\
                              ORDER BY\
                                          total_qty DESC\
                              LIMIT %s' % (logicBoard, limit)
        cr.execute(query)
        result = {}
        key = 0
        for qty, sku in cr.fetchall():
            result[str(sku)] = qty
            key = key + 1
        # result = dict(cr.fetchall())
        return result

    def return_buyer(self, cr, uid, params, fields, offset, limit=None):

        logicBoard = self.afterWhere(cr, uid, params, 'return_buyer')

        query = 'SELECT DISTINCT SUM(sol.price_unit*sol.qty_return) AS total_return_value,\
                               partner.name\
                              FROM \
                                 sale_order_line sol\
                              INNER JOIN \
                                 sale_order so ON so.id = sol.order_id \
                              INNER JOIN \
                                 product_product product ON product.id = sol.product_id \
                              INNER JOIN \
                                 product_template template ON template.id = product.product_tmpl_id \
                              INNER JOIN \
                                (WITH RECURSIVE category(id, parent_id, root_id) AS (\
                              SELECT id, parent_id, id AS root_id\
                              FROM product_category WHERE parent_id IS NULL\
                              UNION ALL\
                              SELECT t2.id, t2.parent_id, category.root_id\
                              FROM product_category t2, category WHERE t2.parent_id = category.id\
                              )\
                              SELECT (SELECT name FROM product_category WHERE id= category.root_id) as pname, id, root_id\
                              FROM category ) as category ON category.id = template.categ_id \
                              INNER JOIN \
                                 res_partner partner\
                              ON \
                                 partner.id = so.partner_id\
                              WHERE \
                                 partner.is_company=TRUE AND sol.qty_return != 0\
                                 %s \
                              GROUP BY \
                                 partner.name\
                              ORDER BY\
                                          total_return_value DESC' % (logicBoard)
        cr.execute(query)
        result = {}
        key = 0
        for price, name in cr.fetchall():
            result[str(name)] = price
            key = key + 1
        # result = dict(cr.fetchall())
        return result

    def line_item_snapshot(self, cr, uid, params, fields, offset, limit=None):

        # ---
        # Pending delivery
        # Partial qty selivery
        # Full Qty Delivery
        # ---

        result = {}
        logicBoard = self.afterWhere(cr, uid, params, 'line_item_snapshot')

        if 'done' in logicBoard and logicBoard.get('done'):

            cr.execute("SELECT DISTINCT \
                        SUM(sol.qty_delivered) AS total_qty, \
                        SUM(sol.price_unit*sol.qty_delivered) AS value \
                     FROM \
                        sale_order_line sol\
                     INNER JOIN \
                        sale_order so ON so.id = sol.order_id \
                     INNER JOIN \
                        product_product product ON product.id = sol.product_id \
                     INNER JOIN \
                        product_template template ON template.id = product.product_tmpl_id \
                     INNER JOIN \
                       (WITH RECURSIVE category(id, parent_id, root_id) AS (\
                     SELECT id, parent_id, id AS root_id\
                     FROM product_category WHERE parent_id IS NULL\
                     UNION ALL\
                     SELECT t2.id, t2.parent_id, category.root_id\
                     FROM product_category t2, category WHERE t2.parent_id = category.id\
                     )\
                     SELECT (SELECT name FROM product_category WHERE id= category.root_id) as pname, id, root_id\
                     FROM category ) as category ON category.id = template.categ_id \
                     INNER JOIN \
                        res_users r_user ON r_user.id = sol.salesman_id \
                     INNER JOIN \
                        res_partner partner ON partner.id = r_user.partner_id\
                     WHERE \
                        so.amount_total > 1 \
                        %s %s " % (logicBoard['common'], logicBoard['done']))

            done = cr.fetchall()
            key = 0
            if done[0][0] == 0:
                result['done'] = 0
                result['done_value'] = 0
            else:
                result['done'] = done[0][0]
                result['done_value'] = done[0][1]

        if 'draft' in logicBoard and logicBoard.get('draft'):

            cr.execute("SELECT DISTINCT \
                        SUM(sol.product_uom_qty) AS total_qty, \
                        SUM(sol.price_unit*sol.product_uom_qty) AS value \
                     FROM \
                        sale_order_line sol\
                     INNER JOIN \
                        sale_order so ON so.id = sol.order_id \
                     INNER JOIN \
                        product_product product ON product.id = sol.product_id \
                     INNER JOIN \
                        product_template template ON template.id = product.product_tmpl_id \
                     INNER JOIN \
                       (WITH RECURSIVE category(id, parent_id, root_id) AS (\
                     SELECT id, parent_id, id AS root_id\
                     FROM product_category WHERE parent_id IS NULL\
                     UNION ALL\
                     SELECT t2.id, t2.parent_id, category.root_id\
                     FROM product_category t2, category WHERE t2.parent_id = category.id\
                     )\
                     SELECT (SELECT name FROM product_category WHERE id= category.root_id) as pname, id, root_id\
                     FROM category ) as category ON category.id = template.categ_id \
                     INNER JOIN \
                        res_users r_user ON r_user.id = sol.salesman_id \
                     INNER JOIN \
                              res_partner partner\
                           ON \
                              partner.id = r_user.partner_id\
                     WHERE \
                        so.amount_total > 1 \
                        %s %s " % (logicBoard['common'], logicBoard['draft']))

            draft = cr.fetchall()
            if draft[0][0] == 0:
                result['draft'] = 0
                result['draft_value'] = 0
            else:
                result['draft'] = draft[0][0]
                result['draft_value'] = draft[0][1]

        if 'partial' in logicBoard and logicBoard.get('partial'):

            cr.execute("SELECT DISTINCT\
                      SUM(sol.qty_delivered) AS total_qty, \
                      SUM(sol.price_unit*sol.qty_delivered) AS value \
                     FROM \
                        sale_order_line sol\
                     INNER JOIN \
                        sale_order so ON so.id = sol.order_id \
                     INNER JOIN \
                        product_product product ON product.id = sol.product_id \
                     INNER JOIN \
                        product_template template ON template.id = product.product_tmpl_id \
                     INNER JOIN \
                       (WITH RECURSIVE category(id, parent_id, root_id) AS (\
                     SELECT id, parent_id, id AS root_id\
                     FROM product_category WHERE parent_id IS NULL\
                     UNION ALL\
                     SELECT t2.id, t2.parent_id, category.root_id\
                     FROM product_category t2, category WHERE t2.parent_id = category.id\
                     )\
                     SELECT (SELECT name FROM product_category WHERE id= category.root_id) as pname, id, root_id\
                     FROM category ) as category ON category.id = template.categ_id \
                     INNER JOIN \
                        res_users r_user \
                     ON \
                        r_user.id = sol.salesman_id \
                     INNER JOIN \
                              res_partner partner\
                           ON \
                              partner.id = r_user.partner_id\
                     WHERE \
                        so.amount_total > 1 \
                        %s %s " % (logicBoard['common'], logicBoard['partial']))

            partial = cr.fetchall()
            key = 0
            if partial[0][0] == 0:
                result['partial'] = 0
                result['partial_value'] = 0
            else:
                result['partial'] = partial[0][0]
                result['partial_value'] = partial[0][1]

        cr.execute("SELECT DISTINCT\
                      SUM(sol.price_unit*sol.qty_delivered) AS value \
                  FROM \
                     sale_order_line sol\
                  INNER JOIN \
                     sale_order so ON so.id = sol.order_id \
                  INNER JOIN \
                     product_product product ON product.id = sol.product_id \
                  INNER JOIN \
                     product_template template ON template.id = product.product_tmpl_id \
                  INNER JOIN \
                    (WITH RECURSIVE category(id, parent_id, root_id) AS (\
                  SELECT id, parent_id, id AS root_id\
                  FROM product_category WHERE parent_id IS NULL\
                  UNION ALL\
                  SELECT t2.id, t2.parent_id, category.root_id\
                  FROM product_category t2, category WHERE t2.parent_id = category.id\
                  )\
                  SELECT (SELECT name FROM product_category WHERE id= category.root_id) as pname, id, root_id\
                  FROM category ) as category ON category.id = template.categ_id \
                  INNER JOIN \
                     res_users r_user \
                  ON \
                     r_user.id = sol.salesman_id \
                  INNER JOIN \
                           res_partner partner\
                        ON \
                           partner.id = r_user.partner_id\
                  WHERE \
                     so.amount_total > 1 AND so.partial_delivered = TRUE AND so.state = 'done'\
                     %s " % (logicBoard['common'])
                   )

        partially_closed_value = cr.fetchall()
        if partially_closed_value[0][0] == 0 or partially_closed_value[0][0] == None:
            result['partially_closed_value'] = 0
        else:
            result['partially_closed_value'] = partially_closed_value[0][0]

        cr.execute("SELECT DISTINCT\
                      SUM(sol.price_unit*sol.qty_return) AS value \
                  FROM \
                     sale_order_line sol\
                  INNER JOIN \
                     sale_order so ON so.id = sol.order_id \
                  INNER JOIN \
                     product_product product ON product.id = sol.product_id \
                  INNER JOIN \
                     product_template template ON template.id = product.product_tmpl_id \
                  INNER JOIN \
                    (WITH RECURSIVE category(id, parent_id, root_id) AS (\
                  SELECT id, parent_id, id AS root_id\
                  FROM product_category WHERE parent_id IS NULL\
                  UNION ALL\
                  SELECT t2.id, t2.parent_id, category.root_id\
                  FROM product_category t2, category WHERE t2.parent_id = category.id\
                  )\
                  SELECT (SELECT name FROM product_category WHERE id= category.root_id) as pname, id, root_id\
                  FROM category ) as category ON category.id = template.categ_id \
                  INNER JOIN \
                     res_users r_user \
                  ON \
                     r_user.id = sol.salesman_id \
                  INNER JOIN \
                           res_partner partner\
                        ON \
                           partner.id = r_user.partner_id\
                  WHERE \
                     so.amount_total > 1 \
                     %s " % (logicBoard['common'])
                   )

        return_value = cr.fetchall()
        if return_value[0][0] == 0 or return_value[0][0] == None:
            result['return_value'] = 0
        else:
            result['return_value'] = return_value[0][0]
        return result

    def line_item_by_category(self, cr, uid, params, fields, offset, limit=None):
        # ---
        # Concern
        # Price
        # Quantity
        # ---

        logicBoard = self.afterWhere(cr, uid, params, 'line_item_by_category')

        result = {}

        mainQuery = "SELECT DISTINCT\
                        SUM(sol.price_unit*sol.qty_delivered) AS total_buy,\
                        (Select SUM(Amount_Sales),\
                        From\
                        SalesTable\
                        Where\
                        SaleDate >= '01/01/2017'\
                        and SaleDate <= '12/31/2017')\
                        category.pname\
                  FROM \
                     sale_order_line sol\
                  INNER JOIN \
                     sale_order so ON so.id = sol.order_id \
                  INNER JOIN \
                     product_product product ON product.id = sol.product_id \
                  INNER JOIN \
                     product_template template ON template.id = product.product_tmpl_id \
                  INNER JOIN \
                    (WITH RECURSIVE category(id, parent_id, root_id) AS (\
                  SELECT id, parent_id, id AS root_id\
                  FROM product_category WHERE parent_id IS NULL\
                  UNION ALL\
                  SELECT t2.id, t2.parent_id, category.root_id\
                  FROM product_category t2, category WHERE t2.parent_id = category.id\
                  )\
                  SELECT (SELECT name FROM product_category WHERE id= category.root_id) as pname, id, root_id\
                  FROM category ) as category ON category.id = template.categ_id \
                  INNER JOIN \
                     res_users r_user ON r_user.id = sol.salesman_id \
                  INNER JOIN \
                     res_partner partner ON partner.id = r_user.partner_id\
                  WHERE \
                     sol.qty_delivered > 0 \
                     %s \
                  GROUP BY \
                     category.pname\
                     " % (logicBoard)
        cr.execute(mainQuery)
        result = cr.fetchall()

        return result

    def concern_list(self, cr, uid, params, fields, offset, limit=None):
        # # ---
        # # Concern
        # # Price
        # # Quantity
        # # ---

        result = {}

        cr.execute("SELECT DISTINCT\
                  partner.id,\
                  partner.name\
            FROM \
               sale_order_line sol\
            INNER JOIN \
               sale_order so ON so.id = sol.order_id \
            INNER JOIN \
               product_product product ON product.id = sol.product_id \
            INNER JOIN \
               product_template template ON template.id = product.product_tmpl_id \
            INNER JOIN \
               product_category category ON category.id = template.categ_id \
            INNER JOIN \
               res_users r_user ON r_user.id = sol.salesman_id \
            INNER JOIN \
               res_partner partner ON partner.id = r_user.partner_id\
            GROUP BY \
               partner.name, partner.id\
               ")

        result = cr.fetchall()

        return result

    def category_list(self, cr, uid, params, fields, offset, limit=None):
        # # ---
        # # Concern
        # # Price
        # # Quantity
        # # ---

        result = {}

        cr.execute("SELECT DISTINCT\
               category.root_id,\
               category.pname\
            FROM \
               sale_order_line sol\
            INNER JOIN \
               sale_order so\
            ON \
               so.id = sol.order_id \
            INNER JOIN \
               product_product product ON product.id = sol.product_id \
            INNER JOIN \
               product_template template ON template.id = product.product_tmpl_id \
            INNER JOIN \
              (WITH RECURSIVE category(id, parent_id, root_id) AS (\
            SELECT id, parent_id, id AS root_id\
            FROM product_category WHERE parent_id IS NULL\
            UNION ALL\
            SELECT t2.id, t2.parent_id, category.root_id\
            FROM product_category t2, category WHERE t2.parent_id = category.id\
            )\
         SELECT (SELECT name FROM product_category WHERE id= category.root_id) as pname, id, root_id\
         FROM category ) as category ON category.id = template.categ_id \
            GROUP BY \
               category.root_id,\
               category.pname\
               ")

        result = cr.fetchall()

        return result

    def company_list(self, cr, uid, params, fields, offset, limit=None):

        cr.execute('SELECT DISTINCT \
                                 partner.id,\
                                 partner.name\
                              FROM \
                                 sale_order so\
                              INNER JOIN \
                                 res_partner partner ON partner.id = so.partner_id\
                              WHERE \
                                 partner.is_company=TRUE\
                              GROUP BY \
                                 partner.id, partner.name')
        result = cr.fetchall()

        return result

    def so_tat(self, cr, uid, params, fields, offset, limit=None):
        # ---
        # SO TAT = Sale Order Turn Around Time
        # Price
        # Quantity
        # ---

        logicBoard = self.afterWhere(cr, uid, params, 'so_tat')

        result = {}

        mainQuery = "SELECT DISTINCT \
                        COUNT(DISTINCT so.id) AS total_order, \
                        (DATE (sp.date_done) - DATE (so.date_confirm)) AS days\
                  FROM\
                     sale_order so\
                  INNER JOIN \
                     sale_order_line sol ON so.id = sol.order_id \
                   INNER JOIN \
                     stock_picking sp ON sp.origin = so.name \
                  INNER JOIN \
                     product_product product ON product.id = sol.product_id \
                  INNER JOIN \
                     product_template template ON template.id = product.product_tmpl_id \
                  INNER JOIN \
                    (WITH RECURSIVE category(id, parent_id, root_id) AS ( \
                  SELECT id, parent_id, id AS root_id \
                  FROM product_category WHERE parent_id IS NULL \
                  UNION ALL \
                  SELECT t2.id, t2.parent_id, category.root_id \
                  FROM product_category t2, category WHERE t2.parent_id = category.id \
                  ) \
                  SELECT (SELECT name FROM product_category WHERE id= category.root_id) as pname, id, root_id \
                  FROM category ) as category ON category.id = template.categ_id \
                  INNER JOIN \
                     res_users r_user ON r_user.id = sol.salesman_id \
                  INNER JOIN \
                     res_partner partner ON partner.id = r_user.partner_id \
                  WHERE \
                     so.amount_total > 1 %s \
                  GROUP BY \
                     days \
                  ORDER BY \
                  days \
                     " % (logicBoard)
        print 'mainQuery=', mainQuery
        cr.execute(mainQuery)
        result = {}
        key = 0
        for order, day in cr.fetchall():
            result[str(day)] = order
            key = key + 1
        # result = dict(cr.fetchall())
        return result

    def po_tat(self, cr, uid, params, fields, offset, limit=None):
        # ---
        # SO TAT = Sale Order Turn Around Time
        # Price
        # Quantity
        # ---

        logicBoard = self.afterWhere(cr, uid, params, 'so_tat')

        result = {}

        mainQuery = "SELECT DISTINCT\
                        COUNT(DISTINCT po.id) AS total_order, \
                        (DATE (sp.date_done) - DATE (po.date_order)) AS days\
                  FROM\
                     purchase_order po\
                  INNER JOIN \
                     purchase_order_line pol ON po.id = pol.order_id \
                   INNER JOIN \
                     stock_picking sp ON sp.origin = po.name \
                  INNER JOIN \
                     product_product product ON product.id = pol.product_id \
                  INNER JOIN \
                     product_template template ON template.id = product.product_tmpl_id \
                  INNER JOIN \
                    (WITH RECURSIVE category(id, parent_id, root_id) AS ( \
                  SELECT id, parent_id, id AS root_id \
                  FROM product_category WHERE parent_id IS NULL \
                  UNION ALL \
                  SELECT t2.id, t2.parent_id, category.root_id \
                  FROM product_category t2, category WHERE t2.parent_id = category.id \
                  ) \
                  SELECT (SELECT name FROM product_category WHERE id= category.root_id) as pname, id, root_id \
                  FROM category ) as category ON category.id = template.categ_id \
                  WHERE \
                     po.amount_total > 1 \
                  GROUP BY \
                     days\
                  ORDER BY \
                     days"
        cr.execute(mainQuery)
        result = {}
        key = 0
        for order, day in cr.fetchall():
            result[str(day)] = order
            key = key + 1
        # result = dict(cr.fetchall())
        return result

    def wms_info(self, cr, uid, params, fields, offset, limit=None):
      print '----------------WMS-INFO-------------', '*' * 15

      if params.has_key('day'):
         # print 'concern is exist in params'
         d = tuple(params.get('day'))
      else:
         d = 30

      d_days_ago = datetime.now() - timedelta(days=d)
      today_date = datetime.today().date()

      # --- Start Order Flow Count and mapping -----
      order_open_count = self.pool.get('sale.order').search_count(cr, uid,
         [('state', '=', 'draft'), ('date_order', '>=', d_days_ago.strftime('%Y-%m-%d 00:00:00'))], context=None)


      # --- Start Picking Count and mapping ..........
      picking_pending_count = self.pool.get('picking.list').search_count(cr, uid,
         [('state', '=', 'pending'), ('date_confirm', '>=', d_days_ago.strftime('%Y-%m-%d 00:00:00'))], context=None)
      picking_done_count = self.pool.get('picking.list').search_count(cr, uid,
         [('state', '=', 'done'), ('date_confirm', '>=', d_days_ago.strftime('%Y-%m-%d 00:00:00'))], context=None)
      picking_cancel_count = self.pool.get('picking.list').search_count(cr, uid,
         [('state', '=', 'cancel')], context=None)

      # --- Start Packing Count and mapping ..........
      packing_pending_count = self.pool.get('packing.list').search_count(cr, uid,
         [('state', '=', 'pending'), ('date_confirm', '>=', d_days_ago.strftime('%Y-%m-%d 00:00:00'))], context=None)
      packing_done_count = self.pool.get('packing.list').search_count(cr, uid,
         [('state', '=', 'done'), ('date_confirm', '>=', d_days_ago.strftime('%Y-%m-%d 00:00:00'))], context=None)
      packing_done_today_count = self.pool.get('packing.list').search_count(cr, uid,
         [('state', '=', 'done'), ('date_confirm', '>=', today_date.strftime('%Y-%m-%d 00:00:00'))], context=None)

      # --- Start Putaway Count and mapping ..........
      putaway_pending_count = self.pool.get('putaway.process').search_count(cr, uid,
         [('state', '=', 'pending'), ('confirmation_date', '>=', d_days_ago.strftime('%Y-%m-%d 00:00:00'))], context=None)
      putaway_done_count = self.pool.get('putaway.process').search_count(cr, uid,
         [('state', '=', 'confirmed'), ('confirmation_date', '>=', d_days_ago.strftime('%Y-%m-%d 00:00:00'))], context=None)


      # ------- Purchase -------------

      purchase_confirmation_count = self.pool.get('purchase.order').search_count(cr, uid,
         [('state', '=', 'approved'), ('shipped', '=', False),
          ('date_order', '>=', d_days_ago.strftime('%Y-%m-%d 00:00:00'))], context=None)

      purchase_created_count = self.pool.get('purchase.order').search_count(cr, uid,
         [('state', '=', 'draft'), ('date_order', '>=', d_days_ago.strftime('%Y-%m-%d 00:00:00'))], context=None)

      purchase_created_more_then_month_count = self.pool.get('purchase.order').search_count(cr, uid,
         [('state', '=', 'draft'), ('date_order', '<', d_days_ago.strftime('%Y-%m-%d 00:00:00'))], context=None)

      purchase_received_today_count = self.pool.get('purchase.order').search_count(cr, uid,
         [('shipped', '=', True), ('invoiced', '=', True),
          ('date_order', '>=', today_date.strftime('%Y-%m-%d 00:00:00'))], context=None)

      purchase_closed_order_count = self.pool.get('purchase.order').search_count(cr, uid,
         [('shipped', '=', True), ('invoiced', '=', True),
          ('date_order', '>=', d_days_ago.strftime('%Y-%m-%d 00:00:00'))], context=None)

      top_sku_obj = self.pool.get('product.product').search_read(cr, uid,
         [('active', '=', True) ],['default_code', 'qty_available'],  order='qty_available desc', limit=5, context=None)

      # picked_list = self.pool.get('picking.list').search_read(cr, uid,
      #    [('state', '=', 'done'), ('date_confirm', '>=', d_days_ago.strftime('%Y-%m-%d 00:00:00'))],['date_confirm', 'id'], context=None)
      #
      #
      # packed_list = self.pool.get('packing.list').search_read(cr, uid,
      #    [('state', '=', 'done'), ('date_confirm', '>=', d_days_ago.strftime('%Y-%m-%d 00:00:00'))],['date_confirm', 'id'], context=None)

      limit = 5

      query = "SELECT DISTINCT SUM(sq.qty) AS total_qty, \
                               product.default_code, product.name_template\
                            FROM \
                               stock_quant sq\
                            INNER JOIN \
                               stock_location sl ON sl.id = sq.location_id \
                            INNER JOIN \
                               product_product product ON product.id = sq.product_id \
                            WHERE \
                               sl.usage='internal' \
                            GROUP BY \
                               product.id\
                            ORDER BY\
                                        total_qty DESC\
                            LIMIT %s" % (limit)
      cr.execute(query)
      # result = {}
      # key = 0
      # for qty, sku in cr.fetchall():
      #     result[str(sku)] = qty
      #     key = key + 1
      top_sku_obj = cr.fetchall()

      result = {
         'order_open': order_open_count,
         'picked_today': packing_done_today_count,
         'purchase_pending_confirmation': purchase_created_count,
         'purchase_yet_to_receive': purchase_confirmation_count,
         'purchase_pending_more_then_month': purchase_created_more_then_month_count,
         'purchase_received_today': purchase_received_today_count,
         'purchase_received': purchase_closed_order_count,
         'purchase_putaway_pending': putaway_pending_count,
         'purchase_putaway_confirm': putaway_done_count,
         'picking_pending': picking_pending_count,
         'picking_confirm': picking_done_count,
         'packing_done': packing_done_count,
         'top_skus': top_sku_obj,
         # 'picked_list': picked_list,
         # 'packed_list': packed_list,
      }

      print '----------------RESULT-------------','*'*15, result
      return result

    def top_sku_in_by_qty(self, cr, uid, params, fields, offset, limit=None):

        logicBoard = self.afterWhere(cr, uid, params, 'top_ten_sku_by_qty')
        limit = 10

        query = 'SELECT DISTINCT SUM(sol.qty_delivered) AS total_qty,\
                               product.name_template\
                              FROM \
                                 sale_order_line sol\
                              INNER JOIN \
                                 sale_order so ON so.id = sol.order_id \
                              INNER JOIN \
                                 product_product product ON product.id = sol.product_id \
                              INNER JOIN \
                                 product_template template ON template.id = product.product_tmpl_id \
                              INNER JOIN \
                                (WITH RECURSIVE category(id, parent_id, root_id) AS (\
                              SELECT id, parent_id, id AS root_id\
                              FROM product_category WHERE parent_id IS NULL\
                              UNION ALL\
                              SELECT t2.id, t2.parent_id, category.root_id\
                              FROM product_category t2, category WHERE t2.parent_id = category.id\
                              )\
                              SELECT (SELECT name FROM product_category WHERE id= category.root_id) as pname, id, root_id\
                              FROM category ) as category ON category.id = template.categ_id \
                              INNER JOIN \
                                 res_partner partner\
                              ON \
                                 partner.id = so.partner_id\
                              WHERE \
                                 sol.price_unit >0 AND sol.qty_delivered > 0 \
                                 %s \
                              GROUP BY \
                                 product.name_template\
                              ORDER BY\
                                          total_qty DESC\
                              LIMIT %s' % (logicBoard, limit)
        cr.execute(query)
        result = {}
        key = 0
        for qty, sku in cr.fetchall():
            result[str(sku)] = qty
            key = key + 1
        # result = dict(cr.fetchall())
        return result

    def top_selling_skus(self, cr, uid, params, fields, offset, limit=None):

        d = 30
        d_days_ago = datetime.now() - timedelta(days=d)
        limit = 5

        query = 'SELECT DISTINCT SUM(sol.qty_delivered) AS total_qty, \
                            COUNT(sol.order_id) AS total_ordered,\
                               product.default_code, product.name_template\
                              FROM \
                                 sale_order_line sol\
                              INNER JOIN \
                                 sale_order so ON so.id = sol.order_id \
                              INNER JOIN \
                                 product_product product ON product.id = sol.product_id \
                              WHERE \
                                 sol.price_unit >0 AND sol.qty_delivered > 0 AND so.date_order >= \'%s\' \
                              GROUP BY \
                               product.default_code, product.name_template\
                              ORDER BY\
                                          total_ordered DESC\
                              LIMIT %s' % (d_days_ago.strftime("%Y-%m-%d 00:00:00"), limit)
        cr.execute(query)
        # result = {}
        # key = 0
        # for qty, sku in cr.fetchall():
        #     result[str(sku)] = qty
        #     key = key + 1
        result = cr.fetchall()
        # print '----------------query-------------', query, '*-' * 15
        # print '----------------RESULT-------------', '*' * 15, result
        return result

dashboard_order()


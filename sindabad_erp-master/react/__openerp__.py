# -*- coding: utf-8 -*-
{
    'name': "React",

    'summary': """
		This module is To connect react with Odoo using xmlrpc connection""",

    'description': """
		This module is To connect react with Odoo using xmlrpc connection
	""",

    'author': "S. M. Sazedul Haque - Zero Gravity Ventures Ltd",
    'website': "http://zerogravity.com.bd/",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/openerp/addons/base/module/module_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': [
            'base',
            'sale',
            'web',
            # 'dashboard',
    ],

    # always loaded
    'data': [
        'security/react_security_view.xml',
        'security/ir.model.access.csv',
        'react_menu.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        # 'demo.xml',
    ],
}

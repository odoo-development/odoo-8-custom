# -*- coding: utf-8 -*-
##########################################################################
#
#    Copyright (c) 2018-Present Zero Gravity ventures Limited (<https://webkul.com/>)
#
##########################################################################

from datetime import datetime, timedelta
from openerp.osv import fields, osv
from openerp.tools.translate import _
from openerp import SUPERUSER_ID, api
import openerp.addons.decimal_precision as dp
import logging
import sys

reload(sys)
sys.setdefaultencoding('utf8')


class category_dashboard(osv.osv):
    _name = "react.category.dashboard"

    # _inherit = "react.purchase.dashboard"

    def category_list(self, cr, uid, params, fields, offset, limit=None):

        currentMonthResult = {}
        d = 60

        # today_date = datetime.today().date()
        # today_date = datetime.now() - timedelta(days=d)
        last_month_date_obj = datetime.today().date()

        # last_month_date_obj = datetime.now() - timedelta(days=d)
        current_month_date = last_month_date_obj.strftime('%Y-%m-01')

        month = last_month_date_obj.strftime('%m')
        last_month = int(month) - 1
        year = datetime.today().date().strftime('%Y')
        last_year = int(year) - 1
        if last_month == 0:
            last_month = 12
            last_month_date = last_month_date_obj.strftime('%d-%02d-01' % (last_year, last_month))
        else:
            last_month_date = last_month_date_obj.strftime('%Y-' + '%02d-01' % last_month)

        currentMonthQuery = "SELECT DISTINCT\
                        SUM(sol.price_unit*sol.qty_delivered) AS total_buy,\
                        category.pname\
                  FROM \
                     sale_order_line sol\
                  INNER JOIN \
                     sale_order so ON so.id = sol.order_id \
                  INNER JOIN \
                     product_product product ON product.id = sol.product_id \
                  INNER JOIN \
                     product_template template ON template.id = product.product_tmpl_id \
                  INNER JOIN \
                    (WITH RECURSIVE category(id, parent_id, root_id) AS (\
                  SELECT id, parent_id, id AS root_id\
                  FROM product_category WHERE parent_id IS NULL\
                  UNION ALL\
                  SELECT t2.id, t2.parent_id, category.root_id\
                  FROM product_category t2, category WHERE t2.parent_id = category.id\
                  )\
                  SELECT (SELECT name FROM product_category WHERE id= category.root_id) as pname, id, root_id\
                  FROM category ) as category ON category.id = template.categ_id \
                  WHERE \
                     sol.qty_delivered > 0 \
                     AND so.date_order >= \' %s \' \
                  GROUP BY \
                     category.pname\
                  ORDER BY \
                    category.pname \
                     " % (current_month_date)
        cr.execute(currentMonthQuery)

        currentMonthResult = cr.fetchall()

        lastMonthQuery = "SELECT DISTINCT\
                        SUM(sol.price_unit*sol.qty_delivered) AS total_buy,\
                        category.pname\
                  FROM \
                     sale_order_line sol\
                  INNER JOIN \
                     sale_order so ON so.id = sol.order_id \
                  INNER JOIN \
                     product_product product ON product.id = sol.product_id \
                  INNER JOIN \
                     product_template template ON template.id = product.product_tmpl_id \
                  INNER JOIN \
                    (WITH RECURSIVE category(id, parent_id, root_id) AS (\
                  SELECT id, parent_id, id AS root_id\
                  FROM product_category WHERE parent_id IS NULL\
                  UNION ALL\
                  SELECT t2.id, t2.parent_id, category.root_id\
                  FROM product_category t2, category WHERE t2.parent_id = category.id\
                  )\
                  SELECT (SELECT name FROM product_category WHERE id= category.root_id) as pname, id, root_id\
                  FROM category ) as category ON category.id = template.categ_id \
                  WHERE \
                     sol.qty_delivered > 0 \
                     AND so.date_order < \' %s \'  AND so.date_order >= \' %s \' \
                  GROUP BY \
                     category.pname\
                  ORDER BY \
                    category.pname \
                     " % (current_month_date, last_month_date)
        cr.execute(lastMonthQuery)

        LastMonthResult = cr.fetchall()

        i = 0
        result = []
        for r in currentMonthResult:
            each_result = (r[1], r[0], LastMonthResult[i][0])
            result.append(each_result)
            i += 1

        # print(current_month_date, last_month_date,month, last_month, year, result, '*'*20)

        return result

    def pending_procurement(self, cr, uid, params, fields, offset, limit=None):

        product_pool = self.pool.get('product.product')
        sale_order_line_pool = self.pool.get('sale.order.line')
        result = []
        result_ids = sale_order_line_pool.search(cr, uid, [('state', '=', 'confirmed')])
        # ('qty_available <= product_uom_qty')
        # result = [(d.default, d.qty_available, d.outgoing_qty) for d in sale_order_line_pool.browse(cr, uid, result_ids) if d.qty_available <= d.product_uom_qty ]
        for d in sale_order_line_pool.browse(cr, uid, result_ids):
            if d.qty_available <= d.product_uom_qty:
                result.append({
                    'default_code': d.default_code,
                    'sale_order': d.order_id.client_order_ref,
                    'product_uom_qty': d.product_uom_qty,
                    'qty_available': d.qty_available,
                    'pending_qty': (d.product_uom_qty - d.qty_available),
                    'warehouse': (d.order_id.warehouse_id.name),
                })

        # result_unique = dict((v['default_code'], v) for v in result).values()
        result_unique = result
        # print result_unique , '*--'*20
        return result_unique

    def procurement_cost(self, cr, uid, params, fields, offset, limit=None):

        d = 90

        last_month_date_obj = datetime.now() - timedelta(days=d)

        query = "SELECT DISTINCT\
                            category.pname, \
                            product.id, product.default_code\
                      FROM \
                         product_product product\
                      INNER JOIN \
                         product_template template ON template.id = product.product_tmpl_id \
                      INNER JOIN \
                        (WITH RECURSIVE category(id, parent_id, root_id) AS (\
                      SELECT id, parent_id, id AS root_id\
                      FROM product_category WHERE parent_id IS NULL\
                      UNION ALL\
                      SELECT t2.id, t2.parent_id, category.root_id\
                      FROM product_category t2, category WHERE t2.parent_id = category.id\
                      )\
                      SELECT (SELECT name FROM product_category WHERE id= category.root_id) as pname, id, root_id\
                      FROM category ) as category ON category.id = template.categ_id \
                      WHERE \
                         product.active = TRUE "
        cr.execute(query)

        result_query = cr.fetchall()

        i = 0
        result = {}
        plus = 0
        minus = 0
        equal = 0
        for r in result_query:
            query = "   SELECT price_unit, order_id\
                            FROM \
                                purchase_order_line pol\
                            WHERE \
                                pol.state  = 'confirmed' AND pol.product_id = %s \
                            ORDER BY \
                                id DESC LIMIT  2 " % (r[1])
            cr.execute(query)

            pol_query = cr.fetchall()

            if len(pol_query) > 1:
                if pol_query[0][0] > pol_query[1][0]:
                    plus = plus + 1
                elif pol_query[0][0] < pol_query[1][0]:
                    minus = minus + 1
                elif pol_query[0][0] == pol_query[1][0]:
                    equal = equal + 1
                else:
                    pass
                # print "product_id= %s | unitPrice of order_id = %s is = %s and unitPrice of 2nd last order_id = %s is = %s" % (r[1], pol_query[0][1] , pol_query[0][0], pol_query[1][1], pol_query[1][0])
                # print "-8-"*15
            result[r[0]] = (minus, plus, equal)
            i += 1
        result_final = []
        result_final=[(k, v[0], v[1], v[2]) for k, v in result.items()]

        print(result_final, '*' * 20)

        return result_final


category_dashboard()

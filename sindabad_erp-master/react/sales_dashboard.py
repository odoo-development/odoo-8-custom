# -*- coding: utf-8 -*-
##########################################################################
#
#    Copyright (c) 2018-Present Zero Gravity ventures Limited (<https://webkul.com/>)
#
##########################################################################

from datetime import datetime, timedelta
from openerp.osv import fields, osv
from openerp.tools.translate import _
from openerp import SUPERUSER_ID, api
import openerp.addons.decimal_precision as dp
import logging
import sys

reload(sys)
sys.setdefaultencoding('utf8')



class sales_dashboard(osv.osv):
    _name = "react.sales.dashboard"

    def sales_info(self, cr, uid, params, fields, offset, limit=None):
      # print '----------------SALES-INFO-------------', '*' * 15

      if params.has_key('day'):
         # print 'concern is exist in params'
         d = tuple(params.get('day'))
      else:
         d = 30

      today_date = datetime.today().date()
      # today_date = datetime.now() - timedelta(days=d)

      ontrack_date = datetime.now() - timedelta(days=2) # 2days = 48 Hours
      delayed_date = datetime.now() - timedelta(days=4) # 4days = 96 Hours


      # first_date_of_month = today_date.strftime('%Y-%m-') + '01'

      ### START]>--------------Sales---Order----Table---###

      ### --- Total--Orders->>--Status==Draft--of--Current--Month,--&--Todays---###
      order_draft_count = self.pool.get('sale.order').search_count(cr, uid,
         [('state', '=', 'draft'), ('date_order', '>=', today_date.strftime('%Y-%m-01 00:00:00'))], context=None)

      today_order_draft_count = self.pool.get('sale.order').search_count(cr, uid,
         [('state', '=', 'draft'), ('date_order', '>=', today_date.strftime('%Y-%m-%d 00:00:00'))], context=None)

      ### --- Total--Orders->>--Status==Progress--of--Current--Month,--&--Todays---###
      order_confirm_count = self.pool.get('sale.order').search_count(cr, uid,
         [('state', '=', 'progress'), ('date_order', '>=', today_date.strftime('%Y-%m-01 00:00:00'))], context=None)

      today_order_confirm_count = self.pool.get('sale.order').search_count(cr, uid,
         [('state', '=', 'progress'), ('date_order', '>=', today_date.strftime('%Y-%m-%d 00:00:00'))], context=None)

      ### --- Total--Orders->>--Status==Cancel--of--Current--Month,--&--Todays---###
      order_cancel_count = self.pool.get('sale.order').search_count(cr, uid,
         [('state', '=', 'cancel'), ('date_order', '>=', today_date.strftime('%Y-%m-01 00:00:00'))], context=None)

      today_order_cancel_count = self.pool.get('sale.order').search_count(cr, uid,
         [('state', '=', 'cancel'), ('date_order', '>=', today_date.strftime('%Y-%m-%d 00:00:00'))], context=None)

      ### --- Invoiced->>--shipped==True==-OPEN--of--Current--Month,--&--Todays---###
      invoice_open_count = self.pool.get('sale.order').search_count(cr, uid,
         [('shipped', '=', True), ('invoiced', '=', False), ('date_order', '>=', today_date.strftime('%Y-%m-01 00:00:00'))], context=None)

      today_invoice_open_count = self.pool.get('sale.order').search_count(cr, uid,
         [('shipped', '=', True), ('invoiced', '=', False), ('date_order', '>=', today_date.strftime('%Y-%m-%d 00:00:00'))], context=None)

      ### --- Invoiced->>--shipped==True==-OPEN--of--Current--Month,--&--Todays---###
      invoice_paid_count = self.pool.get('sale.order').search_count(cr, uid,
         [('shipped', '=', True), ('invoiced', '=', True), ('date_order', '>=', today_date.strftime('%Y-%m-01 00:00:00'))], context=None)

      today_invoice_paid_count = self.pool.get('sale.order').search_count(cr, uid,
         [('shipped', '=', True), ('invoiced', '=', True), ('date_order', '>=', today_date.strftime('%Y-%m-%d 00:00:00'))], context=None)

      ### --- Delivery->>----of--Current--Month,--&--Todays---###

      ready_for_process_count = self.pool.get('stock.picking').search_count(cr, uid,
         [('state', '=', 'assigned'), ('picking_type_id.code', '=', 'outgoing'), ('picked', '=', False), ('date_confirm', '>=', today_date.strftime('%Y-%m-01 00:00:00'))], context=None)
      today_ready_for_process_count = self.pool.get('stock.picking').search_count(cr, uid,
         [('state', '=', 'assigned'), ('picking_type_id.code', '=', 'outgoing'), ('picked', '=', False), ('date_confirm', '>=', today_date.strftime('%Y-%m-%d 00:00:00'))], context=None)

      dispatch_count = self.pool.get('wms.manifest.process').search_count(cr, uid,
         [('state','=','confirm'), ('confirm_time', '>=', today_date.strftime('%Y-%m-01 00:00:00'))], context=None)
      today_dispatch_count = self.pool.get('wms.manifest.process').search_count(cr, uid,
         [('state','=','confirm'), ('confirm_time', '>=', today_date.strftime('%Y-%m-%d 00:00:00'))], context=None)

      return_count = self.pool.get('wms.manifest.outbound').search_count(cr, uid,
         [('has_return','=',True), ('delivery_date', '>=', today_date.strftime('%Y-%m-01'))], context=None)
      today_return_count = self.pool.get('wms.manifest.outbound').search_count(cr, uid,
         [('has_return','=',True), ('delivery_date', '>=', today_date.strftime('%Y-%m-%d'))], context=None)

      delivered_count = self.pool.get('wms.manifest.process').search_count(cr, uid,
         [('state','=','confirm'), ('confirm_time', '>=', today_date.strftime('%Y-%m-01 00:00:00'))], context=None)
      today_delivered_count = self.pool.get('wms.manifest.process').search_count(cr, uid,
         [('state','=','confirm'), ('confirm_time', '>=', today_date.strftime('%Y-%m-%d 00:00:00'))], context=None)

      ### END--]>--------------Sales---Order----Table-------------------------------------###
      ### ]>------------------------------------------------------------------------------###

      ### ]>------------------------------------------------------------------------------###
      ### START]>--------------Sales---Order----PIE-&-CHART--OF----AGEING-----------------###

      ontrack_date_count = self.pool.get('sale.order').search_count(cr, uid,
         [('state', '=', 'progress'), ('date_confirm', '>=', ontrack_date.strftime('%Y-%m-%d'))], context=None)

      delayed_date_count = self.pool.get('sale.order').search_count(cr, uid,
         [('state', '=', 'progress'), ('date_confirm', '<=', delayed_date.strftime('%Y-%m-%d'))], context=None)

      at_risk_count = self.pool.get('sale.order').search_count(cr, uid,
         [('state', '=', 'progress'), ('date_confirm', '<', ontrack_date.strftime('%Y-%m-%d')), ('date_confirm', '>', delayed_date.strftime('%Y-%m-%d'))], context=None)



      ready_for_process_ontrack_count = self.pool.get('stock.picking').search_count(cr, uid,
         [('state', '=', 'assigned'), ('picking_type_id.code', '=', 'outgoing'), ('picked', '=', False), ('date_confirm', '>=', ontrack_date.strftime('%Y-%m-%d 00:00:00'))], context=None)
      ready_for_process_delayed_count = self.pool.get('stock.picking').search_count(cr, uid,
         [('state', '=', 'assigned'), ('picking_type_id.code', '=', 'outgoing'), ('picked', '=', False), ('date_confirm', '<=', delayed_date.strftime('%Y-%m-%d 00:00:00'))], context=None)
      ready_for_process_at_risk_count = self.pool.get('stock.picking').search_count(cr, uid,
         [('state', '=', 'assigned'), ('picking_type_id.code', '=', 'outgoing'), ('picked', '=', False), ('date_confirm', '<', ontrack_date.strftime('%Y-%m-%d 00:00:00')), ('date_confirm', '>', delayed_date.strftime('%Y-%m-%d 00:00:00'))], context=None)

      ready_for_dispatch_ontrack_count = self.pool.get('account.invoice').search_count(cr, uid,
         [('type','=','out_invoice'), ('state', '=', 'draft'), ('date_invoice', '>=', ontrack_date.strftime('%Y-%m-%d'))], context=None)
      ready_for_dispatch_delayed_count = self.pool.get('account.invoice').search_count(cr, uid,
         [('type','=','out_invoice'), ('state', '=', 'draft'), ('date_invoice', '<=', delayed_date.strftime('%Y-%m-%d'))], context=None)
      ready_for_dispatch_at_risk_count = self.pool.get('account.invoice').search_count(cr, uid,
         [('type','=','out_invoice'), ('state', '=', 'draft'), ('date_invoice', '<', ontrack_date.strftime('%Y-%m-%d')), ('date_invoice', '>', delayed_date.strftime('%Y-%m-%d'))], context=None)

      dispatch_ontrack_count = self.pool.get('wms.manifest.process').search_count(cr, uid,
         [('state','=','confirm'), ('confirm_time', '>=', ontrack_date.strftime('%Y-%m-%d 00:00:00'))], context=None)
      dispatch_delayed_count = self.pool.get('wms.manifest.process').search_count(cr, uid,
         [('state','=','confirm'), ('confirm_time', '>=', delayed_date.strftime('%Y-%m-%d 00:00:00'))], context=None)
      dispatch_at_risk_count = self.pool.get('wms.manifest.process').search_count(cr, uid,
         [('state','=','confirm'), ('confirm_time', '<', ontrack_date.strftime('%Y-%m-%d 00:00:00')), ('confirm_time', '>', delayed_date.strftime('%Y-%m-%d 00:00:00'))], context=None)

      delivered_ontrack_count = self.pool.get('sale.order').search_count(cr, uid,
         [('delivery_dispatch','=',True), ('date_confirm', '>=', ontrack_date.strftime('%Y-%m-%d'))], context=None)
      delivered_delayed_count = self.pool.get('sale.order').search_count(cr, uid,
         [('delivery_dispatch','=',True), ('date_confirm', '>=', delayed_date.strftime('%Y-%m-%d'))], context=None)
      delivered_at_risk_count = self.pool.get('sale.order').search_count(cr, uid,
         [('delivery_dispatch','=',True), ('date_confirm', '<', ontrack_date.strftime('%Y-%m-%d')), ('date_confirm', '>', delayed_date.strftime('%Y-%m-%d'))], context=None)
      ### ]>------------------------------------------------------------------------------###
      ### END]]]>--------------Sales---Order----PIE-&-CHART--OF----AGEING-----------------###
      ### ]>------------------------------------------------------------------------------###


      ### ]>------------------------------------------------------------------------------###
      ### START]>--------------Purchase---Order----PIE-&-CHART--OF----AGEING-----------------###

      po_ontrack_date_count = self.pool.get('purchase.order').search_count(cr, uid,
         [('state', '=', 'approved'), ('shipped', '=', False), ('date_order', '>=', ontrack_date.strftime('%Y-%m-%d'))], context=None)

      po_delayed_date_count = self.pool.get('purchase.order').search_count(cr, uid,
         [('state', '=', 'approved'), ('shipped', '=', False), ('date_order', '<=', delayed_date.strftime('%Y-%m-%d'))], context=None)

      po_at_risk_count = self.pool.get('purchase.order').search_count(cr, uid,
         [('state', '=', 'approved'), ('shipped', '=', False), ('date_order', '<', ontrack_date.strftime('%Y-%m-%d')), ('date_order', '>', delayed_date.strftime('%Y-%m-%d'))], context=None)


      # ------- Purchase -------------

      ageing_of_two_days_date = datetime.now() - timedelta(days=2)  # 2 days
      ageing_of_5_days_date = datetime.now() - timedelta(days=5)  # 5 days
      ageing_of_10_days_date = datetime.now() - timedelta(days=10)  # 10 days
      ageing_of_20_days_date = datetime.now() - timedelta(days=20)  # 20 days

      po_ageing_of_two_days_date_count = self.pool.get('purchase.order').search_count(cr, uid,
         [('state', '=', 'draft'), ('date_order', '<', ageing_of_two_days_date.strftime('%Y-%m-%d'))], context=None)
      po_ageing_of_5_days_date_count = self.pool.get('purchase.order').search_count(cr, uid,
         [('state', '=', 'draft'), ('date_order', '<', ageing_of_5_days_date.strftime('%Y-%m-%d'))], context=None)
      po_ageing_of_10_days_date_count = self.pool.get('purchase.order').search_count(cr, uid,
         [('state', '=', 'draft'), ('date_order', '<', ageing_of_10_days_date.strftime('%Y-%m-%d'))], context=None)
      po_ageing_of_20_days_date_count = self.pool.get('purchase.order').search_count(cr, uid,
         [('state', '=', 'draft'), ('date_order', '<', ageing_of_20_days_date.strftime('%Y-%m-%d'))], context=None)

      month_query = "SELECT DISTINCT SUM(amount_total) AS total_amount \
                            FROM account_invoice \
                            WHERE type='out_invoice' AND date_invoice >= \'%s\' " % (today_date.strftime('%Y-%m-01'))
      cr.execute(month_query)
      month_invoiced_amount = cr.fetchall()

      today_query = "SELECT DISTINCT SUM(amount_total) AS total_amount \
                            FROM account_invoice \
                            WHERE type='out_invoice' AND date_invoice = \'%s\' " % (today_date.strftime('%Y-%m-%d'))
      cr.execute(today_query)
      today_invoiced_amount = cr.fetchall()

      delivered_month_query = "SELECT DISTINCT SUM(amount_total) AS total_amount \
                            FROM account_invoice \
                            WHERE delivered=True AND type='out_invoice' AND date_invoice >= \'%s\' " % (today_date.strftime('%Y-%m-01'))
      cr.execute(delivered_month_query)
      delivered_month_invoiced_amount = cr.fetchall()

      delivered_today_query = "SELECT DISTINCT SUM(amount_total) AS total_amount \
                            FROM account_invoice \
                            WHERE delivered=True AND type='out_invoice' AND date_invoice = \'%s\' " % (today_date.strftime('%Y-%m-%d'))
      cr.execute(delivered_today_query)
      delivered_today_invoiced_amount = cr.fetchall()



      process_month_query = "SELECT DISTINCT SUM(amount_total) AS total_amount \
                            FROM account_invoice \
                            WHERE state='open' AND type='out_invoice' AND date_invoice >= \'%s\' " % (today_date.strftime('%Y-%m-01'))
      cr.execute(process_month_query)
      process_month_invoiced_amount = cr.fetchall()

      process_today_query = "SELECT DISTINCT SUM(amount_total) AS total_amount \
                            FROM account_invoice \
                            WHERE state='open' AND type='out_invoice' AND date_invoice = \'%s\' " % (today_date.strftime('%Y-%m-%d'))
      cr.execute(process_today_query)
      process_today_invoiced_amount = cr.fetchall()

      top_sku_obj = cr.fetchall()

      result = {
        'order_draft': order_draft_count if order_draft_count else 0,
        'today_order_draft': today_order_draft_count if today_order_draft_count else 0,
        'order_confirm': order_confirm_count if order_confirm_count else 0,
        'today_order_confirm': today_order_confirm_count if today_order_confirm_count else 0,
        'order_cancel': order_cancel_count if order_cancel_count else 0,
        'today_order_cancel': today_order_cancel_count if today_order_cancel_count else 0,
        'invoice_open': invoice_open_count if invoice_open_count else 0,
        'today_invoice_open': today_invoice_open_count if today_invoice_open_count else 0,
        'invoice_paid': invoice_paid_count if invoice_paid_count else 0,
        'today_invoice_paid': today_invoice_paid_count if today_invoice_paid_count else 0,
        'ready_for_process': ready_for_process_count if ready_for_process_count else 0,
        'today_ready_for_process': today_ready_for_process_count if today_ready_for_process_count else 0,
        'dispatch': dispatch_count if dispatch_count else 0,
        'today_dispatch': today_dispatch_count if today_dispatch_count else 0,
        'return': return_count if return_count else 0,
        'today_return': today_return_count if today_return_count else 0,
        'delivered': delivered_count if delivered_count else 0,
        'today_delivered': today_delivered_count if today_delivered_count else 0,
        'ontrack_date': ontrack_date_count if ontrack_date_count else 0,
        'delayed_date': delayed_date_count if delayed_date_count else 0,
        'at_risk': at_risk_count if at_risk_count else 0,
        'ready_for_process_ontrack': ready_for_process_ontrack_count if ready_for_process_ontrack_count else 0,
        'ready_for_process_delayed': ready_for_process_delayed_count if ready_for_process_delayed_count else 0,
        'ready_for_process_at_risk': ready_for_process_at_risk_count if ready_for_process_at_risk_count else 0,
        'ready_for_dispatch_ontrack': ready_for_dispatch_ontrack_count if ready_for_dispatch_ontrack_count else 0,
        'ready_for_dispatch_delayed': ready_for_dispatch_delayed_count if ready_for_dispatch_delayed_count else 0,
        'ready_for_dispatch_at_risk': ready_for_dispatch_at_risk_count if ready_for_dispatch_at_risk_count else 0,
        'dispatch_ontrack': dispatch_ontrack_count if dispatch_ontrack_count else 0,
        'dispatch_delayed': dispatch_delayed_count if dispatch_delayed_count else 0,
        'dispatch_at_risk': dispatch_at_risk_count if dispatch_at_risk_count else 0,
        'delivered_ontrack': delivered_ontrack_count if delivered_ontrack_count else 0,
        'delivered_delayed': delivered_delayed_count if delivered_delayed_count else 0,
        'delivered_at_risk': delivered_at_risk_count if delivered_at_risk_count else 0,
        'po_ontrack_date': po_ontrack_date_count if po_ontrack_date_count else 0,
        'po_delayed_date': po_delayed_date_count if po_delayed_date_count else 0,
        'po_at_risk': po_at_risk_count if po_at_risk_count else 0,
        'po_ageing_of_two_days_date': po_ageing_of_two_days_date_count if po_ageing_of_two_days_date_count else 0,
        'po_ageing_of_5_days_date': po_ageing_of_5_days_date_count if po_ageing_of_5_days_date_count else 0,
        'po_ageing_of_10_days_date': po_ageing_of_10_days_date_count if po_ageing_of_10_days_date_count else 0,
        'po_ageing_of_20_days_date': po_ageing_of_20_days_date_count if po_ageing_of_20_days_date_count else 0,
        'month_invoiced_amount': month_invoiced_amount[0] if month_invoiced_amount[0] else 0,
        'today_invoiced_amount': today_invoiced_amount[0] if today_invoiced_amount[0] else 0,
        'delivered_month_invoiced_amount': delivered_month_invoiced_amount[0] if delivered_month_invoiced_amount[0] else 0,
        'delivered_today_invoiced_amount': delivered_today_invoiced_amount[0] if delivered_today_invoiced_amount[0] else 0,
        'process_month_invoiced_amount': process_month_invoiced_amount[0] if process_month_invoiced_amount[0] else 0,
        'process_today_invoiced_amount': process_today_invoiced_amount[0] if process_today_invoiced_amount[0] else 0,
      }

      # print '----------------RESULT-------------','*'*15, result
      return result


sales_dashboard()


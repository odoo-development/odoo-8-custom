
from datetime import datetime, timedelta
import time
from openerp import SUPERUSER_ID
from openerp.osv import fields, osv
from openerp.tools.translate import _
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT
import openerp.addons.decimal_precision as dp
from openerp import workflow
from openerp.exceptions import except_orm, Warning, RedirectWarning


class sale_order(osv.osv):
    _inherit = "sale.order"

    def action_button_confirm(self, cr, uid, ids, context=None):

        so = self.browse(cr, uid, ids, context=context)

        parent = so.partner_id
        company_id = None

        for i in range(5):
            if len(parent.parent_id) == 1:
                parent = parent.parent_id
            else:
                # rm_mobile_numbers = parent.user_id.partner_id.phone
                company_id = parent.id
                break

        res_p_obj = self.pool.get('res.partner')
        res_p = res_p_obj.browse(cr, uid, [company_id], context=context)

        # res_p.property_account_receivable.id
        # 7074
        if res_p.property_account_receivable.id == 7074 or res_p.property_account_receivable == None:

            if 'Retail' not in str(so.customer_classification) and 'SOHO' not in str(so.customer_classification) \
                    and 'General' not in str(so.customer_classification) and 'HORECA' not in str(so.customer_classification):
                raise Warning(_('The receivable account has not been assigned by the Finance Team. Please contact with Finance Team!!!'))
            else:
                res = super(sale_order, self).action_button_confirm(cr, uid, ids, context=context)
        else:

            res = super(sale_order, self).action_button_confirm(cr, uid, ids, context=context)

        return res

{
    'name': 'Account Receivable Confirmation',
    'version': '1.0',
    'category': 'ERP',
    'author': 'Shahjalal Hossain',
    'summary': 'Account Receivable Confirmation',
    'description': 'Account Receivable Confirmation',
    'depends': ['base', 'sale', 'purchase', 'account', 'odoo_magento_connect'],
    'installable': True,
    'application': True,
    'auto_install': False,
}

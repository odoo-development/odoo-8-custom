{
    'name': 'Product name population',
    'version': '1.0',
    'category': 'Product name population',
    'author': 'Shahjalal Hossain',
    'summary': 'Product name population',
    'description': 'Product simple sync',
    'depends': ['base', 'odoo_magento_connect', 'stock', 'product'],
    'data': [
        'product_name_pop.xml',
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}

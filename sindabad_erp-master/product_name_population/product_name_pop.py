import math
import re
import time
import urllib

from openerp import api, tools, SUPERUSER_ID
from openerp.osv import osv, fields, expression
from openerp.tools.translate import _
from openerp.tools import DEFAULT_SERVER_DATETIME_FORMAT
import psycopg2

import openerp.addons.decimal_precision as dp
from openerp.tools.float_utils import float_round, float_compare


class product_product(osv.osv):
    _inherit = "product.product"

    def _populate_product_name(self, cr, uid, ids=None, context=None):

        # search [ids] product_name is blank
        blank_prod_name = self.search(cr, uid, [('product_name', '=', None)])

        # browse those [ids] to get "display_name"
        # populate product_name with "display_name"
        count = 0
        for prod in self.browse(cr, uid, blank_prod_name, context=context):
            try:
                disp_name = str(urllib.unquote(str(prod.display_name)).decode('utf8')).replace("\"", "").replace("\'", "")

                if disp_name:
                    prod_name_update_query = "UPDATE product_product SET product_name='{0}' WHERE id={1}".format(disp_name, prod.id)
                    cr.execute(prod_name_update_query)
                    cr.commit()
            except:
                count += 1
                pass
        print("\n\nNumber of products cannot be update product name:" + str(count)+"\n\n")
        return True


from openerp.addons.web import http
from openerp.addons.web.http import request

class outstanding_po_dashboard(http.Controller):
    @http.route(["/outstanding/"], type='http', auth="public", website=True)
    def view(self, **kwargs):

        values = {
        }
        return request.website.render('vendor_outstanding_dashboard.outstanding_chart', values)

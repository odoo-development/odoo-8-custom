from openerp.osv import fields, osv
from openerp.tools.translate import _
from openerp import api
from time import gmtime, strftime
import datetime


class vendor_outstanding(osv.osv):
    _inherit = "res.partner"
    _description = "Vendor outstanding"

    def _get_last_payment_date(self, cr, uid, ids, field_name, args, context=None):
        res = dict.fromkeys(ids, 0)

        po= self.pool.get('purchase.order')
        po_ids = po.search(cr, uid, [('partner_id', 'in',ids)], context=context)

        for po_obj in self.pool.get('purchase.order').browse(cr, uid, po_ids, context=context):
            last_payment_date = ''
            account_invoice = self.pool.get('account.invoice')
            account_invoice_obj = account_invoice.search(cr, uid, [('origin', '=', po_obj.name)], order='write_date', context=context)
            if account_invoice_obj:
                last_payment_data = account_invoice.browse(cr, uid, account_invoice_obj[0])
                last_payment_date = last_payment_data.write_date
                res[po_obj.partner_id.id] = last_payment_date
        return res


    def _get_last_payment_amount(self, cr, uid, ids, field_name, args, context=None):
        res = dict.fromkeys(ids, 0)


        po= self.pool.get('purchase.order')
        po_ids = po.search(cr, uid, [('partner_id', 'in', ids)], context=context)

        for po_obj in self.pool.get('purchase.order').browse(cr, uid, po_ids, context=context):
            last_payment_amount = ''
            account_invoice = self.pool.get('account.invoice')
            account_invoice_obj = account_invoice.search(cr, uid, [('origin', '=', po_obj.name)], order='write_date', context=context)
            if account_invoice_obj:
                last_payment_data = account_invoice.browse(cr, uid, account_invoice_obj[0])
                last_payment_amount = last_payment_data.amount_total
                res[po_obj.partner_id.id] = last_payment_amount

        return res


    def url_view(self, cr, uid, ids, context=None):
        final_url = "/outstanding/?res_id=" + str(context['res_id'])
        return {
            'type': 'ir.actions.act_url',
            'target': 'new',
            'url': final_url,
        }


    _columns = {
        'last_payment_date': fields.function(_get_last_payment_date, string='Last Payment Date', type='char'),
        'last_payment_amount': fields.function(_get_last_payment_amount, string='Last Payment Amount', type='char'),
            }


class purchase_order(osv.osv):
    _inherit = "purchase.order"
    _description = "Vendor outstanding"

    def supplier_outstanding_data(self, cr, uid, res_id , context=None):

        delivery_info={'':[{}]}

        po_info=[{"id": ''}]


        query_po = "select name,id,create_date,(select res_partner.name from res_partner where res_partner.id = purchase_order.create_uid) as creator,amount_total from purchase_order where purchase_order.partner_id = %s"
        cr.execute(query_po,(int(res_id),))
        result = cr.fetchall()
        po_name = [x[0] for x in result]
        if po_name:
            query = "select purchase_order.id,purchase_order.name as po_name,purchase_order.create_date,(select res_partner.name from res_partner where res_partner.id = purchase_order.create_uid) as creator,purchase_order.amount_total as po_amount ,account_invoice.id as acc,account_invoice.number as invoice_number,account_invoice.amount_total as receive_amount, account_invoice.write_date as payment_date,account_invoice.residual as residual,account_invoice.state,stock_picking.name as grn,stock_picking.date_done as receive_date from purchase_order , account_invoice, stock_picking WHERE stock_picking.origin = purchase_order.name and account_invoice.origin = stock_picking.name and stock_picking.origin in %s"

            cr.execute(query, (tuple(po_name),))
            data = cr.dictfetchall()
            po_id_list=[]
            po_ids=[]
            delivery = {}
            if data:
                po_info = []

                for row in data:
                     po_id = row['id']
                     res = {
                         'id': row['acc'],
                         'grn_number': row['grn'],
                         'invoice_number': row['invoice_number'],
                         'referene': '',
                         'receive_date': row['receive_date'],
                         'receive_amount': row['receive_amount'],
                         'paid_amount': float(row['receive_amount']) - float(row['residual']),
                         'payment_date': row['payment_date'] if row['state'] == 'paid' else '',
                         'payment_type': ''
                     }
                     if po_id in po_ids:
                         delivery[po_id].append(res)
                         row['receive_amount']+= row['receive_amount']
                         row['residual']+= row['residual']

                     else:
                         delivery[po_id]=[res]
                         po_ids.append(po_id)

                     delivery_info.update(delivery)
                     po = {
                         "id": row['id'],
                         "po_number": row['po_name'],
                         "po_create_date": row['create_date'],
                         "po_created_by": row['creator'],
                         "po_amount": row['po_amount'],
                         "po_type": '',
                         "po_receive": row['receive_amount'],
                         "amount_remaining": row['residual'],
                         "po_paid_amount": float(row['residual']) - float(row['residual'])
                     }


                     if po_id not in po_id_list:
                         po_info.append(po)

                     po_id_list.append(po_id)
            else:
                po_info = []
                for row_result in result:
                    po_id = row_result[1]

                    delivery_info.update(delivery)
                    po = {
                        "id": row_result[1],
                        "po_number": row_result[0],
                        "po_create_date": row_result[2],
                        "po_created_by": row_result[3],
                        "po_amount": row_result[4],
                        "po_type": '',
                        "po_receive": '',
                        "amount_remaining":'',
                        "po_paid_amount": ''
                    }

                    if po_id not in po_id_list:
                        po_info.append(po)

                    po_id_list.append(po_id)

        result2={'po_data':po_info,'delivery_data':delivery_info}

        return result2


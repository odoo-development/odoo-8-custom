{
    'name': 'Vendor Outstanding Dashboard',
    'version': '8.0.0',
    'category': 'Purchase',
    'description': """
Vendor Outstanding Dashboard 
============================

""",
    'author': 'Rocky',
    'depends': ['base','order_approval_process'],
    'data': [

        'security/vendor_outstanding_security.xml',

        'outstanding/views/vendor_outstanding.xml',
        'outstanding/views/vendor_outstanding_menu.xml',

        'vendor_outstanding_view.xml',
    ],

    'installable': True,
    'auto_install': False,
}
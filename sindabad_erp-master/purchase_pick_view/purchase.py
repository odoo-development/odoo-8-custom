import pytz
from openerp import SUPERUSER_ID, workflow
from datetime import datetime
from dateutil.relativedelta import relativedelta
from operator import attrgetter
from openerp.tools.safe_eval import safe_eval as eval
from openerp.osv import fields, osv
from openerp.tools.translate import _
import openerp.addons.decimal_precision as dp
from openerp.osv.orm import browse_record_list, browse_record, browse_null
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT, DATETIME_FORMATS_MAP
from openerp.tools.float_utils import float_compare

class purchase_order(osv.osv):
    _inherit="purchase.order"

    def view_picking(self, cr, uid, ids, context=None):
        '''
        This function returns an action that display existing picking orders of given purchase order ids.
        '''
        if context is None:
            context = {}
        mod_obj = self.pool.get('ir.model.data')
        dummy, action_id = tuple(mod_obj.get_object_reference(cr, uid, 'stock', 'action_picking_tree'))
        action = self.pool.get('ir.actions.act_window').read(cr, uid, action_id, context=context)

        pick_ids = []
        origin_list= []

        for po in self.browse(cr, uid, ids, context=context):
            pick_ids += [picking.id for picking in po.picking_ids]

        pick_ids_new = self.pool.get('stock.picking').search(cr, uid, [('id', 'in', pick_ids)], context=context)
        abc_data = self.pool.get('stock.picking').browse(cr, uid, pick_ids_new, context=context)

        group_id=[]
        for it_n in abc_data:
            origin_list.append(str(it_n.name))
            group_id = it_n.group_id.id

        final_pick_ids_new = self.pool.get('stock.picking').search(cr, uid, [('origin', 'in', origin_list)], context=context)

        for pic_id in final_pick_ids_new:
            if pic_id not in pick_ids:
                pick_ids.append(pic_id)


        g_final_pick_ids_new = self.pool.get('stock.picking').search(cr, uid, [('group_id', 'in', [group_id])],
                                                                         context=context)
        for pic_id in g_final_pick_ids_new:
            if pic_id not in pick_ids:
                pick_ids.append(pic_id)



        #override the context to get rid of the default filtering on picking type
        action['context'] = {}
        #choose the view_mode accordingly
        if len(pick_ids) > 1:
            action['domain'] = "[('id','in',[" + ','.join(map(str, pick_ids)) + "])]"
        else:
            res = mod_obj.get_object_reference(cr, uid, 'stock', 'view_picking_form')
            action['views'] = [(res and res[1] or False, 'form')]
            action['res_id'] = pick_ids and pick_ids[0] or False

        return action

    def _len_of_picking(self, cr, uid, ids, field_name, arg, context=None):
        '''
        This function returns an action that display existing picking orders of given purchase order ids.
        '''

        if context is None:
            context = {}
        mod_obj = self.pool.get('ir.model.data')
        dummy, action_id = tuple(mod_obj.get_object_reference(cr, uid, 'stock', 'action_picking_tree'))
        action = self.pool.get('ir.actions.act_window').read(cr, uid, action_id, context=context)

        pick_ids = []
        origin_list= []
        inv_ids =[]

        for po in self.browse(cr, uid, ids, context=context):
            pick_ids += [picking.id for picking in po.picking_ids]
            inv_ids = [items.id for items in po.invoice_ids]

        pick_ids_new = self.pool.get('stock.picking').search(cr, uid, [('id', 'in', pick_ids)], context=context)
        abc_data = self.pool.get('stock.picking').browse(cr, uid, pick_ids_new, context=context)

        group_id=[]
        for it_n in abc_data:
            origin_list.append(str(it_n.name))
            group_id = it_n.group_id.id



        final_pick_ids_new = self.pool.get('stock.picking').search(cr, uid, [('origin', 'in', origin_list)], context=context)


        for pic_id in final_pick_ids_new:
            if pic_id not in pick_ids:
                pick_ids.append(pic_id)


        g_final_pick_ids_new = self.pool.get('stock.picking').search(cr, uid, [('group_id', 'in', [group_id])],
                                                                         context=context)
        for pic_id in g_final_pick_ids_new:
            if pic_id not in pick_ids:
                pick_ids.append(pic_id)
        # Folowing code for Invoice counting

        pick_ids_new = self.pool.get('stock.picking').search(cr, uid, [('id', 'in', pick_ids)], context=context)
        new_abc_data = self.pool.get('stock.picking').browse(cr, uid, pick_ids_new, context=context)
        new_origin_list = []

        for stk_obj in new_abc_data:
            new_origin_list.append(str(stk_obj.name))

        final_invoice_ids_new = self.pool.get('account.invoice').search(cr, uid, [('origin', 'in', new_origin_list)],
                                                                        context=context)

        for inv_id in final_invoice_ids_new:
            if inv_id not in inv_ids:
                inv_ids.append(inv_id)




        res ={}
        for po in self.browse(cr, uid, ids, context=context):
            res[po.id]={
                'shipment_count': len(pick_ids),
                'invoice_count': len(inv_ids),
            }



        return res

    def invoice_open(self, cr, uid, ids, context=None):
        mod_obj = self.pool.get('ir.model.data')
        act_obj = self.pool.get('ir.actions.act_window')

        result = mod_obj.get_object_reference(cr, uid, 'account', 'action_invoice_tree2')
        id = result and result[1] or False
        result = act_obj.read(cr, uid, [id], context=context)[0]
        inv_ids = []
        ## Custom INvoice counts ends here
        pick_ids = []
        origin_list = []

        for po in self.browse(cr, uid, ids, context=context):
            inv_ids+= [invoice.id for invoice in po.invoice_ids]
            pick_ids += [picking.id for picking in po.picking_ids]


        pick_ids_new = self.pool.get('stock.picking').search(cr, uid, [('id', 'in', pick_ids)], context=context)
        abc_data = self.pool.get('stock.picking').browse(cr, uid, pick_ids_new, context=context)

        group_id=[]
        for it_n in abc_data:
            origin_list.append(str(it_n.name))
            group_id = it_n.group_id.id



        final_pick_ids_new = self.pool.get('stock.picking').search(cr, uid, [('origin', 'in', origin_list)], context=context)


        for pic_id in final_pick_ids_new:
            if pic_id not in pick_ids:
                pick_ids.append(pic_id)


        g_final_pick_ids_new = self.pool.get('stock.picking').search(cr, uid, [('group_id', 'in', [group_id])],
                                                                         context=context)
        for pic_id in g_final_pick_ids_new:
            if pic_id not in pick_ids:
                pick_ids.append(pic_id)

        pick_ids_new = self.pool.get('stock.picking').search(cr, uid, [('id', 'in', pick_ids)], context=context)
        new_abc_data = self.pool.get('stock.picking').browse(cr, uid, pick_ids_new, context=context)
        new_origin_list = []

        for stk_obj in new_abc_data:
            new_origin_list.append(str(stk_obj.name))

        final_invoice_ids_new = self.pool.get('account.invoice').search(cr, uid, [('origin', 'in', new_origin_list)],
                                                                        context=context)

        for inv_id in final_invoice_ids_new:
            if inv_id not in inv_ids:
                inv_ids.append(inv_id)
        ## Custom INvoice counts ends here

        if not inv_ids:
            raise osv.except_osv(_('Error!'), _('Please create Invoices.'))
         #choose the view_mode accordingly
        if len(inv_ids)>1:
            result['domain'] = "[('id','in',["+','.join(map(str, inv_ids))+"])]"
        else:
            res = mod_obj.get_object_reference(cr, uid, 'account', 'invoice_supplier_form')
            result['views'] = [(res and res[1] or False, 'form')]
            result['res_id'] = inv_ids and inv_ids[0] or False
        return result

    _columns = {
        'shipment_count': fields.function(_len_of_picking, type='integer', string='Incoming Shipments', multi=True),
    }
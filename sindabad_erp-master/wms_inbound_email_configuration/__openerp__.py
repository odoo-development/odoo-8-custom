# Author S. M. Sazedul Haque 2019-01-19

{
    'name': 'WMS Inbound Email Configuration',
    'version': '1.0.0',
    'category': 'Accounts',
    'description': """
WMS Inbound Email Configuration   
==============================================================

""",
    'author': 'S. M. Sazedul Haque',
    'depends': ['account', 'report', 'purchase', 'product', 'wms_inbound', 'wms_menulist'],
    'data': [
        'email_configuration_menu_vew.xml',
        'email_configuration_view.xml',
    ],
    'installable': True,
    'auto_install': False,
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:

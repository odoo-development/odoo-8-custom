
from openerp.osv import fields, osv
import logging
_logger = logging.getLogger(__name__)

class po_email_configuration(osv.osv):
    _name = "po.email.configuration"
    _description = "PO EMAIL Configuration"
    _order = 'id desc'

    _columns = {
        # 'email_to': fields.char('TO',required=True),
        'name': fields.char("Configuration Name", required=True),
        'template_id': fields.many2one('email.template', 'Email Template', domain=[('model', 'in', ['purchase.order','product.product'])]),
        'active': fields.boolean('Active', required=False),

    }

    _sql_constraints = [
        ('template_id_unique', 'unique(template_id)', 'Can\'t be duplicate value for this field!')
    ]

    def email_trigger_with_conditional_po(self, cr, uid, id=None, context=None):

        cr.execute("select name, template_id, active from po_email_configuration")
        email_conf_data = cr.fetchall()
        id = self.pool['purchase.order'].search(cr, uid,[])[-1]

        if len(email_conf_data) >0:
            for query_data in email_conf_data:
                name = query_data[0]
                # print 'length, name=', len(email_conf_data), name, '*'*10
                template_id = query_data[1]
                active = query_data[2]
                if template_id and active is True:
                    try:
                        # print 'id=', id, 'template_id=', template_id
                        self.pool.get('email.template').send_mail(cr, uid, template_id, id, force_send=True)
                    except Exception, e:
                        _logger.warning('Email failed with following exception: %s', e.message)
                else:
                    _logger.warning('Status of %s is %s', name, active)

        return True



    def email_trigger_with_conditional_product(self, cr, uid, id, context=None):
        cr.execute("select name, template_id, active from po_email_configuration")

        email_conf_data = cr.fetchall()
        id = self.pool['product.product'].search(cr, uid,[])[-1]

        if len(email_conf_data) >0:
            for query_data in email_conf_data:
                name = query_data[0]
                # print 'length, name=', len(email_conf_data), name, '*'*10
                template_id = query_data[1]
                active = query_data[2]
                if template_id and active is True:
                    try:
                        # print 'id=', id, 'template_id=', template_id
                        self.pool.get('email.template').send_mail(cr, uid, template_id, id, force_send=True)
                    except Exception, e:
                        _logger.warning('Email failed with following exception: %s', e.message)
                else:
                    _logger.warning('Status of %s is %s', name, active)

        return True




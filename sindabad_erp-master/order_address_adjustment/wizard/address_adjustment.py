##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import logging
from datetime import datetime
from dateutil.relativedelta import relativedelta
from operator import itemgetter
import time

import openerp
from openerp import SUPERUSER_ID, api
from openerp import tools
from openerp.osv import fields, osv, expression
from openerp.tools.float_utils import float_compare, float_round
from openerp.tools.translate import _
from openerp.tools.float_utils import float_round as round

from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT, DATETIME_FORMATS_MAP
from openerp.tools.float_utils import float_compare
import openerp.addons.decimal_precision as dp
from openerp.addons.procurement import procurement

_logger = logging.getLogger(__name__)


class order_address_adjustment(osv.osv_memory):
    _name = "order.address.adjustment"
    _description = "Order Address Mismatch Adjustment"


    _columns = {
        'description': fields.char('Description'),
    }

    def invoice_cancel(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        proxy = self.pool['account.invoice']
        active_ids = context.get('active_ids', []) or []

        for record in proxy.browse(cr, uid, active_ids, context=context):
            if record.state in ('cancel', 'paid'):
                raise osv.except_osv(_('Warning!'), _(
                    "Selected invoice(s) cannot be cancelled as they are already in 'Cancelled' or 'Done' state."))
            record.signal_workflow('invoice_cancel')
        return {'type': 'ir.actions.act_window_close'}

    @api.multi
    def action_cancel_draft(self):
        # go from canceled state to draft state
        self.write({'state': 'draft'})
        self.delete_workflow()
        self.create_workflow()
        return True


    def order_address_adjustment(self, cr, uid, data, context):


        order_ids = context['active_ids']

        order_data_list = self.pool.get('sale.order').browse(cr, uid, context['active_id'], context=context)





        for order_data in order_data_list:
            invoice_id_list=order_data.invoice_ids
            picking_id_list=order_data.picking_ids



            if picking_id_list:
                for pick_id in picking_id_list:
                    cr.execute("update stock_picking set partner_id=%s where id=%s", ([order_data.partner_shipping_id.id, pick_id.id]))

            if invoice_id_list:

                inv_list =[]
                for it in invoice_id_list: inv_list.append(it.id)

                invoice_list = self.pool.get('account.invoice').browse(cr, uid, inv_list, context=context)
                for inv_data in invoice_list:
                    if inv_data.partner_id.id == order_data.partner_invoice_id.id:
                        pass
                    else:
                        if inv_data.state in ('cancel', 'paid'):
                            pass
                        elif inv_data.state in ('draft'):
                            inv_data.signal_workflow('invoice_cancel')
                            cr.execute("update account_invoice set partner_id=%s where id=%s",
                                       ([order_data.partner_invoice_id.id, inv_data.id]))
                            inv_data.write({'state': 'draft'})
                            inv_data.delete_workflow()
                            inv_data.create_workflow()

                        else:
                            inv_data.signal_workflow('invoice_cancel')
                            cr.execute("update account_invoice set partner_id=%s where id=%s",([order_data.partner_invoice_id.id, inv_data.id]))
                            inv_data.write({'state': 'draft'})
                            inv_data.delete_workflow()
                            inv_data.create_workflow()
                            inv_data.signal_workflow('invoice_open')


        return True


































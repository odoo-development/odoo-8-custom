# Mufti Muntasir Ahmed

{
    'name': 'Order Adjustment with address mismatch',
    'version': '1.0',
    'category': 'Sale',
    'summary': """Order Adjustment with address mismatch """,
    'description': """Order Adjustment with address mismatch """,
    'author': 'Mufti Muntasir Ahmed',
    'website': 'http://www.sindabad.com',
    'depends': ['odoo_magento_connect', 'sale'],
    'init_xml': [],
    'update_xml': [
         'wizard/address_adjustment.xml',
         'sale_view.xml',

         
    ],
    'demo_xml': [],
    'installable': True,
    'active': False,

}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:

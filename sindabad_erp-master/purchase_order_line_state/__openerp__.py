{
    'name': 'Purchase Order Line State',
    'summary': """Purchase Order Line State""",
    'description': """

Purchase order line status

    """,
    'author': 'Shahjalal Hossain',
    'website': 'www.sindabad.com',
    'category': 'Sales',
    'version': '1.0.0',
    'depends': ['purchase', 'sale', 'stock', 'stock_account', 'partial_delivery_in_so_list'],
    'data': [
        'purchase_order.xml'
    ],
    'installable': True,
    'auto_install': False,
    'application': True,
}

import pytz
from openerp import SUPERUSER_ID, workflow, api, models, fields
from datetime import datetime
from dateutil.relativedelta import relativedelta
from operator import attrgetter
from openerp.tools.safe_eval import safe_eval as eval
from openerp.osv import fields, osv
from openerp.tools.translate import _
import openerp.addons.decimal_precision as dp
from openerp.osv.orm import browse_record_list, browse_record, browse_null
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT, DATETIME_FORMATS_MAP
from openerp.tools.float_utils import float_compare
from openerp import tools


class purchase_order_line(osv.osv):

    _inherit = 'purchase.order.line'

    _columns = {
        'line_received': fields.char('Received'),
    }


class last_transfer_date_so_process(object):

    def last_transfer_date_of_so(self, cr, uid, ids, context=None):
        # ids -> sale_order_id list

        # so -> ids get "name"
        for id in ids:
            so_name_query = "SELECT name FROM sale_order WHERE id='{0}'".format(id)
            cr.execute(so_name_query)
            for so_n in cr.fetchall():
                so_name = str(so_n[0])

                # SELECT date_done FROM stock_picking WHERE origin='SO18037' ORDER BY date_done DESC NULLS LAST LIMIT 1

                so_last_tr_date_query = "SELECT date_done FROM stock_picking WHERE origin='{0}' ORDER BY date_done DESC NULLS LAST LIMIT 1".format(so_name)

                cr.execute(so_last_tr_date_query)
                for so_tr_date in cr.fetchall():

                    try:

                        # update in sale order
                        if so_tr_date[0] is not None:
                            so_tr_date_update_query = "UPDATE sale_order SET last_transfer_date='{0}' WHERE id='{1}'".format(so_tr_date[0], id)

                            cr.execute(so_tr_date_update_query)
                            cr.commit()
                    except:

                        pass

        return True


class sale_order(osv.osv):
    _inherit = 'sale.order'

    _columns = {
        'last_transfer_date': fields.datetime('Last Transfer Date', help="Last Transfer Date")
    }

    def _last_transfer_date_so_computation(self, cr, uid, ids=None, context=None):
        context = {'lang': 'en_US', 'params': {'action': 404}, 'tz': 'Asia/Dhaka', 'uid': uid}

        so_id_query = "SELECT id FROM sale_order ORDER BY id ASC"
        cr.execute(so_id_query)

        ltdsop = last_transfer_date_so_process()
        for so_id in cr.fetchall():
            ltdsop.last_transfer_date_of_so(cr, uid, [int(so_id[0])], context=context)

        return True


class order_line_status_computation(object):

    def order_line_status_process(self, cr, uid, ids, context=None):

        # ids -> order_id list

        # po -> ids get "name" = "origin" -> stock.move
        for id in ids:
            po_name_query = "SELECT name FROM purchase_order WHERE id='{0}'".format(id)
            cr.execute(po_name_query)
            for po_n in cr.fetchall():
                po_name = str(po_n[0])

                # check stock.move -> origin, product_id -> state
                stock_move_query = "SELECT product_id, state FROM stock_move WHERE origin='{0}'".format(po_name)
                cr.execute(stock_move_query)
                for sm in cr.fetchall():

                    # purchase.order.line -> order_id, product_id -> line_received(save)
                    # update query
                    product_id = int(sm[0])
                    product_line_state = "False"
                    if str(sm[1]) == "done":
                        product_line_state = "True"
                    elif str(sm[1]) == "cancel":
                        product_line_state = "Cancel"
                    else:
                        product_line_state = "False"

                    po_line_query = "UPDATE purchase_order_line SET line_received='{0}' WHERE order_id='{1}' AND product_id='{2}'".format(product_line_state, id, product_id)

                    cr.execute(po_line_query)
                    cr.commit()

        return True


class purchase_order(osv.osv):

    _inherit = 'purchase.order'

    def _purchase_order_line_status_computation(self, cr, uid, ids=None, context=None):

        context = {'lang': 'en_US', 'params': {'action': 404}, 'tz': 'Asia/Dhaka', 'uid': uid}

        po_id_query = "SELECT id FROM purchase_order ORDER BY id ASC"
        cr.execute(po_id_query)

        olsc = order_line_status_computation()
        for po_id in cr.fetchall():
            olsc.order_line_status_process(cr, uid, [int(po_id[0])], context=context)

        return True

    def action_cancel(self, cr, uid, ids, context=None):
        for purchase in self.browse(cr, uid, ids, context=context):
            for pick in purchase.picking_ids:
                for move in pick.move_lines:
                    if pick.state == 'done':
                        raise osv.except_osv(
                            _('Unable to cancel the purchase order %s.') % (purchase.name),
                            _('You have already received some goods for it.  '))
            self.pool.get('stock.picking').action_cancel(cr, uid, [x.id for x in purchase.picking_ids if x.state != 'cancel'], context=context)
            for inv in purchase.invoice_ids:
                if inv and inv.state not in ('cancel', 'draft'):
                    raise osv.except_osv(
                        _('Unable to cancel this purchase order.'),
                        _('You must first cancel all invoices related to this purchase order.'))
            self.pool.get('account.invoice') \
                .signal_workflow(cr, uid, map(attrgetter('id'), purchase.invoice_ids), 'invoice_cancel')
        self.signal_workflow(cr, uid, ids, 'purchase_cancel')

        # -----------------purchase_order_line_state-------------------
        olsc = order_line_status_computation()
        for id in ids:
            olsc.order_line_status_process(cr, uid, [id], context=context)

        # -----------------purchase_order_line_state-------------------
        return True


class stock_transfer_details(models.TransientModel):
    _inherit = 'stock.transfer_details'

    @api.one
    def do_detailed_transfer(self):
        if self.picking_id.state not in ['assigned', 'partially_available']:
            raise Warning(_('You cannot transfer a picking in state \'%s\'.') % self.picking_id.state)

        processed_ids = []
        # Create new and update existing pack operations
        for lstits in [self.item_ids, self.packop_ids]:
            for prod in lstits:
                pack_datas = {
                    'product_id': prod.product_id.id,
                    'product_uom_id': prod.product_uom_id.id,
                    'product_qty': prod.quantity,
                    'package_id': prod.package_id.id,
                    'lot_id': prod.lot_id.id,
                    'location_id': prod.sourceloc_id.id,
                    'location_dest_id': prod.destinationloc_id.id,
                    'result_package_id': prod.result_package_id.id,
                    'date': prod.date if prod.date else datetime.now(),
                    'owner_id': prod.owner_id.id,
                }
                if prod.packop_id:
                    prod.packop_id.with_context(no_recompute=True).write(pack_datas)
                    processed_ids.append(prod.packop_id.id)
                else:
                    pack_datas['picking_id'] = self.picking_id.id
                    packop_id = self.env['stock.pack.operation'].create(pack_datas)
                    processed_ids.append(packop_id.id)
        # Delete the others
        packops = self.env['stock.pack.operation'].search(['&', ('picking_id', '=', self.picking_id.id), '!', ('id', 'in', processed_ids)])
        packops.unlink()

        # Execute the transfer of the picking
        self.picking_id.do_transfer()

        # -----------------purchase_order_line_state-------------------

        if str(self.picking_id.origin).startswith("PO"):

            olsc = order_line_status_computation()
            context = {'lang': 'en_US', 'params': {'action': 404}, 'tz': 'Asia/Dhaka', 'uid': self._uid}
            po_query = "SELECT id FROM purchase_order WHERE name='{0}'".format(str(self.picking_id.origin))
            self._cr.execute(po_query)
            for po_id in self._cr.fetchall():
                olsc.order_line_status_process(self._cr, self._uid, [int(po_id[0])], context=context)
        # -----------------purchase_order_line_state-------------------

        # Set information for transfer buddy
        try:

            picking_data = self.env['stock.picking'].search([('id', '=', self.picking_id.id)])
            po_number = str(picking_data.origin)  # po_number or record_name
            if po_number and po_number.startswith("PO"):
                user_data = self.env['res.users'].search([('id', '=', self._uid)])
                partner_data = self.env['res.partner'].search([('id', '=', user_data.partner_id.id)])
                mail_message = self.env['mail.message'].search(
                    ['&', ('record_name', '=', str(picking_data.origin)),
                     ('body', 'like', '<span>RFQ Approved</span>')])

                message_id = tools.generate_tracking_message_id(str(mail_message.res_id) + '-' + 'purchase.order')
                if mail_message:
                    self.env.cr.execute("""
                                    INSERT INTO mail_message 
                                      (create_date, write_date, write_uid, create_uid, parent_id, res_id, message_id, body, model, record_name, no_auto_thread, date, author_id, type, reply_to, email_from, website_published) 
                                    VALUES 
                                      ((%s), (%s), (%s), (%s), (%s), (%s), (%s), (%s), (%s), (%s), (%s), (%s), (%s), (%s), (%s), (%s), (%s))""",
                                        (datetime.now(), datetime.now(), int(user_data.id), int(user_data.id),
                                         int(mail_message.parent_id), int(mail_message.res_id), str(message_id),
                                         '<p>Products received</p>', 'purchase.order', str(picking_data.origin),
                                         bool(False),
                                         datetime.now(), int(partner_data.commercial_partner_id.id), 'notification',
                                         str(partner_data.email), str(partner_data.email), bool(True)))
                    self.env.cr.commit()
        except Exception:
            pass


        # partial_delivery_in_so_list
        if str(self.picking_id.origin).startswith("SO"):

            sale_order_query = "SELECT client_order_ref FROM sale_order WHERE name='{0}'".format(
                str(self.picking_id.origin))
            self.env.cr.execute(sale_order_query)
            client_order_ref_data = self.env.cr.fetchall()
            client_order_ref = str(client_order_ref_data[0][0])

            context = {'lang': 'en_US', 'params': {'action': 404}, 'tz': 'Asia/Dhaka', 'uid': self._uid}
            # waiting availability
            from ..partial_delivery_in_so_list.sale import partial_waiting_process_class
            pwp = partial_waiting_process_class()
            pwp.waiting_availability_process(self.env.cr, self._uid, client_order_ref, context=context)

            # partially delivered
            sale_order_pool = self.pool.get('sale.order')
            pwp.partially_delivered_prcess(sale_order_pool, self.env.cr, self._uid, client_order_ref, context=context)

            # partially available
            pwp.partially_available_process(self.env.cr, self._uid, client_order_ref, context=context)

            # Last transfer date so process
            so_id_query = "SELECT id FROM sale_order WHERE name='{0}'".format(str(self.picking_id.origin))
            self._cr.execute(so_id_query)
            ltdsop = last_transfer_date_so_process()
            for so_id in self._cr.fetchall():
                ltdsop.last_transfer_date_of_so(self._cr, self._uid, [int(so_id[0])], context=context)

        return True

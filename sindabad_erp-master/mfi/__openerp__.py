{
    'name': 'MFI',
    'version': '1.0',
    'category': 'MFI',
    'author': 'Mufti, Shuvarthi, Babu, Shahjalal',
    'summary': 'MFI',
    'description': 'MFI',
    'depends': ['base', 'odoo_magento_connect', 'account', 'sale', 'stock','order_approval_process'],
    'data': [
        'security/mfi_security.xml',
        'security/ir.model.access.csv',

        'views/wizard/reattempt_view.xml',
        'views/loan_credit_congif_view.xml',
        'views/mfi_customer_view.xml',
        'views/res_partner_view.xml',
        'views/mfi_invoice_view.xml',
        'views/magento_mfi_mapping.xml',
        'views/mfi_credit_history_view.xml',
        'views/bank_bill_submission_create.xml',

        'mfi_menu.xml',


    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}

from openerp import SUPERUSER_ID
from openerp.osv import fields, osv
from openerp import api
from datetime import date
from openerp.tools.translate import _


class mfi_loan_credit_config(osv.osv):
    _name = "mfi.loan.credit.config"
    _description = "MFI loan credit config"

    _columns = {
        'name': fields.char('Name'),
        'code': fields.char('Code'),
        'min_amount': fields.float('Min Amount'),
        'max_amount': fields.float('Max Amount'),
        'credit_days': fields.float('Credit Days'),
        'interest_rate': fields.float('Interest Rate'),
        'interest_rate_duration': fields.float('interest_rate_duration'),
        'charge_amount': fields.float('Charge Amount'),
        'charge_duration': fields.float('Charge Duration'),
        'active': fields.boolean('Active'),
        'calculation_type': fields.selection([
            ('monthly', 'Monthly'),
            ('yearly', 'Yearly')

        ], 'Calculation Type', copy=False, help="Gives the calculation of loan credit", select=True),

        'loan_other_charges_line': fields.one2many('mfi.loan.other.charges.line', 'loan_credit_id', 'Loan Other Charges Line'),

    }



class mfi_loan_other_charges_line(osv.osv):
    _name = "mfi.loan.other.charges.line"
    _description = "mfi loan other charges line"

    _columns = {
        'loan_credit_id': fields.many2one('mfi.loan.credit.config', 'Loan Credit Config ID', required=True,
                                          ondelete='cascade', select=True, readonly=True),
        'charge_name': fields.char('Name'),
        'percent_rate': fields.float('% Rate'),
        'flat_rate_amount': fields.float('Flat Rate Amount'),
        'onetime_charge': fields.boolean(string="One Time")

    }

    _default = {
        'onetime_charge': False,
    }


class sale_order(osv.osv):

    _inherit = "sale.order"

    _columns = {
        'payment_code': fields.char('Payment Code'),
        'mfi_id': fields.many2one('mfi.loan.credit.config', 'MFI'),
    }

    def create(self, cr, uid, data, context=None):

        if data.has_key("payment_code"):

            payment_code = data['payment_code']

            config_obj = self.pool.get("mfi.loan.credit.config")
            config_obj_list = config_obj.search(cr, uid, [('code', '=', payment_code)])
            if config_obj_list:
                data['mfi_id'] = config_obj_list[0]
            else:
                config_data = {
                    'name': payment_code,
                    'code': payment_code,
                    'active': True,
                    'calculation_type': 'yearly'
                }
                mfi_con_id = config_obj.create(cr, uid, config_data, context=context)
                data['mfi_id'] = mfi_con_id

        res = super(sale_order, self).create(cr, uid, data, context=context)

        return res


class stock_picking(osv.osv):
    _inherit = "stock.picking"
    _columns = {
        'payment_code': fields.char('Payment Code'),
        'mfi_id': fields.many2one('mfi.loan.credit.config', 'MFI'),
    }

    def create(self, cr, uid, data, context=None):

        order_no = str(data.get('origin'))

        if order_no.startswith("SO"):
            so_obj = self.pool.get('sale.order')
            so_id = so_obj.search(cr, uid, [('name', '=', order_no)], context=context)
            if so_id:
                so = so_obj.browse(cr, uid, so_id, context=context)

                if so.payment_code == 'ipdc':
                    data['payment_code'] = str(so.payment_code)
                    data['mfi_id'] = so.mfi_id.id

        res = super(stock_picking, self).create(cr, uid, data, context=context)

        return res


class account_invoice(osv.osv):
    _inherit = 'account.invoice'
    _columns = {
        'payment_code': fields.char('Payment Code'),
        'mfi_id': fields.many2one('mfi.loan.credit.config', 'MFI'),
        'bill_submission':fields.boolean('Bill Submission')
    }

    def bank_bill_submission(self,cr,uid,ids,context=None):
        mfi_invoice_obj = self.pool.get('mfi.invoice.submission')

        if context is None:
            context = {}
        # get_data = self.read(cr, uid, ids)[0]
        data = {}
        mfi_invoice_line = []
        total_amount=0.00
        today_date = date.today()


        invoice_data=self.pool.get('account.invoice').browse(cr, uid,context.get('active_ids') , context=context)

        invoice_date = list(set([inv_date.date_invoice for inv_date in invoice_data]))

        if len(invoice_date) == 1:

            for items in invoice_data:
                total_amount += items.amount_total

                so_data_id = self.pool.get('sale.order').search(cr, uid,[('client_order_ref','=',items.name)], context=context)
                so_data = self.pool.get('sale.order').browse(cr, uid,so_data_id, context=context)

                mfi_invoice_line.append([0, False,{
                    'account_invoice_id':items.id,
                    'order_id':so_data[0].id,
                    'partner_id':so_data[0].partner_id.id,
                    'invoice_amount':items.amount_total,
                    'state':'pending',

                }])

                data['payment_code']=items.payment_code
                data['submitted_to']=items.payment_code
                data['mfi_invoice_line'] = mfi_invoice_line

            data.update({
                'total_amount':total_amount,
                'state':'pending',
                'submitted_from':'sindabad.com',
                'submitted_date':today_date,
                'invoice_date': invoice_date[0]

            })
            save_the_data = mfi_invoice_obj.create(cr,uid, data, context=context)
            if save_the_data:

                for i in context.get('active_ids'):
                    update_query = "UPDATE account_invoice SET bill_submission=TRUE WHERE id ={0}".format(i)
                    cr.execute(update_query)
                    cr.commit()
        else:
            raise osv.except_osv(_('Warning!'),
                                 _('Please select the same date invoice'))

        return save_the_data

    def create(self, cr, uid, data, context=None):

        if data.get('name'):
            mag_order_no = str(data.get('name'))
            so_obj = self.pool.get('sale.order')
            so_id = so_obj.search(cr, uid, [('client_order_ref', '=', mag_order_no)], context=context)
            if so_id:
                so = so_obj.browse(cr, uid, so_id, context=context)

                if so.payment_code == 'ipdc':
                    data['payment_code'] = str(so.payment_code)
                    data['mfi_id'] = so.mfi_id.id

        res = super(account_invoice, self).create(cr, uid, data, context=context)

        return res


class mfi_customer(osv.osv):
    _name = "mfi.customer"
    _description = "MFI Customer"

    def _get_available_amount(self, cr, uid, ids, name, arg, context=None):

        res = {}

        for mfi_c in self.browse(cr, uid, ids, context=context):
            res[mfi_c.id] = mfi_c.credit_limit + mfi_c.balance

        return res

    _columns = {
        'partner_id': fields.many2one('res.partner', 'Customer'),
        'mfi_name': fields.char('MFI Name'),
        'mfi_account_number': fields.char('MFI account no'),
        'magento_company_id': fields.integer('Mag Company ID'),
        'credit_limit':  fields.float('Credit Limit'),
        'balance': fields.float('Balance'),
        'credit_days': fields.float('Credit Days'),

        'name': fields.char('Name'),
        'mfi_id': fields.many2one('mfi.loan.credit.config', 'MFI'),
        # 'available_amount': fields.float('Available Amount'),
        'available_amount': fields.function(_get_available_amount, digits=0, string="Available Amount"),

        'email': fields.char('Email'),
        'mobile': fields.char('Mobile'),
        'mfi_code': fields.char('MFI Code'),
        'payment_code': fields.char('Payment Code'),
        'credit_amount': fields.float('Credit Amount'),
        'used_amount': fields.float('Used Amount'),
        'paid_amount': fields.float('Paid Amount'),
        'unpaid_amount': fields.float('Unpaid Amount'),
        'trade_license_file': fields.binary('Trade License File'),
        'trade_license_file_url': fields.char('Trade License File Url',size=1024),
        'trade_license': fields.boolean('Trade License'),
        'tin_number': fields.char('TIN Number'),
        'tin_file': fields.binary('TIN File'),
        'tin_file_url': fields.char('TIN File Url',size=1024),
        'tin': fields.boolean('TIN'),
        'vat_bin_number': fields.char('VAT BIN Number'),
        'vat_file': fields.binary('VAT File'),
        'vat_file_url': fields.char('VAT File Url',size=1024),
        'vat': fields.boolean('VAT'),
        'nid_number': fields.char('NID Number'),
        'nid_file': fields.binary('NID File'),
        'nid_file_url': fields.char('NID File Url',size=1024),
        'nid': fields.boolean('NID'),
        'photograph1_image':fields.binary('Photograph1 Image'),
        'photograph1_image_url':fields.char('Photograph1 Image Url',size=1024),
        'photograph2_image':fields.binary('Photograph2 Image'),
        'photograph2_image_url':fields.char('Photograph2 Url',size=1024),
        'bank_statement_file': fields.binary('Bank Statement File'),
        'bank_statement_url': fields.char('Bank Statement Url', size=1024),
        'bank_statement': fields.boolean('Bank Statement'),
        'sales_ledger_file': fields.binary('Sales Ledger File'),
        'sales_ledger_url': fields.char('Sales Ledger Url', size=1024),
        'sales_ledger': fields.boolean('Sales Ledger'),
        'bill_invoice_do_lo_file': fields.binary('Bill Invoice File'),
        'bill_invoice_do_lo_file_url': fields.char('Bill Invoice File Url', size=1024),
        'bill_invoice_do_lo': fields.boolean('Bill Invoice'),
        'banks_nbfis_libilities_file': fields.binary('Banks nbfis Libilities File'),
        'banks_nbfis_libilities_file_url': fields.char('Banks nbfis Libilities File Url', size=1024),
        'banks_nbfis_libilities': fields.boolean('Banks nbfis Libilities'),
        'electricity_bill_file': fields.binary('Electricity Bill File'),
        'electricity_bill_file_url': fields.char('Electricity Bill File Url', size=1024),
        'electricity_bill': fields.boolean('Electricity Bill'),
        'rental_agreement_file': fields.binary('Rental Agreement File'),
        'rental_agreement_file_url': fields.char('Rental Agreement File Url', size=1024),
        'rental_agreement': fields.boolean('Rental Agreement'),
        'fire_license_file': fields.binary('Fire License File'),
        'fire_license_file_url': fields.char('Fire License File Url', size=1024),
        'fire_license': fields.boolean('Fire License'),
        'env_certificate_file': fields.binary('Environmental Certificate File'),
        'env_certificate_file_url': fields.char('Environmental Certificate File Url', size=1024),
        'env_certificate': fields.boolean('Environmental Certificate'),
        'guarantor_name': fields.char('Guarantor Name'),
        'guarantor_mobile': fields.char('Guarantor Mobile'),
        'guarantor_nid_number': fields.char('Guarantor NID Number'),
        'guarantor_nid_file': fields.binary('Guarantor NID File'),
        'guarantor_nid_file_url': fields.char('Guarantor NID File Url', size=1024),
        'guarantor_nid': fields.boolean('Guarantor NID'),
        'guarantor_tin_number': fields.char('Guarantor TIN Number'),
        'guarantor_tin_file': fields.binary('Guarantor TIN File'),
        'guarantor_tin_file_url': fields.char('Guarantor TIN File Url', size=1024),
        'guarantor_tin': fields.boolean('Guarantor TIN'),
        'guarantor_trade_file': fields.binary('Guarantor Trade File'),
        'guarantor_trade_file_url': fields.char('Guarantor Trade File Url', size=1024),
        'guarantor_trade': fields.boolean('Guarantor Trade'),
        'guarantor_photo1_file': fields.binary('Guarantor Photo1 File'),
        'guarantor_photo1_file_url': fields.char('Guarantor Photo1 File Url', size=1024),
        'guarantor_photo1': fields.boolean('Guarantor Photo1'),
        'guarantor_photo2_file': fields.binary('Guarantor Photo2 File'),
        'guarantor_photo2_file_url': fields.char('Guarantor Photo2 File Url', size=1024),
        'guarantor_photo2': fields.boolean('Guarantor Photo2'),
        'guarantor_info_line': fields.one2many('mfi.guarantor.info.line', 'mfi_customer_id',
                                                   'Guarantor Info Line'),
        'mfi_cre_history_line': fields.one2many('mfi.credit.history', 'mfi_customer_id', 'MFI Credit History'),
    }

    def create(self, cr, uid, data, context=None):

        mfi_name = data['mfi_name']

        partner_obj = self.pool.get("res.partner")
        partner = partner_obj.browse(cr, uid, data['partner_id'], context=context)
        data['name'] = partner.name  # cutomer name

        config_obj = self.pool.get("mfi.loan.credit.config")
        config_obj_list = config_obj.search(cr, uid, [('name', '=', mfi_name), ('code', '=', mfi_name)])
        if config_obj_list:
            data['mfi_id'] = config_obj_list[0]
        else:
            config_data = {
                'name': mfi_name,
                'code': mfi_name,
                'active': True,
                'calculation_type': 'yearly'
            }
            mfi_con_id = config_obj.create(cr, uid, config_data, context=context)
            data['mfi_id'] = mfi_con_id

        res = super(mfi_customer, self).create(cr, uid, data, context=context)

        return res


class mfi_guarantor_info_line(osv.osv):
    _name = "mfi.guarantor.info.line"
    _description = "mfi guarantor info line"

    _columns = {
        'mfi_customer_id': fields.many2one('mfi.customer', 'MFI Customer ID', required=True,
                                          ondelete='cascade', select=True, readonly=True),
        'guarantor_name': fields.char('Guarantor Name'),
        'guarantor_mobile': fields.char('Guarantor Mobile'),
        'guarantor_nid_number': fields.char('Guarantor NID Number'),
        'guarantor_nid_file': fields.binary('Guarantor NID File'),
        'guarantor_nid_file_url': fields.char('Guarantor NID File Url', size=1024),
        'guarantor_nid': fields.boolean('Guarantor NID'),
        'guarantor_tin_number': fields.char('Guarantor TIN Number'),
        'guarantor_tin_file': fields.binary('Guarantor TIN File'),
        'guarantor_tin_file_url': fields.char('Guarantor TIN File Url', size=1024),
        'guarantor_tin': fields.boolean('Guarantor TIN'),
        'guarantor_trade_file': fields.binary('Guarantor Trade File'),
        'guarantor_trade_file_url': fields.char('Guarantor Trade File Url', size=1024),
        'guarantor_trade': fields.boolean('Guarantor Trade'),
        'guarantor_photo1_file': fields.binary('Guarantor Photo1 File'),
        'guarantor_photo1_file_url': fields.char('Guarantor Photo1 File Url', size=1024),
        'guarantor_photo1': fields.boolean('Guarantor Photo1'),
        'guarantor_photo2_file': fields.binary('Guarantor Photo2 File'),
        'guarantor_photo2_file_url': fields.char('Guarantor Photo2 File Url', size=1024),
        'guarantor_photo2': fields.boolean('Guarantor Photo2'),
    }


class res_partner(osv.osv):
    _inherit = 'res.partner'

    _columns = {
        'mfi_customer_line': fields.one2many('mfi.customer', 'partner_id', 'MFI Customer'),
        'mfi_credit_history_line': fields.one2many('mfi.credit.history', 'odoo_company_id', 'MFI Credit History'),
        'mfi_invoice_line': fields.one2many('mfi.invoice.line', 'partner_id', 'MFI Invoice'),
    }


class mfi_invoice_submission(osv.osv):
    _name = "mfi.invoice.submission"
    _description = "MFI Invoice Submission"

    _columns = {
        'total_amount': fields.float('Amount'),
        'name': fields.char('Name'),
        'payment_code': fields.char('Payment Code'),
        'invoice_date': fields.date('Invoice Date'),
        'submission_date': fields.date('Submission Date'),
        'submitted_from': fields.char('Submitted From'),
        'submitted_to': fields.char('Submitted To'),
        'confirmed_date': fields.date('Confirmed Date'),
        'submitted_date': fields.date('Submitted Date'),
        'reattempted': fields.boolean('Reattempted'),
        'state': fields.selection([
            ('pending', 'Pending Submission'),
            ('confirmed', 'Confirmed'),
            ('submitted', 'Submitted'),
            ('cancel', 'cancel')

        ], 'Status', readonly=True, copy=False, help="Gives the status of the MFI Invoice", select=True),
        'mfi_invoice_line':fields.one2many('mfi.invoice.line', 'mfi_invoice_sibmission_id', string='Invoice Lines')
    }

    def confirm_mfi_bill(self, cr, uid, ids, context=None):
        mfi_bill_obj = self.browse(cr, uid, ids[0], context=context)

        if mfi_bill_obj['state']=='pending':
            state='confirmed'
        else:
            state = 'submitted'

        for single_id in ids:
            confirm_query = "UPDATE mfi_invoice_submission SET state='{0}', confirmed_date='{1}' WHERE id={2}".format(
                state, str(fields.datetime.now()), single_id)
            cr.execute(confirm_query)
            cr.commit()

            confirm_query_line = "UPDATE mfi_invoice_line SET state='{0}' WHERE mfi_invoice_sibmission_id={1}".format(
                state, single_id)
            cr.execute(confirm_query_line)
            cr.commit()

        return True


    def cancel_mfi_bill(self, cr, uid, ids, context=None):

        for single_id in ids:
            confirm_query = "UPDATE mfi_invoice_submission SET state='cancel' WHERE id={0}".format(single_id)
            cr.execute(confirm_query)
            cr.commit()

            confirm_query_line = "UPDATE mfi_invoice_line SET state='cancel' WHERE mfi_invoice_sibmission_id={0}".format(
                 single_id)
            cr.execute(confirm_query_line)
            cr.commit()

        for i in self.browse(cr, uid, ids[0], context=context).mfi_invoice_line:
            inv_id = i['account_invoice_id'].id
            if inv_id:
                update_query = "UPDATE account_invoice SET bill_submission=FALSE WHERE id ={0}".format(inv_id)
                cr.execute(update_query)
                cr.commit()

        return True

    @api.model
    def create(self, vals):
        record = super(mfi_invoice_submission, self).create(vals)

        record.name = "MFI/Bill/00" + str(record.id)
        return record


class mfi_invoice_line(osv.osv):
    _name = "mfi.invoice.line"
    _description = "MFI Invoice Submission"

    _columns = {
        'mfi_invoice_sibmission_id':fields.many2one('mfi.invoice.submission', 'mfi invoice submission ID', required=False,
                                          ondelete='cascade', select=True, readonly=True),
        'order_id': fields.many2one('sale.order', 'Order'),
        'account_invoice_id': fields.many2one('account.invoice', 'Invoice'),
        'partner_id': fields.many2one('res.partner', 'Customer'),
        'mfi_customer_id': fields.many2one('mfi.customer', 'MFI Customer'),
        'invoice_amount': fields.float('Amount'),
        'state': fields.selection([
            ('pending', 'Pending Submission'),
            ('confirmed', 'Confirmed'),
            ('submitted', 'Submitted'),
            ('cancel', 'cancel')

        ], 'Status', readonly=True, copy=False, help="Gives the status of the MFI Invoice", select=True),
    }


class mfi_credit_history(osv.osv):
    _name = "mfi.credit.history"
    _description = "MFI Credit History"

    _columns = {
        'mfi_customer_id': fields.many2one('mfi.customer', 'MFI Customer'),
        'odoo_company_id': fields.many2one('res.partner', 'Odoo Customer'),
        'updated_by': fields.text('Updated By'),
        'amount': fields.float('Amount'),
        'balance': fields.float('Balance'),
        'credit_limit': fields.float('Credit Limit'),
        'available_credit': fields.float('Available Credit'),
        'type': fields.char('Type'),
        'purchase_order': fields.char('Purchase Order', help='Customer purchase ref'),
        'comment': fields.text('Comment'),
    }


class magento_mfi_mapping(osv.osv):
    _name = "magento.mfi.mapping"
    _description = "Magento MFI mapping"

    _columns = {
        'odoo_mfi_id': fields.many2one('mfi.customer', 'MFI Customer'),
        # 'mfi_history': fields.text('MFI history'),
        'magento_mfi_credit_id': fields.char('Magento MFI credit ID'),
        'created_by': fields.char('Created By'),
    }


class mfi_invoice_reattempt(osv.osv):
    _name = "mfi.invoice.reattempt"
    _description = "MFI Invoice Bill Reattemp "

    _columns = {
        'reattempted_date': fields.datetime('Reattempted Date'),
        'reattempted_by': fields.many2one('res.users', 'Reattempted By'),
        'submitted_to': fields.char('Submitted To'),
        'reattempted': fields.boolean('Reattempt'),
        'reason': fields.text('Reason', required=True),
    }

    def default_get(self, cr, uid, fields, context=None):
        if context is None:
            context = {}
        res = super(mfi_invoice_reattempt, self).default_get(cr, uid, fields, context=context)
        mfi_inv = self.pool.get('mfi.invoice.submission').browse(cr, uid, context['active_id'], context=context)

        res['submitted_to'] = str(mfi_inv.submitted_to)

        return res

    _defaults = {
        'reattempted_by':  lambda obj, cr, uid, context: uid,
        'reattempted_date':  fields.datetime.now(),
        'reattempted':True
    }


    def mfi_bill_reattempt(self, cr, uid, ids, context=None):
        id = context['active_id']
        reason = str(context['reason'])

        if reason != ' ':

                confirm_query = "UPDATE mfi_invoice_submission SET state='pending',reattempted=TRUE WHERE id={0}".format(id)
                cr.execute(confirm_query)
                cr.commit()

                confirm_query_line = "UPDATE mfi_invoice_line SET state='pending' WHERE mfi_invoice_sibmission_id={0}".format(
                    id)
                cr.execute(confirm_query_line)
                cr.commit()

        else:
            raise osv.except_osv(_('Warning!'),
                                 _('Please, Given actual reason.'))

        return True
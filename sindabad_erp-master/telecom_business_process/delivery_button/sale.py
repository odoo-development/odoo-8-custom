from datetime import date, datetime
from dateutil import relativedelta
import json
import time

import openerp
from openerp.osv import fields, osv
from openerp.tools.float_utils import float_compare, float_round
from openerp.tools.translate import _
from openerp.tools import DEFAULT_SERVER_DATETIME_FORMAT, DEFAULT_SERVER_DATE_FORMAT
from openerp.exceptions import Warning
from openerp import SUPERUSER_ID, api
import openerp.addons.decimal_precision as dp
from openerp.addons.procurement import procurement
import logging
from dateutil.relativedelta import relativedelta
from operator import itemgetter
from openerp.osv import fields, osv, expression
from openerp.tools.float_utils import float_round as round


class telecom_delivered_order_close(osv.osv):
    _name = "telecom.delivered.order.close"
    _description = "Telecom order close with Invoice"

    _columns = {

        'order_name': fields.char('Order Name'),
        'order_ref': fields.char('Order Ref.'),
    }

    def telecom_delivery_close(self, cr, uid, ids, context=None):

        close_order_tbl_ids = ids

        ids = context['active_ids']
        order_close_reason = context['order_name']
        so_obj = self.pool.get('sale.order')


        for so in so_obj.browse(cr, uid, ids, context=context):

            # Transfer delivery and generate Invoice
            for picking_id in so.picking_ids:
                picking=[picking_id.id]

                sale_object = so
                if str(picking_id.state) == 'draft' or str(picking_id.state) == 'confirmed':
                    #     # unreserve
                    self.pool.get('stock.picking').do_unreserve(cr, uid, int(picking_id.id), context=context)

                    # action cancel
                    self.pool.get('stock.picking').action_assign(cr, uid, int(picking_id.id), context=context)


                if str(picking_id.state) == 'waiting':

                    # Rereservation
                    self.pool.get('stock.picking').rereserve_pick(cr, uid, [picking_id.id], context=context)






                if 'cancel' not in [sale_object.state] and str(picking_id.state) == 'assigned':

                    context.update({

                        'active_model': 'stock.picking',
                        'active_ids': picking,
                        'active_id': len(picking) and picking[0] or False
                    })
                    created_id = self.pool['stock.transfer_details'].create(cr, uid, {'picking_id': len(picking) and picking[0] or False}, context)

                    if created_id:
                        created_id = self.pool.get('stock.transfer_details').browse(cr, uid, created_id, context=context)
                        if created_id.picking_id.state in ['assigned', 'partially_available']:
                            # raise Warning(_('You cannot transfer a picking in state \'%s\'.') % created_id.picking_id.state)

                            processed_ids = []
                            # Create new and update existing pack operations
                            for lstits in [created_id.item_ids, created_id.packop_ids]:
                                for prod in lstits:
                                    pack_datas = {
                                        'product_id': prod.product_id.id,
                                        'product_uom_id': prod.product_uom_id.id,
                                        'product_qty': prod.quantity,
                                        'package_id': prod.package_id.id,
                                        'lot_id': prod.lot_id.id,
                                        'location_id': prod.sourceloc_id.id,
                                        'location_dest_id': prod.destinationloc_id.id,
                                        'result_package_id': prod.result_package_id.id,
                                        'date': prod.date if prod.date else datetime.now(),
                                        'owner_id': prod.owner_id.id,
                                    }
                                    if prod.packop_id:
                                        prod.packop_id.with_context(no_recompute=True).write(pack_datas)
                                        processed_ids.append(prod.packop_id.id)
                                    else:
                                        pack_datas['picking_id'] = created_id.picking_id.id
                                        packop_id = created_id.env['stock.pack.operation'].create(pack_datas)
                                        processed_ids.append(packop_id.id)
                            # Delete the others
                            packops = created_id.env['stock.pack.operation'].search(
                                ['&', ('picking_id', '=', created_id.picking_id.id), '!', ('id', 'in', processed_ids)])
                            packops.unlink()


                            # Execute the transfer of the picking
                            created_id.picking_id.do_transfer()

                            # cr.execute("update stock_picking set system_transferred='True' where id=%s", ([picking_id[0]]))

                        if created_id.picking_id.invoice_state != 'invoiced':
                            # Now Create the Invoice for this picking
                            picking_pool = self.pool.get('stock.picking')
                            from datetime import date
                            context['date_inv'] = date.today()
                            context['inv_type'] = 'out_invoice'
                            active_ids = [picking_id.id]
                            res = picking_pool.action_invoice_create(cr, uid, active_ids,
                                                                     journal_id=1146,
                                                                     group=True,
                                                                     type='out_invoice',
                                                                     context=context)
                        # Action Done
                        so_obj.action_done(cr, uid, ids, context=context)

        # for so_id in ids:
        #     so_state_query = "UPDATE sale_order SET shipped=TRUE, waiting_availability=FALSE , partially_available=FALSE, partial_delivered=FALSE, order_close_reason='{0}' WHERE id='{1}'".format(order_close_reason, so_id)
        #
        #     cr.execute(so_state_query)
        #     cr.commit()
        #
        #     for so in so_obj.browse(cr, uid, so_id, context=context):
        #         order_name = str(so.name)
        #         order_ref = str(so.client_order_ref)
        #
        #         for co_id in close_order_tbl_ids:
        #             so_state_query = "UPDATE telecom_delivered_order_close SET order_name='{0}', order_ref='{1}' WHERE id='{2}'".format(
        #                 order_name, order_ref, co_id)
        #
        #             cr.execute(so_state_query)
        #             cr.commit()

        return True


class sale_order(osv.osv):
    _inherit = "sale.order"



    def telecom_business(self, cr, uid, ids, context=None):


        lose_order_tbl_ids = ids

        ids = context['active_ids']

        so_obj = self.pool.get('sale.order')


        for so in so_obj.browse(cr, uid, ids, context=context):

            # Transfer delivery and generate Invoice
            if str(so.state) == 'progress':
                print so.state
                print '*- *- *- '*25
                for picking_id in so.picking_ids:
                    picking=[picking_id.id]

                    sale_object = so
                    if str(picking_id.state) == 'draft' or str(picking_id.state) == 'confirmed':
                        #     # unreserve
                        self.pool.get('stock.picking').do_unreserve(cr, uid, int(picking_id.id), context=context)

                        # action cancel
                        self.pool.get('stock.picking').action_assign(cr, uid, int(picking_id.id), context=context)


                    if str(picking_id.state) == 'waiting':

                        # Rereservation
                        self.pool.get('stock.picking').rereserve_pick(cr, uid, [picking_id.id], context=context)






                    if 'cancel' not in [sale_object.state] and str(picking_id.state) == 'assigned':

                        context.update({

                            'active_model': 'stock.picking',
                            'active_ids': picking,
                            'active_id': len(picking) and picking[0] or False
                        })
                        created_id = self.pool['stock.transfer_details'].create(cr, uid, {'picking_id': len(picking) and picking[0] or False}, context)

                        if created_id:
                            created_id = self.pool.get('stock.transfer_details').browse(cr, uid, created_id, context=context)
                            if created_id.picking_id.state in ['assigned', 'partially_available']:
                                # raise Warning(_('You cannot transfer a picking in state \'%s\'.') % created_id.picking_id.state)

                                processed_ids = []
                                # Create new and update existing pack operations
                                for lstits in [created_id.item_ids, created_id.packop_ids]:
                                    for prod in lstits:
                                        pack_datas = {
                                            'product_id': prod.product_id.id,
                                            'product_uom_id': prod.product_uom_id.id,
                                            'product_qty': prod.quantity,
                                            'package_id': prod.package_id.id,
                                            'lot_id': prod.lot_id.id,
                                            'location_id': prod.sourceloc_id.id,
                                            'location_dest_id': prod.destinationloc_id.id,
                                            'result_package_id': prod.result_package_id.id,
                                            'date': prod.date if prod.date else datetime.now(),
                                            'owner_id': prod.owner_id.id,
                                        }
                                        if prod.packop_id:
                                            prod.packop_id.with_context(no_recompute=True).write(pack_datas)
                                            processed_ids.append(prod.packop_id.id)
                                        else:
                                            pack_datas['picking_id'] = created_id.picking_id.id
                                            packop_id = created_id.env['stock.pack.operation'].create(pack_datas)
                                            processed_ids.append(packop_id.id)
                                # Delete the others
                                packops = created_id.env['stock.pack.operation'].search(
                                    ['&', ('picking_id', '=', created_id.picking_id.id), '!', ('id', 'in', processed_ids)])
                                packops.unlink()


                                # Execute the transfer of the picking
                                created_id.picking_id.do_transfer()

                                # cr.execute("update stock_picking set system_transferred='True' where id=%s", ([picking_id[0]]))
                            res=None
                            if created_id.picking_id.invoice_state != 'invoiced':
                                # Now Create the Invoice for this picking
                                picking_pool = self.pool.get('stock.picking')
                                from datetime import date
                                context['date_inv'] = date.today()
                                context['inv_type'] = 'out_invoice'
                                active_ids = [picking_id.id]
                                res = picking_pool.action_invoice_create(cr, uid, active_ids,
                                                                         journal_id=1146,
                                                                         group=True,
                                                                         type='out_invoice',
                                                                         context=context)
                            # Action Done
                            # if res is not None:
                            #
                            #     so_obj.action_done(cr, uid, ids, context=context)


        return True

{
    'name': 'Telecom Business Process',
    'version': '1.0',
    'category': 'Sale',
    'author': 'Rockibul Alam',
    'summary': 'Telecom Business Process',
    'description': 'Telecom Business Process',
    'depends': ['base', 'sale','stock','account','odoo_magento_connect'],
    'data': [
        'security/telecom_orders_security.xml',
        'security/ir.model.access.csv',
        'views/pending_telecom_orders.xml',
        'views/confirmed_telecom_orders.xml',
        'views/security_modify_menus.xml',

        'delivery_button/order_complete.xml',
        'delivery_button/sale_close.xml',
        'delivery_button/mutlple_selection.xml',
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}

import itertools
from lxml import etree

from openerp import models, api, _
from openerp.exceptions import except_orm, Warning, RedirectWarning
from openerp.tools import float_compare
import openerp.addons.decimal_precision as dp
from openerp.osv import fields, osv


class account_invoice(models.Model):
    _inherit = "account.invoice"

    def active_delivery_dispatch(self, cr, uid, ids, context=None):

        ids = context['active_ids']

        ai_obj = self.browse(cr, uid, ids, context=context)

        for acc_in in ai_obj:

            delivery_dispatch_query = "UPDATE sale_order SET delivery_dispatch=TRUE WHERE client_order_ref='{0}'".format(str(acc_in.name))

            cr.execute(delivery_dispatch_query)
            cr.commit()

        return True


class sale_order(osv.osv):
    _inherit = 'sale.order'

    def _get_shipped(self, cr, uid, ids, name, args, context=None):
        res = {}
        for sale in self.browse(cr, uid, ids, context=context):
            group = sale.procurement_group_id
            if group:
                res[sale.id] = all([proc.state in ['cancel', 'done'] for proc in group.procurement_ids])
            else:
                res[sale.id] = False
        return res


    def _get_orders_procurements(self, cr, uid, ids, context=None):
        res = set()
        for proc in self.pool.get('procurement.order').browse(cr, uid, ids, context=context):
            if proc.state =='done' and proc.sale_line_id:
                res.add(proc.sale_line_id.order_id.id)
        return list(res)

    _columns = {
        'delivery_dispatch': fields.boolean('Delivered'),
        'invoiced_dispatch': fields.boolean('Dispatched'),

        'shipped': fields.function(_get_shipped, string='Order Invoiced', type='boolean', store={
            'procurement.order': (_get_orders_procurements, ['state'], 10)
        }),
    }

{
    'name': 'Order Delivery Dispatch',
    'version': '1.0',
    'category': 'Order Delivery Dispatch',
    'author': 'Shahjalal',
    'summary': 'Sales Order, Delivery, Dispatch',
    'description': 'Order Delivery Dispatch',
    'depends': ['sale', 'account', 'delivery_customer_name'],
    'data': [
        'account_invoice.xml',
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}

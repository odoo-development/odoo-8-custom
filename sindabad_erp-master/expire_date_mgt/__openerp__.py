{
    'name' : 'Product Expire Date with Bucket',
    'version' : '0.1',
    'author' : 'OpenERP S.A.',
    'sequence': 110,
    'category': 'others',
    'website' : 'https://www.sindabad.com',
    'summary' : 'This is a test',
    'description' : "dsdf ",
    'depends' : [
        'stock','wms_inbound'
    ],
    'data' : [
        'expiry_date_calculation.xml',
        'quant_menu.xml',
        'exp_quantity_send_mail.xml',
        'views/report_roqn.xml'




    ],

    'demo': [],

    'installable' : True,
    'application' : True,
}

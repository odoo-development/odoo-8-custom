from datetime import date, datetime
from dateutil import relativedelta
import json
import time

from openerp.osv import fields, osv
from openerp.tools.float_utils import float_compare, float_round
from openerp.tools.translate import _
from openerp.tools import DEFAULT_SERVER_DATETIME_FORMAT, DEFAULT_SERVER_DATE_FORMAT
from openerp.exceptions import Warning
from openerp import SUPERUSER_ID, api
import openerp.addons.decimal_precision as dp
from openerp.addons.procurement import procurement
import logging


_logger = logging.getLogger(__name__)


class stock_quant(osv.osv):
    """
    Quants are the smallest unit of stock physical instances
    """
    _inherit = "stock.quant"
    _description = "Stock Quantity Update"


    _columns = {
        'product_exp_date': fields.date('Product Expiry Date'),
    }


class quality_control_line(osv.osv):
    _inherit="quality.control.line"


    _columns = {
        'product_exp_date': fields.date('Product Expiry Date'),
    }


class accepted_quality_control(osv.osv):
    _inherit="accepted.quality.control.line"

    _columns = {
        'product_exp_date': fields.date('Product Expiry Date'),
    }


class putaway_process_details_view(osv.osv):
    _inherit="putaway.process.details.view"

    _columns = {
        'product_exp_date': fields.date('Product Expiry Date'),
    }











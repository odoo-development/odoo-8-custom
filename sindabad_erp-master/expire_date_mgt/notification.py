import math
import re
import time

from openerp import api, tools, SUPERUSER_ID
from openerp.osv import osv, fields, expression
from openerp.tools.translate import _
from openerp.tools import DEFAULT_SERVER_DATETIME_FORMAT
import psycopg2

import openerp.addons.decimal_precision as dp
from openerp.tools.float_utils import float_round, float_compare

from openerp.report import report_sxw
from openerp.osv import fields, osv
from datetime import datetime, timedelta


class ExpireDateNotification(report_sxw.rml_parse):

    def __init__(self, cr, uid, name, context):
        super(ExpireDateNotification, self).__init__(cr, uid, name, context)

        self.context = context

    def set_context(self, objects, data, ids, report_type=None):

        ids = self.context.get('active_ids')


        return super(ExpireDateNotification, self).set_context(objects, data, ids, report_type)


class report_expiredatenotification(osv.AbstractModel):
    _name = 'report.expire_date_mgt.report_quant'
    _inherit = 'report.abstract_report'
    _template = 'expire_date_mgt.report_quant'
    _wrapped_report_class = ExpireDateNotification

import time
from openerp.osv import osv
from openerp.report import report_sxw
from openerp.osv import fields, osv


class stock_quant(osv.osv):
    _inherit = 'stock.quant'

    def send_expired_norification_mail(self, cr, uid, ids, context=None):
        mail_text = "test mail- reorder level"
        email = "shahjalal.hossain@sindabad.com"

        e_log_obj = self.pool.get('send.email.reorder.qty.level.log')

        email_data = {
            'mail_text': mail_text,
            'model': "product.product",
            'po_id': 10,
            'email': email
        }

        email_log_id = e_log_obj.create(cr, 1, email_data, context=context)

        # send email
        email_template_obj = self.pool.get('email.template')
        template_ids = email_template_obj.search(cr, 1, [('name', '=', 'Expired Quantity - Send by Email')])

        mail_status = email_template_obj.send_mail(cr, 1, template_ids[0], email_log_id, force_send=True)

        return True


class send_email_reorder_qty_level_log(osv.osv):
    _name = "send.email.reorder.qty.level.log"
    _description = "Send email reorder qty level log"

    _columns = {
        'mail_text': fields.text('Email text'),
        'model': fields.char("Model"),
        'po_id': fields.integer("Model Obj ID"),
        'email': fields.char("Email"),
    }


class reoqn_quantity(report_sxw.rml_parse):
    def __init__(self, cr, uid, name, context):
        super(reoqn_quantity, self).__init__(cr, uid, name, context)
        self.localcontext.update({
            'get_reorder': self._get_reorder_qty,
        })

    def _get_reorder_qty(self, obj):

        product_obj = self.pool.get('product.product')

        reo_ps_lst = product_obj.search(self.cr, self.uid, [('reorder_qty_level', '>', 0)], order="categ_id desc")

        # ('reorder_qty_level', '>', 'qty_available')
        reorder_qty_level_ids = list()

        for rel_product in product_obj.browse(self.cr, self.uid, reo_ps_lst):

            if rel_product.reorder_qty_level >= rel_product.qty_available:
                reorder_qty_level_ids.append(rel_product.id)

        all_products = product_obj.browse(self.cr, self.uid, reorder_qty_level_ids)

        # import pdb;pdb.set_trace()



        # all_products = self.pool['product.product'].browse(self.cr, self.uid,self.pool['product.product'].search(self.cr, self.uid, []))

        return all_products if all_products else '-'


class report_roqn(osv.AbstractModel):
    _name = 'report.re_order_level_notifications.report_roqn'
    _inherit = 'report.abstract_report'
    _template = 're_order_level_notifications.report_roqn'
    _wrapped_report_class = reoqn_quantity



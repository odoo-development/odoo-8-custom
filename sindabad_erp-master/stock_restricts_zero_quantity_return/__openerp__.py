{
    'name': 'Stock Restricts Zero Quantity Return',
    'version': '1.0',
    'category': 'Stock Management',
    'author': 'Shahjalal',
    'summary': 'Inventory, Logistic, Storage',
    'description': 'Stock Restricts Zero Quantity Return',
    'depends': ['stock'],
    'installable': True,
    'data': [
        'wizard/stock_return_picking_view.xml',
    ],
    'application': True,
    'auto_install': False,
}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:

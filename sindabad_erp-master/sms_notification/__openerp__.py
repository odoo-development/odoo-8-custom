# -*- coding: utf-8 -*-
##############################################################################
#
# Odoo, Open Source Management Solution
# Copyright (C) 2015 Webkul Software Pvt. Ltd.
# Author :
# www.webkul.com
#
##############################################################################


{
    'name': 'SMS Notification',
    'version': '1.0',
'summary':'This module allows you to send sms notification to your users for the order confirmation, delivery, refund, etc.',
    'category': 'Marketing',
    'description': """
This module is used to send the sms to the registered mobile numbers.
======================================================================================

Also allows users to use one of many sms gateway.
    """,
    "sequence": 1,
    'images':['static/description/Banner.png'],
    "author": "Webkul Software Pvt. Ltd.",
    "website": "http://www.webkul.com",
    "version": '1.0',
    'depends': ['base_setup',
                'mail'
               ],
    'data': [
        'views/configure_gateway_view.xml',
        'views/sms_sms_view.xml',
        'views/res_config_view.xml',
        'views/res_users_view.xml',
        'views/sms_cron_view.xml',
        'security/ir.model.access.csv'
        ],
    "application": True,
    'installable': True,
    'auto_install': False,
    "price": 49,
    "currency": 'EUR',
}

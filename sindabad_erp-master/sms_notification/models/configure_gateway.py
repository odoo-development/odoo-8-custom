# -*- coding: utf-8 -*-
##############################################################################
#
# Odoo, Open Source Management Solution
# Copyright (C) 2013 webkul
# Author :
# www.webkul.com
#
##############################################################################

from openerp import models, fields, api, _


class sms_mail_server(models.Model):
	"""This class is design for configuring the sms mail server."""

	_name = "sms.mail.server"
	_description = "Module for sms mail server configuration."
	_rec_name = 'description'

	@api.model
	def get_reference_type(self):
		return []

	@api.model
	def _get_mob_no(self):
		user_obj = self.env['res.users'].browse([self._uid])
		return user_obj.mob_number

	@api.model
	def _set_mob_no(self):
		user_obj = self.env['res.users'].browse([self._uid])
		user_obj.write({'mob_number':self.user_mobile_no})

	description = fields.Char(string="Description", required=True)
	sequence = fields.Integer(string='Priority', help="Default Priority will be 0.")
	sms_debug = fields.Boolean(string="Debugging", help="If enabled, the error message of sms gateway will be written to the log file")
	user_mobile_no = fields.Char(string="Mobile No.", help="Ten digit mobile number with country code(e.g +91)", default=_get_mob_no, inverse='_set_mob_no')

	gateway = fields.Selection('get_reference_type')

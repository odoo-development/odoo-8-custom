# -*- coding: utf-8 -*-
##############################################################################
#
# Odoo, Open Source Management Solution
# Copyright (C) 2016 webkul
# Author : www.webkul.com
#
##############################################################################

from openerp.osv import fields, osv
from openerp import SUPERUSER_ID, api
import re
import operator
from openerp.exceptions import except_orm, Warning, RedirectWarning


class sms_sms(osv.osv):
	"""SMS sending using SMS mail server."""
	
	_name = "sms.sms"
	_description = "Module for Sms."

	
	_columns = {
		'to' : fields.char("To:",required=True),
		'state':fields.selection([
								('new','Outgoing'),	
								('sent','Sent'),
								('delivered','Delivered'),	
								('undelivered','Undelivered'),
								],"Sms Report"),
		'msg' : fields.text("Message:"),
		'auto_delete': fields.boolean("Auto Delete", help="Permanently delete this SMS after sending it,to save space.")		
	}

	_defaults = {
		'state' : 'new',
		'auto_delete' : True
	}


	def send_sms_via_gateway(self, cr, uid, ids, body_sms, mob_no, context=None):
		if context is None:
			context = {}
		user_obj = self.pool['res.users'].browse(cr, uid, uid, context=context)
		if user_obj.notify_sms:
			gateway_id = self.pool["sms.mail.server"].search(cr, uid, [], order='sequence asc', limit=1, context=context)
			if gateway_id:
				return gateway_id[0]
			else:
				return 	False	
		else:
			return False

	def send_now(self, cr, uid, ids, context=None):
		if context is None:
			context = {}
		for sms_id in ids:
			obj = self.pool['sms.sms'].browse(cr, uid, sms_id, context=context)
			body_sms = obj.msg
			mob_no = [obj.to]
			context['action'] = 'send'		
			self.send_sms_via_gateway(cr, uid, sms_id, body_sms, mob_no, context=context)
	
	def retry(self, cr, uid, ids, context=None):
		if context is None:
			context = {}
		for sms_id in ids:
			obj = self.pool['sms.sms'].browse(cr, uid, sms_id, context=context)
			body_sms = obj.msg
			mob_no = [obj.to]
			context['action'] = 'retry'
			self.send_sms_via_gateway(cr, uid, sms_id, body_sms, mob_no, context=context)

	def cron_function_for_sms(self, cr, uid, context=None):
		pass
		

		


class mail_mail(osv.osv):
	"""SMS sending using SMS mail server."""

	_inherit = "mail.mail"

	def send(self, cr, uid, ids, auto_commit=False, raise_exception=False, context=None):
		if context == None:
			context = {}		
		for mail in self.browse(cr, SUPERUSER_ID, ids, context=context):
			body_sms = re.sub("<.*?>", " ", mail.body_html)
			mob_no = []
			for recipient_id in mail.recipient_ids:				
				if recipient_id.mobile:
					mob_no.append(recipient_id.mobile)
			if mob_no:
				self.pool["sms.sms"].send_sms_via_gateway(cr, uid, ids, body_sms, mob_no, context=context)
				
		super(mail_mail,self).send(cr, uid, ids, auto_commit, raise_exception, context=context)

		return True



# -*- coding: utf-8 -*-
##############################################################################
#
# Odoo, Open Source Management Solution
# Copyright (C) 2013 webkul
# Author :
# www.webkul.com
#
##############################################################################

from openerp import models, fields, api, _



class base_config_settings(models.Model):
	_inherit = "base.config.settings"
	_description = "Base config for Twilio "

	def _check_twilio(self):
		result = self.env['ir.module.module'].search([('name','=','twilio_gateway')])
		if result:
			return True
		else :
			return False

	def _check_plivo(self):
		result = self.env['ir.module.module'].search([('name','=','plivo_gateway')])
		if result:
			return True
		else :
			return False

	def _check_ssl_sms(self):
		result = self.env['ir.module.module'].search([('name','=','ssl_sms_gateway')])
		if result:
			return True
		else :
			return False

	module_ssl_sms_gateway = fields.Boolean(string='Install SSL SMS Gateway', help='It will Install ssl sms gateway automatically.')
	is_ssl_in_addon=fields.Boolean(default=_check_ssl_sms)

	module_twilio_gateway = fields.Boolean(string='Install Twilio SMS Gateway', help='It will Install twilio sms gateway automatically.')
	is_twilio_in_addon=fields.Boolean(default=_check_twilio)

	module_plivo_gateway = fields.Boolean(string='Install Plivo SMS Gateway', help='It will Install plivo sms gateway automatically.')
	is_plivo_in_addon=fields.Boolean(default=_check_plivo)

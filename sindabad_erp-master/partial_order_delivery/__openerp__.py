# Mufti Muntasir Ahmed

{
    'name': 'Partial SO Delivery Order',
    'version': '1.0',
    'category': 'Stock',
    'summary': """Create Partial Delivery Challan For Order """,
    'description': """Create Partial Delivery Challan For Order """,
    'author': 'Mufti Muntasir Ahmed',
    'website': 'http://www.sindabad.com',
    'depends': ['odoo_magento_connect', 'stock'],
    'init_xml': [],
    'update_xml': [

        'security/partial_order_delivery_security.xml',
        'security/ir.model.access.csv',
        'wizard/partial_delivery_create.xml',
        'sale_view.xml',

         
    ],
    'demo_xml': [],
    'installable': True,
    'active': False,

}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:

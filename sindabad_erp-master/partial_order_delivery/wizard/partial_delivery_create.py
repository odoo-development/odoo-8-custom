##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import logging
from datetime import datetime
from dateutil.relativedelta import relativedelta
from operator import itemgetter
import time

import openerp
from openerp import SUPERUSER_ID, api
from openerp import tools
from openerp.osv import fields, osv, expression
from openerp.tools.float_utils import float_compare, float_round
from openerp.tools.translate import _
from openerp.tools.float_utils import float_round as round

from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT, DATETIME_FORMATS_MAP
from openerp.tools.float_utils import float_compare
import openerp.addons.decimal_precision as dp
from openerp.addons.procurement import procurement

_logger = logging.getLogger(__name__)


class partial_delivery_create(osv.osv_memory):
    _name = "partial.delivery.create"
    _description = "Partial Delivery Creation"
    
    
    def _get_picking_in(self, cr, uid, context=None):
        obj_data = self.pool.get('ir.model.data')
        type_obj = self.pool.get('stock.picking.type')
        user_obj = self.pool.get('res.users')
        company_id = user_obj.browse(cr, uid, uid, context=context).company_id.id
        types = type_obj.search(cr, uid, [('code', '=', 'incoming'), ('warehouse_id.company_id', '=', company_id)], context=context)
        if not types:
            types = type_obj.search(cr, uid, [('code', '=', 'incoming'), ('warehouse_id', '=', False)], context=context)
            if not types:
                raise osv.except_osv(_('Error!'), _("Make sure you have at least an incoming picking type defined"))
        return types[0]
    
    
    def get_min_max_date(self, cr, uid, ids, field_name, arg, context=None):
        """ Finds minimum and maximum dates for picking.
        @return: Dictionary of values
        """
        res = {}
        for id in ids:
            res[id] = {'min_date': False, 'max_date': False, 'priority': '1'}
        if not ids:
            return res
        cr.execute("""select
                picking_id,
                min(date_expected),
                max(date_expected),
                max(priority)
            from
                stock_move
            where
                picking_id IN %s
            group by
                picking_id""", (tuple(ids),))
        for pick, dt1, dt2, prio in cr.fetchall():
            res[pick]['min_date'] = dt1
            res[pick]['max_date'] = dt2
            res[pick]['priority'] = prio
        return res

    def _set_min_date(self, cr, uid, id, field, value, arg, context=None):
        move_obj = self.pool.get("stock.move")
        if value:
            move_ids = [move.id for move in self.browse(cr, uid, id, context=context).move_lines]
            move_obj.write(cr, uid, move_ids, {'date_expected': value}, context=context)

    def _get_pickings_dates_priority(self, cr, uid, ids, context=None):
        res = set()
        for move in self.browse(cr, uid, ids, context=context):
            if move.picking_id and (not (move.picking_id.min_date < move.date_expected < move.picking_id.max_date) or move.priority > move.picking_id.priority):
                res.add(move.picking_id.id)
        return list(res)

    def _state_get(self, cr, uid, ids, field_name, arg, context=None):
        '''The state of a picking depends on the state of its related stock.move
            draft: the picking has no line or any one of the lines is draft
            done, draft, cancel: all lines are done / draft / cancel
            confirmed, waiting, assigned, partially_available depends on move_type (all at once or partial)
        '''
        res = {}
        for pick in self.browse(cr, uid, ids, context=context):
            if (not pick.move_lines) or any([x.state == 'draft' for x in pick.move_lines]):
                res[pick.id] = 'draft'
                continue
            if all([x.state == 'cancel' for x in pick.move_lines]):
                res[pick.id] = 'cancel'
                continue
            if all([x.state in ('cancel', 'done') for x in pick.move_lines]):
                res[pick.id] = 'done'
                continue

            order = {'confirmed': 0, 'waiting': 1, 'assigned': 2}
            order_inv = {0: 'confirmed', 1: 'waiting', 2: 'assigned'}
            lst = [order[x.state] for x in pick.move_lines if x.state not in ('cancel', 'done')]
            if pick.move_type == 'one':
                res[pick.id] = order_inv[min(lst)]
            else:
                #we are in the case of partial delivery, so if all move are assigned, picking
                #should be assign too, else if one of the move is assigned, or partially available, picking should be
                #in partially available state, otherwise, picking is in waiting or confirmed state
                res[pick.id] = order_inv[max(lst)]
                if not all(x == 2 for x in lst):
                    if any(x == 2 for x in lst):
                        res[pick.id] = 'partially_available'
                    else:
                        #if all moves aren't assigned, check if we have one product partially available
                        for move in pick.move_lines:
                            if move.partially_available:
                                res[pick.id] = 'partially_available'
                                break
        return res

    def _get_pickings(self, cr, uid, ids, context=None):
        res = set()
        for move in self.browse(cr, uid, ids, context=context):
            if move.picking_id:
                res.add(move.picking_id.id)
        return list(res)

    def _get_pack_operation_exist(self, cr, uid, ids, field_name, arg, context=None):
        res = {}
        for pick in self.browse(cr, uid, ids, context=context):
            res[pick.id] = False
            if pick.pack_operation_ids:
                res[pick.id] = True
        return res

    def _set_priority(self, cr, uid, id, field, value, arg, context=None):
        move_obj = self.pool.get("stock.move")
        if value:
            move_ids = [move.id for move in self.browse(cr, uid, id, context=context).move_lines]
            move_obj.write(cr, uid, move_ids, {'priority': value}, context=context)

    def _get_quant_reserved_exist(self, cr, uid, ids, field_name, arg, context=None):
        res = {}
        for pick in self.browse(cr, uid, ids, context=context):
            res[pick.id] = False
            for move in pick.move_lines:
                if move.reserved_quant_ids:
                    res[pick.id] = True
                    continue
        return res

    def onchange_partner_id(self, cr, uid, ids, partner_id, context=None):
        partner = self.pool.get('res.partner')
        if not partner_id:
            return {}
        supplier = partner.browse(cr, uid, partner_id, context=context)
        return {'value': {
            'pricelist_id': supplier.property_product_pricelist_purchase.id,
#            'fiscal_position': supplier.property_account_position and supplier.property_account_position.id or False,
#            'payment_term_id': supplier.property_supplier_payment_term.id or False,
            }}

    _columns = {

        'partner_id': fields.many2one('res.partner', 'Partner',
                                      states={'done': [('readonly', True)], 'cancel': [('readonly', True)]}),
        'state': fields.char('Status'),
        'date': fields.datetime('Creation Date', help="Creation Date, usually the time of the order", select=True),

        'min_date': fields.datetime('Scheduled Date', help="Creation Date, usually the time of the order", select=True),
        'origin': fields.char('Source Document'),

        'magento_shipment': fields.char('Magento Shipment',
                                        help="Contains Magento Order Shipment Number (eg. 300000008)"),

        'product_id': fields.related('move_lines', 'product_id', type='many2one', relation='product.product',
                                     string='Product'),
        'move_lines': fields.one2many('partial.delivery.create.line', 'move_line_id', 'Order Lines', required=True),
        # --------------------------------------------




    }
    

    
    def default_get(self, cr, uid, fields, context=None):
        product_pricelist = self.pool.get('product.pricelist')
        if context is None:
            context = {}
        res = super(partial_delivery_create, self).default_get(cr, uid, fields, context=context)
        # order = self.pool.get('sale.order').browse(cr, uid, context['active_id'], context=context)
        items = []
        date_order = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        picking_data = self.pool.get('stock.picking').browse(cr, uid, context['active_id'], context=context)

        for line in picking_data.move_lines:
            _logger.info('-------line-----------%r',line)
            
            # - determine price_unit and taxes_id

            price = 0.0
            if 'pricelist_id' in res:
                if res['pricelist_id']:
                    date_order_str = datetime.strptime(date_order, DEFAULT_SERVER_DATETIME_FORMAT).strftime(DEFAULT_SERVER_DATE_FORMAT)
                    price = product_pricelist.price_get(cr, uid, [res['pricelist_id']],
                            line.product_id.id, line.product_uom_qty or 1.0, line.partner_id.id or False, {'uom': line.product_id.uom_po_id.id, 'date': date_order_str})[res['pricelist_id']]
                else:
    #                    price = product.standard_price
                    price = line.product_id.standard_price
            else:
                price = line.product_id.standard_price

            item = {
                'product_id': line.product_id.id,
                'qty': line.product_uom_qty,
                'product_uom' : line.product_id.uom_po_id.id,
                'price': price,
                'subtotal': price * line.product_uom_qty,
                'location_dest_id': line.location_dest_id.id,

                }

            if line.product_id:
                if line.state == "assigned":
                    item['state'] = 'Available'
                    items.append(item)

        # res['client_order_ref']=order.client_order_ref

        res['partner_id'] = picking_data[0].partner_id.id
        res['date'] = picking_data[0].date
        res['origin'] = picking_data[0].origin
        res['min_date'] = picking_data[0].min_date
        res['magento_shipment'] = picking_data[0].magento_shipment

        # res.update(line_ids=items)
        res.update(move_lines=items)
        # import pdb;pdb.set_trace()

        return res

    def partial_order_delivery_challan(self, cr, uid, data, context):

        picking_ids = context['active_ids']  # : [1584]


        saved_data = self.pool.get('partial.delivery.create').browse(cr, uid, data, context=context)

        product_list=[]

        for items in saved_data.move_lines:
            product_list.append(items.product_id)

        stock_move_ids=[]



        picking_data = self.pool.get('stock.picking').browse(cr, uid, context['active_id'], context=context)

        avail_move_line_ids = list()
        for line in picking_data.move_lines:
            if line.state == "assigned":
                avail_move_line_ids.append(line.id)

                if line.product_id in product_list:
                    stock_move_ids.append(line.id)





        # save new stock.picking
        picking_data.state = "assigned"
        # new_picking = self.pool.get('stock.picking').create({'origin': picking_data.origin, 'state': picking_data.state})
        if picking_data.carrier_id:
            carrier_id=picking_data.carrier_id.id
        else:
            carrier_id=None

        new_stock_picking = {
            'origin': picking_data.origin,
            'create_date': picking_data.create_date,
            'date_done': picking_data.date_done,
            'write_uid': picking_data.write_uid,
            'partner_id': picking_data.partner_id.id,
            'priority': picking_data.priority,
            'backorder_id': picking_data.backorder_id.id,
            'picking_type_id': picking_data.picking_type_id.id,
            # 'move_id': picking_data.move_id,
            'message_last_post': picking_data.message_last_post,
            'company_id': picking_data.company_id.id,
            'note': picking_data.note,
            'state': 'confirmed',
            'owner_id': picking_data.owner_id.id,
            'create_uid': picking_data.create_uid,
            'min_date': picking_data.min_date,
            'write_date': picking_data.write_date,
            'date': picking_data.date,
            # 'name': picking_data.name,
            'recompute_pack_op': picking_data.recompute_pack_op,
            'max_date': picking_data.max_date,
            'group_id': picking_data.group_id.id,
            # 'invoice_state': picking_data.invoice_state,
            # 'receiption_to_invoice': picking_data.receiption_to_invoice,
            'carrier_tracking_ref': picking_data.carrier_tracking_ref,
            'number_of_packages': picking_data.number_of_packages,
            'weight': picking_data.weight,
            'carrier_id': carrier_id,
            'weight_uom_id': picking_data.weight_uom_id.id,
            'weight_net': picking_data.weight_net,
            'volume': picking_data.volume,
            'magento_shipment': picking_data.magento_shipment,
            'carrier_code': picking_data.carrier_code,
            'packed': picking_data.packed,
            'picked': picking_data.picked,
            'invoice_state': '2binvoiced'

        }


        
        # import pdb
        # pdb.set_trace()
        # update new stock_move.picking_id with new saved stock.picking id
        # import pdb
        # pdb.set_trace()
        new_stock_id = self.pool.get('stock.picking').create(cr, uid, new_stock_picking, context=context)

        if new_stock_id:

            for items in stock_move_ids:
                cr.execute("update stock_move set picking_id=%s where id=%s", ([new_stock_id, items]))
                cr.execute("update stock_picking set state='assigned' where id=%s", ([new_stock_id]))
                cr.execute("update stock_picking set invoice_state='2binvoiced' where id=%s", ([new_stock_id]))






        return True

    # {
    #     'name': '',
    #     'priority':'',
    #     'create_date':'',
    #     'date': '',
    #     'date_expected':'',
    #
    #     'product_id': '',
    #     'product_qty': '',
    #     'product_uom_qty': '',
    #     'product_uom': '',
    #     'product_uos_qty': '',
    #     'product_uos': '',
    #     'product_tmpl_id': '',
    #
    #     'product_packaging': '',
    #
    #     'location_id': '',
    #     'location_dest_id': '',
    #     'partner_id': '',
    #     'move_dest_id':'',
    #     'move_orig_ids': '',
    #
    #     'picking_id': '',
    #     'state': '',
    #     'partially_available': '',
    #     'price_unit': '',
    # # as it's a technical field, we intentionally don't provide the digits attribute
    #
    #     'company_id': '',
    #     'split_from': '',
    #     'backorder_id': '',
    #     'origin':'',
    #     'procure_method': '',
    #     # used for colors in tree views:
    #     'scrapped': '',
    #     'quant_ids': '',
    #     'reserved_quant_ids': '',
    #     'linked_move_operation_ids': '',
    #     'remaining_qty': '',
    #     'procurement_id': '',
    #     'group_id': '',
    #     'rule_id': '',
    #     'push_rule_id': '',
    #     'propagate': '',
    #     'picking_type_id': '',
    #     'inventory_id': '',
    #     'lot_ids': '',
    #     'origin_returned_move_id': '',
    #     'returned_move_ids': '',
    #     'reserved_availability':'',
    #     'availability': '',
    #     'string_availability_info': '',
    #     'restrict_lot_id': '',
    #     'restrict_partner_id': '',
    #     'route_ids': '',
    #     'warehouse_id': '',
    # }









































class partial_delivery_create_line(osv.osv_memory):
    _name = "partial.delivery.create.line"
    _description = "Back to Back Order"
    
    
    def _amount_line(self, cr, uid, ids, prop, arg, context=None):
        res = {}
        cur_obj=self.pool.get('res.currency')
        tax_obj = self.pool.get('account.tax')
        for line in self.browse(cr, uid, ids, context=context):
            taxes = tax_obj.compute_all(cr, uid, line.taxes_id, line.price, line.qty, line.product_id, line.back_order_id.partner_id)
            cur = line.back_order_id.pricelist_id.currency_id
            res[line.id] = cur_obj.round(cr, uid, cur, taxes['total'])
        return res
    
    def _get_string_qty_information(self, cr, uid, ids, field_name, args, context=None):
        settings_obj = self.pool.get('stock.config.settings')
        uom_obj = self.pool.get('product.uom')
        res = dict.fromkeys(ids, '')
        precision = self.pool['decimal.precision'].precision_get(cr, uid, 'Product Unit of Measure')
        for move in self.browse(cr, uid, ids, context=context):
            if move.state in ('draft', 'done', 'cancel') or move.location_id.usage != 'internal':
                res[move.id] = ''  # 'not applicable' or 'n/a' could work too
                continue
            total_available = min(move.product_qty, move.reserved_availability + move.availability)
            total_available = uom_obj._compute_qty_obj(cr, uid, move.product_id.uom_id, total_available, move.product_uom, round=False, context=context)
            total_available = float_round(total_available, precision_digits=precision)
            info = str(total_available)
            #look in the settings if we need to display the UoM name or not
            config_ids = settings_obj.search(cr, uid, [], limit=1, order='id DESC', context=context)
            if config_ids:
                stock_settings = settings_obj.browse(cr, uid, config_ids[0], context=context)
                if stock_settings.group_uom:
                    info += ' ' + move.product_uom.name
            if move.reserved_availability:
                if move.reserved_availability != total_available:
                    #some of the available quantity is assigned and some are available but not reserved
                    reserved_available = uom_obj._compute_qty_obj(cr, uid, move.product_id.uom_id, move.reserved_availability, move.product_uom, round=False, context=context)
                    reserved_available = float_round(reserved_available, precision_digits=precision)
                    info += _(' (%s reserved)') % str(reserved_available)
                else:
                    #all available quantity is assigned
                    info += _(' (reserved)')
            res[move.id] = info
        return res
    
    # def _get_uom_id(self, cr, uid, context=None):
    #     try:
    #         proxy = self.pool.get('ir.model.data')
    #         result = proxy.get_object_reference(cr, uid, 'product', 'product_uom_unit')
    #         return result[1]
    #     except Exception, ex:
    #         return False
    
    _columns = {
        
#        'location_destination_id': fields.many2one('stock.location', 'Stock Destination Location'),
#        'location_id': fields.many2one('stock.location', 'Stock Source Location'),
        'move_line_id': fields.many2one('partial.delivery.create', 'Move Line', required=True),
        'product_id': fields.many2one('product.product', 'Product'),
        'partial_delivery_create_id': fields.many2one('partial.delivery.create', 'Back Order'),
        'qty': fields.float('Quantity'),
        'price': fields.float('Unit Price'),
        'subtotal': fields.function(_amount_line, string='Subtotal', digits_compute= dp.get_precision('Account')),
        'taxes_id': fields.many2many('account.tax', 'purchase_order_taxe', 'ord_id', 'tax_id', 'Taxes'),
        'product_uom': fields.many2one('product.uom', 'Product Unit of Measure'),
        'location_dest_id': fields.many2one('stock.location', 'Destination Location',
                                            states={'done': [('readonly', True)]}, select=True,
                                            auto_join=True,
                                            help="Location where the system will stock the finished products."),
        'string_availability_info': fields.function(_get_string_qty_information, type='text', string='Availability',
                                                    readonly=True,
                                                    help='Show various information on stock availability for this move'),
        'state': fields.char('Status', readonly=True),
        
           
    }
    
    
    # _defaults = {
    #     'product_uom' : _get_uom_id,
    # }

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:

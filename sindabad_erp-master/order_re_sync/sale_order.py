import new

from openerp.osv import fields, osv
from openerp import api
from openerp.tools.translate import _

import json
import requests

from ..odoo_to_magento_api_connect.api_connect import get_magento_token, submit_request


class sale_order(osv.osv):
    _inherit = 'sale.order'

    _columns = {
        're_sync': fields.boolean(string="Re Sync"),
        'product_missing_ids': fields.one2many('product.missing', 'order_id', 'Product Missing IDs'),
    }


    def order_re_sync(self, cr, uid, ids, context=None):

        so_obj = self.pool.get("sale.order")
        so = so_obj.browse(cr, uid, ids, context=context)

        ### requesting from magento
        if ids:

            try:
                token, url_root = get_magento_token(self, cr, uid, ids)
            except:
                token = None
                url_root = None

        if token and url_root:
            token = token.replace('"', "")
            headers = {'Authorization': token, 'Content-Type': 'application/json'}

            get_url = url_root + "/rest/V1/odoomagentoconnect/GetOrderDetails/" + str(so.client_order_ref)
            get_resp = requests.get(get_url, headers=headers)
            order_data = json.loads(get_resp.text)

            # mag_no = order_data["increment_id"]
            mag_total = order_data["grand_total"]
            mag_items = order_data["items"]

            so_amount_total = so.amount_total
            if so_amount_total != mag_total:

                email_template_obj = self.pool.get('email.template')
                template_ids = email_template_obj.search(cr, uid, [('name', '=', 'Unsync Product')], context=context)

                prod_obj = self.pool.get('product.product')

                # i = 0
                prod_miss_list = []

                for counter in range(5):

                    odoo_line_item_list = []
                    missing_item_list = []
                    item_list = []


                    ### odoo order product item line
                    for line in so.order_line:

                        odoo_line_item_list.append(str(line.default_code))

                    ### magento order product item line
                    for prod_list in mag_items:
                        if str(prod_list.get('sku')) not in odoo_line_item_list:

                            tmp_dict = {}

                            tmp_dict[str(prod_list.get('sku'))] = prod_list
                            missing_item_list.append(tmp_dict)

                        else:
                            tmp_dict = {}

                            tmp_dict[str(prod_list.get('sku'))] = prod_list
                            item_list.append(tmp_dict)

                    # vora
                    if missing_item_list:

                        for mag_item in missing_item_list:
                            sku = mag_item.keys()[0]
                            prod_search = prod_obj.search(cr, uid, [('default_code', '=', str(sku))], context=context)


                            if not prod_search:

                                ## email###

                                uid = 1


                                # try to sync product from magento --- start

                                post_url = url_root + "/rest/V1/odoomagentoconnect/productSyncBulk/"
                                productIds = [mag_item[sku]['product_id']]
                                miss_prods = json.dumps({"productIds": productIds})

                                miss_prod_resp = requests.post(post_url, data=miss_prods, headers=headers)
                                miss_prod_data = json.loads(miss_prod_resp.text)
                                miss_prod_dict = dict()

                                for m_p in miss_prod_data:

                                    miss_prod_dict[m_p[m_p.keys()[0]]['sku']] = m_p[m_p.keys()[0]]['success']

                                # try to sync product from magento --- end

                                if template_ids and len(miss_prod_dict) > 0:

                                    if m_p[m_p.keys()[0]]['sku'] not in prod_miss_list:

                                        prod_miss_list.append(m_p[m_p.keys()[0]]['sku'])



                            else:

                                sol_obj = self.pool.get('sale.order.line')

                                x_discount = (mag_item[sku]['discount_amount']) / (mag_item[sku]['qty_ordered'])
                                price_unit = (mag_item[sku]['price']) - x_discount

                                sol_data = {
                                    'product_id': prod_search[0],
                                    'state': 'draft',
                                    'name': mag_item[sku]['name'],
                                    'default_code': mag_item[sku]['sku'],
                                    'price_unit': price_unit,
                                    'product_uom_qty': mag_item[sku]['qty_ordered'],
                                    'order_id': so.id,
                                    'x_discount': x_discount,
                                    'total_discount': mag_item[sku]['discount_amount'],
                                    'x_original_price_unit': mag_item[sku]['price'],
                                }

                                sol_obj.create(cr, uid, sol_data, context=context)




                    else:

                        # prod_obj = self.pool.get('product.product')

                        # for mag_item in item_list:
                        #     sku = mag_item.keys()[0]
                        #     prod_search = prod_obj.search(cr, uid, [('default_code', '=', str(sku))], context=context)

                        #     if prod_search:

                                # for so_line in so.order_line:
                                #     if prod_search[0]==so_line.product_id.id:
                                #         cr.execute("update sale_order_line set total_discount=%s where id=%s and product_id=%s", (mag_item[sku]['discount_amount'], so_line.id, so_line.product_id.id))
                                #         cr.commit()

                        self.pool.get('sale.order').action_re_calculate_so_amount(cr, uid, ids, context=context)

                    so_amount_total = so_obj.browse(cr, uid, ids, context=context)



                    if so_amount_total.amount_total == mag_total:

                        re_sync_update_query = "UPDATE sale_order SET re_sync= TRUE WHERE id={0}".format(so_amount_total.id)
                        cr.execute(re_sync_update_query)
                        cr.commit()

                        break




                for pm in prod_miss_list:

                    pm_obj = self.pool.get('product.missing')

                    for item in missing_item_list:

                        if str(item.keys()[0]) == str(pm):


                            sku = item.keys()[0]

                            pm_data = {
                                'order_id': ids[0],

                                'miss_name': item[sku]['name'],
                                'miss_sku': str(pm),
                                'miss_qty': item[sku]['qty_ordered'],
                            }

                            pm_obj.create(cr, uid, pm_data, context=context)


                ########## Email and warning #######
                if prod_miss_list:

                    email_template_obj.send_mail(cr, uid, template_ids[0], ids[0], force_send=True, context=context)

                    raise osv.except_osv(_('Warning!!!'), _(
                    'This products are not enlisted in product table. Please this product sync manually \'%s\'.') % str(prod_miss_list))

                ########## End ###############

                return {
                    'type': 'ir.actions.client',
                    'tag': 'reload',
                }


            else:

                re_sync_update_query = "UPDATE sale_order SET re_sync= TRUE WHERE id={0}".format(so.id)
                cr.execute(re_sync_update_query)
                cr.commit()

                # return {
                #     'type': 'ir.actions.client',
                #     'tag': 'reload',
                # }
                #
                # raise osv.except_osv(_('Successful!!!'), _('This order already sync successfully.'))

        return True



    class product_missing(osv.osv):
        _name = 'product.missing'

        _columns = {
            'order_id': fields.many2one('sale.order', string="Order ID"),

            'miss_name': fields.char(string="Product Name"),
            'miss_sku': fields.char(string="SKU"),
            'miss_qty': fields.float(string="Quantity"),
        }



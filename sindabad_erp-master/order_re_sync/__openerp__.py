{
    'name': 'Order Re Sync',
    'version': '1.0',
    'category': 'Sale',
    'summary': """Order Re Sync from Magento""",
    'description': """Order Re Sync from Magento""",
    'author': 'Md Rockibul Alam',
    'website': 'http://www.sindabad.com',
    'depends': ['odoo_magento_connect', 'sale', 'sales_collection_billing_process'],
    'update_xml': [
        'security/order_re_sync_security.xml',
        'security/ir.model.access.csv',
        'sale_order_view.xml',
    ],

    'installable': True,
    'active': False,

}

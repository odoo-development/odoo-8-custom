from datetime import datetime, timedelta
import time
from datetime import date
from openerp import SUPERUSER_ID
from openerp.osv import fields, osv
from openerp.tools.translate import _
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT
import openerp.addons.decimal_precision as dp
from openerp import workflow
from openerp import api
import datetime
from openerp.exceptions import except_orm, Warning, RedirectWarning
from openerp.tools.translate import _


class expense_type(osv.osv):
    _name = "expense.type"
    _description = "Expense Type"

    _columns = {
        'exp_type_id': fields.integer('Expense Type ID', readonly=True),
        'name': fields.char(string="Expense Type", required=True),
        'code': fields.char(string="Expense Code", required=True),
        'prefix': fields.char(string="Expense Prefix", required=True),
        'debit_account_id': fields.many2one('account.account', string="Debit Account ID", required=True),
        'credit_account_id': fields.many2one('account.account', string="Credit Account ID", required=True),
        'department_id': fields.many2one('hr.department', string="Department"),
        'active': fields.boolean('Active'),
    }
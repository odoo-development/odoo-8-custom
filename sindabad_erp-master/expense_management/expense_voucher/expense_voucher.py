from datetime import datetime, timedelta
import time
from datetime import date
from openerp import SUPERUSER_ID
from openerp.osv import fields, osv
from openerp.tools.translate import _
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT
import openerp.addons.decimal_precision as dp
from openerp import workflow
from openerp import api
import datetime
from openerp.exceptions import except_orm, Warning, RedirectWarning
from openerp.tools.translate import _


class expense_voucher(osv.osv):
    _name = "expense.voucher"
    _description = "Expense Voucher"

    def amount_all(self, cr, uid, ids, field, arg, context=None):

        res = {}

        total_amount = 0.0
        for exp_bou in self.browse(cr, uid, ids, context=context):
            for evl_single in exp_bou.expense_voucher_line:
                total_amount += evl_single.amount

        res[ids[0]] = total_amount

        return res

    _columns = {
        'name': fields.char('Name'),
        'department_id': fields.many2one('hr.department', 'Department', required=True),
        'journal_id': fields.many2one('account.move', 'Journal Entries'),
        'confirm_journal_id': fields.many2one('account.move', 'Confirm Journal Entries'),
        'voucher_date': fields.datetime('Date', required=True),
        'type': fields.many2one('expense.type', 'exp_type', required=True),
        'verify': fields.boolean('Verify'),
        'verify_by': fields.many2one('res.users', 'Verify By'),
        'verify_time': fields.datetime('Verify Time'),
        'confirm': fields.boolean('Confirm'),
        'confirm_by': fields.many2one('res.users', 'Confirm By'),
        'confirm_time': fields.datetime('Confirmation Time'),
        'paid': fields.boolean('Paid'),
        'paid_by': fields.many2one('res.users', 'Paid By'),
        'paid_time': fields.datetime('Paid Time'),
        'cancel': fields.boolean('Cancel'),
        'cancel_by': fields.many2one('res.users', 'Cancel By'),
        'cancel_time': fields.datetime('Cancel Time'),
        'state': fields.selection([
            ('draft', 'Draft'),
            ('verify', 'Verified'),
            ('confirm', 'Confirmed'),
            ('paid', 'Paid'),
            ('cancel', 'Cancelled'),

        ], 'Status', readonly=True, copy=False, help="Gives the status of the Expense Voucher", select=True),
        'expense_voucher_line': fields.one2many('expense.voucher.line', 'expense_voucher_id', 'Expense Voucher Line', required=True),

        'total_line_sum': fields.function(amount_all, type='float', string='Total Amount'),

        'cancel_expense_voucher_date': fields.datetime('Cancel Expense Voucher Date'),
        'cancel_expense_voucher_by': fields.many2one('res.users', 'Cancel Expense Voucher By'),
        'cancel_expense_voucher_reason': fields.text('Cancel Expense Voucher Reason'),

        'paid_expense_voucher_date': fields.datetime('Paid Expense Voucher Date'),
        'paid_expense_voucher_by': fields.many2one('res.users', 'Paid Expense Voucher By'),
        'paid_expense_voucher_reason': fields.text('Paid Expense Voucher Reason'),

    }

    _defaults = {
        'user_id': lambda obj, cr, uid, context: uid,
        'state': 'draft',
    }


    @api.model
    def create(self, vals):

        # generate IRN number
        # IRN0123456

        prefix='EXP0'

        record = super(expense_voucher, self).create(vals)

        if record.type.prefix is not None:
            prefix = record.type.prefix

        exp_number = str(prefix)+str('0 ')+str(record.id)

        record.name = exp_number

        return record


    def verify_exp_voucher(self, cr, uid, ids, context=None):


        id = ids[0] if len(ids) >0 else None
        v_expense = self.browse(cr, uid, id, context=context)
        for single_id in ids:
            verify_expense_query = "UPDATE expense_voucher SET state='verify', verify_time='{0}' WHERE id={1}".format(
                str(fields.datetime.now()), single_id)
            cr.execute(verify_expense_query)
            cr.commit()

            verify_expense_query_line = "UPDATE expense_voucher_line SET state='verify', verify_time='{0}' WHERE expense_voucher_id={1}".format(
                str(fields.datetime.now()), single_id)
            cr.execute(verify_expense_query_line)
            cr.commit()



        # if uid == v_expense.department_id.manager_id.user_id.id:
        #     for single_id in ids:
        #         verify_expense_query = "UPDATE expense_voucher SET state='verify', verify_time='{0}' WHERE id={1}".format(
        #             str(fields.datetime.now()), single_id)
        #         cr.execute(verify_expense_query)
        #         cr.commit()
        #
        #         verify_expense_query_line = "UPDATE expense_voucher_line SET state='verify', verify_time='{0}' WHERE expense_voucher_id={1}".format(
        #             str(fields.datetime.now()), single_id)
        #         cr.execute(verify_expense_query_line)
        #         cr.commit()
        # else:
        #     name = 'Only your manager '+ str(v_expense.department_id.manager_id.name) + ' can verify it'
        #     raise Warning(_(name))

        return True

    def confirm_exp_voucher(self, cr, uid, ids, context=None):
        id = ids[0] if len(ids) > 0 else None
        v_expense = self.browse(cr, uid, id, context=context)
        saved_jv_id=False

        if v_expense.type.debit_account_id.id and v_expense.type.debit_account_id.id:
            jv_data = {}
            jv_data['ref'] = context.get('ref')
            jv_data['journal_id'] = 919

            jv_data['period_id'] = 17

            jv_data['line_id'] = [
                [0, False, {'analytic_account_id': False, 'tax_code_id': False, 'tax_amount': 0, 'name': 'dfgdfg',
                            'currency_id': False, 'credit': v_expense.total_line_sum, 'date_maturity': False, 'debit': 0,
                            'amount_currency': 0, 'partner_id': False, 'account_id': v_expense.type.credit_account_id.id}],
                [0, False, {
                    'analytic_account_id': False, 'tax_code_id': False, 'tax_amount': 0, 'name': 'dfgdfg',
                    'currency_id': False,
                    'credit': 0, 'date_maturity': False, 'debit': v_expense.total_line_sum, 'amount_currency': 0,
                    'partner_id': False,
                    'account_id': v_expense.type.debit_account_id.id}]]

            jv_entry = self.pool.get('account.move')
            saved_jv_id = jv_entry.create(cr, uid, jv_data, context=context)

        for single_id in ids:
            confirm_expense_query = "UPDATE expense_voucher SET state='confirm', confirm_time='{0}' , confirm_journal_id={1} WHERE id={2}".format(
                str(fields.datetime.now()),saved_jv_id, single_id)
            cr.execute(confirm_expense_query)
            cr.commit()

            confirm_expense_query_line = "UPDATE expense_voucher_line SET state='confirm', confirm_time='{0}' WHERE expense_voucher_id={1}".format(
                str(fields.datetime.now()), single_id)
            cr.execute(confirm_expense_query_line)
            cr.commit()

        return True

    def paid_exp_voucher(self, cr, uid, ids, context=None):

        for single_id in ids:
            paid_expense_query = "UPDATE expense_voucher SET state='paid', paid_time='{0}' WHERE id={1}".format(
                str(fields.datetime.now()), single_id)
            cr.execute(paid_expense_query)
            cr.commit()

            paid_expense_query_line = "UPDATE expense_voucher_line SET state='paid', paid_time='{0}' WHERE expense_voucher_id={1}".format(
                str(fields.datetime.now()), single_id)
            cr.execute(paid_expense_query_line)
            cr.commit()

        return True

    def cancel_exp_voucher(self, cr, uid, ids, context=None):

        for single_id in ids:
            cancel_expense_query = "UPDATE expense_voucher SET state='cancel', cancel_time='{0}' WHERE id={1}".format(
                str(fields.datetime.now()), single_id)
            cr.execute(cancel_expense_query)
            cr.commit()

            cancel_expense_query_line = "UPDATE expense_voucher_line SET state='cancel', cancel_time='{0}' WHERE expense_voucher_id={1}".format(
                str(fields.datetime.now()), single_id)
            cr.execute(cancel_expense_query_line)
            cr.commit()

        return True


class expense_voucher_line(osv.osv):
    _name = "expense.voucher.line"
    _description = "Expense Voucher Line"

    _columns = {
        'expense_voucher_id': fields.many2one('expense.voucher', 'Expense Voucher ID', required=True,
                                          ondelete='cascade', select=True, readonly=True),

        'date': fields.datetime('Date'),
        'place_from': fields.char('From'),
        'place_to': fields.char('To'),
        'vehicle_by': fields.char('Vehicle By'),
        'purpose': fields.char('Purpose', required=True),
        'amount': fields.float('Amount', required=True),

        'confirm_time': fields.datetime('Confirmation Time'),
        'verify_time': fields.datetime('Verify Time'),
        'paid_time': fields.datetime('Paid Time'),
        'cancel_time': fields.datetime('Cancel Time'),

        'state': fields.selection([
            ('draft', 'Draft'),
            ('verify', 'Verified'),
            ('confirm', 'Confirmed'),
            ('paid', 'Paid'),
            ('cancel', 'Cancelled'),
        ], 'Status', help="Gives the status of the Expense Voucher line", select=True),
    }

    _defaults = {
        'amount': 0,
    }


class CancelExpenseVoucherReason(osv.osv):
    _name = "cancel.expense.voucher.reason"
    _description = "Cancel Expense Voucher Reason"

    _columns = {
        'cancel_expense_voucher_date': fields.datetime('Cancel Expense Voucher Date'),
        'cancel_expense_voucher_by': fields.many2one('res.users', 'Cancel Expense Voucher By'),
        'cancel_expense_voucher_reason': fields.text('Cancel Expense Voucher Reason', required=True),
    }

    def cancel_expense_reason_def(self, cr, uid, ids, context=None):
        ids = context['active_ids']
        cancel_expense_voucher_reason = str(context['cancel_expense_voucher_reason'])
        cancel_expense_voucher_by = uid
        cancel_expense_voucher_date = str(fields.datetime.now())

        expense_voucher_obj = self.pool.get('expense.voucher')

        for s_id in ids:
            cancel_expense_voucher_query = "UPDATE expense_voucher SET cancel_expense_voucher_reason='{0}', cancel_expense_voucher_by={1}, cancel_expense_voucher_date='{2}' WHERE id={3}".format(
                cancel_expense_voucher_reason, cancel_expense_voucher_by, cancel_expense_voucher_date, s_id)
            cr.execute(cancel_expense_voucher_query)
            cr.commit()

            expense_voucher_obj.cancel_exp_voucher(cr, uid, [s_id], context)

        return True


class PaidExpenseVoucherReason(osv.osv):
    _name = "paid.expense.voucher.reason"
    _description = "Paid Expense Voucher Reason"

    _columns = {
        'paid_expense_voucher_date': fields.datetime('Paid Expense Voucher Date'),
        'paid_expense_voucher_by': fields.many2one('res.users', 'Paid Expense Voucher By'),
        'paid_expense_voucher_reason': fields.text('Note'),
        'account_id': fields.many2one('account.account', string="Payment From ",required=True), #CR

    }

    def paid_expense_reason_def(self, cr, uid, ids, context=None):
        ids = context['active_ids']
        paid_expense_voucher_reason = str(context['paid_expense_voucher_reason'])
        paid_expense_voucher_by = uid
        paid_expense_voucher_date = str(fields.datetime.now())
        saved_jv_id = None
        total_amount = 0

        exp_voucher_id = context.get('active_id')

        paid_expense_voucher_obj_data = self.pool.get('expense.voucher').browse(cr, uid, exp_voucher_id, context=context)
        total_amount = paid_expense_voucher_obj_data.total_line_sum




        if context.get('account_id'):

            jv_data = {}
            jv_data['ref'] = context.get('ref')
            jv_data['journal_id'] = 919

            jv_data['period_id'] = 17

            jv_data['line_id'] = [
                [0, False, {'analytic_account_id': False, 'tax_code_id': False, 'tax_amount': 0, 'name': 'dfgdfg',
                            'currency_id': False, 'credit': total_amount, 'date_maturity': False, 'debit': 0,
                            'amount_currency': 0, 'partner_id': False, 'account_id': context.get('account_id')}],
                [0, False, {
                    'analytic_account_id': False, 'tax_code_id': False, 'tax_amount': 0, 'name': '',
                    'currency_id': False,
                    'credit': 0, 'date_maturity': False, 'debit': total_amount, 'amount_currency': 0,
                    'partner_id': False,
                    'account_id': paid_expense_voucher_obj_data.type.credit_account_id.id}]]

            jv_entry = self.pool.get('account.move')
            saved_jv_id = jv_entry.create(cr, uid, jv_data, context=context)

        paid_expense_voucher_obj = self.pool.get('expense.voucher')

        for s_id in ids:
            paid_expense_voucher_query = "UPDATE expense_voucher SET paid_expense_voucher_reason='{0}', paid_expense_voucher_by={1}, paid_expense_voucher_date='{2}',journal_id={3} WHERE id={4}".format(
                paid_expense_voucher_reason, paid_expense_voucher_by, paid_expense_voucher_date,saved_jv_id, s_id)
            cr.execute(paid_expense_voucher_query)
            cr.commit()

            paid_expense_voucher_obj.paid_exp_voucher(cr, uid, [s_id], context)

        return True
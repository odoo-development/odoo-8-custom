{
    'name': 'Expense Management For Finance',
    'version': '1.0',
    'category': 'Account',
    'author': 'Mufti Muntasir Ahmed & Md. Rockibul Alam',
    'summary': 'Account all Expenses records are assigned in this module.',
    'description': 'All Expenses records are assigned in this module.',
    'depends': ['base', 'account'],
    'data': [
        'security/expense_management_security.xml',
        'security/ir.model.access.csv',

        'debit_voucher/wizard/cancel_debit_reasons_view.xml',
        'debit_voucher/wizard/paid_debit_reasons_view.xml',

        'expense_voucher/wizard/cancel_expense_reasons_view.xml',
        'expense_voucher/wizard/paid_expense_reasons_view.xml',

        'expense_type/expense_type_view.xml',

        'debit_voucher/debit_voucher_view.xml',
        'debit_voucher/debit_voucher_menus.xml',
        'debit_voucher/cancel_debit_reasons.xml',
        'debit_voucher/paid_debit_reasons.xml',

        'expense_voucher/expense_voucher_view.xml',
        'expense_voucher/expense_voucher_menus.xml',
        'expense_voucher/cancel_expense_reasons.xml',
        'expense_voucher/paid_expense_reasons.xml',

        'expense_management_menu.xml',

        'debit_voucher/report/report_debit_voucher_layout.xml',
        'debit_voucher/report/debit_voucher_print_menu.xml',
        'expense_voucher/report/report_expense_voucher_layout.xml',
        'expense_voucher/report/expense_voucher_print_menu.xml',
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}

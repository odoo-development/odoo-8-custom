from datetime import datetime, timedelta
import time
from datetime import date
from openerp import SUPERUSER_ID
from openerp.osv import fields, osv
from openerp.tools.translate import _
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT
import openerp.addons.decimal_precision as dp
from openerp import workflow
from openerp import api
import datetime
from openerp.exceptions import except_orm, Warning, RedirectWarning
from openerp.tools.translate import _


class debit_voucher(osv.osv):
    _name = "debit.voucher"
    _description = "Debit Voucher"

    def amount_all(self, cr, uid, ids, field, arg, context=None):

        res = {}

        total_amount = 0.0
        for debit_bou in self.browse(cr, uid, ids, context=context):
            for evl_single in debit_bou.debit_voucher_line:
                total_amount += evl_single.amount

        res[ids[0]] = total_amount

        return res

    _columns = {
        'name': fields.char('Name'),
        'department_id': fields.many2one('hr.department', 'Department', required=True),
        'journal_id': fields.many2one('account.move', 'Journal Entries'),
        'confirm_journal_id': fields.many2one('account.move', 'Confirm Journal Entries'),
        'voucher_date': fields.datetime('Date', required=True),
        'type': fields.many2one('expense.type', 'exp_type', required=True),
        'verify': fields.boolean('Verify'),
        'verify_by': fields.many2one('res.users', 'Verify By'),
        'verify_time': fields.datetime('Verify Time'),
        'confirm': fields.boolean('Confirm'),
        'confirm_by': fields.many2one('res.users', 'Confirm By'),
        'confirm_time': fields.datetime('Confirmation Time'),
        'paid': fields.boolean('Paid'),
        'paid_by': fields.many2one('res.users', 'Paid By'),
        'paid_time': fields.datetime('Paid Time'),
        'cancel': fields.boolean('Cancel'),
        'cancel_by': fields.many2one('res.users', 'Cancel By'),
        'cancel_time': fields.datetime('Cancel Time'),
        'state': fields.selection([
            ('draft', 'Draft'),
            ('verify', 'Verified'),
            ('confirm', 'Confirmed'),
            ('paid', 'Paid'),
            ('cancel', 'Cancelled'),

        ], 'Status', readonly=True, copy=False, help="Gives the status of the Debit Voucher", select=True),
        'debit_voucher_line': fields.one2many('debit.voucher.line', 'debit_voucher_id', 'Debit Voucher Line', required=True),

        'total_line_sum': fields.function(amount_all, type='float', string='Total Amount'),

        'cancel_debit_voucher_date': fields.datetime('Cancel Debit Voucher Date'),
        'cancel_debit_voucher_by': fields.many2one('res.users', 'Cancel Debit Voucher By'),
        'cancel_debit_voucher_reason': fields.text('Cancel Debit Voucher Reason'),

        'paid_debit_voucher_date': fields.datetime('Paid Debit Voucher Date'),
        'paid_debit_voucher_by': fields.many2one('res.users', 'Paid Debit Voucher By'),
        'paid_debit_voucher_reason': fields.text('Paid Debit Voucher Reason'),

    }

    _defaults = {
        'user_id': lambda obj, cr, uid, context: uid,
        'state': 'draft',
    }


    @api.model
    def create(self, vals):

        # generate IRN number
        # IRN0123456

        prefix='EXP0'

        record = super(debit_voucher, self).create(vals)

        if record.type.prefix is not None:
            prefix = record.type.prefix

        deb_number = str(prefix)+str('0 ')+str(record.id)

        record.name = deb_number

        return record

    def verify_debit_voucher(self, cr, uid, ids, context=None):

        id = ids[0] if len(ids) >0 else None
        v_debit = self.browse(cr, uid, id, context=context)
        for single_id in ids:
            verify_debit_query = "UPDATE debit_voucher SET state='verify', verify_time='{0}' WHERE id={1}".format(
                str(fields.datetime.now()), single_id)
            cr.execute(verify_debit_query)
            cr.commit()

            verify_debit_query_line = "UPDATE debit_voucher_line SET state='verify', verify_time='{0}' WHERE debit_voucher_id={1}".format(
                str(fields.datetime.now()), single_id)
            cr.execute(verify_debit_query_line)
            cr.commit()



        # if uid == v_debit.department_id.manager_id.user_id.id:
        #     for single_id in ids:
        #         verify_debit_query = "UPDATE debit_voucher SET state='verify', verify_time='{0}' WHERE id={1}".format(
        #             str(fields.datetime.now()), single_id)
        #         cr.execute(verify_debit_query)
        #         cr.commit()
        #
        #         verify_debit_query_line = "UPDATE debit_voucher_line SET state='verify', verify_time='{0}' WHERE debit_voucher_id={1}".format(
        #             str(fields.datetime.now()), single_id)
        #         cr.execute(verify_debit_query_line)
        #         cr.commit()
        # else:
        #     name = 'Only your manager '+ str(v_debit.department_id.manager_id.name) + ' can verify it'
        #     raise Warning(_(name))

        return True

    def confirm_debit_voucher(self, cr, uid, ids, context=None):
        id = ids[0] if len(ids) > 0 else None
        v_debit = self.browse(cr, uid, id, context=context)
        saved_jv_id=False

        if v_debit.type.debit_account_id.id and v_debit.type.debit_account_id.id:
            jv_data = {}
            jv_data['ref'] = context.get('ref')
            jv_data['journal_id'] = 919

            jv_data['period_id'] = 17

            jv_data['line_id'] = [
                [0, False, {'analytic_account_id': False, 'tax_code_id': False, 'tax_amount': 0, 'name': 'dfgdfg',
                            'currency_id': False, 'credit': v_debit.total_line_sum, 'date_maturity': False, 'debit': 0,
                            'amount_currency': 0, 'partner_id': False, 'account_id': v_debit.type.credit_account_id.id}],
                [0, False, {
                    'analytic_account_id': False, 'tax_code_id': False, 'tax_amount': 0, 'name': 'dfgdfg',
                    'currency_id': False,
                    'credit': 0, 'date_maturity': False, 'debit': v_debit.total_line_sum, 'amount_currency': 0,
                    'partner_id': False,
                    'account_id': v_debit.type.debit_account_id.id}]]

            jv_entry = self.pool.get('account.move')
            saved_jv_id = jv_entry.create(cr, uid, jv_data, context=context)

        for single_id in ids:
            confirm_debit_query = "UPDATE debit_voucher SET state='confirm', confirm_time='{0}' , confirm_journal_id={1} WHERE id={2}".format(
                str(fields.datetime.now()),saved_jv_id, single_id)
            cr.execute(confirm_debit_query)
            cr.commit()

            confirm_debit_query_line = "UPDATE debit_voucher_line SET state='confirm', confirm_time='{0}' WHERE debit_voucher_id={1}".format(
                str(fields.datetime.now()), single_id)
            cr.execute(confirm_debit_query_line)
            cr.commit()

        return True

    def paid_debit_voucher(self, cr, uid, ids, context=None):

        for single_id in ids:
            paid_debit_query = "UPDATE debit_voucher SET state='paid', paid_time='{0}' WHERE id={1}".format(
                str(fields.datetime.now()), single_id)
            cr.execute(paid_debit_query)
            cr.commit()

            paid_debit_query_line = "UPDATE debit_voucher_line SET state='paid', paid_time='{0}' WHERE debit_voucher_id={1}".format(
                str(fields.datetime.now()), single_id)
            cr.execute(paid_debit_query_line)
            cr.commit()

        return True

    def cancel_debit_voucher(self, cr, uid, ids, context=None):

        for single_id in ids:
            cancel_debit_query = "UPDATE debit_voucher SET state='cancel', cancel_time='{0}' WHERE id={1}".format(
                str(fields.datetime.now()), single_id)
            cr.execute(cancel_debit_query)
            cr.commit()

            cancel_debit_query_line = "UPDATE debit_voucher_line SET state='cancel', cancel_time='{0}' WHERE debit_voucher_id={1}".format(
                str(fields.datetime.now()), single_id)
            cr.execute(cancel_debit_query_line)
            cr.commit()

        return True


class debit_voucher_line(osv.osv):
    _name = "debit.voucher.line"
    _description = "Debit Voucher Line"

    _columns = {
        'debit_voucher_id': fields.many2one('debit.voucher', 'Debit Voucher ID', required=True,
                                          ondelete='cascade', select=True, readonly=True),

        'date': fields.datetime('Date'),
        'place_from': fields.char('From'),
        'place_to': fields.char('To'),
        'vehicle_by': fields.char('Vehicle By'),
        'particulars': fields.char('Particulars', required=True),
        'amount': fields.float('Amount', required=True),

        'confirm_time': fields.datetime('Confirmation Time'),
        'verify_time': fields.datetime('Verify Time'),
        'paid_time': fields.datetime('Paid Time'),
        'cancel_time': fields.datetime('Cancel Time'),

        'state': fields.selection([
            ('draft', 'Draft'),
            ('verify', 'Verified'),
            ('confirm', 'Confirmed'),
            ('paid', 'Paid'),
            ('cancel', 'Cancelled'),
        ], 'Status', help="Gives the status of the Debit Voucher line", select=True),
    }

    _defaults = {
        'amount': 0,
    }


class CancelDebitVoucherReason(osv.osv):
    _name = "cancel.debit.voucher.reason"
    _description = "Cancel Debit Voucher Reason"

    _columns = {
        'cancel_debit_voucher_date': fields.datetime('Cancel Debit Voucher Date'),
        'cancel_debit_voucher_by': fields.many2one('res.users', 'Cancel Debit Voucher By'),
        'cancel_debit_voucher_reason': fields.text('Cancel Debit Voucher Reason', required=True),
    }

    def cancel_debit_reason_def(self, cr, uid, ids, context=None):
        ids = context['active_ids']
        cancel_debit_voucher_reason = str(context['cancel_debit_voucher_reason'])
        cancel_debit_voucher_by = uid
        cancel_debit_voucher_date = str(fields.datetime.now())

        debit_voucher_obj = self.pool.get('debit.voucher')

        for s_id in ids:
            cancel_debit_voucher_query = "UPDATE debit_voucher SET cancel_debit_voucher_reason='{0}', cancel_debit_voucher_by={1}, cancel_debit_voucher_date='{2}' WHERE id={3}".format(
                cancel_debit_voucher_reason, cancel_debit_voucher_by, cancel_debit_voucher_date, s_id)
            cr.execute(cancel_debit_voucher_query)
            cr.commit()

            debit_voucher_obj.cancel_debit_voucher(cr, uid, [s_id], context)

        return True


class PaidDebitVoucherReason(osv.osv):
    _name = "paid.debit.voucher.reason"
    _description = "Paid Debit Voucher Reason"

    _columns = {
        'paid_debit_voucher_date': fields.datetime('Paid Debit Voucher Date'),
        'paid_debit_voucher_by': fields.many2one('res.users', 'Paid Debit Voucher By'),
        'paid_debit_voucher_reason': fields.text('Note'),
        'account_id': fields.many2one('account.account', string="Payment From ",required=True), #CR

    }

    def paid_debit_reason_def(self, cr, uid, ids, context=None):
        ids = context['active_ids']
        paid_debit_voucher_reason = str(context['paid_debit_voucher_reason'])
        paid_debit_voucher_by = uid
        paid_debit_voucher_date = str(fields.datetime.now())
        saved_jv_id = None
        total_amount = 0

        deb_voucher_id = context.get('active_id')

        paid_debit_voucher_obj_data = self.pool.get('debit.voucher').browse(cr, uid, deb_voucher_id, context=context)
        total_amount = paid_debit_voucher_obj_data.total_line_sum




        if context.get('account_id'):

            jv_data = {}
            jv_data['ref'] = context.get('ref')
            jv_data['journal_id'] = 919

            jv_data['period_id'] = 17

            jv_data['line_id'] = [
                [0, False, {'analytic_account_id': False, 'tax_code_id': False, 'tax_amount': 0, 'name': 'dfgdfg',
                            'currency_id': False, 'credit': total_amount, 'date_maturity': False, 'debit': 0,
                            'amount_currency': 0, 'partner_id': False, 'account_id': context.get('account_id')}],
                [0, False, {
                    'analytic_account_id': False, 'tax_code_id': False, 'tax_amount': 0, 'name': '',
                    'currency_id': False,
                    'credit': 0, 'date_maturity': False, 'debit': total_amount, 'amount_currency': 0,
                    'partner_id': False,
                    'account_id': paid_debit_voucher_obj_data.type.credit_account_id.id}]]

            jv_entry = self.pool.get('account.move')
            saved_jv_id = jv_entry.create(cr, uid, jv_data, context=context)

        paid_debit_voucher_obj = self.pool.get('debit.voucher')

        for s_id in ids:
            paid_debit_voucher_query = "UPDATE debit_voucher SET paid_debit_voucher_reason='{0}', paid_debit_voucher_by={1}, paid_debit_voucher_date='{2}',journal_id={3} WHERE id={4}".format(
                paid_debit_voucher_reason, paid_debit_voucher_by, paid_debit_voucher_date,saved_jv_id, s_id)
            cr.execute(paid_debit_voucher_query)
            cr.commit()

            paid_debit_voucher_obj.paid_debit_voucher(cr, uid, [s_id], context)

        return True
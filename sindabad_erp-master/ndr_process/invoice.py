# Author Mufti Muntasir Ahmed 10-02-2019


import itertools
from lxml import etree

from openerp import models, fields, api, _
from openerp.exceptions import except_orm, Warning, RedirectWarning
from openerp.tools import float_compare
import openerp.addons.decimal_precision as dp



class account_invoice(models.Model):
    _inherit = "account.invoice"


    ndr = fields.Boolean(string='Re-Attempt')
    ndr_count = fields.Integer(string='Re-Attempt COUNT', default=0)

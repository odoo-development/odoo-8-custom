from openerp import models, fields, api
import datetime


class ActualDelivery(models.Model):
    _name = 'actual.delivery'

    from_date = fields.Date(string="Start Date", required=True)
    to_date = fields.Date(string="End Date", required=True)
    warehouse_id = fields.Many2one('stock.warehouse', String="Warehouse")

    _defaults = {
        'from_date': datetime.datetime.today(),
        'to_date': datetime.datetime.today()
    }


    @api.model
    def compute_ageing(self, data):
        rec = self.browse(data)
        data = {}
        data['form'] = rec.read(['from_date', 'to_date', 'warehouse_id'])[0]
        return self.env['report'].get_action(rec, 'actual_delivery_report.report_actual_delivery',
                                             data=data)


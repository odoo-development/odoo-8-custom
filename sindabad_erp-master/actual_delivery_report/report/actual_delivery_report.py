from openerp import models, api
from datetime import datetime


class ActualDeliveryReport(models.AbstractModel):
    _name = 'report.actual_delivery_report.report_actual_delivery'

    def get_pickings(self, docs):
        pickings = []

        if docs:
            stock_picking_env = self.pool.get('stock.picking')
            picking_list = stock_picking_env.search(self._cr, self._uid,
                                                  [('origin', 'like', 'SO%'),
                                                   ('state', '=', 'done'),
                                                   ('date_done', '>=', docs.from_date),
                                                   ('date_done', '<=', docs.to_date)])

            magento_number = []
            for items in stock_picking_env.browse(self._cr, self._uid, picking_list, context=self._context):
                if str(items.mag_no) not in magento_number and items.mag_no is not False:
                    magento_number.append(str(items.mag_no))

            order_wise_picking_dict = {}

            for order_no in magento_number:
                order_data_tmp = []

                for stock in stock_picking_env.browse(self._cr, self._uid, picking_list, context=self._context):

                    if order_no == str(stock.mag_no) and stock.mag_no is not False:
                        order_data_tmp.append(stock)

                order_wise_picking_dict[order_no] = order_data_tmp

            for key, item in order_wise_picking_dict.iteritems():

                tmp_list = []

                for all_pick in item:
                    for move in all_pick.move_lines:
                        found = 0
                        for tmp in tmp_list:
                            if tmp['product_id'] == move.product_id.id:
                                tmp['qty'] += move.product_uom_qty
                                found = 1

                        if found == 0:
                            delivered_amount=0
                            for sale_line in move.sale_line_id:
                                if move.product_id.id == sale_line.product_id.id:
                                    delivered_amount = move.product_uom_qty * sale_line.price_unit
                            temp = {
                                'order_no': key,
                                'company_name': all_pick.partner_id.commercial_partner_id.name,
                                'customer_name': all_pick.partner_id.name,
                                'customer_email': all_pick.partner_id.email,
                                'classification': all_pick.partner_id.commercial_partner_id.x_classification,
                                'rm_name': all_pick.partner_id.user_id.name,
                                'date_of_confirmation': all_pick.date,
                                'delivery_date': all_pick.date_done,
                                'product': move.product_id.name,
                                'category': move.product_id.categ_id.name,
                                'qty': move.product_uom_qty,
                                'cost': 0,
                                'delivery_amount': delivered_amount,
                                'product_id': move.product_id.id
                            }
                            tmp_list.append(temp)
                pickings += tmp_list

                # import pdb
                # pdb.set_trace()

        return pickings

    @api.model
    def render_html(self, docids, data=None):

        self.model = self.env.context.get('active_model')
        docs = self.env[self.model].browse(self.env.context.get('active_id'))
        pickings = self.get_pickings(docs)
        docargs = {
            'doc_ids': self.ids,
            'doc_model': self.model,
            'docs': docs,
            'pickings': pickings,
        }
        return self.env['report'].render('actual_delivery_report.report_actual_delivery', docargs)

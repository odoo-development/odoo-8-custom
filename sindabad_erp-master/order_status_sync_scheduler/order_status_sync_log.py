from datetime import datetime, timedelta
import time, datetime, json
from openerp import SUPERUSER_ID
from openerp.osv import fields, osv
from openerp.tools.translate import _
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT
import openerp.addons.decimal_precision as dp
from openerp import workflow


class order_status_sync_log(osv.osv):
    _name = "order.status.sync.log"
    _description = "Order Status Sync Log"

    _columns = {
        'order_id': fields.many2one('sale.order', 'Odoo Order'),
        'odoo_so_no': fields.char(string="Odoo Order No."),
        'mage_so_no': fields.char(string="Magento Order No."),
        'order_status': fields.char(string="Order Status"),
    }

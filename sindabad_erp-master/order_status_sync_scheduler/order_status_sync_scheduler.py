from datetime import datetime, timedelta
import time, datetime, json
from openerp import SUPERUSER_ID
from openerp.osv import fields, osv
from openerp.tools.translate import _
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT
import openerp.addons.decimal_precision as dp
from openerp import workflow
import logging
from ..odoo_to_magento_api_connect.api_connect import get_magento_token, submit_request


XMLRPC_API = '/index.php/api/xmlrpc'

_logger = logging.getLogger(__name__)


class sale_order(osv.osv):
    _inherit = "sale.order"

    def order_status_sync_scheduler(self, cr, uid, active=False, ids=None, cron_mode=True, context=None):
        if context is None:
            context = {
                'lang': 'en_US',
                'params': {'action': 404},
                'tz': 'Asia/Dhaka',
                'uid': uid
            }

        # delete this block
        # print("-------------------------")
        # print("Scheduler is running: "+str(fields.datetime.now()))
        # print("-------------------------")
        # delete this block

        # search for last one month sale order
        # [13673, 13672, 13671, 13670, 13669, 13668]
        sale_order_ids = self.search(cr, uid, [('date_order', '>', (datetime.date.today()-datetime.timedelta(days=15)).strftime('%Y-%m-%d 00:00:00'))])

        # get token
        token = None
        url_root = None
        if sale_order_ids:
            # sale_order_ids = [13673, 13672, 13671, 13670, 13669, 13668]
            token, url_root = get_magento_token(self, cr, uid, sale_order_ids)
        count = 1

        if token:
            ossl_obj = self.pool.get('order.status.sync.log')
            for so in self.browse(cr, uid, sale_order_ids):
                status_text = 'processing'

                ossl_id_list = ossl_obj.search(cr, uid, [('order_id', '=', so.id)], context=context)

                if ossl_id_list:
                    ossl = ossl_obj.browse(cr, uid, ossl_id_list, context=context)

                try:
                    sync_required = False

                    if so.delivery_dispatch == True and so.partially_invoiced_dispatch == False:
                        status_text = 'delivered'
                    if so.partially_invoiced_dispatch == True or so.partial_delivered == True:
                        status_text = 'partially_delivered'
                    if so.invoiced_dispatch == True:
                        status_text = 'dispatched'
                    if so.partially_invoiced_dispatch == True:
                        status_text = 'partially_dispatched'
                    if so.state == 'done':
                        status_text = 'complete'
                    if str(so.state) in ['draft', 'approval_pending', 'check_pending', 'new_cus_approval_pending']:
                        status_text = 'pending'
                    if so.state == 'cancel':
                        status_text = 'canceled'

                    if not ossl_id_list:
                        sync_required = True
                    elif ossl and str(ossl.order_status) != status_text:
                        sync_required = True

                    if sync_required:
                        url = url_root + "/index.php/rest/V1/odoomagentoconnect/OrderStatus"
                        map_order_data = {'orderId': str(so.client_order_ref), 'status': status_text}
                        statusData = dict()
                        statusData['statusData'] = map_order_data
                        data = json.dumps(statusData)
                        resp = 400
                        if status_text != 'canceled' and status_text != 'pending' and status_text != 'processing':
                            resp = submit_request(url, token, data)

                        # delete this block
                        #if resp == 200:
                        #    print("\n\nSuccess in SO submission: " + str(so.id))
                        #    print("Total orders: " + str(len(sale_order_ids)))
                        #    print("Order counts: " + str(count))
                        #    count += 1
                        # print("Orders: " + str(sale_order_ids) + "\n\n")
                        # if resp != 200:
                        #    print("Error in" + " SO: " + str(so.id) + "--" + status_text)
                        # delete this block

                        if resp == 200:
                            # create or update the order status into order.status.sync.log
                            if not ossl_id_list:
                                values = {
                                    'order_id': so.id,
                                    'odoo_so_no': str(so.name),
                                    'mage_so_no': str(so.client_order_ref),
                                    'order_status': status_text,
                                }
                                ossl_obj.create(cr, uid, values, context=context)
                            else:
                                # update
                                ossl.write({"order_status": status_text})

                except:
                    # delete this block
                    # print(" ")
                    # print("-------------------------")
                    # print("Error in" + ": " + str(so.id) + "--" + status_text)
                    # print("-------------------------")
                    # print(" ")
                    # delete this block

                    pass

        return True

{
    'name': 'Order Status Sync Scheduler',
    'version': '1.0',
    'category': 'Order Status Sync Scheduler',
    'author': 'Shahjalal Hossain',
    'summary': 'Order Status Sync Scheduler',
    'description': 'Order Status Sync Scheduler',
    'depends': ['base', 'odoo_magento_connect', 'sale', 'stock', 'wms_manifest', 'order_status_sync', 'odoo_to_magento_api_connect'],
    'data': [
        'order_status_sync_scheduler_cron.xml',
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}

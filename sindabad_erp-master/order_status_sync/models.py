# -*- coding: utf-8 -*-
##########################################################################
#    Author: S. M. Sazedul Haque
##########################################################################

import binascii
import json
import re
import xmlrpclib
import requests
from datetime import date
from openerp.http import request

from datetime import datetime
import xmlrpclib
from openerp.osv import fields, osv
from openerp.tools.translate import _
from openerp import SUPERUSER_ID, api
import openerp.addons.decimal_precision as dp
import logging

XMLRPC_API = '/index.php/api/xmlrpc'

_logger = logging.getLogger(__name__)


class sale_order(osv.osv):
    _inherit = "sale.order"
    #  cr, uid, increment_id, order_name, instance_id, context

    def manual_magento_order_status(self, cr, uid, increment_id, order_name, instance_id, status_text, context=None):
        _logger.warn('magento_order_status_change of %s with status_text = %s' % (order_name, status_text))
        text = ''
        status = 'no'
        session = 0
        statusData = {}
        obj = self.pool.get('magento.configure').browse(cr, uid, instance_id)
        if obj.state != 'enable':
            return False
        else:
            connection = self.pool.get(
                'magento.configure')._create_connection(cr, uid, context)
            if connection:
                url = connection[0]
                token = connection[1]

                # if status_text == 'processing':
                map_product_data = {'orderId': increment_id, 'status': status_text}
                statusData['statusData'] = map_product_data
                cc = json.dumps(statusData)
                map_option_url = url + "/index.php/rest/V1/odoomagentoconnect/OrderStatus"
                token = token.replace('"', "")
                try:
                    userAgent = request.httprequest.environ.get('HTTP_USER_AGENT', '')
                except:
                    userAgent=''
                headers = {'Authorization': token,
                           'Content-Type': 'application/json', 'User-Agent': userAgent}

                status = requests.post(
                    map_option_url, data=cc, headers=headers, verify=False)
                """
                if status:
                    text = 'sales order %s has been sucessfully status changed to %s from magento.' % (order_name, status)
                    status = 'yes'
                else:
                    text = 'Order %s cannot be status changed to %s from magento, Because Magento order %s is in different state.' % (
                        order_name, status, increment_id)
                """
            # cr.commit()
            # self.pool.get('magento.sync.history').create(cr, uid, {
            #     'status': status, 'action_on': 'order', 'action': 'b', 'error_message': text})
        return True

    def action_status(self, cr, uid, sale_ids, status_text, context=None):

        # res = super(sale_order, self).action_status_text(cr, uid, sale_ids, status_text, context)
        if context is None:
            context = {}
        for order_id in sale_ids:
            order_ids = self.pool.get('magento.orders').search(
                cr, uid, [('order_ref', '=', order_id)])
            if order_ids:
                map_obj = self.pool.get('magento.orders').browse(
                    cr, uid, order_ids[0])
                order_name = map_obj.order_ref.name
                instance_id = map_obj.instance_id.id
                increment_id = map_obj.mage_increment_id
                self.manual_magento_order_status(cr, uid, increment_id, order_name, instance_id, status_text, context)
                return True

    def action_button_confirm(self, cr, uid, ids, context=None):

        super(sale_order, self).action_button_confirm(cr, uid, ids, context)
        sale_ids = ids

        try:
            s_uid = 1
            abc = self.browse(cr, uid, ids[0], context=context)
            data = {
                'odoo_order_id': ids[0],
                'magento_id': str(abc.client_order_ref),
                'order_state': str('processing'),
                'state_time': fields.datetime.now(),
            }
            new_r = self.pool["order.status.synch.log"].create(cr, s_uid, data)
        except:
            pass

        try:
         self.pool.get("sale.order").action_status(cr, uid, sale_ids, 'processing', context=context)
        except:
            pass
        return True


sale_order()


class Account_voucher(osv.osv):
    _inherit = "account.voucher"

    def magento_final_sync(self, cr, uid, order_id, increment_id, order_name, instance_id, context=None):

        if context is None:
            context = {}
        text = ''
        status = 'no'
        session = False
        mage_shipment = False
        total_return_qty = 0
        obj = self.pool.get('magento.configure').browse(cr, uid, instance_id)
        if obj.state != 'enable':
            return False
        # email = obj.notify
        connection = self.pool.get(
            'magento.configure')._create_connection(cr, uid, context)
        if connection:
            url = connection[0]
            token = connection[1]
            itemData = {}
            return_itemData = {}
            cancelItem = {}
            sale_order_line = self.pool.get('sale.order.line')
            stock_picking = self.pool.get('stock.picking')
            order_line_ids = sale_order_line.search(cr, uid, [('order_id', '=', order_id)])
            toBeInvoiced = stock_picking.search(cr, uid, [('origin', '=', order_name), ('invoice_state', '=', '2binvoiced'), ('state', '=', 'done')])
            if len(toBeInvoiced) > 0:
                return False
            for orderLine in sale_order_line.browse(cr, uid, order_line_ids, context):
                productSku = orderLine.product_id.default_code or False
                if productSku:
                    quantity = orderLine.qty_delivered
                    itemData[productSku] = int(quantity)
                    return_quantity = (int(orderLine.product_uom_qty) - int(quantity))
                    total_return_qty += return_quantity
                    return_itemData[productSku] = int(return_quantity)
            map_product_data = {'orderId': increment_id, 'itemData': itemData}
            cc = json.dumps(map_product_data)
            map_option_url = url + "/index.php/rest/V1/odoomagentoconnect/OrderShipment"
            # map_option_url = url + "/index.php/rest/V1/odoomagentoconnect/OrderShipment"
            token = token.replace('"', "")
            userAgent = request.httprequest.environ.get('HTTP_USER_AGENT', '')
            headers = {'Authorization': token,
                       'Content-Type': 'application/json', 'User-Agent': userAgent}
            """
            shipment = requests.post(
                map_option_url, data=cc, headers=headers, verify=False)

            try:
                mage_shipment = json.loads(shipment.text)
            except ValueError as e:
                pass
            if shipment:
                text = 'shipment of order %s has been successfully updated on magento.' % order_name
                status = 'yes'
            else:
                text = 'Magento shipment Error For Order %s , Error' % (
                    order_name)
                status = 'yes'
            """
        cr.commit()
        self.pool.get('magento.sync.history').create(cr, uid, {
            'status': status, 'action_on': 'order', 'action': 'b', 'error_message': text})

        # START>>-------------IF-----RETURN exist-------------------#
        so_obj = self.pool.get('sale.order')
        so = so_obj.browse(cr, uid, [order_id], context=context)

        returned_prds = dict()

        for line in so.order_line:
            if line.qty_return > 0 and line.product_id.default_code:
                returned_prds[str(line.product_id.default_code).replace('"', '\"')] = line.qty_return

        # if total_return_qty > 0:
        if returned_prds:

            # return_map_product_data = {'orderId': increment_id, 'itemData': return_itemData}
            return_map_product_data = {'orderId': str(so.client_order_ref), 'itemData': returned_prds}
            map_option_return_url = url + "/index.php/rest/V1/odoomagentoconnect/OrderItemCancel"

            cancelItem = dict()
            cancelItem['cancelItem'] = return_map_product_data
            cc = json.dumps(cancelItem)

            # return_connection = self.pool.get(
            #     'magento.configure')._create_connection(cr, uid, context)
            # return_token = return_connection[1]
            # return_token = return_token.replace('"', "")
            userAgent = request.httprequest.environ.get('HTTP_USER_AGENT', '')
            headers = {'Authorization': token,
                       'Content-Type': 'application/json', 'User-Agent': userAgent}
            shipment = requests.post(
                map_option_return_url, data=cc, headers=headers, verify=False)

            try:
                mage_shipment = json.loads(shipment.text)
            except ValueError as e:
                pass
            if shipment:
                text = 'shipment of order %s has been successfully updated on magento.' % order_name
                status = 'yes'
            else:
                text = 'Magento shipment Error For Order %s , Error' % (
                    order_name)
                status = 'yes'
            cr.commit()
            self.pool.get('magento.sync.history').create(cr, uid, {
                'status': status, 'action_on': 'order', 'action': 'b', 'error_message': text})
        self.pool.get("sale.order").action_status(cr, uid, [order_id, ], 'complete', context=context)
        # END>>>>-------------IF-----RETURN exist-------------------#
        return mage_shipment

    def button_proforma_voucher(self, cr, uid, ids, context=None):
        self.signal_workflow(cr, uid, ids, 'proforma_voucher')
        order_ids = self.pool.get('magento.orders').search(
            cr, uid, [('mage_increment_id', '=', context['default_reference'])])
        if order_ids:
            map_obj = self.pool.get('magento.orders').browse(
                cr, uid, order_ids[0])
            if map_obj.order_ref.invoiced is True and map_obj.order_ref.state == 'done':
                order_name = map_obj.order_ref.name
                order_id = map_obj.order_ref.id
                instance_id = map_obj.instance_id.id
                increment_id = map_obj.mage_increment_id
                self.magento_final_sync(cr, uid, order_id, increment_id, order_name, instance_id, context)
        return {'type': 'ir.actions.act_window_close'}


Account_voucher()


class stock_picking(osv.osv):
    _inherit = "stock.picking"

    def action_cancel(self, cr, uid, ids, context=None):
        if isinstance(ids, (int, long)):
            ids = [ids]
        super(stock_picking, self).action_cancel(cr, uid, ids, context)
        for picking_id in ids:
            picking = self.pool.get('stock.picking').read(cr, uid, picking_id, ['sale_id', 'origin'])
            if picking['sale_id']:
                sale_id = picking['sale_id'][0]
                order_ids = self.pool.get('magento.orders').search(
                    cr, uid, [('order_ref', '=', sale_id)])
                if order_ids:
                    map_obj = self.pool.get('magento.orders').browse(
                        cr, uid, order_ids[0])
                    if map_obj.order_ref.invoiced is True and map_obj.order_ref.state == 'done':
                        order_name = map_obj.order_ref.name
                        order_id = map_obj.order_ref.id
                        instance_id = map_obj.instance_id.id
                        increment_id = map_obj.mage_increment_id
                        self.pool.get("account.voucher").magento_final_sync(cr, uid, order_id, increment_id, order_name, instance_id, context)
                return True


stock_picking()


class wms_manifest_process(osv.osv):
    _inherit = "wms.manifest.process"

    def confirm_wms_manifest(self, cr, uid, ids, context=None):


        data = self.browse(cr, uid, ids[0], context=context)

        for items in data.picking_line:
            magento_no = str(items.magento_no)
            query_for_SO = self.pool['sale.order'].search(cr, uid, [('client_order_ref', '=', magento_no)])

            if len(query_for_SO) > 0:

                sales = self.pool.get('sale.order').browse(cr, uid, query_for_SO, context=context)[0]

                sale_order = self.pool.get("sale.order")
                for so_item in sales:
                    if so_item.state != 'cancel':
                        if so_item.delivered_amount == so_item.amount_total:
                            dispatch_wms_query = "UPDATE sale_order SET invoiced_dispatch=TRUE  WHERE id='{1}'".format(uid, so_item.id)
                            cr.execute(dispatch_wms_query)
                            cr.commit()

                            try:
                                s_uid = 1

                                data = {
                                    'odoo_order_id': so_item.id,
                                    'magento_id': str(magento_no),
                                    'order_state': str('dispatched'),
                                    'state_time': fields.datetime.now(),
                                }
                                new_r = self.pool["order.status.synch.log"].create(cr, s_uid, data)
                            except:
                                pass

                            # try:
                            #     sale_order.action_status(cr, uid, [so_item.id, ], 'dispatched', context=context)
                            # except:
                            #     pass

                        else:
                            dispatch_wms_query = "UPDATE sale_order SET partially_invoiced_dispatch=TRUE  WHERE id='{1}'".format(uid, so_item.id)
                            cr.execute(dispatch_wms_query)
                            cr.commit()

                            try:
                                s_uid = 1
                                data = {
                                    'odoo_order_id': so_item.id,
                                    'magento_id': str(magento_no),
                                    'order_state': str('partially_dispatched'),
                                    'state_time': fields.datetime.now(),
                                }
                                new_r = self.pool["order.status.synch.log"].create(cr, s_uid, data)
                            except:
                                pass
                            # try:
                            #     sale_order.action_status(cr, uid, [so_item.id, ], 'partially_dispatched',
                            #                              context=context)
                            # except:
                            #     pass

        try:
            for single_id in ids:
                confirm_wms_query = "UPDATE wms_manifest_process SET state='confirm', confirm_by='{0}', confirm_time='{1}' WHERE id='{2}'".format(
                    uid, str(fields.datetime.now()), single_id)
                cr.execute(confirm_wms_query)
                cr.commit()
        except:
            pass

        return True


class wms_manifest_outbound(osv.osv):
    _inherit = "wms.manifest.outbound"

    def create(self, cr, uid, vals, context=None):

        order_number = None
        from datetime import date
        today_date = date.today()

        has_return = False
        inv_id = None
        for items in vals.get('outbound_line'):
            if items:
                for items2 in items:
                    if isinstance(items2, dict):
                        inv_id = items2.get('invoice_id')
                        order_number = items2.get('magento_no')
                        if float(items2.get('quantity')) > float(items2.get('delivered')):
                            items2['return_qty'] = str(float(items2.get('quantity')) - float(items2.get('delivered')))
                            has_return = True
                        else:
                            items2['return_qty'] = '0.00'

                        today_date = items2.get('delivery_date')

        vals['has_return'] = has_return
        vals['name'] = order_number
        today_date = vals.get('delivery_date')

        inv_data = self.pool.get('account.invoice').browse(cr, uid, [inv_id], context=context)
        if inv_data:
            if inv_data.delivered:
                raise osv.except_osv(_('Already Assigned!'), _('This Order Already Delivered'))
        if context is None:
            context = {}

        # create_context = dict(context, alias_model_name='crm.lead', alias_parent_model_name=self._name)
        section_id = super(wms_manifest_outbound, self).create(cr, uid, vals, context=context)
        # if not has_return:
        #     if section_id and inv_id is not None:

        cr.execute("update account_invoice set delivery_date=%s,delivered=TRUE where id=%s", ([today_date, inv_id]))

        cr.commit()
        # if context.has_key('manifest_line_id') and context['manifest_line_id']:
        #
        #     cr.execute("update wms_manifest_line set manifest_out_status='assigned' where id=%s", ([context['manifest_line_id']]))
        #
        #     cr.commit()

        if order_number is not None:
            cr.execute("select id from  sale_order where client_order_ref=%s", ([str(order_number)]))
            abc = cr.fetchall()
            sale_ids = [abc[0][0]]

            sales = self.pool.get('sale.order').browse(cr, uid, sale_ids, context=context)[0]

            sale_order = self.pool.get("sale.order")
            sales_amount_total = float(sales.amount_total) + float(sales.x_discount_amount) if sales.x_discount_amount else 0.00

            # if sales.x_prepaid == True:
            #     if int(sales.invoiced_amount) == int(sales.amount_total):
            #         cr.execute("update sale_order set delivery_dispatch=TRUE where id=%s", ([sales.id]))
            #         cr.commit()
            #         cr.execute("update sale_order set partially_invoiced_dispatch=FALSE where id=%s", ([sales.id]))
            #         cr.commit()
            #         try:
            #             s_uid = 1
            #
            #             data = {
            #                 'odoo_order_id': sales.id,
            #                 'magento_id': str(order_number),
            #                 'order_state': str('point_gain'),
            #                 'state_time': fields.datetime.now(),
            #             }
            #             new_r = self.pool["order.status.synch.log"].create(cr, s_uid, data)
            #         except:
            #             pass
            #     else:
            #         cr.execute("update sale_order set partially_invoiced_dispatch=TRUE where id=%s", ([sales.id]))
            #         cr.commit()
            #
            #         try:
            #             s_uid = 1
            #
            #             data = {
            #                 'odoo_order_id': sales.id,
            #                 'magento_id': str(order_number),
            #                 'order_state': str('delivered'),
            #                 'state_time': fields.datetime.now(),
            #             }
            #             new_r = self.pool["order.status.synch.log"].create(cr, s_uid, data)
            #         except:
            #             pass

            # else:
            if int(sales.invoiced_amount) == int(sales.amount_total):
                cr.execute("update sale_order set delivery_dispatch=TRUE where id=%s", ([sales.id]))
                cr.commit()
                cr.execute("update sale_order set partially_invoiced_dispatch=FALSE where id=%s", ([sales.id]))
                cr.commit()
                try:
                    s_uid = 1

                    data = {
                        'odoo_order_id': sales.id,
                        'magento_id': str(order_number),
                        'order_state': str('point_gain'),
                        'state_time': fields.datetime.now(),
                    }
                    new_r = self.pool["order.status.synch.log"].create(cr, s_uid, data)
                except:
                    pass
                # try:
                #     sale_order.action_status(cr, uid, [sales.id, ], 'delivered', context=context)
                # except:
                #     pass
            elif int(sales.invoiced_amount) < int(sales.amount_total) and int(sales.invoiced_amount) > 0:
                cr.execute("update sale_order set partially_invoiced_dispatch=TRUE where id=%s", ([sales.id]))
                cr.commit()

                try:
                    s_uid = 1

                    data = {
                        'odoo_order_id': sales.id,
                        'magento_id': str(order_number),
                        'order_state': str('delivered'),
                        'state_time': fields.datetime.now(),
                    }
                    new_r = self.pool["order.status.synch.log"].create(cr, s_uid, data)
                except:
                    pass
                # try:
                #     sale_order.action_status(cr, uid, [sales.id, ], 'partially_delivered', context=context)
                # except:
                #     pass

            # elif round(sales.invoiced_amount, 2) == round(sales_amount_total, 2) and sales.invoiced_amount > 0:
            #         cr.execute("update sale_order set delivery_dispatch=TRUE where id=%s", ([sales.id]))
            #         cr.commit()
            #         cr.execute("update sale_order set partially_invoiced_dispatch=FALSE where id=%s", ([sales.id]))
            #         cr.commit()
            #
            #         try:
            #             s_uid = 1
            #
            #             data = {
            #                 'odoo_order_id': sales.id,
            #                 'magento_id': str(order_number),
            #                 'order_state': str('delivered'),
            #                 'state_time': fields.datetime.now(),
            #             }
            #             new_r = self.pool["order.status.synch.log"].create(cr, s_uid, data)
            #         except:
            #             pass

                    # try:
                    #     sale_order.action_status(cr, uid, [sales.id, ], 'delivered', context=context)
                    # except:
                    #     pass

                ### Call Status Synch Funciotn


                # so_ids = [sales.id]

                # self.pool.get('order.item.wise.close').manual_order_status_sync(cr, uid, so_ids,
                #                                                                 context=None)  ## Ids means Sales Order ID

                ## Ends Here Status Synch Function

        cr.commit()
        return section_id

    def receipt_return(self, cr, uid, ids, context=None):

        invoice_id = None
        stock_picking_name = None
        invoice_name = None
        return_list = self.pool.get('wms.manifest.outbound').browse(cr, uid, ids, context=context)[0]

        if return_list.stock_return_id:
            raise osv.except_osv(_('Already Received!'), _('Already Received'))

        # cr.execute("select invoice_id from  wms_manifest_outbound_line where wms_manifest_outbound_id=%s", (ids))

        # invoice_ids = cr.fetchall()

        # new_inv_ids = [items[0] for items in invoice_ids]

        # if len(new_inv_ids) > 0:
        #     invoice_id = new_inv_ids[0]
        #
        #     cr.execute("select origin,number,name from  account_invoice where id=%s", ([invoice_id]))
        #     origin = cr.fetchall()
        #
        #     for item in origin:
        #         if item:
        #             stock_picking_name = item[0]
        #             invoice_name = item[1]
        #             order_name = item[2]

        # cr.execute("select id from  stock_picking where name=%s", ([stock_picking_name]))

        # stock_id_lists = [items[0] for items in cr.fetchall()]
        # stock_object = self.pool.get('stock.picking').browse(cr, uid, stock_id_lists, context=context)[0]
        #
        # product_dict = {}
        # product_return_moves = [[5, False, False]]
        # for items in return_list.outbound_line:
        #
        #     if items.quantity > items.delivered:
        #         product_dict = {}
        #         product_dict['product_id'] = items.product_id
        #         product_dict['quantity'] = items.quantity - items.delivered
        #         product_dict['lot_id'] = False
        #
        #         for st_items in stock_object.move_lines:
        #             if st_items.product_id.id == items.product_id:
        #                 product_dict['move_id'] = st_items.id
        #                 product_return_moves.append([0, False, product_dict])
        #                 product_dict = {}
        #
        # prepare_data = {'product_return_moves': product_return_moves,
        #                 'invoice_state': '2binvoiced'}
        #
        # return_pool = self.pool['stock.return.picking']
        # data = prepare_data
        #
        # values = prepare_data
        # context.update({
        #     'active_model': 'stock.picking',
        #     'active_ids': [stock_object.id],
        #     'active_id': stock_object.id,
        #     'refund': True
        # })
        #
        # created_id = self.pool['stock.return.picking'].create(cr, uid, values, context=context)
        #
        # return_stock_id = None
        # return_inv_id = None
        #
        # id_list = [created_id]
        # context['active_id'] = stock_object.id
        # stock_picking_id = return_pool._create_returns(cr, uid, id_list, context=context)
        # # 2 id is coming
        #
        # cr.commit()
        # picking = []
        #
        # if stock_picking_id:
        #     return_stock_id = stock_picking_id[0]
        #
        #     # Stock Transfer Starts from Here
        #     picking = [stock_picking_id[0]]
        #
        #     context.update({
        #
        #         'active_model': 'stock.picking',
        #         'active_ids': picking,
        #         'active_id': len(picking) and picking[0] or False
        #     })
        #
        #     created_id = self.pool['stock.transfer_details'].create(cr, uid, {
        #         'picking_id': len(picking) and picking[0] or False}, context)
        #
        #     if created_id:
        #         created_id = self.pool.get('stock.transfer_details').browse(cr, uid, created_id, context=context)
        #         if created_id.picking_id.state in ['assigned', 'partially_available']:
        #             # raise Warning(_('You cannot transfer a picking in state \'%s\'.') % created_id.picking_id.state)
        #
        #             processed_ids = []
        #             # Create new and update existing pack operations
        #             for lstits in [created_id.item_ids, created_id.packop_ids]:
        #                 for prod in lstits:
        #                     pack_datas = {
        #                         'product_id': prod.product_id.id,
        #                         'product_uom_id': prod.product_uom_id.id,
        #                         'product_qty': prod.quantity,
        #                         'package_id': prod.package_id.id,
        #                         'lot_id': prod.lot_id.id,
        #                         'location_id': prod.sourceloc_id.id,
        #                         'location_dest_id': prod.destinationloc_id.id,
        #                         'result_package_id': prod.result_package_id.id,
        #                         'date': prod.date if prod.date else datetime.now(),
        #                         'owner_id': prod.owner_id.id,
        #                     }
        #                     if prod.packop_id:
        #                         prod.packop_id.with_context(no_recompute=True).write(pack_datas)
        #                         processed_ids.append(prod.packop_id.id)
        #                     else:
        #                         pack_datas['picking_id'] = created_id.picking_id.id
        #                         packop_id = created_id.env['stock.pack.operation'].create(pack_datas)
        #                         processed_ids.append(packop_id.id)
        #             # Delete the others
        #             packops = created_id.env['stock.pack.operation'].search(
        #                 ['&', ('picking_id', '=', created_id.picking_id.id), '!', ('id', 'in', processed_ids)])
        #             packops.unlink()
        #
        #             # Execute the transfer of the picking
        #             created_id.picking_id.do_transfer()
        #
        #             # Stock Transfer End Here
        #
        #             # Invoice start Here
        #
        #         # if created_id.picking_id.invoice_state != 'invoiced':
        #         #     # Now Create the Invoice for this picking
        #         #     res = []
        #         #     picking_pool = self.pool.get('stock.picking')
        #         #     from datetime import date
        #         #     context['date_inv'] = date.today()
        #         #     context['inv_type'] = 'in_invoice'
        #         #     active_ids = [stock_picking_id[0]]
        #         #     res = picking_pool.action_invoice_create(cr, uid, active_ids,
        #         #                                              journal_id=1147,
        #         #                                              group=True,
        #         #                                              type='out_refund',
        #         #                                              context=context)
        #         #
        #         #     return_inv_id = res[0] if len(res) > 0 else None
        #
        #         if created_id.picking_id.invoice_state != 'invoiced':
        #             # Now Create the Invoice for this picking
        #             res = []
        #             picking_pool = self.pool.get('stock.picking')
        #             from datetime import date
        #             context['date_inv'] = date.today()
        #             context['inv_type'] = 'in_invoice'
        #             active_ids = [stock_picking_id[0]]
        #
        #             stock_invoice_onshipping_data ={
        #                 'journal_id': 1147,
        #                 'journal_type': 'sale_refund',
        #                 'invoice_date': date.today(),
        #                 'invoice_number': invoice_name
        #             }
        #
        #
        #             stock_invoice_onshipping_obj = self.pool.get("stock.invoice.onshipping")
        #             stock_invoice_onshipping_id = stock_invoice_onshipping_obj.create(cr, uid, stock_invoice_onshipping_data, context=context)
        #
        #             res = stock_invoice_onshipping_obj.open_invoice(cr, uid, [stock_invoice_onshipping_id], context)
        #
        #             return_inv_id = res if res else None
        #
        # stock_return_id = return_stock_id
        # invoice_return_id = return_inv_id
        #
        # cr.commit()
        # for wms_id in ids:
        #     cr.execute("update wms_manifest_outbound set invoice_return_id=%s,stock_return_id=%s where id=%s", ([invoice_return_id, stock_return_id, wms_id]))
        #     cr.commit()

        # Dispatch Delivered Flag update

        # rst_list = self.pool.get('stock.picking').browse(cr, uid, stock_id_lists, context=context)[0]

        so_name = return_list.name

        cr.execute("select id from  sale_order where client_order_ref=%s", ([so_name]))
        abc = cr.fetchall()
        sale_ids = [abc[0][0]]

        sales = self.pool.get('sale.order').browse(cr, uid, sale_ids, context=context)[0]

        sale_order = self.pool.get("sale.order")
        if int(sales.invoiced_amount) < int(sales.amount_total) and int(sales.invoiced_amount) > 0:
            cr.execute("update sale_order set partial_delivered=TRUE where id=%s", ([sales.id]))
            cr.commit()
            try:
                s_uid = 1

                data = {
                    'odoo_order_id': sales.id,
                    'magento_id': str(sales.client_order_ref),
                    'order_state': str('delivered'),
                    'state_time': fields.datetime.now(),
                }
                new_r = self.pool["order.status.synch.log"].create(cr, s_uid, data)
            except:
                pass

            # try:
            #     sale_order.action_status(cr, uid, [sales.id, ], 'partially_delivered', context=context)
            #
            # except:
            #     pass
        elif int(sales.invoiced_amount) > int(sales.amount_total) and int(sales.invoiced_amount) > 0:
            cr.execute("update sale_order set partial_delivered=TRUE where id=%s", ([sales.id]))
            cr.commit()
            try:
                s_uid = 1

                data = {
                    'odoo_order_id': sales.id,
                    'magento_id': str(sales.client_order_ref),
                    'order_state': str('delivered'),
                    'state_time': fields.datetime.now(),
                }
                new_r = self.pool["order.status.synch.log"].create(cr, s_uid, data)
            except:
                pass

            # try:
            #     sale_order.action_status(cr, uid, [sales.id, ], 'partially_delivered', context=context)
            # except:
            #     pass
        elif int(sales.delivered_amount) == int(sales.invoiced_amount):
            cr.execute("update sale_order set delivery_dispatch=TRUE where id=%s", ([sales.id]))
            # sale_order.action_status(cr, uid, [sales.id, ], 'delivered', context=context)

        if context.has_key('full_return') and context.get('full_return'):

            try:
                s_uid = 1
                data = {
                    'odoo_order_id': sales.id,
                    'magento_id': str(sales.client_order_ref),
                    'order_state': str('delivered'),
                    'state_time': fields.datetime.now(),
                }
                new_r = self.pool["order.status.synch.log"].create(cr, s_uid, data)
            except:
                pass

            # try:
            #     sale_order.action_status(cr, uid, [sales.id, ], 'delivered', context=context)
            # except:
            #     pass
        cr.commit()

        return True

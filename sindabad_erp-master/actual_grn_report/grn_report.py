
# Author Mufti Muntasir Ahmed 02-09-2020

from openerp import models, api


class sales_cogs_margin(models.Model):
    _name = 'grn.stock.picking'

    @api.model
    def _report_xls_fields(self):

        return [
            'po_no','po_confirmation_date','payment_status','grn_no','supplier','received_date','type','product_category','product_name','received_qty','price_unit','total_price'

        ]

    # Change/Add Template entries
    @api.model
    def _report_xls_template(self):

        return {}

# Author Mufti Muntasir Ahmed 02-09-2020

{
    'name': 'Purchase GRN Report',
    'version': '8.0.1',
    'author': "Mufti Muntasir Ahmed",
    'category': 'Purchase',
    'summary': 'Details Summary of GRN quantity with Cost',
    'depends': ['product', 'stock', 'sale','purchase', 'report_xls'],
    'data': [
            'security/actual_grn_report_security.xml',
            'security/ir.model.access.csv',
            'wizard/actual_grn_report_filter.xml',
            'report/actual_grn_report.xml',
    ],
    'installable': True,
    'auto_install': False,
    'application': False,
}


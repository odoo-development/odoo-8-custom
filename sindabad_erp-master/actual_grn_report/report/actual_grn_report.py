# Author Mufti Muntasir Ahmed 02-09-2020

from openerp import models, api
from datetime import datetime
import datetime


class ActualGRNReport(models.AbstractModel):
    _name = 'report.actual_grn_report.report_actual_grn'

    def get_pickings(self,cr,uid, docs):
        pickings_list = []

        query = "select (select name from res_partner where res_partner.id=stock_picking.partner_id) as customer_name,stock_picking.origin,stock_picking.partner_id,stock_picking.id,stock_picking.name,stock_picking.partner_id,stock_move.product_id,stock_picking.state,stock_move.name as move_name,stock_picking.date_done,stock_move.product_uom_qty,(select purchase_order.date_order from purchase_order where  purchase_order.id=purchase_order_line.order_id) as po_confirmation_date,(select purchase_order.adv_po_snd from purchase_order where  purchase_order.id=purchase_order_line.order_id) as payment_status,(select product_category.name from product_category, product_product,product_template where product_category.id=product_template.categ_id and  product_product.product_tmpl_id=product_template.id and  product_product.id=stock_move.product_id) as product_category, purchase_order_line.price_unit, (stock_move.product_uom_qty*purchase_order_line.price_unit) as total_price from stock_picking,stock_move,purchase_order_line where stock_picking.id=stock_move.picking_id and stock_picking.state='done' and stock_move.purchase_line_id=purchase_order_line.id and stock_picking.date_done >=%s and stock_picking.date_done <=%s"

        if docs:

            from_date = docs.from_date
            end_date = docs.to_date
            date_obj = datetime.datetime.strptime(end_date, '%Y-%m-%d') + datetime.timedelta(days=1)

            increased_date = date_obj.strftime('%Y-%m-%d')
            end_date = increased_date


            cr.execute(query, (from_date, end_date))
            grn_list = cr.dictfetchall()


            for items in grn_list:
                total_price = items.get('total_price')


                mov_name = items.get('name')
                grn_type = 'Received'

                if 'IN' not in mov_name:
                    grn_type='Return'
                    total_price = total_price *-1



                tmp_line= {
                    'po_no':items.get('origin'),
                    'grn_no':items.get('name'),
                    'supplier':items.get('customer_name'),
                    'received_date':items.get('date_done'),
                    'type':grn_type,
                    'product_name':items.get('move_name'),
                    'received_qty':items.get('product_uom_qty'),
                    'price_unit':items.get('price_unit'),
                    'total_price':total_price,
                    'po_confirmation_date':items.get('po_confirmation_date'),
                    'payment_status':items.get('payment_status'),
                    'product_category':items.get('product_category')

                }
                pickings_list.append(tmp_line)






        return pickings_list

    @api.model
    def render_html(self, docids, data=None):

        self.model = self.env.context.get('active_model')
        docs = self.env[self.model].browse(self.env.context.get('active_id'))
        pickings = self.get_pickings(docs)
        docargs = {
            'doc_ids': self.ids,
            'doc_model': self.model,
            'docs': docs,
            'pickings': pickings,
        }
        return self.env['report'].render('actual_grn_report.report_actual_grn', docargs)

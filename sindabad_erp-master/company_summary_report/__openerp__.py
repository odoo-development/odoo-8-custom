# -*- coding: utf-8 -*-
{
    'name': "Company Summary Report",

    'summary': """
       Company Summary Report""",

    'description': """
        Company Summary Report
    """,

    'author': "S. M. Sazedul Haque - Zero Gravity Ventures Ltd",
    'website': "http://zerogravity.com.bd/",
    'depends': ['account'],
    'data': [
        'company_menu.xml',
        'wizard/company_report_statement_view.xml',
        'views/report_company_summary.xml',
        'report/statement_layout.xml',
    ],

    'installable': True,
    'auto_install': False,
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:

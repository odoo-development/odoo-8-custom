from openerp.osv import fields, osv
from operator import itemgetter

class product_need_to_buy(osv.osv):
    _inherit = "product.need.to.buy"

    def emergency_need_to_buy_list(self, cr, uid, ids=None, context=None):
        query = "select stock_picking.picking_type_id,stock_move.product_id, sum((select sum(qty) from stock_quant where stock_quant.reservation_id=stock_move.id)) as reserved_qty, sum(stock_move.product_uom_qty) as total_order_qty, sum((select sum(qty) from stock_quant where stock_quant.location_id in (12,75,29,53,19,36,60,68) and stock_quant.product_id=stock_move.product_id)) as total_qty_on_hand, stock_picking_type.warehouse_id,(select sum(qty) from stock_quant as sq,stock_warehouse as sw where sq.location_id in (12,75,29,53,19,36,60,68) and sq.location_id= sw.lot_stock_id and sw.id=stock_picking_type.warehouse_id and sq.product_id=stock_move.product_id) as wh_on_hand_qty from stock_move,stock_picking,stock_picking_type where  stock_move.state not in ('draft','cancel','done') and stock_move.picking_id = stock_picking.id and stock_picking.picking_type_id in (2,8,14,19,24,29,34,39) and stock_picking.picking_type_id =stock_picking_type.id group by stock_picking.picking_type_id,stock_move.product_id,stock_picking_type.warehouse_id"


        cr.execute(query)

        all_data = cr.fetchall()


        #### All incoming PO's QTY


        in_query = "select stock_move.product_id, stock_picking.picking_type_id,sum(stock_move.product_qty) as total_qty from stock_picking,stock_move where stock_picking.id = stock_move.picking_id  and  stock_move.state !='draft' and stock_move.state !='cancel' and stock_move.state !='done' and stock_picking.picking_type_id in (1,7,13,18,23,28,33,38)  group by stock_move.product_id,stock_picking.picking_type_id"

        cr.execute(in_query)

        all_incoming_data = cr.fetchall()


        #########


        product_list = [ items[1] for items in all_data]

        product_list = list(dict.fromkeys(product_list))


        product_query = "select product_product.id,product_product.default_code,product_template.categ_id from product_product,product_template where product_product.id in %s and product_product.product_tmpl_id=product_template.id"

        cr.execute(product_query,(tuple(product_list),))

        all_product_list = cr.fetchall()

        b2b_order_id_query = "select sale_order.id,stock_move.product_id,stock_move.state from sale_order, stock_picking,stock_move where sale_order.procurement_group_id=stock_picking.group_id and stock_picking.id = stock_move.picking_id and  (stock_move.state='waiting' or stock_move.state='confirmed')"

        cr.execute(b2b_order_id_query)
        b2b_order_id_query_list = cr.fetchall()



        product_dict ={} ## it contains key as p_id and values as dict

        for all_item in all_data:


            wh_quantity = all_item[6] if all_item[6] is not None else 0
            wh_forcast_quantity= all_item[3] if all_item[3] is not None else 0
            wh_free_quantity= (wh_quantity- wh_forcast_quantity) if (wh_quantity- wh_forcast_quantity) >0 else 0



            if product_dict.get(all_item[1]) is None:
                p_code=''
                p_cat_id=''
                total_ordered_qty = 0
                total_need_to_buy=0
                order_ref_list=[]

                for b2b_items in b2b_order_id_query_list:
                    if b2b_items[1]== all_item[1]:
                        order_ref_list.append(b2b_items[0])
                if len(order_ref_list)>0:
                    order_ref_list = str(order_ref_list)
                else:
                    order_ref_list = None




                for p_in_item in all_product_list:
                    if p_in_item[0] == all_item[1]:

                        p_code = p_in_item[1]
                        p_cat_id = p_in_item[2]
                        break

                product_dict[all_item[1]]={
                    'name':all_item[1],
                    'default_code':p_code,
                    'prod_uom':'',
                    'categ_id':p_cat_id,
                    'lst_price':0,
                    'standard_price':0,
                    'incoming_qty':0,
                    'outgoing_qty':0,
                    'calculate_qty_available':0,
                    'calculate_virtual_available':0,
                    'calculate_damage_quantity':0,
                    'uttwh_quantity':0,
                    'uttwh_forcast_quantity':0,
                    'uttwh_free_qty':0,
                    'uttwh_retail_quantity':0,
                    'uttwh_retail_forcast_quantity':0,
                    'uttwh_retail_free_quantity':0,
                    'nodda_quantity':0,
                    'nodda_forcast_quantity':0,
                    'nodda_free_quantity':0,
                    'nodda_retail_quantity':0,
                    'nodda_retail_forcast_quantity':0,
                    'nodda_retail_free_quantity':0,
                    'moti_quantity':0,
                    'moti_forcast_quantity':0,
                    'moti_free_quantity':0,
                    'moti_retail_quantity':0,
                    'moti_retail_forcast_quantity':0,
                    'moti_retail_free_quantity':0,
                    'partner_id':0,
                    'pricelist_id':0,
                    'picking_type_id':0,
                    'adv_po_snd':0,
                    'location_id':0,
                    'po_line_no':0,
                    'image_small':0,
                    'po_line_prod_uom':0,
                    'system_generated_po':0,
                    'stn_true':0,
                    'ordered_qty':total_ordered_qty,
                    'total_need_to_buy':total_need_to_buy,
                    'b2b_order_ids':order_ref_list,
                }





            if all_item[5]==1:
                in_qty=0

                for all_in_item in all_incoming_data:
                    if all_item[1]== all_in_item[0] and all_in_item[1]==1:
                        in_qty = all_in_item[2]
                        break
                cal_culated_qty = wh_forcast_quantity - in_qty - wh_quantity


                product_dict[all_item[1]]['uttwh_quantity'] = wh_quantity
                product_dict[all_item[1]]['uttwh_forcast_quantity'] = cal_culated_qty
                product_dict[all_item[1]]['uttwh_free_quantity'] = wh_free_quantity
                product_dict[all_item[1]]['total_need_to_buy'] = product_dict[all_item[1]]['total_need_to_buy'] + (cal_culated_qty if cal_culated_qty >0 else 0)
                product_dict[all_item[1]]['ordered_qty'] = product_dict[all_item[1]]['ordered_qty'] + all_item[3]


            if all_item[5]==2:
                in_qty = 0

                for all_in_item in all_incoming_data:
                    if all_item[1] == all_in_item[0] and all_in_item[1] == 7:
                        in_qty = all_in_item[2]
                        break
                cal_culated_qty = wh_forcast_quantity - in_qty - wh_quantity
                product_dict[all_item[1]]['nodda_quantity'] = wh_quantity
                product_dict[all_item[1]]['nodda_forcast_quantity'] = cal_culated_qty
                product_dict[all_item[1]]['nodda_free_quantity'] = wh_free_quantity
                product_dict[all_item[1]]['total_need_to_buy'] = product_dict[all_item[1]]['total_need_to_buy'] + (cal_culated_qty if cal_culated_qty >0 else 0)
                product_dict[all_item[1]]['ordered_qty'] = product_dict[all_item[1]]['ordered_qty'] + all_item[3]


            if all_item[5] == 3:
                in_qty = 0

                for all_in_item in all_incoming_data:
                    if all_item[1] == all_in_item[0] and all_in_item[1] == 13:
                        in_qty = all_in_item[2]
                        break
                cal_culated_qty = wh_forcast_quantity - in_qty - wh_quantity
                product_dict[all_item[1]]['nodda_retail_quantity'] = wh_quantity
                product_dict[all_item[1]]['nodda_retail_forcast_quantity'] = cal_culated_qty
                product_dict[all_item[1]]['nodda_retail_free_quantity'] = wh_free_quantity
                product_dict[all_item[1]]['total_need_to_buy'] = product_dict[all_item[1]]['total_need_to_buy'] + (cal_culated_qty if cal_culated_qty >0 else 0)
                product_dict[all_item[1]]['ordered_qty'] = product_dict[all_item[1]]['ordered_qty'] + all_item[3]

            if all_item[5] == 4:
                in_qty = 0

                for all_in_item in all_incoming_data:
                    if all_item[1] == all_in_item[0] and all_in_item[1] == 18:
                        in_qty = all_in_item[2]
                        break
                cal_culated_qty = wh_forcast_quantity - in_qty - wh_quantity
                product_dict[all_item[1]]['moti_quantity'] = wh_quantity
                product_dict[all_item[1]]['moti_forcast_quantity'] = cal_culated_qty
                product_dict[all_item[1]]['moti_free_quantity'] = wh_free_quantity
                product_dict[all_item[1]]['total_need_to_buy'] = product_dict[all_item[1]]['total_need_to_buy'] + (cal_culated_qty if cal_culated_qty >0 else 0)
                product_dict[all_item[1]]['ordered_qty'] = product_dict[all_item[1]]['ordered_qty'] + all_item[3]

            if all_item[5] == 6:
                in_qty = 0

                for all_in_item in all_incoming_data:
                    if all_item[1] == all_in_item[0] and all_in_item[1] == 23:
                        in_qty = all_in_item[2]
                        break
                cal_culated_qty = wh_forcast_quantity - in_qty - wh_quantity
                product_dict[all_item[1]]['uttwh_retail_quantity'] = wh_quantity
                product_dict[all_item[1]]['uttwh_retail_forcast_quantity'] = cal_culated_qty
                product_dict[all_item[1]]['uttwh_retail_free_quantity'] = wh_free_quantity
                product_dict[all_item[1]]['total_need_to_buy'] = product_dict[all_item[1]]['total_need_to_buy'] + (cal_culated_qty if cal_culated_qty >0 else 0)
                product_dict[all_item[1]]['ordered_qty'] = product_dict[all_item[1]]['ordered_qty'] + all_item[3]


            if all_item[5] == 7:
                in_qty = 0

                for all_in_item in all_incoming_data:
                    if all_item[1] == all_in_item[0] and all_in_item[1] == 28:
                        in_qty = all_in_item[2]
                        break
                cal_culated_qty = wh_forcast_quantity - in_qty - wh_quantity
                product_dict[all_item[1]]['moti_retail_quantity'] = wh_quantity
                product_dict[all_item[1]]['moti_retail_forcast_quantity'] = cal_culated_qty
                product_dict[all_item[1]]['moti_retail_free_quantity'] = wh_free_quantity
                product_dict[all_item[1]]['total_need_to_buy'] = product_dict[all_item[1]]['total_need_to_buy'] + (cal_culated_qty if cal_culated_qty >0 else 0)
                product_dict[all_item[1]]['ordered_qty'] = product_dict[all_item[1]]['ordered_qty'] + all_item[3]




        stock_query="select stock_quant.product_id,sum(stock_quant.qty) as total_qty,stock_quant.location_id from stock_quant,stock_location where stock_quant.location_id=stock_location.id and stock_location.usage='internal' and stock_quant.product_id in %s group by stock_quant.product_id,stock_quant.location_id"

        cr.execute(stock_query,(tuple(product_list),))

        stock_all_data = cr.fetchall()
        update_stock = 0


        for stock_item in stock_all_data:

            for prod_id, all_item in product_dict.iteritems():

                if stock_item[0]==prod_id:
                    update_stock = 0
                    if stock_item[2] == 12:
                        if product_dict[prod_id]['uttwh_forcast_quantity'] == 0:
                            product_dict[prod_id]['uttwh_quantity'] = stock_item[1]
                            product_dict[prod_id]['uttwh_free_quantity'] = stock_item[1]
                            update_stock=1

                    if stock_item[2] == 19:
                        if product_dict[prod_id]['nodda_forcast_quantity'] == 0:
                            product_dict[prod_id]['nodda_quantity'] = stock_item[1]
                            product_dict[prod_id]['nodda_free_quantity'] = stock_item[1]
                            update_stock=1

                    if stock_item[2] == 29:
                        if product_dict[prod_id]['nodda_retail_forcast_quantity'] == 0:
                            product_dict[prod_id]['nodda_retail_quantity'] = stock_item[1]
                            product_dict[prod_id]['nodda_retail_free_quantity'] = stock_item[1]
                            update_stock=1

                    if stock_item[2] == 36:
                        if product_dict[prod_id]['moti_forcast_quantity'] == 0:
                            product_dict[prod_id]['moti_quantity'] = stock_item[1]
                            product_dict[prod_id]['moti_free_quantity'] = stock_item[1]
                            update_stock=1

                    if stock_item[2] == 53:
                        if product_dict[prod_id]['uttwh_retail_forcast_quantity'] == 0:
                            product_dict[prod_id]['uttwh_retail_quantity'] = stock_item[1]
                            product_dict[prod_id]['uttwh_retail_free_quantity'] = stock_item[1]
                            update_stock=1

                    if stock_item[2] == 60:
                        if product_dict[prod_id]['moti_retail_forcast_quantity'] == 0:
                            product_dict[prod_id]['moti_retail_quantity'] = stock_item[1]
                            product_dict[prod_id]['moti_retail_free_quantity'] = stock_item[1]
                            update_stock=1


        stn_data_list = list()
        stn_process_query = "SELECT id FROM stn_process WHERE stock_id IN \
(SELECT picking_id FROM stock_move \
WHERE picking_id IN (SELECT stock_id FROM stn_process WHERE state = 'requested' OR state = 'send'))"
        cr.execute(stn_process_query)
        for d in cr.fetchall():
            stn_data_list.append(d[0])

        stock_move_obj = self.pool.get('stock.move')
        stn_data = self.pool.get('stn.process').browse(cr, uid, stn_data_list, context=context)

        # product wise data process
        # 'uttwh_in_qty'
        # 'uttwh_retail_in_qty'
        # 'uttwh_stn_in_qty'
        # 'uttwh_retail_stn_in_qty'
        # 'nodda_in_qty'
        # 'nodda_retail_in_qty'
        # 'nodda_stn_qty'
        # 'nodda_retail_stn_in_qty'

        stn_data_dict = dict()

        for s_d in stn_data:

            name_split = str(s_d.name).split("/")
            des_to = name_split[1]
            des_from = name_split[2]

            for line in s_d.stn_process_line:

                if line.product_id.id in stn_data_dict:
                    # prepare data for existing data
                    # assigning STN quantity
                    stock_mv_list = stock_move_obj.search(cr, uid, ['&', ('picking_id', '=', s_d.stock_id.id), ('product_id', '=', line.product_id.id)])
                    if stock_mv_list and len(stock_mv_list) == 1:
                        stk_mv = stock_move_obj.browse(cr, uid, stock_mv_list, context=context)
                        if des_to == 'UttWH':
                            stn_data_dict[line.product_id.id]['uttwh_stn_in_qty'] = stn_data_dict[line.product_id.id]['uttwh_stn_in_qty'] + stk_mv.product_qty
                        if des_to == 'UTTRT':
                            stn_data_dict[line.product_id.id]['uttwh_retail_stn_in_qty'] = stn_data_dict[line.product_id.id]['uttwh_retail_stn_in_qty'] + stk_mv.product_qty
                        if des_to == 'NODDA':
                            stn_data_dict[line.product_id.id]['nodda_stn_in_qty'] = stn_data_dict[line.product_id.id]['nodda_stn_in_qty'] + stk_mv.product_qty
                        if des_to == 'NODRT':
                            stn_data_dict[line.product_id.id]['nodda_retail_stn_in_qty'] = stn_data_dict[line.product_id.id]['nodda_retail_stn_in_qty'] + stk_mv.product_qty

                        if des_from == 'UttWH':
                            stn_data_dict[line.product_id.id]['uttwh_stn_in_qty'] = stn_data_dict[line.product_id.id]['uttwh_stn_in_qty'] + stk_mv.product_qty
                        if des_from == 'UTTRT':
                            stn_data_dict[line.product_id.id]['uttwh_retail_stn_in_qty'] = stn_data_dict[line.product_id.id]['uttwh_retail_stn_in_qty'] + stk_mv.product_qty
                        if des_from == 'NODDA':
                            stn_data_dict[line.product_id.id]['nodda_stn_in_qty'] = stn_data_dict[line.product_id.id]['nodda_stn_in_qty'] + stk_mv.product_qty
                        if des_from == 'NODRT':
                            stn_data_dict[line.product_id.id]['nodda_retail_stn_in_qty'] = stn_data_dict[line.product_id.id]['nodda_retail_stn_in_qty'] + stk_mv.product_qty

                else:
                    # ['STN', 'UTTRT', 'NODRT', '04294']
                    # 'UttWH', 'UTTRT', 'NODDA', 'NODRT',
                    tmp_dict = dict()

                    # Filter "Pending PO trace" and Status "Available"
                    # tmp_dict['uttwh_in_qty'] = 0  # Stock move "Quandity" PO incoming
                    # tmp_dict['uttwh_retail_in_qty'] = 0  # Stock move "Quandity" PO incoming
                    # tmp_dict['nodda_in_qty'] = 0  # Stock move "Quandity" PO incoming
                    # tmp_dict['nodda_retail_in_qty'] = 0  # Stock move "Quandity" PO incoming

                    # For stn quantity
                    tmp_dict['uttwh_stn_in_qty'] = 0  # Stock move "Quantity"
                    tmp_dict['uttwh_retail_stn_in_qty'] = 0  # Stock move "Quantity"
                    tmp_dict['nodda_stn_in_qty'] = 0  # Stock move "Quantity"
                    tmp_dict['nodda_retail_stn_in_qty'] = 0  # Stock move "Quantity"

                    # assigning STN quantity
                    stock_mv_list = stock_move_obj.search(cr, uid, ['&', ('picking_id', '=', s_d.stock_id.id), ('product_id', '=', line.product_id.id)])

                    if stock_mv_list and len(stock_mv_list) == 1:
                        stk_mv = stock_move_obj.browse(cr, uid, stock_mv_list, context=context)
                        # if len(stk_mv) > 1:
                        #    import pdb;pdb.set_trace()

                        if des_to == 'UttWH':
                            tmp_dict['uttwh_stn_in_qty'] = stk_mv.product_qty
                        if des_to == 'UTTRT':
                            tmp_dict['uttwh_retail_stn_in_qty'] = stk_mv.product_qty
                        if des_to == 'NODDA':
                            tmp_dict['nodda_stn_in_qty'] = stk_mv.product_qty
                        if des_to == 'NODRT':
                            tmp_dict['nodda_retail_stn_in_qty'] = stk_mv.product_qty

                        if des_from == 'UttWH':
                            tmp_dict['uttwh_stn_in_qty'] = stk_mv.product_qty
                        if des_from == 'UTTRT':
                            tmp_dict['uttwh_retail_stn_in_qty'] = stk_mv.product_qty
                        if des_from == 'NODDA':
                            tmp_dict['nodda_stn_in_qty'] = stk_mv.product_qty
                        if des_from == 'NODRT':
                            tmp_dict['nodda_retail_stn_in_qty'] = stk_mv.product_qty

                    stn_data_dict[line.product_id.id] = tmp_dict



        # PO incoming (Pending PO Trace)
        # [['state', '!=', 'cancel'], ['state', '!=', 'done'], ['origin', 'ilike', 'PO'], ['location_id', 'ilike', 'Partner']]
        # stock.move
        po_in_data_dict = dict()
        po_in_list = stock_move_obj.search(cr, uid, ['&', '&', '&', ('state', '!=', 'cancel'), ('state', '!=', 'done'), ('origin', 'ilike', 'PO'), ('location_id', 'ilike', 'Partner')])
        for po_in in stock_move_obj.browse(cr, uid, po_in_list):

            pick_query = "SELECT name FROM stock_picking WHERE id={0}".format(po_in.picking_id.id)
            cr.execute(pick_query)
            pick_name = ''
            for pn in cr.fetchall():
                pick_name = str(pn[0])

            p_name_split = str(pick_name).split("/")
            p_des_to = p_name_split[0]

            if po_in.product_id.id in po_in_data_dict:

                # already exists then add
                if p_des_to == 'UttWH':
                    po_in_data_dict[po_in.product_id.id]['uttwh_in_qty'] = po_in_data_dict[po_in.product_id.id]['uttwh_in_qty'] + po_in.product_qty
                if p_des_to == 'UTTRT':
                    po_in_data_dict[po_in.product_id.id]['uttwh_retail_in_qty'] = po_in_data_dict[po_in.product_id.id]['uttwh_retail_in_qty'] + po_in.product_qty
                if p_des_to == 'NODDA':
                    po_in_data_dict[po_in.product_id.id]['nodda_in_qty'] = po_in_data_dict[po_in.product_id.id]['nodda_in_qty'] + po_in.product_qty
                if p_des_to == 'NODRT':
                    po_in_data_dict[po_in.product_id.id]['nodda_retail_in_qty'] = po_in_data_dict[po_in.product_id.id]['nodda_retail_in_qty'] + po_in.product_qty

            else:
                tmp_po_dict = dict()

                # Filter "Pending PO trace" and Status "Available"
                tmp_po_dict['uttwh_in_qty'] = 0  # Stock move "Quandity" PO incoming
                tmp_po_dict['uttwh_retail_in_qty'] = 0  # Stock move "Quandity" PO incoming
                tmp_po_dict['nodda_in_qty'] = 0  # Stock move "Quandity" PO incoming
                tmp_po_dict['nodda_retail_in_qty'] = 0  # Stock move "Quandity" PO incoming

                # 'UttWH', 'UTTRT', 'NODDA', 'NODRT'
                if p_des_to == 'UttWH':
                    tmp_po_dict['uttwh_in_qty'] = po_in.product_qty
                if p_des_to == 'UTTRT':
                    tmp_po_dict['uttwh_retail_in_qty'] = po_in.product_qty
                if p_des_to == 'NODDA':
                    tmp_po_dict['nodda_in_qty'] = po_in.product_qty
                if p_des_to == 'NODRT':
                    tmp_po_dict['nodda_retail_in_qty'] = po_in.product_qty

                po_in_data_dict[po_in.product_id.id] = tmp_po_dict

        # Required QTY
        pending_quty_dict = dict()
        prod_obj = self.pool.get('product.product')

        cr.execute('delete from product_need_to_buy')
        cr.commit()
        for prod_id,product_item in product_dict.iteritems():

            # For PO incoming
            uttwh_in_qty = po_in_data_dict[prod_id]['uttwh_in_qty'] if prod_id in po_in_data_dict else 0
            uttwh_retail_in_qty = po_in_data_dict[prod_id]['uttwh_retail_in_qty'] if prod_id in po_in_data_dict else 0
            nodda_in_qty = po_in_data_dict[prod_id]['nodda_in_qty'] if prod_id in po_in_data_dict else 0
            nodda_retail_in_qty = po_in_data_dict[prod_id]['nodda_retail_in_qty'] if prod_id in po_in_data_dict else 0

            # For stn quantity
            uttwh_stn_in_qty = stn_data_dict[prod_id]['uttwh_stn_in_qty'] if prod_id in stn_data_dict else 0
            uttwh_retail_stn_in_qty = stn_data_dict[prod_id]['uttwh_retail_stn_in_qty'] if prod_id in stn_data_dict else 0
            nodda_stn_in_qty = stn_data_dict[prod_id]['nodda_stn_in_qty'] if prod_id in stn_data_dict else 0
            nodda_retail_stn_in_qty = stn_data_dict[prod_id]['nodda_retail_stn_in_qty'] if prod_id in stn_data_dict else 0

            # Required QTY
            prod = prod_obj.browse(cr, uid, prod_id, context=context)
            pending_qty = prod.pending_qty
            uttwh_free_quantity = prod.uttwh_free_quantity
            nodda_free_quantity = prod.nodda_free_quantity

            uttwh_required_qty = pending_qty - uttwh_free_quantity
            nodda_required_qty = pending_qty - nodda_free_quantity

            # Warehouse wise Need to buy
            uttwh_need_to_buy = uttwh_required_qty - uttwh_stn_in_qty - uttwh_in_qty
            nodda_need_to_buy = nodda_required_qty - nodda_stn_in_qty - nodda_in_qty


            ntbl_insert_query = "INSERT INTO product_need_to_buy (name, default_code, prod_uom, categ_id, lst_price, standard_price, incoming_qty, outgoing_qty, calculate_qty_available, calculate_virtual_available, calculate_damage_quantity, uttwh_quantity, uttwh_forcast_quantity, uttwh_free_quantity, uttwh_retail_quantity, uttwh_retail_forcast_quantity, uttwh_retail_free_quantity, nodda_quantity, nodda_forcast_quantity, nodda_free_quantity, nodda_retail_quantity, nodda_retail_forcast_quantity, nodda_retail_free_quantity, moti_quantity, moti_forcast_quantity, moti_free_quantity, moti_retail_quantity, moti_retail_forcast_quantity, moti_retail_free_quantity,total_need_to_buy,ordered_qty,b2b_order_ids, \
uttwh_in_qty, uttwh_retail_in_qty, nodda_in_qty, nodda_retail_in_qty, uttwh_stn_in_qty, uttwh_retail_stn_in_qty, nodda_stn_in_qty, nodda_retail_stn_in_qty, uttwh_required_qty, nodda_required_qty, uttwh_need_to_buy, nodda_need_to_buy) VALUES ({0}, $${1}$$,'{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}','{11}','{12}','{13}','{14}','{15}','{16}','{17}','{18}','{19}','{20}','{21}','{22}','{23}','{24}','{25}','{26}','{27}','{28}','{29}','{30}','{31}', \
'{32}','{33}','{34}','{35}','{36}','{37}','{38}','{39}', '{40}', '{41}', '{42}', '{43}')".format(
                product_item.get('name'),
                product_item.get('default_code'),
                product_item.get('prod_uom'),
                product_item.get('categ_id'),
                product_item.get('lst_price'),
                product_item.get('standard_price'),
                product_item.get('incoming_qty'),
                product_item.get('outgoing_qty'),
                product_item.get('calculate_qty_available'),
                product_item.get('calculate_virtual_available'),
                product_item.get('calculate_damage_quantity'),
                product_item.get('uttwh_quantity'),
                product_item.get('uttwh_forcast_quantity'),
                product_item.get('uttwh_free_qty'),
                product_item.get('uttwh_retail_quantity'),
                product_item.get('uttwh_retail_forcast_quantity'),
                product_item.get('uttwh_retail_free_quantity'),
                product_item.get('nodda_quantity'),
                product_item.get('nodda_forcast_quantity'),
                product_item.get('nodda_free_quantity'),
                product_item.get('nodda_retail_quantity'),
                product_item.get('nodda_retail_forcast_quantity'),
                product_item.get('nodda_retail_free_quantity'),
                product_item.get('moti_quantity'),
                product_item.get('moti_forcast_quantity'),
                product_item.get('moti_free_quantity'),
                product_item.get('moti_retail_quantity'),
                product_item.get('moti_retail_forcast_quantity'),
                product_item.get('moti_retail_free_quantity'),
                product_item.get('total_need_to_buy'),
                product_item.get('ordered_qty'),
                product_item.get('b2b_order_ids'),
                uttwh_in_qty,
                uttwh_retail_in_qty,
                nodda_in_qty,
                nodda_retail_in_qty,
                uttwh_stn_in_qty,
                uttwh_retail_stn_in_qty,
                nodda_stn_in_qty,
                nodda_retail_stn_in_qty,
                uttwh_required_qty,
                nodda_required_qty,
                uttwh_need_to_buy,
                nodda_need_to_buy

            )

            cr.execute(ntbl_insert_query)
            cr.commit()

        return True

    _columns = {
        'total_need_to_buy': fields.float(string='Total QTY Need To Buy'),
        'ordered_qty': fields.float(string='Total Ordered QTY'),
        'b2b_order_ids': fields.char(string='Total B2B Order Reference List'),

        'uttwh_in_qty': fields.float(string='Uttara incoming QTY'),
        'uttwh_retail_in_qty': fields.float(string='Uttara retail incoming QTY'),
        'uttwh_stn_in_qty': fields.float(string='Uttara STN incoming QTY'),
        'uttwh_retail_stn_in_qty': fields.float(string='Uttara retail STN incoming QTY'),
        'nodda_in_qty': fields.float(string='Nodda incoming QTY'),
        'nodda_retail_in_qty': fields.float(string='Nodda retail incoming QTY'),
        'nodda_stn_in_qty': fields.float(string='Nodda STN incoming QTY'),
        'nodda_retail_stn_in_qty': fields.float(string='Nodda retail STN incoming QTY'),

        'uttwh_required_qty': fields.float(string='Uttara Required quanity'),
        'nodda_required_qty': fields.float(string='Nodda Required quanity'),

        'uttwh_need_to_buy': fields.float(string='Uttara need to buy'),
        'uttwh_retail_need_to_buy': fields.float(string='Uttara retail need to buy'),
        'nodda_need_to_buy': fields.float(string='Nodda need to buy'),
        'nodda_retail_need_to_buy': fields.float(string='Nodda retail need to buy'),

    }










{
    'name': 'Create Purchase Order from Need to buy list',
    'version': '1.0',
    'category': 'Tools',
    'summary': """Create Purchase Order from Need to buy list""",
    'description': """Create Purchase Order from Need to buy list""",
    'author': 'Mufti Muntasir Ahmed and Rockibul Alam',
    'depends': ['base','sale','purchase', 'odoo_magento_connect', 'sindabad_customization'],
    'data': [

        'security/po_create_ntbl_security.xml',
        'security/ir.model.access.csv',

        'wizard/po_create_wizard_view.xml',
        'need_to_buy_list_inherit_view.xml',

    ],

    'installable': True,
    'auto_install': False,

}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:

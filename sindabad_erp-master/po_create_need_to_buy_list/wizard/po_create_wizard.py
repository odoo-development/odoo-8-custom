import logging
from datetime import datetime
from dateutil.relativedelta import relativedelta
from operator import itemgetter
import time

import openerp
from openerp import SUPERUSER_ID, api
from openerp import tools
from openerp.osv import fields, osv, expression
from openerp.tools.translate import _
from openerp.tools.float_utils import float_round as round

from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT, DATETIME_FORMATS_MAP
from openerp.tools.float_utils import float_compare
import openerp.addons.decimal_precision as dp

_logger = logging.getLogger(__name__)


class po_create_need_to_buy_list(osv.osv_memory):
    _name = "po.create.need.to.buy.list"
    _description = "PO Create From Need to Buy List"



    
    
    _columns = {
        'partner_id': fields.many2one('res.partner', 'Supplier', required=True),
        'date_order': fields.datetime('Date Order', required=True),
        'line_ids': fields.one2many('po.create.need.to.buy.list.line','line_id', 'Product Lines', required=True),
        'location_id': fields.many2one('stock.location', 'Destination', domain=[('usage','<>','view')]),
        'picking_type_id': fields.many2one('stock.picking.type', 'Deliver To', help="This will determine picking type of incoming shipment"),
        'pricelist_id':fields.many2one('product.pricelist', 'Pricelist', help="The pricelist sets the currency used for this purchase order. It also computes the supplier price for the selected products/quantities."),
        'currency_id': fields.many2one('res.currency','Currency', readonly=True),
        'client_order_ref': fields.char('Reference/Description'),
        'product_uom': fields.char('Product Unit of Measure'),
        
    }
    
    _defaults = {
        'date_order': fields.datetime.now,
        
        'location_id': lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).company_id.partner_id.property_stock_customer.id,

        'currency_id': lambda self, cr, uid, context: self.pool.get('res.users').browse(cr, uid, uid, context=context).company_id.currency_id.id,

    }

    def default_get(self, cr, uid, fields, context=None):

        if context is None:
            context = {}

        res = super(po_create_need_to_buy_list, self).default_get(cr, uid, fields, context=context)

        order = self.pool.get('product.need.to.buy').browse(cr, uid, context['active_ids'], context=context)



        items = []

        for line in order:
            remaining_qty = line.ordered_qty

            ## Chack Already Generated PO

            query = "select purchase_order_line.product_id,purchase_order.picking_type_id,sum(purchase_order_line.product_qty) as total_qty from purchase_order,purchase_order_line where purchase_order.id=purchase_order_line.order_id and purchase_order.state ='draft' and purchase_order.system_generated_po = True and purchase_order_line.product_id =%s group by  purchase_order_line.product_id, purchase_order.picking_type_id"

            cr.execute(query, ([line.name.id]))

            prd_info = cr.fetchall()

            if len(prd_info) >0:
                generated_qty = prd_info[0][1]

                remaining_qty = line.total_need_to_buy




            ## Ends Here
            if remaining_qty >0:

                item = {
                    'product_id': line.name.id,
                    'qty': remaining_qty,
                    'product_uom': str(line.prod_uom),
                    'price': 0,
                    'subtotal': 0

                }

                if line.name:
                    items.append(item)

        res.update(line_ids=items)

        return res



    def po_create(self, cr, uid, ids, context):
        if context is None:
            context = {}


        po_data ={
            'partner_id':None,
            'date_order':None,
            'picking_type_id':None,
            'pricelist_id':None,
            'description':None,
            'order_line':None,
            'system_generated_po':True,

        }

        need_to_buy_obj = self.pool['po.create.need.to.buy.list'].browse(cr, uid, ids, context=context)

        utt_order_line = []
        uttrt_order_line = []
        nodd_order_line = []
        noddrt_order_line = []
        moti_order_line = []
        motirt_order_line = []
        saved_po_ids=[]


        for need_buy in need_to_buy_obj:
            po_data = {
                'partner_id': need_buy.partner_id.id,
                'date_order': fields.datetime.now(),
                'picking_type_id': None,
                'location_id': None,
                'pricelist_id': 2,
                'description': None,
                'order_line': None,
                'system_generated_po': True,
                'adv_po_snd': 'credit',

            }
            order_line=[]


            for o_line in need_buy.line_ids:
                need_to_buy_obj = self.pool['product.need.to.buy']

                need_ids = need_to_buy_obj.search(cr, uid, [('name', '=', o_line.product_id.id)], context=context)


                need_to_buy_list = need_to_buy_obj.browse(cr, uid, need_ids, context=context)[0]



                if need_to_buy_list.uttwh_forcast_quantity >0:
                   utt_order_line.append([0, False,{
                        'product_id':o_line.product_id.id,
                        'name':o_line.product_id.name,
                        'price_unit':o_line.price,
                        'product_qty':need_to_buy_list.uttwh_forcast_quantity,
                        'date_planned':fields.datetime.now()
                    }])

                if need_to_buy_list.uttwh_retail_forcast_quantity >0:
                    uttrt_order_line.append([0, False,{
                        'product_id':o_line.product_id.id,
                        'name':o_line.product_id.name,
                        'price_unit':o_line.price,
                        'product_qty':need_to_buy_list.uttwh_retail_forcast_quantity,
                        'date_planned':fields.datetime.now()
                    }])

                if need_to_buy_list.nodda_forcast_quantity >0:
                    nodd_order_line.append([0, False,{
                        'product_id':o_line.product_id.id,
                        'name':o_line.product_id.name,
                        'price_unit':o_line.price,
                        'product_qty':need_to_buy_list.nodda_forcast_quantity,
                        'date_planned':fields.datetime.now()
                    }])

                if need_to_buy_list.nodda_retail_forcast_quantity >0:
                    noddrt_order_line.append([0, False,{
                        'product_id':o_line.product_id.id,
                        'name':o_line.product_id.name,
                        'price_unit':o_line.price,
                        'product_qty':need_to_buy_list.nodda_retail_forcast_quantity,
                        'date_planned':fields.datetime.now()
                    }])

                if need_to_buy_list.moti_forcast_quantity >0:
                    moti_order_line.append([0, False,{
                        'product_id':o_line.product_id.id,
                        'name':o_line.product_id.name,
                        'price_unit':o_line.price,
                        'product_qty':need_to_buy_list.moti_forcast_quantity,
                        'date_planned':fields.datetime.now()
                    }])

                if need_to_buy_list.moti_retail_forcast_quantity >0:
                    motirt_order_line.append([0, False,{
                        'product_id':o_line.product_id.id,
                        'name':o_line.product_id.name,
                        'price_unit':o_line.price,
                        'product_qty':need_to_buy_list.moti_retail_forcast_quantity,
                        'date_planned':fields.datetime.now()
                    }])

        

        if len(utt_order_line)>0:
            po_data['name']=None
            po_data.pop("name", None)
            po_data['location_id']=12
            po_data['picking_type_id']=1
            po_data['order_line']= utt_order_line
            data=po_data
            purchase_obj = self.pool.get('purchase.order')
            saved_id = purchase_obj.create(cr, uid, data, context)
            saved_po_ids.append(saved_id)

            for items in utt_order_line:
                p_id = items[2].get('product_id')
                need_ids = need_to_buy_obj.search(cr, uid, [('name', '=', p_id)], context=context)

                need_to_buy_list = need_to_buy_obj.browse(cr, uid, need_ids, context=context)[0].b2b_order_ids
                if need_to_buy_list !='None':
                    need_to_buy_list = str(need_to_buy_list)
                    need_to_buy_list = need_to_buy_list.replace(']', '')
                    need_to_buy_list = need_to_buy_list.replace('[', '')
                    need_to_buy_list = need_to_buy_list.split(',')
                    for so_id in need_to_buy_list:
                        cr.execute("select * from sale_order where warehouse_id=1 and id =%s",([so_id]))
                        abc = cr.fetchall()
                        if len(abc)>0:
                            cr.execute("INSERT INTO sale_b2b_ref (sale_id,po_id,product_id) VALUES ('{0}','{1}','{2}')".format(so_id,saved_id,p_id))
                            cr.commit()




            cr.commit()

        if len(uttrt_order_line)>0:
            po_data.pop("name", None)
            po_data['location_id']=53
            po_data['picking_type_id']=23
            po_data['order_line'] = uttrt_order_line
            data=po_data
            purchase_obj = self.pool.get('purchase.order')
            saved_id = purchase_obj.create(cr, uid, data, context)


            for items in uttrt_order_line:
                p_id = items[2].get('product_id')
                need_ids = need_to_buy_obj.search(cr, uid, [('name', '=', p_id)], context=context)

                need_to_buy_list = need_to_buy_obj.browse(cr, uid, need_ids, context=context)[0].b2b_order_ids
                if need_to_buy_list !='None':
                    need_to_buy_list = str(need_to_buy_list)
                    need_to_buy_list = need_to_buy_list.replace(']', '')
                    need_to_buy_list = need_to_buy_list.replace('[', '')
                    need_to_buy_list = need_to_buy_list.split(',')
                    for so_id in need_to_buy_list:
                        cr.execute("select * from sale_order where warehouse_id=6 and id =%s",([so_id]))
                        abc = cr.fetchall()
                        if len(abc)>0:
                            cr.execute("INSERT INTO sale_b2b_ref (sale_id,po_id,product_id) VALUES ('{0}','{1}','{2}')".format(so_id,saved_id,p_id))
                            cr.commit()


            saved_po_ids.append(saved_id)
            cr.commit()




        if len(nodd_order_line)>0:
            po_data.pop("name", None)
            po_data['location_id']=19
            po_data['picking_type_id']=7
            po_data['order_line']=nodd_order_line
            data=po_data
            purchase_obj = self.pool.get('purchase.order')
            saved_id = purchase_obj.create(cr, uid, data, context)
            saved_po_ids.append(saved_id)

            for items in nodd_order_line:
                p_id = items[2].get('product_id')
                need_ids = need_to_buy_obj.search(cr, uid, [('name', '=', p_id)], context=context)

                need_to_buy_list = need_to_buy_obj.browse(cr, uid, need_ids, context=context)[0].b2b_order_ids

                if need_to_buy_list !='None':
                    need_to_buy_list = str(need_to_buy_list)
                    need_to_buy_list = need_to_buy_list.replace(']', '')
                    need_to_buy_list = need_to_buy_list.replace('[', '')
                    need_to_buy_list = need_to_buy_list.split(',')
                    for so_id in need_to_buy_list:
                        cr.execute("select * from sale_order where warehouse_id=2 and id =%s",([so_id]))
                        abc = cr.fetchall()
                        if len(abc)>0:
                            cr.execute("INSERT INTO sale_b2b_ref (sale_id,po_id,product_id) VALUES ('{0}','{1}','{2}')".format(so_id,saved_id,p_id))
                            cr.commit()



            cr.commit()


        if len(noddrt_order_line)>0:
            po_data.pop("name", None)
            po_data['location_id']=29
            po_data['picking_type_id']=13
            po_data['order_line']=noddrt_order_line
            data=po_data
            purchase_obj = self.pool.get('purchase.order')
            saved_id = purchase_obj.create(cr, uid, data, context)


            for items in noddrt_order_line:
                p_id = items[2].get('product_id')
                need_ids = need_to_buy_obj.search(cr, uid, [('name', '=', p_id)], context=context)

                need_to_buy_list = need_to_buy_obj.browse(cr, uid, need_ids, context=context)[0].b2b_order_ids
                if need_to_buy_list !='None':
                    need_to_buy_list = str(need_to_buy_list)
                    need_to_buy_list = need_to_buy_list.replace(']', '')
                    need_to_buy_list = need_to_buy_list.replace('[', '')
                    need_to_buy_list = need_to_buy_list.split(',')
                    for so_id in need_to_buy_list:
                        cr.execute("select * from sale_order where warehouse_id=3 and id =%s",([so_id]))
                        abc = cr.fetchall()
                        if len(abc)>0:
                            cr.execute("INSERT INTO sale_b2b_ref (sale_id,po_id,product_id) VALUES ('{0}','{1}','{2}')".format(so_id,saved_id,p_id))
                            cr.commit()

            saved_po_ids.append(saved_id)
            cr.commit()


        if len(moti_order_line)>0:
            po_data.pop("name", None)
            po_data['location_id']=36
            po_data['picking_type_id']=18
            po_data['order_line']=moti_order_line
            data=po_data
            purchase_obj = self.pool.get('purchase.order')
            saved_id = purchase_obj.create(cr, uid, data, context)


            for items in moti_order_line:
                p_id = items[2].get('product_id')
                need_ids = need_to_buy_obj.search(cr, uid, [('name', '=', p_id)], context=context)

                need_to_buy_list = need_to_buy_obj.browse(cr, uid, need_ids, context=context)[0].b2b_order_ids
                if need_to_buy_list !='None':
                    need_to_buy_list = str(need_to_buy_list)
                    need_to_buy_list = need_to_buy_list.replace(']', '')
                    need_to_buy_list = need_to_buy_list.replace('[', '')
                    need_to_buy_list = need_to_buy_list.split(',')
                    for so_id in need_to_buy_list:
                        cr.execute("select * from sale_order where warehouse_id=4 and id =%s",([so_id]))
                        abc = cr.fetchall()
                        if len(abc)>0:
                            cr.execute("INSERT INTO sale_b2b_ref (sale_id,po_id,product_id) VALUES ('{0}','{1}','{2}')".format(so_id,saved_id,p_id))
                            cr.commit()



            saved_po_ids.append(saved_id)
            cr.commit()


        if len(motirt_order_line)>0:
            po_data.pop("name", None)
            po_data['location_id']=60
            po_data['picking_type_id']=28
            po_data['order_line']=motirt_order_line
            data=po_data
            purchase_obj = self.pool.get('purchase.order')
            saved_id = purchase_obj.create(cr, uid, data, context)



            for items in motirt_order_line:
                p_id = items[2].get('product_id')
                need_ids = need_to_buy_obj.search(cr, uid, [('name', '=', p_id)], context=context)

                need_to_buy_list = need_to_buy_obj.browse(cr, uid, need_ids, context=context)[0].b2b_order_ids
                if need_to_buy_list !='None':
                    need_to_buy_list = str(need_to_buy_list)
                    need_to_buy_list = need_to_buy_list.replace(']', '')
                    need_to_buy_list = need_to_buy_list.replace('[', '')
                    need_to_buy_list = need_to_buy_list.split(',')
                    for so_id in need_to_buy_list:
                        cr.execute("select * from sale_order where warehouse_id=7 and id =%s",([so_id]))
                        abc = cr.fetchall()
                        if len(abc)>0:
                            cr.execute("INSERT INTO sale_b2b_ref (sale_id,po_id,product_id) VALUES ('{0}','{1}','{2}')".format(so_id,saved_id,p_id))
                            cr.commit()






            saved_po_ids.append(saved_id)
            cr.commit()



        ##### Update The Quantity


        # it is what we have saved the data




        ### Ends here

        return True


class po_create_need_to_buy_list_line(osv.osv_memory):
    _name = "po.create.need.to.buy.list.line"
    _description = "PO Create Need to Buy List Line"

    
    def _amount_line(self, cr, uid, ids, prop, arg, context=None):
        res = {}
        cur_obj=self.pool.get('res.currency')
        tax_obj = self.pool.get('account.tax')
        for line in self.browse(cr, uid, ids, context=context):
            taxes = tax_obj.compute_all(cr, uid, line.taxes_id, line.price, line.qty, line.product_id, line.line_id.partner_id)
            cur = line.line_id.pricelist_id.currency_id
            res[line.id] = cur_obj.round(cr, uid, cur, taxes['total'])
        return res
    
    



    _columns = {
        
#        'location_destination_id': fields.many2one('stock.location', 'Stock Destination Location'),
#        'location_id': fields.many2one('stock.location', 'Stock Source Location'),
        'product_id': fields.many2one('product.product', 'Product'),
        'line_id': fields.many2one('po.create.need.to.buy.list', 'Product Lines'),
        'qty': fields.float('Quantity'),
        'price': fields.float('Unit Price'),
        'subtotal': fields.function(_amount_line, string='Subtotal', digits_compute= dp.get_precision('Account')),
        'taxes_id': fields.many2many('account.tax', 'purchase_order_taxe', 'ord_id', 'tax_id', 'Taxes'),
        'product_uom': fields.char('Product Unit of Measure'),
        
           
    }

class sale_to_po_reference(osv.osv):
    _name ="sale.b2b.ref"

    _columns={
        'sale_id': fields.many2one('sale.order', 'Sale Order'),
        'po_id': fields.many2one('purchase.order', 'Purchase Order'),
        'product_id': fields.many2one('product.product', 'Product'),

    }


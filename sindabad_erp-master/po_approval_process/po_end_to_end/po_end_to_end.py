from openerp.osv import fields, osv

class po_end_to_end(osv.osv):
    _name = "po.end.to.end"
    _description = "PO Approval End-to-End"

    STATE_SELECTION = [
        ('draft', 'Draft PO'),
        ('sent', 'RFQ'),
        ('bid', 'Bid Received'),
        ('category_head_approval_pending', 'Category Head Approval Pending'),
        ('category_vp_approval_pending', 'Category VP Approval Pending'),
        ('coo_approval_pending', 'COO Approval Pending'),
        ('ceo_approval_pending', 'CEO Approval Pending'),
        ('confirmed', 'Waiting Approval'),
        ('approved', 'Purchase Confirmed'),
        ('except_picking', 'Shipping Exception'),
        ('except_invoice', 'Invoice Exception'),
        ('done', 'Done'),
        ('cancel', 'Cancelled')
    ]

    _columns = {
        'po_number': fields.char('PO Number'),
        'po_id': fields.integer('Purchase ID'),

        'category_head_approve_date': fields.text('Category Head Approve Date'),
        'category_head_approve_by': fields.text('Category Head Approve By'),

        'category_vp_approve_date': fields.text('Category VP Approve Date'),
        'category_vp_approve_by': fields.text('Category VP Approve By'),

        'coo_approve_date': fields.text('COO Approve Date'),
        'coo_approve_by': fields.text('COO Approve By'),

        'ceo_approve_date': fields.text('CEO Approve Date'),
        'ceo_approve_by': fields.text('CEO Approve By'),

        'state': fields.selection(STATE_SELECTION, 'Status', readonly=True,
                                  help="The status of the purchase order or the quotation request. "
                                       "A request for quotation is a purchase order in a 'Draft' status. "
                                       "Then the order has to be confirmed by the user, the status switch "
                                       "to 'Confirmed'. Then the supplier must confirm the order to change "
                                       "the status to 'Approved'. When the purchase order is paid and "
                                       "received, the status becomes 'Done'. If a cancel action occurs in "
                                       "the invoice or in the receipt of goods, the status becomes "
                                       "in exception.",
                                  select=True, copy=False),
    }




    def po_data_create(self, cr, uid, po_number, po_id, value, context=None):

        po_count = self.search_count(cr, uid, [('po_number','=',po_number), ('po_id', '=', po_id)])

        if int(po_count) == 0:
            created_id = self.create(cr, uid, {
                'po_number': po_number,
                'po_id': po_id,
                'po_date_create': value['po_date_create'],
                'state':value['state']
            }, context=context)

        return True



    def po_confirm_update(self, cr, uid, po_number, po_id, value, context=None):

        po_etoe_id = self.search(cr, uid, [('po_number', '=', po_number), ('po_id', '=', po_id)])

        self.write(cr, uid, po_etoe_id, {
            'category_head_approve_date': value['category_head_approve_date'],
            'category_head_approve_by': value['category_head_approve_by'],
            'category_vp_approve_date': value['category_vp_approve_date'],
            'category_vp_approve_by': value['category_vp_approve_by'],
            'coo_approve_date': value['coo_approve_date'],
            'coo_approve_by': value['coo_approve_by'],
            'state': value['state'],
        }, context=context)

        return True




from datetime import datetime, timedelta
import time
from datetime import date
from openerp import SUPERUSER_ID
from openerp.osv import fields, osv
from openerp.tools.translate import _
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT
import openerp.addons.decimal_precision as dp
from openerp import workflow
from openerp import api
import datetime
from ..base.res.res_partner import format_address
from openerp.exceptions import Warning, except_orm



class po_approval_amount_range(osv.osv):

    _name = "po.approval.amount.range"

    _columns = {
        'head_range1': fields.integer('Category Head Amount Ranges From'),
        'head_range2': fields.integer('Category Head Amount Ranges To'),
        'vp_range1': fields.integer('Category VP Amount Ranges From'),
        'vp_range2': fields.integer('Category VP Amount Ranges To'),
        'coo_range1': fields.integer('COO Amount Ranges From'),
        'coo_range2': fields.integer('COO Amount Ranges To'),
        'ceo_range1': fields.integer('CEO Amount Ranges From'),
        'ceo_range2': fields.integer('CEO Amount Ranges To'),
    }





class purchase_order(osv.osv):
    _inherit = "purchase.order"

    STATE_SELECTION = [
        ('draft', 'Draft PO'),
        ('sent', 'RFQ'),
        ('bid', 'Bid Received'),
        ('category_head_approval_pending', 'Category Head Approval Pending'),
        ('category_vp_approval_pending', 'Category VP Approval Pending'),
        ('coo_approval_pending', 'COO Approval Pending'),
        ('ceo_approval_pending', 'CEO Approval Pending'),
        ('confirmed', 'Waiting Approval'),
        ('approved', 'Purchase Confirmed'),
        ('except_picking', 'Shipping Exception'),
        ('except_invoice', 'Invoice Exception'),
        ('done', 'Done'),
        ('cancel', 'Cancelled')
    ]

    _columns = {
        'category_head_approve_date': fields.datetime('Category Head Approve Date'),
        'category_head_approve_by': fields.many2one('res.users', 'Category Head Approve By'),

        'category_vp_approve_date': fields.datetime('Category VP Approve Date'),
        'category_vp_approve_by': fields.many2one('res.users', 'Category VP Approve By'),

        'coo_approve_date': fields.datetime('COO Approve Date'),
        'coo_approve_by': fields.many2one('res.users', 'COO Approve By'),

        'ceo_approve_date': fields.datetime('CEO Approve Date'),
        'ceo_approve_by': fields.many2one('res.users', 'CEO Approve By'),

        'state': fields.selection(STATE_SELECTION, 'Status', readonly=True,
                                  help="The status of the purchase order or the quotation request. "
                                       "A request for quotation is a purchase order in a 'Draft' status. "
                                       "Then the order has to be confirmed by the user, the status switch "
                                       "to 'Confirmed'. Then the supplier must confirm the order to change "
                                       "the status to 'Approved'. When the purchase order is paid and "
                                       "received, the status becomes 'Done'. If a cancel action occurs in "
                                       "the invoice or in the receipt of goods, the status becomes "
                                       "in exception.",
                                  select=True, copy=False),

        'paid': fields.boolean('Paid'),
        'paid_amount': fields.float('Paid Amount'),
        'description': fields.text('Description'),
        'paid_date': fields.datetime('Date'),
    }





    def category_head_approval(self, cr, uid, ids, context=None):

        po_obj = self.browse(cr, uid, ids[0], context=context)

        for single_id in ids:

            try:

                category_head_approve_po_query = "UPDATE purchase_order SET state='category_head_approval_pending', category_head_approve_date='{0}', category_head_approve_by='{1}' WHERE id='{2}'".format(
                    str(fields.datetime.now()), uid, single_id)
                cr.execute(category_head_approve_po_query)
                cr.commit()

                category_head_approve_pol_query = "UPDATE purchase_order_line SET state='category_head_approval_pending', category_head_approve_date='{0}' WHERE order_id='{1}'".format(
                    str(fields.datetime.now()), single_id)
                cr.execute(category_head_approve_pol_query)
                cr.commit()

                self.message_post(cr, uid, ids, body=_("RFQ Approved By Category Head"), context=context)

            except:

                pass

        self.pool.get('po.end.to.end').po_data_create(cr, uid, str(po_obj.name), po_obj.id, value={'po_date_create': po_obj.create_date, 'state': po_obj.state}, context=context)

        return True





    def po_confirm_order(self, cr, uid, ids, context=None):

        po_obj = self.browse(cr, uid, ids[0], context=context)

        range_value = self.pool['po.approval.amount.range'].browse(cr, uid, self.pool['po.approval.amount.range'].search(cr, uid, []))

        po_amount = po_obj.amount_total

        range_head = range_value.head_range1 <= po_amount <= range_value.head_range2
        range_vp = range_value.vp_range1 <= po_amount <= range_value.vp_range2
        range_coo = range_value.coo_range1 <= po_amount <= range_value.coo_range2
        range_ceo = range_value.ceo_range1 <= po_amount

        if range_head or str(po_obj.client_order_ref).startswith("10000") or po_obj.adv_po_snd == 'credit':

            po_obj.signal_workflow('purchase_confirm')

            self.pool.get('po.end.to.end').po_confirm_update(cr, uid, str(po_obj.name), po_obj.id, value={
                'category_head_approve_date': po_obj.category_head_approve_date if po_obj.category_head_approve_date else None,
                'category_head_approve_by': str(po_obj.category_head_approve_by.name) if (po_obj.category_head_approve_by.name) else None,
                'category_vp_approve_date': po_obj.category_vp_approve_date if po_obj.category_vp_approve_date else None,
                'category_vp_approve_by': str(po_obj.category_vp_approve_by.name) if (po_obj.category_vp_approve_by.name) else None,
                'coo_approve_date': po_obj.coo_approve_date if po_obj.coo_approve_date else None,
                'coo_approve_by': str(po_obj.coo_approve_by.name) if (po_obj.coo_approve_by.name) else None,
                'state': po_obj.state}, context=context)


        if range_vp and po_obj.adv_po_snd == 'advance':

            if str(po_obj.state) == 'category_head_approval_pending':

                category_vp_approve_po_query = "UPDATE purchase_order SET state='category_vp_approval_pending', category_vp_approve_date='{0}', category_vp_approve_by='{1}' WHERE id='{2}'".format(
                    str(fields.datetime.now()), uid, po_obj.id)
                cr.execute(category_vp_approve_po_query)
                cr.commit()

                category_vp_approve_pol_query = "UPDATE purchase_order_line SET state='category_vp_approval_pending', category_vp_approve_date='{0}' WHERE order_id='{1}'".format(
                    str(fields.datetime.now()), po_obj.id)
                cr.execute(category_vp_approve_pol_query)
                cr.commit()

                self.message_post(cr, uid, ids, body=_("RFQ Approved By Category VP"), context=context)

                self.pool.get('po.end.to.end').po_confirm_update(cr, uid, str(po_obj.name), po_obj.id, value={
                    'category_head_approve_date': po_obj.category_head_approve_date if po_obj.category_head_approve_date else None,
                    'category_head_approve_by': str(po_obj.category_head_approve_by.name) if (
                        po_obj.category_head_approve_by.name) else None,
                    'category_vp_approve_date': po_obj.category_vp_approve_date if po_obj.category_vp_approve_date else None,
                    'category_vp_approve_by': str(po_obj.category_vp_approve_by.name) if (
                        po_obj.category_vp_approve_by.name) else None,
                    'coo_approve_date': po_obj.coo_approve_date if po_obj.coo_approve_date else None,
                    'coo_approve_by': str(po_obj.coo_approve_by.name) if (po_obj.coo_approve_by.name) else None,
                    'state': po_obj.state}, context=context)


                return {
                    'type': 'ir.actions.client',
                    'tag': 'reload',
                }

            if str(po_obj.state) == 'category_vp_approval_pending':

                po_obj.signal_workflow('purchase_confirm')

                self.pool.get('po.end.to.end').po_confirm_update(cr, uid, str(po_obj.name), po_obj.id, value={
                    'category_head_approve_date': po_obj.category_head_approve_date if po_obj.category_head_approve_date else None,
                    'category_head_approve_by': str(po_obj.category_head_approve_by.name) if (
                        po_obj.category_head_approve_by.name) else None,
                    'category_vp_approve_date': po_obj.category_vp_approve_date if po_obj.category_vp_approve_date else None,
                    'category_vp_approve_by': str(po_obj.category_vp_approve_by.name) if (
                        po_obj.category_vp_approve_by.name) else None,
                    'coo_approve_date': po_obj.coo_approve_date if po_obj.coo_approve_date else None,
                    'coo_approve_by': str(po_obj.coo_approve_by.name) if (po_obj.coo_approve_by.name) else None,
                    'state': po_obj.state}, context=context)




        if range_coo and po_obj.adv_po_snd == 'advance':

            if str(po_obj.state) == 'category_head_approval_pending':

                category_vp_approve_po_query = "UPDATE purchase_order SET state='category_vp_approval_pending', category_vp_approve_date='{0}', category_vp_approve_by='{1}' WHERE id='{2}'".format(
                    str(fields.datetime.now()), uid, po_obj.id)
                cr.execute(category_vp_approve_po_query)
                cr.commit()

                category_vp_approve_pol_query = "UPDATE purchase_order_line SET state='category_vp_approval_pending', category_vp_approve_date='{0}' WHERE order_id='{1}'".format(
                    str(fields.datetime.now()), po_obj.id)
                cr.execute(category_vp_approve_pol_query)
                cr.commit()

                self.message_post(cr, uid, ids, body=_("RFQ Approved By Category VP"), context=context)

                self.pool.get('po.end.to.end').po_confirm_update(cr, uid, str(po_obj.name), po_obj.id, value={
                    'category_head_approve_date': po_obj.category_head_approve_date if po_obj.category_head_approve_date else None,
                    'category_head_approve_by': str(po_obj.category_head_approve_by.name) if (
                        po_obj.category_head_approve_by.name) else None,
                    'category_vp_approve_date': po_obj.category_vp_approve_date if po_obj.category_vp_approve_date else None,
                    'category_vp_approve_by': str(po_obj.category_vp_approve_by.name) if (
                        po_obj.category_vp_approve_by.name) else None,
                    'coo_approve_date': po_obj.coo_approve_date if po_obj.coo_approve_date else None,
                    'coo_approve_by': str(po_obj.coo_approve_by.name) if (po_obj.coo_approve_by.name) else None,
                    'state': po_obj.state}, context=context)


                return {
                    'type': 'ir.actions.client',
                    'tag': 'reload',
                }

            if str(po_obj.state) == 'category_vp_approval_pending':

                coo_approve_po_query = "UPDATE purchase_order SET state='coo_approval_pending', coo_approve_date='{0}', coo_approve_by='{1}' WHERE id='{2}'".format(
                    str(fields.datetime.now()), uid, po_obj.id)
                cr.execute(coo_approve_po_query)
                cr.commit()

                coo_approve_pol_query = "UPDATE purchase_order_line SET state='coo_approval_pending', coo_approve_date='{0}' WHERE order_id='{1}'".format(
                    str(fields.datetime.now()), po_obj.id)
                cr.execute(coo_approve_pol_query)
                cr.commit()

                self.message_post(cr, uid, ids, body=_("RFQ Approved By COO"), context=context)

                self.pool.get('po.end.to.end').po_confirm_update(cr, uid, str(po_obj.name), po_obj.id, value={
                    'category_head_approve_date': po_obj.category_head_approve_date if po_obj.category_head_approve_date else None,
                    'category_head_approve_by': str(po_obj.category_head_approve_by.name) if (
                        po_obj.category_head_approve_by.name) else None,
                    'category_vp_approve_date': po_obj.category_vp_approve_date if po_obj.category_vp_approve_date else None,
                    'category_vp_approve_by': str(po_obj.category_vp_approve_by.name) if (
                        po_obj.category_vp_approve_by.name) else None,
                    'coo_approve_date': po_obj.coo_approve_date if po_obj.coo_approve_date else None,
                    'coo_approve_by': str(po_obj.coo_approve_by.name) if (po_obj.coo_approve_by.name) else None,
                    'state': po_obj.state}, context=context)


                return {
                    'type': 'ir.actions.client',
                    'tag': 'reload',
                }

            if str(po_obj.state) == 'coo_approval_pending':

                po_obj.signal_workflow('purchase_confirm')

                self.pool.get('po.end.to.end').po_confirm_update(cr, uid, str(po_obj.name), po_obj.id, value={
                    'category_head_approve_date': po_obj.category_head_approve_date if po_obj.category_head_approve_date else None,
                    'category_head_approve_by': str(po_obj.category_head_approve_by.name) if (
                        po_obj.category_head_approve_by.name) else None,
                    'category_vp_approve_date': po_obj.category_vp_approve_date if po_obj.category_vp_approve_date else None,
                    'category_vp_approve_by': str(po_obj.category_vp_approve_by.name) if (
                        po_obj.category_vp_approve_by.name) else None,
                    'coo_approve_date': po_obj.coo_approve_date if po_obj.coo_approve_date else None,
                    'coo_approve_by': str(po_obj.coo_approve_by.name) if (po_obj.coo_approve_by.name) else None,
                    'state': po_obj.state}, context=context)





        # if range_ceo and po_obj.adv_po_snd == 'advance':
        #
        #     if str(po_obj.state) == 'category_head_approval_pending':
        #         category_vp_approve_po_query = "UPDATE purchase_order SET state='category_vp_approval_pending', category_vp_approve_date='{0}', category_vp_approve_by='{1}' WHERE id='{2}'".format(
        #             str(fields.datetime.now()), uid, po_obj.id)
        #         cr.execute(category_vp_approve_po_query)
        #         cr.commit()
        #
        #         category_vp_approve_pol_query = "UPDATE purchase_order_line SET state='category_vp_approval_pending', category_vp_approve_date='{0}' WHERE order_id='{1}'".format(
        #             str(fields.datetime.now()), po_obj.id)
        #         cr.execute(category_vp_approve_pol_query)
        #         cr.commit()
        #
        #         self.message_post(cr, uid, ids, body=_("RFQ Approved By Category VP"), context=context)
        #
        #
        #         # self.pool.get('po.end.to.end').po_confirm_update(cr, uid, po_obj.po_number, po_obj.po_id, str(po_obj.category_head_approve_date), str(po_obj.category_head_approve_by.name), 'category_head_approval_pending', context=context)
        #
        #
        #         return {
        #             'type': 'ir.actions.client',
        #             'tag': 'reload',
        #         }
        #
        #
        #     if str(po_obj.state) == 'category_vp_approval_pending':
        #         coo_approve_po_query = "UPDATE purchase_order SET state='coo_approval_pending', coo_approve_date='{0}', coo_approve_by='{1}' WHERE id='{2}'".format(
        #             str(fields.datetime.now()), uid, po_obj.id)
        #         cr.execute(coo_approve_po_query)
        #         cr.commit()
        #
        #         coo_approve_pol_query = "UPDATE purchase_order_line SET state='coo_approval_pending', coo_approve_date='{0}' WHERE order_id='{1}'".format(
        #             str(fields.datetime.now()), po_obj.id)
        #         cr.execute(coo_approve_pol_query)
        #         cr.commit()
        #
        #         self.message_post(cr, uid, ids, body=_("RFQ Approved By COO"), context=context)
        #
        #
        #         # self.pool.get('po.end.to.end').po_confirm_update(cr, uid, po_obj.po_number, po_obj.po_id, str(po_obj.category_head_approve_date), str(po_obj.category_head_approve_by.name), 'category_head_approval_pending', context=context)
        #
        #
        #         return {
        #             'type': 'ir.actions.client',
        #             'tag': 'reload',
        #         }
        #
        #
        #     if str(po_obj.state) == 'coo_approval_pending':
        #         ceo_approve_po_query = "UPDATE purchase_order SET state='ceo_approval_pending', ceo_approve_date='{0}', ceo_approve_by='{1}' WHERE id='{2}'".format(
        #             str(fields.datetime.now()), uid, po_obj.id)
        #         cr.execute(ceo_approve_po_query)
        #         cr.commit()
        #
        #         ceo_approve_pol_query = "UPDATE purchase_order_line SET state='ceo_approval_pending', ceo_approve_date='{0}' WHERE order_id='{1}'".format(
        #             str(fields.datetime.now()), po_obj.id)
        #         cr.execute(ceo_approve_pol_query)
        #         cr.commit()
        #
        #         self.message_post(cr, uid, ids, body=_("RFQ Approved By CEO"), context=context)
        #
        #
        #         # self.pool.get('po.end.to.end').po_confirm_update(cr, uid, po_obj.po_number, po_obj.po_id, str(po_obj.category_head_approve_date), str(po_obj.category_head_approve_by.name), 'category_head_approval_pending', context=context)
        #
        #
        #         return {
        #             'type': 'ir.actions.client',
        #             'tag': 'reload',
        #         }
        #
        #
        #     if str(po_obj.state) == 'ceo_approval_pending':
        #
        #         po_obj.signal_workflow('purchase_confirm')



        return True



class purchase_order_line(osv.osv):
    _inherit = "purchase.order.line"

    STATE_SELECTION = [
        ('draft', 'Draft PO'),
        ('sent', 'RFQ'),
        ('bid', 'Bid Received'),
        ('category_head_approval_pending', 'Category Head Approval Pending'),
        ('category_vp_approval_pending', 'Category VP Approval Pending'),
        ('coo_approval_pending', 'COO Approval Pending'),
        ('ceo_approval_pending', 'CEO Approval Pending'),
        ('confirmed', 'Waiting Approval'),
        ('approved', 'Purchase Confirmed'),
        ('except_picking', 'Shipping Exception'),
        ('except_invoice', 'Invoice Exception'),
        ('done', 'Done'),
        ('cancel', 'Cancelled')
    ]

    _columns = {
        'category_head_approve_date': fields.datetime('Category Head Approve Date'),
        'category_head_approve_by': fields.many2one('res.users', 'Category Head Approve By'),

        'category_vp_approve_date': fields.datetime('Category VP Approve Date'),
        'category_vp_approve_by': fields.many2one('res.users', 'Category VP Approve By'),

        'coo_approve_date': fields.datetime('COO Approve Date'),
        'coo_approve_by': fields.many2one('res.users', 'COO Approve By'),

        'ceo_approve_date': fields.datetime('CEO Approve Date'),
        'ceo_approve_by': fields.many2one('res.users', 'CEO Approve By'),

        'state': fields.selection(STATE_SELECTION, 'Status', readonly=True,
                                  help="The status of the purchase order or the quotation request. "
                                       "A request for quotation is a purchase order in a 'Draft' status. "
                                       "Then the order has to be confirmed by the user, the status switch "
                                       "to 'Confirmed'. Then the supplier must confirm the order to change "
                                       "the status to 'Approved'. When the purchase order is paid and "
                                       "received, the status becomes 'Done'. If a cancel action occurs in "
                                       "the invoice or in the receipt of goods, the status becomes "
                                       "in exception.",
                                  select=True, copy=False),
    }




class advance_po_paid(osv.osv):

    _name = "advance.po.paid"

    _columns = {

        'paid': fields.boolean('Paid'),
        'paid_amount': fields.float('Paid Amount'),
        'description': fields.text('Paid Description'),
        'paid_date': fields.datetime('Paid Date'),
    }



    def _get_po_amount(self, cr, uid, context=None):

        po_obj = self.pool.get('purchase.order').browse(cr, uid, context['active_id'], context=context)
        po_amount = po_obj.amount_total if po_obj.amount_total else 0

        return po_amount


    _defaults = {
        'paid_date': fields.datetime.now,
        'paid_amount': _get_po_amount,
    }





    def po_paid_def(self, cr, uid, ids, context=None):

        po_obj = self.pool.get('purchase.order')

        po_obj.write(cr, uid, context['active_id'], context)

        return True
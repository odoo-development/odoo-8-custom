{
    'name': 'PO Approval Process',
    'version': '1.0',
    'category': 'PO Approval Process',
    'author': 'Rocky',
    'description': 'PO Approval Process',
    'depends': ['base','purchase','saad_po','sgeede_b2b'],
    'data': [
        'security/po_approval_process_security.xml',
        'security/ir.model.access.csv',

        'wizard/paid_form_view.xml',

        'views/po_approval_amount_range.xml',
        'views/po_approval_inherit_button.xml',

        'po_end_to_end/po_end_to_end_view.xml',

        'po_approval_menu.xml',
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}

# Mufti Muntasir Ahmed 10-05-2018
from openerp.osv import fields, osv
from openerp.tools.translate import _
import openerp.addons.decimal_precision as dp

from openerp import models, api
from openerp.exceptions import Warning
from datetime import datetime


class partial_waiting_process_class(object):

    def partially_available_process(self, cr, uid, client_order_ref, context=None):

        """
        stock_picking any picking state "partially_available"
        """

        try:
            if client_order_ref != 'None':
                so_query = "SELECT name FROM sale_order WHERE client_order_ref='{0}'".format(client_order_ref)
                cr.execute(so_query)
                sale_order_data = cr.fetchall()
                so_name = str(sale_order_data[0][0])

                stock_picking_status = list()
                stock_picking_query = "SELECT state FROM stock_picking WHERE origin='{0}'".format(so_name)
                cr.execute(stock_picking_query)
                for sp in cr.fetchall():
                    stock_picking_status.append(str(sp[0]))

                partially_available = False
                if "partially_available" in stock_picking_status:
                    partially_available = True
                else:
                    partially_available = False

                query_string = "UPDATE sale_order SET partially_available='{0}' WHERE name='{1}'".format(partially_available, so_name)

                cr.execute(query_string)
                cr.commit()

        except:
            pass

        return True

    def partially_delivered_prcess(self, sale_order_pool, cr, uid, client_order_ref, context=None):
        if client_order_ref.isdigit():
            # partially delivered amount
            so_query = "SELECT id, amount_total FROM sale_order WHERE client_order_ref='{0}'".format(client_order_ref)
            cr.execute(so_query)
            sale_order_data = cr.fetchall()
            so_id = sale_order_data[0][0]
            so_amount_total = sale_order_data[0][1]

            res = {}
            for sale in sale_order_pool.browse(cr, uid, [so_id], context=context):
                total_sum = 0.00

                for obj in sale.order_line:
                    total_sum = total_sum + (obj.qty_delivered * obj.price_unit) - (obj.qty_return * obj.price_unit)

                res[sale.id] = total_sum

            partially_delivered = False
            if res[so_id] < so_amount_total and res[so_id] > 0:
                partially_delivered = True
            else:
                partially_delivered = False

            query_string = "UPDATE sale_order SET partial_delivered='{0}' WHERE id='{1}'".format(partially_delivered, so_id)

            cr.execute(query_string)
            cr.commit()

        return True

    def waiting_availability_process(self, cr, uid, client_order_ref, context=None):

        if client_order_ref.isdigit():

            sale_order_data_query = "SELECT id, client_order_ref, state, amount_total, name FROM sale_order WHERE client_order_ref='{0}'".format(client_order_ref)

            cr.execute(sale_order_data_query)
            sale_order_data = cr.fetchall()
            so_id = sale_order_data[0][0]
            so_mg_ref = str(sale_order_data[0][1])
            so_state = str(sale_order_data[0][2])
            so_amount_total = sale_order_data[0][3]
            so_name = str(sale_order_data[0][4])

            # for partially available
            stock_move_query = "SELECT state FROM stock_move WHERE origin = '{0}'".format(so_name)
            cr.execute(stock_move_query)
            stock_move_ref = cr.fetchall()
            move_state_list = [str(sm[0]) for sm in stock_move_ref]

            waiting_available = False
            if "confirmed" in move_state_list:
                waiting_available = True
            else:
                waiting_available = False

            query_string = "UPDATE sale_order SET waiting_availability='{0}' WHERE name='{1}'".format(waiting_available, so_name)

            cr.execute(query_string)
            cr.commit()

        return True


class sale_order(osv.osv):
    _inherit = "sale.order"

    # partial delivered status
    def _get_partial_delivered_status(self, cursor, user, ids, name, arg, context=None):
        res = False
        res = {}
        """
        for sale in self.browse(cursor, user, ids, context=context):
            res[sale.id]=False
            if  sale.shipped == False:
                for obj in sale.order_line:
                    if obj.partial_delivery_flag == True:
                        res[sale.id] = True
                        break
        """
        so_delivery_amount = self._get_delivered_amount(cursor, user, ids, name, arg, context)

        # Raw query to set res
        sale_order_amount_query = "SELECT id, amount_total FROM sale_order WHERE id IN {0}".format(tuple(ids))
        cursor.execute(sale_order_amount_query)
        for sale in cursor.fetchall():
            if so_delivery_amount[int(sale[0])] < sale[1] and so_delivery_amount[int(sale[0])] > 0:
                res[int(sale[0])] = True
            else:
                res[int(sale[0])] = False

        # ORM to set res
        """
        for sale in self.browse(cursor, user, ids, context=context):
            
            if so_delivery_amount[sale.id] < sale.amount_total and so_delivery_amount[sale.id] > 0:
                res[sale.id] = True
            else:
                res[sale.id] = False
        """

        return res

    # partial delivered search
    def _partial_delivery_search(self, cursor, user, obj, name, args, context=None):
        if not len(args):
            return []

        # select all so where state is not draft
        confirmed_so_list = list()
        so_not_draft_query = "SELECT id FROM sale_order WHERE state IN ('progress', 'done')"
        cursor.execute(so_not_draft_query)
        for so in cursor.fetchall():
            confirmed_so_list.append(so[0])

        so_delivery_amount = self._get_delivered_amount(cursor, user, confirmed_so_list, name, args, context)
        res = dict()

        # Raw query to set res
        sale_order_amount_query = "SELECT id, amount_total FROM sale_order WHERE id IN {0}".format(tuple(confirmed_so_list))
        cursor.execute(sale_order_amount_query)
        for sale in cursor.fetchall():
            if so_delivery_amount[int(sale[0])] < sale[1] and so_delivery_amount[int(sale[0])] > 0:
                res[int(sale[0])] = True
            else:
                res[int(sale[0])] = False

        # ORM to set res
        """
        for sale in self.browse(cursor, user, confirmed_so_list, context=context):
            if so_delivery_amount[sale.id] < sale.amount_total and so_delivery_amount[sale.id] > 0:
                res[sale.id] = True
            else:
                res[sale.id] = False
        """

        # ('partial_delivered', '!=', True)
        args_str = eval(str(args[0]))

        arg_status = False
        if args_str[1] == '!=':
            arg_status = False
        else:
            arg_status = True

        filtered_ids = [('id', 'in', [item for item in res if res[item] == arg_status])]

        return filtered_ids

    # waiting availability status
    def _get_waiting_availability_status(self, cursor, user, ids, name, arg, context=None):

        res = dict()

        if len(ids) == 1:
            all_ids = "("+ str(ids[0]) +")"
        else:
            all_ids = tuple(ids)

        sale_order_names_query = "SELECT name FROM sale_order WHERE id IN {0}".format(all_ids)
        cursor.execute(sale_order_names_query)
        sale_order_names_ref = cursor.fetchall()
        sale_order_names = [str(son[0]) for son in sale_order_names_ref]

        if len(sale_order_names) == 1:
            all_sale_order_names = "('"+ str(sale_order_names[0]) +"')"
        else:
            all_sale_order_names = tuple(sale_order_names)

        # stock_move confirmed -> waiting_availability
        stock_move_query = "SELECT DISTINCT(origin) FROM stock_move WHERE state='confirmed' AND origin IN {0}".format(all_sale_order_names)
        cursor.execute(stock_move_query)
        stock_move_ref = cursor.fetchall()
        stock_picking_partial = [str(sm[0]) for sm in stock_move_ref]

        for so in self.browse(cursor, user, ids, context=context):

            if str(so.name) in stock_picking_partial:
                res[int(str(so.id))] = True

            else:
                res[int(str(so.id))] = False

        return res

    # waiting availability search
    def _waiting_availability_search(self, cursor, user, obj, name, args, context=None):
        if not len(args):
            return []

        # select all so where state is not draft
        confirmed_so_list = list()
        confirmed_so_query = "SELECT id, name FROM sale_order WHERE state IN ('progress', 'done')"
        cursor.execute(confirmed_so_query)
        confirmed_so_ref = cursor.fetchall()
        confirmed_so_list = [int(cso[0]) for cso in confirmed_so_ref]
        confirmed_so_name_list = [str(cso[1]) for cso in confirmed_so_ref]

        stock_picking_query = "SELECT origin FROM stock_picking WHERE state = 'partially_available' AND origin IN {0}".format(tuple(confirmed_so_name_list))
        cursor.execute(stock_picking_query)
        stock_picking_ref = cursor.fetchall()
        stock_picking_partial_origin_list = [str(spr[0]) for spr in stock_picking_ref]

        res = dict()
        for sale in self.browse(cursor, user, confirmed_so_list, context=context):
            if str(sale.name) in stock_picking_partial_origin_list:
                res[sale.id] = True
            else:
                res[sale.id] = False

        # ('waiting_availability', '!=', True)
        args_str = eval(str(args[0]))

        arg_status = False
        if args_str[1] == '!=':
            arg_status = False
        else:
            arg_status = True

        filtered_ids = [('id', 'in', [item for item in res if res[item] == arg_status])]
        return filtered_ids

    # delivered amount
    def _get_delivered_amount(self, cursor, user, ids, name, arg, context=None):
        res = False
        res = {}

        for sale in self.browse(cursor, user, ids, context=context):
            total_sum = 0.00

            for obj in sale.order_line:
                total_sum = total_sum + (obj.qty_delivered * obj.price_unit) - (obj.qty_return * obj.price_unit)

            res[sale.id] = total_sum
        return res

    def _get_confirmed_so_list(self, cr):

        # select all so where state is not draft
        confirmed_so_list = list()
        so_not_draft_query = "SELECT id FROM sale_order WHERE state IN ('progress', 'done')"
        cr.execute(so_not_draft_query)
        for so in cr.fetchall():
            confirmed_so_list.append(so[0])

        return confirmed_so_list

    def _partial_delivered_computation(self, cr, uid, ids=None, context=None):

        confirmed_so_list = self._get_confirmed_so_list(cr)

        partial_delivered_res = self._get_partial_delivered_status(cr, uid, confirmed_so_list, "partial_delivered", None, context=None)

        for res_id in partial_delivered_res:
            query_string = "UPDATE sale_order SET partial_delivered='{0}' WHERE id='{1}'".format(partial_delivered_res[res_id], res_id)

            cr.execute(query_string)
            cr.commit()

        return True

    def _waiting_availability_computation(self, cr, uid, ids=None, context=None):

        confirmed_so_list = self._get_confirmed_so_list(cr)

        waiting_availability_res = self._get_waiting_availability_status(cr, uid, confirmed_so_list, "waiting_availability", None, context=None)

        for res_id in waiting_availability_res:
            query_string = "UPDATE sale_order SET waiting_availability='{0}' WHERE id='{1}'".format(
                waiting_availability_res[res_id], res_id)

            cr.execute(query_string)
            cr.commit()

        return True

    def _partial_available_computation(self, cr, uid, ids=None, context=None):

        so_query = "SELECT client_order_ref FROM sale_order WHERE state IN ('progress', 'done')"
        cr.execute(so_query)
        # so_client_order_ref = cr.fetchall()
        pwp = partial_waiting_process_class()
        for client_order_ref in cr.fetchall():

            pwp.partially_available_process(cr, uid, str(client_order_ref[0]), context=context)

        return True

    def _get_full_partially_available(self, cursor, user, ids, name, arg, context=None):
        res = {}
        full_partially_available = False

        for so in self.browse(cursor, user, ids, context=context):

            stock_move_obj = self.pool.get("stock.move")

            stock_move = stock_move_obj.search(cursor, user, [('origin', '=', str(so.name))])
            stock_move_line = stock_move_obj.browse(cursor, user, stock_move, context=context)

            for sm in stock_move_line:
                if str(sm.state) == "assigned" and so.partially_available == True:
                    full_partially_available = True
                    break
                else:
                    full_partially_available = False

            res[so.id] = full_partially_available

        return res

    def _get_full_partially_available_search(self, cursor, user, obj, name, args, context=None):
        if not len(args):
            return []
        res = dict()
        count = 1

        for so in self.browse(cursor, user, self.search(cursor, user, [])):
            count +=1
            if count == 3000:
                break

            stock_move_obj = self.pool.get("stock.move")

            stock_move = stock_move_obj.search(cursor, user, [('origin', '=', str(so.name))])
            stock_move_line = stock_move_obj.browse(cursor, user, stock_move, context=context)

            for sm in stock_move_line:
                if str(sm.state) == "assigned" and so.partially_available == True:

                    res[so.id] = True
                    break
                else:
                    res[so.id] = False

        args_str = eval(str(args[0]))
        arg_status = False
        if args_str[1] == '!=':
            arg_status = False
        else:
            arg_status = True

        filtered_ids = [('id', 'in', [item for item in res if res[item] == arg_status])]

        return filtered_ids

    _columns = {

        'delivered_amount': fields.function(_get_delivered_amount, string='Delivered Amount', type='float'),
        'partial_delivered': fields.boolean('Partial Delivered'),
        'waiting_availability': fields.boolean('Waiting Availability'),
        'partially_available': fields.boolean('Partially Available'),
        'full_partially_available': fields.function(_get_full_partially_available, string='Full Partially Available', type='boolean', fnct_search=_get_full_partially_available_search),
        # 'partial_delivered': fields.function(_get_partial_delivered_status, string='Partially Delivered',
        #                                     fnct_search=_partial_delivery_search, type='boolean'),

        # 'waiting_availability': fields.function(_get_waiting_availability_status, string='Waiting Availability',
        #                                     fnct_search=_waiting_availability_search, type='boolean'),
    }


# invoice
class stock_invoice_onshipping(osv.osv_memory):
    _inherit = "stock.invoice.onshipping"

    def open_invoice(self, cr, uid, ids, context=None):
        if context is None:
            context = {}

        invoice_ids = self.create_invoice(cr, uid, ids, context=context)
        if not invoice_ids:
            raise osv.except_osv(_('Error!'), _('No invoice created!'))

        data = self.browse(cr, uid, ids[0], context=context)

        action_model = False
        action = {}

        journal2type = {'sale': 'out_invoice', 'purchase': 'in_invoice', 'sale_refund': 'out_refund',
                        'purchase_refund': 'in_refund'}
        inv_type = journal2type.get(data.journal_type) or 'out_invoice'
        data_pool = self.pool.get('ir.model.data')
        if inv_type == "out_invoice":
            action_id = data_pool.xmlid_to_res_id(cr, uid, 'account.action_invoice_tree1')
        elif inv_type == "in_invoice":
            action_id = data_pool.xmlid_to_res_id(cr, uid, 'account.action_invoice_tree2')
        elif inv_type == "out_refund":
            action_id = data_pool.xmlid_to_res_id(cr, uid, 'account.action_invoice_tree3')
        elif inv_type == "in_refund":
            action_id = data_pool.xmlid_to_res_id(cr, uid, 'account.action_invoice_tree4')

        # get sale order info
        invoice_pool = self.pool.get('account.invoice')
        invoice_data = invoice_pool.browse(cr, uid, invoice_ids[0], context=context)

        if not str(invoice_data.name).startswith("PO"):

            # waiting availability
            pwp = partial_waiting_process_class()
            pwp.waiting_availability_process(cr, uid, str(invoice_data.name), context=context)

            # partially delivered
            sale_order_pool = self.pool.get('sale.order')
            pwp.partially_delivered_prcess(sale_order_pool, cr, uid, str(invoice_data.name), context=context)

            # partially available
            pwp.partially_available_process(cr, uid, str(invoice_data.name), context=context)

        if action_id:
            action_pool = self.pool['ir.actions.act_window']
            action = action_pool.read(cr, uid, action_id, context=context)
            action['domain'] = "[('id','in', [" + ','.join(map(str, invoice_ids)) + "])]"
            return action
        return True


# transfer
class stock_transfer_details(models.TransientModel):
    _inherit = 'stock.transfer_details'

    @api.one
    def do_detailed_transfer(self):
        if self.picking_id.state not in ['assigned', 'partially_available']:
            raise Warning(_('You cannot transfer a picking in state \'%s\'.') % self.picking_id.state)

        processed_ids = []
        # Create new and update existing pack operations
        for lstits in [self.item_ids, self.packop_ids]:
            for prod in lstits:
                pack_datas = {
                    'product_id': prod.product_id.id,
                    'product_uom_id': prod.product_uom_id.id,
                    'product_qty': prod.quantity,
                    'package_id': prod.package_id.id,
                    'lot_id': prod.lot_id.id,
                    'location_id': prod.sourceloc_id.id,
                    'location_dest_id': prod.destinationloc_id.id,
                    'result_package_id': prod.result_package_id.id,
                    'date': prod.date if prod.date else datetime.now(),
                    'owner_id': prod.owner_id.id,
                }
                if prod.packop_id:
                    prod.packop_id.with_context(no_recompute=True).write(pack_datas)
                    processed_ids.append(prod.packop_id.id)
                else:
                    pack_datas['picking_id'] = self.picking_id.id
                    packop_id = self.env['stock.pack.operation'].create(pack_datas)
                    processed_ids.append(packop_id.id)
        # Delete the others
        packops = self.env['stock.pack.operation'].search(
            ['&', ('picking_id', '=', self.picking_id.id), '!', ('id', 'in', processed_ids)])
        packops.unlink()

        # Execute the transfer of the picking
        self.picking_id.do_transfer()

        if str(self.picking_id.origin).startswith("SO"):
            sale_order_query = "SELECT client_order_ref FROM sale_order WHERE name='{0}'".format(
                str(self.picking_id.origin))
            self.env.cr.execute(sale_order_query)
            client_order_ref_data = self.env.cr.fetchall()
            client_order_ref = str(client_order_ref_data[0][0])

            context = {'lang': 'en_US', 'params': {'action': 404}, 'tz': 'Asia/Dhaka', 'uid': self._uid}
            # waiting availability
            pwp = partial_waiting_process_class()
            pwp.waiting_availability_process(self.env.cr, self._uid, client_order_ref, context=context)

            # partially delivered
            sale_order_pool = self.pool.get('sale.order')
            pwp.partially_delivered_prcess(sale_order_pool, self.env.cr, self._uid, client_order_ref, context=context)

            # partially available
            pwp.partially_available_process(self.env.cr, self._uid, client_order_ref, context=context)

        return True


# stock picking
class stock_picking(osv.osv):
    _inherit = "stock.picking"

    # Check Availability
    def action_assign(self, cr, uid, ids, context=None):
        """ Check availability of picking moves.
        This has the effect of changing the state and reserve quants on available moves, and may
        also impact the state of the picking as it is computed based on move's states.
        @return: True
        """
        for pick in self.browse(cr, uid, ids, context=context):
            if pick.state == 'draft':
                self.action_confirm(cr, uid, [pick.id], context=context)
            #skip the moves that don't need to be checked
            move_ids = [x.id for x in pick.move_lines if x.state not in ('draft', 'cancel', 'done')]
            if not move_ids:
                raise osv.except_osv(_('Warning!'), _('Nothing to check the availability for.'))
            self.pool.get('stock.move').action_assign(cr, uid, move_ids, context=context)

        if str(pick.origin).startswith("SO"):

            so_query = "SELECT client_order_ref FROM sale_order WHERE name='{0}'".format(str(pick.origin))
            cr.execute(so_query)
            sale_order_data = cr.fetchall()
            client_order_ref = str(sale_order_data[0][0])

            pwp = partial_waiting_process_class()
            pwp.partially_available_process(cr, uid, client_order_ref, context=context)

            waiting_state_list = ['partially_available', 'confirmed']

            waiting_available = False
            if str(pick.state) in waiting_state_list:
                waiting_available = True
            else:
                waiting_available = False

            query_string = "UPDATE sale_order SET waiting_availability='{0}' WHERE name='{1}'".format(waiting_available, str(pick.origin))

            cr.execute(query_string)
            cr.commit()

        return True

    # Recheck Availability
    def rereserve_pick(self, cr, uid, ids, context=None):
        """
        This can be used to provide a button that rereserves taking into account the existing pack operations
        """
        for pick in self.browse(cr, uid, ids, context=context):
            self.rereserve_quants(cr, uid, pick, move_ids = [x.id for x in pick.move_lines
                                                             if x.state not in ('done', 'cancel')], context=context)

        if str(pick.origin).startswith("SO"):

            so_query = "SELECT client_order_ref FROM sale_order WHERE name='{0}'".format(str(pick.origin))
            cr.execute(so_query)
            sale_order_data = cr.fetchall()
            client_order_ref = str(sale_order_data[0][0])

            pwp = partial_waiting_process_class()
            pwp.waiting_availability_process(cr, uid, client_order_ref, context=context)
            pwp.partially_available_process(cr, uid, client_order_ref, context=context)


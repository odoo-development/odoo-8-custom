# Mufti Muntasir Ahmed 09-05-2018



{
    'name': 'Bring Partial Delivery flag and amount for Sales Order',
    'version': '8.0.0',
    'category': 'Sales',
    'description': """

""",
    'author': 'Mufti Muntasir Ahmed',
    'depends': ['sale', 'account', 'stock', 'stock_account'],
    'data': [
        'sale.xml'
    ],

    'installable': True,
    'auto_install': False,
}
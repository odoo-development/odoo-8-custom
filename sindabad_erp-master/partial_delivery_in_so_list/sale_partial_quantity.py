# Mufti Muntasir Ahmed 09-05-2018


from openerp import models, fields, api, _
import openerp.addons.decimal_precision as dp



class SaleOrderLine(models.Model):
    _inherit = 'sale.order.line'



    partial_delivery_flag = fields.Boolean(
            compute="_get_counted",
            string='Partially ',
            store=True,
            help="Quantity Partially Delivered"
    )



    @api.model
    def check_move(self, move):
        if move.state != 'done':
            return False
        if all([self._context.get('date_start'), self._context.get('date_stop')]):
            if not (move.date >= self._context['date_start'] and
                            move.date <= self._context['date_stop']):
                return False
        return True

    def calculate_delivered_amount(self, cr, uid,delivered_qty=None,qty_return=None,ordered_qty=None,
                                   order_id=None, product_id=None,context=None):
        cr.execute("select product_id, product_uom_qty,qty_delivered, price_unit, qty_return from "
                   " sale_order_line where order_id=%s", ([order_id.id]))

        all_data = cr.fetchall()

        res = False
        total_sum = 0.00

        for items in all_data:
            if items[0]==product_id:
                total_sum = total_sum + (items[3] * delivered_qty) - (items[3] * qty_return)
            else:
                total_sum = total_sum + (items[2] * items[3]) - (items[3] * items[4])

        cr.execute("update sale_order set delivered_amount=%s where id=%s", ([total_sum, order_id.id]))

        return res

    def calculate_result_data(self, cr, uid,delivered_qty=None,qty_return=None,ordered_qty=None,order_id=None, context=None):
        cr.execute("select product_uom_qty,qty_delivered, partial_delivery_flag from  sale_order_line where order_id=%s", ([order_id.id]))

        all_data = cr.fetchall()

        all_products_delivered = False

        count_delivered_items=1
        res = False




        for items in all_data:
            if items[0]==items[1]:
                count_delivered_items +=1

        if len(all_data) != count_delivered_items:
                res=True

        return res

    @api.model
    def _get_real_move_qty(self, move):
        src = move.location_id.usage
        dst = move.location_dest_id.usage

        if src == dst:
            return 0.0
        elif src == 'internal':
            return self.product_uom._compute_qty_obj(
                    move.product_uom,
                    move.product_qty,
                    self.product_uom)
        elif dst == 'internal':
            return -self.product_uom._compute_qty_obj(
                    move.product_uom,
                    move.product_qty,
                    self.product_uom)
        else:
            return 0.0

    @api.one
    # @api.depends('move_ids.state')
    def _get_counted(self):

        qty_delivered = 0
        qty_return = 0
        for move in self.move_ids:
            if self.check_move(move):
                # Add Quantity
                if self._get_real_move_qty(move) > 0:
                    qty_delivered += self._get_real_move_qty(move)
                else:
                    qty_return -= self._get_real_move_qty(move)

        self.partial_delivery_flag = False

        if qty_delivered > 0:
            ordered_qty = self.product_uom_qty
            if ordered_qty > (qty_delivered-abs(qty_return)):
                self.partial_delivery_flag=True

        #     flag = self.calculate_result_data(delivered_qty=qty_delivered,qty_return=abs(qty_return),
        #                                       ordered_qty=self.product_uom_qty,order_id=self.order_id)
        #     self.partial_delivery_flag=flag
        #
        # calculated_amount = self.calculate_delivered_amount(delivered_qty=qty_delivered,qty_return=abs(qty_return),
        #                                                     ordered_qty=self.product_uom_qty,order_id=self.order_id,
        #                                                     product_id=self.product_id.id)
        # #










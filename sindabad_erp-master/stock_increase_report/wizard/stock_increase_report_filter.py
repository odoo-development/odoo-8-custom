from openerp import models, fields, api
import datetime


class StockIncrease(models.Model):
    _name = 'stock.increase'

    from_date = fields.Date(string="Start Date", required=True)


    _defaults = {
        'from_date': datetime.datetime.today()

    }


    @api.model
    def compute_ageing(self, data):
        rec = self.browse(data)
        data = {}
        data['form'] = rec.read(['from_date'])[0]
        return self.env['report'].get_action(rec, 'stock_increase_report.report_stock_increase',
                                             data=data)

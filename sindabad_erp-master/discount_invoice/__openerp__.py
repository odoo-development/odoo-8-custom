
{
    'name': 'Per Product Discount',
    'version': '1.0',
    'category': 'Per Product Discount',
    'sequence': 14,
    'summary': 'Per Product Discount',
    'description': """
Per Product Discount
    """,
    'author': 'OpenERP SA',
    'website': 'https://www.odoo.com/page/crm',
    'depends': ['sales_team','account_voucher', 'procurement', 'report','sale','stock', 'account'],
    'data': [
    ],
    'demo': [],
    'test': [

    ],
    'installable': True,
    'auto_install': False,
    'application': True,
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:

{
     'name': 'Picking and Packing Report Extend',
     'description': 'Picking and Packing Report Extend',
     'author': 'Mehedi Hasan',
     'depends': ['pickings', 'packings'],
     'data' : [
         'picking_extend.xml',
         'packing_extend.xml',
               ]

 }
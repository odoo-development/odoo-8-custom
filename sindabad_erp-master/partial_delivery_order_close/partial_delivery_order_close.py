from datetime import date, datetime
from dateutil import relativedelta
import json
import time

import openerp
from openerp.osv import fields, osv
from openerp.tools.float_utils import float_compare, float_round
from openerp.tools.translate import _
from openerp.tools import DEFAULT_SERVER_DATETIME_FORMAT, DEFAULT_SERVER_DATE_FORMAT
from openerp.exceptions import Warning
from openerp import SUPERUSER_ID, api
import openerp.addons.decimal_precision as dp
from openerp.addons.procurement import procurement
import logging
from dateutil.relativedelta import relativedelta
from operator import itemgetter
from openerp.osv import fields, osv, expression
from openerp.tools.float_utils import float_round as round


class sale_order(osv.osv):
    _inherit = "sale.order"

    # Received amount
    def _get_canceled_amount(self, cr, uid, ids, name, arg, context=None):
        res = False
        res = {}

        stock_move_obj = self.pool.get("stock.move")

        for sale_obj in self.browse(cr, uid, ids, context=context):
            total_sum = 0.00

            init_data = stock_move_obj.search(cr, uid, [('origin', '=', sale_obj.name)], context=context)

            for so_move in stock_move_obj.browse(cr, uid, init_data, context=context):
                if so_move.state == 'cancel':
                    total_sum = total_sum + (so_move.product_qty * so_move.price_unit)
                # elif so_move.origin_returned_move_id and not so_move.state == 'cancel':
                #     total_sum = total_sum - (so_move.product_qty * so_move.price_unit)

            res[sale_obj.id] = total_sum
        return res

    _columns = {
        'canceled_amount': fields.function(_get_canceled_amount, string='Cancelled Amount', type='float'),
        'order_close_reason': fields.char('Order Close Reason', required=True),
    }


class partially_delivered_order_close(osv.osv):
    _name = "partially.delivered.order.close"
    _description = "Partially delivered order close"

    _columns = {
        'order_close_reason':  fields.selection([
            ("product_not_available", "Product Not Available"),
            ("product_not_available_customer_will_take_alternative_product", "Product not available, Customer will take alternative product"),
            ("partially_delivered", "Partially delivered"),
            ("reordered", "Reordered#10000"),
            ("payment_issue", "Payment issue"),
            ("cash_problem", "Cash problem"),
            ("damage_product", "Damage product"),
            ("double_ordered", "Double ordered"),
            ("sample_not_approved", "Sample not approved"),
            ("wrong_sku", "Wrong SKU"),
            ("not_interested", "Not Interested"),
            ("wrong_product", "Wrong Product"),
            ("quality_issue", "Quality issue"),
            ("delay_delivery", "Delay delivery"),
            ("sourcing_delay", "Sourcing Delay"),
            ("not_for_retail", "Not for Retail")
    ], 'Order Close Reason', copy=False, help="Reasons", select=True),


        'order_name': fields.char('Order Name'),
        'order_ref': fields.char('Order Ref.'),
    }

    def partial_delivery_close(self, cr, uid, ids, context=None):

        close_order_tbl_ids = ids

        ids = context['active_ids']
        order_close_reason = context['order_close_reason']
        so_obj = self.pool.get('sale.order')

        for so in so_obj.browse(cr, uid, ids, context=context):

            stock_picking_query = "SELECT id FROM stock_picking WHERE origin='{0}' and state!='done'".format(str(so.name))
            cr.execute(stock_picking_query)

            for sp_id in cr.fetchall():
                # unreserve
                self.pool.get('stock.picking').do_unreserve(cr, uid, int(sp_id[0]), context=context)

                # action cancel
                self.pool.get('stock.picking').action_cancel(cr, uid, int(sp_id[0]), context=context)

        # Action Done
        so_obj.action_done(cr, uid, ids, context=context)

        for so_id in ids:
            so_state_query = "UPDATE sale_order SET shipped=TRUE, waiting_availability=FALSE , partially_available=FALSE, partial_delivered=FALSE, order_close_reason='{0}' WHERE id='{1}'".format(order_close_reason, so_id)

            cr.execute(so_state_query)
            cr.commit()

            for so in so_obj.browse(cr, uid, so_id, context=context):
                order_name = str(so.name)
                order_ref = str(so.client_order_ref)

                for co_id in close_order_tbl_ids:
                    so_state_query = "UPDATE partially_delivered_order_close SET order_name='{0}', order_ref='{1}' WHERE id='{2}'".format(
                        order_name, order_ref, co_id)

                    cr.execute(so_state_query)
                    cr.commit()
            self.pool.get('order.item.wise.close').manual_order_status_sync(cr, uid, [so_id], context=None)
        return True

{
    'name': 'Partial Delivery Order Close',
    'version': '8.0.0',
    'category': 'Sales',
    'description': "Partial Delivery Order Close",
    'author': 'Shahjalal Hossain',
    'depends': ['odoo_magento_connect','sale', 'account', 'stock', 'stock_account'],
    'data': [
            'security/partial_delivery_order_close_security.xml',
            'security/ir.model.access.csv',
            'wizard/order_close.xml',
            'sale_view.xml'
    ],
    'installable': True,
    'auto_install': False,
}

# Report DB preparation -

1. Create a new database named "reports" with role or owner "odoo"
2. Create tables -

```
Connection String :
DB: reports
IP: 
user: odoo
pass: admin1234 or postgres

ALTER USER odoo PASSWORD 'admin1234';
```
    A.
    ```SQL
    # Pending Procurement (canceled)
    CREATE TABLE pending_procurement(
        id serial PRIMARY KEY,
        sku VARCHAR (250),
        uttara_pending_qty NUMERIC,
        nodda_pending_qty NUMERIC,
        moti_pending_qty NUMERIC,
        uttara_free_stock NUMERIC,
        nodda_free_stock NUMERIC,
        moti_free_stock NUMERIC,
        pending_orders NUMERIC,
        order_number TEXT
    	)

    # Pending Procurement
    CREATE TABLE pending_procurement(
        id serial PRIMARY KEY,
        sku VARCHAR (250),
        warehouse VARCHAR (250),
        pending_qty NUMERIC,
        free_stock_qty NUMERIC,
        pending_order_number TEXT
    	)

    # Excess Inventory by Category # 60 days
    CREATE table excess_inventory_by_category(
        id serial PRIMARY KEY,
        category VARCHAR(250),
        sku VARCHAR(250),
        order_qty NUMERIC,
        sale_value NUMERIC,
        order_date DATE,
        order_no VARCHAR(250)
    )

    # Procurement Cost
    CREATE table procurement_cost(
        id serial PRIMARY KEY,
        category VARCHAR(250),
        sku VARCHAR(250),
        previous_cost NUMERIC,
        previous_po VARCHAR(250),
        recent_cost NUMERIC,
        recent_po VARCHAR(250),
        reduced_cost NUMERIC, /* (previous_cost - recent_cost) < 0 */
        increased_cost NUMERIC, /* (previous_cost - recent_cost) > 0 */
        cost_changed BOOLEAN /* (previous_cost - recent_cost) == 0 */
    )

    # COGS Report
    CREATE table cogs_report(
        id serial PRIMARY KEY,
        category VARCHAR(250),
        sku VARCHAR(250),
        prev_mon_deli_amount NUMERIC,
        cur_mon_deli_amount NUMERIC,
        prev_mon_margin NUMERIC,
        cur_mon_margin NUMERIC
    )

    # product sales
    CREATE TABLE product_sales(
        id serial PRIMARY KEY,
        line_id NUMERIC,
        sku VARCHAR(250),
        product_name VARCHAR(250),
        category VARCHAR(250),
        unit_price NUMERIC,
        quantity NUMERIC,
        order_date DATE,
        order_status VARCHAR(250),
        subtotal NUMERIC
    	)

    # sales order info
    CREATE TABLE sale_order_info(
        id serial PRIMARY KEY,
        order_id VARCHAR(250),
        order_date TIMESTAMP,
        order_source VARCHAR(250),
        order_total NUMERIC,
        order_status VARCHAR(250),
        order_delivery_state VARCHAR(250),
        order_delivery_city VARCHAR(250),
        order_delivery_country VARCHAR(250),
        customer_email VARCHAR(250),
        customer_source VARCHAR(250),
        customer_since TIMESTAMP,
        customer_classification VARCHAR(250)
    	)

    # sales sales info
    CREATE TABLE sales_info(
        id serial PRIMARY KEY,
        order_id VARCHAR(250),
        date TIMESTAMP,
        order_source VARCHAR(250),
        order_total NUMERIC,
        order_status VARCHAR(250),
        order_delivery_state VARCHAR(250),
        order_delivery_city VARCHAR(250),
        order_delivery_country VARCHAR(250),
        customer_email VARCHAR(250),
        customer_source VARCHAR(250),
        customer_since TIMESTAMP,
        customer_classification VARCHAR(250),
        sales_rep VARCHAR(250)
    	)

    # Add new column
    ALTER TABLE sales_info ADD COLUMN sales_rep VARCHAR(250)

    ALTER TABLE sales_info ADD COLUMN cus_name VARCHAR(250);
    ALTER TABLE sales_info ADD COLUMN cus_is_company VARCHAR(250);
    ALTER TABLE sales_info ADD COLUMN cus_industry VARCHAR(250);


    # retail dashboard info
    CREATE TABLE retail_dashboard(
        id serial PRIMARY KEY,
        today_date DATE,
        from_date DATE,
        to_date DATE,
        mtd_parent_account_order_count NUMERIC,
        mtd_sales_order_placed_count NUMERIC,
        total_unique_customer_count NUMERIC,
        mtd_so_unique_new_customer_count NUMERIC,
        mtd_so_unique_repeat_customer_count NUMERIC,
        mtd_so_unique_multi_order_customer_count NUMERIC,
        mtd_child_account_order_count NUMERIC,
        mtd_so_unique_regular_customer_count NUMERIC,
        today_mtd_sales_order_placed_count NUMERIC,
        mtd_sales_customer_count NUMERIC,
        mtd_so_unique_irregular_customer_count NUMERIC,
        mtd_so_unique_customer_count NUMERIC,
        actual_delivery_sales_amount NUMERIC,
        req_sales_day NUMERIC,
        refund_amount NUMERIC,
        sales_avg NUMERIC,
        delivery_sales_avg NUMERIC,
        gpm NUMERIC,
        today_mtd_sales_amount NUMERIC,
        target_amount NUMERIC,
        mtd_sales_amount NUMERIC,
        refund_sales_avg NUMERIC,
        todays_actual_delivery_amount NUMERIC,
        customer_sales_avg NUMERIC,
        sales_day NUMERIC,
        avg_order_size NUMERIC,
        mtd_so_unique_repeat_customer_percent NUMERIC,
        today_sales_customer_count NUMERIC
    	)

    ```
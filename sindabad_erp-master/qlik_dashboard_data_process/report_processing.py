from datetime import datetime, timedelta
import time
from openerp import SUPERUSER_ID
from openerp.osv import fields, osv
from openerp.tools.translate import _
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT
import openerp.addons.decimal_precision as dp
from openerp import workflow
from openerp.tools import ustr, DEFAULT_SERVER_DATE_FORMAT as DF
import psycopg2
import datetime


class sale_order(osv.osv):
    _inherit = "sale.order"

    def prepare_pending_procurement_data(self, cr, uid, ids, context=None):

        # prepare "Pending Procurement" data

        truncate_table = 'pending_procurement'
        so_obj = self.pool.get('sale.order')
        # last one month sale data
        one_month_prev = datetime.datetime.now() + datetime.timedelta(-90)

        # [19269, 19268, 19267, 19266, 19265, 19264]
        so_list = so_obj.search(cr, uid, [('state', '!=', 'done'), ('state', '!=', 'cancel'), ('date_order', '>', one_month_prev.strftime('%Y-%m-%d 23:01:01'))])

        so_data = so_obj.browse(cr, uid, so_list, context=context)

        # create db connection
        conn = self.create_connection()

        if conn:
            self.truncate_table(conn, truncate_table)

            for so in so_data:

                wh_name = str(so.warehouse_id.name)

                for line in so.order_line:
                    """
                    id serial PRIMARY KEY,
                    sku VARCHAR (250),
                    warehouse VARCHAR (250),
                    pending_qty NUMERIC,
                    free_stock_qty NUMERIC,
                    pending_order_number TEXT
                    """

                    ordered_qty = float(format(line.product_uom_qty, '.2f'))
                    available_qty = float(format(line.qty_available, '.2f'))

                    if ordered_qty > available_qty:

                        if line.default_code:

                            # Central Warehouse Uttara
                            if so.warehouse_id.id == 1:
                                free_stock_qty = line.product_id.uttwh_free_quantity
                            # Nodda Warehouse
                            elif so.warehouse_id.id == 2:
                                free_stock_qty = line.product_id.nodda_free_quantity
                            # Nodda Retail Warehouse
                            elif so.warehouse_id.id == 3:
                                free_stock_qty = line.product_id.nodda_retail_free_quantity
                            # Motijheel Warehouse
                            elif so.warehouse_id.id == 4:
                                free_stock_qty = line.product_id.moti_free_quantity
                            # Uttara Retail Warehouse
                            elif so.warehouse_id.id == 6:
                                free_stock_qty = line.product_id.uttwh_retail_free_quantity
                            # Motijheel Retail Warehouse
                            elif so.warehouse_id.id == 7:
                                free_stock_qty = line.product_id.moti_retail_free_quantity
                            else:
                                free_stock_qty = available_qty - ordered_qty

                            free_stock_qty = free_stock_qty * -1 if free_stock_qty < 0 else free_stock_qty

                            pending_qty = free_stock_qty - ordered_qty

                            pending_qty = 0 if pending_qty > 0 else pending_qty * -1

                            query = "INSERT INTO pending_procurement (sku, warehouse, pending_qty, free_stock_qty, pending_order_number) VALUES ('{0}', '{1}', '{2}', '{3}', '{4}')".format(str(line.default_code), wh_name, pending_qty, free_stock_qty, str(so.name) )

                            # call "send_data_to_report_db" with "query"
                            self.send_data_to_report_db(conn, query)

            # close db connection
            self.close_connection(conn)

        """
        product_list = list()
        product_with_so_dict = dict()
        product_with_so_qty = dict()
        warehouse_ordered_qty = dict()
        warehouse_dict = dict()

        warehouse_dict['uttara'] = 1
        warehouse_dict['nodda'] = 2
        warehouse_dict['moti'] = 4


        for so in so_data:

            for line in so.order_line:

                prod_id = int(line.product_id)

                # product list
                if prod_id not in product_list:
                    product_list.append(prod_id)

                if product_with_so_dict.has_key(prod_id):
                    product_with_so_dict[prod_id] = str(product_with_so_dict[prod_id]) + " " + str(so.name)
                else:
                    product_with_so_dict[prod_id] = str(so.name)

                if product_with_so_qty.has_key(prod_id):
                    product_with_so_qty[prod_id] = float(product_with_so_qty[prod_id]) + float(line.product_uom_qty)
                else:
                    product_with_so_qty[prod_id] = float(line.product_uom_qty)

                warehouse = int(so.warehouse_id.id)
                if warehouse_ordered_qty.has_key(int(so.warehouse_id.id)):

                    if warehouse_ordered_qty[warehouse].has_key(prod_id):
                        warehouse_ordered_qty[warehouse][prod_id] = warehouse_ordered_qty[warehouse][prod_id] + float(line.product_uom_qty)
                    else:
                        warehouse_ordered_qty[warehouse] = dict()
                        warehouse_ordered_qty[warehouse][prod_id] = float(line.product_uom_qty)

                    # warehouse_ordered_qty[warehouse] = warehouse_ordered_qty[warehouse] + float(line.product_uom_qty)
                else:
                    warehouse_ordered_qty[warehouse] = dict()
                    warehouse_ordered_qty[warehouse][prod_id] = float(line.product_uom_qty)

        prod_obj = self.pool.get('product.product')
        prod_data = prod_obj.browse(cr, uid, product_list)

        
        warehouse_ordered_qty
        {9: {20045: 1.0}, 1: {43422: 100.0}}
        

        # create db connection
        conn = self.create_connection()

        if conn:

            self.truncate_table(conn, truncate_table)

            # prepare query
            for prod in prod_data:

                pending_orders_qty = len(product_with_so_dict[prod.id].split(" "))

                uttara_ordered_qty = 0
                if warehouse_ordered_qty.has_key('1'):
                    if warehouse_ordered_qty['1'].has_key(str(prod)):
                        uttara_ordered_qty = warehouse_ordered_qty['1'][str(prod)]
                    else:
                        uttara_ordered_qty = 0
                else:
                    uttara_ordered_qty = 0

                nodda_ordered_qty = 0
                if warehouse_ordered_qty.has_key('2'):
                    if warehouse_ordered_qty['2'].has_key(str(prod)):
                        nodda_ordered_qty = warehouse_ordered_qty['2'][str(prod)]
                    else:
                        nodda_ordered_qty = 0
                else:
                    nodda_ordered_qty = 0

                moti_ordered_qty = 0
                if warehouse_ordered_qty.has_key('4'):
                    if warehouse_ordered_qty['4'].has_key(str(prod)):
                        moti_ordered_qty = warehouse_ordered_qty['4'][str(prod)]
                    else:
                        moti_ordered_qty = 0
                else:
                    moti_ordered_qty = 0

                uttara_pe = float(format(prod.uttwh_free_quantity - uttara_ordered_qty, '.2f'))
                uttara_pending = uttara_pe * -1 if uttara_pe < 0 else uttara_pe

                nodda_pe = float(format(prod.nodda_free_quantity - nodda_ordered_qty, '.2f'))
                nodda_pending = nodda_pe * -1 if nodda_pe < 0 else nodda_pe

                moti_pe = float(format(prod.moti_free_quantity - moti_ordered_qty, '.2f'))
                moti_pending = moti_pe * -1 if moti_pe < 0 else moti_pe

                if prod.default_code:
                    query = "INSERT INTO pending_procurement (sku, uttara_pending_qty, nodda_pending_qty, moti_pending_qty, uttara_free_stock, nodda_free_stock, moti_free_stock, pending_orders, order_number) VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}')".format(str(prod.default_code), uttara_pending, nodda_pending, moti_pending, prod.uttwh_free_quantity, prod.nodda_free_quantity, prod.moti_free_quantity, pending_orders_qty, product_with_so_dict[prod.id])

                    # call "send_data_to_report_db" with "query"
                    self.send_data_to_report_db(conn, query)

            # close db connection
            self.close_connection(conn)
        """

        return True

    def excess_inventory_by_category(self, cr, uid, ids, context=None):

        truncate_table = 'excess_inventory_by_category'
        so_obj = self.pool.get('sale.order')
        # last one month sale data
        one_month_prev = datetime.datetime.now() + datetime.timedelta(-60)

        # [19269, 19268, 19267, 19266, 19265, 19264]
        so_list = so_obj.search(cr, uid, [('date_order', '>', one_month_prev.strftime('%Y-%m-%d 23:01:01'))])

        so_data = so_obj.browse(cr, uid, so_list, context=context)

        # create db connection
        conn = self.create_connection()

        if conn:
            self.truncate_table(conn, truncate_table)

            for so in so_data:

                for line in so.order_line:

                    # line.product_id.product_tmpl_id.categ_id.name.parent_id
                    category = str(line.product_id.product_tmpl_id.categ_id.name)
                    sku = line.product_id.default_code
                    order_qty = line.product_uom_qty
                    sale_value = line.product_uom_qty * line.price_unit
                    order_date = str(so.date_order).split(" ")[0]
                    order_no = str(so.name)

                    if sku:
                        query = "INSERT INTO excess_inventory_by_category (category, sku, order_qty, sale_value, order_date, order_no) VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}')".format(category, sku, order_qty, sale_value, order_date, order_no)

                        # call "send_data_to_report_db" with "query"
                        self.send_data_to_report_db(conn, query)

            # close db connection
            self.close_connection(conn)

        return True

    def procurement_cost(self, cr, uid, ids, context=None):

        truncate_table = 'procurement_cost'

        pol_obj = self.pool.get('purchase.order.line')
        po_obj = self.pool.get('purchase.order')

        compare_date_obj = datetime.datetime.now() - datetime.timedelta(days=61)
        compare_date_month_obj = datetime.datetime.now() - datetime.timedelta(days=61)

        compare_date = compare_date_obj.strftime(DF)
        compare_month = compare_date_month_obj.strftime(DF)

        # [19269, 19268, 19267, 19266, 19265, 19264]
        po_list = po_obj.search(cr, uid, [('date_order', '>', compare_date)])

        po_data = po_obj.browse(cr, uid, po_list, context=context)

        # get last 90 days order's product
        # create db connection
        conn = self.create_connection()

        if conn:
            self.truncate_table(conn, truncate_table)

            for po in po_data:

                for line in po.order_line:
                    line_ids = pol_obj.search(cr, uid, [('product_id', '=', line.product_id.id), ('create_date', '<', compare_month)], context=context)

                    if line_ids:
                        # sort and take only 1
                        line_ids = [sorted(line_ids, reverse=True)[0]]

                        previous_pol_data = pol_obj.browse(cr, uid, line_ids, context=context)
                        category = str(line.product_id.product_tmpl_id.categ_id.name)
                        sku = line.product_id.default_code
                        previous_cost = previous_pol_data.price_unit
                        previous_po = po_obj.browse(cr, uid, [previous_pol_data.order_id.id], context=context).name
                        recent_cost = line.price_unit
                        recent_po = po.name

                        cost_changed = previous_cost - recent_cost
                        reduced_cost = cost_changed if cost_changed < 0 else 0
                        increased_cost = cost_changed if cost_changed > 0 else 0
                        cost_changed = True if cost_changed == 0 else False

                        if sku:
                            query = "INSERT INTO procurement_cost (category, sku, previous_cost, previous_po, recent_cost, recent_po, reduced_cost, increased_cost, cost_changed) VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}','{7}','{8}')".format(category, sku, previous_cost, previous_po, recent_cost, recent_po, reduced_cost, increased_cost, cost_changed)

                            # call "send_data_to_report_db" with "query"
                            self.send_data_to_report_db(conn, query)

            # close db connection
            self.close_connection(conn)

        return True

    def cogs_report(self, cr, uid, ids, context=None):

        truncate_table = 'cogs_report'

        so_obj = self.pool.get('sale.order')
        sol_obj = self.pool.get('sale.order.line')
        prod_obj = self.pool.get('product.product')

        # compare_date_obj = datetime.datetime.now() - datetime.timedelta(days=31)
        # compare_date_month_obj = datetime.datetime.now() - datetime.timedelta(days=31)

        # compare_date = compare_date_obj.strftime(DF)
        # compare_month = compare_date_month_obj.strftime(DF)

        # create db connection
        conn = self.create_connection()

        if conn:
            self.truncate_table(conn, truncate_table)

            # current and previous month query date
            cur_mon_query_date = str(datetime.datetime.now().year) + "-" + str(datetime.datetime.now().month) + "-1"
            prev_mon_query_date = datetime.datetime.now() - datetime.timedelta(days=31)
            prev_mon_query_date = str(prev_mon_query_date.year) + "-" + str(prev_mon_query_date.month) + "-1"

            # current month query data
            cur_mon_sol_data = sol_obj.search(cr, uid, [('create_date', '>', cur_mon_query_date), ('state', '!=', 'cancel')])
            prev_mon_sol_data = sol_obj.search(cr, uid, [('create_date', '>', prev_mon_query_date), ('create_date', '<', cur_mon_query_date), ('state', '!=', 'cancel')])

            # product dictionary
            """
            cur_mon_deli_prod = {
                    80036: 100
                    }
            
            prev_mon_deli_prod = {
                    80036: 100
                    }
            
            cur_mon_margin = {
                    80036: 10
            }
            
            prev_mon_margin = {
                    80036: 10
            }
            
            """
            cur_mon_deli_prod = dict()
            prev_mon_deli_prod = dict()

            cur_mon_margin = dict()
            prev_mon_margin = dict()

            for prod_line in sol_obj.browse(cr, uid, cur_mon_sol_data, context=context):

                product_id = int(prod_line.product_id)

                if not cur_mon_deli_prod.has_key(product_id):

                    product_qty = 0

                    cur_mon_prod_val_query = "SELECT SUM(product_uom_qty) * AVG(price_unit) AS total, SUM(product_uom_qty) AS quantity FROM sale_order_line WHERE product_id={0} AND create_date > '{1}' AND state != 'cancel'".format(product_id, cur_mon_query_date)
                    cr.execute(cur_mon_prod_val_query)

                    for val in cr.fetchall():
                        cur_mon_deli_prod[product_id] = round(val[0], 3)
                        product_qty = val[1]

                    # current month margin
                    cost_price = 0
                    prod_data = prod_obj.browse(cr, uid, [product_id], context=context)
                    for prod in prod_data:
                        cost_price = prod.standard_price

                    total_prod_cost = cost_price * product_qty

                    curr_mon_prod_margin = (100 * (cur_mon_deli_prod[product_id] - total_prod_cost)) / cur_mon_deli_prod[product_id]

                    cur_mon_margin[product_id] = round(curr_mon_prod_margin, 3)

                    # ##########################
                    # previous month calculation
                    prev_product_qty = 0
                    prev_mon_prod_val_query = "SELECT SUM(product_uom_qty) * AVG(price_unit) AS total, SUM(product_uom_qty) AS quantity FROM sale_order_line WHERE product_id={0} AND create_date > '{1}' AND create_date < '{2}' AND state != 'cancel'".format(product_id, prev_mon_query_date, cur_mon_query_date)
                    cr.execute(prev_mon_prod_val_query)

                    for val1 in cr.fetchall():

                        try:
                            prev_mon_deli_prod[product_id] = round(val1[0], 3)
                            prev_product_qty = val1[1]

                            # previous month margin
                            prev_mon_prod_margin = (100 * (prev_mon_deli_prod[product_id] - total_prod_cost)) / prev_mon_deli_prod[product_id]
                            prev_mon_margin[product_id] = round(prev_mon_prod_margin, 3)
                        except:
                            prev_mon_deli_prod[product_id] = 0
                            prev_product_qty = 0

                            # previous month margin
                            prev_mon_prod_margin = 0
                            prev_mon_margin[product_id] = 0

                    # submit product data to superset db
                    if prod_data.default_code:
                        query = "INSERT INTO cogs_report (category, sku, prev_mon_deli_amount, cur_mon_deli_amount, prev_mon_margin, cur_mon_margin) VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}')".format(str(prod_data.product_tmpl_id.categ_id.name), prod_data.default_code, prev_mon_deli_prod[product_id], cur_mon_deli_prod[product_id], prev_mon_margin[product_id], cur_mon_margin[product_id])

                        # call "send_data_to_report_db" with "query"
                        self.send_data_to_report_db(conn, query)

            # close db connection
            self.close_connection(conn)

        return True

    def remove_special_char(self, data):
        sku = str(data[2])
        product_name = str(data[1])
        if "'" in sku or '"' in sku:
            sku = sku.replace("'", "").replace('"', '')
        else:
            sku = sku

        if "'" in product_name or '"' in product_name:
            product_name = product_name.replace("'", "").replace('"', '')
        else:
            product_name = product_name
        dict_prod = dict()
        dict_prod['sku'] = sku
        dict_prod['product_name'] = product_name
        return dict_prod

    def product_sale_data(self, cr, uid, ids, context=None):

        # create db connection
        conn = self.create_connection()

        if conn:

            # create a cursor
            cur = conn.cursor()

            # submit
            cur.execute("SELECT COUNT(*) FROM product_sales")
            row_count = cur.fetchall()

            if int(row_count[0][0]) == 0:
                query = "SELECT sale_order_line.id as line_id, \
                       product_product.name_template as product_name, \
                       product_product.default_code sku, \
                       sale_order_line.price_unit as unit_price, \
                       sale_order_line.product_uom_qty as quantity, \
                       sale_order_line.create_date::date as order_date, \
                       product_category.name as category, \
                       sale_order_line.state as order_status, \
                       sale_order_line.price_unit * sale_order_line.product_uom_qty as subtotal \
                      FROM   sale_order_line \
                        INNER JOIN product_product \
                                ON sale_order_line.product_id = product_product.id \
                        INNER JOIN product_template \
                		ON product_product.product_tmpl_id=product_template.id \
                	INNER JOIN product_category \
                		ON product_template.categ_id=product_category.id "

                cr.execute(query)

                query_fetch = cr.fetchall()

                for i in query_fetch:
                    remove_special_char = self.remove_special_char(i)
                    set_data_query = "INSERT INTO product_sales(line_id, sku, product_name, category, unit_price, quantity, order_date,order_status, subtotal)VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}','{7}','{8}')".format(
                        i[0], remove_special_char['sku'], remove_special_char['product_name'], str(i[6]), float(i[3]),
                        float(i[4]), i[5], str(i[7]), float(i[8]))
                    self.send_data_to_report_db(conn, set_data_query)
            else:
                # -----------------------------------------------------------
                one_month_data_query = "SELECT sale_order_line.id as line_id, \
                               product_product.name_template as product_name, \
                               product_product.default_code sku, \
                               sale_order_line.price_unit as unit_price, \
                               sale_order_line.product_uom_qty as quantity, \
                               sale_order_line.create_date::date as order_date, \
                               product_category.name as category, \
                               sale_order_line.state as order_status, \
                               sale_order_line.price_unit * sale_order_line.product_uom_qty as subtotal \
                              FROM   sale_order_line \
                                INNER JOIN product_product \
                                        ON sale_order_line.product_id = product_product.id \
                                INNER JOIN product_template \
                        		ON product_product.product_tmpl_id=product_template.id \
                        	INNER JOIN product_category \
                        		ON product_template.categ_id=product_category.id WHERE sale_order_line.create_date > now() - interval '30 days'"
                cr.execute(one_month_data_query)

                omd_query_fetch = cr.fetchall()
                for line in omd_query_fetch:

                    check_query = "SELECT order_status FROM product_sales WHERE line_id = {0}".format(line[0])
                    cur.execute(check_query)
                    product_sales_data_query = cur.fetchall()

                    product_status = ''
                    if product_sales_data_query:
                        for status in product_sales_data_query:
                            product_status = status[0]

                        if str(line[7]) != product_status:
                            update_query = "UPDATE product_sales SET order_status='{0}' WHERE line_id={1}".format(
                                str(line[7]), line[0])
                            self.send_data_to_report_db(conn, update_query)
                        else:
                            pass
                    else:
                        remove_special_char = self.remove_special_char(line)
                        insert_query = "INSERT INTO product_sales(line_id, sku, product_name, category, unit_price, quantity, order_date,order_status, subtotal)VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}','{7}','{8}')".format(
                            line[0], remove_special_char['sku'], remove_special_char['product_name'], str(line[6]),
                            float(line[3]), float(line[4]), line[5], str(line[7]), float(line[8]))
                        self.send_data_to_report_db(conn, insert_query)
        self.close_connection(conn)

        return True

    def sale_order_info(self, cr, uid, ids, context=None):

        truncate_table = 'sales_info'
        so_obj = self.pool.get('sale.order')

        # [19269, 19268, 19267, 19266, 19265, 19264]
        so_list = so_obj.search(cr, uid, ['|', ('state', '=', 'progress'), ('state', '=', 'done')])

        so_data = so_obj.browse(cr, uid, so_list, context=context)

        # create db connection
        conn = self.create_connection()

        if conn:
            self.truncate_table(conn, truncate_table)

            for so in so_data:

                order_state = ''

                if str(so.state) == "done":
                    order_state = "delivered"
                elif str(so.state) == 'progress':
                    order_state = 'processing'
                elif so.partial_delivered:
                    order_state = 'partial_delivered'

                customer_since = "\'"+str(so.partner_id.x_created_at)+"\'" if so.partner_id.x_created_at else 'NULL'
                """
                date_order = str(so.date_order).split(" ")[0]
                if customer_since != 'NULL':
                    customer_since = customer_since.split(" ")[0] + "\'"
                """

                customer_classification = so.customer_classification
                if not so.customer_classification:
                    customer_classification = "N/A"

                """
                customer_source = so.partner_id.x_customer_source
                if not so.partner_id.x_customer_source:

                    customer_source = 'Website'
                """

                order_source = so.x_order_source
                if not order_source:
                    order_source = "Website"

                delivery_state = ''
                if so.partner_shipping_id.state_id:
                    delivery_state = str(so.partner_shipping_id.state_id.name)

                else:
                    delivery_state = "N/A"

                sales_rep = ''
                if so.custom_salesperson:
                    sales_rep = str(so.custom_salesperson.login)
                else:
                    sales_rep = 'N/A'

                parent = so.partner_id

                for i in range(5):
                    if len(parent.parent_id) == 1:
                        parent = parent.parent_id
                    else:

                        parent = parent
                        break

                cus_name = str(parent.name)
                cus_is_company = str(parent.is_company)
                cus_industry = str(parent.x_industry) if parent.x_industry else 'N/A'

                query = "INSERT INTO sales_info (order_id, date, order_source, order_total, order_status, order_delivery_state, order_delivery_city, order_delivery_country, customer_email, customer_source, customer_since, customer_classification, sales_rep, cus_name, cus_is_company, cus_industry) VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}', '{9}', {10}, $${11}$$, '{12}', $${13}$$, $${14}$$, $${15}$$)".format(str(so.client_order_ref), str(so.date_order), str(order_source), so.amount_total, str(order_state), str(delivery_state), str(so.partner_shipping_id.city), str(so.partner_shipping_id.country_id.name), str(so.partner_id.email), str(so.partner_id.x_customer_source), customer_since, str(customer_classification), sales_rep, cus_name, cus_is_company, cus_industry)

                # call "send_data_to_report_db" with "query"
                self.send_data_to_report_db(conn, query)

        return True

    def retail_dashboard_info(self, cr, uid, ids, context=None):

        truncate_table = 'retail_dashboard'
        retail_obj = self.pool.get('retail.dashboard')
        retail_data = retail_obj.browse(cr, uid, retail_obj.search(cr, uid, []), context=context)

        # create db connection
        conn = self.create_connection()

        if conn:
            self.truncate_table(conn, truncate_table)

            for data in retail_data:

                query = "INSERT INTO retail_dashboard (today_date ,from_date , to_date ,mtd_parent_account_order_count ,mtd_sales_order_placed_count ,total_unique_customer_count ,mtd_so_unique_new_customer_count ,mtd_so_unique_repeat_customer_count ,mtd_so_unique_multi_order_customer_count ,mtd_child_account_order_count ,mtd_so_unique_regular_customer_count ,today_mtd_sales_order_placed_count ,mtd_sales_customer_count ,mtd_so_unique_irregular_customer_count , mtd_so_unique_customer_count ,actual_delivery_sales_amount ,req_sales_day ,refund_amount ,sales_avg ,delivery_sales_avg ,gpm ,today_mtd_sales_amount ,target_amount ,mtd_sales_amount ,refund_sales_avg , todays_actual_delivery_amount ,customer_sales_avg ,sales_day ,avg_order_size ,mtd_so_unique_repeat_customer_percent ,today_sales_customer_count) VALUES ('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}','{11}','{12}','{13}','{14}','{15}','{16}','{17}','{18}','{19}','{20}','{21}','{22}','{23}','{24}','{25}','{26}','{27}','{28}','{29}','{30}')".\
                    format(str(data.today_date) ,str(data.from_date), str(data.to_date),str(data.mtd_parent_account_order_count),str(data.mtd_sales_order_placed_count),str(data.total_unique_customer_count),str(data.mtd_so_unique_new_customer_count),str(data.mtd_so_unique_repeat_customer_count),str(data.mtd_so_unique_multi_order_customer_count),str(data.mtd_child_account_order_count),str(data.mtd_so_unique_regular_customer_count),str(data.today_mtd_sales_order_placed_count),str(data.mtd_sales_customer_count),str(data.mtd_so_unique_irregular_customer_count),str(data. mtd_so_unique_customer_count),
                           str(data.actual_delivery_sales_amount),str(data.req_sales_day) ,str(data.refund_amount),str(data.sales_avg),str(data.delivery_sales_avg),str(data.gpm),str(data.today_mtd_sales_amount),str(data.target_amount),str(data.mtd_sales_amount),str(data.refund_sales_avg), str(data.todays_actual_delivery_amount),str(data.customer_sales_avg),str(data.sales_day),str(data.avg_order_size),str(data.mtd_so_unique_repeat_customer_percent),str(data.today_sales_customer_count))
                # call "send_data_to_report_db" with "query"
                self.send_data_to_report_db(conn, query)

        return True

    def create_connection(self):

        # create connection to "reports" database
        conn = psycopg2.connect(host='192.168.9.77', dbname='reports2', user='postgres', password='sindabad@321#', port='5432')

        return conn

    def close_connection(self, conn):

        conn.close()

        return True

    def truncate_table(self, conn, truncate_table):

        try:

            trunc_query = "TRUNCATE TABLE " + truncate_table
            # create a cursor
            cur = conn.cursor()

            # truncate before submit
            cur.execute(trunc_query)
            conn.commit()

            cur.close()

        except (Exception, psycopg2.DatabaseError) as error:
            cur.close()

        return True

    def send_data_to_report_db(self, conn, query):

        try:

            # create a cursor
            cur = conn.cursor()

            # submit
            cur.execute(query)
            conn.commit()

            cur.close()

        except (Exception, psycopg2.DatabaseError) as error:

            cur.close()

        return True

    # def dashboard_rpt_sync_scheduler(self, cr, uid, active=False, ids=None, cron_mode=True, context=None):
    #
    #     # self.prepare_pending_procurement_data(cr, uid, ids, context=context)
    #     self.excess_inventory_by_category(cr, uid, ids, context=context)
    #     self.procurement_cost(cr, uid, ids, context=context)
    #     # self.cogs_report(cr, uid, ids, context=context)
    #
    #     self.product_sale_data(cr, uid, ids, context=context)
    #
    #     return True

    # execute excess_inventory scheduler
    def dashboard_rpt_excess_inventory(self, cr, uid, active=False, ids=None, cron_mode=True, context=None):

        self.excess_inventory_by_category(cr, uid, ids, context=context)

        return True

    def sale_order_info_for_dashboard(self, cr, uid, active=False, ids=None, cron_mode=True, context=None):

        self.sale_order_info(cr, uid, ids, context=context)
        self.retail_dashboard_info(cr,uid,ids,context=context)

        return True

    # execute procurement_cost scheduler
    def dashboard_rpt_procurement_cost(self, cr, uid, active=False, ids=None, cron_mode=True, context=None):

        self.procurement_cost(cr, uid, ids, context=context)

        return True

    # execute product_sale_data scheduler
    def dashboard_rpt_product_sale_data(self, cr, uid, active=False, ids=None, cron_mode=True, context=None):

        self.product_sale_data(cr, uid, ids, context=context)

        return True

    # delete this after done. only need to run from in button action
    def action_sync_superset_data(self, cr, uid, ids, context=None):

        # self.dashboard_rpt_sync_scheduler(cr, uid, active=False, ids=ids, cron_mode=True, context=None)

        return True


{
    'name': 'Qlik Dashboard Data Process',
    'version': '1.0',
    'category': 'Qlik',
    'author': 'Shahjalal',
    'summary': 'Qlik Dashboard Data Process',
    'description': 'Qlik Dashboard Data Process',
    "website": "Sindabad.com",
    'depends': ['base', 'odoo_magento_connect', 'account', 'sale', 'purchase', 'report_xls', 'sales_collection_billing_process'],
    'data': [
        'qlik_sync_from_so.xml',
        'dashboard_report_sync_scheduler_new.xml',
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}

# -*- coding: utf-8 -*-
##########################################################################
#
#    Copyright (c) 2017-Present Webkul Software Pvt. Ltd. (<https://webkul.com/>)
#
##########################################################################

from openerp.osv import fields, osv
from openerp.tools.translate import _
import openerp.netsvc
import xmlrpclib


class magento_wizard(osv.osv_memory):
    _name = "magento.wizard"

    _columns = {
        'magento_store_view': fields.many2one('magento.store.view', string='Default Magento Store'),
    }

    def buttun_select_default_magento_store(self, cr, uid, ids, context=None):
        context = dict(context or {})
        if context.has_key('active_id'):
            obj = self.browse(cr, uid, ids, context)
            self.pool['magento.configure'].write(cr, uid, context['active_id'], {
                                                 'store_id': obj.magento_store_view.id})
        partial = self.pool['message.wizard'].create(
            cr, uid, {'text': context['text']})

        return {'name'		: ("Information"),
                'view_mode': 'form',
                'view_type': 'form',
                'res_model': 'message.wizard',
                'view_id'	: False,
                'res_id'	: partial,
                'type'		: 'ir.actions.act_window',
                'nodestroy': True,
                'target'	: 'new',
                }

# -*- coding: utf-8 -*-
##########################################################################
#
#    Copyright (c) 2017-Present Webkul Software Pvt. Ltd. (<https://webkul.com/>)
#
##########################################################################

import requests
import json
import xmlrpclib
from openerp.osv import fields, osv
from openerp.tools.translate import _
import openerp.netsvc
from openerp.http import request

class message_wizard(osv.osv_memory):
    _name = "message.wizard"
    _columns = {
        'text': fields.text('Message', readonly=True, translate=True),
    }

message_wizard()


class region_wizard(osv.osv_memory):
    _name = "region.wizard"
    _columns = {
        'country_ids': fields.many2one('res.country', 'Country'),
    }

    def _sync_mage_region(self, cr, uid, url, token, country_code, context=None):
        stateData = {}
        server = xmlrpclib.Server(url)
        try:
            attr_url = url + "/index.php/rest/V1/directory/countries/" + \
                str(country_code)
            token = token.replace('"', "")
            userAgent = request.httprequest.environ.get('HTTP_USER_AGENT', '')
            headers = {'Authorization': token,
                       'Content-Type': 'application/json', 'User-Agent': userAgent}
            region_data = requests.get(attr_url, headers=headers, verify=False)
            if region_data.status_code != 200:
                raise osv.except_osv(_('Error'), _(
                    "Requested country is not available at Magento %r ") % str(country_code))
            region_data = json.loads(region_data.text)
        except xmlrpclib.Fault, e:
            raise osv.except_osv(_('Error'), _(" %s") % e)
        if region_data:
            region_data['name'] = region_data['full_name_english']
            region_data['region_code'] = region_data['id']
            region_data['country_code'] = country_code
            self.pool.get('magento.region').create(cr, uid, region_data)
            if country_code != 'US':
                country_ids = self.pool.get('res.country').search(
                    cr, uid, [('code', '=', country_code)])
                stateData['name'] = region_data['name']
                stateData['country_id'] = country_ids[0]
                stateData['code'] = region_data['name'][:2].upper()
                self.pool.get('res.country.state').create(cr, uid, stateData)

            return len(region_data)
        else:
            return 0

    def sync_state(self, cr, uid, ids, context=None):
        config_id = self.pool.get('magento.configure').search(
            cr, uid, [('active', '=', True)])
        if len(config_id) > 1:
            raise osv.except_osv(_('Error'), _(
                "Sorry, only one Active Configuration setting is allowed."))
        if not config_id:
            raise osv.except_osv(_('Error'), _(
                "Please create the configuration part for connection!!!"))
        else:
            obj = self.pool.get('magento.configure').browse(
                cr, uid, config_id[0])
            connection = self.pool.get(
                'magento.configure')._create_connection(cr, uid, context)
            url = connection[0]
            token = connection[1]
            context['instance_id'] = instance_id = connection[2]
            if token:
                country_id = self.browse(cr, uid, ids[0]).country_ids
                country_code = self.pool.get('res.country').browse(
                    cr, uid, country_id.id).code
                map_id = self.pool.get('magento.region').search(
                    cr, uid, [('country_code', '=', country_code)])
                if not map_id:
                    total_regions = self._sync_mage_region(
                        cr, uid, url, token, country_code)
                    if total_regions == 0:
                        raise osv.except_osv(_('Error'), _(
                            "There is no any region exist for country %s.") % (country_id.name))
                        return {
                            'type': 'ir.actions.act_window_close',
                        }
                    else:
                        text = "%s Region of %s are sucessfully Imported to OpenERP." % (
                            total_regions, country_id.name)
                        partial = self.pool.get('message.wizard').create(
                            cr, uid, {'text': text}, context)
                        return {'name': _("Message"),
                                'view_mode': 'form',
                                'view_id': False,
                                'view_type': 'form',
                                'res_model': 'message.wizard',
                                'res_id': partial,
                                'type': 'ir.actions.act_window',
                                'nodestroy': True,
                                'target': 'new',
                                'domain': '[]',
                                }
                else:
                    raise osv.except_osv(_('Information'), _(
                        "All regions of %s are already imported to OpenERP.") % (country_id.name))
region_wizard()

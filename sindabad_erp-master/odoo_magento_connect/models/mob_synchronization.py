# -*- coding: utf-8 -*-
##########################################################################
#
#    Copyright (c) 2017-Present Webkul Software Pvt. Ltd. (<https://webkul.com/>)
#
##########################################################################

import urllib
import xmlrpclib
import requests
import json
from openerp import SUPERUSER_ID
from openerp.osv import fields, osv
from openerp.http import request
from openerp.tools.translate import _
import logging
_logger = logging.getLogger(__name__)

try:
    import cStringIO as StringIO
except ImportError:
    import StringIO
from PIL import Image


class magento_synchronization(osv.osv_memory):
    _name = "magento.synchronization"
    _description = "Magento Synchronization"

    def open_configuration(self, cr, uid, ids, context=None):
        context = dict(context or {})
        view_id = False
        setting_ids = self.pool.get('magento.configure').search(
            cr, uid, [('active', '=', True)])
        if setting_ids:
            view_id = setting_ids[0]
        return {
            'type': 'ir.actions.act_window',
            'name': 'Configure Magento Api',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'magento.configure',
            'res_id': view_id,
            'target': 'current',
            'domain': '[]',
        }

    def sync_attribute_set(self, cr, uid, data, context=None):
        context = dict(context or {})
        erp_set_id = 0
        set_dic = {}
        res = False
        set_pool = self.pool.get('magento.attribute.set')

        if data.has_key('name') and data.get('name'):
            set_map_ids = set_pool.search(
                cr, uid, [('name', '=', data.get('name'))])
            if not set_map_ids:
                set_dic['name'] = data.get('name')
                if data.has_key('set_id') and data.get('set_id'):
                    set_dic['set_id'] = data.get('set_id')
                set_dic['created_by'] = 'Magento'
                set_dic['instance_id'] = context.get('instance_id')
                erp_set_id = set_pool.create(cr, uid, set_dic)
            else:
                erp_set_id = set_map_ids[0]
            if erp_set_id:
                if data.has_key('set_id') and data.get('set_id'):
                    dic = {}
                    dic['set_id'] = data.get('set_id')
                    if data.has_key('attribute_ids') and data.get('attribute_ids'):
                        dic['attribute_ids'] = [
                            (6, 0, data.get('attribute_ids'))]
                    else:
                        dic['attribute_ids'] = [[5]]
                    if context.has_key('instance_id') and context['instance_id']:
                        dic['instance_id'] = context.get('instance_id')
                    res = set_pool.write(cr, uid, erp_set_id, dic, context)
        return res

    #############################################
    ##   Export Attributes and values          ##
    #############################################
    def export_attributes_and_their_values(self, cr, uid, ids, context=None):
        context = dict(context or {})
        map_dic = []
        map_dict = {}
        error_message = ''
        attribute_count = 0
        attribute_pool = self.pool.get('product.attribute')
        attribute_value_pool = self.pool.get('product.attribute.value')
        attribute_mapping_pool = self.pool.get('magento.product.attribute')
        value_mapping_pool = self.pool.get('magento.product.attribute.value')
        connection = self.pool.get(
            'magento.configure')._create_connection(cr, uid, context)
        if connection:
            url = connection[0]
            token = connection[1]
            context['instance_id'] = instance_id = connection[2]
            attribute_map_ids = attribute_mapping_pool.search(
                cr, uid, [('instance_id', '=', instance_id)])
            for attribute_map_id in attribute_map_ids:
                attribute_map_obj = attribute_mapping_pool.browse(
                    cr, uid, attribute_map_id)
                map_dic.append(attribute_map_obj.erp_id)
                map_dict.update({attribute_map_obj.erp_id: [
                    attribute_map_obj.mage_id, attribute_map_obj.mage_attribute_code]})
            attribute_ids = attribute_pool.search(cr, uid, [], context=context)
            if attribute_ids:
                for attribute_id in attribute_ids:
                    attribute_obj = attribute_pool.browse(
                        cr, uid, attribute_id, context)
                    if attribute_id not in map_dic:
                        name = attribute_obj.name
                        label = attribute_obj.name
                        attribute_code = attribute_obj.name.lower().replace(" ", "_").replace("-", "_")
                        attribute_response = self.create_product_attribute(
                            cr, uid, url, token, attribute_id, name, label, context)
                    else:
                        map_data = map_dict.get(attribute_id)
                        mage_id = map_data[0]
                        attribute_code = map_data[1]
                        attribute_response = [1, int(mage_id)]
                    if attribute_response[0] == 0:
                        error_message = error_message + attribute_response[1]
                    if attribute_response[0] > 0:
                        mage_id = attribute_response[1]
                        attribute_value_dict = {}

                        for value_obj in attribute_obj.value_ids:
                            attribute_value_dict[value_obj.name] = value_obj.id
                            if not value_mapping_pool.search(cr, uid, [('erp_id', '=', value_obj.id), ('instance_id', '=', instance_id)]):
                                name = value_obj.name
                                position = value_obj.sequence
                                value_response = self.create_attribute_value(
                                    cr, uid, url, token, attribute_code, value_obj.id, name, position, context)
                                if value_response[0] == 0:
                                    error_message = error_message + \
                                        value_response[1]
                        self.map_attribute_values(
                            cr, uid, url, token, attribute_code, attribute_value_dict, context)
                        attribute_count += 1
            else:
                error_message = "No Attribute(s) Found To Be Export At Magento!!!"
            if attribute_count:
                error_message += "\n %s Attribute(s) and their value(s) successfully Synchronized To Magento." % (
                    attribute_count)
            partial = self.pool.get('message.wizard').create(
                cr, uid, {'text': error_message})
            return {
                'name': _("Message"),
                'view_mode': 'form',
                'view_id': False,
                'view_type': 'form',
                'res_model': 'message.wizard',
                'res_id': partial,
                'type': 'ir.actions.act_window',
                'nodestroy': True,
                'target': 'new',
                'domain': '[]',
            }

    def create_product_attribute(self, cr, uid, url, token, attribute_id, name, label, context=None):
        context = dict(context or {})
        name = name.lower().replace(" ", "_").replace("-", "_")
        name = name.strip()
        if token:
            attrribute_data = {"attribute": {
                'attributeCode': name,
                'scope': 'global',
                'frontendInput': 'select',
                'isRequired': 0,
                'frontendLabels': [{'storeId': 0, 'label': label}]
            }
            }
            cc = json.dumps(attrribute_data)
            attr_url = url + "/index.php/rest/V1/products/attributes"
            token = token.replace('"', "")
            userAgent = request.httprequest.environ.get('HTTP_USER_AGENT', '')
            headers = {'Authorization': token,
                       'Content-Type': 'application/json', 'User-Agent': userAgent}
            mage_id = requests.post(attr_url, data=cc, headers=headers, verify=False)
            mage_id = json.loads(mage_id.text)
            mage_attribute_id = 0

            if mage_id.has_key('id') and mage_id['id'] > 0:
                mage_attribute_id = mage_id['id']
                mage_id = [1, mage_attribute_id]
            else:
                attr_url = url + "/index.php/rest/V1/products/attributes/" + name
                attribute_data = requests.get(
                    attr_url, data=cc, headers=headers, verify=False)
                attribute_data = json.loads(attribute_data.text)
                if attribute_data.get('attribute_id', 0) > 0:
                    mage_attribute_id = attribute_data['attribute_id']
                    mage_id = [1, mage_attribute_id]
                else:
                    mage_id = [0, attribute_data]
            erp_map_data = {
                'name': attribute_id,
                'erp_id': attribute_id,
                'mage_id': mage_attribute_id,
                'mage_attribute_code': name,
                'instance_id': context.get('instance_id'),
            }
            self.pool.get('magento.product.attribute').create(
                cr, uid, erp_map_data)
            map_product_data = {'attribute': {
                'name':name,
                'magento_id': mage_attribute_id, 
                'odoo_id': attribute_id, 
                'created_by': 'Odoo'
            }}
            cc = json.dumps(map_product_data)
            map_option_url = url + "/index.php/rest/V1/odoomagentoconnect/attribute"
            requests.post(map_option_url, data=cc, headers=headers, verify=False)
            return mage_id

    def create_attribute_value(self, cr, uid, url, token, attribute_code, erp_attr_id, name, position='0', context=None):
        if context is None:
            context = {}
        if token:
            name = name.strip()
            options_data = {"option": {
                "label": name,
                "sortOrder": position,
                "isDefault": 0,
                'storeLabels': [{'storeId': 0, 'label': name}]
            }}
            cc = json.dumps(options_data)
            attr_option_url = url + "/index.php/rest/V1/products/attributes/" + \
                attribute_code + "/options"
            token = token.replace('"', "")
            userAgent = request.httprequest.environ.get('HTTP_USER_AGENT', '')
            headers = {'Authorization': token,
                       'Content-Type': 'application/json', 'User-Agent': userAgent}
            mage_res = requests.post(attr_option_url, data=cc, headers=headers, verify=False)
            mage_id = json.loads(mage_res.text)
            if mage_res.ok :
                return [1, mage_id]
            else :
                return [0, mage_id.get('message', '')]

    def map_attribute_values(self, cr, uid, url, token, attribute_code, attribute_value_dict, context=None):
        context = dict(context or {})
        attr_option_url = url + "/index.php/rest/V1/products/attributes/" + \
            attribute_code + "/options"
        token = token.replace('"', "")
        userAgent = request.httprequest.environ.get('HTTP_USER_AGENT', '')
        headers = {'Authorization': token, 'Content-Type': 'application/json', 'User-Agent': userAgent}
        mage_ids = json.loads(requests.get(
            attr_option_url, headers=headers, verify=False).text)
        if type(mage_ids) is list:
            for mage_id in mage_ids:
                if mage_id.has_key('value') and mage_id['value']:
                    map_attribute_value_obj = self.pool['magento.product.attribute.value'].search(cr, uid, [(
                        'mage_id', '=', int(mage_id['value'])), ('instance_id', '=', context.get('instance_id'))])
                    if len(map_attribute_value_obj) == 0 and attribute_value_dict.has_key(mage_id['label']):
                        erp_map_data = {
                            'name': attribute_value_dict[mage_id['label']],
                            'erp_id': attribute_value_dict[mage_id['label']],
                            'mage_id': int(mage_id['value']),
                            'instance_id': context.get('instance_id')
                        }
                        self.pool['magento.product.attribute.value'].create(
                            cr, uid, erp_map_data)
                        map_product_data = {'option': {
                            'name': mage_id['label'], 
                            'magento_id': mage_id['value'], 
                            'odoo_id': attribute_value_dict[mage_id['label']], 
                            'created_by': 'Odoo'
                        }}
                        cc = json.dumps(map_product_data)
                        map_option_url = url + "/index.php/rest/V1/odoomagentoconnect/option"
                        mage_id = requests.post(
                            map_option_url, data=cc, headers=headers, verify=False)
        return True

    def assign_attribute_Set(self, cr, uid, template_ids, context=None):
        context = dict(context or {})
        temp_pool = self.pool.get('product.template')
        connection = self.pool.get('magento.configure')._create_connection(
            cr, uid, context=context)
        if connection:
            success_ids = []
            for temp_id in template_ids:
                attribute_line_ids = temp_pool.browse(
                    cr, uid, temp_id).attribute_line_ids
                set_id = self.get_default_attribute_set(
                    cr, uid, context=context)
                if attribute_line_ids:
                    set_id = self.get_magento_attribute_set(
                        cr, uid, attribute_line_ids, context=context)

                if set_id:
                    temp_pool.write(cr, uid, temp_id, {
                                    'attribute_set_id': set_id}, context=context)
                    success_ids.append(temp_id)
        else:
            raise osv.except_osv(("Connection Error"),
                                 ("Error in Odoo Connection"))
        return True

    def get_default_attribute_set(self, cr, uid, context=None):
        context = dict(context or {})
        mage_set_pool = self.pool.get('magento.attribute.set')
        default_search = mage_set_pool.search(
            cr, uid, [('set_id', '=', 4), ('instance_id', '=', context['instance_id'])])
        if default_search:
            return default_search[0]
        else:
            raise osv.except_osv(_('Information'), _(
                "Default Attribute set not Found, please sync all Attribute set from Magento!!!"))

    def get_magento_attribute_set(self, cr, uid, attribute_line_ids, context=None):
        context = dict(context or {})
        flag = False
        attribute_ids = []
        template_attribute_ids = []
        mage_set_pool = self.pool.get('magento.attribute.set')
        for attr in attribute_line_ids:
            template_attribute_ids.append(attr.attribute_id.id)
        set_ids = mage_set_pool.search(
            cr, uid, [('instance_id', '=', context['instance_id'])], order="set_id asc")
        for set_id in set_ids:
            set_attribute_ids = mage_set_pool.browse(
                cr, uid, set_id, context=context).attribute_ids.ids
            common_attributes = list(
                set(set_attribute_ids) & set(template_attribute_ids))
            common_attributes.sort()
            template_attribute_ids.sort()
            if common_attributes == template_attribute_ids:
                return set_id

        return False

    #############################################
    ##      Start Of Category Synchronizations ##
    #############################################

    #############################################
    ##          Export/Update Categories       ##
    #############################################

    def get_map_category_ids(self, cr, uid, category_ids, context=None):
        context = dict(context or {})
        product_category_ids = []
        mage_cat_pool = self.pool.get('magento.category')
        map_ids = mage_cat_pool.search(
            cr, uid, [('instance_id', '=', context['instance_id'])], context=context)
        for map_id in map_ids:
            product_category_ids.append(
                mage_cat_pool.browse(cr, uid, map_id).oe_category_id)
        erp_category_ids = list(set(category_ids) | set(product_category_ids))
        erp_category_ids = list(set(erp_category_ids) ^
                                set(product_category_ids))
        return erp_category_ids

    def get_update_category_ids(self, cr, uid, category_ids, context=None):
        context = dict(context or {})
        map_category_ids = []
        mage_cat_pool = self.pool.get('magento.category')
        map_ids = mage_cat_pool.search(cr, uid, [('need_sync', '=', 'Yes'), (
            'mag_category_id', '!=', -1), ('instance_id', '=', context['instance_id'])], context=context)
        for map_id in map_ids:
            if mage_cat_pool.browse(cr, uid, map_id).oe_category_id in category_ids:
                map_category_ids.append(map_id)
        return map_category_ids

    def export_categories_check(self, cr, uid, context=None):
        context = dict(context or {})
        text = text1 = text2 = ''
        up_error_ids = []
        success_ids = []
        success_up_ids = []
        category_ids = []
        connection = self.pool.get('magento.configure')._create_connection(
            cr, uid, context=context)
        if connection:
            mage_cat_pool = self.pool.get('magento.category')
            mage_sync_history = self.pool.get('magento.sync.history')
            url = connection[0]
            token = connection[1]
            instance_id = context['instance_id'] = connection[2]

            if context.has_key('active_model') and context.get('active_model') == "product.category":
                category_ids = context.get('active_ids')
            else:
                category_ids = self.pool.get('product.category').search(
                    cr, uid, [], context=context)

            if context.has_key('sync_opr') and context['sync_opr'] == 'export':
                erp_category_ids = self.get_map_category_ids(
                    cr, uid, category_ids, context=context)
                if not erp_category_ids:
                    raise osv.except_osv(_('Information'), _(
                        "All category(s) has been already exported on magento."))
                for erp_category_id in erp_category_ids:
                    categ_id = self.sync_categories(
                        cr, uid, url, token, erp_category_id, context=context)
                    if categ_id:
                        success_ids.append(categ_id)
                        text = "\nThe Listed category ids %s has been created on magento." % (
                            success_ids)
                        mage_sync_history.create(cr, uid, {
                                                 'status': 'yes', 'action_on': 'category', 'action': 'b', 'error_message': text}, context)

            if context.has_key('sync_opr') and context['sync_opr'] == 'update':
                update_map_ids = self.get_update_category_ids(
                    cr, uid, category_ids, context=context)
                if not update_map_ids:
                    raise osv.except_osv(_('Information'), _(
                        "All category(s) has been already updated on magento."))
                cat_update = self._update_specific_category(
                    cr, uid, update_map_ids, url, token, context=context)
                if cat_update:
                    if cat_update[0] != 0:
                        success_up_ids.append(cat_update[1])
                        text1 = '\nThe Listed category ids %s has been successfully updated to Magento. \n' % success_up_ids
                        mage_sync_history.create(cr, uid, {
                                                 'status': 'yes', 'action_on': 'category', 'action': 'c', 'error_message': text1}, context)
                    else:
                        up_error_ids.append(cat_update[1])
                        text2 = '\nThe Listed category ids %s does not updated on magento.' % up_error_ids
                        mage_sync_history.create(cr, uid, {
                                                 'status': 'no', 'action_on': 'category', 'action': 'c', 'error_message': text2}, context)

            partial = self.pool.get('message.wizard').create(
                cr, uid, {'text': text + text1 + text2})
            return {'name': _("Information"),
                    'view_mode': 'form',
                    'view_id': False,
                    'view_type': 'form',
                    'res_model': 'message.wizard',
                    'res_id': partial,
                    'type': 'ir.actions.act_window',
                    'nodestroy': True,
                    'target': 'new',
                    'domain': '[]',
                    }
        ########## update specific category ##########

    def _update_specific_category(self, cr, uid, update_map_ids, url, token, context=None):
        context = dict(context or {})
        cat_mv = False
        category_ids = []
        cat_pool = self.pool.get('magento.category')
        for id in update_map_ids:
            cat_obj = cat_pool.browse(cr, uid, id)
            cat_id = cat_obj.oe_category_id
            mage_id = cat_obj.mag_category_id
            mag_parent_id = 1
            if cat_id and mage_id:
                get_category_data = {}
                category_ids.append(cat_id)
                obj_cat = self.pool.get('product.category').browse(
                    cr, uid, cat_id, context=context)
                get_category_data['name'] = obj_cat.name
                parent_id = obj_cat.parent_id.id or False
                if parent_id:
                    search = cat_pool.search(cr, uid, [(
                        'cat_name', '=', parent_id), ('instance_id', '=', context['instance_id'])])
                    if search:
                        mag_parent_id = cat_pool.browse(
                            cr, uid, search[0], context=context).mag_category_id or 1
                    else:
                        mag_parent_id = self.sync_categories(
                            cr, uid, url, token, parent_id, context)
                get_category_data['parent_id'] = mag_parent_id
                get_category_data = {"category": get_category_data}
                cc = json.dumps(get_category_data)
                cat_url = url + "/index.php/rest/V1/categories/" + str(mage_id)
                token = token.replace('"', "")
                userAgent = request.httprequest.environ.get('HTTP_USER_AGENT', '')
                headers = {'Authorization': token,
                           'Content-Type': 'application/json', 'User-Agent': userAgent}
                mage_cat = requests.put(cat_url, data=cc, headers=headers, verify=False)
                map_product_data = {'categoryId': mage_id,'name': obj_cat.name}
                cc = json.dumps(map_product_data)
                map_category_url = url + "/index.php/rest/V1/odoomagentoconnect/category"
                mage_id = requests.put(
                    map_category_url, data=cc, headers=headers, verify=False)
                cat_pool.write(cr, uid, id, {'need_sync': 'No'}, context)
        return [1, category_ids]

    def sync_categories(self, cr, uid, url, token, erp_category_id, context=None):
        context = dict(context or {})
        map_category_obj = self.pool.get('magento.category')
        instance_id = 0
        if context.has_key('instance_id'):
            instance_id = context['instance_id']
        else:
            connection = self.pool.get('magento.configure')._create_connection(
                cr, uid, context=context)
            if connection:
                instance_id = connection[2]
        if erp_category_id:
            map_category_ids = map_category_obj.search(
                cr, uid, [('cat_name', '=', erp_category_id), ('instance_id', '=', instance_id)])
            if not map_category_ids:
                obj_catg = self.pool.get('product.category').browse(
                    cr, uid, erp_category_id, context=context)
                name = obj_catg.name
                if obj_catg.parent_id.id:
                    p_cat_id = self.sync_categories(
                        cr, uid, url, token, obj_catg.parent_id.id, context)
                else:
                    p_cat_id = self.create_category(
                        cr, uid, url, token, erp_category_id, 1, name, context)
                    if p_cat_id[0] > 0:
                        return p_cat_id[1]
                    else:
                        return False
                category_id = self.create_category(
                    cr, uid, url, token, erp_category_id, p_cat_id, name, context)
                if category_id[0] > 0:
                    return category_id[1]
                else:
                    False
            else:
                mage_id = map_category_obj.browse(
                    cr, uid, map_category_ids[0]).mag_category_id
                return mage_id
        else:
            return False

    def create_category(self, cr, uid, url, token, catg_id, parent_id, catgname, context=None):
        context = dict(context or {})
        if catg_id < 1:
            return False

        catgdetail = {"category": {
            'name': catgname,
            'parentId': parent_id,
            'isActive': 1,
            'includeInMenu': 1
        }}
        cc = json.dumps(catgdetail)
        cat_url = url + "/index.php/rest/V1/categories"
        token = token.replace('"', "")
        userAgent = request.httprequest.environ.get('HTTP_USER_AGENT', '')
        headers = {'Authorization': token, 'Content-Type': 'application/json', 'User-Agent': userAgent}
        mage_cats = requests.post(cat_url, data=cc, headers=headers, verify=False)
        mage_cat = json.loads(mage_cats.text)
        if mage_cat.has_key('id') and mage_cat['id'] > 0:
            ##########  Mapping Entry  ###########
            self.pool.get('magento.category').create(cr, uid, {'cat_name': catg_id, 'oe_category_id': catg_id, 'mag_category_id': mage_cat[
                'id'], 'created_by': 'odoo', 'instance_id': context.get('instance_id')})
            map_product_data = {'category': {'magento_id': mage_cat['id'], 'odoo_id': catg_id, 'created_by': 'Odoo'}}
            cc = json.dumps(map_product_data)
            map_category_url = url + "/index.php/rest/V1/odoomagentoconnect/category"
            mage_id = requests.post(map_category_url, data=cc, headers=headers, verify=False)
            return [1, mage_cat['id']]
        else:
            return [0, mage_cat]

    #############################################
    ##      End Of Category Synchronizations   ##
##########################################################

##########################################################
    ##      Start Of Product Synchronizations  ##
    #############################################

    #############################################
    ##          export all products            ##
    #############################################

    def get_map_template_ids(self, cr, uid, product_template_ids, context=None):
        context = dict(context or {})
        template_ids = []
        update_template_map_ids = []
        mage_product_obj = self.pool.get('magento.product.template')
        if context.has_key('sync_opr') and context['sync_opr'] == 'export':
            map_ids = mage_product_obj.search(
                cr, uid, [('instance_id', '=', context['instance_id'])], context=context)
            for map_id in map_ids:
                map_obj = mage_product_obj.browse(cr, uid, map_id)
                erp_template_id = map_obj.erp_template_id
                template_ids.append(erp_template_id)
            not_mapped_template_ids = list(
                set(product_template_ids) | set(template_ids))
            not_mapped_template_ids = list(
                set(not_mapped_template_ids) ^ set(template_ids))
            return not_mapped_template_ids
        if context.has_key('sync_opr') and context['sync_opr'] == 'update':
            for product_template_id in product_template_ids:
                map_ids = mage_product_obj.search(cr, uid, [('instance_id', '=', context['instance_id']), (
                    'need_sync', '=', 'Yes'), ('erp_template_id', '=', product_template_id)], context=context)
                if map_ids:
                    update_template_map_ids.append(map_ids[0])
            return update_template_map_ids
        return False

    #############################################
    ##  export bulk/selected products template ##
    #############################################

    def export_product_check(self, cr, uid, context=None):
        context = dict(context or {})
        text = text1 = text2 = ''
        up_error_ids = []
        success_ids = []
        error_ids = []
        success_exp_ids = []
        success_up_ids = []
        template_ids = []
        not_mapped_template_ids = 0
        update_mapped_template_ids = 0
        connection = self.pool.get('magento.configure')._create_connection(
            cr, uid, context=context)
        if connection:
            mage_sync_history = self.pool.get('magento.sync.history')
            template_obj = self.pool.get('product.template')
            url = connection[0]
            token = connection[1]
            instance_id = context['instance_id'] = connection[2]
            pro_dt = len(self.pool.get('product.attribute').search(
                cr, uid, [], context=context))
            map_dt = len(self.pool.get('magento.product.attribute').search(
                cr, uid, [('instance_id', '=', context['instance_id'])], context=context))
            pro_op = len(self.pool.get('product.attribute.value').search(
                cr, uid, [], context=context))
            map_op = len(self.pool.get('magento.product.attribute.value').search(
                cr, uid, [('instance_id', '=', context['instance_id'])]))
            if pro_dt != map_dt or pro_op != map_op:
                raise osv.except_osv(('Warning'), _(
                    'Please, first map all ERP Product attributes and it\'s all options'))
            if context.has_key('active_model') and context.get('active_model') == "product.template":
                template_ids = context.get('active_ids')
            else:
                template_ids = template_obj.search(
                    cr, uid, [('type', '!=', 'service')], context=context)
            if not template_ids:
                raise osv.except_osv(_('Information'), _(
                    "No new product(s) Template found to be Sync."))

            if context.has_key('sync_opr') and context['sync_opr'] == 'export':
                not_mapped_template_ids = self.get_map_template_ids(
                    cr, uid, template_ids, context=context)
                if not not_mapped_template_ids:
                    raise osv.except_osv(_('Information'), _(
                        "Listed product(s) has been already exported on magento."))
                for template_id in not_mapped_template_ids:
                    prodtype = template_obj.browse(
                        cr, uid, template_id, context=context).type
                    if prodtype == 'service':
                        error_ids.append(template_id)
                        continue
                    pro = self._export_specific_template(
                        cr, uid, template_id, url, token, context=context)
                    if pro:
                        if pro[0] > 0:
                            success_exp_ids.append(template_id)
                        else:
                            error_ids.append(pro[1])
                    else:
                        continue

            if context.has_key('sync_opr') and context['sync_opr'] == 'update':
                update_mapped_template_ids = self.get_map_template_ids(
                    cr, uid, template_ids, context=context)
                if not update_mapped_template_ids:
                    raise osv.except_osv(_('Information'), _(
                        "Listed product(s) has been already updated on magento."))
                for template_map_id in update_mapped_template_ids:
                    pro_update = self._update_specific_product_template(
                        cr, uid, template_map_id, url, token, context)
                    if pro_update:
                        if pro_update[0] > 0:
                            success_up_ids.append(pro_update[1])
                        else:
                            up_error_ids.append(pro_update[1])
            if success_exp_ids:
                text = "\nThe Listed product(s) %s successfully created on Magento." % (
                    success_exp_ids)
            if error_ids:
                text += '\nThe Listed Product(s) %s does not synchronized on magento.' % error_ids
            if text:
                mage_sync_history.create(cr, uid, {
                                         'status': 'yes', 'action_on': 'product', 'action': 'b', 'error_message': text}, context)
            if success_up_ids:
                text1 = '\nThe Listed Product(s) %s has been successfully updated to Magento. \n' % success_up_ids
                mage_sync_history.create(cr, uid, {
                                         'status': 'yes', 'action_on': 'product', 'action': 'c', 'error_message': text1}, context)
            if up_error_ids:
                text2 = '\nThe Listed Product(s) %s does not updated on magento.' % up_error_ids
                mage_sync_history.create(cr, uid, {
                                         'status': 'no', 'action_on': 'product', 'action': 'c', 'error_message': text2}, context)
            partial = self.pool.get('message.wizard').create(
                cr, uid, {'text': text + text1 + text2})
            return {'name': _("Information"),
                    'view_mode': 'form',
                    'view_id': False,
                    'view_type': 'form',
                    'res_model': 'message.wizard',
                    'res_id': partial,
                    'type': 'ir.actions.act_window',
                    'nodestroy': True,
                    'target': 'new',
                    'domain': '[]',
                    }

    #############################################
    ##          Specific template sync         ##
    #############################################

    def _export_specific_template(self, cr, uid, template_id, url, token, context=None):
        context = dict(context or {})
        if template_id:
            mage_set_id = 0
            get_product_data = {}
            mage_price_changes = {}
            mage_attribute_ids = []
            map_tmpl_pool = self.pool.get('magento.product.template')
            val_price_pool = self.pool.get('product.attribute.price')
            product_tmp_pool = self.pool.get('product.template')
            obj_pro = product_tmp_pool.browse(
                cr, uid, template_id, context=context)
            template_sku = obj_pro.default_code or 'Template Ref %s' % template_id
            if not obj_pro.product_variant_ids:
                return [-2, str(template_id) + ' No Variant Ids Found!!!']
            else:
                if not obj_pro.attribute_set_id.id:
                    self.assign_attribute_Set(
                        cr, uid, [template_id], context=context)
                set_id = obj_pro.attribute_set_id.id
                set_id = self._check_valid_attribute_set(
                    cr, uid, set_id, template_id, context=context)
                wk_attribute_line_ids = obj_pro.attribute_line_ids

                if not wk_attribute_line_ids:
                    template_sku = 'single_variant'
                    mage_ids = self._sync_template_variants(
                        cr, uid, obj_pro, template_sku, url, token, context=context)
                    price = obj_pro.list_price or 0.0
                    if mage_ids:
                        erp_map_data = {
                            'template_name': template_id,
                            'erp_template_id': template_id,
                            'mage_product_id': mage_ids[0],
                            'base_price': price,
                            'is_variants': False,
                            'instance_id': context.get('instance_id')
                        }
                        try:
                            check = map_tmpl_pool.create(cr, uid, erp_map_data)
                        except Exception, e:
                            _logger.info(
                                "----------3-------------Exception----------Template Attribute: %r", e)
                        return mage_ids
                else:
                    check_attribute = self._check_attribute_with_set(
                        cr, uid, set_id, wk_attribute_line_ids)
                    if check_attribute and check_attribute[0] == -1:
                        return check_attribute
                    mage_set_id = obj_pro.attribute_set_id.set_id
                    get_product_data['attribute_set_id'] = mage_set_id

                    if not mage_set_id:
                        return [-3, str(template_id) + ' Attribute Set Name not found!!!']
                    else:
                        for type_obj in wk_attribute_line_ids:
                            mage_attr_ids = self._check_attribute_sync(
                                cr, uid, type_obj)
                            if not mage_attr_ids:
                                return [-1, str(template_id) + ' Attribute not syned at magento!!!']
                            mage_attribute_ids.append(mage_attr_ids[0])
                            attrname = type_obj.attribute_id.name.lower().replace(" ", "_").replace("-", "_")
                            val_dic = self._search_single_values(
                                cr, uid, template_id, type_obj.attribute_id.id, context=context)
                            if val_dic:
                                context.update(val_dic)
                            for value_id in type_obj.value_ids:
                                price_extra = 0.0
                                ##### product template and value extra price ##
                                price_search = val_price_pool.search(
                                    cr, uid, [('product_tmpl_id', '=', template_id), ('value_id', '=', value_id.id)])
                                if price_search:
                                    price_extra = val_price_pool.browse(
                                        cr, uid, price_search[0]).price_extra
                                valuename = value_id.name
                                if mage_price_changes.has_key(attrname):
                                    mage_price_changes[attrname].update(
                                        {valuename: price_extra})
                                else:
                                    mage_price_changes[attrname] = {
                                        valuename: price_extra}
                        mage_ids = self._sync_template_variants(
                            cr, uid, obj_pro, template_sku, url, token, context=context)
                        if mage_ids:
                            get_product_data['custom_attributes'] = []
                            get_product_data['visibility'] = 4
                            get_product_data[
                                'price'] = obj_pro.list_price or 0.00
                            get_product_data = self._get_product_array(
                                cr, uid, url, token, obj_pro, get_product_data, context=context)
                            get_product_data['custom_attributes'].append(
                                {"attribute_code": "tax_class_id", "value": "0"})
                            template_sku = 'Template sku %s' % template_id
                            get_product_data['sku'] = template_sku
                            get_product_data['type_id'] = 'configurable'
                            options_data = self._create_product_attribute_option(
                                cr, uid, wk_attribute_line_ids, context)
                            get_product_data['extension_attributes'] = {
                                'configurable_product_links': mage_ids,
                                'configurable_product_options': options_data,
                                'stock_item': { "is_in_stock": True}
                            }
                            product_data = {"product": get_product_data}
                            cc = json.dumps(product_data)
                            product_url = url + "/index.php/rest/V1/products"
                            token = token.replace('"', "")
                            userAgent = request.httprequest.environ.get('HTTP_USER_AGENT', '')
                            headers = {'Authorization': token,
                                       'Content-Type': 'application/json', 'User-Agent': userAgent}
                            mage_id = requests.post(
                                product_url, data=cc, headers=headers, verify=False)
                            product_data = json.loads(mage_id.text)
                            product_tmp_pool.write(cr, uid, template_id, {
                                                   'prod_type': 'configurable'}, context=context)
                            if mage_id.ok and product_data.get('id', 0) > 0:
                                mage_product_id = product_data['id']
                                map_tmpl_pool.create(cr, uid, {'template_name': template_id, 'erp_template_id': template_id, 'mage_product_id': mage_product_id, 'base_price': get_product_data[
                                                     'price'], 'is_variants': True, 'instance_id': context.get('instance_id')})
                                map_product_data = {'template': {
                                    'magento_id': mage_product_id, 
                                    'odoo_id': template_id, 
                                    'created_by': 'Odoo'
                                }}
                                cc = json.dumps(map_product_data)
                                map_product_url = url + "/index.php/rest/V1/odoomagentoconnect/template"
                                mage_id = requests.post(
                                    map_product_url, data=cc, headers=headers, verify=False)
                                return [1, mage_product_id]
                            else:
                                return [0, str(template_id) + "Not Created at magento, Reason >> " + product_data.get('message', '')]
                        else:
                            return [0, str(template_id) + "Not Created at magento"]
        else:
            return False

    def _create_product_attribute_option(self, cr, uid, wk_attribute_line_ids, context=None):
        options_data = []
        context = dict(context or {})
        for type_obj in wk_attribute_line_ids:
            get_product_option_data = {}
            mage_attr_ids = self._check_attribute_sync(
                cr, uid, type_obj, context)
            get_product_option_data['attribute_id'] = mage_attr_ids[0]
            get_product_option_data['label'] = type_obj.attribute_id.name
            get_product_option_data['position'] = 0
            get_product_option_data['isUseDefault'] = True
            get_product_option_data['values'] = []
            for value_id in type_obj.value_ids.ids:
                type_search = self.pool['magento.product.attribute.value'].search(
                    cr, uid, [('name', '=', value_id)])
                if type_search:
                    mage_id = self.pool['magento.product.attribute.value'].browse(
                        cr, uid, type_search[0]).mage_id
                    get_product_option_data['values'].append(
                        {"value_index": mage_id})
            options_data.append(get_product_option_data)
        return options_data

    def _check_valid_attribute_set(self, cr, uid, set_id, template_id, context=None):
        context = dict(context or {})
        if context.has_key('instance_id'):
            instance_id = self.pool.get('magento.attribute.set').browse(
                cr, uid, set_id).instance_id.id
            if instance_id == context['instance_id']:
                return set_id
            else:
                return False
        else:
            return False

    ############# sync template variants ########
    def _sync_template_variants(self, cr, uid, temp_obj, template_sku, url, token, context=None):
        context = dict(context or {})
        mage_variant_ids = []
        map_prod_pool = self.pool.get('magento.product')
        for obj in temp_obj.product_variant_ids:
            vid = obj.id
            search_ids = map_prod_pool.search(cr, uid, [(
                'oe_product_id', '=', vid), ('instance_id', '=', context.get('instance_id'))])

            if search_ids:
                map_obj = map_prod_pool.browse(cr, uid, search_ids[0])
                mage_variant_ids.append(map_obj.mag_product_id)
            else:
                mage_ids = self._export_specific_product(
                    cr, uid, vid, template_sku, url, token, context=context)
                if mage_ids and mage_ids.has_key('id'):
                    if mage_ids and mage_ids['id'] > 0:
                        mage_variant_ids.append(mage_ids['id'])
        return mage_variant_ids

    ############# check single attribute lines ########
    def _search_single_values(self, cr, uid, temp_id, attr_id, context=None):
        context = dict(context or {})
        dic = {}
        attr_line_pool = self.pool.get('product.attribute.line')
        search_ids = attr_line_pool.search(cr, uid, [(
            'product_tmpl_id', '=', temp_id), ('attribute_id', '=', attr_id)], context=context)
        if search_ids and len(search_ids) == 1:
            att_line_obj = attr_line_pool.browse(
                cr, uid, search_ids[0], context=context)
            if len(att_line_obj.value_ids) == 1:
                dic[att_line_obj.attribute_id.name] = att_line_obj.value_ids.name
        return dic

    ############# check attributes lines and set attributes are same ########
    def _check_attribute_with_set(self, cr, uid, set_id, attribute_line_ids, context=None):
        context = dict(context or {})
        set_attr_ids = self.pool.get('magento.attribute.set').browse(
            cr, uid, set_id).attribute_ids
        if not set_attr_ids:
            return [-1, str(id) + ' Attribute Set Name has no attributes!!!']
        set_attr_list = list(set_attr_ids.ids)
        for attr_id in attribute_line_ids:
            if attr_id.attribute_id.id not in set_attr_list:
                return [-1, str(id) + ' Attribute Set Name not matched with attributes!!!']
        return [1]

    ############# check attributes syned return mage attribute ids ########
    def _check_attribute_sync(self, cr, uid, type_obj, context=None):
        context = dict(context or {})
        map_attr_pool = self.pool.get('magento.product.attribute')
        mage_attribute_ids = []
        type_search = map_attr_pool.search(
            cr, uid, [('name', '=', type_obj.attribute_id.id)])
        if type_search:
            map_type_obj = map_attr_pool.browse(cr, uid, type_search[0])
            mage_attr_id = map_type_obj.mage_id
            mage_attribute_ids.append(mage_attr_id)
        return mage_attribute_ids

    ############# fetch product details ########
    def _get_product_array(self, cr, uid, url, token, obj_pro, get_product_data, context=None):
        context = dict(context or {})
        prod_catg = []
        for i in obj_pro.categ_ids:
            mage_categ_id = self.sync_categories(
                cr, uid, url, token, i.id, context=context)
            prod_catg.append(mage_categ_id)
        if obj_pro.categ_id.id:
            mage_categ_id = self.sync_categories(
                cr, uid, url, token, obj_pro.categ_id.id, context=context)
            prod_catg.append(mage_categ_id)
        status = 2
        if obj_pro.sale_ok:
            status = 1
        get_product_data['name'] = obj_pro.name
        if obj_pro.image:
            image_stream = StringIO.StringIO(obj_pro.image.decode('base64'))
            image = Image.open(image_stream)
            imageType = image.format.lower()
            if not imageType:
                imageType = 'jpeg'
            magentoImageType = "image/" + imageType
            image_file = {
                'position': 1,
                'media_type': 'image',
                'disabled': False,
                'label': '',
                'types': ["image", "small_image", "thumbnail", "swatch_image"],
                'content': {'base64_encoded_data': obj_pro.image, 'type': magentoImageType, 'name': 'OdooProductImage'}}

            get_product_data['media_gallery_entries'] = [image_file]
        get_product_data['custom_attributes'].append(
            {"attribute_code": "description", "value": obj_pro.description})
        get_product_data['custom_attributes'].append(
            {"attribute_code": "short_description", "value": obj_pro.description_sale})
        get_product_data['weight'] = obj_pro.weight or 0.00
        get_product_data['custom_attributes'].append(
            {"attribute_code": "category_ids", "value": prod_catg})
        get_product_data['status'] = status

        # if not get_product_data.has_key('websites'):
        #   get_product_data['websites'] = [1]
        return get_product_data

    #############################################
    ##          Specific product sync          ##
    #############################################
    def _export_specific_product(self, cr, uid, id, template_sku, url, token, context=None):
        """
        @param code: product Id.
        @param context: A standard dictionary
        @return: list
        """
        get_product_data = {}
        context = dict(context or {})
        map_variant = []
        stock = False
        quantity = 0
        price_extra = 0
        if id:
            obj_pro = self.pool.get('product.product').browse(
                cr, uid, id, context)
            sku = obj_pro.default_code or 'Ref %s' % id
            get_product_data['attribute_set_id'] = ""
            get_product_data['custom_attributes'] = []
            if obj_pro.attribute_value_ids:
                for value_id in obj_pro.attribute_value_ids:
                    attrname = str(value_id.attribute_id.name.lower(
                    ).replace(" ", "_").replace("-", "_"))
                    valuename = value_id.name
                    type_search = self.pool['magento.product.attribute.value'].search(
                        cr, uid, [('name', '=', value_id.id)])
                    if type_search:
                        typeObj = self.pool['magento.product.attribute.value'].browse(
                            cr, uid, type_search[0])
                        get_product_data['custom_attributes'].append(
                            {"attribute_code": attrname, "value": typeObj.mage_id})
                    search_price_id = self.pool.get('product.attribute.price').search(cr, uid, [(
                        'product_tmpl_id', '=', obj_pro.product_tmpl_id.id), ('value_id', '=', value_id.id)])
                    if search_price_id:
                        price_extra += self.pool.get('product.attribute.price').browse(
                            cr, uid, search_price_id[0]).price_extra

            get_product_data[
                'attribute_set_id'] = obj_pro.product_tmpl_id.attribute_set_id.set_id
            if template_sku == "single_variant":
                get_product_data['visibility'] = 4
            else:
                get_product_data['visibility'] = 1

            get_product_data['price'] = obj_pro.list_price + \
                price_extra or 0.00
            get_product_data = self._get_product_array(
                cr, uid, url, token, obj_pro, get_product_data, context=context)
            if obj_pro.type in ['product', 'consu']:
                prodtype = 'simple'
            else:
                prodtype = 'virtual'
            get_product_data['type_id'] = prodtype
            self.pool.get('product.product').write(
                cr, uid, id, {'prod_type': prodtype, 'default_code': sku}, context)
            quantity = self.pool['magento.synchronization'].get_product_qty(
                cr, uid, obj_pro.id)
            get_product_data['extension_attributes'] = {"stock_item": {
                "stock_id": 1, "qty": quantity, "is_in_stock": True}}
            pro = self.prodcreate(cr, uid, url, token, id,
                                  prodtype, sku, get_product_data, context)
            return pro

    #############################################
    ##          single products create         ##
    #############################################
    def prodcreate(self, cr, uid, url, token, pro_id, prodtype, prodsku, put_product_data, context=None):
        context = dict(context or {})
        put_product_data['sku'] = prodsku
        product_data = {"product": put_product_data}
        cc = json.dumps(product_data)
        product_url = url + "/index.php/rest/V1/products"
        token = token.replace('"', "")
        userAgent = request.httprequest.environ.get('HTTP_USER_AGENT', '')
        headers = {'Authorization': token, 'Content-Type': 'application/json', 'User-Agent': userAgent}
        mage_id = requests.post(product_url, data=cc, headers=headers, verify=False)
        product_data = json.loads(mage_id.text)
        if product_data.has_key('id'):
            if product_data['id'] > 0:
                erp_map_data = {
                    'pro_name': pro_id,
                    'oe_product_id': pro_id,
                    'mag_product_id': int(product_data['id']),
                    'instance_id': context.get('instance_id'),
                    'magento_stock_id': product_data['extension_attributes']['stock_item']['item_id']
                }
                check = self.pool.get('magento.product').create(
                    cr, uid, erp_map_data)
                map_product_data = {'product': {'magento_id': product_data['id'], 'odoo_id': pro_id, 'created_by': 'Odoo'}}
                cc = json.dumps(map_product_data)
                map_product_url = url + "/index.php/rest/V1/odoomagentoconnect/product"
                token = token.replace('"', "")
                headers = {'Authorization': token,
                           'Content-Type': 'application/json', 'User-Agent': userAgent}
                mage_id = requests.post(
                    map_product_url, data=cc, headers=headers, verify=False)
        if product_data.has_key('message'):
            self.pool.get('magento.sync.history').create(cr, uid, {
                'status': 'no', 'action_on': 'product', 'action': 'b', 'error_message': "Product Export Error for " +str(pro_id)+". Reason -> "+ product_data['message']}, context)
        return product_data

    #############################################
    ##      update specific product template   ##
    #############################################
    def _update_specific_product_template(self, cr, uid, id, url, token, context=None):
        context = dict(context or {})
        get_product_data = {}
        mage_variant_ids = []
        mage_price_changes = {}
        map_tmpl_pool = self.pool.get('magento.product.template')
        temp_obj = map_tmpl_pool.browse(cr, uid, id)
        temp_id = temp_obj.template_name.id
        mage_id = temp_obj.mage_product_id
        if temp_id and mage_id:
            map_prod_pool = self.pool.get('magento.product')
            obj_pro = self.pool.get('product.template').browse(
                cr, uid, temp_id, context)
            get_product_data['price'] = obj_pro.list_price or 0.00
            get_product_data['custom_attributes'] = []
            wk_attribute_line_ids = obj_pro.attribute_line_ids
            get_product_data = self._get_product_array(
                cr, uid, url, token, obj_pro, get_product_data, context)
            if get_product_data.has_key('media_gallery_entries'):
                get_product_data.pop('media_gallery_entries')
            if obj_pro.product_variant_ids:
                if temp_obj.is_variants == True and obj_pro.is_product_variant == False:
                    if wk_attribute_line_ids:
                        for obj in obj_pro.product_variant_ids:
                            mage_update_ids = []
                            vid = obj.id
                            search_ids = map_prod_pool.search(
                                cr, uid, [('oe_product_id', '=', vid)])
                            if search_ids:
                                mage_update_ids = self._update_specific_product(
                                    cr, uid, search_ids[0], url, token, context)
                                mage_id = map_prod_pool.browse(
                                    cr, uid, search_ids[0]).mag_product_id
                                mage_variant_ids.append(mage_id)
                else:
                    for obj in obj_pro.product_variant_ids:
                        price = obj_pro.list_price or 0.0
                        mage_update_ids = []
                        vid = obj.id
                        search_ids = map_prod_pool.search(
                            cr, uid, [('oe_product_id', '=', vid)])
                        if search_ids:
                            mage_update_ids = self._update_specific_product(
                                cr, uid, search_ids[0], url, token, context=context)
                        if mage_update_ids and mage_update_ids[0] > 0:
                            map_tmpl_pool.write(
                                cr, uid, id, {'need_sync': 'No'}, context)
                        return mage_update_ids
            else:
                return [-1, str(id) + ' No Variant Ids Found!!!']
            options_data = self._create_product_attribute_option(
                cr, uid, wk_attribute_line_ids, context)
            get_product_data['extension_attributes'] = {
                'configurable_product_links': mage_variant_ids,
                'configurable_product_options': options_data,
                'stock_item': {"is_in_stock": True}
            }
            product_data = {"product": get_product_data}
            cc = json.dumps(product_data)
            template_sku = 'Template sku %s' % temp_id
            template_sku = urllib.quote(template_sku, safe='')

            temp_url = url + "/index.php/rest/V1/products/" + str(template_sku)
            token = token.replace('"', "")
            userAgent = request.httprequest.environ.get('HTTP_USER_AGENT', '')
            headers = {'Authorization': token,
                       'Content-Type': 'application/json', 'User-Agent': userAgent}
            mage_cat = requests.put(temp_url, data=cc, headers=headers, verify=False)
            if mage_cat.ok :
                map_tmpl_pool.write(cr, uid, id, {'need_sync': 'No'}, context)
            else :
                return [0, mage_cat.reason]
            return [1, temp_id]

    #############################################
    ##          update specific product        ##
    #############################################
    def _update_specific_product(self, cr, uid, id, url, token, context=None):
        context = dict(context or {})
        get_product_data = {}
        pro_obj = self.pool.get('magento.product').browse(cr, uid, id)
        pro_id = pro_obj.pro_name.id
        mage_id = pro_obj.mag_product_id
        stock_item_id = pro_obj.magento_stock_id
        if pro_id and mage_id:
            quantity = 0
            stock = 0
            price_extra = 0
            obj_pro = self.pool.get('product.product').browse(
                cr, uid, pro_id, context)
            get_product_data['custom_attributes'] = []
            if obj_pro.attribute_value_ids:
                for value_id in obj_pro.attribute_value_ids:
                    attrname = str(value_id.attribute_id.name.lower(
                    ).replace(" ", "_").replace("-", "_"))
                    valuename = value_id.name
                    type_search = self.pool['magento.product.attribute.value'].search(
                        cr, uid, [('name', '=', value_id.id)])
                    if type_search:
                        typeObj = self.pool['magento.product.attribute.value'].browse(
                            cr, uid, type_search[0])
                        get_product_data['custom_attributes'].append(
                            {"attribute_code": attrname, "value": typeObj.mage_id})
                    search_price_id = self.pool.get('product.attribute.price').search(cr, uid, [(
                        'product_tmpl_id', '=', obj_pro.product_tmpl_id.id), ('value_id', '=', value_id.id)])
                    if search_price_id:
                        price_extra += self.pool.get('product.attribute.price').browse(
                            cr, uid, search_price_id[0]).price_extra
            get_product_data['price'] = obj_pro.list_price + \
                price_extra or 0.00
            get_product_data = self._get_product_array(
                cr, uid, url, token, obj_pro, get_product_data, context)
            if get_product_data.has_key('media_gallery_entries'):
                get_product_data.pop('media_gallery_entries')
            quantity = self.pool['magento.synchronization'].get_product_qty(
                cr, uid, pro_id)
            if stock_item_id:
                get_product_data['extension_attributes'] = {"stock_item": {
                    "itemId": stock_item_id, "stock_id": 1, "qty": quantity, "is_in_stock": True}}
            product_data = {"product": get_product_data}
            cc = json.dumps(product_data)
            product_sku = pro_obj.pro_name.default_code
            try:
                product_sku = urllib.quote(product_sku, safe='')
            except Exception, e:
                product_sku = urllib.quote(product_sku.encode("utf-8"), safe='')
            product_url = url + \
                "/index.php/rest/V1/products/" + str(product_sku)
            token = token.replace('"', "")
            userAgent = request.httprequest.environ.get('HTTP_USER_AGENT', '')
            headers = {'Authorization': token,
                       'Content-Type': 'application/json', 'User-Agent': userAgent}
            mage_cat = requests.put(product_url, data=cc, headers=headers, verify=False)
            if mage_cat.ok :
                product_data = json.loads(mage_cat.text)
                self.pool.get('magento.product').write(
                    cr, uid, id, {'need_sync': 'No'}, context)
                return [1, pro_id]
            else :
                return [0, pro_id]

    def get_mage_region_id(self, cr, uid, url, token, region, country_code, context=None):
        """ 
        @return magneto region id 
        """
        context = dict(context or {})
        region_obj = self.pool.get('magento.region')
        map_id = region_obj.search(
            cr, uid, [('country_code', '=', country_code)])
        if not map_id:
            return_id = self.pool.get('region.wizard')._sync_mage_region(
                cr, uid, url, token, country_code)
        region_ids = region_obj.search(
            cr, uid, [('name', '=', region), ('country_code', '=', country_code)])
        if region_ids:
            id = region_obj.browse(cr, uid, region_ids[0]).mag_region_id
            return id
        else:
            return 0

    def get_product_qty(self, cr, uid, productId, mobStockAction='', context=None):
        context = dict(context or {})
        if not mobStockAction :
            mobStockAction = self.pool.get('ir.values').get_default(
                cr, SUPERUSER_ID, 'mob.config.settings', 'mob_stock_action')
        productObj = self.pool['product.product'].browse(
            cr, uid, productId, context)
        if mobStockAction == "qoh":
            productQty = productObj.qty_available - productObj.outgoing_qty
        else:
            productQty = productObj.virtual_available
        return productQty

magento_synchronization()
# END

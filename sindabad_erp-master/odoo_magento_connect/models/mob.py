# -*- coding: utf-8 -*-
##########################################################################
#
#    Copyright (c) 2017-Present Webkul Software Pvt. Ltd. (<https://webkul.com/>)
#
##########################################################################


import re
import xmlrpclib
import json
import requests
import logging
from openerp.osv import fields, osv
from openerp.tools.translate import _
from openerp import SUPERUSER_ID, api
from openerp.addons.base.res.res_partner import _lang_get
from openerp.exceptions import Warning
from openerp.http import request

XMLRPC_API = '/integration/admin/token'
_logger = logging.getLogger(__name__)

def _unescape(text):
    ##
    # Replaces all encoded characters by urlib with plain utf8 string.
    #
    # @param text source text.
    # @return The plain text.
    from urllib import unquote_plus
    try:
        text = unquote_plus(text.encode('utf8'))
        return text
    except Exception, e:
        return text


class magento_website(osv.osv):
    _name = "magento.website"
    _description = "Magento Website"
    _columns = {
        'name': fields.char('Website Name', size=64, required=True),
        'website_id': fields.integer('Magento Webiste Id', readonly=True),
        'instance_id': fields.many2one('magento.configure', 'Magento Instance'),
        'code': fields.char('Code', size=64, required=True),
        'sort_order': fields.char('Sort Order', size=64),
        'is_default': fields.boolean('Is Default', readonly=True),
        'default_group_id': fields.integer('Default Store', readonly=True),
        'create_date': fields.datetime('Created Date', readonly=True),
    }

    def _get_website(self, cr, uid, url, token, context=None):
        website_id = 0
        instance_id = context.get('instance_id')
        url = url + "/index.php/rest/V1/store/websites"
        userAgent = request.httprequest.environ.get('HTTP_USER_AGENT', '')
        headers = {'Authorization': token, 'Content-Type': 'application/json', 'User-Agent': userAgent}
        magento_websites = requests.get(url, headers=headers, verify=False).text
        magento_websites = json.loads(magento_websites)
        for magento_website in magento_websites:
            websites = self.search(cr, uid, [('website_id', '=', magento_website[
                                   'id']), ('instance_id', '=', instance_id)])
            if websites:
                website_id = websites[0]
            else:
                website_dict = {
                    'name': magento_website['name'],
                    'code': magento_website['code'],
                    'instance_id': instance_id,
                    'website_id': magento_website['id'],
                    'default_group_id': magento_website['default_group_id']
                }
                website_id = self.create(cr, uid, website_dict)
        return website_id

magento_website()


class magento_store(osv.osv):
    _name = "magento.store"
    _description = "Magento Store"
    _columns = {
        'name': fields.char('Store Name', size=64, required=True),
        'group_id': fields.integer('Magento Store Id', readonly=True),
        'instance_id': fields.many2one('magento.configure', 'Magento Instance'),
        'root_category_id': fields.integer('Root Category Id', readonly=True),
        'default_store_id': fields.integer('Default Store Id'),
        'website_id': fields.many2one('magento.website', 'Website Id'),
        'create_date': fields.datetime('Created Date', readonly=True),
    }

    def _get_store_group(self, cr, uid, url, token, context=None):
        group_id = 0
        instance_id = context.get('instance_id')
        store_url = url + "/index.php/rest/V1/store/storeGroups"
        userAgent = request.httprequest.environ.get('HTTP_USER_AGENT', '')
        headers = {'Authorization': token, 'Content-Type': 'application/json', 'User-Agent': userAgent}
        magento_stores = requests.get(store_url, headers=headers, verify=False).text
        website_id = self.pool['magento.website']._get_website(
            cr, uid, url, token, context)
        magento_stores = json.loads(magento_stores)

        for magento_store in magento_stores:
            groups = self.search(cr, uid, [('group_id', '=', magento_store[
                                 'id']), ('instance_id', '=', instance_id)])
            if groups:
                group_id = groups[0]
            else:
                website_ids = self.pool['magento.website'].search(cr, uid, [(
                    'website_id', '=', magento_store['website_id']), ('instance_id', '=', instance_id)])
                if website_ids:
                    website_id = website_ids[0]
                group_dict = {
                    'name': magento_store['name'],
                    'website_id': website_id,
                    'group_id': magento_store['id'],
                    'instance_id': instance_id,
                    'root_category_id': magento_store['root_category_id'],
                    'default_store_id': magento_store['default_store_id'],
                }
                group_id = self.create(cr, uid, group_dict)
        return group_id

magento_store()


class magento_store_view(osv.osv):
    _name = "magento.store.view"
    _description = "Magento Store View"
    _columns = {
        'name': fields.char('Store View Name', size=64, required=True),
        'code': fields.char('Code', size=64, required=True),
        'view_id': fields.integer('Magento Store View Id', readonly=True),
        'instance_id': fields.many2one('magento.configure', 'Magento Instance'),
        'group_id': fields.many2one('magento.store', 'Store Id'),
        'is_active': fields.boolean('Active'),
        'sort_order': fields.integer('Sort Order'),
        'create_date': fields.datetime('Created Date', readonly=True),
    }

    def name_get(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        if isinstance(ids, (int, long)):
            ids = [ids]
        res = []
        for record in self.browse(cr, uid, ids, context=context):
            name = record.name
            if record.group_id:
                name = name + "\n(%s)" % (record.group_id.name) + \
                    "\n(%s)" % (record.group_id.website_id.name)
            res.append((record.id, name))
        return res

    def _get_store_view(self, cr, uid, url, token, context=None):
        if context is None:
            context = {}
        group_id = 0
        instance_id = context.get('instance_id')
        store_view_url = url + "/index.php/rest/V1/store/storeViews"
        userAgent = request.httprequest.environ.get('HTTP_USER_AGENT', '')
        headers = {'Authorization': token, 'Content-Type': 'application/json', 'User-Agent': userAgent}
        store_views = requests.get(store_view_url, headers=headers, verify=False).text
        group_id = self.pool['magento.store']._get_store_group(
            cr, uid, url, token, context)
        store_views = json.loads(store_views)
        for store_view in store_views:
            views = self.search(cr, uid, [('view_id', '=', store_view[
                                'id']), ('instance_id', '=', instance_id)])
            if views:
                view_id = views[0]
            else:
                views = self.pool['magento.store'].search(cr, uid, [('group_id', '=', store_view[
                                                          'store_group_id']), ('instance_id', '=', instance_id)])
                if views:
                    group_id = views[0]
                view_dict = {
                    'name': store_view['name'],
                    'code': store_view['code'],
                    'view_id': store_view['id'],
                    'group_id': group_id,
                    'instance_id': instance_id,
                }
                view_id = self.create(cr, uid, view_dict)
        return view_id

magento_store_view()


class magento_configure(osv.osv):
    _name = "magento.configure"
    _inherit = ['mail.thread']
    _description = "Magento Configuration"
    _rec_name = 'instance_name'

    def _default_category(self, cr, uid, context=None):
        if context is None:
            context = {}
        if 'categ_id' in context and context['categ_id']:
            return context['categ_id']
        md = self.pool.get('ir.model.data')
        res = False
        try:
            res = md.get_object_reference(
                cr, uid, 'product', 'product_category_all')[1]
        except ValueError:
            res = False
        return res

    def _default_instance_name(self, cr, uid, context=None):
        if context is None:
            context = {}
        res = self.pool.get('ir.sequence').next_by_code(cr, uid, 'magento.configure')
        return res

    def _fetch_magento_store(self, cr, uid, url, token, context=None):
        if context is None:
            context = {}
        store_info = {}
        store_obj = self.pool['magento.store.view']._get_store_view(
            cr, uid, url, token, context)
        store_info['store_id'] = store_obj
        return store_info

    _columns = {
        'name': fields.char('Base URL', required=True, size=255, select=True),
        'instance_name': fields.char("Instance Name", size=64, select=True),
        'user': fields.char('User Name', required=True, size=100),
        'pwd': fields.char('Password', required=True, size=100),
        'token': fields.char('Token', size=100),
        'status': fields.char('Connection Status', readonly=True, size=255),
        'active': fields.boolean('Active'),
        'store_id': fields.many2one('magento.store.view', 'Default Magento Store'),
        'group_id': fields.related('store_id', 'group_id', type="many2one", relation="magento.store", string="Default Store", readonly=True, store=True),
        'website_id': fields.related('group_id', 'website_id', type="many2one", relation="magento.website", string="Default Magento Website", readonly=True),
        'credential': fields.boolean('Show/Hide Credentials Tab',
                                     help="If Enable, Credentials tab will be displayed, "
                                     "And after filling the details you can hide the Tab."),
        'auto_invoice': fields.boolean('Auto Invoice',
                                       help="If Enabled, Order will automatically Invoiced on Magento "
                                       " when Odoo order Get invoiced."),
        'auto_ship': fields.boolean('Auto Shipment',
                                    help="If Enabled, Order will automatically shipped on Magento"
                                    " when Odoo order Get Delivered."),
        'notify': fields.boolean('Notify Customer By Email',
                                 help="If True, customer will be notify"
                                 "during order shipment and invoice, else it won't."),
        'language': fields.selection(_lang_get, "Default Language", help="Selected language is loaded in the system, "
                                     "all documents related to this contact will be synched in this language."),
        'category': fields.many2one('product.category', "Default Category", help="Selected Category will be set default category for odoo's product, "
                                    "in case when magento product doesn\'t belongs to any catgeory."),
        'state': fields.selection([('enable', 'Enable'), ('disable', 'Disable')], 'Status', help="status will be consider during order invoice, "
                                  "order delivery and order cancel, to stop asynchronous process at other end.", size=100),
        'inventory_sync': fields.selection([	('enable', 'Enable'),
                                             ('disable', 'Disable')],
                                           'Inventory Update',
                                           help="If Enable, Invetory will Forcely Update During Product Update Operation.", size=100),
        'warehouse_id': fields.many2one('stock.warehouse', 'Warehouse',
                                        help="Used During Inventory Synchronization From Magento to Odoo."),
        'location_id': fields.related('warehouse_id', 'lot_stock_id', type='many2one', relation='stock.location', string='Location ID'),

        'create_date': fields.datetime('Created Date'),
        'correct_mapping': fields.boolean('Correct Mapping'),
    }
    _defaults = {
        'correct_mapping': True,
        'instance_name': _default_instance_name,
        'active': lambda *a: 1,
        'auto_ship': lambda *a: 1,
        'auto_invoice': lambda *a: 1,
        'credential': lambda *a: 1,
        'language': api.model(lambda self: self.env.lang),
        'category': _default_category,
        'state': 'enable',
        'inventory_sync': 'enable',
        'notify': lambda *a: 1,
        'warehouse_id': lambda self, cr, uid, c: self.pool.get('sale.order')._get_default_warehouse(cr, uid, context=c),
    }

    def create(self, cr, uid, vals, context=None):
        active_ids = self.pool.get('magento.configure').search(
            cr, uid, [('active', '=', True)])
        if vals['active']:
            if active_ids:
                raise osv.except_osv(_('Warning'), _(
                    "Sorry, Only one active connection is allowed."))
        return super(magento_configure, self).create(cr, uid, vals, context=context)

    def write(self, cr, uid, ids, vals, context=None):
        active_ids = self.pool.get('magento.configure').search(
            cr, uid, [('active', '=', True)])
        if len(active_ids) > 0 and vals.has_key('active') and vals['active']:
            raise Warning(
                _('Warning!\nSorry, Only one active connection is allowed.'))
        instance_obj = self.browse(cr, uid, ids)
        if vals:
            for instance_value in instance_obj:
                if (vals.has_key('name') and vals['name'] != instance_value.name) or (vals.has_key('user') and vals['user'] != instance_value.user) or (vals.has_key('pwd') and vals['pwd'] != instance_value.pwd):
                    token = self.create_magento_connection(
                        cr, uid, instance_value, vals, context)
                    if token:
                        if token[0]:
                            vals['token'] = str(token[0])
                            vals[
                                'status'] = "Congratulation, It's Successfully Connected with Magento Api."
                        else:
                            vals['token'] = False
                            vals['status'] = str(token[1])
                if instance_value.instance_name == None or instance_value.instance_name == False:
                    vals['instance_name'] = self.pool.get(
                        'ir.sequence').next_by_code(cr, uid, 'magento.configure')
        return super(magento_configure, self).write(cr, uid, ids, vals, context=context)

    def fetch_connection_info(self, cr, uid, vals):
        """
                Called by Xmlrpc from Magento
        """
        if vals.has_key('magento_url'):
            active_connection_id = self.search(
                cr, uid, [('active', '=', True)])
            for odoo_id in active_connection_id:
                return self.read(cr, uid, odoo_id, ['language', 'category', 'warehouse_id'])
        return False

    def correct_instance_mapping(self, cr, uid, ids, context=None):
        self.mapped_status(cr, uid, ids, "magento.product", context)
        self.mapped_status(cr, uid, ids, "magento.product.template", context)
        self.mapped_status(cr, uid, ids, "magento.orders", context)
        self.mapped_status(cr, uid, ids, "magento.customers", context)
        self.mapped_status(
            cr, uid, ids, "magento.product.attribute.value", context)
        self.mapped_status(cr, uid, ids, "magento.product.attribute", context)
        self.mapped_status(cr, uid, ids, "magento.category", context)
        self.mapped_status(cr, uid, ids, "magento.website", context)
        self.mapped_status(cr, uid, ids, "magento.store", context)
        self.mapped_status(cr, uid, ids, "magento.store.view", context)
        self.mapped_status(cr, uid, ids, "magento.attribute.set", context)
        return True

    def mapped_status(self, cr, uid, ids, model, context=None):
        if isinstance(ids, (int, long)):
            ids = [ids]
        rest_ids = self.pool.get(model).search(
            cr, uid, [('instance_id', '=', False)])
        if ids and rest_ids:
            self.pool.get(model).write(
                cr, uid, rest_ids, {'instance_id': ids[0]})
        return True

    def set_default_magento_website(self, cr, uid, ids, url, token, context=None):
        if context is None:
            context = {}
        for obj in self.browse(cr, uid, ids):
            store_id = obj.store_id
            context = dict(context or {})
            context['instance_id'] = obj.id
            if not store_id:
                store_info = self._fetch_magento_store(
                    cr, uid, url, token, context)
                if not store_info:
                    raise Warning(
                        _('Error!\nMagento Default Website Not Found!!!'))
        return True

    #############################################
    ##    		magento connection		   	   ##
    #############################################
    def test_connection(self, cr, uid, ids, context=None):
        token = 0
        status = 'Magento Connection Un-successful'
        text = 'Test connection Un-successful please check the magento credentials!!! '
        obj = self.browse(cr, uid, ids[0])
        check_mapping = obj.correct_mapping
        token = self.create_magento_connection(cr, uid, obj, {}, context)
        if token:
            if token[0]:
                self.write(cr, uid, ids[0], {'token': str(token[0])})
                store_id = self.set_default_magento_website(
                    cr, uid, ids, obj.name, str(token[0]), context)
                text = str(token[1])
                status = "Congratulation, It's Successfully Connected with Magento Api."
            else:
                status = str(_unescape(token[1]))
                text += status
        self.write(cr, uid, ids[0], {'status': status})
        res_model = 'message.wizard'
        partial = self.pool['message.wizard'].create(cr, uid, {'text': text})
        if not obj.store_id:
            partial = self.pool['magento.wizard'].create(
                cr, uid, {'magento_store_view': obj.store_id.id})
            res_model = 'magento.wizard'
        if check_mapping:
            self.correct_instance_mapping(cr, uid, ids, context)
        ctx = dict(context or {})
        ctx['text'] = text
        return {'name': ("Odoo Magento Bridge"),
                'view_mode': 'form',
                'view_type': 'form',
                'res_model': res_model,
                'view_id': False,
                'res_id': partial,
                'type': 'ir.actions.act_window',
                'nodestroy': True,
                'context': ctx,
                'target': 'new',
                }

    def _create_connection(self, cr, uid, context=None):
        """ create a connection between Odoo and magento 
                returns: False or list"""
        token = 0
        instance_obj = 0
        if context.has_key('instance_id'):
            instance_obj = self.browse(cr, uid, context.get('instance_id'))
        else:
            config_id = self.search(cr, uid, [('active', '=', True)])
            if len(config_id) > 1:
                raise osv.except_osv(_('Error'), _(
                    "Sorry, only one Active Configuration setting is allowed."))
            if not config_id:
                raise osv.except_osv(_('Error'), _(
                    "Please create the configuration part for Magento connection!!!"))
            else:
                instance_obj = self.browse(cr, uid, config_id[0])

        token_generation = self.create_magento_connection(
            cr, uid, instance_obj, {}, context)
        if token_generation:
            self.write(cr, uid, instance_obj.id, {
                        'token': str(token_generation[0])})
            token = token_generation[0]
        if token:
            return [instance_obj.name, token, instance_obj.id]
        else:
            _logger.info("MOB Connection Error: %r",token_generation)
            return False

    def create_magento_connection(self, cr, uid, obj, vals={}, context=None):
        text, token = '', ''
        url = obj.name + "/index.php/rest/V1" + XMLRPC_API
        user = obj.user
        pwd = obj.pwd
        if vals:
            if vals.has_key('name'):
                url = vals['name'] + "/index.php/rest/V1" + XMLRPC_API
            if vals.has_key('user'):
                user = vals['user']
            if vals.has_key('pwd'):
                pwd = vals['pwd']
        Cre = {
            "username": user,
            "password": pwd
        }


        Cred = json.dumps(Cre)
        try:
            userAgent = request.httprequest.environ.get('HTTP_USER_AGENT', '')
        except:
            userAgent=''
        headers = {'Content-Type': 'application/json', 'User-Agent': userAgent}
        if obj.language:
            context = dict(context or {})
            context['lang'] = obj.language
        try:
            respConnection = requests.post(url, data=Cred, headers=headers, verify=False)
            response = json.loads(respConnection.text)
            if respConnection.ok :
                token = "Bearer " + response
                text = 'Test Connection with magento is successful, now you can proceed with synchronization.'
            else :
                text = ('Magento Connection Error: %s') % response.get('message')
        except xmlrpclib.Fault, e:
            text = ('Error, %s!\nInvalid Login Credentials!!!') % e.faultString
        except IOError, e:
            text = ('Error!\n %s') % e
        except Exception, e:
            text = ('Error!\nMagento Connection Error in connecting: %s') % e
        return [token, text]

magento_configure()

################### Catalog Mapping Models ########################


class magento_product_template(osv.osv):
    _name = "magento.product.template"
    _order = 'id desc'
    _description = "Magento Product Template"

    def create(self, cr, uid, vals, context=None):
        if context is None:
            context = {}
        vals['erp_template_id'] = vals['template_name']
        if not vals.has_key('base_price'):
            vals['base_price'] = self.pool.get('product.template').browse(
                cr, uid, vals['erp_template_id']).list_price
        return super(magento_product_template, self).create(cr, uid, vals, context=context)

    _columns = {
        'template_name': fields.many2one('product.template', 'Template Name'),
        'erp_template_id': fields.integer('Odoo`s Template Id'),
        'mage_product_id': fields.integer('Magento`s Product Id'),
        'instance_id': fields.many2one('magento.configure', 'Magento Instance'),
        'base_price': fields.float('Base Price(excl. impact)'),
        'is_variants': fields.boolean('Is Variants'),
        'created_by': fields.char('Created By', size=64),
        'create_date': fields.datetime('Created Date'),
        'write_date': fields.datetime('Updated Date'),
        'need_sync': fields.selection([('Yes', 'Yes'), ('No', 'No')], 'Update Required'),
    }
    _defaults = {
        'created_by': 'odoo',
        'need_sync': 'No',
    }

    def create_n_update_attribute_line(self, cr, uid, data, context=None):
        line_dict = {}
        if context is None:
            context = {}
        price_pool = self.pool.get('product.attribute.price')
        attribute_line = self.pool.get('product.attribute.line')
        if data.has_key('product_tmpl_id'):
            template_id = data.get('product_tmpl_id')
            attribute_id = data.get('attribute_id')
            if data.has_key('values') and data['values']:
                value_ids = []
                for value in data['values']:
                    value_id = value['value_id']
                    value_ids.append(value_id)
                    if value['price_extra']:
                        price_extra = value['price_extra']
                        search_ids = price_pool.search(
                            cr, uid, [('product_tmpl_id', '=', template_id), ('value_id', '=', value_id)])
                        if search_ids:
                            price_pool.write(cr, uid, search_ids[0], {
                                             'price_extra': price_extra})
                        else:
                            a = price_pool.create(cr, uid, {
                                                  'product_tmpl_id': template_id, 'value_id': value_id, 'price_extra': price_extra})
                line_dict['value_ids'] = [(6, 0, value_ids)]
            search = attribute_line.search(cr, uid, [(
                'product_tmpl_id', '=', template_id), ('attribute_id', '=', attribute_id)])
            if search:
                attribute_line.write(cr, uid, search[0], line_dict, context)
            else:
                line_dict['attribute_id'] = attribute_id
                line_dict['product_tmpl_id'] = template_id
                attr_line_id = attribute_line.create(
                    cr, uid, line_dict, context)
            return True
        return False

    def create_template_mapping(self, cr, uid, data, context=None):
        if data.has_key('erp_product_id'):
            product_id = data.get('erp_product_id')
            magento_id = data.get('mage_product_id')
            instance_id = data.get('instance_id')
            product_obj = self.pool.get('product.product').browse(cr, uid, product_id, context)
            template_id = product_obj.product_tmpl_id
            template_data = {
                'template_name': template_id.id,
                'erp_template_id': template_id.id,
                'mage_product_id': magento_id,
                'instance_id': instance_id,
                'created_by': 'Manual Mapping',
            }
            res = self.create(cr, uid, template_data, context)
        return True

magento_product_template()


class magento_product(osv.osv):
    _name = "magento.product"
    _order = 'id desc'
    _rec_name = "pro_name"
    _description = "Magento Product"
    _columns = {
        'pro_name': fields.many2one('product.product', 'Product Name'),
        'oe_product_id': fields.integer('Odoo Product Id'),
        'mag_product_id': fields.integer('Magento Product Id'),
        'magento_stock_id': fields.integer('Magento Stock Item Id'),
        'need_sync': fields.selection([('Yes', 'Yes'), ('No', 'No')], 'Update Required'),
        'instance_id': fields.many2one('magento.configure', 'Magento Instance'),
        'create_date': fields.datetime('Created Date'),
        'write_date': fields.datetime('Updated Date'),
        'created_by': fields.char('Created By', size=64)
    }
    _defaults = {
        'created_by': 'odoo',
        'need_sync': 'No',
    }
magento_product()


class magento_category(osv.osv):
    _name = "magento.category"
    _order = 'id desc'
    _rec_name = "cat_name"
    _description = "Magento Category"

    _columns = {
        'cat_name': fields.many2one('product.category', 'Category Name'),
        'oe_category_id': fields.integer('Odoo Category Id'),
        'mag_category_id': fields.integer('Magento Category Id'),
        'instance_id': fields.many2one('magento.configure', 'Magento Instance'),
        'need_sync': fields.selection([('Yes', 'Yes'), ('No', 'No')], 'Update Required'),
        'create_date': fields.datetime('Created Date'),
        'write_date': fields.datetime('Updated Date'),
        'created_by': fields.char('Created By', size=64)
    }
    _defaults = {
        'created_by': 'odoo',
        'need_sync': 'No',
    }

    def create_category(self, cr, uid, data, context=None):
        """Create and update a category by any webservice like xmlrpc.
        @param data: details of category fields in list.
        """
        if context is None:
            context = {}
        categ_dic = {}
        category_id = 0
        if data.has_key('name') and data['name']:
            categ_dic['name'] = _unescape(data.get('name'))

        if data.has_key('type'):
            categ_dic['type'] = data.get('type')
        if data.has_key('parent_id'):
            categ_dic['parent_id'] = data.get('parent_id')
        if data.get('method') == 'create':
            mage_category_id = data.get('mage_id')
            category_id = self.pool.get('product.category').create(
                cr, uid, categ_dic, context)
            self.create(cr, uid, {'cat_name': category_id, 'oe_category_id': category_id,
                                  'mag_category_id': mage_category_id, 'instance_id': context.get('instance_id'), 'created_by': 'Magento'})
            return category_id
        if data.get('method') == 'write':
            category_id = data.get('category_id')
            self.pool.get('product.category').write(
                cr, uid, category_id, categ_dic, context)
            return True
        return False
magento_category()


class magento_attribute_set(osv.osv):
    _name = "magento.attribute.set"
    _description = "Magento Attribute Set"
    _order = 'id desc'

    _columns = {
        'name': fields.char('Magento Attribute Set'),
        'attribute_ids': fields.many2many('product.attribute', 'product_attr_set', 'set_id', 'attribute_id', 'Product Attributes', readonly=True, help="Magento Set attributes will be handle only at magento."),
        'instance_id': fields.many2one('magento.configure', 'Magento Instance'),
        'set_id': fields.integer('Magento Set Id', readonly=True),
        'created_by': fields.char('Created By', size=64),
        'create_date': fields.datetime('Created Date'),
        'write_date': fields.datetime('Updated Date'),
    }
    _defaults = {
        'created_by': 'odoo',
    }

    def create(self, cr, uid, vals, context=None):
        if context is None:
            context = {}
        if context.has_key('instance_id'):
            vals['instance_id'] = context.get('instance_id')
        return super(magento_attribute_set, self).create(cr, uid, vals, context=context)

magento_attribute_set()


class magento_product_attribute(osv.osv):
    _name = "magento.product.attribute"
    _order = 'id desc'
    _description = "Magento Product Attribute"

    def create(self, cr, uid, vals, context=None):
        if context is None:
            context = {}
        vals['erp_id'] = vals['name']
        if context.has_key('instance_id'):
            vals['instance_id'] = context.get('instance_id')
        return super(magento_product_attribute, self).create(cr, uid, vals, context=context)

    def write(self, cr, uid, ids, vals, context=None):
        if context is None:
            context = {}
        if vals.has_key('name'):
            vals['erp_id'] = vals['name']
        if context.has_key('instance_id'):
            vals['instance_id'] = context.get('instance_id')
        return super(magento_product_attribute, self).write(cr, uid, ids, vals, context=context)

    _columns = {
        'name': fields.many2one('product.attribute', 'Product Attribute'),
        'erp_id': fields.integer('Odoo`s Attribute Id'),
        'mage_id': fields.integer('Magento`s Attribute Id'),
        'mage_attribute_code': fields.char('Magento`s Attribute Code'),
        'instance_id': fields.many2one('magento.configure', 'Magento Instance'),
        'created_by': fields.char('Created By', size=64),
        'create_date': fields.datetime('Created Date'),
        'write_date': fields.datetime('Updated Date'),
    }
    _defaults = {
        'created_by': 'odoo',
    }
magento_product_attribute()


class magento_product_attribute_value(osv.osv):
    _name = "magento.product.attribute.value"
    _order = 'id desc'
    _description = "Magento Product Attribute Value"

    def create(self, cr, uid, vals, context=None):
        if context is None:
            context = {}
        vals['erp_id'] = vals['name']
        if context.has_key('instance_id'):
            vals['instance_id'] = context.get('instance_id')
        return super(magento_product_attribute_value, self).create(cr, uid, vals, context=context)

    def write(self, cr, uid, ids, vals, context=None):
        if context is None:
            context = {}
        if vals.has_key('name'):
            vals['erp_id'] = vals['name']
        if context.has_key('instance_id'):
            vals['instance_id'] = context.get('instance_id')
        return super(magento_product_attribute_value, self).write(cr, uid, ids, vals, context=context)

    _columns = {
        'name': fields.many2one('product.attribute.value', 'Attribute Value'),
        'erp_id': fields.integer('Odoo Attribute Value Id'),
        'mage_id': fields.integer('Magento Attribute Value Id'),
        'instance_id': fields.many2one('magento.configure', 'Magento Instance'),
        'created_by': fields.char('Created By', size=64),
        'create_date': fields.datetime('Created Date'),
        'write_date': fields.datetime('Updated Date'),
    }
    _defaults = {
        'created_by': 'odoo',
    }
magento_product_attribute_value()

################### Catalog Mapping Models End ########################

############## Magento Customer Mapping Models ################


class magento_customers(osv.osv):
    _name = "magento.customers"
    _order = 'id desc'
    _rec_name = "cus_name"
    _description = "Magento Customers"
    _columns = {
        'cus_name': fields.many2one('res.partner', 'Customer Name'),
        'oe_customer_id': fields.integer('Odoo Customer Id'),
        'mag_customer_id': fields.char('Magento Customer Id', size=50),
        'instance_id': fields.many2one('magento.configure', 'Magento Instance'),
        'mag_address_id': fields.char('Magento Address Id', size=50),
        'need_sync': fields.selection([('Yes', 'Yes'), ('No', 'No')], 'Update Required'),
        'created_by': fields.char('Created By', size=64),
        'create_date': fields.datetime('Created Date'),
        'write_date': fields.datetime('Updated Date'),
    }
    _defaults = {
        'created_by': 'odoo',
        'need_sync': 'No',
    }

magento_customers()


class magento_region(osv.osv):
    _name = "magento.region"
    _order = 'id desc'
    _description = "Magento Region"
    _columns = {
        'name': fields.char('Region Name', size=100),
        'mag_region_id': fields.integer('Magento Region Id'),
        'country_code': fields.char('Country Code', size=10),
        'region_code': fields.char('Region Code', size=10),
        'created_by': fields.char('Created By', size=64),
        'create_date': fields.datetime('Created Date'),
        'write_date': fields.datetime('Updated Date'),
    }
    _defaults = {
        'created_by': 'odoo'
    }
magento_region()
############# Customer Model End #############################

ORDER_STATUS = [
    ('draft', 'Draft Quotation'),
    ('sent', 'Quotation Sent'),
    ('cancel', 'Cancelled'),
    ('waiting_date', 'Waiting Schedule'),
    ('progress', 'Sales Order'),
    ('manual', 'Sale to Invoice'),
    ('shipping_except', 'Shipping Exception'),
    ('invoice_except', 'Invoice Exception'),
    ('done', 'Done'),
]


class magento_orders(osv.osv):
    _name = "magento.orders"
    _order = 'id desc'
    _rec_name = "order_ref"
    _description = "Magento Orders"
    _columns = {
        'order_ref': fields.many2one('sale.order', 'Order Reference'),
        'oe_order_id': fields.integer('Odoo order Id'),
        'mage_increment_id': fields.char('Magento order Id', size=100),
        'instance_id': fields.many2one('magento.configure', 'Magento Instance'),
        'order_status': fields.related('order_ref', 'state', type='selection', selection=ORDER_STATUS, string='Order Status'),
        'paid_status': fields.related('order_ref', 'invoiced', type='boolean', relation='sale.order', string='Paid'),
        'ship_status': fields.related('order_ref', 'shipped', type='boolean', relation='sale.order', string='Shipped'),
        'order_total': fields.related('order_ref', 'amount_total', type='float', relation='sale.order', string='Order Total'),
        'create_date': fields.datetime('Created Date'),
    }

    def create(self, cr, uid, vals, context=None):
        if context is None:
            context = {}
        if context.has_key('instance_id'):
            vals['instance_id'] = context.get('instance_id')
        return super(magento_orders, self).create(cr, uid, vals, context=context)
magento_orders()

###############     History 	####################


class magento_sync_history(osv.osv):
    _name = "magento.sync.history"
    _order = 'id desc'
    _description = "Magento Synchronization History"
    _columns = {
        'status': fields.selection((('yes', 'Successfull'), ('no', 'Un-Successfull')), 'Status'),
        'action_on': fields.selection((('product', 'Product'), ('category', 'Category'), ('customer', 'Customer'), ('order', 'Order')), 'Action On'),
        'action': fields.selection((('a', 'Import'), ('b', 'Export'), ('c', 'Update')), 'Action'),
        'create_date': fields.datetime('Created Date'),
        'error_message': fields.text('Summary'),
    }
magento_sync_history()
# END

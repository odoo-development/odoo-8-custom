# -*- coding: utf-8 -*-
##############################################################################
#
#    Cybrosys Technologies Pvt. Ltd.
#    Copyright (C) 2017-TODAY Cybrosys Technologies(<https://www.cybrosys.com>).
#    Author: Linto C T(<https://www.cybrosys.com>)
#    you can modify it under the terms of the GNU LESSER
#    GENERAL PUBLIC LICENSE (LGPL v3), Version 3.
#
#    It is forbidden to publish, distribute, sublicense, or sell copies
#    of the Software or modified copies of the Software.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU LESSER GENERAL PUBLIC LICENSE (LGPL v3) for more details.
#
#    You should have received a copy of the GNU LESSER GENERAL PUBLIC LICENSE
#    GENERAL PUBLIC LICENSE (LGPL v3) along with this program.
#    If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp import models, api
from datetime import datetime


class ReportAvgPrices(models.AbstractModel):
    _name = 'report.product_ageing_report.report_ageing_analysis'

    def get_productss(self, docs):
        """input : starting date, location and category
          output: a dictionary with all the products and their stock for that currespnding intervals"""

        cr = self._cr
        # if docs.location_id and docs.product_categ:
        #     cr.execute("select sq.id from stock_quant sq inner join product_product pp on(pp.id=sq.product_id) "
        #                " inner join product_template pt on(pt.id=pp.product_tmpl_id and pt.categ_id in %s) "
        #                "where sq.location_id in %s and sq.qty > 0 and sq.in_date <=%s", (tuple(docs.product_categ.ids),
        #                                                              tuple(docs.location_id.ids), docs.from_date))
        # elif docs.location_id:
        #     cr.execute("select sq.id from stock_quant sq where sq.location_id in %s and sq.qty > 0 and sq.in_date <=%s",
        #                (tuple(docs.location_id.ids), docs.from_date))
        # elif docs.product_categ:
        #     cr.execute("select sq.id from stock_quant sq inner join product_product pp on(pp.id=sq.product_id) "
        #                " inner join product_template pt on(pt.id=pp.product_tmpl_id and pt.categ_id in %s)"
        #                "where sq.qty > 0  and sq.in_date <=%s", (tuple(docs.product_categ.ids), docs.from_date))
        # else:
        #     # cr.execute("select id from stock_quant where qty > 0  and in_date <=%s", (docs.from_date,))


        ### o - 30 days

        from datetime import timedelta
        date1 = datetime.strptime(docs.from_date, '%Y-%m-%d %H:%M:%S').date()


        start_date1 =date1-timedelta(days=docs.interval)
        end_date1 = date1


        cr.execute("SELECT  in_date,product_id,SUM(qty) Total FROM    stock_quant WHERE  location_id in (68,60,36,19,53,29,75,12,50,150,186,228,246) GROUP BY product_id,in_date", (start_date1,docs.from_date,))

        first_range_data = cr.fetchall()



        no_days = docs.interval*2

        start_date2 = date1 - timedelta(days=no_days)
        end_date2 = start_date1

        no_days = docs.interval * 3

        start_date3= date1 - timedelta(days=no_days)
        end_date3 = start_date2

        no_days = docs.interval * 4

        start_date4 =  date1 - timedelta(days=no_days)
        end_date4 = start_date3



        start_date5 = start_date4



        products = {}
        product_list = []


        cr.execute("SELECT  stock_quant.product_id,sum(( select stock_move.price_unit from stock_quant_move_rel, stock_move where stock_move.id = stock_quant_move_rel.move_id and stock_quant_move_rel.quant_id=stock_quant.id limit 1) * stock_quant.qty) as inventory_value ,sum(stock_quant.cost * stock_quant.qty)FROM  stock_quant WHERE  stock_quant.location_id in (68,60,36,19,53,29,75,12,50,150,186,228,246) group by stock_quant.product_id")

        inventory_value_query = cr.fetchall()
        inventory_value_dict={}

        for row in inventory_value_query:
            cost_value = row[1] if row[1] >0 else row[2]
            inventory_value_dict.update({

                    row[0]:cost_value


            })


        for item in first_range_data:
            temp={}
            current_date = datetime.strptime(item[0], '%Y-%m-%d %H:%M:%S').date()
            if item[1] not in product_list:
                product_list.append(item[1])

                ids = [item[1]]
                uid = 1
                prod_obj = self.pool.get('product.product').browse(cr, uid, ids, context={})[0]
                # product_cost = prod_obj.average_purchaes_price
                #
                # product_cost = product_cost if product_cost > 0 else prod_obj.standard_price

                temp = {
                    'product_id': item[1],
                    'product': prod_obj.name,
                    'product_sku': prod_obj.default_code,
                    'category': prod_obj.categ_id.name,
                    'product_cost': 0,
                    'total_qty': 0,
                    '1st_range': 0,
                    '2nd_range': 0,
                    '3rd_range': 0,
                    '4th_range': 0,
                    '5th_range': 0,
                    'inventory_value': 0
                }




                if start_date1 < current_date <= end_date1:
                    temp['1st_range'] += item[2]
                elif start_date2 < current_date <= end_date2:
                    temp['2nd_range'] += item[2]
                elif start_date3 < current_date <= end_date3:
                    temp['3rd_range'] += item[2]
                elif start_date4 < current_date <= end_date4:
                    temp['4th_range'] += item[2]
                elif current_date <= start_date4:
                    temp['5th_range'] += item[2]
                temp['total_qty'] += item[2]
                temp['inventory_value']=inventory_value_dict[item[1]]
                products[item[1]] = temp

            else:
                temp = products[item[1]]
                if start_date1 < current_date <= end_date1:
                    temp['1st_range'] += item[2]
                elif start_date2 < current_date <= end_date2:
                    temp['2nd_range'] += item[2]
                elif start_date3 < current_date <= end_date3:
                    temp['3rd_range'] += item[2]
                elif start_date4 < current_date <= end_date4:
                    temp['4th_range'] += item[2]
                elif current_date <= start_date4:
                    temp['5th_range'] += item[2]

                temp['total_qty'] += item[2]
                temp['inventory_value'] = inventory_value_dict[item[1]]
                products[item[1]] = temp



        return products

    @api.model
    def render_html(self, docids, data=None):
        """we are overwriting this function because we need to show values from other models in the report
                we pass the objects in the docargs dictionary"""

        self.model = self.env.context.get('active_model')
        docs = self.env[self.model].browse(self.env.context.get('active_id'))
        products = self.get_productss(docs)
        interval = ['0-'+str(docs.interval),
                    str(docs.interval)+'-'+str(2*docs.interval),
                    str(2*docs.interval)+'-'+str(3*docs.interval),
                    str(3*docs.interval)+'-'+str(4*docs.interval),
                    str(4*docs.interval)+'+']
        loc = ""
        categ = ""
        for i in docs.location_id:
            if i.location_id.name and i.name:
                loc += i.location_id.name+" / "+i.name+", "
        for i in docs.product_categ:
            if i.name:
                categ += i.name+", "
        loc = loc[:-2]
        categ = categ[:-2]
        docargs = {
            'doc_ids': self.ids,
            'doc_model': self.model,
            'docs': docs,
            'loc': loc,
            'categ': categ,
            'interval': interval,
            'products': products,
        }
        return self.env['report'].render('product_ageing_report.report_ageing_analysis', docargs)

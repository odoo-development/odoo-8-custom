{
    'name': "IT Assets Management System",

    'version': '1.0',

    'category': 'Information Technology',

    'author': "Md Rockibul Alam Babu",

    'description': """
    
IT asset management.
==========================================
This Module manages the assets owned by a company or an individual. It will keep 
your IT assets on your system.

    """,

    'website': "http://www.zerogravity.com.bd",

    'data': [
        'security/assets_security.xml',
        'security/ir.model.access.csv',
        'assets_report.xml',
        'assets_view.xml',
        'views/report_assets_receive.xml',
        'views/report_assets_return.xml',
        'views/report_asset_policy.xml',
    ],

    'depends': [
        'base',
        'hr',
        'product',
        'purchase',
        'sale'
    ],

    'installable': True,
    'auto_install': False,
}
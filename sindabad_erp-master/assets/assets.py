import openerp
from openerp.osv import osv, fields


class asset(osv.Model):
    _name = 'assets.asset'

    _columns = {

        'name': fields.many2one('hr.employee', 'Name', required=True),

        'department_id': fields.many2one('hr.department', 'Department'),

        'job_id': fields.many2one('hr.job', 'Job Title'),

        'device': fields.selection(
            [('select', "Select"),
             ('desktop', "Desktop"),
             ('laptop', "Laptop")],
            'Device', required=True,
            default='select'),

        'asset_property': fields.selection(
            [('select', "Select"),
             ('personal', "Personal"),
             ('company', "Company")],
            'Device Property', required=True,
            default='select'),
        'work_email': fields.char('Email', size=240),
        'mobile_phone': fields.char('Mobile', readonly=False),
        'work_location': fields.char('Office Location'),
        'ip_address': fields.char('IP Address'),
        'subnet_masks': fields.char('Subnet Mask'),
        'gateway': fields.char('Gateway'),
        'hr_name': fields.char(' HR- Name'),
        'hr_department': fields.char(' HR- Department'),
        'hr_designation': fields.char(' HR- Designation'),
        'admin_name': fields.char(' IT Admin- Name'),
        'admin_department': fields.char(' IT Admin- Department'),
        'admin_designation': fields.char(' IT Admin- Designation'),

        'note': fields.text('Note'),

        'asset_lines': fields.one2many('assets.line', 'line_id', 'Assets Lines', required=True),
    }



    def onchange_employee_id(self, cr, uid, ids, emp_i, context=None):
        v = {}
        if emp_i:
            employee = self.pool.get('hr.employee').browse(cr, uid, emp_i, context=context)[0]
            if employee.department_id.id:
                v['department_id'] = employee.department_id and employee.department_id.id or False
            if employee.job_id:
                v['job_id'] = employee.job_id
            if employee.work_email:
                v['work_email'] = employee.work_email
            if employee.mobile_phone:
                v['mobile_phone'] = employee.mobile_phone
            if employee.work_location:
                v['work_location'] = employee.work_location

        return {'value': v}



class mymodule_line(osv.Model):
    _name = 'assets.line'

    _columns = {
        'line_id': fields.many2one('assets.assets', string='Order'),
        'product_id': fields.many2one('product.template', string='Product'),
        'details': fields.char('Details'),
        'receive_date': fields.date('Receive Date', required=True),
        'return_date': fields.date('Return Date'),
        'model_name': fields.char('Model'),
        'brand_name': fields.char('Brand'),
        'vendor_name': fields.char('Vendor'),
    }




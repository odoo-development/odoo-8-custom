# Author Mufti Muntasir Ahmed 26-02-2018

{
    'name': 'Partner Statement',
    'version': '1.0.0',
    'category': 'Accounts',
    'description': """
Allows you to print Partner Statement    
==============================================================



""",
    'author': 'Mufti Muntasir Ahmed',
    'depends': ['account'],
    'data': [

        'partner_menu.xml',
        'wizard/customer_report_statement_view.xml',
        'views/report_customerstatement.xml',


    ],

    'installable': True,
    'auto_install': False,
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:

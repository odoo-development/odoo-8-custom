{
    "name": "Add Paid Image on Invoice",
    "version": "1.0",
    "depends": [
        "base",
        "account"
    ],
    "author": "Rocky",
    "category": "Accounting",
    "description": """
Features
======================================================================

* Add image on Invoice depending it's state: Paid

""",
    "data": [
        "report/paid_invoice_report_view.xml",
    ],
    "installable": True,
    "application": True,
    "auto_install": False,
}

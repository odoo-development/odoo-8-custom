{
    'name': "Warehouse wise area mapping",
    'version': '0.1',
    'category': 'Warehouse',
    'author': "Md Rockibul Alam Babu",
    'summary': """
         Warehouse wise area mapping for orders""",
    'description': """
        Warehouse wise area mapping for orders.
    """,
    'depends': [
        'base', 'stock',
    ],
    'data': [
        'security/wh_area_security.xml',
        'security/ir.model.access.csv',
        'views/wh_area_mapping_view.xml',
        'wh_area_mapping_menu.xml',
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}
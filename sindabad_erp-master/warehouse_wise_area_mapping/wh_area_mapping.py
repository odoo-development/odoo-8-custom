from datetime import datetime, timedelta
import time
from datetime import date
from openerp import SUPERUSER_ID
from openerp.osv import fields, osv
from openerp.tools.translate import _
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT
import openerp.addons.decimal_precision as dp
from openerp import workflow
from openerp import api
import datetime
from openerp.exceptions import except_orm, Warning, RedirectWarning
from openerp.tools.translate import _


class warehouse_area(osv.osv):
    _name = "warehouse.area"
    _description = "Warehouse area"
    _order = 'id desc'

    _columns = {
        'warehouse_id': fields.many2one('stock.warehouse', string='Warehouse Name'),
        'code': fields.char('Code'),
        'areas': fields.text('Areas'),
    }

    def onchange_warehouse_id(self, cr, uid, ids, warehouse_id, context=None):
        res = {}
        if warehouse_id:

            wh_name = self.pool.get('stock.warehouse').browse(cr, uid, warehouse_id, context=context)[0]
            if wh_name.code:
                res['code'] = wh_name.code

        return {'value': res}




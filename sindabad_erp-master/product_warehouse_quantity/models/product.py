# -*- coding: utf-8 -*-
#################################################################################
#
#    Odoo, Open Source Management Solution
#    Copyright (C) 2017 Ascetic Business Solution <www.asceticbs.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#################################################################################

from openerp import api, fields, models, _


import time
from datetime import datetime
from dateutil.relativedelta import relativedelta

from openerp.osv import fields, osv
import openerp.addons.decimal_precision as dp
from openerp.tools import float_compare
from openerp.tools.translate import _



class product_product(osv.osv):
    _inherit = "product.product"


    def _get_warehouse_quantity(self, cr, uid, ids, name, arg, context=None):
        res = {}
        for record in ids:
            warehouse_quantity_text = ''


            quant_ids = self.pool.get('stock.quant').search(cr, uid, [('product_id','=',record),('location_id.usage','=','internal')])
            quant_ids_obj = self.pool.get('stock.quant').browse(cr, uid, quant_ids, context=context)

            t_warehouses = {}
            for quant in quant_ids_obj:
                if quant.location_id:
                    if quant.location_id not in t_warehouses:
                        t_warehouses.update({quant.location_id:0})
                    t_warehouses[quant.location_id] += quant.qty

            tt_warehouses = {}
            for location in t_warehouses:
                warehouse = False
                location1 = location

                while (not warehouse and location1):
                    warehouse_id = self.pool.get('stock.warehouse').search(cr, uid,[('lot_stock_id','=',location1.id)])
                    if len(warehouse_id) > 0:
                        warehouse = True
                    else:
                        warehouse = False
                    location1 = location1.location_id
                if warehouse_id:
                    warehouse_id = self.pool.get('stock.warehouse').browse(cr, uid, warehouse_id, context=context)
                    if warehouse_id.name not in tt_warehouses:
                        tt_warehouses.update({warehouse_id.name:0})
                    tt_warehouses[warehouse_id.name] += t_warehouses[location]

            for item in tt_warehouses:
                if tt_warehouses[item] != 0:
                    warehouse_quantity_text = warehouse_quantity_text + ' ** ' + item + ': ' + str(tt_warehouses[item])

            res[record] = warehouse_quantity_text
        return res

    def _get_uttwh_warehouse_quantity(self, cr, uid, ids, name, arg, context=None):
        res = {}
        for record in ids:
            warehouse_quantity = 0


            quant_ids = self.pool.get('stock.quant').search(cr, uid, [('product_id','=',record),('location_id.usage','=','internal')])
            quant_ids_obj = self.pool.get('stock.quant').browse(cr, uid, quant_ids, context=context)

            t_warehouses = {}
            for quant in quant_ids_obj:
                if quant.location_id:
                    if quant.location_id not in t_warehouses:
                        t_warehouses.update({quant.location_id:0})
                    t_warehouses[quant.location_id] += quant.qty

            tt_warehouses = {}
            for location in t_warehouses:
                warehouse = False
                location1 = location

                while (not warehouse and location1):
                    warehouse_id = self.pool.get('stock.warehouse').search(cr, uid,[('lot_stock_id','=',location1.id)])
                    if len(warehouse_id) > 0:
                        warehouse = True
                    else:
                        warehouse = False
                    location1 = location1.location_id
                if warehouse_id:
                    warehouse_id = self.pool.get('stock.warehouse').browse(cr, uid, warehouse_id, context=context)
                    if warehouse_id.name not in tt_warehouses:
                        tt_warehouses.update({warehouse_id.id:0})

                    tt_warehouses[warehouse_id.id] += t_warehouses[location]

            for item in tt_warehouses:
                
                if item == 1:
                    warehouse_quantity = tt_warehouses[item]

            res[record] = warehouse_quantity
        return res


    def _get_nodda_warehouse_quantity(self, cr, uid, ids, name, arg, context=None):
        res = {}
        for record in ids:
            warehouse_quantity = 0


            quant_ids = self.pool.get('stock.quant').search(cr, uid, [('product_id','=',record),('location_id.usage','=','internal')])
            quant_ids_obj = self.pool.get('stock.quant').browse(cr, uid, quant_ids, context=context)

            t_warehouses = {}
            for quant in quant_ids_obj:
                if quant.location_id:
                    if quant.location_id not in t_warehouses:
                        t_warehouses.update({quant.location_id:0})
                    t_warehouses[quant.location_id] += quant.qty

            tt_warehouses = {}
            for location in t_warehouses:
                warehouse = False
                location1 = location

                while (not warehouse and location1):
                    warehouse_id = self.pool.get('stock.warehouse').search(cr, uid,[('lot_stock_id','=',location1.id)])
                    if len(warehouse_id) > 0:
                        warehouse = True
                    else:
                        warehouse = False
                    location1 = location1.location_id
                if warehouse_id:
                    warehouse_id = self.pool.get('stock.warehouse').browse(cr, uid, warehouse_id, context=context)
                    if warehouse_id.name not in tt_warehouses:
                        tt_warehouses.update({warehouse_id.id:0})

                    tt_warehouses[warehouse_id.id] += t_warehouses[location]

            for item in tt_warehouses:
                
                if item == 2:
                    warehouse_quantity = tt_warehouses[item]

            res[record] = warehouse_quantity
        return res


    def _get_moti_warehouse_quantity(self, cr, uid, ids, name, arg, context=None):
        res = {}
        for record in ids:
            warehouse_quantity = 0


            quant_ids = self.pool.get('stock.quant').search(cr, uid, [('product_id','=',record),('location_id.usage','=','internal')])
            quant_ids_obj = self.pool.get('stock.quant').browse(cr, uid, quant_ids, context=context)

            t_warehouses = {}
            for quant in quant_ids_obj:
                if quant.location_id:
                    if quant.location_id not in t_warehouses:
                        t_warehouses.update({quant.location_id:0})
                    t_warehouses[quant.location_id] += quant.qty

            tt_warehouses = {}
            for location in t_warehouses:
                warehouse = False
                location1 = location

                while (not warehouse and location1):
                    warehouse_id = self.pool.get('stock.warehouse').search(cr, uid,[('lot_stock_id','=',location1.id)])
                    if len(warehouse_id) > 0:
                        warehouse = True
                    else:
                        warehouse = False
                    location1 = location1.location_id
                if warehouse_id:
                    warehouse_id = self.pool.get('stock.warehouse').browse(cr, uid, warehouse_id, context=context)
                    if warehouse_id.name not in tt_warehouses:
                        tt_warehouses.update({warehouse_id.id:0})

                    tt_warehouses[warehouse_id.id] += t_warehouses[location]

            for item in tt_warehouses:
                
                if item == 4:
                    warehouse_quantity = tt_warehouses[item]

            res[record] = warehouse_quantity
        return res

    def _get_fun_for_free_qty(self, cr, uid, ids, location):
        res = {}

        for item in ids:
            cr.execute(
                "select sum(qty) from stock_quant where  reservation_id NOTNULL and product_id = {0} and location_id = {1}".format(
                    item, location))
            result = cr.fetchall()
            if len(result) > 0:
                res[item] = result[0][0]
            else:
                res[item] = 0
        return res


    def _get_uttwh_forcast_quantity(self, cr, uid, ids, name, arg, context=None):
        res = {}

        for id in ids:
            product = self.browse(cr, uid, id, context=context)
            qty =product.with_context({'location': 12}).virtual_available
            res[id]=qty
        return res

    def _get_nodda_forcast_quantity(self, cr, uid, ids, name, arg, context=None):
        res = {}

        for record in ids:
            product = self.browse(cr, uid, record, context=context)
            qty = product.with_context({'location': 19}).virtual_available
            res[record]=qty
        return res

    def _get_moti_forcast_quantity(self, cr, uid, ids, name, arg, context=None):
        res = {}

        for record in ids:
            product = self.browse(cr, uid, record, context=context)
            qty = product.with_context({'location': 36}).virtual_available
            res[record] = qty

        return res

    def _get_moti_free_quantity(self, cr, uid, ids, name, arg, context=None):
        res = {}

        loc_id = 36

        stock_in_hand = self._get_moti_warehouse_quantity(cr, uid, ids, name, arg, context=None)
        forcasted_qty = self._get_fun_for_free_qty(cr, uid, ids, loc_id)
        for k, v in stock_in_hand.iteritems(): stock_in_hand[k] = 0 if v is None else v
        for k, v in forcasted_qty.iteritems(): forcasted_qty[k] = 0 if v is None else v


        for record in ids:
            res_count = stock_in_hand.get(record) - forcasted_qty.get(record)
            if res_count <0:
                res_count=0
            res[record]=res_count


        return res

    def _get_uttwh_free_quantity(self, cr, uid, ids, name, arg, context=None):
        res = {}

        loc_id = 12

        stock_in_hand = self._get_uttwh_warehouse_quantity(cr, uid, ids, name, arg, context=None)
        forcasted_qty = self._get_fun_for_free_qty(cr, uid, ids, loc_id)

        for k, v in stock_in_hand.iteritems(): stock_in_hand[k] = 0 if v is None else v
        for k, v in forcasted_qty.iteritems(): forcasted_qty[k] = 0 if v is None else v



        for record in ids:
            res_count = stock_in_hand.get(record) - forcasted_qty.get(record)
            if res_count <0:
                res_count=0
            res[record]=res_count



        return res

    def _get_nodda_free_quantity(self, cr, uid, ids, name, arg, context=None):
        res = {}

        loc_id = 19

        stock_in_hand = self._get_nodda_warehouse_quantity(cr, uid, ids, name, arg, context=None)
        forcasted_qty = self._get_fun_for_free_qty(cr, uid, ids, loc_id)

        for k, v in stock_in_hand.iteritems(): stock_in_hand[k] = 0 if v is None else v
        for k, v in forcasted_qty.iteritems(): forcasted_qty[k] = 0 if v is None else v

        for record in ids:
            res_count = stock_in_hand.get(record) - forcasted_qty.get(record)
            if res_count <0:
                res_count=0
            res[record]=res_count

        return res

    def _get_nodda_reatil_warehouse_quantity(self, cr, uid, ids, name, arg, context=None):
        res = {}
        for record in ids:
            warehouse_quantity = 0

            quant_ids = self.pool.get('stock.quant').search(cr, uid, [('product_id', '=', record),
                                                                      ('location_id.usage', '=', 'internal')])
            quant_ids_obj = self.pool.get('stock.quant').browse(cr, uid, quant_ids, context=context)

            t_warehouses = {}
            for quant in quant_ids_obj:
                if quant.location_id:
                    if quant.location_id not in t_warehouses:
                        t_warehouses.update({quant.location_id: 0})
                    t_warehouses[quant.location_id] += quant.qty

            tt_warehouses = {}
            for location in t_warehouses:
                warehouse = False
                location1 = location

                while (not warehouse and location1):
                    warehouse_id = self.pool.get('stock.warehouse').search(cr, uid,
                                                                           [('lot_stock_id', '=', location1.id)])
                    if len(warehouse_id) > 0:
                        warehouse = True
                    else:
                        warehouse = False
                    location1 = location1.location_id
                if warehouse_id:
                    warehouse_id = self.pool.get('stock.warehouse').browse(cr, uid, warehouse_id, context=context)
                    if warehouse_id.name not in tt_warehouses:
                        tt_warehouses.update({warehouse_id.id: 0})

                    tt_warehouses[warehouse_id.id] += t_warehouses[location]

            for item in tt_warehouses:

                if item == 3:
                    warehouse_quantity = tt_warehouses[item]

            res[record] = warehouse_quantity
        return res

    def _get_nodda_retail_forcast_quantity(self, cr, uid, ids, name, arg, context=None):
        res = {}

        for record in ids:
            product = self.browse(cr, uid, record, context=context)
            qty = product.with_context({'location': 29}).virtual_available
            res[record] = qty


        return res



    def _get_nodda_retail_free_quantity(self, cr, uid, ids, name, arg, context=None):
        res = {}

        loc_id = 29

        stock_in_hand = self._get_nodda_reatil_warehouse_quantity(cr, uid, ids, name, arg, context=None)
        forcasted_qty = self._get_fun_for_free_qty(cr, uid, ids, loc_id)
        for k, v in stock_in_hand.iteritems(): stock_in_hand[k] = 0 if v is None else v
        for k, v in forcasted_qty.iteritems(): forcasted_qty[k] = 0 if v is None else v

        for record in ids:
            res_count = stock_in_hand.get(record) - forcasted_qty.get(record)
            if res_count <0:
                res_count=0
            res[record]=res_count

        return res



    def _get_uttwh_reatil_warehouse_quantity(self, cr, uid, ids, name, arg, context=None):
        res = {}
        for record in ids:
            warehouse_quantity = 0

            quant_ids = self.pool.get('stock.quant').search(cr, uid, [('product_id', '=', record),
                                                                      ('location_id.usage', '=', 'internal')])
            quant_ids_obj = self.pool.get('stock.quant').browse(cr, uid, quant_ids, context=context)

            t_warehouses = {}
            for quant in quant_ids_obj:
                if quant.location_id:
                    if quant.location_id not in t_warehouses:
                        t_warehouses.update({quant.location_id: 0})
                    t_warehouses[quant.location_id] += quant.qty

            tt_warehouses = {}
            for location in t_warehouses:
                warehouse = False
                location1 = location

                while (not warehouse and location1):
                    warehouse_id = self.pool.get('stock.warehouse').search(cr, uid,
                                                                           [('lot_stock_id', '=', location1.id)])
                    if len(warehouse_id) > 0:
                        warehouse = True
                    else:
                        warehouse = False
                    location1 = location1.location_id
                if warehouse_id:
                    warehouse_id = self.pool.get('stock.warehouse').browse(cr, uid, warehouse_id, context=context)
                    if warehouse_id.name not in tt_warehouses:
                        tt_warehouses.update({warehouse_id.id: 0})

                    tt_warehouses[warehouse_id.id] += t_warehouses[location]

            for item in tt_warehouses:

                if item == 6:
                    warehouse_quantity = tt_warehouses[item]

            res[record] = warehouse_quantity
        return res


    def _get_uttwh_retail_forcast_quantity(self, cr, uid, ids, name, arg, context=None):
        res = {}

        for record in ids:
            product = self.browse(cr, uid, record, context=context)
            qty = product.with_context({'location': 53}).virtual_available
            res[record] = qty


        return res


    def _get_uttwh_retail_free_quantity(self, cr, uid, ids, name, arg, context=None):
        res = {}

        loc_id = 53

        stock_in_hand = self._get_uttwh_reatil_warehouse_quantity(cr, uid, ids, name, arg, context=None)
        forcasted_qty = self._get_fun_for_free_qty(cr, uid, ids, loc_id)
        for k, v in stock_in_hand.iteritems(): stock_in_hand[k] = 0 if v is None else v
        for k, v in forcasted_qty.iteritems(): forcasted_qty[k] = 0 if v is None else v

        for record in ids:
            res_count = stock_in_hand.get(record) - forcasted_qty.get(record)
            if res_count <0:
                res_count=0
            res[record]=res_count

        return res

    def _get_moti_retail_warehouse_quantity(self, cr, uid, ids, name, arg, context=None):
        res = {}
        for record in ids:
            warehouse_quantity = 0

            quant_ids = self.pool.get('stock.quant').search(cr, uid, [('product_id', '=', record),
                                                                      ('location_id.usage', '=', 'internal')])
            quant_ids_obj = self.pool.get('stock.quant').browse(cr, uid, quant_ids, context=context)

            t_warehouses = {}
            for quant in quant_ids_obj:
                if quant.location_id:
                    if quant.location_id not in t_warehouses:
                        t_warehouses.update({quant.location_id: 0})
                    t_warehouses[quant.location_id] += quant.qty

            tt_warehouses = {}
            for location in t_warehouses:
                warehouse = False
                location1 = location

                while (not warehouse and location1):
                    warehouse_id = self.pool.get('stock.warehouse').search(cr, uid,
                                                                           [('lot_stock_id', '=', location1.id)])
                    if len(warehouse_id) > 0:
                        warehouse = True
                    else:
                        warehouse = False
                    location1 = location1.location_id
                if warehouse_id:
                    warehouse_id = self.pool.get('stock.warehouse').browse(cr, uid, warehouse_id, context=context)
                    if warehouse_id.name not in tt_warehouses:
                        tt_warehouses.update({warehouse_id.id: 0})

                    tt_warehouses[warehouse_id.id] += t_warehouses[location]

            for item in tt_warehouses:

                if item == 7:
                    warehouse_quantity = tt_warehouses[item]

            res[record] = warehouse_quantity
        return res


    def _get_moti_retail_forcast_quantity(self, cr, uid, ids, name, arg, context=None):
        res = {}

        for record in ids:
            product = self.browse(cr, uid, record, context=context)
            qty = product.with_context({'location': 60}).virtual_available
            res[record] = qty

        return res


    def _get_moti_retail_free_quantity(self, cr, uid, ids, name, arg, context=None):
        res = {}

        loc_id = 60

        stock_in_hand = self._get_moti_retail_warehouse_quantity(cr, uid, ids, name, arg, context=None)
        forcasted_qty = self._get_fun_for_free_qty(cr, uid, ids, loc_id)
        for k, v in stock_in_hand.iteritems(): stock_in_hand[k] = 0 if v is None else v
        for k, v in forcasted_qty.iteritems(): forcasted_qty[k] = 0 if v is None else v

        for record in ids:
            res_count = stock_in_hand.get(record) - forcasted_qty.get(record)
            if res_count <0:
                res_count=0
            res[record]=res_count

        return res


    def _get_damage_warehouse_quantity(self, cr, uid, ids, name, arg, context=None):
        res = {}
        for record in ids:
            warehouse_quantity = 0

            quant_ids = self.pool.get('stock.quant').search(cr, uid, [('product_id', '=', record),
                                                                      ('location_id.usage', '=', 'internal')])
            quant_ids_obj = self.pool.get('stock.quant').browse(cr, uid, quant_ids, context=context)

            t_warehouses = {}
            for quant in quant_ids_obj:
                if quant.location_id:
                    if quant.location_id not in t_warehouses:
                        t_warehouses.update({quant.location_id: 0})
                    t_warehouses[quant.location_id] += quant.qty

            tt_warehouses = {}
            for location in t_warehouses:
                warehouse = False
                location1 = location

                while (not warehouse and location1):
                    warehouse_id = self.pool.get('stock.warehouse').search(cr, uid,
                                                                           [('lot_stock_id', '=', location1.id)])
                    if len(warehouse_id) > 0:
                        warehouse = True
                    else:
                        warehouse = False
                    location1 = location1.location_id
                if warehouse_id:
                    warehouse_id = self.pool.get('stock.warehouse').browse(cr, uid, warehouse_id, context=context)
                    if warehouse_id.name not in tt_warehouses:
                        tt_warehouses.update({warehouse_id.id: 0})

                    tt_warehouses[warehouse_id.id] += t_warehouses[location]

            for item in tt_warehouses:

                if item == 8:
                    warehouse_quantity = tt_warehouses[item]

            res[record] = warehouse_quantity
        return res

    # Comilla Warehouse Quantity, Forcasting Quantity and Free Quantity

    def _get_comilla_warehouse_quantity(self, cr, uid, ids, name, arg, context=None):
        res = {}
        for record in ids:
            warehouse_quantity = 0

            quant_ids = self.pool.get('stock.quant').search(cr, uid, [('product_id', '=', record),
                                                                      ('location_id.usage', '=', 'internal')])
            quant_ids_obj = self.pool.get('stock.quant').browse(cr, uid, quant_ids, context=context)

            t_warehouses = {}
            for quant in quant_ids_obj:
                if quant.location_id:
                    if quant.location_id not in t_warehouses:
                        t_warehouses.update({quant.location_id: 0})
                    t_warehouses[quant.location_id] += quant.qty

            tt_warehouses = {}
            for location in t_warehouses:
                warehouse = False
                location1 = location

                while (not warehouse and location1):
                    warehouse_id = self.pool.get('stock.warehouse').search(cr, uid,
                                                                           [('lot_stock_id', '=', location1.id)])
                    if len(warehouse_id) > 0:
                        warehouse = True
                    else:
                        warehouse = False
                    location1 = location1.location_id
                if warehouse_id:
                    warehouse_id = self.pool.get('stock.warehouse').browse(cr, uid, warehouse_id, context=context)
                    if warehouse_id.name not in tt_warehouses:
                        tt_warehouses.update({warehouse_id.id: 0})

                    tt_warehouses[warehouse_id.id] += t_warehouses[location]

            for item in tt_warehouses:

                if item == 19: ## In live 19
                    warehouse_quantity = tt_warehouses[item]

            res[record] = warehouse_quantity
        return res

    def _get_comilla_free_quantity(self, cr, uid, ids, name, arg, context=None):
        res = {}

        loc_id = 138 ## In live 138

        stock_in_hand = self._get_comilla_warehouse_quantity(cr, uid, ids, name, arg, context=None)
        forcasted_qty = self._get_fun_for_free_qty(cr, uid, ids, loc_id)
        for k, v in stock_in_hand.iteritems(): stock_in_hand[k] = 0 if v is None else v
        for k, v in forcasted_qty.iteritems(): forcasted_qty[k] = 0 if v is None else v

        for record in ids:
            res_count = stock_in_hand.get(record) - forcasted_qty.get(record)
            if res_count <0:
                res_count=0
            res[record]=res_count

        return res

    def _get_comilla_forcast_quantity(self, cr, uid, ids, name, arg, context=None):
        res = {}

        for id in ids:
            product = self.browse(cr, uid, id, context=context)
            qty =product.with_context({'location': 138}).virtual_available ## In live 138
            res[id]=qty
        return res

    # Signboard Warehouse Quantity, Forcasting Quantity and Free Quantity

    def _get_signboard_warehouse_quantity(self, cr, uid, ids, name, arg, context=None):
        res = {}
        for record in ids:
            warehouse_quantity = 0

            quant_ids = self.pool.get('stock.quant').search(cr, uid, [('product_id', '=', record),
                                                                      ('location_id.usage', '=', 'internal')])
            quant_ids_obj = self.pool.get('stock.quant').browse(cr, uid, quant_ids, context=context)

            t_warehouses = {}
            for quant in quant_ids_obj:
                if quant.location_id:
                    if quant.location_id not in t_warehouses:
                        t_warehouses.update({quant.location_id: 0})
                    t_warehouses[quant.location_id] += quant.qty

            tt_warehouses = {}
            for location in t_warehouses:
                warehouse = False
                location1 = location

                while (not warehouse and location1):
                    warehouse_id = self.pool.get('stock.warehouse').search(cr, uid,
                                                                           [('lot_stock_id', '=', location1.id)])
                    if len(warehouse_id) > 0:
                        warehouse = True
                    else:
                        warehouse = False
                    location1 = location1.location_id
                if warehouse_id:
                    warehouse_id = self.pool.get('stock.warehouse').browse(cr, uid, warehouse_id, context=context)
                    if warehouse_id.name not in tt_warehouses:
                        tt_warehouses.update({warehouse_id.id: 0})

                    tt_warehouses[warehouse_id.id] += t_warehouses[location]

            for item in tt_warehouses:

                if item == 21:  ## In live 21
                    warehouse_quantity = tt_warehouses[item]

            res[record] = warehouse_quantity
        return res

    def _get_signboard_free_quantity(self, cr, uid, ids, name, arg, context=None):
        res = {}

        loc_id = 150  ## In live 150

        stock_in_hand = self._get_signboard_warehouse_quantity(cr, uid, ids, name, arg, context=None)
        forcasted_qty = self._get_fun_for_free_qty(cr, uid, ids, loc_id)
        for k, v in stock_in_hand.iteritems(): stock_in_hand[k] = 0 if v is None else v
        for k, v in forcasted_qty.iteritems(): forcasted_qty[k] = 0 if v is None else v

        for record in ids:
            res_count = stock_in_hand.get(record) - forcasted_qty.get(record)
            if res_count < 0:
                res_count = 0
            res[record] = res_count

        return res

    def _get_signboard_forcast_quantity(self, cr, uid, ids, name, arg, context=None):
        res = {}

        for id in ids:
            product = self.browse(cr, uid, id, context=context)
            qty = product.with_context({'location': 150}).virtual_available  ## In live 150
            res[id] = qty
        return res

    # Badda warehouse total quantity and all operation

    def _get_Badda_warehouse_quantity(self, cr, uid, ids, name, arg, context=None):
        res = {}
        for record in ids:
            warehouse_quantity = 0

            quant_ids = self.pool.get('stock.quant').search(cr, uid, [('product_id', '=', record),
                                                                      ('location_id.usage', '=', 'internal')])
            quant_ids_obj = self.pool.get('stock.quant').browse(cr, uid, quant_ids, context=context)

            t_warehouses = {}
            for quant in quant_ids_obj:
                if quant.location_id:
                    if quant.location_id not in t_warehouses:
                        t_warehouses.update({quant.location_id: 0})
                    t_warehouses[quant.location_id] += quant.qty

            tt_warehouses = {}
            for location in t_warehouses:
                warehouse = False
                location1 = location

                while (not warehouse and location1):
                    warehouse_id = self.pool.get('stock.warehouse').search(cr, uid,
                                                                           [('lot_stock_id', '=', location1.id)])
                    if len(warehouse_id) > 0:
                        warehouse = True
                    else:
                        warehouse = False
                    location1 = location1.location_id
                if warehouse_id:
                    warehouse_id = self.pool.get('stock.warehouse').browse(cr, uid, warehouse_id, context=context)
                    if warehouse_id.name not in tt_warehouses:
                        tt_warehouses.update({warehouse_id.id: 0})

                    tt_warehouses[warehouse_id.id] += t_warehouses[location]

            for item in tt_warehouses:

                if item == 27:  ## In live 27
                    warehouse_quantity = tt_warehouses[item]

            res[record] = warehouse_quantity
        return res

    def _get_Badda_free_quantity(self, cr, uid, ids, name, arg, context=None):
        res = {}

        loc_id = 186  ## In live 186

        stock_in_hand = self._get_Badda_warehouse_quantity(cr, uid, ids, name, arg, context=None)
        forcasted_qty = self._get_fun_for_free_qty(cr, uid, ids, loc_id)
        for k, v in stock_in_hand.iteritems(): stock_in_hand[k] = 0 if v is None else v
        for k, v in forcasted_qty.iteritems(): forcasted_qty[k] = 0 if v is None else v

        for record in ids:
            res_count = stock_in_hand.get(record) - forcasted_qty.get(record)
            if res_count < 0:
                res_count = 0
            res[record] = res_count

        return res

    def _get_Badda_forcast_quantity(self, cr, uid, ids, name, arg, context=None):
        res = {}

        for id in ids:
            product = self.browse(cr, uid, id, context=context)
            qty = product.with_context({'location': 186}).virtual_available  ## In live 186
            res[id] = qty
        return res

    # Saver warehouse total quantity and all operation

    def _get_saver_warehouse_quantity(self, cr, uid, ids, name, arg, context=None):
        res = {}
        for record in ids:
            warehouse_quantity = 0

            quant_ids = self.pool.get('stock.quant').search(cr, uid, [('product_id', '=', record),
                                                                      ('location_id.usage', '=', 'internal')])
            quant_ids_obj = self.pool.get('stock.quant').browse(cr, uid, quant_ids, context=context)

            t_warehouses = {}
            for quant in quant_ids_obj:
                if quant.location_id:
                    if quant.location_id not in t_warehouses:
                        t_warehouses.update({quant.location_id: 0})
                    t_warehouses[quant.location_id] += quant.qty

            tt_warehouses = {}
            for location in t_warehouses:
                warehouse = False
                location1 = location

                while (not warehouse and location1):
                    warehouse_id = self.pool.get('stock.warehouse').search(cr, uid,
                                                                           [('lot_stock_id', '=', location1.id)])
                    if len(warehouse_id) > 0:
                        warehouse = True
                    else:
                        warehouse = False
                    location1 = location1.location_id
                if warehouse_id:
                    warehouse_id = self.pool.get('stock.warehouse').browse(cr, uid, warehouse_id, context=context)
                    if warehouse_id.name not in tt_warehouses:
                        tt_warehouses.update({warehouse_id.id: 0})

                    tt_warehouses[warehouse_id.id] += t_warehouses[location]

            for item in tt_warehouses:

                if item == 37:  ## In live 37
                    warehouse_quantity = tt_warehouses[item]

            res[record] = warehouse_quantity
        return res

    def _get_saver_free_quantity(self, cr, uid, ids, name, arg, context=None):
        res = {}

        loc_id = 246  ## In live 246

        stock_in_hand = self._get_saver_warehouse_quantity(cr, uid, ids, name, arg, context=None)
        forcasted_qty = self._get_fun_for_free_qty(cr, uid, ids, loc_id)
        for k, v in stock_in_hand.iteritems(): stock_in_hand[k] = 0 if v is None else v
        for k, v in forcasted_qty.iteritems(): forcasted_qty[k] = 0 if v is None else v

        for record in ids:
            res_count = stock_in_hand.get(record) - forcasted_qty.get(record)
            if res_count < 0:
                res_count = 0
            res[record] = res_count

        return res

    def _get_saver_forcast_quantity(self, cr, uid, ids, name, arg, context=None):
        res = {}

        for id in ids:
            product = self.browse(cr, uid, id, context=context)
            qty = product.with_context({'location': 246}).virtual_available  ## In live 246
            res[id] = qty
        return res

    # Badda HORECA warehouse total quantity and all operation

    def _get_badda_horeca_warehouse_quantity(self, cr, uid, ids, name, arg, context=None):
        res = {}
        for record in ids:
            warehouse_quantity = 0

            quant_ids = self.pool.get('stock.quant').search(cr, uid, [('product_id', '=', record),
                                                                      ('location_id.usage', '=', 'internal')])
            quant_ids_obj = self.pool.get('stock.quant').browse(cr, uid, quant_ids, context=context)

            t_warehouses = {}
            for quant in quant_ids_obj:
                if quant.location_id:
                    if quant.location_id not in t_warehouses:
                        t_warehouses.update({quant.location_id: 0})
                    t_warehouses[quant.location_id] += quant.qty

            tt_warehouses = {}
            for location in t_warehouses:
                warehouse = False
                location1 = location

                while (not warehouse and location1):
                    warehouse_id = self.pool.get('stock.warehouse').search(cr, uid,
                                                                           [('lot_stock_id', '=', location1.id)])
                    if len(warehouse_id) > 0:
                        warehouse = True
                    else:
                        warehouse = False
                    location1 = location1.location_id
                if warehouse_id:
                    warehouse_id = self.pool.get('stock.warehouse').browse(cr, uid, warehouse_id, context=context)
                    if warehouse_id.name not in tt_warehouses:
                        tt_warehouses.update({warehouse_id.id: 0})

                    tt_warehouses[warehouse_id.id] += t_warehouses[location]

            for item in tt_warehouses:

                if item == 34:  ## In live 34
                    warehouse_quantity = tt_warehouses[item]

            res[record] = warehouse_quantity
        return res

    def _get_badda_horeca_free_quantity(self, cr, uid, ids, name, arg, context=None):
        res = {}

        loc_id = 228  ## In live 228

        stock_in_hand = self._get_badda_horeca_warehouse_quantity(cr, uid, ids, name, arg, context=None)
        forcasted_qty = self._get_fun_for_free_qty(cr, uid, ids, loc_id)
        for k, v in stock_in_hand.iteritems(): stock_in_hand[k] = 0 if v is None else v
        for k, v in forcasted_qty.iteritems(): forcasted_qty[k] = 0 if v is None else v

        for record in ids:
            res_count = stock_in_hand.get(record) - forcasted_qty.get(record)
            if res_count < 0:
                res_count = 0
            res[record] = res_count

        return res

    def _get_badda_horeca_forcast_quantity(self, cr, uid, ids, name, arg, context=None):
        res = {}

        for id in ids:
            product = self.browse(cr, uid, id, context=context)
            qty = product.with_context({'location': 228}).virtual_available  ## In live 186
            res[id] = qty
        return res

    _columns = {

        'warehouse_quantity': fields.function(_get_warehouse_quantity, string='Quantity per warehouse', type='char'),
        'uttwh_quantity': fields.function(_get_uttwh_warehouse_quantity, string='Uttara Quantity', type='float'),
        'nodda_quantity': fields.function(_get_nodda_warehouse_quantity, string='Nodda Quantity', type='float'),
        'moti_quantity': fields.function(_get_moti_warehouse_quantity, string='Motijheel Quantity', type='float'),
        'uttwh_forcast_quantity': fields.function(_get_uttwh_forcast_quantity, string='Uttara Outgoing Quantity',
                                                  type='float'),
        'nodda_forcast_quantity': fields.function(_get_nodda_forcast_quantity, string='Nodda Outgoing Quantity',
                                                  type='float'),
        'moti_forcast_quantity': fields.function(_get_moti_forcast_quantity, string='Motijheel Outgoing Quantity',
                                                 type='float'),
        'moti_free_quantity': fields.function(_get_moti_free_quantity, string='Motijheel Free Quantity', type='float'),
        'uttwh_free_quantity': fields.function(_get_uttwh_free_quantity, string='Uttara Free Quantity', type='float'),
        'nodda_free_quantity': fields.function(_get_nodda_free_quantity, string='Nodda Free Quantity', type='float'),

        'nodda_retail_quantity': fields.function(_get_nodda_reatil_warehouse_quantity, string='Nodda Retail Quantity',
                                                 type='float'),

        'nodda_retail_forcast_quantity': fields.function(_get_nodda_retail_forcast_quantity,
                                                         string='Nodda Retail Outgoing Qty', type='float'),

        'nodda_retail_free_quantity': fields.function(_get_nodda_retail_free_quantity, string='Nodda Retail Free Qty',
                                                      type='float'),

        'uttwh_retail_quantity': fields.function(_get_uttwh_reatil_warehouse_quantity, string='Uttara Retail Quantity',
                                                 type='float'),

        'uttwh_retail_forcast_quantity': fields.function(_get_uttwh_retail_forcast_quantity,
                                                         string='Uttara Retail Outgoing Qty', type='float'),

        'uttwh_retail_free_quantity': fields.function(_get_uttwh_retail_free_quantity, string='Uttara Retail Free Qty',
                                                      type='float'),

        'moti_retail_quantity': fields.function(_get_moti_retail_warehouse_quantity, string='Motijheel Retail Quantity',
                                                type='float'),

        'moti_retail_forcast_quantity': fields.function(_get_moti_retail_forcast_quantity,
                                                        string='Motijheel Retail Outgoing Qty', type='float'),

        'moti_retail_free_quantity': fields.function(_get_moti_retail_free_quantity, string='Motijheel Retail Free Qty',
                                                     type='float'),

        'damage_quantity': fields.function(_get_damage_warehouse_quantity, string='Damage Quantity', type='float'),

        'comilla_quantity': fields.function(_get_comilla_warehouse_quantity, string='Comilla Quantity', type='float'),

        'comilla_free_quantity': fields.function(_get_comilla_free_quantity, string='Comilla Free Quantity',
                                                 type='float'),

        'comilla_forcast_quantity': fields.function(_get_comilla_forcast_quantity, string='Comilla Outgoing Quantity',
                                                    type='float'),

        'signboard_quantity': fields.function(_get_signboard_warehouse_quantity, string='Signboard Quantity',
                                                type='float'),
        'signboard_forcast_quantity': fields.function(_get_signboard_forcast_quantity,
                                                        string='Signboard Outgoing Qty', type='float'),
        'signboard_free_quantity': fields.function(_get_signboard_free_quantity, string='Signboard Free Qty',
                                                     type='float'),
        'Badda_quantity': fields.function(_get_Badda_warehouse_quantity, string='Badda Quantity',
                                              type='float'),
        'Badda_forcast_quantity': fields.function(_get_Badda_forcast_quantity,
                                                      string='Badda Outgoing Qty', type='float'),
        'Badda_free_quantity': fields.function(_get_Badda_free_quantity, string='Badda Free Qty',
                                                   type='float'),

        'badda_horeca_quantity': fields.function(_get_badda_horeca_warehouse_quantity, string='Badda HORECA Quantity',
                                          type='float'),
        'badda_horeca_forcast_quantity': fields.function(_get_badda_horeca_forcast_quantity,
                                                  string='Badda HORECA Outgoing Qty', type='float'),
        'badda_horeca_free_quantity': fields.function(_get_badda_horeca_free_quantity, string='Badda HORECA Free Qty',
                                               type='float'),

        'saver_quantity': fields.function(_get_saver_warehouse_quantity, string='Savar Quantity',
                                          type='float'),
        'saver_forcast_quantity': fields.function(_get_saver_forcast_quantity,
                                                  string='Savar Outgoing Qty', type='float'),
        'saver_free_quantity': fields.function(_get_saver_free_quantity, string='Savar Free Qty',
                                               type='float'),

    }

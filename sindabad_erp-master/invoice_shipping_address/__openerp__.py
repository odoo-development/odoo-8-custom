{
    'name': 'Print Invoices report Shipping address fixing',
    'version': '8.0.0',
    'category': 'Sales',
    'description': """
Print Invoices report Shipping address fixing.  
=============================================

""",
    'author': 'Md Rockibul Alam Babu',
    'depends': ['packings','account'],
    'data': [
        'views/inh_account_invoice_report.xml',
    ],

    'installable': True,
    'auto_install': False,
}
import time
from openerp.report import report_sxw
from openerp.osv import osv


class batchpayslipdata(report_sxw.rml_parse):
    def __init__(self, cr, uid, name, context):
        super(batchpayslipdata, self).__init__(cr, uid, name, context)
        self.localcontext.update({
            'get_top_chart_data': self.get_top_chart_data,
            'get_department_wise_data': self.get_department_wise_data,
            'get_detail_data_list': self.get_detail_data_list,
        })



    def get_top_chart_data(self, obj):

        get_department_ids = []

        get_department_ids=[slip_items.contract_id.employee_id.department_id.id for slip_items in obj.slip_ids]

        get_department_ids= list(dict.fromkeys(get_department_ids))
        get_department_ids.sort()

        department_wise_value=[]
        gran_tmp_dict = {}
        total_actual_sal = 0
        total_benifits = 0
        total_payable = 0
        total_heads = 0

        for dept_id in get_department_ids:
            tmp_dict ={
                'department':'',
                'total_head_count':0.0,
                'actual_salary':0.0,
                'additional_benifit':0.0,
                'total_paybale':0.0,
            }
            head_count=0
            for slip_items in obj.slip_ids:
                if slip_items.contract_id.employee_id.department_id.id == dept_id:
                    head_count=head_count+1
                    tmp_dict['department'] =slip_items.contract_id.employee_id.department_id.name
                    tmp_dict['total_head_count'] =head_count
                    tmp_dict['actual_salary'] =  tmp_dict['actual_salary'] + 0
                    tmp_dict['additional_benifit'] = tmp_dict['additional_benifit'] + slip_items.contract_id.x_cash_allowance
                    net_salary=0
                    for detal_slip in slip_items.details_by_salary_rule_category:
                        if str(detal_slip.category_id.name) == 'Net':
                            net_salary = detal_slip.total
                            tmp_dict['actual_salary'] = tmp_dict['actual_salary'] + net_salary
                            break
                    tmp_dict['total_paybale'] = tmp_dict['actual_salary'] + tmp_dict['additional_benifit']

            total_actual_sal = total_actual_sal + tmp_dict['actual_salary']
            total_benifits = total_benifits + tmp_dict['additional_benifit']
            total_payable = total_payable + tmp_dict['total_paybale']
            total_heads = total_heads + head_count

            department_wise_value.append(tmp_dict)

        department_wise_value.append({'department':'Grand Total',
                'total_head_count':total_heads,
                'actual_salary':total_actual_sal,
                'additional_benifit':total_benifits,
                'total_paybale':total_actual_sal+total_benifits,})


        return department_wise_value


    def get_details_by_data(self, obj):
        payslip_line = self.pool.get('hr.payslip.line')
        rule_cate_obj = self.pool.get('hr.salary.rule.category')

        def get_recursive_parent(current_rule_category, rule_categories = None):
            if rule_categories:
                rule_categories = current_rule_category | rule_categories
            else:
                rule_categories = current_rule_category

            if current_rule_category.parent_id:
                return get_recursive_parent(current_rule_category.parent_id, rule_categories)
            else:
                return rule_categories

        res = []
        result = {}
        ids = []
        total_office_allowance = 0
        total_deduction = 0



        for id in range(len(obj)):
            ids.append(obj[id].id)
        if ids:
            self.cr.execute('''SELECT pl.id, pl.category_id FROM hr_payslip_line as pl \
                LEFT JOIN hr_salary_rule_category AS rc on (pl.category_id = rc.id) \
                WHERE pl.id in %s \
                GROUP BY rc.parent_id, pl.sequence, pl.id, pl.category_id \
                ORDER BY pl.sequence, rc.parent_id''',(tuple(ids),))
            for x in self.cr.fetchall():
                result.setdefault(x[1], [])
                result[x[1]].append(x[0])
            for key, value in result.iteritems():
                rule_categories = rule_cate_obj.browse(self.cr, self.uid, [key])
                parents = get_recursive_parent(rule_categories)

                category_total = 0
                for line in payslip_line.browse(self.cr, self.uid, value):
                    category_total += line.total
                level = 0
                for parent in parents:
                    res.append({
                        'rule_category': parent.name,
                        'name': parent.name,
                        'code': parent.code,
                        'level': level,
                        'total': category_total,
                    })
                    level += 1


                for line in payslip_line.browse(self.cr, self.uid, value):
                    res.append({
                        'rule_category': line.name,
                        'name': line.name,
                        'code': line.code,
                        'total': line.total,
                        'level': level
                    })

                    if rule_categories.parent_id.id == 2 :
                        total_office_allowance += line.total
                    elif rule_categories.id == 4 or rule_categories.parent_id.id == 4:
                        total_deduction += line.total

        gross_salary = 0.0
        net_salary = 0.0


        for items in res:
            if items.get('rule_category') == 'Gross':
                gross_salary = items.get('total')
            if items.get('rule_category') == 'Net':
                net_salary = items.get('total')
        final_result =[{
            'gross':gross_salary,
            'total_office_allowance':total_office_allowance,
            'total_deduction':total_deduction,
            'net_salary':net_salary
        }]


        return final_result




    def get_department_wise_data(self, obj):


        get_department_ids = []


        get_department_ids=[slip_items.contract_id.employee_id.department_id.id for slip_items in obj.slip_ids]

        get_department_ids= list(dict.fromkeys(get_department_ids))
        get_department_ids.sort()

        department_wise_value=[]



        return get_department_ids


    def get_detail_data_list(self, dep_id,obj):




        get_department_ids = [dep_id]



        department_wise_value=[]

        for dept_id in get_department_ids:
            tmp_list=[]
            tmp_dict ={
                'employee_id':'',
                'employee_name':'',
                'joining_date':'',
                'department_name':'',
                'designation':'',
                'gross_salary':0,
                'total_addition':0,
                'total_deduction':0,
                'net_apyable':0,
                'ac_no':'',

            }
            head_count=0
            for slip_items in obj.slip_ids:
                if slip_items.contract_id.employee_id.department_id.id == dept_id:
                    tmp_dict={}
                    head_count=head_count+1
                    tmp_dict['employee_id'] =slip_items.contract_id.employee_id.x_emid_sin
                    tmp_dict['employee_name'] =slip_items.contract_id.employee_id.name
                    tmp_dict['joining_date'] =slip_items.contract_id.date_start
                    tmp_dict['gross_salary'] = slip_items.contract_id.x_gross_cus
                    tmp_dict['department_name'] =slip_items.contract_id.employee_id.department_id.name
                    tmp_dict['designation'] =slip_items.contract_id.employee_id.job_id.name
                    tmp_dict['ac_no'] =slip_items.contract_id.employee_id.x_bankaccount_cus





                    result = self.get_details_by_data(slip_items.line_ids)

                    if len(result)>0:

                        tmp_dict['total_addition']=result[0].get('total_office_allowance')
                        tmp_dict['total_deduction']=result[0].get('total_deduction')
                        tmp_dict['net_apyable']=result[0].get('net_salary')
                    # import pdb
                    # pdb.set_trace()

                    department_wise_value.append(tmp_dict)





        return department_wise_value





class report_batch_payslip_layout(osv.AbstractModel):
    _name = 'report.payslip_report_batch.report_batch_payslip_layout'
    _inherit = 'report.abstract_report'
    _template = 'payslip_report_batch.report_batch_payslip_layout'
    _wrapped_report_class = batchpayslipdata
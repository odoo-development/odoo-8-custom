{
    'name': 'Product Old Sticker Format',
    'version': '8.0.0',
    'category': 'Sales',
    'description': "Product Old Sticker Format",
    'author': 'Mufti Muntasir Ahmed',
    'depends': ['product'],
    'data': [
        'sticker_format.xml',
        'menu.xml'

    ],
    'installable': True,
    'auto_install': False,
}

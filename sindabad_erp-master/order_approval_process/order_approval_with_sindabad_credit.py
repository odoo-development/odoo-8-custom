from datetime import datetime, timedelta
import time
from datetime import date
from openerp import SUPERUSER_ID
from openerp.osv import fields, osv
from openerp.tools.translate import _
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT
import openerp.addons.decimal_precision as dp
from openerp import workflow
from openerp import api
import datetime
from ..base.res.res_partner import format_address
from openerp.exceptions import Warning, except_orm


class res_partner(osv.Model, format_address):
    _inherit = "res.partner"

    _columns = {
        'credit_days': fields.integer('Credit Days'),
        'monthly_limit': fields.boolean('Monthly Limit')
    }


class magento_orders(osv.osv):
    _inherit = "magento.orders"

    def create(self, cr, uid, vals, context=None):

        warehouse_dict = {}

        wh_area = self.pool.get('warehouse.area')
        wh_area_search = wh_area.search(cr, uid, [('code', '=', 'NODDA')], context=context)
        wh_area_browse = wh_area.browse(cr, uid, wh_area_search, context=context)

        areas = wh_area_browse.areas

        nodda_list = areas.split(',')
        nodda_list = [a.lstrip() for a in nodda_list]

        """
        nodda_list = ["Hatirjheel", "Kadamtali", "Kalachadpur", "Meradia", "Purbachal", "Ramna", "Vatara", "Arambagh", "Babu Bazar", "Badda", "Bakshi Bazar", "Banani", "Banani Cantonment", "Banani Dohs", "Banasree", "Bangla Bazar", "Bangla Motor", "Bangshal", "Baridhara", "Baridhara Dohs", "Basabo", "Bashundhara R/A", "Chankharpool", "Chawk Bazar", "Dilkusha", "Dohar", "Eskaton", "Fulbaria", "Gendaria", "Goran", "Gulistan", "Gulshan 1", "Gulshan 2", "Hatirpool", "Islampur", "Jatrabari", "Jurain", "Kakrail", "Kamalapur", "Karwan Bazar", "Keraniganj", "Khilgaon", "Khilkhet", "Kuril", "Malibag", "Mitford", "Moghbazar", "Mohakhali", "Mohakhali Dohs", "Motijheel", "Mouchak", "Mugdapara", "Nadda", "Narinda", "Nawabganj", "Nawabpur", "Naya Bazar", "Niketon", "Nikunja-1", "Nikunja-2", "Nilkhet", "Noya Paltan", "Palashy", "Postagola", "Purana Paltan", "Rajarbag", "Rampura", "Sadarghat", "Sayedabad", "Segun Bagicha", "Shahbagh", "Shahjahanpur", "Shantibag", "Shantinagar", "Siddeswary", "Sutrapur", "Tejgaon", "Tejkunipara", "Tejturi Bazar", "Wari"]


        ["Badda", "Banani", "Banani Cantonment", "Banani Dohs", "Baridhara", "Baridhara Dohs",
                     "Bashundhara R/A", "Eskaton", "Firmgate", "Gulshan 1", "Gulshan 2", "Karwan Bazar", "Khilgaon",
                     "Khilkhet", "Moghbazar", "Mohakhali", "Mohakhali Dohs", "Niketon", "Nikunja-1", "Nikunja-2",
                     "Rampura", "Tejgaon", "Nadda", "Kuril",
                      "Babu Bazar", "Bakshi Bazar", "Banasree", "Bangla Bazar", "Bangla Motor", "Bangshal", "Bashabo", "Chankharpul", "Chawk Bazar", "Dilkusha", "Elephant Road", "Fulbaria", "Gandaria", "Goran", "Gulisthan", "Hatirjheel", "Hatirpool", "Jatrabari", "Jurain", "Kadamtali", "Kafrul", "Kakrail", "Kalachadpur", "Kamrangirchar", "Kawranbazar", "Komolapur", "Kotwali", "Kuril", "Lalbagh", "Malibagh", "Meradia", "Midfort", "Modhubag", "Mogbazar", "Mohakhali Kachabazar", "Motijheel", "Mouchak", "Mugda", "Narindda", "Nilkhet", "Noya Paltan", "Paltan", "Panthapath", "Polashi", "Postagola", "Puran Paltan", "Purbachal", "Rajarbagh", "Ramna", "Sabujbagh", "Saydabad", "Shadarghat", "Shahbagh", "Shahjahanpur", "Shantibagh", "Shantinagar", "Shegun Bagicha", "Sher e Bangla Nagar", "Shyampur", "Siddheswari", "Sutrapur", "Vatara", "Wari", "Arambagh", "Basabo"]





        motijheel_list = ["Basabo", "Dilkusha", "Gendaria", "Gulistan", "Kakrail", "Motijheel", "Noya Paltan", "Posta",
                        "Purana Paltan", "Wari", "Dohar", "Keraniganj", "Jatrabari", "Sayedabad","Araihazar","Baidder Bazar",
                          "Bandar","Fatullah","Narayanganj Sadar","Rupganj","Siddirganj","Shantinagar","Rajarbag","Shantibag","Malibag","Mogbazar","Mouchak", "Shahjahanpur", "Bangshal", "Hatirpool", "Shahbagh", "Bangla Motor", "Bakshi Bazar", "Chawk Bazar", "Babu Bazar", "Mitford", "Islampur", "Chankharpool", "Nawabpur", "Naya Bazar", "Bangla Bazar", "Sadarghat", "Narinda", "Jurain", "Sutrapur", "Postagola", "Islambagh", "Siddeswary", "Fulbaria", "Segun Bagicha", "Goran", "Banasree", "Mugdapara", "Kamalapur", "Arambagh"]
        """

        # nodda_list = ["Nikunja 2", "Kuril", "Basundhara R/A", "Jamuna Future Park", "Baridhara", "Gulshan 2",
        #               "Gulshan 1", "Karail", "Mohakhali", "Arjatpara", "Shahinbag", "Elenbari", "Tejgaon", "Farmgate",
        #               "Karwan Bazar", "Tejgaon I/A", "Niketan Housing Project", "South Badda", "Badda", "Ananda Nagar",
        #               "Tekpara", "Nama Para", "Bawaila Para", "East Nurerchala", "Khilkhet", "Kurmitola",
        #               "Progoti Sarani", "Rupnagar", "Merul Badda"]
        #
        # motijheel_list = ["Hatir Jheel", "Aftabnagar", "Rampura", "Mirbag", "Taltola", "Adarshabag", "Khilgaon",
        #                   "Nandipara", "Shiddheswari", "Shahidbag", "Bashabo", "Nutan Para", "Shabujbag", "Baganbari",
        #                   "Kadamtala", "Dhaka City College", "Shahbag", "Baily Road", "Ramna Park",
        #                   "University Of Dhaka", "Tarabo", "Motijheel", "Kamalapur", "Maniknagar", "Golapbag", "Demra",
        #                   "Konapara", "Shonir Akhra", "Matuail", "Rayerbag", "Sign Board", "Shiddhirganj",
        #                   "Saddam Market", "Mohammadbag", "Chanpara", "Shampur", "Jurain", "Muradpur", "Donia",
        #                   "Janata Market", "South Jatra Bari", "Jatra Bari", "Sayedabad", "Sutrapur", "Wari",
        #                   "Gandaria", "Sadarghat", "Faridabad", "Nazirabag", "Kaliganj", "Keraniganj", "Malitola",
        #                   "Mitfort", "Islambag", "Lalbag", "Kamrangirchar", "Nawab Ganj", "Company Ghat", "Moneshwar",
        #                   "Dhaka New Market", "Ekuria", "Dholeshshor", "Zazira", "Kundar Char", "Pagla", "Munshibag",
        #                   "Bhuigor", "Mizmizi", "Jalkuray", "Adamjee", "Geneva Camp", "Kadamtoli", "Fatullah",
        #                   "Narayangonj", "Bandar", "Godenail"]

        warehouse_dict[2] = nodda_list  # Here key is the nodda warehouse ID
        # warehouse_dict[4] = motijheel_list  # Here key is the Motijeel warehouse ID



        if context is None:
            context = {}
        if context.has_key('instance_id'):
            vals['instance_id'] = context.get('instance_id')

        # ----------------------------------------------
        try:
            found_retail = False
            odoo_order_id = vals['order_ref']
            order_info = self.pool.get('sale.order').browse(cr, uid, [odoo_order_id], context=context)
            partner_classification = str(order_info.partner_id.x_classification)
            wh_id = None
            # The following code for Area wise Warhouse change Auto
            for k, v in warehouse_dict.iteritems():

                state_name = order_info.partner_shipping_id.state_id.name
                for item in v:
                    if state_name.upper() == item.upper():
                        wh_id = k
                        break
                if wh_id is not None:
                    break
            """
            if 'Retail' in str(partner_classification) or "SOHO" in str(partner_classification):
                found_retail = True
                if wh_id == 2:
                    wh_id=3
                elif wh_id == 4:
                    wh_id = 3
                else:
                    wh_id = 6
            """
            # if product contains name "Airtel or Robi"
            for p in order_info.order_line:
                if "Airtel" in p.name:
                    wh_id = 9
                    break

            if wh_id is not None:
                update_so_query_area = "UPDATE sale_order SET warehouse_id='{0}' WHERE id='{1}'".format(
                    wh_id, str(odoo_order_id))
                cr.execute(update_so_query_area)
                cr.commit()

            ### Ends here area wise warehouse selection



            """
            For now x_credit will be checked. But order_ref will be checked when the credit limit will come from magento.
            """

            if vals.get('order_ref') and order_info.x_prepaid == False and found_retail == False:

                if order_info.partner_id.parent_id.is_company == True:
                    company_id = order_info.partner_id.parent_id.id
                else:
                    company_id = order_info.partner_id.id



                    # if order_info.x_credit:
                customer_info = self.pool.get('res.partner').browse(cr, uid, [company_id], context=context)

                cust_so_count_query = "SELECT COUNT(id) FROM sale_order WHERE partner_id='{0}'".format(
                    str(order_info.partner_id.id))

                cr.execute(cust_so_count_query)
                cust_so_count = 0
                for c in cr.fetchall():
                    cust_so_count = int(c[0])

                new_customer = False
                credit_limit_exceed = False
                state = 'draft'
                if cust_so_count > 1:
                    new_customer = False
                else:
                    new_customer = True

                if customer_info.credit_limit > 0:
                    total_receivable_amount = customer_info.property_account_receivable.balance + order_info.amount_total

                    if total_receivable_amount > customer_info.credit_limit:
                        credit_limit_exceed = True
                        state = 'approval_pending'

                if new_customer and state == 'approval_pending':
                    state = 'approval_pending'
                elif new_customer and state == 'draft':
                    state = 'new_cus_approval_pending'

                # Next Month Validation




                """
                if customer_info.monthly_limit is True and customer_info.credit_days > 0:
                    credit_days = (customer_info.credit_days %30) if customer_info.credit_days > 30 else customer_info.credit_days

                    currentMonth = datetime.datetime.now().month
                    currentYear = datetime.datetime.now().year

                    make_date = str(credit_days) + '-'+ str(currentMonth)+ '-'+str(currentYear)

                    last_limit_date = datetime.datetime.strptime(make_date, "%d-%m-%Y").strftime("%d-%m-%Y")
                    last_limit_date = datetime.datetime.strptime(last_limit_date, '%d-%m-%Y').date()

                    today_date = datetime.date.today()

                    if today_date > last_limit_date:
                        credit_days_exceed = True
                        state = 'approval_pending'
                    else:
                        credit_days_exceed = False


                # credit days limit check
                el
                """
                if customer_info.credit_days > 0:
                    # partner_id = str(order_info.partner_id.id)

                    # handling credit days for all child
                    mother_customer = False
                    if not customer_info.parent_id.id:
                        # mother_customer = True

                        # call method to set credit days for all child
                        # call with parent_id
                        parent_id = str(customer_info.id)

                        credit_days = customer_info.credit_days

                        self._set_credit_days(cr, parent_id, credit_days)
                        # check if credit days exceed
                        credit_days_exceed = self._check_credit_days_exceed(cr, uid, parent_id, context)

                    else:
                        # get the parent_id and set credit days to all child
                        # mother_customer = False
                        parent_id = str(order_info.partner_id.id)

                        parent_customer_info = self.pool.get('res.partner').browse(cr, uid, [int(parent_id)],
                                                                                   context=context)

                        self._set_credit_days(cr, parent_id, parent_customer_info.credit_days)
                        # check if credit days exceed
                        credit_days_exceed = self._check_credit_days_exceed(cr, uid, parent_id, context)
                    if credit_days_exceed:
                        state = 'approval_pending'

                # if "Robi or Airtel" state will be "draft"
                for p in order_info.order_line:
                    if "Airtel" in p.name:
                        state = "draft"
                        break

                try:
                    if "Ananta" not in str(partner_classification) and "Corporate" not in str(
                            partner_classification) and "Retail" not in str(partner_classification):
                        new_customer = False
                        state = "draft"
                except:
                    pass

                try:
                    if order_info.payment_code == 'ipdc':
                        new_customer = False
                        state = "draft"
                except:
                    pass

                try:
                    if credit_limit_exceed:
                        # send SMS & mail
                        parent = order_info.partner_id

                        for i in range(5):
                            if len(parent.parent_id) == 1:
                                parent = parent.parent_id
                            else:
                                parent = parent

                                break

                        credit_amount = str(parent.credit_limit)
                        phone_number = parent.mobile

                        if not phone_number:
                            phone_number = str(parent.phone)

                        email = str(parent.email)

                        sms_text = ''
                        if phone_number:
                            sms_text = "Dear Valued Customer,\nYour credit amount '{0}' has exceed the limit. Please clear the previous dues so that process the order. For further details, please contact your relationship manager.".format(
                                credit_amount)

                            self.pool.get("send.sms.on.demand").send_sms_on_demand(cr, uid, [order_info.id], sms_text,
                                                                                   phone_number, str(parent.name),
                                                                                   context=context)

                        # send email
                        if email:
                            e_log_obj = self.pool.get('send.email.on.demand.log')

                            email_data = {
                                'mail_text': sms_text,
                                'model': "sale.order",
                                'so_id': order_info.id,
                                'email': email
                            }

                            email_log_id = e_log_obj.create(cr, uid, email_data, context=context)
                            # tmp_data = e_log_obj.browse(cr, uid, email_log_id, context=context)

                            # send email
                            email_template_obj = self.pool.get('email.template')
                            template_ids = email_template_obj.search(cr, uid, [('name', '=', 'SO process mail')],
                                                                     context=context)

                            email_template_obj.send_mail(cr, uid, template_ids[0], email_log_id, force_send=True,
                                                         context=context)

                except:
                    pass

                if "Sindabad Credit" in order_info.note:
                    update_so_query = "UPDATE sale_order SET new_customer='{0}', credit_limit_exceed='{1}', state='{2}' WHERE id='{3}'".format(
                        new_customer, credit_limit_exceed, state, str(odoo_order_id))
                    cr.execute(update_so_query)
                    cr.commit()
        except:
            pass
        # ----------------------------------------------

        return super(magento_orders, self).create(cr, uid, vals, context=context)

    def _set_credit_days(self, cr, parent_id, credit_days):

        partner_id_list = list()

        try:
            all_partner_id_query = "SELECT id from res_partner WHERE parent_id='{0}'".format(parent_id)
            cr.execute(all_partner_id_query)
            for c_id in cr.fetchall():
                partner_id_list.append(c_id[0])

            for p_id in partner_id_list:
                update_credit_days_query = "UPDATE res_partner SET credit_days='{0}' WHERE id='{1}'".format(credit_days,
                                                                                                            p_id)
                cr.execute(update_credit_days_query)
                cr.commit()
        except:
            pass

        return True

    def _check_credit_days_exceed(self, cr, uid, parent_id, context=None):

        credit_days_exceed = False

        partner_id_list = list()

        try:

            all_partner_id_query = "SELECT id from res_partner WHERE parent_id='{0}'".format(parent_id)
            cr.execute(all_partner_id_query)
            for c_id in cr.fetchall():
                partner_id_list.append(c_id[0])

            partner_id_str = ''

            if len(partner_id_list) > 0:
                for p_id in partner_id_list:
                    partner_id_str += str(p_id) + ", "

                partner_id_str = partner_id_str[:-2]

                # SELECT date_invoice FROM account_invoice WHERE partner_id IN ('4746', '9714', '11783', '11768', '11766', '11781', '11767', '11782') and state='open' ORDER BY date_invoice LIMIT 1
                first_inv_date = ''
                first_inv_date_query = "SELECT date_invoice FROM account_invoice WHERE partner_id IN ({0}) and state='open' ORDER BY date_invoice LIMIT 1".format(
                    partner_id_str)

                cr.execute(first_inv_date_query)
                for fiv in cr.fetchall():
                    first_inv_date = str(fiv[0])

                parent_customer_info = self.pool.get('res.partner').browse(cr, uid, [int(parent_id)], context=context)
                credit_days = int(parent_customer_info.credit_days)

                today_date = datetime.date.today()

                date_1 = datetime.datetime.strptime(first_inv_date, '%Y-%m-%d')
                limit_date = date_1 + datetime.timedelta(days=credit_days)

                if today_date > limit_date.date():
                    credit_days_exceed = True
                else:
                    credit_days_exceed = False
        except:
            pass

        return credit_days_exceed


class sale_order(osv.osv):
    _inherit = 'sale.order'

    _columns = {
        'credit_limit_exceed': fields.boolean('Credit Limit Exceed'),
        'new_customer': fields.boolean('New Customer'),
        'new_customer_approve_date': fields.datetime('New Customer Approve Date'),
        'new_customer_approve_by': fields.many2one('res.users', 'New Customer Approve By'),
        'new_customer_approve_reason': fields.text('New Customer Approve Reason'),
        'new_customer_cancel_date': fields.datetime('New Customer Cancel Date'),
        'new_customer_cancel_by': fields.many2one('res.users', 'New Customer Cancel By'),
        'new_customer_cancel_reason': fields.text('New Customer Cancel Reason'),
        'cl_approve_date': fields.datetime('CL Approve Date'),
        'cl_approve_by': fields.many2one('res.users', 'CL Approve By'),
        'cl_approve_reason': fields.text('CL Approve Reason'),

        'check_pending_approve_date': fields.datetime('CL Pending Approve Date'),
        'check_pending_approve_by': fields.many2one('res.users', 'CL Pending Approve By'),
        'check_pending_approve_reason': fields.text('CL Approve Reason'),
        'check_pending_cancel_date': fields.datetime('CL Pending Cancel Date'),
        'check_pending_cancel_by': fields.many2one('res.users', 'CL Pending Cancel By'),
        'check_pending_cancel_reason': fields.text('CL Cancel Reason'),

        'cl_cancel_date': fields.datetime('CL Cancel Date'),
        'cl_cancel_by': fields.many2one('res.users', 'CL Cancel By'),
        'cl_cancel_reason': fields.text('CL Cancel Reason'),
        'credit_days_exceed': fields.boolean('Credit Days Exceed'),
        'state': fields.selection([
            ('approval_pending', 'Approval Pending'),
            ('check_pending', 'Check Pending'),
            ('new_cus_approval_pending', 'New Customer Approval Pending'),
            ('draft', 'Draft Quotation'),
            ('sent', 'Quotation Sent'),
            ('cancel', 'Cancelled'),
            ('waiting_date', 'Waiting Schedule'),
            ('progress', 'Sales Order'),
            ('manual', 'Sale to Invoice'),
            ('shipping_except', 'Shipping Exception'),
            ('invoice_except', 'Invoice Exception'),
            ('done', 'Done'),
        ], 'Status', readonly=True, copy=False, help="Gives the status of the quotation or sales order.\
                  \nThe exception status is automatically set when a cancel operation occurs \
                  in the invoice validation (Invoice Exception) or in the picking list process (Shipping Exception).\nThe 'Waiting Schedule' status is set when the invoice is confirmed\
                   but waiting for the scheduler to run on the order date.", select=True),

    }


# ------------------- CREDIT LIMIT EXCEED ---------------------
class approve_limit_exceed(osv.osv):
    _name = "approve.limit.exceed"
    _description = "Approve Limit Exceed"

    _columns = {

        'approve_date': fields.datetime('CL Approve Date'),
        # 'approve_date': date.today().strftime('%Y-%m-%d'),
        'approve_by': fields.many2one('res.users', 'CL Approve By'),
        'approve_reason': fields.text('CL Approve Reason'),

    }

    def so_approve_limit_exceed(self, cr, uid, ids, context=None):

        approve_date = context['approve_date']
        approve_by = uid
        approve_reason = context['approve_reason']
        ids = context['active_ids']  # [15526]

        for id in ids:

            try:

                # cancel_approve_query = "UPDATE sale_order SET cl_approve_date='{0}', cl_approve_by='{1}', cl_approve_reason='{2}', state='draft' WHERE id='{3}'".format(approve_date, approve_by, approve_reason, id)
                approve_query = "UPDATE sale_order SET cl_approve_date='{0}', cl_approve_by='{1}', cl_approve_reason='{2}', state='check_pending' WHERE id='{3}'".format(
                    approve_date, approve_by, approve_reason, id)
                cr.execute(approve_query)
                cr.commit()

            except:

                pass

        return True


class cancel_limit_exceed(osv.osv):
    _name = "cancel.limit.exceed"
    _description = "Cancel Limit Exceed"

    _columns = {

        'cancel_date': fields.datetime('CL Cancel Date'),
        # 'cancel_date': date.today().strftime('%Y-%m-%d'),
        'cancel_by': fields.many2one('res.users', 'CL Cancel By'),
        'cancel_reason': fields.selection([

            ("product_unavailability", "Product unavailability"),
            ("delay_delivery", "Delay delivery"),
            ("pricing_issue", "Pricing issue"),
            ("wrong_order", "Wrong order"),
            ("cash_problem", "Cash problem"),
            ("damage_product", "Damage product"),
            ("double_ordered", "Double ordered"),
            ("sample_not_approved", "Sample not approved"),
            ("wrong_sku", "Wrong SKU"),
            ("not_interested", "Not interested"),
            ("wrong_product", "Wrong product"),
            ("quality_issue", "Quality issue"),
            ("test_order", "Test Order"),
            ("payment_not_done", "Payment not done"),
            ("apnd", "Advance payment not done"),
            ("invalid_information", "Invalid Information"),
            ("customer_unreachable", "Customer unreachable"),
            ("same_day_delivery", "Same day delivery"),
            ("fake_order", "Fake Order"),
            ("sourcing_delay", "Sourcing Delay"),
            ("reorder", "Reorder"),
            ("discount_issue", "Discount Issue"),
            ("product_not_available_customer_will_take_alternative_product",
             "Product not available, Customer will take alternative product"),

        ], 'CL Cancel Reason', readonly=True, copy=False, help="Reasons", select=True)

    }

    def so_cancel_limit_exceed(self, cr, uid, ids, context=None):

        cancel_date = context['cancel_date']
        cancel_by = uid
        cancel_reason = context['cancel_reason']
        ids = context['active_ids']

        for id in ids:

            try:
                if cancel_reason:

                    cancel_approve_query = "UPDATE sale_order SET cl_cancel_date='{0}', cl_cancel_by='{1}', cl_cancel_reason='{2}' WHERE id='{3}'".format(
                        cancel_date, cancel_by, cancel_reason, id)
                    cr.execute(cancel_approve_query)
                    cr.commit()
                    self.pool.get('sale.order').action_cancel(cr, uid, [id], context=context)
                else:
                    raise except_orm('Cancel Reason', 'Can not submit blank reason!!!')

            except:

                pass

        return True


# ------------------- CREDIT LIMIT EXCEED CHECK PENDING ---------------------
class approve_limit_exceed_check_pending(osv.osv):
    _name = "approve.limit.exceed.check.pending"
    _description = "Approve Limit Exceed Check Pending"

    _columns = {

        'approve_date': fields.datetime('CL Approve Date'),
        # 'approve_date': date.today().strftime('%Y-%m-%d'),
        'approve_by': fields.many2one('res.users', 'CL Approve By'),
        'approve_reason': fields.text('CL Approve Reason'),

    }

    _defaults = {
        'approve_date': fields.datetime.now(),

    }

    def so_approve_limit_exceed_check_pending(self, cr, uid, ids, context=None):
        approve_date = context['approve_date']
        approve_by = uid
        approve_reason = context['approve_reason']
        ids = context['active_ids']  # [15526]

        for id in ids:

            try:
                # 'check_pending_approve_date': fields.datetime('CL Pending Approve Date'),
                # 'check_pending_approve_by': fields.many2one('res.users', 'CL Pending Approve By'),
                # 'check_pending_approve_reason': fields.text('CL Approve Reason'),

                approve_query = "UPDATE sale_order SET check_pending_approve_date='{0}', check_pending_approve_by='{1}', check_pending_approve_reason='{2}', state='draft' WHERE id='{3}'".format(
                    approve_date, approve_by, approve_reason, id)
                cr.execute(approve_query)
                cr.commit()

            except:

                pass

        return True


class cancel_limit_exceed_check_pending(osv.osv):
    _name = "cancel.limit.exceed.check.pending"
    _description = "Cancel Limit Exceed Check Pending"

    _columns = {

        'cancel_date': fields.datetime('CL Cancel Date'),
        # 'cancel_date': date.today().strftime('%Y-%m-%d'),
        'cancel_by': fields.many2one('res.users', 'CL Cancel By'),
        'cancel_reason': fields.selection([

            ("product_unavailability", "Product unavailability"),
            ("delay_delivery", "Delay delivery"),
            ("pricing_issue", "Pricing issue"),
            ("wrong_order", "Wrong order"),
            ("cash_problem", "Cash problem"),
            ("damage_product", "Damage product"),
            ("double_ordered", "Double ordered"),
            ("sample_not_approved", "Sample not approved"),
            ("wrong_sku", "Wrong SKU"),
            ("not_interested", "Not interested"),
            ("wrong_product", "Wrong product"),
            ("quality_issue", "Quality issue"),
            ("test_order", "Test Order"),
            ("payment_not_done", "Payment not done"),
            ("apnd", "Advance payment not done"),
            ("invalid_information", "Invalid Information"),
            ("customer_unreachable", "Customer unreachable"),
            ("same_day_delivery", "Same day delivery"),
            ("fake_order", "Fake Order"),
            ("sourcing_delay", "Sourcing Delay"),
            ("reorder", "Reorder"),
            ("discount_issue", "Discount Issue"),
            ("product_not_available_customer_will_take_alternative_product",
             "Product not available, Customer will take alternative product"),

        ], 'CL Cancel Reason', readonly=True, copy=False, help="Reasons", select=True)

    }

    _defaults = {
        'cancel_date': fields.datetime.now(),
    }

    def so_cancel_limit_exceed_check_pending(self, cr, uid, ids, context=None):

        cancel_date = context['cancel_date']
        cancel_by = uid
        cancel_reason = context['cancel_reason']
        ids = context['active_ids']

        for id in ids:

            try:

                if cancel_reason:
                    """
                    'check_pending_cancel_date': fields.datetime('CL Pending Cancel Date'),
                    'check_pending_cancel_by': fields.many2one('res.users', 'CL Pending Cancel By'),
                    'check_pending_cancel_reason': fields.text('CL Cancel Reason'),
                    """
                    cancel_query = "UPDATE sale_order SET check_pending_cancel_date='{0}', check_pending_cancel_by='{1}', check_pending_cancel_reason='{2}' WHERE id='{3}'".format(
                        cancel_date, cancel_by, cancel_reason, id)
                    cr.execute(cancel_query)
                    cr.commit()
                    self.pool.get('sale.order').action_cancel(cr, uid, [id], context=context)
                else:
                    raise except_orm('Cancel Reason', 'Can not submit blank reason!!!')

            except:

                pass

        return True


# ------------------- NEW CUSTOMER ---------------------
class approve_new_customer(osv.osv):
    _name = "approve.new.customer"
    _description = "Approve New Customer"

    _columns = {

        'approve_date': fields.datetime('New Customer Approve Date'),
        # 'approve_date': date.today().strftime('%Y-%m-%d'),
        'approve_by': fields.many2one('res.users', 'New Customer Approve By'),
        'approve_reason': fields.text('New Customer Approve Reason'),

    }

    def so_approve_new_customer(self, cr, uid, ids, context=None):

        approve_date = context['approve_date']
        approve_by = uid
        approve_reason = context['approve_reason']
        ids = context['active_ids']  # [15526]

        for id in ids:

            try:

                cancel_approve_query = "UPDATE sale_order SET new_customer_approve_date='{0}', new_customer_approve_by='{1}', new_customer_approve_reason='{2}', state='draft' WHERE id='{3}'".format(
                    approve_date, approve_by, approve_reason, id)
                cr.execute(cancel_approve_query)
                cr.commit()

            except:

                pass

        return True


class cancel_new_customer(osv.osv):
    _name = "cancel.new.customer"
    _description = "Cancel New Customer"

    _columns = {

        'cancel_date': fields.datetime('CL Cancel Date'),
        # 'cancel_date': date.today().strftime('%Y-%m-%d'),
        'cancel_by': fields.many2one('res.users', 'CL Cancel By'),
        'cancel_reason': fields.selection([

            ("product_unavailability", "Product unavailability"),
            ("delay_delivery", "Delay delivery"),
            ("pricing_issue", "Pricing issue"),
            ("wrong_order", "Wrong order"),
            ("cash_problem", "Cash problem"),
            ("damage_product", "Damage product"),
            ("double_ordered", "Double ordered"),
            ("sample_not_approved", "Sample not approved"),
            ("wrong_sku", "Wrong SKU"),
            ("not_interested", "Not interested"),
            ("wrong_product", "Wrong product"),
            ("quality_issue", "Quality issue"),
            ("test_order", "Test Order"),
            ("payment_not_done", "Payment not done"),
            ("apnd", "Advance payment not done"),
            ("invalid_information", "Invalid Information"),
            ("customer_unreachable", "Customer unreachable"),
            ("same_day_delivery", "Same day delivery"),
            ("fake_order", "Fake Order"),
            ("sourcing_delay", "Sourcing Delay"),
            ("reorder", "Reorder"),
            ("discount_issue", "Discount Issue"),
            ("product_not_available_customer_will_take_alternative_product",
             "Product not available, Customer will take alternative product"),

        ], 'CL Cancel Reason', readonly=True, copy=False, help="Reasons", select=True)

    }

    def so_cancel_new_customer(self, cr, uid, ids, context=None):

        cancel_date = context['cancel_date']
        cancel_by = uid
        cancel_reason = context['cancel_reason']
        ids = context['active_ids']

        for id in ids:

            try:
                if cancel_reason:

                    cancel_approve_query = "UPDATE sale_order SET new_customer_cancel_date='{0}', new_customer_cancel_by='{1}', new_customer_cancel_reason='{2}' WHERE id='{3}'".format(
                        cancel_date, cancel_by, cancel_reason, id)
                    cr.execute(cancel_approve_query)
                    cr.commit()
                    self.pool.get('sale.order').action_cancel(cr, uid, [id], context=context)
                else:
                    raise except_orm('Cancel Reason', 'Can not submit blank reason!!!')

            except:

                pass

        return True

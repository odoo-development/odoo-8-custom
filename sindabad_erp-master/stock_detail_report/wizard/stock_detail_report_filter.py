from openerp import models, fields, api
import datetime


class StockDetail(models.Model):
    _name = 'stock.detail'

    from_date = fields.Datetime(string="From Date")
    to_date = fields.Datetime(string="End Date")
    warehouse_id = fields.Many2one('stock.warehouse', String="Warehouse")

    _defaults = {
        'from_date': datetime.datetime.today(),
        'to_date': datetime.datetime.today()
    }


    @api.model
    def compute_ageing(self, data):
        rec = self.browse(data)
        data = {}
        data['form'] = rec.read(['from_date'])[0]
        return self.env['report'].get_action(rec, 'stock_detail_report.report_stock_detail',
                                             data=data)


from openerp import models, api
from datetime import datetime


class StockDetailReport(models.AbstractModel):
    _name = 'report.stock_detail_report.report_stock_detail'

    def get_pickings(self, docs):
        pickings = []
        from_date = docs.from_date
        to_date = docs.to_date


        if docs:

            internal_location_env = self.pool.get('stock.location')
            internal_location_ids = internal_location_env.search(self._cr, self._uid,[('usage', 'like', 'internal')])

            ## Start The query to get the total balance
            opening_balance_product_id=[]

            self._cr.execute("select sum(stock_move.product_uom_qty) as total, stock_move.product_id from stock_move where stock_move.state='done' and stock_move.date <%s  and stock_move.location_dest_id in (select stock_location.id from stock_location where usage!='internal') group by stock_move.product_id ", (from_date,))
            all_out_product_qty = self._cr.fetchall()

            self._cr.execute(
                "select sum(stock_move.product_uom_qty) as total, stock_move.product_id from stock_move where stock_move.state='done' and stock_move.date <%s  and stock_move.location_dest_id in (select stock_location.id from stock_location where usage='internal') group by stock_move.product_id ", (from_date,))
            all_in_product_qty = self._cr.fetchall()

            opening_balance_dict = {}
            exists_in_opening_p_ids=[]

            for item in all_in_product_qty:
                not_found=0
                for item_out in all_out_product_qty:
                    if item[1] == item_out[1]:
                        if (item[0]-item_out[0]) >0:
                            opening_balance_dict[item[1]]= {
                                    'op_bal':abs(item[0]-item_out[0]),
                                    'grn_recv':0,
                                    'delivered':0,
                                    'adjustment':0,
                                    'adjustment_out': 0,
                                    'cb':0,
                                                            }
                            not_found=1
                            exists_in_opening_p_ids.append(item[1])
                        break
                if not_found==0 and item[0]>0:
                    opening_balance_dict[item[1]] = {
                                    'op_bal':abs(item[0]),
                                    'grn_recv':0,
                                    'delivered':0,
                                    'adjustment':0,
                                    'adjustment_out': 0,
                                    'cb':0
                                    }
                exists_in_opening_p_ids.append(item[1])






            ## Ends Here The Query



            ### All Bucket  Value With Start and End Date

            self._cr.execute("select sum(stock_move.product_uom_qty) as total, stock_move.product_id from stock_move,stock_picking where stock_move.state='done' and stock_move.date >%s and stock_move.date <=%s and stock_move.picking_id=stock_picking.id and stock_move.location_dest_id in (select stock_location.id from stock_location where usage='internal') group by stock_move.product_id" ,(from_date,to_date))
            all_grn_value = self._cr.fetchall()


            self._cr.execute(
                "select sum(stock_move.product_uom_qty) as total, stock_move.product_id from stock_move,stock_picking where stock_move.state='done' and stock_move.date >%s and stock_move.date <=%s and stock_move.picking_id=stock_picking.id and stock_move.location_dest_id in (select stock_location.id from stock_location where usage!='internal') group by stock_move.product_id",(from_date,to_date))
            all_delivered_value = self._cr.fetchall()

            self._cr.execute(
                "select sum(stock_move.product_uom_qty) as total, stock_move.product_id from stock_move where stock_move.state='done' and stock_move.date >%s and stock_move.date <=%s and stock_move.picking_id is NULL and stock_move.location_dest_id in (select stock_location.id from stock_location where usage='internal') group by stock_move.product_id",(from_date,to_date))
            all_in_adjusted_value = self._cr.fetchall()

            self._cr.execute(
                "select sum(stock_move.product_uom_qty) as total, stock_move.product_id from stock_move where stock_move.state='done' and stock_move.date >%s and stock_move.date <=%s and stock_move.picking_id is NULL and stock_move.location_dest_id in (select stock_location.id from stock_location where usage!='internal') group by stock_move.product_id",(from_date,to_date))
            all_out_adjusted_value = self._cr.fetchall()


            for g_item in all_grn_value:
                try:
                    opening_balance_dict[g_item[1]]['grn_recv']=g_item[0]

                except:

                    opening_balance_dict[g_item[1]] = {
                        'op_bal': 0,
                        'grn_recv': abs(g_item[0]),
                        'delivered': 0,
                        'adjustment': 0,
                        'adjustment_out': 0,
                        'cb': 0
                    }
                exists_in_opening_p_ids.append(g_item[1])



            for d_item in all_delivered_value:
                try:
                    opening_balance_dict[d_item[1]]['delivered']=d_item[0]
                except:
                    opening_balance_dict[d_item[1]] = {
                        'op_bal': 0,
                        'grn_recv': 0,
                        'delivered': abs(d_item[0]),
                        'adjustment': 0,
                        'adjustment_out': 0,
                        'cb': 0
                    }
                    exists_in_opening_p_ids.append(d_item[1])

            for in_item in all_in_adjusted_value:
                try:
                    opening_balance_dict[in_item[1]]['adjustment']=in_item[0]
                except:
                    opening_balance_dict[in_item[1]] = {
                        'op_bal': 0,
                        'grn_recv': 0,
                        'delivered': 0,
                        'adjustment': abs(in_item[0]),
                        'adjustment_out':0,
                        'cb': 0
                    }
                    exists_in_opening_p_ids.append(in_item[1])

            for out_item in all_out_adjusted_value:
                try:
                    opening_balance_dict[out_item[1]]['adjustment_out'] = out_item[0]
                except:
                    opening_balance_dict[out_item[1]] = {
                        'op_bal': 0,
                        'grn_recv': 0,
                        'delivered': 0,
                        'adjustment': 0,
                        'adjustment_out': abs(out_item[0]),
                        'cb': 0
                    }
                    exists_in_opening_p_ids.append(out_item[1])



            ## Ends Here

            product_id_list=[]

            for k, v in opening_balance_dict.iteritems():
                product_id_list.append(k)


            context={}

            for key, vals in opening_balance_dict.iteritems():
                product = self.pool['product.product'].browse(self._cr, self._uid, int(key), context=context)
                total_cb_balance = vals.get('op_bal',0)+vals.get('grn_recv',0)+vals.get('adjustment',0)-vals.get('delivered',0)-vals.get('adjustment_out',0)

                pickings.append({
                    'product_name':product.name,
                    'product_sku':product.default_code,
                    'prev_bal':vals.get('op_bal',0),
                    'grn_recv':vals.get('grn_recv',0),
                    'delivered':vals.get('delivered',0),
                    'adjustment':vals.get('adjustment',0),
                    'adjustment_out':vals.get('adjustment_out',0),
                    'cb':total_cb_balance,
                })

                # import pdb
                # pdb.set_trace()

        return pickings

    @api.model
    def render_html(self, docids, data=None):

        self.model = self.env.context.get('active_model')
        docs = self.env[self.model].browse(self.env.context.get('active_id'))
        pickings = self.get_pickings(docs)
        docargs = {
            'doc_ids': self.ids,
            'doc_model': self.model,
            'docs': docs,
            'pickings': pickings,
        }
        return self.env['report'].render('stock_detail_report.report_stock_detail', docargs)

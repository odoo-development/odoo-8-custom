from openerp.osv import fields, osv
from openerp import SUPERUSER_ID, api
from openerp.exceptions import except_orm, Warning, RedirectWarning
from openerp.tools.translate import _


class rtv_process_line(osv.osv):
    _inherit = "rtv.process.line"


    _columns = {
        'po_id': fields.many2one('purchase.order', 'PO Number',domain="[('state','=','done')]"),
        'po_invoice_id': fields.many2one('account.invoice', 'Invoice Number'),
        'po_qty': fields.float('PO Qty'),
        'po_updated_qty': fields.boolean('PO Updated QTY'),
    }

    def onchange_purchase_id(self, cr, uid, ids, po_id=False, context=None):
        rtv_process_line = []
        warehouse_id = context.get('warehouse_id')
        picking_type = self.pool['stock.picking.type'].search(cr, uid,
                                                              [('warehouse_id', '=', warehouse_id)], context=context)
        picking_type_id = self.pool['purchase.order'].search(cr, uid,
                                                             [('picking_type_id', '=', picking_type)], context=context)

        for record in self.pool['purchase.order'].browse(cr, uid, po_id, context=context):
            if record.id in picking_type_id:
                for inv in record.invoice_ids:
                    rtv_process_line.append(inv.id)
                return {'domain': {'po_invoice_id': [('id', '=', rtv_process_line)]}}
            else:
                raise except_orm(_('PO not found!'), _("This PO is not in selected W/H PO"))
        return True


    def onchange_invoice_id(self, cr, uid, ids, po_invoice_id=False, context=None):
            rtv_process_line = []
            if po_invoice_id:
                for record in self.pool['account.invoice'].browse(cr, uid, po_invoice_id, context=context):
                    for inv in record.invoice_line:
                        rtv_process_line.append(inv.product_id.id)

                return {'domain': {'product_id': [('id', '=', rtv_process_line)]}}


class rtv_process(osv.osv):
    _inherit = "rtv.process"



    def _process_rtv_po_qty(self, po_id,po_invoice_id,product_id,po_quantity,requested_qty):

        quantity = 0.0
        rtv_obj = self.pool.get('rtv.process.line').browse(self._cr, self._uid,self.pool['rtv.process.line'].search(
            self._cr, self._uid,([('po_id', '=', po_id),('po_invoice_id', '=', po_invoice_id),
                                  ('product_id', '=', product_id),('state', '!=', 'cancel')])))

        for product in rtv_obj:
            if product.state!='resolved':
                quantity+= product.requested_qty

        po_qty = po_quantity - quantity
        if po_qty >0:
            po_qty = po_qty + requested_qty
        else:
            po_qty = po_qty + requested_qty

        if  quantity == po_qty:
            po_qty=0

        return po_qty


    @api.model
    def create(self,vals):
        record = super(rtv_process, self).create(vals)
        picking_type_id = self.pool['stock.picking.type'].search(self._cr, self._uid,
                                                              [('warehouse_id', '=', vals['warehouse_id'])], context={})

        po_obj = self.pool.get('purchase.order').browse(self._cr, self._uid,
                                                        self.pool['purchase.order'].search(self._cr, self._uid, [
                                                            ('picking_type_id', '=', picking_type_id)]))

        po_id_list = [po_id.id for po_id in po_obj]

        for items in vals.get('rtv_process_line'):
            if items[2]['po_id'] in po_id_list:
                for po in po_obj:
                    if items[2]['po_id']== po.id:

                        if po.partner_id.id!=vals['vendor']:
                            raise osv.except_osv(_('PO number-%s' %(po.name,)), _("Correct vendor name-%s. Please select the correct one" %(po.partner_id.name,)))

                        else:
                            if po.invoice_ids:
                                for inv in po.invoice_ids:
                                    if inv.id == items[2]['po_invoice_id']:
                                        for inv_line in inv.invoice_line:
                                            if inv_line.product_id.id==items[2]['product_id']:

                                                inv_quantity = self._process_rtv_po_qty(po.id, inv.id, inv_line.product_id.id, inv_line.quantity,items[2].get('requested_qty'))

                                                if inv_quantity > 0.00 and items[2].get('requested_qty') > inv_quantity:
                                                    raise osv.except_osv(_("Warning!!! "), _(
                                                        "Product - %s \n Requested Quantity can not greater than %s" % (inv_line.product_id.name,inv_quantity,)))

                                                elif inv_quantity < 0.00:
                                                    raise osv.except_osv(_("Warning!!! "), _(
                                                        "Product - %s \n Please check the Requested Quantity" % (inv_line.product_id.name,)))

                                                elif inv_quantity == 0.00:
                                                    raise osv.except_osv(_("Warning!!! "), _(
                                                        "Product - %s \n Already returned." % (inv_line.product_id.name,)))

                                                else:
                                                    items[2]['po_qty'] = inv_line.quantity
                                                    items[2]['return_qty'] = items[2].get('requested_qty')
                                                    items[2]['po_updated_qty'] = items[2]['po_qty'] - items[2].get('requested_qty')


                            else:
                                raise osv.except_osv(_("Warning!!!"), _(
                                "No invoice found"))

            else:
                raise osv.except_osv(_('PO not found!'), _("This PO is not in selected W/H PO"))
        return record

    @api.multi
    def write(self, vals):
        record = super(rtv_process, self).write(vals)
        warehouse_id = vals['warehouse_id'] if vals.has_key('warehouse_id') else self.warehouse_id

        picking_type_id = self.pool['stock.picking.type'].search(self._cr, self._uid,
                                                                 [('warehouse_id', '=', warehouse_id)],
                                                                 context={})

        po_obj = self.pool.get('purchase.order').browse(self._cr, self._uid,
                                                        self.pool['purchase.order'].search(self._cr, self._uid, [
                                                            ('picking_type_id', '=', picking_type_id)]))

        po_id_list = [po_id.id for po_id in po_obj]

        if vals.has_key('rtv_process_line'):
            for items in vals['rtv_process_line']:
                if not not items[2]:
                    for req_l in self.rtv_process_line:
                        if req_l.id == items[1]:
                            items[2]['po_id'] = items[2]['po_id'] if items[2].has_key('po_id') else req_l.po_id.id
                            items[2]['po_invoice_id'] = items[2]['po_invoice_id'] if items[2].has_key('po_invoice_id') else req_l.po_invoice_id.id
                            items[2]['product_id'] = items[2]['product_id'] if items[2].has_key('product_id') else req_l.product_id.id
                            items[2]['requested_qty'] = items[2]['requested_qty'] if items[2].has_key(
                                'requested_qty') else req_l.requested_qty
                            vals['vendor'] = vals['vendor'] if vals.has_key('vendor') else self.vendor.id

                            if items[2]['po_id'] in po_id_list:
                                for po in po_obj:
                                    if items[2]['po_id'] == po.id:

                                        if po.partner_id.id != vals['vendor']:
                                            raise osv.except_osv(_('PO number-%s' % (po.name,)), _(
                                                "Correct vendor name-%s. Please select the correct one" % (po.partner_id.name,)))
                                        else:
                                            if po.invoice_ids:
                                                for inv in po.invoice_ids:
                                                    if inv.id == items[2]['po_invoice_id']:
                                                        for inv_line in inv.invoice_line:
                                                            if inv_line.product_id.id == items[2]['product_id']:

                                                                inv_quantity = self._process_rtv_po_qty(po.id, inv.id,
                                                                                                        inv_line.product_id.id,
                                                                                                        inv_line.quantity,
                                                                                                        items[2]['requested_qty'])

                                                                if inv_quantity > 0.00 and items[2].get('requested_qty') > inv_quantity:
                                                                    raise osv.except_osv(_("Warning!!! "), _(
                                                                        "Product - %s \n Requested Quantity can not greater than %s" % (
                                                                        inv_line.product_id.name, inv_quantity,)))

                                                                elif inv_quantity < 0.00:
                                                                    raise osv.except_osv(_("Warning!!! "), _(
                                                                        "Product - %s \n Please check the Requested Quantity" % (
                                                                        inv_line.product_id.name,)))

                                                                elif inv_quantity == 0.00:
                                                                    raise osv.except_osv(_("Warning!!! "), _(
                                                                        "Product - %s \n Already returned." % (inv_line.product_id.name,)))

                                                                else:
                                                                    items[2]['po_qty'] = inv_line.quantity
                                                                    items[2]['po_updated_qty'] = items[2]['po_qty'] - items[2].get(
                                                                        'requested_qty')


                                            else:
                                                raise osv.except_osv(_("Warning!!!"), _(
                                                    "No invoice found"))

                            else:
                                raise osv.except_osv(_('PO not found!'), _("This PO is not in selected W/H PO"))

        return record
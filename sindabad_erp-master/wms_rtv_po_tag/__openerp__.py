{
    'name': "WMS RTV PO Tag",
    'version': '1.0',
    'category': 'WMS',
    'author': "Shuvarthi Dhar",
    'description': """WMS RTV PO Tag""",
    'website': "http://www.sindabad.com",
    'depends':['base', 'wms_inbound','wms_rtv_process'],
    'data': [
        'views/rtv_view.xml',
    ],

    'installable': True,
    'application': True,
    'auto_install': False,
}

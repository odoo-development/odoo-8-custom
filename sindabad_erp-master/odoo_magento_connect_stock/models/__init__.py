# -*- coding: utf-8 -*-
##########################################################################
#
#    Copyright (c) 2017-Present Webkul Software Pvt. Ltd. (<https://webkul.com/>)
#
##########################################################################

import res_config
import magento_openerp_stock

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:

# -*- coding: utf-8 -*-
##########################################################################
#
#    Copyright (c) 2017-Present Webkul Software Pvt. Ltd. (<https://webkul.com/>)
#
##########################################################################

import json
import re
import requests
from openerp import SUPERUSER_ID
from openerp.osv import fields, osv
from openerp.tools.translate import _
import xmlrpclib
import urllib
from openerp.http import request

################## .............magento-Odoo stock.............###########


class stock_move(osv.osv):
    _inherit = "stock.move"

    def action_confirm(self, cr, uid, ids, context=None):
        """ Confirms stock move or put it in waiting if it's linked to another move.
        """
        context = dict(context or {})

        mob_stock_action = self.pool.get('ir.values').get_default(
            cr, SUPERUSER_ID, 'mob.config.settings', 'mob_stock_action')
        res = super(stock_move, self).action_confirm(
            cr, uid, ids, context=context)
        if mob_stock_action == "fq":
            context['mob_stock_action_val'] = mob_stock_action
            self.fetch_stock_warehouse(cr, SUPERUSER_ID, res, context)
        return res

    def action_cancel(self, cr, uid, ids, context=None):
        """ Confirms stock move or put it in waiting if it's linked to another move.
        """
        context = dict(context or {})
        context['action_cancel'] = True
        mob_stock_action = self.pool.get('ir.values').get_default(
            cr, SUPERUSER_ID, 'mob.config.settings', 'mob_stock_action')
        check = False
        for id in ids:
            data = self.browse(cr, uid, id)
            if data.state == "cancel":
                check = True
        res = super(stock_move, self).action_cancel(
            cr, uid, ids, context=context)
        if mob_stock_action == "fq" and not check:
            self.fetch_stock_warehouse(cr, SUPERUSER_ID, ids, context)
        return res

    def action_done(self, cr, uid, ids, context=None):
        """ Process completly the moves given as ids and if all moves are done, it will finish the picking.
        """
        context = dict(context or {})
        mob_stock_action = self.pool.get('ir.values').get_default(
            cr, SUPERUSER_ID, 'mob.config.settings', 'mob_stock_action')
        check = False
        for id in ids:
            data = self.browse(cr, uid, id)
            if data.location_id.usage == "inventory" or data.location_dest_id.usage == "inventory":
                check = True

        res = super(stock_move, self).action_done(
            cr, uid, ids, context=context)
        if mob_stock_action == "qoh" or check:
            context['mob_stock_action_val'] = mob_stock_action
            self.fetch_stock_warehouse(cr, SUPERUSER_ID, ids, context)
        return res

    def fetch_stock_warehouse(self, cr, uid, ids, context=None):
        context = dict(context or {})
        product_quantity = 0

        if not context.has_key('stock_from'):
            for id in ids:
                data = self.browse(cr, uid, id)
                sku = data.product_id.default_code
                erp_product_id = data.product_id.id
                flag = 1
                if data.origin and data.origin.startswith('SO'):
                    sale_id = self.pool.get('sale.order').search(
                        cr, uid, [('name', '=', data.origin)])
                    if sale_id:
                        get_channel = self.pool.get('sale.order').browse(
                            cr, uid, sale_id[0]).channel
                        if get_channel == 'magento' and 'IN' not in data.picking_id.name:
                            flag = 0
                else:
                    flag = 2  # no origin
                warehouse_id = 0
                if flag == 1:
                    warehouse_id = data.warehouse_id.id
                if flag == 2:
                    location_id = data.location_dest_id.id
                    company_id = data.company_id.id
                    check_in = self.pool.get('stock.warehouse').search(cr, uid, [(
                        'lot_stock_id', '=', location_id), ('company_id', '=', company_id)], limit=1)
                    if not check_in:
                        check_in = self.check_warehouse_location(
                            cr, uid, data.location_dest_id.id, data.company_id.id, context=context)
                    if check_in:
                        # Getting Goods.
                        warehouse_id = check_in[0]
                    check_out = self.pool.get('stock.warehouse').search(cr, uid, [(
                        'lot_stock_id', '=', data.location_id.id), ('company_id', '=', data.company_id.id)], limit=1)
                    if not check_out:
                        check_out = self.check_warehouse_location(
                            cr, uid, data.location_id.id, data.company_id.id, context=context)
                    if check_out:
                        # Sending Goods.
                        warehouse_id = check_out[0]
                self.check_warehouse(
                    cr, uid, erp_product_id, sku, warehouse_id, product_quantity, context=context)
        return True

    def check_warehouse_location(self, cr, uid, location_id, company_id, context=None):
        flag = True
        check_in = []
        while flag == True and location_id:
            location_id = self.pool.get('stock.location').browse(
                cr, uid, location_id).location_id.id
            check_in = self.pool.get('stock.warehouse').search(cr, uid, [(
                'lot_stock_id', '=', location_id), ('company_id', '=', company_id)], limit=1)
            if check_in:
                flag = False
        return check_in

    def check_warehouse(self, cr, uid, erp_product_id, sku, warehouse_id, product_qty, context=None):
        context = dict(context or {})
        mapping_ids = self.pool.get('magento.product').search(
            cr, uid, [('pro_name', '=', erp_product_id)])
        if mapping_ids:
            mapping_obj = self.pool.get(
                'magento.product').browse(cr, uid, mapping_ids[0])
            instance_id = mapping_obj.instance_id.id
            mage_product_id = mapping_obj.mag_product_id
            stock_item_id = mapping_obj.magento_stock_id
            if mapping_obj.instance_id.warehouse_id.id == warehouse_id:
                product_qty = self.pool['magento.synchronization'].get_product_qty(
                    cr, uid, erp_product_id, context.get('mob_stock_action_val'))
                self.synch_quantity(cr, uid, mage_product_id, product_qty,
                                    sku, stock_item_id, instance_id, context=context)
        return True

    def synch_quantity(self, cr, uid, mage_product_id, product_qty, sku, stock_item_id, instance_id, context=None):
        response = self.update_quantity(
            cr, uid, mage_product_id, product_qty, sku, stock_item_id, instance_id, context=context)
        if response[0] == 1:
            return True
        else:
            self.pool.get('magento.sync.history').create(cr, uid, {
                'status': 'no', 'action_on': 'product', 'action': 'c', 'error_message': response[1]})

    def update_quantity(self, cr, uid, mage_product_id, quantity, sku, stock_item_id, instance_id, context=None):
        context = dict(context or {})
        qty = 0
        text = ''
        stock = 0
        session = False
        context['instance_id'] = instance_id
        sku = urllib.quote(sku, safe='')
        if mage_product_id:
            obj = self.pool.get('magento.configure').browse(
                cr, uid, instance_id)
            if not obj.active:
                return [0, ' Connection needs one Active Configuration setting.']
            else:
                stock_url = str(obj.name) + "/index.php/rest/V1/products/" + \
                    str(sku) + '/stockItems/' + str(stock_item_id)
                token = obj.token
                try:
                    if type(quantity) == str:
                        quantity = quantity.split('.')[0]
                    if type(quantity) == float:
                        quantity = quantity.as_integer_ratio()[0]
                    if quantity > 0:
                        stock = True
                    product_stock_data = {"stockItem": {
                        "itemId": stock_item_id,
                        "productId": mage_product_id,
                        "stockId": 1,
                        "qty": quantity,
                        "isInStock": stock, }
                    }
                    json_encoded_data = json.dumps(product_stock_data)
                    token = str(token.replace('"', ""))
                    userAgent = request.httprequest.environ.get('HTTP_USER_AGENT', '')
                    headers = {'Authorization': token,
                               'Content-Type': 'application/json', 'User-Agent': userAgent}
                    response = requests.put(
                        stock_url, data=json_encoded_data, headers=headers, verify=False)
                    responseData = json.loads(response.text)
                    if not response.ok:
                        return [0,'Error in Updating Stock for Magneto Product Id %s , Reason >> %s \n %s'% (
                                    mage_product_id, str(responseData.get('message', '')), str(responseData.get('parameters', '')))]
                    return [1, '']
                except Exception, e:
                    return [0, ' Error in Updating Quantity for Magneto Product Id %s , Reason >> %s' % (mage_product_id, str(e))]
        else:
            return [1, 'Error in Updating Stock, Magento Product Id Not Found!!!']
stock_move()

# -*- coding: utf-8 -*-
##########################################################################
#
#    Copyright (c) 2017-Present Webkul Software Pvt. Ltd. (<https://webkul.com/>)
#
##########################################################################

{
    'name': 'Odoo Magento Stock Management',
    'version': '1.0.2',
    'category': 'Generic Modules',
    'sequence': 1,
    'author': 'Webkul Software Pvt. Ltd.',
    'website': 'http://www.webkul.com',
    'summary': 'Odoo Magento Stock Extension',
    'description': """

Odoo Magento Stock Extesnion
============================

Stock Management From Odoo To Magento.
--------------------------------------


This module will automatically manage stock from odoo to magento during below operations.
-----------------------------------------------------------------------------------------

    1. Sales Order

    2. Purchase Order
    
    3. Point Of Sales
	
NOTE : This module works very well with latest version of magento 2.* and latest version of Odoo 8.0.
-----------------------------------------------------------------------------------------------------

    """,
    'depends': ['odoo_magento_connect'],
    'data': [
            'views/res_config_view.xml',
    ],
    'application': True,
    'installable': True,
    'active': False,
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:

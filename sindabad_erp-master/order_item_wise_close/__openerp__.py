{
    'name': 'Order item wise close',
    'version': '8.0.0',
    'category': 'Sales',
    'description': "Order item wise close",
    'author': 'Shuvarthi Dhar',
    'depends': ['base','odoo_magento_connect','sale', 'purchase', 'account', 'stock', 'stock_account','sales_collection_billing_process','send_sms_on_demand'],
    'data': [
        'security/order_item_wise_clos_Security.xml',
        'security/ir.model.access.csv',
        'wizard/order_close.xml',
        'order_item_wise_close_view.xml',
        'sale_view.xml',
        'purchase_view.xml',
    ],
    'installable': True,
    'auto_install': False,
}

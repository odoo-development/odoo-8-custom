from datetime import date, datetime
from dateutil import relativedelta
import json
import time
import requests
import openerp
from openerp.osv import fields, osv
from openerp.tools.float_utils import float_compare, float_round
from openerp.tools.translate import _
from openerp.tools import DEFAULT_SERVER_DATETIME_FORMAT, DEFAULT_SERVER_DATE_FORMAT
from openerp.exceptions import Warning
from openerp import SUPERUSER_ID, api
import openerp.addons.decimal_precision as dp
from openerp.addons.procurement import procurement
import logging
from dateutil.relativedelta import relativedelta
from operator import itemgetter
from openerp.osv import fields, osv, expression
from openerp.tools.float_utils import float_round as round
from ..odoo_to_magento_api_connect.api_connect import get_magento_token, submit_request
from openerp.http import request


class sale_order(osv.osv):
    _inherit = "sale.order"

    # Received amount
    def _get_canceled_amount(self, cr, uid, ids, name, arg, context=None):
        res = False
        res = {}

        stock_move_obj = self.pool.get("stock.move")

        for sale_obj in self.browse(cr, uid, ids, context=context):
            total_sum = 0.00

            init_data = stock_move_obj.search(cr, uid, [('origin', '=', sale_obj.name)], context=context)

            for so_move in stock_move_obj.browse(cr, uid, init_data, context=context):
                for order in sale_obj.order_line:

                    if order.closed_quantity > float(0):
                        total_sum = total_sum + (order.closed_quantity * so_move.price_unit)

            res[sale_obj.id] = total_sum
        return res

    _columns = {
        'canceled_amount': fields.function(_get_canceled_amount, string='Cancelled Amount', type='float'),
        'order_close_reason': fields.char('Order Close Reason', required=True),
        'sale_order_ids': fields.one2many('order.item.wise.close.line', 'sale_order_id', 'Order Lines')
    }

    def manual_so_status_sync(self, cr, uid, ids, context=None):
        ids = context['active_ids']
        self.pool.get('order.item.wise.close').manual_order_status_sync(cr, uid, ids, context=None)
        return True


class sale_order_line(osv.osv):
    _inherit = "sale.order.line"

    _columns = {
        'closed_quantity': fields.float('closed_quantity')
    }

class purchase_order(osv.osv):
    _inherit = "purchase.order"

    # Received amount
    def _get_received_amount(self, cr, uid, ids, name, arg, context=None):
        res = False
        res = {}

        inv_ids = []
        for po in self.browse(cr, uid, ids, context=context):
            total_sum = 0.00
            for invoice in po.invoice_ids:
                if invoice.type == 'in_invoice':
                    total_sum = total_sum + invoice.amount_total
                if invoice.type == 'in_refund':
                    total_sum = total_sum + invoice.amount_total
            res[po.id] = total_sum

        return res

    _columns = {
        'received_amount': fields.function(_get_received_amount, string='Received Amount', type='float'),
        'order_close_reason': fields.char('Purchase Order Close Reason', required=True),
    }


class purchase_order_line(osv.osv):
    _inherit = "purchase.order.line"

    _columns = {
        'closed_quantity': fields.float('closed_quantity')
    }


class order_item_wise_close(osv.osv):
    _name = "order.item.wise.close"
    _description = "Order Item Wise Close"

    _columns = {
        'so_close_reason':  fields.selection([
            ("product_not_available", "Product Not Available"),
            ("product_not_available_customer_will_take_alternative_product", "Product not available, Customer will take alternative product"),
            ("partially_delivered", "Partially delivered"),
            ("reordered", "Reordered#10000"),
            ("payment_issue", "Payment issue"),
            ("cash_problem", "Cash problem"),
            ("damage_product", "Damage product"),
            ("double_ordered", "Double ordered"),
            ("sample_not_approved", "Sample not approved"),
            ("wrong_sku", "Wrong SKU"),
            ("not_interested", "Not Interested"),
            ("wrong_product", "Wrong Product"),
            ("quality_issue", "Quality issue"),
            ("delay_delivery", "Delay delivery"),
            ("sourcing_delay", "Sourcing Delay"),
            ("not_for_retail", "Not for Retail")
    ], 'Sale Order Close Reason', copy=False, help="Reasons", select=True),
        'po_close_reason': fields.char('Purchase Order Close Reason'),
        'order_name': fields.char('Order Name'),
        'order_ref': fields.char('Order Ref.'),
        'order_id': fields.many2one('sale.order', 'Sale Order'),
        'purchase_order_id': fields.many2one('purchase.order', 'Purchase Order'),
        'order_type': fields.char('Type'),
        'sale': fields.boolean('Sale'),
        'sync_done': fields.boolean('Sync Done'),
        'purchase': fields.boolean('Purchase'),
        'order_item_line': fields.one2many('order.item.wise.close.line', 'close_order_id', 'Order Line',
                                            required=True),
    }

    def create(self, cr, uid, data, context=None):
        context = dict(context or {})


        found_zero_qty=False
        order_id= data.get('order_id')

        order_obj = self.pool.get('sale.order').browse(cr, uid, [order_id], context=context)

        for items in data.get("order_item_line"):
            try:
                if items[2].get('quantity') == 0:
                    found_zero_qty=True
            except:
                pass

        if found_zero_qty == True:
            raise osv.except_osv(_('Remove Zero (0) from Product Close Quantity'),
                                     _('You can not close due to Product quantity is set Zero (0). Please Remove it.'))




        item_line_id = super(order_item_wise_close, self).create(cr, uid, data, context=context)

        return item_line_id


    def default_get(self, cr, uid, fields, context=None):

        if context is None:
            context = {}
        res = super(order_item_wise_close, self).default_get(cr, uid, fields, context=context)

        order_obj = None
        purchase_order_id = None
        sale_order_id = None
        if context['sale']==True:
            order_obj = self.pool.get('sale.order').browse(cr, uid, context['active_id'], context=context)
            res['order_type'] = 'sale'
            res['sale'] = True
            res['order_ref'] = order_obj.client_order_ref
            res['order_id'] = context['active_id']
            order_item_wise_close_query = "SELECT id FROM order_item_wise_close WHERE order_id={0} ".format(
                context['active_id'])
            cr.execute(order_item_wise_close_query)

        elif context['purchase']== True:
            order_obj = self.pool.get('purchase.order').browse(cr, uid, context['active_id'], context=context)
            res['order_type'] = 'purchase'
            res['purchase'] = True
            res['order_ref'] = order_obj.client_order_ref
            res['purchase_order_id'] = context['active_id']

            order_item_wise_close_query = "SELECT id FROM order_item_wise_close WHERE purchase_order_id={0} ".format(
                context['active_id'])
            cr.execute(order_item_wise_close_query)

        items = []
        close_item_qty = []

        close_ids = [item_wise_close[0] for item_wise_close in cr.fetchall()]

        if close_ids:
            close_item_line_query = "SELECT product_id,sum(quantity) FROM order_item_wise_close_line WHERE close_order_id in %s GROUP BY product_id "
            cr.execute(close_item_line_query,(tuple(close_ids),))

            close_item_qty =[{sp_id[0]:sp_id[1]} for sp_id in cr.fetchall()]

        for lines in order_obj.order_line:
            delivered_qty = 0.00
            append_item = True
            if context['purchase'] == True:
                lines.product_uom_qty= lines.product_qty
                purchase_order_id = int(context['active_id'])

            if context['sale'] == True:
                lines.product_uom_qty= lines.product_uom_qty
                sale_order_id = int(context['active_id'])

            moves= lines.move_ids

            for move_ids in moves:
                if move_ids.state == 'done' and move_ids.product_id == lines.product_id and move_ids.product_qty!= lines.product_uom_qty:
                    delivered_qty = delivered_qty + move_ids.product_qty

                elif move_ids.state=='done' and move_ids.product_id == lines.product_id and move_ids.product_qty== lines.product_uom_qty:
                   append_item = False

            if lines.product_id.id == 20045:
                append_item = False

            quantity = float(lines.product_uom_qty - delivered_qty)

            if close_item_qty :
                for item_qty in close_item_qty:
                    if item_qty.has_key(lines.product_id.id) and quantity - item_qty.get(lines.product_id.id) == 0.00:
                        append_item = False
                    elif item_qty.has_key(lines.product_id.id)and quantity - item_qty.get(lines.product_id.id) > 0.00 :
                        quantity = quantity - item_qty[lines.product_id.id]
                        append_item = True

            item = {

                'order_id': int(context['active_id']),
                'sale_order_id': sale_order_id ,
                'purchase_order_id': purchase_order_id,
                'product_id': int(lines.product_id.id),
                'quantity': float(quantity),
                'order_quantity': float(quantity)
            }
            if append_item:
                items.append(item)


        res['order_name'] = str(order_obj.name)
        res.update(order_item_line=items)

        return res



    def item_wise_close(self, cr, uid, ids, context=None):

        if context is None:
            context = {}
        get_data = self.read(cr, uid, ids)[0]

        order_obj=None


        so_obj = self.pool.get('sale.order')
        po_obj = self.pool.get('purchase.order')

        if get_data['sale']==True:
            get_data['order_id'] = get_data['order_id'][0]
            order_obj = so_obj.browse(cr, uid, get_data['order_id'], context=context)[0]

            if get_data['so_close_reason'] is False:
                raise osv.except_osv(_('Close Reason ERROR!'),
                                 _('Please give the Order Close Reason!!!'))

        if get_data['purchase']==True:
            get_data['purchase_order_id'] = get_data['purchase_order_id'][0]
            order_obj = po_obj.browse(cr, uid, get_data['purchase_order_id'], context=context)[0]

            if get_data['po_close_reason']==' ' or get_data['po_close_reason'] is False or get_data['po_close_reason'] is None :
                raise osv.except_osv(_('Close Reason ERROR!'),
                                     _('Please give the Order Close Reason!!!'))

        stock_picking_query = "SELECT id FROM stock_picking WHERE origin='{0}' and state!='done'".format(str( get_data['order_name']))
        cr.execute(stock_picking_query)


        for sp_id in cr.fetchall():
            # unreserve
            self.pool.get('stock.picking').do_unreserve(cr, uid, int(sp_id[0]), context=context)
            picking = [sp_id]
            stock_obj = self.pool.get('stock.picking').browse(cr, uid, sp_id, context=context)

            if stock_obj.move_lines:
                for prod in stock_obj.move_lines:
                    # moves = line.move_ids

                    for item_lines in self.browse(cr, uid, ids[0], context=context).order_item_line:

                        if prod.product_id.id == item_lines.product_id.id and prod.product_qty == item_lines.quantity:

                            self.pool.get('stock.move').write(cr, uid, prod.id, {'product_uom_qty':0.00,'product_uos_qty':0.00},context=context)


                        elif prod.product_id.id == item_lines.product_id.id and prod.product_qty!=item_lines.quantity:

                            self.pool.get('stock.move').write(cr, uid, prod.id, {'product_uom_qty':item_lines.order_quantity - item_lines.quantity,'product_uos_qty':item_lines.order_quantity - item_lines.quantity},
                                                          context=context)

                for item_line in self.browse(cr, uid, ids[0], context=context).order_item_line:
                    for line in order_obj.order_line:
                        if get_data['sale'] == True:
                            line.product_uom_qty = line.product_uom_qty

                        if get_data['purchase'] == True:
                            line.product_uom_qty = line.product_qty

                        if line.state != 'cancel' and line.product_id.id == item_line.product_id.id and line.product_uom_qty == item_line.quantity:
                            if get_data['sale'] == True:
                                self.pool.get('sale.order.line').write(cr, uid, line.id, {'state': 'done',
                                                                                          'closed_quantity': item_line.quantity},
                                                                       context=context)
                            if get_data['purchase'] == True:
                                self.pool.get('purchase.order.line').write(cr, uid, line.id, {'state': 'done',
                                                                                              'closed_quantity': item_line.quantity},
                                                                           context=context)

                        elif line.state != 'cancel' and line.product_id.id == item_line.product_id.id and line.product_uom_qty != item_line.quantity:
                            if get_data['sale'] == True:

                                self.pool.get('sale.order.line').write(cr, uid, line.id, {
                                    'closed_quantity': item_line.quantity + line.closed_quantity}, context=context)
                            if get_data['purchase'] == True:
                                self.pool.get('purchase.order.line').write(cr, uid, line.id, {
                                    'closed_quantity': item_line.quantity + line.closed_quantity}, context=context)

                if stock_obj.state == 'draft' or stock_obj.state == 'confirmed':
                    #     # unreserve
                    self.pool.get('stock.picking').do_unreserve(cr, uid, [sp_id[0]], context=context)

                    # action cancel
                    self.pool.get('stock.picking').action_assign(cr, uid, [sp_id[0]], context=context)

                if stock_obj.state == 'waiting':
                    # Rereservation
                    self.pool.get('stock.picking').rereserve_pick(cr, uid, [sp_id[0]], context=context)

                # self.pool.get('stock.move').write(cr, uid, [moves.id for moves in stock_obj.move_lines if moves.product_qty == 0.00], {'state': 'done'}, context=context)

                non_zero_moves_id_list = [mv.id for mv in stock_obj.move_lines if mv.product_qty != 0.00]

                if not non_zero_moves_id_list:
                    self.pool.get('stock.move').write(cr, uid, [moves.id for moves in stock_obj.move_lines if moves.product_qty == 0.00], {'state': 'done'}, context=context)

        stock_query = "SELECT state FROM stock_picking WHERE origin='{0}'".format(str(get_data['order_name']))
        cr.execute(stock_query)
        stock_picking_status= [stock[0] for stock in cr.fetchall()]

        state_count=0
        if 'draft' in stock_picking_status:
            state_count+=1
        elif 'waiting' in stock_picking_status:
            state_count+=1
        elif 'confirmed' in stock_picking_status:
            state_count+=1
        elif 'partially_available' in stock_picking_status:
            state_count+=1
        elif 'assigned' in stock_picking_status:
            state_count+=1

        if state_count==0:
            if get_data['sale'] == True:
                so_obj.action_done(cr, uid, get_data['order_id'], context=context)

                so_state_query = "UPDATE sale_order SET shipped=TRUE,order_close_reason='{0}' WHERE id={1}".format(get_data['so_close_reason'], get_data['order_id'])

                cr.execute(so_state_query)
                cr.commit()

            if get_data['purchase'] == True:
                po_obj.wkf_po_done(cr, uid, get_data['purchase_order_id'], context=context)

                po_state_query = "UPDATE purchase_order SET shipped=TRUE,order_close_reason='{0}' WHERE id={1}".format(
                    get_data['po_close_reason'], get_data['purchase_order_id'])

                cr.execute(po_state_query)
                cr.commit()

        ## for sms
        if get_data['sale'] == True:
            try:
                so_data = so_obj.browse(cr, uid, get_data['order_id'], context=context)
                close_data = self.browse(cr, uid, ids[0], context=context).order_item_line
                close_product_name=''
                close_product_list=[]
                count = 1
                for close_product in close_data:
                    if close_product.order_quantity == close_product.quantity:

                        close_product_name += str(count) +". "+str(close_product.product_id.name) + ", " if close_product.product_id else ''
                        close_product_list.append(close_product)
                        count += 1


                parent = so_data.partner_id

                for i in range(5):
                    if len(parent.parent_id) == 1:
                        parent = parent.parent_id
                    else:
                        parent = parent

                        break

                company_phone = str(parent.mobile) if parent.mobile else parent.phone
                phone_number = str(so_data.partner_id.mobile) if so_data.partner_id.mobile else company_phone

                if not phone_number:
                    phone_number = str(so_data.partner_id.phone)
                if phone_number and '+88' not in phone_number:
                    phone_number = '+88'+phone_number

                if close_product_list:
                    if phone_number :
                        sms_text = "Dear Customer,\nOrder Number {0}.\nYour order product(s) '{1}' has been canceled due to unavailability ".format(str(so_data.client_order_ref), close_product_name)

                        name = "order.item.wise.close"

                        self.pool.get("send.sms.on.demand").send_sms_on_demand(cr, uid, ids, sms_text,phone_number, name,context=context)
            except:
                pass

        ## for magento sync
        if get_data['sale'] == True:
            try:
                token, url = get_magento_token(self, cr, uid, [get_data['order_id']])
                if token:
                    token = token.replace('"', "")
                    so = so_obj.browse(cr, uid, [get_data['order_id']], context=context)

                    cancle_prds = dict()

                    for line in so.order_line:
                        if line.closed_quantity > 0 and line.product_id.default_code:
                            cancle_prds[str(line.product_id.default_code).replace('"', '\"')] = line.closed_quantity

                    if cancle_prds :
                        # return_map_product_data = {'orderId': increment_id, 'itemData': return_itemData}
                        return_map_product_data = {'orderId': str(so.client_order_ref), 'itemData': cancle_prds}
                        map_option_return_url = url + "/index.php/rest/V1/odoomagentoconnect/OrderItemCancel"

                        cancelItem = dict()
                        cancelItem['cancelItem'] = return_map_product_data
                        cc = json.dumps(cancelItem)

                        userAgent = request.httprequest.environ.get('HTTP_USER_AGENT', '')
                        headers = {'Authorization': token,
                                   'Content-Type': 'application/json', 'User-Agent': userAgent}

                        try:
                            shipment = requests.post(
                                map_option_return_url, data=cc, headers=headers, verify=False)
                            mage_shipment = json.loads(shipment.text)
                            sync_done_query = "UPDATE order_item_wise_close SET sync_done=TRUE WHERE id={0}".format(ids[0])

                            cr.execute(sync_done_query)
                            cr.commit()
                        except :
                            pass

                    status_text = 'processing'

                    if so.partially_invoiced_dispatch == True or so.partial_delivered == True:
                        status_text = 'partially_delivered'
                    if so.state == 'done':
                        status_text = 'delivered'

                    if so.invoice_ids:

                        invoices = [inv.state for inv in so.invoice_ids if
                                    inv.type == 'out_invoice' and inv.state != 'cancel']
                        invoices_state = [inv_state for inv_state in invoices if inv_state != 'paid']
                        if not invoices_state:
                            status_text = 'complete'

                    # if so.invoice_ids:
                    #     refund_invoices = [inv.state for inv in so.invoice_ids if
                    #                 inv.type == 'out_refund' and inv.state != 'cancel']
                    if int(so.invoiced_amount) == int(so.amount_total) and int(so.invoiced_amount)>0:
                        status_text = 'point_gain'

                    try:
                        if status_text == 'delivered' or status_text == 'partially_delivered' or status_text =='complete' or status_text == 'point_gain':
                            self.order_wise_status_sync(token, url, status_text,so.client_order_ref)
                    except:
                        pass
            except :
                pass

        return True

    def order_wise_status_sync(self,token, url_root, status_text,order_number):
        try:
            url = url_root + "/index.php/rest/V1/odoomagentoconnect/OrderStatus"
            map_order_data = {'orderId': str(order_number), 'status': status_text}
            statusData = dict()
            statusData['statusData'] = map_order_data
            data = json.dumps(statusData)

            userAgent = request.httprequest.environ.get('HTTP_USER_AGENT', '')
            headers = {'Authorization': token,
                       'Content-Type': 'application/json', 'User-Agent': userAgent}

            res = requests.post(
                url, data=data, headers=headers, verify=False)


        except:
            pass

        return True

    def item_cancel_sync(self, cr, uid, ids, context=None):

        close_data=self.browse(cr, uid, ids[0], context=context)
        if close_data.sale == True:
            order_id = close_data.order_id.id

            try:
                token, url = get_magento_token(self, cr, uid, [order_id])
                if token:
                    token = token.replace('"', "")
                    so = self.pool.get('sale.order').browse(cr, uid, [order_id], context=context)

                    cancle_prds = dict()

                    for line in so.order_line:
                        if line.closed_quantity > 0 and line.product_id.default_code:
                            cancle_prds[str(line.product_id.default_code).replace('"', '\"')] = line.closed_quantity


                    if cancle_prds :
                        # return_map_product_data = {'orderId': increment_id, 'itemData': return_itemData}
                        return_map_product_data = {'orderId': str(so.client_order_ref), 'itemData': cancle_prds}
                        map_option_return_url = url + "/index.php/rest/V1/odoomagentoconnect/OrderItemCancel"

                        cancelItem = dict()
                        cancelItem['cancelItem'] = return_map_product_data
                        cc = json.dumps(cancelItem)

                        userAgent = request.httprequest.environ.get('HTTP_USER_AGENT', '')
                        headers = {'Authorization': token,
                                   'Content-Type': 'application/json', 'User-Agent': userAgent}

                        try:
                            shipment = requests.post(
                                map_option_return_url, data=cc, headers=headers, verify=False)
                            mage_shipment = json.loads(shipment.text)
                            sync_done_query = "UPDATE order_item_wise_close SET sync_done=TRUE WHERE id={0}".format(ids[0])

                            cr.execute(sync_done_query)
                            cr.commit()
                        except :
                            raise osv.except_osv(_('Connection ERROR!'),
                                                 _('Can not connect with magento!!!'))

            except :
                raise osv.except_osv(_('Connection ERROR!'),
                                     _('Can not connect with magento!!!'))
        return True


    def manual_order_status_sync(self, cr, uid, sale_order_id, context=None):

        so_obj = self.pool.get('sale.order')
        try:

            token, url = get_magento_token(self, cr, uid, sale_order_id)
            if token:

                for so in so_obj.browse(cr, uid, sale_order_id):

                    status_text = 'processing'

                    if so.delivery_dispatch == True and so.partially_invoiced_dispatch == False:
                        status_text = 'delivered'
                    if so.partially_invoiced_dispatch == True or so.partial_delivered == True:
                        status_text = 'partially_delivered'
                    # if so.invoiced_dispatch == True:
                    #     status_text = 'dispatched'
                    # if so.partially_invoiced_dispatch == True:
                    #     status_text = 'partially_dispatched'
                    if so.state == 'done':
                        status_text = 'delivered'
                    if str(so.state) in ['draft', 'approval_pending', 'check_pending', 'new_cus_approval_pending']:
                        status_text = 'pending'
                    if so.state == 'cancel':
                        status_text = 'canceled'

                    if so.invoice_ids:

                        invoices =[inv.state for inv in so.invoice_ids if inv.type=='out_invoice' and inv.state!='cancel']
                        invoices_state =[inv_state for inv_state in invoices if inv_state!='paid']

                        if not invoices_state:
                            status_text = 'complete'

                    if int(so.invoiced_amount) == int(so.amount_total) and int(so.invoiced_amount)>0:
                        status_text = 'point_gain'


                    try:
                        token = token.replace('"', "")
                        self.order_wise_status_sync(token, url, status_text,so.client_order_ref)
                    except:
                        pass
        except:
            pass

        return True

    def complete_order_status_sync(self, cr, uid, sale_order_id, context=None):

        so_obj = self.pool.get('sale.order')
        try:

            token, url = get_magento_token(self, cr, uid, sale_order_id)
            if token:

                for so in so_obj.browse(cr, uid, sale_order_id):


                    if so.invoice_ids:
                        invoices =[inv.state for inv in so.invoice_ids if inv.type=='out_invoice' and inv.state!='cancel']
                        invoices_state =[inv_state for inv_state in invoices if inv_state!='paid']
                        try:

                            if not invoices_state and so.state == 'done':
                                status_text = 'complete'
                                token = token.replace('"', "")
                                self.order_wise_status_sync(token, url, status_text, so.client_order_ref)
                        except:
                            pass




        except:
            pass

        return True


class order_item_wise_close_line(osv.osv):
    _name = "order.item.wise.close.line"
    _description = "Order Item Wise Close line"
    _columns = {
        'close_order_id': fields.many2one('order.item.wise.close', 'Close Order ID', required=True,
                                          ondelete='cascade', select=True, readonly=True),

        'order_id': fields.integer('Order ID'),
        'sale_order_id': fields.many2one('sale.order', 'Sale Order'),
        'purchase_order_id': fields.many2one('purchase.order', 'Purchase Order'),
        'product_id': fields.many2one('product.product', 'Product', required=True),
        'quantity': fields.float('Close Quantity'),
        'order_quantity': fields.float('Update Quantity'),
    }

from openerp.osv import fields, osv
from openerp.tools.translate import _
from openerp import api
from time import gmtime, strftime
import datetime


class retail_dashboard(osv.osv):
    _name = "retail.dashboard"
    _description = "Retail Dashboard"

    _columns = {

        'target_amount': fields.float('Target Amount'),
        'today_mtd_sales_amount': fields.float('today_mtd_sales_amount'),
        'sales_avg': fields.float('sales_avg'),
        'mtd_sales_amount': fields.float('mtd_sales_amount'),
        'customer_sales_avg': fields.float('customer_sales_avg'),
        'avg_order_size': fields.float('avg_order_size'),
        'req_sales_day': fields.float('req_sales_day'),
        'sales_day': fields.float('sales_day'),
        'delivery_sales_avg': fields.float('delivery_sales_avg'),
        'actual_delivery_sales_amount': fields.float('actual_delivery_sales_amount'),
        'todays_actual_delivery_amount': fields.float('actual_delivery_sales_amount'),
        'refund_amount': fields.float('refund_amount'),
        'refund_sales_avg': fields.float('refund_sales_avg'),
        'gpm': fields.float('gpm'),
        'today_sales_customer_count': fields.integer('today_sales_customer_count'),
        'mtd_sales_customer_count': fields.integer('mtd_sales_customer_count'),
        'total_unique_customer_count': fields.integer('total_unique_customer_count'),
        'mtd_so_unique_customer_count': fields.integer('mtd_so_unique_customer_count'),
        'mtd_so_unique_new_customer_count': fields.integer('mtd_so_unique_new_customer_count'),
        'mtd_so_unique_repeat_customer_count': fields.integer('mtd_so_unique_new_customer_count'),
        'mtd_so_unique_multi_order_customer_count': fields.integer('mtd_so_unique_multi_order_customer_count'),
        'mtd_so_unique_repeat_customer_percent': fields.float('mtd_so_unique_repeat_customer_percent'),
        'mtd_so_unique_regular_customer_count': fields.integer('mtd_so_unique_regular_customer_count'),
        'mtd_so_unique_irregular_customer_count': fields.integer('mtd_so_unique_irregular_customer_count'),
        'mtd_parent_account_order_count': fields.integer('mtd_parent_account_order_count'),
        'mtd_child_account_order_count': fields.integer('mtd_child_account_order_count'),
        'today_mtd_sales_order_placed_count': fields.integer('today_mtd_sales_order_placed_count'),
        'mtd_sales_order_placed_count': fields.integer('mtd_sales_order_placed_count'),
        'from_date': fields.date('From Date'),
        'to_date': fields.date('To Date'),
        'today_date': fields.date('today_date')

    }
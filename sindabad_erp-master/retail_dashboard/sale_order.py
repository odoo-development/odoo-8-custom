from openerp import models, api
import datetime
from datetime import datetime


class sale_order_line(models.Model):
    _inherit = 'sale.order'

    @api.model
    def _report_xls_fields_retail(self):

        return [
           'name','description','value','general_value'


        ]

    # Change/Add Template entries
    @api.model
    def _report_xls_template_retail(self):

        return {}



    def sales_dashboard_data(self, cr, uid,start_date=None, end_date=None,sale_type=None, context=None):
        parameter_list = []
        done_parameter_list = []

        today_date = datetime.today().date()
        old_st_date =today_date.replace(day=1)
        old_end_date =  datetime.today().date()
        tmp_old_st_date = old_st_date.strftime('%Y-%m-%d')
        tmp_old_end_date = old_end_date.strftime('%Y-%m-%d')
        st_date = tmp_old_st_date + str(' 00:00:00')
        en_date = tmp_old_end_date + str(' 23:59:59')
        parameter_list = [st_date,en_date]
        sale_type=sale_type



        done_parameter_list=[tmp_old_st_date,tmp_old_end_date]




        if start_date is not None and end_date is not None:
            done_parameter_list=[start_date,end_date]
            parameter_list = []
            start_date1 = datetime.strptime(start_date, '%Y-%m-%d')
            end_date1 = datetime.strptime(end_date, '%Y-%m-%d')

            start_date_time= start_date+' 00:00:00'
            end_date_time= end_date+' 23:59:59'
            done_parameter_list = [start_date_time, end_date_time]



        # query = "select stock_move.id as moved_id,stock_move.product_id,stock_picking.date_done,stock_picking.picking_type_id,stock_move.product_uom_qty,sale_order_line.price_unit,sale_order_line.order_partner_id,sale_order.client_order_ref,sale_order.date_confirm,sale_order_line.name, sale_order.name as odoo_no,(select product_category.name from product_category,product_template,product_product where product_category.id = product_template.categ_id and product_template.id = product_product.product_tmpl_id and product_product.id = stock_move.product_id ) as category,(select sum((sq.qty*sm.price_unit)) as total from stock_quant_move_rel as sqm,stock_move as sm, stock_quant as sq where sqm.quant_id in ((select sq2.id from stock_quant_move_rel as sqmr2, stock_quant as sq2 where sqmr2.move_id=stock_move.id and sqmr2.quant_id = sq2.id)) and sqm.move_id = sm.id and sm.purchase_line_id >0 and sq.id=sqm.quant_id group by sq.product_id)as total_price , (select name from stock_warehouse where id=stock_move.warehouse_id) as warhouse_name,account_invoice.number as invoice_number, account_invoice.date_invoice as invoice_date  from stock_move,stock_picking,sale_order_line,sale_order,account_invoice where stock_move.picking_id=stock_picking.id and stock_move.sale_line_id = sale_order_line.id and sale_order_line.order_id = sale_order.id  and stock_picking.name = account_invoice.origin and stock_picking.state='done'  and stock_picking.date_done >= %s and stock_picking.date_done <= %s"
        #
        # cr.execute(query, (st_date, end_date))
        # sold_objects = cr.dictfetchall()



        total_query = "select stock_picking.picking_type_id,sale_order.partner_id,sum((select sum((sq.qty*sm.price_unit)) as total from stock_quant_move_rel as sqm,stock_move as sm, stock_quant as sq where sqm.quant_id in ((select sq2.id from stock_quant_move_rel as sqmr2, stock_quant as sq2 where sqmr2.move_id=stock_move.id and sqmr2.quant_id = sq2.id)) and sqm.move_id = sm.id and sm.purchase_line_id >0 and sq.id=sqm.quant_id group by sq.product_id))as total_price ,sum((stock_move.product_uom_qty*sale_order_line.price_unit)) as total_sale from stock_move,stock_picking,sale_order_line,sale_order where stock_move.picking_id=stock_picking.id and stock_move.sale_line_id = sale_order_line.id and sale_order.id= sale_order_line.order_id and stock_picking.state='done'  and stock_picking.date_done >= %s and stock_picking.date_done <= %s group by stock_picking.picking_type_id,sale_order.partner_id"

        cr.execute(total_query,(done_parameter_list))
        total_objects = cr.dictfetchall()

        partner_list = []
        product_id_list = []
        final_product_list = {}
        final_company_list = {}

        partner_obj = self.pool("res.partner")

        # user_ids = partner_obj.search(cr, uid, domain, context=context)


        for items in total_objects:
            partner_list.append(items.get('partner_id'))





        p_obj = partner_obj.browse(cr, uid, partner_list, context=context)

        for p_items in p_obj:
            if p_items.parent_id:
                final_company_list[p_items.id] = {
                    'customer_name': p_items.name,
                    'company_name': p_items.parent_id.name,
                    'company_code': p_items.parent_code,
                    'classification': p_items.x_classification,
                    'email': p_items.email,
                    'company_email': p_items.parent_id.email,
                }
            else:
                final_company_list[p_items.id] = {
                    'customer_name': p_items.name,
                    'company_name': p_items.parent_id.name,
                    'classification': p_items.parent_id.x_classification if p_items.parent_id.x_classification else p_items.x_classification,
                    'email': p_items.parent_id.email if p_items.parent_id.email else p_items.email,
                    'company_code': p_items.parent_id.parent_code,
                    'company_email': p_items.parent_id.email,
                }

        result_list = []


        ananta_cost_value = 0.0
        ananta_sale_value = 0.0
        non_ananta_sale_value =0.0
        non_ananta_cost_value =0.0
        retail_sale_value = 0.0
        retail_cost_value = 0.0

        customer_wise_sale_dict = {}
        customer_wise_profit = {}
        top_ten_delivered_product = []
        top_ent_sold_products = []

        incoming_ids = []
        in_query = "select id from  stock_picking_type where code='incoming'"
        cr.execute(in_query)
        in_objects = cr.dictfetchall()

        incoming_ids = [in_item.get('id') for in_item in in_objects]










        for all_items in total_objects:


            partner_id = all_items.get('partner_id')
            abc = final_company_list.get(partner_id)
            total_sale = all_items.get('total_sale')
            if all_items.get('total_price') is not None:
                total_cost = all_items.get('total_price')
            else:
                total_cost = 0


            if all_items.get('picking_type_id') in incoming_ids:
                total_sale = total_sale * -1
                total_cost = total_cost* -1

            if customer_wise_sale_dict.get(partner_id) is not None and customer_wise_sale_dict.get(partner_id) is not False:
                customer_wise_sale_dict[partner_id] = customer_wise_sale_dict[partner_id] + total_sale
            else:
                customer_wise_sale_dict[partner_id] = total_sale

            if customer_wise_profit.get(partner_id) is not None and customer_wise_profit.get(
                    partner_id) is not False:
                customer_wise_profit[partner_id] = customer_wise_profit[partner_id] + (total_sale - total_cost)
            else:
                customer_wise_profit[partner_id] = (total_sale - total_cost)



            if abc.get('classification') is not False and abc.get('classification') is not None:

                got_class = abc.get('classification').lower()


                if 'ananta' in got_class:
                    if all_items.get('picking_type_id') in incoming_ids:
                        ananta_cost_value -= (all_items.get('total_price') if all_items.get('total_price') is not None else 0)
                        ananta_sale_value -= all_items.get('total_sale')

                    else:
                        ananta_cost_value += (all_items.get('total_price') if all_items.get('total_price') is not None else 0)
                        ananta_sale_value += all_items.get('total_sale')
                elif 'corporate' in got_class:


                    if all_items.get('picking_type_id') in incoming_ids:
                        non_ananta_cost_value -= (all_items.get('total_price') if all_items.get('total_price') is not None else 0)
                        non_ananta_sale_value -= all_items.get('total_sale')
                    else:
                        non_ananta_cost_value += (all_items.get('total_price') if all_items.get('total_price') is not None else 0)
                        non_ananta_sale_value += all_items.get('total_sale')
                else:


                    if all_items.get('picking_type_id') in incoming_ids:
                        retail_cost_value -= (all_items.get('total_price') if all_items.get('total_price') is not None else 0)
                        retail_sale_value -= all_items.get('total_sale')
                    else:
                        retail_cost_value += (all_items.get('total_price') if all_items.get('total_price') is not None else 0)
                        retail_sale_value += all_items.get('total_sale')



        total_ananta_sale = round(ananta_sale_value,2)
        total_profict_ananta_sale = round((total_ananta_sale - round(ananta_cost_value,2)),2)
        ananta_margin = 0


        if total_profict_ananta_sale >0 and total_ananta_sale>0:
            ananta_margin = round(((total_profict_ananta_sale/total_ananta_sale)*100),2)
        else:
            total_profict_ananta_sale=0

        non_ananta_sale_value = round(non_ananta_sale_value,2)
        total_profit_non_anant_sale = round((non_ananta_sale_value - round(non_ananta_cost_value,2)),2)
        non_ananta_margin=0


        if total_profit_non_anant_sale > 0 and non_ananta_sale_value >0:
            non_ananta_margin= round(((total_profit_non_anant_sale/non_ananta_sale_value)*100),2)
        else:
            total_profit_non_anant_sale=0

        retail_sale_value = round(retail_sale_value,2)
        retail_cost_value = round(retail_cost_value,2)
        retail_profit = round((retail_sale_value - retail_cost_value),2)
        retaile_margin =0

        if retail_profit >0 and retail_sale_value >0:
            retaile_margin = round(((retail_profit/retail_sale_value)*100),2)

        top10_aompany_names = []
        top10_aompany_values = []
        to10_prfit_company_names = []
        to10_prfit_company_values = []

        top_10_sold_amount_customer_wise = sorted(customer_wise_sale_dict.iteritems(), key=lambda (k, v): (-v, k))
        top_10_profit_amount_customer_wise = sorted(customer_wise_profit.iteritems(), key=lambda (k, v): (-v, k))

        count = 0

        for items in top_10_sold_amount_customer_wise:
            if count == 10:
                break

            abc = final_company_list.get(items[0])


            if sale_type == '1' and 'Ananta' in abc.get('classification'):
                top10_aompany_names.append(abc.get('company_name'))
                top10_aompany_values.append(items[1])
                count = count + 1
            elif sale_type == '2' and 'Corporate' in abc.get('classification'):
                top10_aompany_names.append(abc.get('company_name'))
                top10_aompany_values.append(items[1])
                count = count + 1
            elif sale_type == '3' and 'Retail' in abc.get('classification'):
                top10_aompany_names.append(abc.get('company_name'))
                top10_aompany_values.append(items[1])
                count = count + 1
            elif sale_type is None or sale_type == '0':
                top10_aompany_names.append(abc.get('company_name'))
                top10_aompany_values.append(items[1])
                count = count + 1


        p_count=0
        for items in top_10_profit_amount_customer_wise:
            if p_count == 10:
                break
            abc = final_company_list.get(items[0])

            to10_prfit_company_names.append(abc.get('company_name'))
            to10_prfit_company_values.append(items[1])
            p_count = p_count + 1




        ##Sales Person Wise Sales

        user_sold_query = "select stock_picking.picking_type_id,sale_order.custom_salesperson as user_id,sale_order.partner_id,sum((stock_move.product_uom_qty*sale_order_line.price_unit)) as total_sale from stock_move,stock_picking,sale_order_line ,sale_order where stock_move.picking_id=stock_picking.id and stock_move.sale_line_id = sale_order_line.id and stock_picking.state='done'   and sale_order_line.order_id = sale_order.id and stock_picking.date_done >= %s and stock_picking.date_done <= %s group by stock_picking.picking_type_id,sale_order.custom_salesperson,sale_order.partner_id "



        cr.execute(user_sold_query,(done_parameter_list))
        uesr_sold_data =cr.dictfetchall()

        user_wise_sale_dict ={}

        ## Filter data with sale type

        sold_items_partner_list = []
        sold_company_list={}
        for user_items in uesr_sold_data:
            sold_items_partner_list.append(user_items.get('partner_id'))

        p_obj = partner_obj.browse(cr, uid, partner_list, context=context)

        for p_items in p_obj:
            if p_items.parent_id:
                sold_company_list[p_items.id] = {
                    'customer_name': p_items.name,
                    'company_name': p_items.parent_id.name,
                    'company_code': p_items.parent_code,
                    'classification': p_items.x_classification,
                    'email': p_items.email,
                    'company_email': p_items.parent_id.email,
                }
            else:
                sold_company_list[p_items.id] = {
                    'customer_name': p_items.name,
                    'company_name': p_items.parent_id.name,
                    'classification': p_items.parent_id.x_classification if p_items.parent_id.x_classification else p_items.x_classification,
                    'email': p_items.parent_id.email if p_items.parent_id.email else p_items.email,
                    'company_code': p_items.parent_id.parent_code,
                    'company_email': p_items.parent_id.email,
                }


        for user_items in uesr_sold_data:
            user_data = sold_company_list.get(user_items.get('partner_id'))


            if sale_type == '1' and 'Ananta' in user_data.get('classification'):

                total_sold = user_items.get('total_sale')
                if user_items.get('picking_type_id') in incoming_ids:
                    total_sold = total_sold * -1

                if user_wise_sale_dict.get(user_items.get('user_id')) is not False and user_wise_sale_dict.get(
                        user_items.get('user_id')) is not None:
                    user_wise_sale_dict[user_items.get('user_id')] = user_wise_sale_dict[
                                                                         user_items.get('user_id')] + total_sold
                else:
                    user_wise_sale_dict[user_items.get('user_id')] = total_sold

            elif sale_type == '2' and 'Corporate' in user_data.get('classification'):
                total_sold = user_items.get('total_sale')
                if user_items.get('picking_type_id') in incoming_ids:
                    total_sold = total_sold * -1

                if user_wise_sale_dict.get(user_items.get('user_id')) is not False and user_wise_sale_dict.get(
                        user_items.get('user_id')) is not None:
                    user_wise_sale_dict[user_items.get('user_id')] = user_wise_sale_dict[
                                                                         user_items.get('user_id')] + total_sold
                else:
                    user_wise_sale_dict[user_items.get('user_id')] = total_sold

            elif sale_type == '3' and 'Retail' in user_data.get('classification'):
                total_sold = user_items.get('total_sale')
                if user_items.get('picking_type_id') in incoming_ids:
                    total_sold = total_sold * -1

                if user_wise_sale_dict.get(user_items.get('user_id')) is not False and user_wise_sale_dict.get(
                        user_items.get('user_id')) is not None:
                    user_wise_sale_dict[user_items.get('user_id')] = user_wise_sale_dict[
                                                                         user_items.get('user_id')] + total_sold
                else:
                    user_wise_sale_dict[user_items.get('user_id')] = total_sold
            elif sale_type is None or sale_type == '0':

                total_sold = user_items.get('total_sale')
                if user_items.get('picking_type_id') in incoming_ids:
                    total_sold = total_sold * -1

                if user_wise_sale_dict.get(user_items.get('user_id')) is not False and user_wise_sale_dict.get(
                        user_items.get('user_id')) is not None:
                    user_wise_sale_dict[user_items.get('user_id')] = user_wise_sale_dict[
                                                                         user_items.get('user_id')] + total_sold
                else:
                    user_wise_sale_dict[user_items.get('user_id')] = total_sold





        ## Ends here





        user_names = []
        user_values = []


        for k,v in user_wise_sale_dict.iteritems():
            query = "select res_partner.name from res_users,res_partner where res_users.partner_id=res_partner.id and  res_users.id=%s"
            cr.execute(query, ([k]))
            res_dict = cr.dictfetchall()
            if len(res_dict) > 0:
                for res_it in res_dict:
                    user_names.append(res_it.get('name'))
                    user_values.append(v)
                    break



        top5_sales_man=[]
        top5_sales_man_value=[]

        top5_sales_man_dict = sorted(user_wise_sale_dict.iteritems(), key=lambda (k, v): (-v, k))[:5]

        for i_data in top5_sales_man_dict:
            query = "select res_partner.name from res_users,res_partner where res_users.partner_id=res_partner.id and  res_users.id=%s"
            cr.execute(query,([i_data[0]]))
            res_dict = cr.dictfetchall()
            if len(res_dict) >0:
                for res_it in res_dict:
                    top5_sales_man.append(res_it.get('name'))
                    top5_sales_man_value.append(i_data[1])
                    break








        ## Ends Here


        ## Start here Product Wise Sales


        product_query = "select stock_picking.picking_type_id,stock_move.product_id,sale_order.partner_id,sum((stock_move.product_uom_qty*sale_order_line.price_unit)) as total_sale,stock_move.name  from stock_move,stock_picking,sale_order_line ,sale_order where stock_move.picking_id=stock_picking.id and stock_move.sale_line_id = sale_order_line.id and stock_picking.state='done'   and sale_order_line.order_id = sale_order.id and stock_picking.date_done >= %s and stock_picking.date_done <= %s group by stock_picking.picking_type_id,stock_move.product_id,sale_order.partner_id,stock_move.name"

        cr.execute(product_query,(done_parameter_list))
        product_sold_data = cr.dictfetchall()

        product_wise_sale_dict = {}

        product_company_list ={}





        for user_items in product_sold_data:
            user_data = sold_company_list.get(user_items.get('partner_id'))
            if sale_type == '1' and 'Ananta' in user_data.get('classification'):

                total_sold = user_items.get('total_sale')
                if user_items.get('picking_type_id') in incoming_ids:
                    total_sold = total_sold *-1

                if product_wise_sale_dict.get(user_items.get('product_id')) is not False and product_wise_sale_dict.get(user_items.get('product_id')) is not None:
                    product_wise_sale_dict[user_items.get('product_id')] = product_wise_sale_dict[user_items.get('product_id')] + total_sold
                else:
                    product_wise_sale_dict[user_items.get('product_id')] = total_sold
            elif sale_type == '2' and 'Corporate' in user_data.get('classification'):
                total_sold = user_items.get('total_sale')
                if user_items.get('picking_type_id') in incoming_ids:
                    total_sold = total_sold * -1

                if product_wise_sale_dict.get(user_items.get('product_id')) is not False and product_wise_sale_dict.get(
                        user_items.get('product_id')) is not None:
                    product_wise_sale_dict[user_items.get('product_id')] = product_wise_sale_dict[user_items.get(
                        'product_id')] + total_sold
                else:
                    product_wise_sale_dict[user_items.get('product_id')] = total_sold
            elif sale_type == '3' and 'Retail' in user_data.get('classification'):

                total_sold = user_items.get('total_sale')
                if user_items.get('picking_type_id') in incoming_ids:
                    total_sold = total_sold * -1

                if product_wise_sale_dict.get(user_items.get('product_id')) is not False and product_wise_sale_dict.get(
                        user_items.get('product_id')) is not None:
                    product_wise_sale_dict[user_items.get('product_id')] = product_wise_sale_dict[user_items.get(
                        'product_id')] + total_sold
                else:
                    product_wise_sale_dict[user_items.get('product_id')] = total_sold
            elif sale_type is None or sale_type == '0':

                total_sold = user_items.get('total_sale')
                if user_items.get('picking_type_id') in incoming_ids:
                    total_sold = total_sold * -1

                if product_wise_sale_dict.get(user_items.get('product_id')) is not False and product_wise_sale_dict.get(
                        user_items.get('product_id')) is not None:
                    product_wise_sale_dict[user_items.get('product_id')] = product_wise_sale_dict[user_items.get(
                        'product_id')] + total_sold
                else:
                    product_wise_sale_dict[user_items.get('product_id')] = total_sold




        top_10_sold_amount_product_wise={}
        top_10_sold_amount_product_wise = sorted(product_wise_sale_dict.iteritems(), key=lambda (k, v): (-v, k))[:10]


        product_name =[]
        product_values=[]

        for item in top_10_sold_amount_product_wise:
            for user_items in product_sold_data:
                if user_items.get('product_id') == item[0]:
                    product_name.append(user_items.get('name'))
                    break
            product_values.append(item[1])











        ## Ends Here



        if sale_type == '1': # Ananta Sales
            non_ananta_sale_value = 0
            total_profit_non_anant_sale = 0
            non_ananta_margin =0

            retail_sale_value = 0
            retail_profit = 0
            retaile_margin = 0

        elif sale_type == '2':  # Corporate Sales
            ananta_sale_value = 0
            total_profict_ananta_sale = 0
            ananta_margin = 0

            retail_sale_value = 0
            retail_profit = 0
            retaile_margin = 0

        elif sale_type == '3':  ## Retail Sales
            ananta_sale_value = 0
            total_profict_ananta_sale = 0
            ananta_margin = 0
            non_ananta_sale_value = 0
            total_profit_non_anant_sale = 0
            non_ananta_margin = 0


        ananta_sale_value = 'Sales : ' + str(round(ananta_sale_value,2))
        total_profict_ananta_sale =  'Profit : ' + str(round(total_profict_ananta_sale,2))
        ananta_margin = 'Margin : ' + str(round(ananta_margin,3))

        non_ananta_sale_value = 'Sales : ' + str(round(non_ananta_sale_value,2))
        total_profit_non_anant_sale = 'Profit : ' + str(round(total_profit_non_anant_sale,2))
        non_ananta_margin = 'Margin : ' + str(round(non_ananta_margin,3))

        retail_sale_value = 'Sales : '+ str(round(retail_sale_value,2))
        retail_profit = 'Profit : ' + str(round(retail_profit,2))

        retaile_margin = 'Margin : ' + str(round(retaile_margin,3))





        return [
            ananta_sale_value,total_profict_ananta_sale,ananta_margin,
            non_ananta_sale_value,total_profit_non_anant_sale,non_ananta_margin,
            retail_sale_value,retail_profit,retaile_margin,
            top10_aompany_names,top10_aompany_values,
            to10_prfit_company_names,to10_prfit_company_values,
            user_names,user_values,
            product_name,product_values,
            top5_sales_man,top5_sales_man_value
        ]























from openerp import models, api
from openerp.osv import osv, fields
from openerp import api, fields, models


class UniqueCustomerOrderReport(models.Model):
    _inherit = 'sale.order'

    @api.model
    def _report_xls_fields_customer_order(self):

        return [
            'order_number','customer_name','customer_classification','confirm_date',
        'order_amount','company_name','customer_email','company_email','child_email'
        ]


    # Change/Add Template entries
    @api.model
    def _report_xls_template_customer_order(self):

        return {}


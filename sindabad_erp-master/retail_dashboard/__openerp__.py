{
    'name': 'Retail Dashoboard',
    'version': '8.0.0.6.0',
    'author': "Shuvarthy & Rocky & Mufti",
    'category': 'Sales',
    'summary': 'Retail Dashobard',
    'depends': ['sale', 'product','report_xls'],

    'data': [

        'security/retail_dashboard_security.xml',
        'security/ir.model.access.csv',

        'wizard/retail_dashboard_filter_view.xml',
        'report/retail_dashboard_xls_view.xml',

        'wizard/unique_customer_report_filter.xml',
        'report/unique_customer_report_xls.xml',

        'wizard/unique_customer_order_report_filter.xml',
        'report/unique_customer_order_report_xls.xml',

        'views/sales_dashboard.xml',
        'views/sales_dashboard_menu.xml',

        'views/sales_target_view.xml',
        'sales_target_menu.xml',

    ],

    'installable': True,
    'auto_install': False,
}

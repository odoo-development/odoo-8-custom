from openerp import models, api
from openerp.osv import osv, fields
from openerp import api, fields, models


class UniqueCustomerReport(models.Model):
    _inherit = 'sale.order'

    @api.model
    def _report_xls_fields_unique(self):

        # return [
        #     'order_number','customer_name','customer_classification','confirm_date','order_amount','company_name','customer_email','email','customer_phone',
        #     'magento_flag','customer_address'
        # ]
        return [
            'customer_name', 'customer_classification', 'company_name',
            'customer_email','company_email', 'email', 'customer_phone',
            'magento_flag', 'customer_address','create_date'
        ]

    # Change/Add Template entries
    @api.model
    def _report_xls_template_unique(self):

        return {}


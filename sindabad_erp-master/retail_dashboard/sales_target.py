from openerp.osv import fields, osv
from openerp import api
from openerp.tools.translate import _
import datetime


class sales_target(osv.osv):

    _name = "sales.target"

    _columns = {
        'date_from': fields.date('Date From'),
        'date_to': fields.date('Date To'),
        'amount': fields.float('Sales Amount'),
        'delivery_amount': fields.float('Delivery Amount'),

        'approve': fields.boolean('Approve'),
        'approve_by': fields.many2one('res.users', 'Approve By'),
        'approve_time': fields.datetime('Approve Date'),

        'cancel': fields.boolean('Cancel'),
        'cancel_by': fields.many2one('res.users', 'Cancel By'),
        'cancel_time': fields.datetime('Cancel Date'),

        'sales_target_rm_line': fields.one2many('sales.target.rm', 'sales_target_rm_id', 'Sales Target RM Line', required=True),

        'state': fields.selection([
            ('pending', 'Pending'),
            ('approve', 'Approve'),
            ('cancel', 'Cancel'),

        ], 'Status', help="Gives the status of sales target rm", select=True),

        'sales_target_holiday_line': fields.one2many('sales.target.holiday', 'sales_target_holiday_id', 'Sales Target Holiday Line'),

    }

    _defaults = {
        'date_from': fields.datetime.now,
        'date_to': fields.datetime.now,
        'user_id': lambda obj, cr, uid, context: uid,
        'state': 'pending',

    }



    def approve_sales_target(self, cr, uid, ids, context=None):

        for single_id in ids:
            approve_sales_target_query = "UPDATE sales_target SET state='approve', approve=TRUE, approve_by='{0}', approve_time='{1}' WHERE id={2}".format(
                uid, str(fields.datetime.now()), single_id)
            cr.execute(approve_sales_target_query)
            cr.commit()

            approve_sales_target_rm_query = "UPDATE sales_target_rm SET state='approve', approve=TRUE, approve_by='{0}', approve_time='{1}' WHERE sales_target_rm_id={2}".format(
                uid, str(fields.datetime.now()), single_id)
            cr.execute(approve_sales_target_rm_query)
            cr.commit()

        return True



    def cancel_sales_target(self, cr, uid, ids, context=None):

        for single_id in ids:
            cancel_sales_target_query = "UPDATE sales_target SET state='cancel', cancel=TRUE, cancel_by='{0}', cancel_time='{1}' WHERE id={2}".format(
                uid, str(fields.datetime.now()), single_id)
            cr.execute(cancel_sales_target_query)
            cr.commit()

            cancel_sales_target_rm_query = "UPDATE sales_target_rm SET state='cancel', cancel=TRUE, cancel_by='{0}', cancel_time='{1}' WHERE sales_target_rm_id={2}".format(
                uid, str(fields.datetime.now()), single_id)
            cr.execute(cancel_sales_target_rm_query)
            cr.commit()

        return True



    @api.model
    def create(self, vals):

        record = None

        amount = vals.get("amount")
        total_amount = 0

        if vals.has_key('sales_target_rm_line'):
            for single_line in vals['sales_target_rm_line']:

                # import pdb;
                # pdb.set_trace()

                if not not single_line[2]:
                    total_amount = total_amount + single_line[2]["target_amount"]

        if amount != total_amount:
            raise osv.except_osv(_('Warning!!!'),
                                 _('Employees total target amount are must be equal as Sales target amount!!!'))
        else:
            record = super(sales_target, self).create(vals)


        return record




class sales_target_rm(osv.osv):

    _name = "sales.target.rm"

    _columns = {

        'sales_target_rm_id': fields.many2one('sales.target', 'Sales Target RM ID', required=True, ondelete='cascade', select=True, readonly=True),

        'amount': fields.float('Amount'),

        'emp_name': fields.many2one('res.users', 'Employee Name'),
        'target_amount': fields.float('Target Sales Amount'),
        'target_delivery_amount': fields.float('Target Delivery Amount'),

        'approve': fields.boolean('Approve'),
        'approve_by': fields.many2one('res.users', 'Approve By'),
        'approve_time': fields.datetime('Approve Date'),

        'cancel': fields.boolean('Cancel'),
        'cancel_by': fields.many2one('res.users', 'Cancel By'),
        'cancel_time': fields.datetime('Cancel Date'),

        'state': fields.selection([
            ('pending', 'Pending'),
            ('approve', 'Approve'),
            ('cancel', 'Cancel'),

        ], 'Status', help="Gives the status of sales target rm", select=True),

    }




class sales_target_holiday(osv.osv):

    _name = "sales.target.holiday"

    _columns = {

        'sales_target_holiday_id': fields.many2one('sales.target', 'Sales Target Holiday ID', required=True, ondelete='cascade', select=True, readonly=True),

        'holiday': fields.date('Holiday'),

    }

import xlwt
from datetime import datetime,timedelta,date
from openerp.osv import orm,osv
from openerp.report import report_sxw
from openerp.addons.report_xls.report_xls import report_xls
from openerp.addons.report_xls.utils import rowcol_to_cell, _render
from openerp.tools.translate import translate, _
from dateutil.relativedelta import relativedelta
import logging
import itertools
import calendar
import re


_logger = logging.getLogger(__name__)


_ir_translation_name = 'retail.dashboard.xls'


class retail_dashboard_xls_parser(report_sxw.rml_parse):

    def __init__(self, cr, uid, name, context):
        super(retail_dashboard_xls_parser, self).__init__(
            cr, uid, name, context=context)
        move_obj = self.pool.get('sale.order')
        self.context = context
        wanted_list = move_obj._report_xls_fields_retail(cr, uid, context)
        template_changes = move_obj._report_xls_template_retail(cr, uid, context)
        self.localcontext.update({
            'datetime': datetime,
            'wanted_list': wanted_list,
            'template_changes': template_changes,
            '_': self._,
        })

    def _(self, src):
        lang = self.context.get('lang', 'en_US')
        return translate(self.cr, _ir_translation_name, 'report', lang, src) \
            or src


class retail_dashboard_xls(report_xls):

    def __init__(self, name, table, rml=False, parser=False, header=True,
                 store=False):
        super(retail_dashboard_xls, self).__init__(
            name, table, rml, parser, header, store)

        # Cell Styles
        _xs = self.xls_styles
        # header
        rh_cell_format = _xs['bold'] + _xs['fill'] + _xs['borders_all']
        self.rh_cell_style = xlwt.easyxf(rh_cell_format)
        self.rh_cell_style_center = xlwt.easyxf(rh_cell_format + _xs['center'])
        self.rh_cell_style_right = xlwt.easyxf(rh_cell_format + _xs['right'])
        # lines
        aml_cell_format = _xs['borders_all']
        self.aml_cell_style = xlwt.easyxf(aml_cell_format)
        self.aml_cell_style_center = xlwt.easyxf(
            aml_cell_format + _xs['center'])
        self.aml_cell_style_date = xlwt.easyxf(
            aml_cell_format + _xs['left'],
            num_format_str=report_xls.date_format)
        self.aml_cell_style_decimal = xlwt.easyxf(
            aml_cell_format + _xs['right'],
            num_format_str=report_xls.decimal_format)
        # totals
        rt_cell_format = _xs['bold'] + _xs['fill'] + _xs['borders_all']
        self.rt_cell_style = xlwt.easyxf(rt_cell_format)
        self.rt_cell_style_right = xlwt.easyxf(rt_cell_format + _xs['right'])
        self.rt_cell_style_decimal = xlwt.easyxf(
            rt_cell_format + _xs['right'],
            num_format_str=report_xls.decimal_format)

        # XLS Template
        self.col_specs_template = {
            'name': {
                'header': [1, 20, 'text', _render("_('')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},
            'description': {
                'header': [1, 20, 'text', _render("_('Description')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},
            'value': {
                'header': [1, 20, 'text', _render("_('Retail & SOHO')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},
            'general_value': {
                'header': [1, 20, 'text', _render("_('Online')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

        }

    def daterange(self,date1, date2):
        for n in range(int((date2 - date1).days) + 1):
            yield date1 + timedelta(n)

    def generate_xls_report(self, _p, _xs, data, objects, wb):

        wanted_list = _p.wanted_list
        self.col_specs_template.update(_p.template_changes)
        _ = _p._

        context = self.context
        from_date= data.get('form').get('date_from')
        st_date = data.get('form').get('date_to')
        context['start_date'] = from_date
        context['end_date'] = st_date

        filter_date = datetime.strptime(st_date, '%Y-%m-%d')
        from_date_obj = datetime.strptime(from_date, '%Y-%m-%d')

        date_list=[]

        for dt in self.daterange(from_date_obj, filter_date):
            date_list.append(dt.strftime("%Y-%m-%d"))

        #### monthwise target holiday ######

        target_holiday_query = "SELECT holiday.holiday FROM sales_target_holiday holiday, sales_target target_sale where target_sale.id=holiday.sales_target_holiday_id and target_sale.date_from <=%s and target_sale.date_to >=%s"

        self.cr.execute(target_holiday_query, (from_date, st_date))
        target_holiday_data_list = self.cr.dictfetchall()
        holiday_list = [holiday['holiday'] for holiday in target_holiday_data_list]
        holiday_count = 0

        # for holiday_date in holiday_list:
        #     if datetime.strptime(holiday_date, '%Y-%m-%d') < datetime.strptime(from_date, '%Y-%m-%d'):
        #         holiday_list.remove(holiday_date)
        #

        for holiday_date in holiday_list:
            if holiday_date in date_list:
                holiday_count += 1

     ###################################

        three_months = from_date_obj + relativedelta(months=-3)
        three_month = str(three_months.month)
        three_months_date = str(three_months.year)+'-'+three_month+'-01'

        last_date_of_month = datetime(filter_date.year, filter_date.month, 1) + relativedelta(months=1, days=-1)
        last_date_duration = last_date_of_month - filter_date
        last_date_time_month = str(last_date_of_month.year)+'-'+str(last_date_of_month.month)+'-'+str(last_date_of_month.day)+' 23:59:59'

        frist_date_of_month = datetime(filter_date.year, filter_date.month, 1)
        frist_date_time_month = str(frist_date_of_month.year)+'-'+str(frist_date_of_month.month)+'-01 00:00:00'
        frist_date_duration = filter_date - frist_date_of_month

        from_date_time = from_date + ' 00:00:00'
        st_date_time = st_date + ' 23:59:59'

        if frist_date_duration.days!=0:
            frist_date_duration = frist_date_duration.days
            frist_date_duration = frist_date_duration - holiday_count
        else :
            frist_date_duration = 1

        if last_date_duration.days!=0:
            last_date_duration = last_date_duration.days
            last_date_hoiliday_duration = len(holiday_list) - holiday_count
            last_date_duration = last_date_duration - last_date_hoiliday_duration
            if last_date_duration==0:
                last_date_duration = 1

        else :
            last_date_duration = 1

        self.context = context
        date_range = _("Date: %s to %s" % (from_date,st_date))

        # report_name = objects[0]._description or objects[0]._name
        report_name = _("Retail Dashboard")
        ws = wb.add_sheet(report_name[:31])
        ws.panes_frozen = True
        ws.remove_splits = True
        ws.portrait = 0  # Landscape
        ws.fit_width_to_pages = 1
        row_pos = 0

        # set print header/footer
        ws.header_str = self.xls_headers['standard']
        ws.footer_str = self.xls_footers['standard']

        # Title
        cell_style = xlwt.easyxf(_xs['xls_title'])
        c_specs = [
            ('report_name', 2, 2, 'text', report_name),
            ('date_range', 1, 1, 'text', date_range),
        ]
        row_data = self.xls_row_template(c_specs, ['report_name','date_range'])
        row_pos = self.xls_write_row(
            ws, row_pos, row_data, row_style=cell_style)
        row_pos += 1

        c_specs = map(lambda x: self.render(
            x, self.col_specs_template, 'header', render_space={'_': _p._}),
                      wanted_list)
        row_data = self.xls_row_template(c_specs, [x[0] for x in c_specs])
        row_pos = self.xls_write_row(
            ws, row_pos, row_data, row_style=self.rh_cell_style,
            set_column_size=True)
        ws.set_horz_split_pos(row_pos)

        final_data=[]

        target_dict = {'name': 'Sales', 'description': 'Sales Target'}
        result_today_mtd_sales = {'name': 'Sales', 'description': "Today's Sales("+st_date+")" }
        sales_avg = {'name': 'Sales', 'description': 'Sales Achv.'}
        result_mtd_sales = {'name': 'Sales', 'description': 'MTD Sales'}
        total_customer_sales_avg = {'name': 'Sales', 'description': 'Avg. Cart Value/Customer'}
        avg_order_size = {'name': 'Sales', 'description': 'Avg. Order Size'}
        req_sales_day = {'name': 'Sales', 'description': 'Required Sales/Day'}
        sales_day = {'name': 'Sales', 'description': 'Sales/Day'}

        delivery_target_dict = {'name': 'Delivered', 'description': 'Delivery Target'}
        delivery_sales_avg = {'name': 'Delivered', 'description': 'Del. Achv./Sales Order'}
        actual_delivery_sales = {'name': 'Delivered', 'description': 'MTD Delivered(COGS)/Sales Amount'}

        result_todays_actual_delivery = {'name': 'Delivered', 'description': "Today's Delivered Amount (" + st_date+")"}

        result_refund = {'name': 'Refund', 'description': 'Refund on Sales Amount'}
        result_current_month_refund = {'name': 'Refund', 'description': "Current Month Order's Return Value"}
        refund_sales_avg = {'name': 'Refund', 'description': 'Refund%/Sales Order'}
        total_profit_avg = {'name': 'GPM', 'description': 'GPM'}

        result_mtd_sales_order = {'name': 'Order', 'description': 'MTD Number. of Order Place'}
        result_sales_customer = {'name': 'Customer', 'description': 'MTD Number. of Customer Place order'}

        # result_new_register_customer_sales = {'name': 'Customer', 'description': 'MTD New Register Customer Place Order'}
        # result_new_register_customer = {'name': 'Customer', 'description': 'MTD New Register Customer'}

        result_sales_new_customer = {'name': 'Customer', 'description': 'MTD New Customer Place Order'}
        result_sales_old_customer = {'name': 'Customer', 'description': 'MTD Repeat Customer Place Order'}
        result_sales_regular_customer = {'name': 'Customer', 'description': 'MTD Regular Customer Place Order'}
        result_sales_irregular_customer = {'name': 'Customer', 'description': 'MTD Irregular Customer Place Order'}
        # result_sales_regular_unique_customer = {'name': 'Customer', 'description': 'MTD Regular Unique Customer Place Order'}

        result_sales_multi_order_customer = {'name': 'Customer', 'description': 'MTD Multiple Order Customer Place Order'}
        result_sales_repeat_customer_percent = {'name': 'Customer', 'description': 'Repeat Customer %'}

        result_today_mtd_sales_order = {'name': 'Order', 'description': "Today's Number. of Order Place(" + st_date+")"}
        result_today_sales_customer = {'name': 'Customer', 'description': "Today's Number. of Customer Place order(" + st_date+")"}

        result_unique_customer = {'name': 'Customer', 'description': 'Total Number. of Unique Customer Registered'}
        result_so_unique_customer = {'name': 'Customer', 'description': 'MTD Number. of Unique Customer Place Order'}
        # result_no_so_unique_customer = {'name': 'Customer', 'description': "MTD Number. of Unique Customer doesn't Place Order"}

        result_child_account_order = {'name': 'Customer', 'description': 'Child Account Place Order'}
        result_parent_account_order = {'name': 'Customer', 'description': 'Parent Account Place Order'}

        # result_app_child_account_order = {'name': 'APP', 'description': 'Child Account Place Order(App)'}
        # result_app_parent_account_order = {'name': 'APP', 'description': 'Parent Account Place Order(App)'}

        #### monthwise target amount ######

        target_query = "SELECT date_from,sum(amount)as amount, sum(delivery_amount)as delivery_amount from sales_target where date_from >= %s and date_from <=%s  GROUP BY date_from"

        self.cr.execute(target_query, (frist_date_time_month, last_date_time_month))
        target_data_list = self.cr.dictfetchall()

        target_dict.update({'value':sum(target_data_row['amount'] for target_data_row in target_data_list),
                            'general_value':sum(target_data_row['amount'] for target_data_row in target_data_list)})
        delivery_target_dict.update({'value': sum(target_data_row['delivery_amount'] for target_data_row in target_data_list),
                                     'general_value': sum(target_data_row['delivery_amount'] for target_data_row in target_data_list)})

        ##########################

        in_query = "select id from  stock_picking_type where code='incoming'"
        self.cr.execute(in_query)
        incoming_objects = self.cr.dictfetchall()

        incoming_ids = [in_item.get('id') for in_item in incoming_objects]



        delivered_sales="select stock_move.id as moved_id,stock_move.product_id,stock_picking.date_done, stock_picking.picking_type_id,sale_order_line.order_partner_id,stock_move.product_uom_qty,sale_order_line.price_unit,sale_order.date_confirm,sale_order_line.order_id," \
                        "(select sum((sq.qty*sm.price_unit)) as total from stock_quant_move_rel as sqm,stock_move as sm, stock_quant as sq where sqm.quant_id in ((select sq2.id from stock_quant_move_rel as sqmr2, stock_quant as sq2 where sqmr2.move_id=stock_move.id and sqmr2.quant_id = sq2.id)) and sqm.move_id = sm.id and sm.purchase_line_id >0 and sq.id=sqm.quant_id group by sq.product_id)as total_cost_price from stock_move,stock_picking,sale_order,sale_order_line where stock_move.picking_id=stock_picking.id and stock_move.sale_line_id = sale_order_line.id and sale_order_line.order_id = sale_order.id  and stock_picking.state='done'and stock_picking.date_done >= %s and stock_picking.date_done <= %s"

        self.cr.execute(delivered_sales, (from_date_time, st_date_time))
        delivered_sales_data_list = self.cr.dictfetchall()
        result_delivery = {}
        result_profit={}
        delivery_partner_list=[]
        retail_delivery_partner_id_list=[]
        general_delivery_partner_id_list = []


        #####  Get retail, general ID Lists

        partner_obj = self.pool("res.partner")
        for items in delivered_sales_data_list:
            delivery_partner_list.append(items.get('order_partner_id'))

        p_obj = partner_obj.browse(self.cr, self.uid, delivery_partner_list, context=context)

        for p_items in p_obj:
            if p_items.parent_id:
                if p_items.x_classification is not False and p_items.x_classification is not None:
                    p_classification = p_items.x_classification.lower()

                    if p_classification == 'ananta':
                        pass
                    elif p_classification == 'corporate':
                        pass
                    elif p_classification == 'general':
                        general_delivery_partner_id_list.append(p_items.id)
                    elif p_classification == '':
                        retail_delivery_partner_id_list.append(p_items.id)
                    else:
                        retail_delivery_partner_id_list.append(p_items.id)
            else:
                p_classification=p_items.parent_id.x_classification if p_items.parent_id.x_classification else p_items.x_classification

                if p_classification is not False and p_classification is not None:
                    p_classification = p_items.x_classification.lower()

                    if p_classification == 'ananta':
                        pass
                    elif p_classification == 'corporate':
                        pass
                    elif p_classification == 'general':
                        general_delivery_partner_id_list.append(p_items.id)
                    elif p_classification == '':
                        retail_delivery_partner_id_list.append(p_items.id)
                    else:
                        retail_delivery_partner_id_list.append(p_items.id)

        #### Ends Here
        total_refund_sale = 0
        total_delivery_sale = 0
        todays_refund_sale = 0
        todays_delivery_sale = 0
        profit=0

        general_total_refund_sale = 0
        general_total_delivery_sale = 0
        general_todays_refund_sale = 0
        general_todays_delivery_sale = 0
        general_profit = 0
        general_month_wise_sales_return={}
        month_wise_sales_return={}

        for delivered_sales_data in delivered_sales_data_list:

            if delivered_sales_data.get('order_partner_id') in retail_delivery_partner_id_list:


                cal_cost = delivered_sales_data.get('total_cost_price')
                moved_id = delivered_sales_data.get('moved_id')


                if cal_cost == 0:

                    get_adjusted_cost_query = "select sum((sq.qty * sm.price_unit)) as total from stock_quant_move_rel as sqm,stock_move as sm, stock_quant as sq where sqm.quant_id in ((select sq2.id from stock_quant_move_rel as sqmr2, stock_quant as sq2 where sqmr2.move_id=%s and sqmr2.quant_id = sq2.id)) and sqm.move_id = sm.id and sm.inventory_id >0 and sq.id=sqm.quant_id group by sq.product_id"

                    self.cr.execute(get_adjusted_cost_query, ([moved_id]))
                    get_adjusted_cost = self.cr.dictfetchall()
                    for a_cost in get_adjusted_cost:
                        if a_cost.get('total') is not None:
                            cal_cost = a_cost.get('total')


                #  Floowing chekup for sq qty is zero then
                if cal_cost == 0:

                    get_adjusted_query = "select sq.cost,sm.price_unit from stock_quant_move_rel as sqm,stock_move as sm, stock_quant as sq where sqm.quant_id in (select sq2.id from stock_quant_move_rel as sqmr2, stock_quant as sq2 where sqmr2.move_id=%s and sqmr2.quant_id = sq2.id) and sqm.move_id = sm.id  and sq.id=sqm.quant_id and (sm.purchase_line_id >0 or sm.inventory_id >0) ORDER BY sm.id desc limit 1"

                    self.cr.execute(get_adjusted_query, ([moved_id]))
                    get_adjusted_cost = self.cr.dictfetchall()
                    for a_cost in get_adjusted_cost:
                        if a_cost.get('price_unit') is not None:
                            cal_cost = a_cost.get('price_unit') * delivered_sales_data.get('product_uom_qty')
                        elif a_cost.get('cost') is not None:
                            cal_cost = a_cost.get('cost') * delivered_sales_data.get('product_uom_qty')

                total_cost = cal_cost
                if total_cost:
                    total_cost=total_cost
                else:
                    total_cost=0


                if delivered_sales_data.get('picking_type_id') in incoming_ids:

                    total_sale = round((delivered_sales_data.get('price_unit') * delivered_sales_data.get('product_uom_qty')),2)

                    sale_profit = round(((total_sale - total_cost)*(-1)),2)

                    total_refund_sale= total_refund_sale + total_sale
                    month_wise_sales_return.update({
                        delivered_sales_data.get('order_id'):total_sale
                    })

                    if delivered_sales_data.get('date_done').split(" ")[0] ==  st_date:

                        todays_refund_sale = todays_refund_sale + total_sale


                else:
                    total_sale = round((delivered_sales_data.get('price_unit') * delivered_sales_data.get('product_uom_qty')),2)

                    sale_profit = round((total_sale - total_cost),2)

                    total_delivery_sale = total_delivery_sale + total_sale

                    if delivered_sales_data.get('date_done').split(" ")[0] == st_date:
                        todays_delivery_sale = todays_delivery_sale + total_sale

                profit = profit + sale_profit

            if delivered_sales_data.get('order_partner_id') in general_delivery_partner_id_list:

                general_cal_cost = delivered_sales_data.get('total_cost_price')
                general_moved_id = delivered_sales_data.get('moved_id')


                if general_cal_cost == 0:

                    general_get_adjusted_cost_query = "select sum((sq.qty * sm.price_unit)) as total from stock_quant_move_rel as sqm,stock_move as sm, stock_quant as sq where sqm.quant_id in ((select sq2.id from stock_quant_move_rel as sqmr2, stock_quant as sq2 where sqmr2.move_id=%s and sqmr2.quant_id = sq2.id)) and sqm.move_id = sm.id and sm.inventory_id >0 and sq.id=sqm.quant_id group by sq.product_id"

                    self.cr.execute(general_get_adjusted_cost_query, ([general_moved_id]))
                    general_get_adjusted_cost = self.cr.dictfetchall()
                    for general_a_cost in general_get_adjusted_cost:
                        if general_a_cost.get('total') is not None:
                            general_cal_cost = general_a_cost.get('total')


                #  Floowing chekup for sq qty is zero then
                if general_cal_cost == 0:

                    general_get_adjusted_query = "select sq.cost,sm.price_unit from stock_quant_move_rel as sqm,stock_move as sm, stock_quant as sq where sqm.quant_id in (select sq2.id from stock_quant_move_rel as sqmr2, stock_quant as sq2 where sqmr2.move_id=%s and sqmr2.quant_id = sq2.id) and sqm.move_id = sm.id  and sq.id=sqm.quant_id and (sm.purchase_line_id >0 or sm.inventory_id >0) ORDER BY sm.id desc limit 1"

                    self.cr.execute(general_get_adjusted_query, ([general_moved_id]))
                    general_get_adjusted_cost = self.cr.dictfetchall()
                    for general_a_cost in general_get_adjusted_cost:
                        if general_a_cost.get('price_unit') is not None:
                            general_cal_cost = general_a_cost.get('price_unit') * delivered_sales_data.get('product_uom_qty')
                        elif general_a_cost.get('cost') is not None:
                            general_cal_cost = general_a_cost.get('cost') * delivered_sales_data.get('product_uom_qty')

                general_total_cost = general_cal_cost
                if general_total_cost:
                    general_total_cost=general_total_cost
                else:
                    general_total_cost=0


                if delivered_sales_data.get('picking_type_id') in incoming_ids:

                    general_total_sale = round((delivered_sales_data.get('price_unit') * delivered_sales_data.get('product_uom_qty')),2)

                    general_sale_profit = round(((general_total_sale - general_total_cost)*(-1)),2)

                    general_total_refund_sale= general_total_refund_sale + general_total_sale

                    general_month_wise_sales_return.update({
                        delivered_sales_data.get('order_id'): general_total_sale
                    })

                    if delivered_sales_data.get('date_done').split(" ")[0] ==  st_date:
                        general_todays_refund_sale = general_todays_refund_sale + general_total_sale


                else:
                    general_total_sale = round((delivered_sales_data.get('price_unit') * delivered_sales_data.get('product_uom_qty')),2)

                    general_sale_profit = round((general_total_sale - general_total_cost),2)

                    general_total_delivery_sale = general_total_delivery_sale + general_total_sale

                    if delivered_sales_data.get('date_done').split(" ")[0] == st_date:
                        general_todays_delivery_sale = general_todays_delivery_sale + general_total_sale

                general_profit = general_profit + general_sale_profit


        result_delivery.update({'value':total_delivery_sale,'general_value':general_total_delivery_sale})
        result_refund.update({'value':total_refund_sale,'general_value':general_total_refund_sale})
        result_profit.update({'value':profit,'general_value':general_profit})
        result_todays_actual_delivery.update({'value':round(float(todays_delivery_sale - todays_refund_sale),2),
                                              'general_value':round(float(general_todays_delivery_sale - general_todays_refund_sale),2)})

        ###########################################

        ############# sales order #########

        query="SELECT so.id,so.partner_id,so.x_order_source,cus.email,so.date_confirm,so.amount_total as mtd_sales from sale_order so,res_partner cus  WHERE so.partner_id=cus.id and so.state not IN ('draft', 'sent', 'cancel', 'new_cus_approval_pending','approval_pending','check_pending')  and so.date_confirm >= %s and so.date_confirm <= %s"

        self.cr.execute(query, (from_date, st_date))
        sale_order_list = self.cr.dictfetchall()

        sales_partner_list = []
        retail_sales_partner_id_list = []
        general_sales_partner_id_list = []
        exception_list =[]
        #####  Get retail ID Lists

        partner_obj = self.pool("res.partner")
        for items in sale_order_list:
            sales_partner_list.append(items.get('partner_id'))

        sales_p_obj = partner_obj.browse(self.cr, self.uid, sales_partner_list, context=context)
        retail_customer_email={}
        general_customer_email={}
        retail_customer_parent_email={}
        general_customer_parent_email={}

        for sales_p_items in sales_p_obj:
            if sales_p_items.parent_id:
                if sales_p_items.x_classification is not False and sales_p_items.x_classification is not None:
                    sales_p_classification = sales_p_items.x_classification.lower()

                    if sales_p_classification == 'ananta':
                        pass
                    elif sales_p_classification == 'corporate':
                        pass
                    elif sales_p_classification == 'general':
                        general_sales_partner_id_list.append(sales_p_items.id)
                        general_customer_email.update({sales_p_items.id: sales_p_items.email})
                        general_customer_parent_email.update({sales_p_items.id: sales_p_items.parent_id.email})

                    elif sales_p_items.x_classification == '':
                        retail_sales_partner_id_list.append(sales_p_items.id)
                        retail_customer_email.update({sales_p_items.id:sales_p_items.email})
                        retail_customer_parent_email.update({ sales_p_items.id: sales_p_items.parent_id.email})

                    else:
                        retail_sales_partner_id_list.append(sales_p_items.id)
                        retail_customer_email.update({sales_p_items.id: sales_p_items.email})
                        retail_customer_parent_email.update({ sales_p_items.id: sales_p_items.parent_id.email})
                else:
                    exception_list.append(sales_p_items.id)

            else:
                sales_p_classification = sales_p_items.parent_id.x_classification if sales_p_items.parent_id.x_classification else sales_p_items.x_classification

                if sales_p_classification is not False and sales_p_classification is not None:
                    sales_p_classification = sales_p_items.x_classification.lower()

                    if sales_p_classification == 'ananta':
                        pass
                    elif sales_p_classification == 'corporate':
                        pass
                    elif sales_p_classification == 'general':
                        general_sales_partner_id_list.append(sales_p_items.id)
                        general_customer_email.update({sales_p_items.id: sales_p_items.email})
                        general_customer_parent_email.update({sales_p_items.id: sales_p_items.parent_id.email})
                    elif sales_p_items.x_classification == '':
                        retail_sales_partner_id_list.append(sales_p_items.id)
                        retail_customer_email.update({sales_p_items.id: sales_p_items.email})
                        retail_customer_parent_email.update({ sales_p_items.id: sales_p_items.parent_id.email})
                    else:
                        retail_sales_partner_id_list.append(sales_p_items.id)
                        retail_customer_email.update({sales_p_items.id: sales_p_items.email})
                        retail_customer_parent_email.update({sales_p_items.id: sales_p_items.parent_id.email})
                else:
                    exception_list.append(sales_p_items.id)

                        #### Ends Here
        retail_customer_count = list(set(retail_sales_partner_id_list))
        general_customer_count = list(set(general_sales_partner_id_list))

        if len(retail_customer_count)==0:
            raise osv.except_osv(_('Warning!!!!'),
                                 _('Data are not found in this date range..\n Please select the valid date'))

        customer_query = "SELECT id,partner_id from sale_order  WHERE state not IN ('draft', 'sent', 'cancel', 'new_cus_approval_pending','approval_pending','check_pending') and partner_id in %s and date_confirm < %s"


        self.cr.execute(customer_query, (tuple(retail_customer_count), from_date))

        customer_query_list = self.cr.dictfetchall()
        old_customer_id_list = [p['partner_id'] for p in customer_query_list]
        old_customer_dict = {}

        customer_query_list.sort(key=lambda datum: datum['partner_id'])

        for k1, g1 in itertools.groupby(customer_query_list, key=lambda datum: datum['partner_id']):
            old_customer_dict.update({
                int(k1): [i1['id'] for i1 in g1]
            })

        if general_customer_count:
            general_customer_query = "SELECT id,partner_id from sale_order  WHERE state not IN ('draft', 'sent', 'cancel', 'new_cus_approval_pending','approval_pending','check_pending') and partner_id in %s and date_confirm < %s"

            self.cr.execute(general_customer_query, (tuple(general_customer_count), from_date))

            general_customer_query_list = self.cr.dictfetchall()
            general_old_customer_id_list = [general_p['partner_id'] for general_p in general_customer_query_list]
            general_old_customer_dict = {}

            general_customer_query_list.sort(key=lambda datum1: datum1['partner_id'])

            for general_k1, general_g1 in itertools.groupby(general_customer_query_list, key=lambda datum1: datum1['partner_id']):
                general_old_customer_dict.update({
                    int(general_k1): [general_i1['id'] for general_i1 in general_g1]
                })


        ####### new register customer #####

        new_register_customer_id_list = partner_obj.search(self.cr, self.uid,
                                                  ['|', '|','|', ('x_classification', 'ilike', 'Retail'),
                                                   ('x_classification', 'ilike', 'SOHO'),
                                                   ('x_classification', 'ilike', ''),('x_classification', 'ilike', 'General'),
                                                   ('create_date', '>=', from_date_time), ('create_date', '<=', st_date_time)], context=context)

        new_register_customer_p_obj = partner_obj.browse(self.cr, self.uid, new_register_customer_id_list, context=context)

        retail_new_register_customer_id_list = []
        general_new_register_customer_id_list = []

        for new_register_customer_p_items in new_register_customer_p_obj:
            if new_register_customer_p_items.parent_id:
                if new_register_customer_p_items.x_classification is not False and new_register_customer_p_items.x_classification is not None:
                    new_register_customer_p_classification = new_register_customer_p_items.x_classification.lower()

                    if new_register_customer_p_classification == 'ananta':
                        pass
                    elif new_register_customer_p_classification == 'corporate':
                        pass
                    elif new_register_customer_p_classification == 'general':
                        general_new_register_customer_id_list.append(new_register_customer_p_items.id)
                    elif new_register_customer_p_classification == '':
                        retail_new_register_customer_id_list.append(new_register_customer_p_items.id)
                    else:
                        retail_new_register_customer_id_list.append(new_register_customer_p_items.id)
            else:
                new_register_customer_p_classification = new_register_customer_p_items.parent_id.x_classification if new_register_customer_p_items.parent_id.x_classification else new_register_customer_p_items.x_classification

                if new_register_customer_p_classification is not False and new_register_customer_p_classification is not None:
                    new_register_customer_p_classification = new_register_customer_p_items.x_classification.lower()

                    if new_register_customer_p_classification == 'ananta':
                        pass
                    elif new_register_customer_p_classification == 'corporate':
                        pass
                    elif new_register_customer_p_classification == 'general':
                        general_new_register_customer_id_list.append(new_register_customer_p_items.id)
                    elif new_register_customer_p_classification == '':
                        retail_new_register_customer_id_list.append(new_register_customer_p_items.id)

                    else:
                        retail_new_register_customer_id_list.append(new_register_customer_p_items.id)

        ###################

        ####### unique customer #####


        unique_customer_ids_list = partner_obj.search(self.cr, self.uid,
                                                      ['|', ('x_classification', 'ilike', 'Retail'),
                                                       ('x_classification', 'ilike', 'SOHO'), ('parent_id', '=', None),
                                                       ('create_date', '<=', st_date_time), ('customer', '=', True),
                                                    ('email', '!=', None),('active', '=', True)], context=context) ## total unique customer retail SOHO count ##

        general_unique_customer_ids_list = partner_obj.search(self.cr, self.uid,
                                                      [('x_classification', 'ilike', 'General'),
                                                       ('parent_id', '=', None),
                                                       ('create_date', '<=', st_date_time), ('customer', '=', True),
                                                       ('email', '!=', None), ('active', '=', True)],
                                                      context=context)  ## total unique customer general count ##

        all_unique_customer_ids_list = partner_obj.search(self.cr, self.uid,
                                                  ['|', '|', '|','|',('x_classification', 'ilike', 'Retail'),
                                                   ('x_classification', 'ilike', 'SOHO'),('x_classification', 'ilike', 'General'),
                                                   ('x_classification', '=',None),('x_classification', '=', ''),
                                                   ('parent_id', '=', None),
                                                   ('create_date','<=', st_date_time),('customer', '=', True),
                                                   ('email', '!=', None),('active', '=', True) ], context=context)


        unique_customer_id_list = partner_obj.search(self.cr, self.uid, [('parent_id', 'in', all_unique_customer_ids_list)],context=context)



        list_of_unique_ids = all_unique_customer_ids_list + unique_customer_id_list

        unique_customer_sales_p_obj = partner_obj.browse(self.cr, self.uid, list_of_unique_ids, context=context)

        retail_sales_unique_partner_id_list = []
        general_sales_unique_partner_id_list = []

        for unique_customer_sales_p_items in unique_customer_sales_p_obj:
            if unique_customer_sales_p_items.parent_id:
                if unique_customer_sales_p_items.x_classification is not False and unique_customer_sales_p_items.x_classification is not None:
                    unique_customer_sales_p_classification = unique_customer_sales_p_items.x_classification.lower()

                    if unique_customer_sales_p_classification == 'ananta':
                        pass
                    elif unique_customer_sales_p_classification == 'corporate':
                        pass
                    elif unique_customer_sales_p_classification == 'general':
                        general_sales_unique_partner_id_list.append(unique_customer_sales_p_items.id)

                    else:
                        retail_sales_unique_partner_id_list.append(unique_customer_sales_p_items.id)
            else:
                unique_customer_sales_p_classification = unique_customer_sales_p_items.parent_id.x_classification if unique_customer_sales_p_items.parent_id.x_classification else unique_customer_sales_p_items.x_classification

                if unique_customer_sales_p_classification is not False and unique_customer_sales_p_classification is not None:
                    unique_customer_sales_p_classification = unique_customer_sales_p_items.x_classification.lower()

                    if unique_customer_sales_p_classification == 'ananta':
                        pass
                    elif unique_customer_sales_p_classification == 'corporate':
                        pass
                    elif unique_customer_sales_p_classification == 'general':
                        general_sales_unique_partner_id_list.append(unique_customer_sales_p_items.id)

                    else:
                        retail_sales_unique_partner_id_list.append(unique_customer_sales_p_items.id)

        #########################

        #########################
        ######retail

        regular_customer_dict = {}
        three_month_sale_query = "SELECT id, partner_id,split_part(date_confirm::TEXT,'-', 2) date_confirm  from sale_order WHERE state not IN ('draft', 'sent', 'cancel', 'new_cus_approval_pending','approval_pending','check_pending') and partner_id in %s and date_confirm >= %s and date_confirm <= %s"

        self.cr.execute(three_month_sale_query, (tuple(retail_customer_count), three_months_date, st_date))
        three_month_sale_order_list = self.cr.dictfetchall()

        three_month_sale_order_list.sort(key=lambda datum: datum['partner_id'])

        for k2, g2 in itertools.groupby(three_month_sale_order_list, key=lambda datum: datum['partner_id']):
            regular_customer_dict.update({
                int(k2): len(list(set([i2['date_confirm'] for i2 in g2])))
            })

        ######general

        general_regular_customer_dict = {}
        general_three_month_sale_query = "SELECT id, partner_id,split_part(date_confirm::TEXT,'-', 2) date_confirm  from sale_order WHERE state not IN ('draft', 'sent', 'cancel', 'new_cus_approval_pending','approval_pending','check_pending') and partner_id in %s and date_confirm >= %s and date_confirm <= %s"

        self.cr.execute(general_three_month_sale_query, (tuple(general_customer_count), three_months_date, st_date))
        general_three_month_sale_order_list = self.cr.dictfetchall()

        general_three_month_sale_order_list.sort(key=lambda datum2: datum2['partner_id'])

        for general_k2, general_g2 in itertools.groupby(general_three_month_sale_order_list, key=lambda datum3: datum3['partner_id']):
            general_regular_customer_dict.update({
                int(general_k2): len(list(set([general_i2['date_confirm'] for general_i2 in general_g2])))
            })

        ###########################

        sale_order_id_list=[]
        sale_order_partner_id_list=[]
        new_customer_list = []
        old_customer_list = []
        multi_order_customer_list = []
        child_account_list = []
        # app_child_account_list = []
        # app_parent_account_list = []
        sales_regular_customer_list = []
        sales_irregular_customer_list = []
        unique_sales_order_customer_list = []
        # new_register_sales_order_customer_list = []

        mtd_sales = 0
        current_month_total_refund_sale = 0.0

        general_sale_order_id_list = []
        general_sale_order_partner_id_list = []
        general_new_customer_list = []
        general_old_customer_list = []
        general_multi_order_customer_list = []
        general_child_account_list = []
        general_sales_regular_customer_list = []
        general_sales_irregular_customer_list = []
        general_unique_sales_order_customer_list = []
        general_mtd_sales = 0
        general_current_month_total_refund_sale = 0.0


        for sale_order_data_row in sale_order_list:

            if sale_order_data_row.get('partner_id') in retail_sales_partner_id_list:
                sale_order_id_list.append(sale_order_data_row.get('id'))
                sale_order_partner_id_list.append(sale_order_data_row.get('partner_id'))
                mtd_sales=mtd_sales+sale_order_data_row.get('mtd_sales')

                match = re.search(r'sin[0-9]\w+', retail_customer_email[sale_order_data_row.get('partner_id')])
                if match is not None:
                    child_account_list.append(sale_order_data_row.get('partner_id'))
                    # if sale_order_data_row.get('x_order_source') == 'Android App':
                    #     app_child_account_list.append(sale_order_data_row.get('partner_id'))
                # else:
                #     if sale_order_data_row.get('x_order_source') == 'Android App':
                #         app_parent_account_list.append(sale_order_data_row.get('partner_id'))

                if sale_order_data_row.get('partner_id') in retail_sales_unique_partner_id_list:
                    # unique_sales_order_customer_list.append(sale_order_data_row.get('partner_id'))
                    if retail_customer_parent_email.get(sale_order_data_row.get('partner_id')):
                        email = retail_customer_parent_email[sale_order_data_row.get('partner_id')]
                    else:
                        email = retail_customer_email[sale_order_data_row.get('partner_id')]

                    unique_compamy_match = re.search(r'sin[0-9]\w+', email)

                    if unique_compamy_match is None:
                        unique_sales_order_customer_list.append(sale_order_data_row.get('partner_id'))

                        if old_customer_id_list and sale_order_data_row.get('partner_id') not in old_customer_id_list:
                            new_customer_list.append(sale_order_data_row.get('partner_id'))
                        else:
                            old_customer_list.append(sale_order_data_row.get('partner_id'))
                            order_count = len(
                                old_customer_dict.get(sale_order_data_row.get('partner_id'))) if old_customer_dict.get(
                                sale_order_data_row.get('partner_id')) else 0
                            if order_count > 1:
                                multi_order_customer_list.append(sale_order_data_row.get('partner_id'))

                # if sale_order_data_row.get('partner_id') in retail_new_register_customer_id_list:
                #     new_register_sales_order_customer_list.append(sale_order_data_row.get('partner_id'))

                        if regular_customer_dict.has_key(sale_order_data_row.get('partner_id')):
                            if regular_customer_dict.get(sale_order_data_row.get('partner_id'))==4:
                                sales_regular_customer_list.append(sale_order_data_row.get('partner_id'))

                            else:
                                sales_irregular_customer_list.append(sale_order_data_row.get('partner_id'))

                if month_wise_sales_return.has_key(sale_order_data_row.get('id')):
                    current_month_total_refund_sale = current_month_total_refund_sale + month_wise_sales_return.get(sale_order_data_row.get('id'))

            if sale_order_data_row.get('partner_id') in general_sales_partner_id_list:
                general_sale_order_id_list.append(sale_order_data_row.get('id'))
                general_sale_order_partner_id_list.append(sale_order_data_row.get('partner_id'))
                general_mtd_sales = general_mtd_sales+sale_order_data_row.get('mtd_sales')

                match = re.search(r'sin[0-9]\w+', general_customer_email[sale_order_data_row.get('partner_id')])
                if match is not None:
                    general_child_account_list.append(sale_order_data_row.get('partner_id'))

                if sale_order_data_row.get('partner_id') in general_sales_unique_partner_id_list:

                    if general_customer_parent_email.get(sale_order_data_row.get('partner_id')):
                        email = general_customer_parent_email[sale_order_data_row.get('partner_id')]
                    else:
                        email = general_customer_email[sale_order_data_row.get('partner_id')]

                    general_unique_compamy_match = re.search(r'sin[0-9]\w+', email)

                    if general_unique_compamy_match is None:
                        general_unique_sales_order_customer_list.append(sale_order_data_row.get('partner_id'))

                        if general_old_customer_id_list and sale_order_data_row.get('partner_id') not in general_old_customer_id_list:
                            general_new_customer_list.append(sale_order_data_row.get('partner_id'))
                        else:
                            general_old_customer_list.append(sale_order_data_row.get('partner_id'))
                            general_order_count = len(
                                general_old_customer_dict.get(sale_order_data_row.get('partner_id'))) if general_old_customer_dict.get(
                                sale_order_data_row.get('partner_id')) else 0
                            if general_order_count > 1:
                                general_multi_order_customer_list.append(sale_order_data_row.get('partner_id'))

                        if general_regular_customer_dict.has_key(sale_order_data_row.get('partner_id')):
                            if general_regular_customer_dict.get(sale_order_data_row.get('partner_id'))==4:
                                general_sales_regular_customer_list.append(sale_order_data_row.get('partner_id'))

                            else:
                                general_sales_irregular_customer_list.append(sale_order_data_row.get('partner_id'))

                if general_month_wise_sales_return.has_key(sale_order_data_row.get('id')):
                    general_current_month_total_refund_sale = general_current_month_total_refund_sale + general_month_wise_sales_return.get(sale_order_data_row.get('id'))

        so_data = self.pool("sale.order").browse(self.cr, self.uid, sale_order_id_list, context=context)
        invoice_amount = 0.00
        for so in so_data:
            invoice_amount+= so.delivered_amount

        general_so_data = self.pool("sale.order").browse(self.cr, self.uid, general_sale_order_id_list, context=context)
        general_invoice_amount = 0.00
        for general_so in general_so_data:
            general_invoice_amount+= general_so.delivered_amount


        result_current_month_refund.update({'value': current_month_total_refund_sale, 'general_value': general_current_month_total_refund_sale})

        new_customer_sales=list(set(new_customer_list))
        old_customer_sales=list(set(old_customer_list))
        multi_order_customer_sales=list(set(multi_order_customer_list))
        child_account=list(set(child_account_list))
        # app_child_account=list(set(app_child_account_list))
        # app_parent_account=list(set(app_parent_account_list))
        irregular_customer_sales=list(set(sales_irregular_customer_list))
        regular_customer_sales=list(set(sales_regular_customer_list))

        unique_customer_count = len(list(set(unique_customer_ids_list)))
        so_unique_customer_count = len(list(set(unique_sales_order_customer_list)))
        # no_so_unique_customer = int(unique_customer_count) - int(so_unique_customer_count)

        # new_register_customer_count = list(set(new_register_customer_id_list))
        # so_new_register_customer_count = list(set(new_register_sales_order_customer_list))

        general_new_customer_sales=list(set(general_new_customer_list))
        general_old_customer_sales=list(set(general_old_customer_list))
        general_multi_order_customer_sales=list(set(general_multi_order_customer_list))
        general_child_account=list(set(general_child_account_list))
        general_irregular_customer_sales=list(set(general_sales_irregular_customer_list))
        general_regular_customer_sales=list(set(general_sales_regular_customer_list))

        general_unique_customer_count = len(list(set(general_unique_customer_ids_list)))
        general_so_unique_customer_count = len(list(set(general_unique_sales_order_customer_list)))


        result_mtd_sales_order.update({'value':len(sale_order_id_list),'general_value':len(general_sale_order_id_list)})
        result_mtd_sales.update({'value':mtd_sales,'general_value':general_mtd_sales})
        result_sales_customer.update({'value':len(list(set(sale_order_partner_id_list))),
                                      'general_value':len(list(set(general_sale_order_partner_id_list)))})

        # result_new_register_customer_sales.update({'value':len(so_new_register_customer_count)})
        # result_new_register_customer.update({'value':len(new_register_customer_count)})

        result_sales_new_customer.update({'value':len(new_customer_sales),'general_value':len(general_new_customer_sales),})
        result_sales_old_customer.update({'value':len(old_customer_sales),'general_value':len(general_old_customer_sales)})

        result_child_account_order.update({'value':len(child_account),'general_value':len(general_child_account)})

        result_unique_customer.update({'value': unique_customer_count,'general_value': general_unique_customer_count})
        result_so_unique_customer.update({'value': so_unique_customer_count,'general_value': general_so_unique_customer_count})
        # result_no_so_unique_customer.update({'value': no_so_unique_customer})
        # result_sales_regular_unique_customer.update({'value': len(regular_unique_customer_sales)})
        result_sales_regular_customer.update({'value': len(regular_customer_sales),'general_value': len(general_regular_customer_sales)})
        result_sales_irregular_customer.update({'value': len(irregular_customer_sales) - len(new_customer_sales),
                                                'general_value': len(general_irregular_customer_sales) - len(general_new_customer_sales)})

        # result_app_child_account_order.update({'value': len(app_child_account)})
        # result_app_parent_account_order.update({'value': len(app_parent_account)})

        ##################################


        ###############todays's data#############

        today_query = "SELECT partner_id,amount_total as mtd_sales,id from sale_order so  WHERE state not IN ('draft', 'sent', 'cancel', 'new_cus_approval_pending','approval_pending','check_pending')  and date_confirm = %s"

        self.cr.execute(today_query, (st_date,))
        sale_order_today_list = self.cr.dictfetchall()

        if len(sale_order_today_list)>0:

            today_sales_partner_list = []
            today_retail_sales_partner_id_list = []
            today_general_sales_partner_id_list = []

            #####  Get retail, general ID Lists

            for items in sale_order_today_list:
                today_sales_partner_list.append(items.get('partner_id'))

            today_sales_p_obj = partner_obj.browse(self.cr, self.uid, today_sales_partner_list, context=context)

            for today_sales_p_items in today_sales_p_obj:
                if today_sales_p_items.parent_id:
                    if today_sales_p_items.x_classification is not False and today_sales_p_items.x_classification is not None:
                        today_sales_p_classification = today_sales_p_items.x_classification.lower()

                        if today_sales_p_classification == 'ananta':
                            pass
                        elif today_sales_p_classification == 'corporate':
                            pass
                        elif today_sales_p_classification == 'general':
                            today_general_sales_partner_id_list.append(today_sales_p_items.id)
                        elif today_sales_p_classification == '':
                            today_retail_sales_partner_id_list.append(today_sales_p_items.id)
                        else:
                            today_retail_sales_partner_id_list.append(today_sales_p_items.id)
                else:
                    today_sales_p_classification = today_sales_p_items.parent_id.x_classification if today_sales_p_items.parent_id.x_classification else today_sales_p_items.x_classification

                    if today_sales_p_classification is not False and today_sales_p_classification is not None:
                        today_sales_p_classification = today_sales_p_items.x_classification.lower()

                        if today_sales_p_classification == 'ananta':
                            pass
                        elif today_sales_p_classification == 'corporate':
                            pass
                        elif today_sales_p_classification == 'general':
                            today_general_sales_partner_id_list.append(today_sales_p_items.id)
                        elif today_sales_p_classification == '':
                            today_retail_sales_partner_id_list.append(today_sales_p_items.id)
                        else:
                            today_retail_sales_partner_id_list.append(today_sales_p_items.id)

                            #### Ends Here

            sale_order_today_id_list = []
            sale_order_today_partner_id_list = []
            todays_mtd_sales = 0

            general_sale_order_today_id_list = []
            general_sale_order_today_partner_id_list = []
            general_todays_mtd_sales = 0

            for sale_order_today_data_row in sale_order_today_list:
                if sale_order_today_data_row.get('partner_id') in today_retail_sales_partner_id_list:
                    todays_mtd_sales=todays_mtd_sales+sale_order_today_data_row.get('mtd_sales')
                    sale_order_today_id_list.append(sale_order_today_data_row.get('id'))
                    sale_order_today_partner_id_list.append(sale_order_today_data_row.get('partner_id'))

                if sale_order_today_data_row.get('partner_id') in today_general_sales_partner_id_list:
                    general_todays_mtd_sales = general_todays_mtd_sales+sale_order_today_data_row.get('mtd_sales')
                    general_sale_order_today_id_list.append(sale_order_today_data_row.get('id'))
                    general_sale_order_today_partner_id_list.append(sale_order_today_data_row.get('partner_id'))

            result_today_mtd_sales.update({'value':todays_mtd_sales,'general_value':general_todays_mtd_sales})
            result_today_mtd_sales_order.update({'value':len(sale_order_today_id_list),'general_value':len(general_sale_order_today_id_list)})
            result_today_sales_customer.update({'value':len(list(set(sale_order_today_partner_id_list))),'general_value':len(list(set(general_sale_order_today_partner_id_list)))})

        else:
            result_today_mtd_sales.update({'value':0,'general_value':0})
            result_today_mtd_sales_order.update({'value':0,'general_value':0})
            result_today_sales_customer.update({'value':0,'general_value':0})

        ###############

        sales_target = float(target_dict.get('value')) if target_dict.get('value') else 0
        delivery_target = float(delivery_target_dict.get('value')) if delivery_target_dict.get('value') else 0
        mtd_total_sales = float(result_mtd_sales.get('value')) if result_mtd_sales.get('value') else 0
        mtd_sales_order = float(result_mtd_sales_order.get('value')) if result_mtd_sales_order.get('value') else 0
        mtd_sales_customer = int(result_sales_customer.get('value')) if result_sales_customer.get('value') else 0
        delivery_total = float(result_delivery.get('value')) if result_delivery.get('value') else 0
        refund_total = float(result_refund.get('value')) if result_refund.get('value') else 0
        current_month_refund_total = float(result_current_month_refund.get('value')) if result_current_month_refund.get('value') else 0
        total_profit = float(result_profit.get('value')) if result_profit.get('value') else 0
        total_child_account_order = int(result_child_account_order.get('value')) if result_child_account_order.get('value') else 0
        total_parent_account_order = mtd_sales_customer-total_child_account_order
        total_invoice_amount = float(invoice_amount)

        general_sales_target = float(target_dict.get('value')) if target_dict.get('value') else 0
        general_delivery_target = float(delivery_target_dict.get('value')) if delivery_target_dict.get('value') else 0
        general_mtd_total_sales = float(result_mtd_sales.get('general_value')) if result_mtd_sales.get('general_value') else 0
        general_mtd_sales_order = float(result_mtd_sales_order.get('general_value')) if result_mtd_sales_order.get('general_value') else 0
        general_mtd_sales_customer = int(result_sales_customer.get('general_value')) if result_sales_customer.get('general_value') else 0
        general_delivery_total = float(result_delivery.get('general_value')) if result_delivery.get('general_value') else 0
        general_refund_total = float(result_refund.get('general_value')) if result_refund.get('general_value') else 0
        general_current_month_refund_total = float(result_current_month_refund.get('general_value')) if result_current_month_refund.get('general_value') else 0
        general_total_profit = float(result_profit.get('general_value')) if result_profit.get('general_value') else 0
        general_total_child_account_order = int(result_child_account_order.get('general_value')) if result_child_account_order.get(
            'general_value') else 0
        general_total_parent_account_order = general_mtd_sales_customer - general_total_child_account_order
        general_total_invoice_amount = float(general_invoice_amount)


        sales_avg.update({'value':str(round((mtd_total_sales/sales_target),2)*100)+'%' if sales_target else 0,
                          'general_value':str(round((general_mtd_total_sales/general_sales_target),2)*100)+'%' if general_sales_target else 0})

        avg_order_size.update({'value':round((mtd_total_sales/mtd_sales_order),2),
                               'general_value':round((general_mtd_total_sales/general_mtd_sales_order),2)})

        result_sales_multi_order_customer.update({'value':len(multi_order_customer_sales),
                                                  'general_value':len(general_multi_order_customer_sales)})

        result_sales_repeat_customer_percent.update(
            {'value': str(round((len(old_customer_sales) / float(mtd_sales_customer)), 2) * 100) + '%' if len(old_customer_sales) and mtd_sales_customer else 0,
            'general_value': str(round((len(general_old_customer_sales) / float(general_mtd_sales_customer)), 2) * 100) + '%' if len(general_old_customer_sales) and general_mtd_sales_customer else 0})


        total_customer_sales_avg.update({'value':round((mtd_total_sales/mtd_sales_customer),2),
                                         'general_value':round((general_mtd_total_sales/general_mtd_sales_customer),2)})

        result_parent_account_order.update({'value':total_parent_account_order,
                                            'general_value':general_total_parent_account_order})

        req_sales_day.update({'value':round(((sales_target-mtd_total_sales)/int(last_date_duration)),2),
                              'general_value':round(((general_sales_target-general_mtd_total_sales)/int(last_date_duration)),2)})

        sales_day.update({'value':round((mtd_total_sales/int(frist_date_duration)),2),
                          'general_value':round((general_mtd_total_sales/int(frist_date_duration)),2)})

        actual_delivery_sales.update({'value':round((delivery_total-refund_total),2),
                                      'general_value':round((general_delivery_total-general_refund_total),2)})

        delivery_sales_avg.update({'value':str(round(((delivery_total-refund_total)/mtd_total_sales),2)*100)+'%' if delivery_total and mtd_total_sales else 0,
                                   'general_value':str(round(((general_delivery_total-general_refund_total)/general_mtd_total_sales),2)*100)+'%' if general_delivery_total and general_mtd_total_sales else 0})

        # refund_sales_avg.update({'value':str(round((refund_total/mtd_total_sales),2)*100)+'%' if refund_total and mtd_total_sales else 0})
        refund_sales_avg.update({'value':str(round((refund_total/total_invoice_amount),2)*100)+'%' if refund_total and total_invoice_amount else 0,
                                 'general_value':str(round((general_refund_total/general_total_invoice_amount),2)*100)+'%' if general_refund_total and general_total_invoice_amount else 0})

        total_profit_avg.update({'value':str(round((total_profit/(delivery_total-refund_total)),3)*100)+'%' if total_profit and (delivery_total-refund_total) else 0,
                                 'general_value':str(round((general_total_profit/(general_delivery_total-general_refund_total)),3)*100)+'%' if general_total_profit and (general_delivery_total-general_refund_total) else 0})

        ###########################

        final_data.append(target_dict) # Target

        final_data.append(result_today_mtd_sales) # Todays Sales
        final_data.append(result_mtd_sales) # MTD Sales
        final_data.append(sales_avg) # Sales Achv
        final_data.append(avg_order_size) #Avg. Order Size
        final_data.append(total_customer_sales_avg) #Avg. Cart Value/Customer
        final_data.append(req_sales_day) # Required Sales/Day
        final_data.append(sales_day) # Sales/Day

        final_data.append(delivery_target_dict) # Delivery Target
        final_data.append(result_todays_actual_delivery) # Todays Delivery/Sales
        final_data.append(actual_delivery_sales) # MTD Delivery/Sales
        final_data.append(delivery_sales_avg) # Del. Achv./Sales

        final_data.append(result_refund) # MTD Refund on Sales
        final_data.append(result_current_month_refund) # MTD Refund on Current Month Sales
        final_data.append(refund_sales_avg) # Refund%/Sales

        final_data.append(total_profit_avg) # GPM

        final_data.append(result_today_sales_customer)  # Number. of Customer Place order
        final_data.append(result_sales_customer)  # MTD Number. of Customer Place order

        final_data.append(result_unique_customer)  # Unique Customer
        final_data.append(result_so_unique_customer)  # MTD Number. of Unique Customer Placed Order
        # final_data.append(result_no_so_unique_customer)  # MTD Number. of Unique Customer doesn't Placed Order

        final_data.append(result_sales_new_customer)  # MTD New Customer Place Order
        final_data.append(result_sales_old_customer)  # MTD Old Customer Place Order

        final_data.append(result_sales_multi_order_customer)  # Repeat Customer Order
        final_data.append(result_sales_repeat_customer_percent)  # Repeat Customer %

        final_data.append(result_sales_regular_customer)  # MTD Regular Customer Place Order
        final_data.append(result_sales_irregular_customer)  # MTD Irregular Customer Place Order
        # final_data.append(result_sales_regular_unique_customer)  # MTD Regular Customer Place Order


        # final_data.append(result_new_register_customer)  # MTD New Register Customer
        # final_data.append(result_new_register_customer_sales)  # MTD New Register Customer Place Order

        final_data.append(result_parent_account_order)  # Parent Account
        final_data.append(result_child_account_order)  # Child Account

        final_data.append(result_today_mtd_sales_order)  # Number. of Order Place
        final_data.append(result_mtd_sales_order)  # MTD Number. of Order Place

        # final_data.append(result_app_parent_account_order)  # Parent Account App
        # final_data.append(result_app_child_account_order)  # Child Account App

        for data_items in final_data:
            c_specs = map(
                lambda x: self.render(x, self.col_specs_template, 'lines'),
                wanted_list)

            for list_data in c_specs:

                if str(list_data[0]) == str('name'):
                    list_data[4] = str(data_items.get('name'))

                if str(list_data[0]) == str('description'):
                    list_data[4] = str(data_items.get('description'))

                if str(list_data[0]) == str('value'):
                    month_1_data  = str(data_items.get('value'))
                    list_data[4] = month_1_data
                    list_data[4] = str(month_1_data.replace("None", ""))

                if str(list_data[0]) == str('general_value'):
                    month_1_data  = str(data_items.get('general_value'))
                    list_data[4] = month_1_data
                    list_data[4] = str(month_1_data.replace("None", ""))

            row_data = self.xls_row_template(c_specs, [x[0] for x in c_specs])
            row_pos = self.xls_write_row(
                ws, row_pos, row_data, row_style=self.aml_cell_style)

        # self.cr.execute("delete from retail_dashboard where create_date >= %s and create_date <= %s",(datetime.today().strftime("%Y-%m-%d 00:00:00"),datetime.today().strftime("%Y-%m-%d 23:59:59")))
        # self.cr.commit()



        retail_dashboard_data={
            'target_amount ': target_dict['value'],
            'today_mtd_sales_amount': result_today_mtd_sales['value'],
            'sales_avg':round((mtd_total_sales/sales_target),2)*100 if sales_target else 0,
            'mtd_sales_amount': result_mtd_sales['value'],
            'customer_sales_avg': total_customer_sales_avg['value'],
            'avg_order_size': avg_order_size['value'],
            'req_sales_day': req_sales_day['value'],
            'sales_day': sales_day['value'],
            'delivery_sales_avg': round(((delivery_total-refund_total)/mtd_total_sales),2)*100 if delivery_total and mtd_total_sales else 0,
            'actual_delivery_sales_amount': actual_delivery_sales['value'],
            'todays_actual_delivery_amount': result_todays_actual_delivery['value'],
            'refund_amount': result_refund['value'],
            'refund_sales_avg': round((refund_total/mtd_total_sales),2)*100 if refund_total and mtd_total_sales else 0,
            'gpm': round((total_profit/mtd_total_sales),3)*100 if total_profit and mtd_total_sales else 0,
            'today_sales_customer_count': result_today_sales_customer['value'],
            'mtd_sales_customer_count': result_sales_customer['value'],
            'total_unique_customer_count': result_unique_customer['value'],
            'mtd_so_unique_customer_count': result_so_unique_customer['value'],
            'mtd_so_unique_new_customer_count': result_sales_new_customer['value'],
            'mtd_so_unique_repeat_customer_count': result_sales_old_customer['value'],
            'mtd_so_unique_multi_order_customer_count': result_sales_multi_order_customer['value'],
            'mtd_so_unique_repeat_customer_percent': round((len(old_customer_sales) / float(mtd_sales_customer)), 2) * 100 if len(old_customer_sales) and mtd_sales_customer else 0,
            'mtd_so_unique_regular_customer_count': result_sales_regular_customer['value'],
            'mtd_so_unique_irregular_customer_count': result_sales_irregular_customer['value'],
            'mtd_parent_account_order_count': result_parent_account_order['value'],
            'mtd_child_account_order_count': result_child_account_order['value'],
            'today_mtd_sales_order_placed_count':
                result_today_mtd_sales_order['value'],
            'mtd_sales_order_placed_count': result_mtd_sales_order['value'],
            'from_date': from_date,
            'to_date': st_date,
            'today_date': st_date
        }

        self.pool.get('retail.dashboard').create(self.cr, self.uid, retail_dashboard_data, context=context)

retail_dashboard_xls('report.retail.dashboard.xls',
                    'sale.order',
                    parser=retail_dashboard_xls_parser)

# Author S. M. Sazedul Haque 2018/11/06

import xlwt
from datetime import datetime, timedelta
import time
from openerp.osv import orm
from openerp.report import report_sxw
from openerp.addons.report_xls.report_xls import report_xls
from openerp.addons.report_xls.utils import rowcol_to_cell, _render
from openerp.tools.translate import translate, _
import logging
import re
from ast import literal_eval as make_tuple

_logger = logging.getLogger(__name__)


_ir_translation_name = 'unique.customer.order.xls'


class unique_customer_order_xls_parser(report_sxw.rml_parse):

    def __init__(self, cr, uid, name, context):
        super(unique_customer_order_xls_parser, self).__init__(
            cr, uid, name, context=context)
        move_obj = self.pool.get('sale.order')
        self.context = context
        wanted_list = move_obj._report_xls_fields_customer_order(cr, uid, context)
        template_changes = move_obj._report_xls_template_customer_order(cr, uid, context)
        self.localcontext.update({
            'datetime': datetime,
            'wanted_list': wanted_list,
            'template_changes': template_changes,
            '_': self._,
        })

    def _(self, src):
        lang = self.context.get('lang', 'en_US')
        return translate(self.cr, _ir_translation_name, 'report', lang, src) \
            or src


class unique_customer_order_xls(report_xls):

    def __init__(self, name, table, rml=False, parser=False, header=True, store=False, context=None):
        super(unique_customer_order_xls, self).__init__(
            name, table, rml, parser, header, store)
        # Cell Styles
        _xs = self.xls_styles
        # header
        rh_cell_format = _xs['bold'] + _xs['fill'] + _xs['borders_all']
        self.rh_cell_style = xlwt.easyxf(rh_cell_format)
        self.rh_cell_style_center = xlwt.easyxf(rh_cell_format + _xs['center'])
        self.rh_cell_style_right = xlwt.easyxf(rh_cell_format + _xs['right'])
        # lines
        aml_cell_format = _xs['borders_all']
        self.aml_cell_style = xlwt.easyxf(aml_cell_format)
        self.aml_cell_style_center = xlwt.easyxf(
            aml_cell_format + _xs['center'])
        self.aml_cell_style_date = xlwt.easyxf(
            aml_cell_format + _xs['left'],
            num_format_str=report_xls.date_format)
        self.aml_cell_style_decimal = xlwt.easyxf(
            aml_cell_format + _xs['right'],
            num_format_str=report_xls.decimal_format)
        # totals
        rt_cell_format = _xs['bold'] + _xs['fill'] + _xs['borders_all']
        self.rt_cell_style = xlwt.easyxf(rt_cell_format)
        self.rt_cell_style_right = xlwt.easyxf(rt_cell_format + _xs['right'])
        self.rt_cell_style_decimal = xlwt.easyxf(
            rt_cell_format + _xs['right'],
            num_format_str=report_xls.decimal_format)

        self.col_specs_template = {

            'order_number': {
                'header': [1, 20, 'text', _render("_('Order Number')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'customer_name': {
                'header': [1, 20, 'text', _render("_('Customer Name')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'customer_classification': {
                'header': [1, 20, 'text', _render("_('Classification')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'confirm_date': {
                'header': [1, 20, 'text', _render("_('Confirm Date')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'order_amount': {
                'header': [1, 20, 'text', _render("_('Order Amount')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'company_name': {
                'header': [1, 20, 'text', _render("_('Compamy Name')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'customer_email': {
                'header': [1, 20, 'text', _render("_('Customer Email')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'company_email': {
                'header': [1, 20, 'text', _render("_('Company Email')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'child_email': {
                'header': [1, 20, 'text', _render("_('Email')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]}
        }
        # XLS Template

    def generate_xls_report(self, _p, _xs, data, objects, wb):

        wanted_list = _p.wanted_list
        self.col_specs_template.update(_p.template_changes)
        _ = _p._

        context = self.context
        from_date = data.get('form').get('date_from')
        st_date = data.get('form').get('date_to')
        context['start_date'] = from_date
        context['end_date'] = st_date
        self.context = context
        date_range = _("Date: %s to %s" % (from_date,st_date))

        # report_name = objects[0]._description or objects[0]._name
        report_name = _("Unique Customer Report")
        ws = wb.add_sheet(report_name[:31])
        ws.panes_frozen = True
        ws.remove_splits = True
        ws.portrait = 0  # Landscape
        ws.fit_width_to_pages = 1
        row_pos = 0

        # set print header/footer
        ws.header_str = self.xls_headers['standard']
        ws.footer_str = self.xls_footers['standard']

        # Title
        cell_style = xlwt.easyxf(_xs['xls_title'])
        c_specs = [
            ('report_name', 4, 3, 'text', report_name),
        ]
        row_data = self.xls_row_template(c_specs, ['report_name'])
        row_pos = self.xls_write_row(
            ws, row_pos, row_data, row_style=cell_style)
        row_pos += 1

        cell_style = xlwt.easyxf(_xs['xls_title'])
        c_specs = [
            ('date_range', 4, 3, 'text', date_range),
        ]
        row_data = self.xls_row_template(c_specs, ['date_range'])
        row_pos = self.xls_write_row(
            ws, row_pos, row_data, row_style=cell_style)
        row_pos += 1
        # Column headers
        c_specs = map(lambda x: self.render(
            x, self.col_specs_template, 'header', render_space={'_': _p._}),
            wanted_list)
        row_data = self.xls_row_template(c_specs, [x[0] for x in c_specs])
        row_pos = self.xls_write_row(
            ws, row_pos, row_data, row_style=self.rh_cell_style,
            set_column_size=True)
        ws.set_horz_split_pos(row_pos)

        from_date_time = from_date + ' 00:00:00'
        st_date_time = st_date + ' 23:59:59'

        #########################################
        query = "SELECT id, client_order_ref, partner_id ,date_confirm, amount_total  from sale_order WHERE state not IN ('draft', 'sent', 'cancel', 'new_cus_approval_pending','approval_pending','check_pending')  and date_confirm >= %s and date_confirm <= %s"

        self.cr.execute(query, (from_date, st_date))
        sale_order_list = self.cr.dictfetchall()

        partner_obj = self.pool.get('res.partner')

        all_unique_customer_ids_list = partner_obj.search(self.cr, self.uid,
                                                          ['|', '|', '|', '|', ('x_classification', 'ilike', 'Retail'),
                                                           ('x_classification', 'ilike', 'SOHO'),
                                                           ('x_classification', 'ilike', 'General'),
                                                           ('x_classification', '=', None),
                                                           ('x_classification', '=', ''),
                                                           ('parent_id', '=', None),('create_date', '>=', from_date_time),
                                                           ('create_date', '<=', st_date_time), ('customer', '=', True),
                                                           ('email', '!=', None), ('active', '=', True)],
                                                          context=context)

        unique_customer_id_list = partner_obj.search(self.cr, self.uid,
                                                     [('parent_id', 'in', all_unique_customer_ids_list)],
                                                     context=context)

        list_of_unique_ids = all_unique_customer_ids_list + unique_customer_id_list

        unique_customer_sales_p_obj = partner_obj.browse(self.cr, self.uid, list_of_unique_ids, context=context)

        final_company_list = {}
        data_list=[]

        for p_items in unique_customer_sales_p_obj:

            if p_items.parent_id:
                final_company_list[p_items.id] ={
                    'customer_name': p_items.name,
                    'company_name': p_items.parent_id.name,
                    'classification': p_items.x_classification,
                    'customer_email': p_items.email,
                    'email': p_items.email if re.search(r'sin[0-9]\w+',p_items.email) else '',
                    'company_email': p_items.parent_id.email,
                }
            else:
                final_company_list[p_items.id] = {
                    'customer_name': p_items.name,
                    'company_name': p_items.parent_id.name,
                    'classification': p_items.parent_id.x_classification if p_items.parent_id.x_classification else p_items.x_classification,
                    'customer_email': p_items.email,
                    'email':  p_items.email if re.search(r'sin[0-9]\w+',p_items.email) else '',
                    'company_code': p_items.parent_id.parent_code,
                    'company_email': p_items.parent_id.email
                }

        for sale_order in sale_order_list:
            if sale_order.get('partner_id') in list_of_unique_ids:
                data_list.append(sale_order)

        for line in data_list:

            c_specs = map(
                lambda x: self.render(x, self.col_specs_template, 'lines'),
                wanted_list)

            for list_data in c_specs:

                if str(list_data[0]) == str('order_number'):
                    list_data[4] = str(line.get('client_order_ref'))

                if str(list_data[0]) == str('customer_name'):
                    partner_id = line.get('partner_id')
                    abc = final_company_list.get(partner_id)
                    list_data[4] = str(abc.get('customer_name'))


                if str(list_data[0]) == str('customer_classification'):
                    partner_id = line.get('partner_id')
                    abc = final_company_list.get(partner_id)
                    list_data[4] = str(abc.get('classification'))

                if str(list_data[0]) == str('order_amount'):
                    list_data[4] = str(line.get('amount_total'))

                if str(list_data[0]) == str('confirm_date'):
                    list_data[4] = str(line.get('date_confirm'))

                if str(list_data[0]) == str('company_name'):
                    partner_id = line.get('partner_id')
                    abc = final_company_list.get(partner_id)
                    list_data[4] = str(abc.get('company_name'))

                if str(list_data[0]) == str('customer_email'):
                    partner_id = line.get('partner_id')
                    abc = final_company_list.get(partner_id)
                    list_data[4] = str(abc.get('customer_email'))

                if str(list_data[0]) == str('company_email'):
                    partner_id = line.get('partner_id')
                    abc = final_company_list.get(partner_id)
                    list_data[4] = str(abc.get('company_email'))

                if str(list_data[0]) == str('child_email'):
                    partner_id = line.get('partner_id')
                    abc = final_company_list.get(partner_id)
                    list_data[4] = str(abc.get('email'))

            row_data = self.xls_row_template(c_specs, [x[0] for x in c_specs])
            row_pos = self.xls_write_row(
                ws, row_pos, row_data, row_style=self.aml_cell_style)


unique_customer_order_xls('report.unique.customer.order.xls',
                      'sale.order',
                      parser=unique_customer_order_xls_parser)

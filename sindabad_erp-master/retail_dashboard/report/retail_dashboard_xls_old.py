import xlwt
from datetime import datetime
from openerp.osv import orm
from openerp.report import report_sxw
from openerp.addons.report_xls.report_xls import report_xls
from openerp.addons.report_xls.utils import rowcol_to_cell, _render
from openerp.tools.translate import translate, _
from dateutil.relativedelta import relativedelta
import logging
import itertools
import calendar
import re


_logger = logging.getLogger(__name__)


_ir_translation_name = 'retail.dashboard.xls'


class retail_dashboard_xls_parser(report_sxw.rml_parse):

    def __init__(self, cr, uid, name, context):
        super(retail_dashboard_xls_parser, self).__init__(
            cr, uid, name, context=context)
        move_obj = self.pool.get('sale.order')
        self.context = context
        wanted_list = move_obj._report_xls_fields_retail(cr, uid, context)
        template_changes = move_obj._report_xls_template_retail(cr, uid, context)
        self.localcontext.update({
            'datetime': datetime,
            'wanted_list': wanted_list,
            'template_changes': template_changes,
            '_': self._,
        })

    def _(self, src):
        lang = self.context.get('lang', 'en_US')
        return translate(self.cr, _ir_translation_name, 'report', lang, src) \
            or src


class retail_dashboard_xls(report_xls):

    def __init__(self, name, table, rml=False, parser=False, header=True,
                 store=False):
        super(retail_dashboard_xls, self).__init__(
            name, table, rml, parser, header, store)

        # Cell Styles
        _xs = self.xls_styles
        # header
        rh_cell_format = _xs['bold'] + _xs['fill'] + _xs['borders_all']
        self.rh_cell_style = xlwt.easyxf(rh_cell_format)
        self.rh_cell_style_center = xlwt.easyxf(rh_cell_format + _xs['center'])
        self.rh_cell_style_right = xlwt.easyxf(rh_cell_format + _xs['right'])
        # lines
        aml_cell_format = _xs['borders_all']
        self.aml_cell_style = xlwt.easyxf(aml_cell_format)
        self.aml_cell_style_center = xlwt.easyxf(
            aml_cell_format + _xs['center'])
        self.aml_cell_style_date = xlwt.easyxf(
            aml_cell_format + _xs['left'],
            num_format_str=report_xls.date_format)
        self.aml_cell_style_decimal = xlwt.easyxf(
            aml_cell_format + _xs['right'],
            num_format_str=report_xls.decimal_format)
        # totals
        rt_cell_format = _xs['bold'] + _xs['fill'] + _xs['borders_all']
        self.rt_cell_style = xlwt.easyxf(rt_cell_format)
        self.rt_cell_style_right = xlwt.easyxf(rt_cell_format + _xs['right'])
        self.rt_cell_style_decimal = xlwt.easyxf(
            rt_cell_format + _xs['right'],
            num_format_str=report_xls.decimal_format)

        # XLS Template
        self.col_specs_template = {
            'name': {
                'header': [1, 20, 'text', _render("_('')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},
            'description': {
                'header': [1, 20, 'text', _render("_('')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},
            'month1': {
                'header': [1, 20, 'text', _render("_('')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},
            'month2': {
                'header': [1, 20, 'text', _render("_('')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},
            'month3': {
                'header': [1, 20, 'text', _render("_('')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},
            'month4': {
                'header': [1, 20, 'text', _render("_('')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

        }



    def generate_xls_report(self, _p, _xs, data, objects, wb):

        wanted_list = _p.wanted_list
        self.col_specs_template.update(_p.template_changes)
        _ = _p._

        context = self.context
        st_date = data.get('form').get('date_from')
        context['start_date'] = st_date

        filter_date=datetime.strptime(st_date, '%Y-%m-%d')

        three_months = filter_date + relativedelta(months=-3)
        three_month = str(three_months.month)
        three_months_date = str(three_months.year)+'-'+three_month+'-01'

        last_date_of_month = datetime(filter_date.year, filter_date.month, 1) + relativedelta(months=1, days=-1)
        last_date_duration = last_date_of_month - filter_date

        frist_date_of_month = datetime(filter_date.year, filter_date.month, 1)
        frist_date_month = str(frist_date_of_month.year)+'-'+str(frist_date_of_month.month)+'-01'
        frist_date_duration = filter_date - frist_date_of_month

        st_date_time = st_date + ' 23:59:59'
        three_months_date1 = three_months_date + ' 00:00:00'
        frist_date_time_month = frist_date_month + ' 00:00:00'

        if frist_date_duration!=0:
            frist_date_duration=frist_date_duration
        else :
            frist_date_duration = 1

        self.context = context
        date_range = _("Date: %s" % (st_date))

        # report_name = objects[0]._description or objects[0]._name
        report_name = _("Retail Dashboard")
        ws = wb.add_sheet(report_name[:31])
        ws.panes_frozen = True
        ws.remove_splits = True
        ws.portrait = 0  # Landscape
        ws.fit_width_to_pages = 1
        row_pos = 0

        # set print header/footer
        ws.header_str = self.xls_headers['standard']
        ws.footer_str = self.xls_footers['standard']

        # Title
        cell_style = xlwt.easyxf(_xs['xls_title'])
        c_specs = [
            ('report_name', 3, 3, 'text', report_name),
            ('date_range', 3, 3, 'text', date_range),
        ]
        row_data = self.xls_row_template(c_specs, ['report_name','date_range'])
        row_pos = self.xls_write_row(
            ws, row_pos, row_data, row_style=cell_style)
        # row_pos += 1


        final_data=[]

        target_dict = {'name': 'Sales', 'description': 'Sales Target'}
        result_today_mtd_sales = {'name': 'Sales', 'description': "Today's Sales"}
        sales_avg = {'name': 'Sales', 'description': 'Sales Achv.'}
        result_mtd_sales = {'name': 'Sales', 'description': 'MTD Sales'}
        total_customer_sales_avg = {'name': 'Sales', 'description': 'Avg. Cart Value/Customer'}
        avg_order_size = {'name': 'Sales', 'description': 'Avg. Order Size'}
        req_sales_day = {'name': 'Sales', 'description': 'Required Sales/Day'}
        sales_day = {'name': 'Sales', 'description': 'Sales/Day'}

        delivery_sales_avg = {'name': 'Delivered', 'description': 'Del. Achv./Sales'}
        actual_delivery_sales = {'name': 'Delivered', 'description': 'Delivery/Sales'}

        result_refund = {'name': 'Refund', 'description': 'Refund on Sales'}
        refund_sales_avg = {'name': 'Refund', 'description': 'Refund%/Sales'}
        total_profit_avg = {'name': 'GPM', 'description': 'GPM'}

        result_profit = {'name': 'GPM', 'description': 'GPM'}

        result_mtd_sales_order = {'name': 'Customer', 'description': 'MTD Number. of Order Place'}
        result_sales_customer = {'name': 'Customer', 'description': 'MTD Number. of Customer Place order'}
        result_sales_new_customer = {'name': 'Customer', 'description': 'New Customer Order'}
        result_sales_old_customer = {'name': 'Customer', 'description': 'Repeat Customer Order'}
        result_sales_old_customer_percent = {'name': 'Customer', 'description': 'Repeat Customer %'}
        result_today_mtd_sales_order = {'name': 'Customer', 'description': 'Number. of Order Place'}
        result_today_sales_customer = {'name': 'Customer', 'description': 'Number. of Customer Place order'}

        result_child_account_order = {'name': 'APP', 'description': 'Child Account'}
        result_parent_account_order = {'name': 'APP', 'description': 'Parent Account'}

        #### monthwise target amount ######

        target_query = "SELECT split_part(date_from::TEXT, '-', 2) date_month ,split_part(date_from::TEXT, '-', 1) date_year ,sum(amount)as amount from sales_target where date_from >= %s and date_from <=%s  GROUP BY date_from"

        self.cr.execute(target_query, (three_months_date, st_date))
        target_data_list = self.cr.dictfetchall()

        for target_data_row in target_data_list:
            target_data_row.update({'date_month_year':target_data_row['date_year']+target_data_row['date_month']})


        key = lambda datum: datum['date_month_year']
        target_data_list.sort(key=key)

        for k1, g1 in itertools.groupby(target_data_list, key=key):
            target_dict.update({
                int(k1): sum(float(i1['amount']) for i1 in g1)
            })

        in_query = "select id from  stock_picking_type where code='incoming'"
        self.cr.execute(in_query)
        incoming_objects = self.cr.dictfetchall()

        incoming_ids = [in_item.get('id') for in_item in incoming_objects]
        ##########################


        delivered_sales="select stock_move.id as moved_id,stock_move.product_id,stock_picking.date_done, stock_picking.picking_type_id,sale_order_line.order_partner_id,stock_move.product_uom_qty,sale_order_line.price_unit,(select sum((sq.qty*sm.price_unit)) as total from stock_quant_move_rel as sqm,stock_move as sm, stock_quant as sq where sqm.quant_id in ((select sq2.id from stock_quant_move_rel as sqmr2, stock_quant as sq2 where sqmr2.move_id=stock_move.id and sqmr2.quant_id = sq2.id)) and sqm.move_id = sm.id and sm.purchase_line_id >0 and sq.id=sqm.quant_id group by sq.product_id)as total_cost_price from stock_move,stock_picking,sale_order_line where stock_move.picking_id=stock_picking.id and stock_move.sale_line_id = sale_order_line.id   and stock_picking.state='done'and stock_picking.date_done >= %s and stock_picking.date_done <= %s"

        self.cr.execute(delivered_sales, (three_months_date1, st_date_time))
        delivered_sales_data_list = self.cr.dictfetchall()

        refund=[]
        delivery=[]
        result_delivery = {}
        profit_list=[]
        delivery_partner_list=[]
        retail_delivery_partner_id_list=[]


        #####  Get retail ID Lists

        partner_obj = self.pool("res.partner")
        for items in delivered_sales_data_list:
            delivery_partner_list.append(items.get('order_partner_id'))

        p_obj = partner_obj.browse(self.cr, self.uid, delivery_partner_list, context=context)

        for p_items in p_obj:
            if p_items.parent_id:
                if p_items.x_classification is not False and p_items.x_classification is not None:
                    p_classification = p_items.x_classification.lower()

                    if p_classification == 'ananta':
                        pass
                    elif p_classification == 'corporate':
                        pass
                    elif p_classification == '':
                        retail_delivery_partner_id_list.append(p_items.id)
                    else:
                        retail_delivery_partner_id_list.append(p_items.id)
            else:
                p_classification=p_items.parent_id.x_classification if p_items.parent_id.x_classification else p_items.x_classification

                if p_classification is not False and p_classification is not None:
                    p_classification = p_items.x_classification.lower()

                    if p_classification == 'ananta':
                        pass
                    elif p_classification == 'corporate':
                        pass
                    elif p_classification == '':
                        retail_delivery_partner_id_list.append(p_items.id)
                    else:
                        retail_delivery_partner_id_list.append(p_items.id)

        #### Ends Here

        for delivered_sales_data in delivered_sales_data_list:

            if delivered_sales_data.get('order_partner_id') in retail_delivery_partner_id_list:


                cal_cost = delivered_sales_data.get('total_cost_price')
                moved_id = delivered_sales_data.get('moved_id')


                if cal_cost == 0:

                    get_adjusted_cost_query = "select sum((sq.qty * sm.price_unit)) as total from stock_quant_move_rel as sqm,stock_move as sm, stock_quant as sq where sqm.quant_id in ((select sq2.id from stock_quant_move_rel as sqmr2, stock_quant as sq2 where sqmr2.move_id=%s and sqmr2.quant_id = sq2.id)) and sqm.move_id = sm.id and sm.inventory_id >0 and sq.id=sqm.quant_id group by sq.product_id"

                    self.cr.execute(get_adjusted_cost_query, ([moved_id]))
                    get_adjusted_cost = self.cr.dictfetchall()
                    for a_cost in get_adjusted_cost:
                        if a_cost.get('total') is not None:
                            cal_cost = a_cost.get('total')


                #  Floowing chekup for sq qty is zero then
                if cal_cost == 0:

                    get_adjusted_query = "select sq.cost,sm.price_unit from stock_quant_move_rel as sqm,stock_move as sm, stock_quant as sq where sqm.quant_id in (select sq2.id from stock_quant_move_rel as sqmr2, stock_quant as sq2 where sqmr2.move_id=%s and sqmr2.quant_id = sq2.id) and sqm.move_id = sm.id  and sq.id=sqm.quant_id and (sm.purchase_line_id >0 or sm.inventory_id >0) ORDER BY sm.id desc limit 1"

                    self.cr.execute(get_adjusted_query, ([moved_id]))
                    get_adjusted_cost = self.cr.dictfetchall()
                    for a_cost in get_adjusted_cost:
                        if a_cost.get('price_unit') is not None:
                            cal_cost = a_cost.get('price_unit') * delivered_sales_data.get('product_uom_qty')
                        elif a_cost.get('cost') is not None:
                            cal_cost = a_cost.get('cost') * delivered_sales_data.get('product_uom_qty')

                total_cost = cal_cost
                if total_cost:
                    total_cost=total_cost
                else:
                    total_cost=0

                date_month = delivered_sales_data.get('date_done')[5:7]
                date_year = delivered_sales_data.get('date_done')[0:4]
                date_done = date_year+date_month

                if delivered_sales_data.get('picking_type_id') in incoming_ids:

                    total_sale = round((delivered_sales_data.get('price_unit') * delivered_sales_data.get('product_uom_qty')),2)

                    profit = round(((total_sale - total_cost)*(-1)),2)


                    refund.append({
                        'date_done': date_done,
                        'total_delivery_sale': total_sale,

                    })

                else:
                    total_sale = round((delivered_sales_data.get('price_unit') * delivered_sales_data.get('product_uom_qty')),2)

                    profit = round((total_sale - total_cost),2)

                    delivery.append({
                        'date_done':date_done,
                        'total_delivery_sale':total_sale,

                    })

                profit_list.append({
                    'date_done': date_done,
                    'profit':profit
                })



        key = lambda datum: datum['date_done']

        delivery.sort(key=key)

        for k2, g2 in itertools.groupby(delivery, key=key):
            result_delivery.update({
                int(k2):sum(float(i2['total_delivery_sale']) for i2 in g2)
            })

        refund.sort(key=key)
        for k3, g3 in itertools.groupby(refund, key=key):
            result_refund.update({
                int(k3):sum(float(i3['total_delivery_sale']) for i3 in g3)
            })

        profit_list.sort(key=key)
        for k7, g7 in itertools.groupby(profit_list, key=key):
            result_profit.update({
                int(k7): sum(float(i7['profit']) for i7 in g7)
            })

        ###########################################

        query="SELECT so.partner_id,cus.email,split_part(date_confirm::TEXT,'-', 2) date_month_confirm ,split_part(date_confirm::TEXT,'-', 1) date_year_confirm,so.amount_total as mtd_sales from sale_order so,res_partner cus  WHERE so.partner_id=cus.id and so.state not IN ('draft', 'sent', 'cancel', 'new_cus_approval_pending','approval_pending','check_pending')  and so.date_confirm >= %s and so.date_confirm <= %s"

        self.cr.execute(query, (three_months_date, st_date))
        sale_order_list = self.cr.dictfetchall()

        sales_partner_list = []
        retail_sales_partner_id_list = []


        exception_list =[]
        #####  Get retail ID Lists

        partner_obj = self.pool("res.partner")
        for items in sale_order_list:
            sales_partner_list.append(items.get('partner_id'))

        sales_p_obj = partner_obj.browse(self.cr, self.uid, sales_partner_list, context=context)
        retail_customer_email={}

        for sales_p_items in sales_p_obj:
            if sales_p_items.parent_id:
                if sales_p_items.x_classification is not False and sales_p_items.x_classification is not None:
                    sales_p_classification = sales_p_items.x_classification.lower()

                    if sales_p_classification == 'ananta':
                        pass
                    elif sales_p_classification == 'corporate':
                        pass
                    elif sales_p_items.x_classification == '':
                        retail_sales_partner_id_list.append(sales_p_items.id)
                        retail_customer_email.update({sales_p_items.id:sales_p_items.email})

                    else:
                        retail_sales_partner_id_list.append(sales_p_items.id)
                        retail_customer_email.update({sales_p_items.id: sales_p_items.email})
                else:
                    exception_list.append(sales_p_items.id)

            else:
                sales_p_classification = sales_p_items.parent_id.x_classification if sales_p_items.parent_id.x_classification else sales_p_items.x_classification

                if sales_p_classification is not False and sales_p_classification is not None:
                    sales_p_classification = sales_p_items.x_classification.lower()

                    if sales_p_classification == 'ananta':
                        pass
                    elif sales_p_classification == 'corporate':
                        pass
                    elif sales_p_items.x_classification == '':
                        retail_sales_partner_id_list.append(sales_p_items.id)
                        retail_customer_email.update({sales_p_items.id: sales_p_items.email})
                    else:
                        retail_sales_partner_id_list.append(sales_p_items.id)
                        retail_customer_email.update({sales_p_items.id: sales_p_items.email})
                else:
                    exception_list.append(sales_p_items.id)

                        #### Ends Here

        retail_customer_count = list(set(retail_sales_partner_id_list))

        customer_query = "SELECT so.id,so.partner_id from sale_order so  WHERE so.state not IN ('draft', 'sent', 'cancel', 'new_cus_approval_pending','approval_pending','check_pending') and so.partner_id in %s and so.date_confirm >%s"

        self.cr.execute(customer_query, (tuple(retail_customer_count), three_months_date))
        customer_query_list = self.cr.dictfetchall()
        old_customer_list = [p['partner_id'] for p in customer_query_list]

        new_customer_query = "SELECT id,split_part(create_date::TEXT,'-', 2) date_month ,split_part(create_date::TEXT,'-', 1) date_year from res_partner WHERE id in %s and create_date >= %s and create_date <= %s"
        self.cr.execute(new_customer_query, (tuple(retail_customer_count), three_months_date1,st_date_time))
        new_customer_query_list = self.cr.dictfetchall()

        for new_customer_row in new_customer_query_list:
            new_customer_row.update(
                    {'date_month_year': new_customer_row['date_year'] + new_customer_row['date_month']})

        new_customer_dict={}
        for k81, g81 in itertools.groupby(new_customer_query_list, key=lambda datum: datum['date_month_year']):
            new_customer_dict.update({
                str(k81): [i81['id'] for i81 in g81]
            })

        sale_order_data_list=[]

        for sale_order_data_row in sale_order_list:

            if sale_order_data_row.get('partner_id') in retail_sales_partner_id_list:
                date_month_year = sale_order_data_row['date_year_confirm'] + sale_order_data_row['date_month_confirm']
                match = re.search(r'sin[0-9]\w+', retail_customer_email[sale_order_data_row.get('partner_id')])
                if match is not None:
                    child_account=1
                else:
                    child_account = 0

                if new_customer_dict.get(date_month_year) and sale_order_data_row.get('partner_id') in new_customer_dict.get(date_month_year):
                    new_customer= True
                else:
                    new_customer = False

                sale_order_data_row.update({'date_month_year':date_month_year,
                                            'mtd_order':1,
                                            'new_customer':new_customer,
                                            'child_account':child_account})


                sale_order_data_list.append(sale_order_data_row)


        months_list =list(set([int(month['date_month_year']) for month in sale_order_data_list]))
        month_list=sorted(months_list)

        month_dict={'description':"Description",
        'month1':month_list[0],
        'month2':month_list[-3],
        'month3':month_list[-2],
        'month4':month_list[-1]}

        monthyear1 = str(calendar.month_name[int(str(month_dict['month1'])[4:])])+"'"+str(month_dict['month1'])[0:4]
        monthyear2 = str(calendar.month_name[int(str(month_dict['month2'])[4:])])+"'"+str(month_dict['month2'])[0:4]
        monthyear3 = str(calendar.month_name[int(str(month_dict['month3'])[4:])])+"'"+str(month_dict['month3'])[0:4]
        monthyear4 = str(calendar.month_name[int(str(month_dict['month4'])[4:])])+"'"+str(month_dict['month4'])[0:4]

        # Sub Title
        cell_style = xlwt.easyxf(_xs['xls_title'])
        c_specs = [
            ('description', 2, 3, 'text', month_dict['description']),
            ('month1', 1, 3, 'text', monthyear1),
            ('month2', 1, 3, 'text', monthyear2),
            ('month3', 1, 3, 'text', monthyear3),
            ('month4', 1, 3, 'text', monthyear4),
        ]
        row_data = self.xls_row_template(c_specs, ['description', 'month1', 'month2', 'month3', 'month4'])
        row_pos = self.xls_write_row(
            ws, row_pos, row_data, row_style=cell_style)
        # row_pos += 1

        c_specs = map(lambda x: self.render(
            x, self.col_specs_template, 'header', render_space={'_': _p._}),
            wanted_list)
        row_data = self.xls_row_template(c_specs, [x[0] for x in c_specs])
        row_pos = self.xls_write_row(
            ws, row_pos, row_data, row_style=self.rh_cell_style,
            set_column_size=True)
        ws.set_horz_split_pos(row_pos)

        key = lambda datum: datum['date_month_year']

        sale_order_data_list.sort(key=key)
        for k4, g4 in itertools.groupby(sale_order_data_list, key=key):
            result_mtd_sales.update({
                int(k4): sum(float(i4['mtd_sales']) for i4 in g4)
            }) ## Total Sales

        for k5, g5 in itertools.groupby(sale_order_data_list, key=key):
            result_mtd_sales_order.update({
                int(k5): sum((i5['mtd_order']) for i5 in g5)
            })  ### Order Count Month Wise


        for k6, g6 in itertools.groupby(sale_order_data_list, key=key):
            result_sales_customer.update({
                int(k6): len(list(set([i6['partner_id'] for i6 in g6])))
            })


        for k8, g8 in itertools.groupby(sale_order_data_list, key=key):
            result_sales_new_customer.update({
                int(k8): len(list(set([i8['partner_id'] for i8 in g8 if i8['new_customer']==True])))
            })


        for k9, g9 in itertools.groupby(sale_order_data_list, key=key):
            result_child_account_order.update({
                int(k9): sum((i9['child_account']) for i9 in g9)
            })

        ###############todays's data

        today_query = "SELECT so.partner_id,split_part(date_confirm::TEXT,'-', 2) date_month_confirm ,split_part(date_confirm::TEXT,'-', 1) date_year_confirm,so.amount_total as mtd_sales from sale_order so  WHERE so.state not IN ('draft', 'sent', 'cancel', 'new_cus_approval_pending','approval_pending','check_pending')  and so.date_confirm = %s"

        self.cr.execute(today_query, (st_date,))
        sale_order_today_list = self.cr.dictfetchall()

        if len(sale_order_today_list)>0:

            today_sales_partner_list = []
            today_retail_sales_partner_id_list = []

            #####  Get retail ID Lists

            partner_obj = self.pool("res.partner")
            for items in sale_order_today_list:
                today_sales_partner_list.append(items.get('partner_id'))

            today_sales_p_obj = partner_obj.browse(self.cr, self.uid, today_sales_partner_list, context=context)

            for today_sales_p_items in today_sales_p_obj:
                if today_sales_p_items.parent_id:
                    if today_sales_p_items.x_classification is not False and today_sales_p_items.x_classification is not None:
                        today_sales_p_classification = today_sales_p_items.x_classification.lower()

                        if today_sales_p_classification == 'ananta':
                            pass
                        elif today_sales_p_classification == 'corporate':
                            pass
                        elif today_sales_p_classification == '':
                            today_retail_sales_partner_id_list.append(today_sales_p_items.id)
                        else:
                            today_retail_sales_partner_id_list.append(today_sales_p_items.id)
                else:
                    today_sales_p_classification = today_sales_p_items.parent_id.x_classification if today_sales_p_items.parent_id.x_classification else today_sales_p_items.x_classification

                    if today_sales_p_classification is not False and today_sales_p_classification is not None:
                        today_sales_p_classification = today_sales_p_items.x_classification.lower()

                        if today_sales_p_classification == 'ananta':
                            pass
                        elif today_sales_p_classification == 'corporate':
                            pass
                        elif today_sales_p_classification == '':
                            today_retail_sales_partner_id_list.append(today_sales_p_items.id)
                        else:
                            today_retail_sales_partner_id_list.append(today_sales_p_items.id)

                            #### Ends Here
            sale_order_today_data_list = []

            for sale_order_today_data_row in sale_order_today_list:
                if sale_order_today_data_row.get('partner_id') in today_retail_sales_partner_id_list:
                    sale_order_today_data_row.update({'date_month_year': sale_order_today_data_row['date_year_confirm'] +
                                                                         sale_order_today_data_row['date_month_confirm'],
                                                      'mtd_order': 1})
                    sale_order_today_data_list.append(sale_order_today_data_row)


            sale_order_today_data_list.sort(key=key)
            for k41, g41 in itertools.groupby(sale_order_today_data_list, key=key):
                result_today_mtd_sales.update({
                    int(k41): sum(float(i41['mtd_sales']) for i41 in g41)
                })



            for k51, g51 in itertools.groupby(sale_order_today_data_list, key=key):
                result_today_mtd_sales_order.update({
                    int(k51): sum(float(i51['mtd_order']) for i51 in g51)
                })



            for k61, g61 in itertools.groupby(sale_order_today_data_list, key=key):
                result_today_sales_customer.update({
                    int(k61): len(list(set([i61['partner_id'] for i61 in g61])))
                })

        else:
            result_today_mtd_sales.update({filter_date.month:0})
            result_today_mtd_sales_order.update({filter_date.month:0})
            result_today_sales_customer.update({filter_date.month:0})

        ###############

        for month_row_data in month_list:

            sales_target = float(target_dict.get(month_row_data)) if target_dict.get(month_row_data) else 0
            mtd_total_sales = float(result_mtd_sales.get(month_row_data)) if result_mtd_sales.get(month_row_data) else 0
            mtd_sales_order = int(result_mtd_sales_order.get(month_row_data)) if result_mtd_sales_order.get(month_row_data) else 0
            mtd_sales_customer = int(result_sales_customer.get(month_row_data)) if result_sales_customer.get(month_row_data) else 0
            delivery_total = float(result_delivery.get(month_row_data)) if result_delivery.get(month_row_data) else 0
            refund_total = float(result_refund.get(month_row_data)) if result_refund.get(month_row_data) else 0
            total_profit = float(result_profit.get(month_row_data)) if result_profit.get(month_row_data) else 0
            total_new_customer = float(result_sales_new_customer.get(month_row_data)) if result_sales_new_customer.get(month_row_data) else 0
            total_child_account_order = float(result_child_account_order.get(month_row_data)) if result_child_account_order.get(month_row_data) else 0
            total_old_customer = mtd_sales_customer-total_new_customer
            total_parent_account_order = mtd_sales_order-total_child_account_order


            sales_avg.update({month_row_data:str(round((mtd_total_sales/sales_target),2)*100)+'%' if sales_target else 0})

            avg_order_size.update({month_row_data:round((mtd_total_sales/mtd_sales_order),2)})

            result_sales_old_customer.update({month_row_data:round(total_old_customer,2)})

            result_sales_old_customer_percent.update(
                {month_row_data: str(round((total_old_customer / mtd_sales_customer), 2) * 100) + '%' if total_old_customer else 0})


            total_customer_sales_avg.update({month_row_data:round((mtd_total_sales/mtd_sales_customer),2)})

            result_parent_account_order.update({month_row_data:round((total_parent_account_order),2)})

            req_sales_day.update({month_row_data:round(((sales_target-mtd_total_sales)/int(last_date_duration.days)),2) if last_date_duration!=0 else 0})

            sales_day.update({month_row_data:round((mtd_total_sales/int(frist_date_duration.days)),2)})

            actual_delivery_sales.update({month_row_data:round((delivery_total-refund_total),2)})

            delivery_sales_avg.update({month_row_data:str(round(((delivery_total-refund_total)/mtd_total_sales),2)*100)+'%' if delivery_total and mtd_total_sales else 0})

            refund_sales_avg.update({month_row_data:str(round((refund_total/mtd_total_sales),2)*100)+'%' if refund_total and mtd_total_sales else 0})

            total_profit_avg.update({month_row_data:str(round((total_profit/mtd_total_sales),3)*100)+'%' if total_profit and mtd_total_sales else 0})


        final_data.append(target_dict) # Target

        final_data.append(result_today_mtd_sales) # Todays Sales
        final_data.append(result_mtd_sales) # MTD Sales
        final_data.append(sales_avg) # Sales Achv

        final_data.append(avg_order_size) #Avg. Order Size
        final_data.append(total_customer_sales_avg) #Avg. Cart Value/Customer
        final_data.append(req_sales_day) # Required Sales/Day
        final_data.append(sales_day) # Sales/Day

        final_data.append(actual_delivery_sales) # Delivery/Sales
        final_data.append(delivery_sales_avg) # Del. Achv./Sales
        final_data.append(result_refund) # Refund on Sales
        final_data.append(refund_sales_avg) # Refund%/Sales

        final_data.append(total_profit_avg) # GPM

        final_data.append(result_today_sales_customer)  # Number. of Customer Place order
        final_data.append(result_sales_customer)  # MTD Number. of Customer Place order
        final_data.append(result_today_mtd_sales_order)  # Number. of Order Place
        final_data.append(result_mtd_sales_order)  # MTD Number. of Order Place
        final_data.append(result_sales_new_customer)  # New Customer Order
        final_data.append(result_sales_old_customer)  # Repeat Customer Order
        final_data.append(result_sales_old_customer_percent)  # Repeat Customer %
        final_data.append(result_parent_account_order)  # Parent Account
        final_data.append(result_child_account_order)  # Child Account


        # Number Of Uniquecustomers

        unique_partner_list = self.pool.get('res.partner').search(self.cr, self.uid, [('x_classification', 'ilike', 'Retail'), ('parent_id', '=', None)], context=context)





        for data_items in final_data:
            c_specs = map(
                lambda x: self.render(x, self.col_specs_template, 'lines'),
                wanted_list)

            for list_data in c_specs:

                if str(list_data[0]) == str('name'):
                    list_data[4] = str(data_items.get('name'))

                if str(list_data[0]) == str('description'):
                    list_data[4] = str(data_items.get('description'))

                if str(list_data[0]) == str('month1'):
                    month_1_data  = str(data_items.get(month_dict.get('month1')))
                    list_data[4] = month_1_data
                    list_data[4] = str(month_1_data.replace("None", ""))

                if str(list_data[0]) == str('month2'):
                    month_2_data = str(data_items.get(month_dict.get('month2')))
                    list_data[4] = str(data_items.get(month_dict.get('month2')))
                    list_data[4] = str(month_2_data.replace("None", ""))

                if str(list_data[0]) == str('month3'):
                    month_3_data = str(data_items.get(month_dict.get('month3')))
                    list_data[4] = str(data_items.get(month_dict.get('month3')))
                    list_data[4] = str(month_3_data.replace("None", ""))

                if str(list_data[0]) == str('month4'):
                    month_4_data = str(data_items.get(month_dict.get('month4')))
                    list_data[4] = str(data_items.get(month_dict.get('month4')))
                    list_data[4] = str(month_4_data.replace("None", ""))

            row_data = self.xls_row_template(c_specs, [x[0] for x in c_specs])
            row_pos = self.xls_write_row(
                ws, row_pos, row_data, row_style=self.aml_cell_style)



retail_dashboard_xls('report.retail.dashboard.xls',
                    'sale.order',
                    parser=retail_dashboard_xls_parser)

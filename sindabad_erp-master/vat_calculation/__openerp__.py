{
    'name' : 'New Vat calculation with profit/loss',
    'version': '1.0',
    'depends': [
        'base',
        'account'

    ],
    'author': 'Mufti Muntasir Ahmed',
    'category': '',
    'description': """
Vat Calculation of Invoices
================================================================================
    """,
    'website': 'https://www.sindabad.com',
    'data': [

    ],
    'demo': [],
    'auto_install': True,
    'installable': True,
}

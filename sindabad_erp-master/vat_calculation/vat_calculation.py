# Author Mufti Muntasir Ahmed


import re, time, random
from openerp import api
from openerp.osv import fields, osv
from openerp.tools.translate import _
import logging
_logger = logging.getLogger(__name__)




class account_invoice(osv.osv):
    _inherit = 'account.invoice'

    def calculation_vat(self, cr, uid, ids, context=None):



        for inv in self.browse(cr, uid, ids, context):
            vals = {}
            if inv.amount_tax == 0:

                tmp_invoice_line_data = []
                stock_pick_number = inv.origin

                if stock_pick_number is not None:
                    pick_ids = self.pool.get('stock.picking').search(cr, uid, [('name', '=', stock_pick_number)],
                                                                     context=context)
                    picking_data = self.pool.get('stock.picking').browse(cr, uid, pick_ids, context=context)
                    product_dict = {}

                    for items in picking_data.move_lines:
                        tmp_list = []

                        avg_cost_price = 0
                        inventory_value = 0
                        delivered_qty = 0
                        po_ref = 'Calculated '
                        for moved_data in items.quant_ids:
                            if moved_data.product_id.id == items.product_id.id:

                                inventory_value += moved_data.inventory_value
                                delivered_qty += moved_data.qty

                                for his_item in moved_data.history_ids:
                                    po_string = his_item.picking_id.origin
                                    if 'PO' in po_string:
                                        po_ref = po_ref + str(po_string)

                        if delivered_qty > 0:
                            avg_cost_price = round(inventory_value / delivered_qty, 2)

                        if avg_cost_price == 0:
                            if items.product_id.average_purchaes_price > 0:
                                avg_cost_price = items.product_id.average_purchaes_price

                        avg_cost_price = round(avg_cost_price, 2)

                        total = 0
                        delivered_qty = items.product_uom_qty if items.product_uom_qty else 0.00
                        total = avg_cost_price * delivered_qty

                        tmp_list.append(avg_cost_price)
                        tmp_list.append(total)
                        tmp_list.append(po_ref)

                        product_dict[items.product_id.id] = tmp_list

                    total_vat_amount = 0

                    if picking_data is not None:
                        for inv_items in inv.invoice_line:
                            if product_dict.get(inv_items.product_id.id) is not None:
                                vat = 0
                                uni_cost = product_dict[inv_items.product_id.id][0]
                                total_unit_cost = product_dict[inv_items.product_id.id][1]
                                po_references = product_dict[inv_items.product_id.id][2]
                                profit = inv_items.price_subtotal - total_unit_cost

                                if profit > 0:
                                    vat = profit * 0.05
                                    total_vat_amount += vat

                                tmp_invoice_line_data.append([
                                    1, inv_items.id, {
                                        'unit_cost': uni_cost,
                                        'total_unit_cost': total_unit_cost,
                                        'total_profit_loss': profit,
                                        '5_vat': vat,
                                        'po_references': po_references,
                                    }
                                ])

                    if len(tmp_invoice_line_data) > 0:
                        vals['invoice_line'] = tmp_invoice_line_data
                    if total_vat_amount > 0:
                        vals['tax_line'] = [[0, False,
                                             {'base_amount': 0, 'amount': total_vat_amount, 'tax_amount': total_vat_amount,
                                              'name': 'Calculated By System', 'account_id': 10052}]]

                    account_obj = self.pool.get('account.invoice')

                    account_obj.write(cr, uid, inv.id, vals, context=context)

        return True



class account_invoice_line(osv.osv):
    _inherit = 'account.invoice.line'

    _columns = {

        'unit_cost': fields.float('Unit Cost'),
        'total_unit_cost': fields.float('Total Unit Cost'),
        'total_profit_loss': fields.float('Total Profit/Loss'),
        '5_vat': fields.float('5% Vat'),
        'po_references': fields.char('PO References'),

    }

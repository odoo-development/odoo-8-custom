import logging
from datetime import datetime
from dateutil.relativedelta import relativedelta
from operator import itemgetter
import time

import openerp
from openerp import SUPERUSER_ID, api
from openerp import tools
from openerp.osv import fields, osv, expression
from openerp.tools.translate import _
from openerp.tools.float_utils import float_round as round

from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT, DATETIME_FORMATS_MAP
from openerp.tools.float_utils import float_compare
import openerp.addons.decimal_precision as dp

_logger = logging.getLogger(__name__)


class bulk_payment_data_load(osv.osv_memory):
    _name = "bulk.payment.data.load"
    _description = "Bulk Payment Data Load"

    _columns = {
        'total_cash_amount': fields.float("Total Cash Amount"),
        'total_bank_amount': fields.float("Total Advance/Paid Amount"),
        'total_receiving_amount': fields.float("Total Receiving Amount"),
        'due_amount': fields.float('Total Due Amount'),
        'cash_payment_method': fields.many2one('account.journal', 'Cash Payment Method', required=True),
        'bank_payment_method': fields.many2one('account.journal', 'Bank Payment Method', required=True),
        'period_id': fields.many2one('account.period', 'Period', required=True),
        'manifest_id': fields.many2one('wms.manifest.process', 'Manifest ID', required=True),
        'bulk_order_payment_line': fields.one2many('bulk.payment.data.load.line', 'bulk_order_payment_id',
                                                   'Bulk Invoice payment line',
                                                   required=True),

    }





    def _get_period(self, cr, uid, context=None):
        if context is None: context = {}
        if context.get('period_id', False):
            return context.get('period_id')
        periods = self.pool.get('account.period').find(cr, uid, context=context)
        return periods and periods[0] or False

    def default_get(self, cr, uid, fields, context=None):
        if context is None:
            context = {}
        res = super(bulk_payment_data_load, self).default_get(cr, uid, fields, context=context)
        manifest_data = self.pool.get('wms.manifest.process').browse(cr, uid, context['active_id'], context=context)
        res['manifest_id'] = manifest_data.id
        res['total_cash_amount'] = manifest_data.total_cash_amount
        res['total_receiving_amount'] = manifest_data.total_cash_amount
        res['total_bank_amount'] = manifest_data.total_paid_amount
        res['due_amount'] = 0



        periods = self.pool.get('account.period').find(cr, uid, context=context)
        if len(periods)>0:
            res['period_id'] = periods[0]

        line_vals=[]

        for man_item in manifest_data.picking_line:
            inv_type = man_item.payment_type


            if man_item.full_delivered == True or man_item.partial_delivered == True:
                if 'Cash On Delivery' in inv_type or 'PAID' in inv_type or 'Mobile Banking' in inv_type or 'Credit/Debit Cards' in inv_type:

                    line_vals.append([0, False, {
                        'invoice_id': man_item.invoice_id,
                        'mag_no': man_item.magento_no,
                        'invoiced_amount': man_item.amount,
                        'due_amount': 0,
                        'receiving_amount': man_item.amount,
                        'manifest_line_id': man_item.id,
                        'inv_pay_type': man_item.payment_type
                    }])




        res['bulk_order_payment_line']=line_vals


        return res

    def wkf_confirm_order(self, cr, uid, ids, context):


        for items in self.browse(cr, uid, ids, context=context):
            vals = {}
            line_vals =[]
            for man_item in items.bulk_order_payment_line:

                line_vals.append([0, False,{
                    'invoice_id':man_item.invoice_id.id,
                    'mag_no':man_item.mag_no,
                    'invoiced_amount':man_item.invoiced_amount,
                    'receiving_amount':man_item.receiving_amount,
                    'manifest_line_id':man_item.manifest_line_id.id,
                    'due_amount':man_item.due_amount,
                    'inv_pay_type':man_item.inv_pay_type
                }])
            if len(line_vals)>0:
                vals['period_id']= items.period_id.id
                vals['cash_receiving_amount']= items.total_cash_amount
                vals['bank_receiving_amount']= items.total_bank_amount

                vals['receiving_amount']= items.total_receiving_amount
                vals['cash_payment_method']= items.cash_payment_method.id
                vals['bank_payment_method']= items.bank_payment_method.id
                vals['due_amount']= items.due_amount
                vals['bulk_order_payment_line']= line_vals
                statement_id = self.pool.get('bulk.order.payment').create(cr, uid, vals, context=context)
                validation = self.pool.get('bulk.order.payment').confirm_the_payment(cr, uid, [statement_id], context=context)

                cr.execute("update wms_manifest_process set bulk_payment_id=%s,state='closed' where id=%s", (statement_id,items.manifest_id.id))
                cr.commit()





        return True

    @api.onchange('bulk_order_payment_line')
    def test_code(self):
        total_recv_sum = 0
        total_due_sum = 0
        total_cash_sum = 0
        total_bank_sum = 0
        for line_item in self.bulk_order_payment_line:
            if 'Cash On Delivery' in line_item.inv_pay_type:
                total_cash_sum = total_cash_sum + line_item.receiving_amount
            if 'PAID' in line_item.inv_pay_type or 'Mobile Banking' in line_item.inv_pay_type or 'Credit/Debit Cards' in line_item.inv_pay_type:
                total_bank_sum = total_bank_sum + line_item.receiving_amount

            total_recv_sum = total_recv_sum + line_item.receiving_amount
            total_due_sum = total_due_sum + line_item.due_amount
        self.total_cash_amount = total_cash_sum - total_due_sum
        self.total_receiving_amount = total_recv_sum - total_due_sum
        self.total_bank_amount = total_bank_sum

        self.due_amount = total_due_sum
        return 'ss'







class bulk_payment_data_load_line(osv.osv_memory):
    _name = "bulk.payment.data.load.line"
    _description = "Bulk Payment Data Load Line"

    _columns = {

        'bulk_order_payment_id': fields.many2one('bulk.payment.data.load', 'Bulk Order Payment ID', required=True,
                                                 ondelete='cascade', select=True, readonly=True),


        'invoice_id': fields.many2one('account.invoice', string="Invoice No"),
        'mag_no': fields.char('Magento No'),
        'order_id': fields.many2one('sale.order', string="Order No."),
        'invoiced_amount': fields.float('Invoiced Amount'),
        'receiving_amount': fields.float('Receiving Amount'),
        'due_amount': fields.float('Due Amount'),
        'journal_id': fields.many2one('account.move', 'Journal '),
        'manifest_line_id': fields.many2one('wms.manifest.line', 'Manifest Line '),
        'inv_pay_type': fields.char('Invoice Payment Type'),

    }

    @api.onchange('receiving_amount')
    def mag_on_select(self):
        if self.receiving_amount:
            self.due_amount = float(self.invoiced_amount) - float(self.receiving_amount)

        return "xXxXxXxXxX"










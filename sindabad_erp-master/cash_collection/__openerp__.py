{
    'name': 'WMS Cash management',
    'version': '8.0.0.6.0',
    'author': "Mufti Muntasir Ahmed",
    'category': 'Sale',
    'summary': 'WMS Cash Management/ Warehouse Management System',
    'depends': ['base', 'odoo_magento_connect', 'account', 'sale', 'purchase', 'wms_manifest', 'send_sms_on_demand','wms_manifest_extends'],
    'data': [
'security/bulk_security.xml',
        'security/ir.model.access.csv',
'wizard/bulk_payment_data_load.xml',
        'bulk_order_payment_view.xml',
        'wms_manifest_add_button.xml',
        'wms_delivered_menu.xml',

    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}

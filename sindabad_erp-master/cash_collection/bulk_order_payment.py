## By Mufti Muntasir Ahmed

from datetime import datetime, timedelta
import time
from datetime import date
from openerp import SUPERUSER_ID
from openerp.osv import fields, osv
from openerp.tools.translate import _
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT
import openerp.addons.decimal_precision as dp
from openerp import workflow
from openerp import api
import datetime


class wms_manifest_process(osv.osv):
    _inherit = "wms.manifest.process"


    _columns = {
        'total_receivable_amount': fields.float('Total Delivered Amount'),
        'total_return_amount': fields.float('Total Return Amount'),
        'total_ndr_amount': fields.float('Total NDR Amount'),
        'total_received_amount': fields.float('Total Receiving Amount'),
        'total_cash_amount': fields.float('Total Cash Amount'),
        'total_credit_amount': fields.float('Total Credit Amount'),
        'total_paid_amount': fields.float('Total Advance/Paid Amount'),
        'bulk_payment_id': fields.many2one('bulk.order.payment', 'Bulk Payment ID'),
        'state': fields.selection([
            ('pending', 'Pending'),
            ('confirm', 'Confirmation'),
            ('delivered', 'Delivered & Not Paid'),
            ('closed', 'Paid & Closed'),
            ('cancel', 'Cancelled'),

        ], 'Status', readonly=True, copy=False, help="Gives the status of the Manifest", select=True),


    }

    def receive_payment(self, cr, uid, ids, context=None):
        return True
    def calculate_total(self, cr, uid, ids, context=None):

        abc = self.browse(cr,uid,ids,context)[0]
        total_delivered = 0
        total_return_amount=0
        total_ndr_amount = 0
        total_advance_amount =0
        total_credit_amount = 0
        total_cash_amount = 0
        total_received_amount=0
        all_manifest_did_not_taken_action=False

        for items in abc.picking_line:
            if items.full_delivered == True or items.partial_delivered ==True:
                inv_id = items.invoice_id
                inv_data = self.pool.get('account.invoice').browse(cr, uid, [inv_id], context=context)

                total_delivered = total_delivered + inv_data.residual
                if 'Cash On Delivery' in items.payment_type:
                    total_cash_amount = total_cash_amount + inv_data.residual
                elif 'Credit/Debit Cards' in items.payment_type or 'Mobile Banking' in items.payment_type or 'PAID' in items.payment_type:
                    total_advance_amount = total_advance_amount + inv_data.residual
                else:
                    total_credit_amount=total_credit_amount + inv_data.residual
                total_return_amount = total_return_amount + (inv_data.amount_total-inv_data.residual)


                cr.execute("UPDATE wms_manifest_line SET delivered_amount=%s,return_amount=%s WHERE id=%s", ([inv_data.residual,(inv_data.amount_total-inv_data.residual),items.id]))
                cr.commit()


            elif items.full_return == True:
                total_return_amount = total_return_amount + items.amount
                cr.execute("UPDATE wms_manifest_line SET return_amount=%s WHERE id=%s",([items.amount, items.id]))
                cr.commit()
            elif items.reschedule == True:
                total_ndr_amount = total_ndr_amount+items.amount
                cr.execute("UPDATE wms_manifest_line SET ndr_amount=%s WHERE id=%s", ([items.amount, items.id]))
                cr.commit()
            else:
                all_manifest_did_not_taken_action = True

        cr.execute("UPDATE wms_manifest_process SET total_receivable_amount=%s,total_cash_amount=%s,total_credit_amount=%s,total_paid_amount=%s,total_return_amount=%s,total_received_amount=%s,total_ndr_amount=%s WHERE id=%s", ([total_delivered,total_cash_amount,total_credit_amount,total_advance_amount,total_return_amount,total_received_amount,total_ndr_amount, abc.id]))
        cr.commit()

        if all_manifest_did_not_taken_action == False:
            stat='delivered'
            cr.execute(
                "UPDATE wms_manifest_process SET state=%s WHERE id=%s",([stat, abc.id]))
            cr.commit()











        return True





class wms_manifest_line(osv.osv):
    _inherit = "wms.manifest.line"


    _columns = {
        'delivered_amount': fields.float('Delivered Amount'),
        'return_amount': fields.float('Return Amount'),
        'ndr_amount': fields.float('NDR Amount'),
        'journal_id': fields.many2one('account.move','Journal ID'),

    }


class bulk_order_payment(osv.osv):
    _name ='bulk.order.payment'

    _columns = {
        'scan': fields.char('Scan Invoice Here'),
        'name': fields.char('Name'),
        'payment_date': fields.date('Date Of Received'),
        'period_id': fields.many2one('account.period', 'Period', required=True),
        'cash_receiving_amount': fields.float('Total Cash Amount'),
        'bank_receiving_amount': fields.float('Total Advance Paid Amount'),
        'receiving_amount': fields.float('Total Receiving Amount'),
        'due_amount': fields.float('Total Due Amount'),
        'vat_amount': fields.float('Total Vat Amount'),
        'cash_payment_method': fields.many2one('account.journal', 'Cash Payment Method', required=True),
        'bank_payment_method': fields.many2one('account.journal', 'Bank Payment Method', required=True),
        'vat_payment_method': fields.many2one('account.journal', 'Vat Payment Method'),
        'description': fields.text('Remarks'),
        'bulk_order_payment_line': fields.one2many('bulk.order.payment.line', 'bulk_order_payment_id', 'Bulk Invoice payment line',
                                              required=True),

        'confirm_time': fields.datetime('Confirmation Time'),
        'cancel_time': fields.datetime('Cancel Time'),
        'state': fields.selection([
            ('pending', 'Pending'),
            ('confirm', 'Confirmed'),
            ('cancel', 'Cancelled'),

        ], 'Status', readonly=True, copy=False, help="Gives the status of the Customer Order Payment Voucher",
            select=True),



    }

    def _get_period(self, cr, uid, context=None):
        if context is None: context = {}
        if context.get('period_id', False):
            return context.get('period_id')
        periods = self.pool.get('account.period').find(cr, uid, context=context)
        return periods and periods[0] or False


    _defaults = {
        'payment_date':datetime.datetime.today(),
        'period_id':_get_period,
        'state': 'pending'
    }

    _order = 'id desc'

    def write(self, cr, uid, ids, vals, context=None):

        data = self.browse(cr, uid, ids[0], context=context)

        if data.state == 'confirm':
            raise osv.except_osv(_('Confirmed!'), _('It is already confirmed. You can not change the value'))

        return super(bulk_order_payment, self).write(cr, uid, ids, vals, context=context)


    def create(self, cr, uid, vals, context=None):

        if context is None:
            context = {}

        stored = super(bulk_order_payment, self).create(cr, uid, vals, context) # return ID int object


        if stored is not None:
            name_text = 'BOR-0' + str(stored)
            cr.execute('update bulk_order_payment set name=%s where id=%s', (name_text, stored))
            cr.commit()



        return stored


    def _check_line_for_same_invoice(self, packing_line, invoice_id):
        same_invoice = False



        for line in packing_line:
            if str(line.invoice_id.id) == invoice_id:
                same_invoice = True


        return same_invoice

    def cancel_the_payment(self, cr, uid, ids, context=None):

        data = self.browse(cr, uid, ids[0], context=context)

        if data.state == 'confirm':
            raise osv.except_osv(_('Confirmed!'), _('It is already confirmed. You can not cancel.'))

        update_refund_residual_amount = "UPDATE bulk_order_payment set state='cancel' where id='{0}'".format(data.id)

        cr.execute(update_refund_residual_amount)
        cr.commit()

        return True


    def confirm_the_payment(self, cr, uid, ids, context=None):

        data = self.browse(cr, uid, ids[0], context=context)

        if data.state == 'confirm':
            raise osv.except_osv(_('Confirmed!'), _('It is already confirmed'))

        cash_payment_method = data.cash_payment_method.default_debit_account_id.id
        bank_payment_method = data.bank_payment_method.default_debit_account_id.id



        for items in data.bulk_order_payment_line:

            if items.invoice_id.state == 'open':
                j_vals = {}
                line_ids = []
                if 'Cash On Delivery' in items.inv_pay_type:
                    line_ids.append((0, 0, {
                        'name': data.name,
                        'account_id': cash_payment_method,
                        'partner_id':items.invoice_id.commercial_partner_id.id,
                        'debit': items.receiving_amount,
                    }))
                else:
                    line_ids.append((0, 0, {
                        'name': data.name,
                        'account_id': bank_payment_method,
                        'partner_id': items.invoice_id.commercial_partner_id.id,
                        'debit': items.receiving_amount,
                    }))


                line_ids.append((0, 0, {
                    'name': data.name,
                    'account_id': items.invoice_id.partner_id.property_account_receivable.id,
                    'partner_id': items.invoice_id.commercial_partner_id.id,
                    'credit': items.receiving_amount,
                }))

                j_vals = {'name': '/',
                          'journal_id': 1141,
                          'date': data.payment_date,
                          'period_id': data.period_id.id,
                          'ref': items.mag_no,
                          'line_id': line_ids

                          }
                inv_id = items.invoice_id.id

                jv_entry = self.pool.get('account.move')
                saved_jv_id = jv_entry.create(cr, uid, j_vals, context=context)
                if saved_jv_id > 0:
                    journal_id = saved_jv_id
                jv_entry.button_validate(cr, uid, [saved_jv_id], context)

                if journal_id > 0:
                    cr.execute('INSERT INTO invoice_journal_payment_relation (journal_id, invoice_id,invoice_amount) values (%s, %s,%s)',(journal_id, items.invoice_id.id, items.receiving_amount))
                    cr.commit()
                    new_residual = 0
                    new_residual = items.due_amount
                    if new_residual > 0:
                        inv_status = 'open'
                    else:
                        inv_status='paid'

                    update_refund_residual_amount = "UPDATE account_invoice set new_residual='{0}',state='{1}' where id='{2}'".format(
                        new_residual,inv_status, inv_id)

                    cr.execute(update_refund_residual_amount)
                    cr.commit()



                    update_refund_residual_amount = "UPDATE bulk_order_payment_line set journal_id='{0}'where id='{1}'".format(journal_id, items.id)

                    cr.execute(update_refund_residual_amount)
                    cr.commit()

        update_refund_residual_amount = "UPDATE bulk_order_payment set state='confirm' where id='{0}'".format(data.id)

        cr.execute(update_refund_residual_amount)
        cr.commit()




        return True

    @api.onchange('scan')
    def invoice_barcode_onchange(self):
        invoice_id = str(self.scan)

        bulk_payment_line_list=[]
        cash_receiving_amount=0
        bank_receiving_amount=0


        if invoice_id != 'False':


            if self._check_line_for_same_invoice(self.bulk_order_payment_line, invoice_id):
                self.scan=''


            else:


                bulk_payment_line_list =[]


                for invoice_line in self.bulk_order_payment_line:

                    inv_type = invoice_line.inv_pay_type
                    if 'Cash On Delivery' in inv_type:
                        cash_receiving_amount = cash_receiving_amount + invoice_line.receiving_amount
                    if 'PAID' in inv_type or 'Mobile Banking' in inv_type or 'Credit/Debit Cards' in inv_type:
                        bank_receiving_amount = bank_receiving_amount + invoice_line.receiving_amount


                    bulk_payment_line_list.append({
                        'invoice_id': invoice_line.invoice_id.id,
                        'mag_no': invoice_line.mag_no,
                        'order_id': '',
                        'invoiced_amount': invoice_line.invoiced_amount,
                        'receiving_amount': invoice_line.receiving_amount,
                        'journal_id': '',
                        'manifest_line_id': '',
                        'inv_pay_type': inv_type,
                    })



                invoice_line_env = self.env['account.invoice']
                invoice_line_obj = invoice_line_env.search([('id', '=', invoice_id)])
                for invoice_line in invoice_line_obj:
                    inv_type = invoice_line.comment if invoice_line.comment is not False else ''

                    if 'Cash On Delivery' in inv_type:
                        cash_receiving_amount = cash_receiving_amount + invoice_line.residual
                    if 'PAID' in inv_type or 'Mobile Banking' in inv_type or 'Credit/Debit Cards' in inv_type:
                        bank_receiving_amount = bank_receiving_amount + invoice_line.residual

                    bulk_payment_line_list.append({
                            'invoice_id': invoice_line.id,
                            'mag_no': invoice_line.name,
                            'order_id': '',
                            'invoiced_amount': invoice_line.amount_total,
                            'receiving_amount': invoice_line.residual,
                            'journal_id': '',
                            'manifest_line_id': '',
                            'inv_pay_type': invoice_line.comment,

                        })

                self.scan =""


                self.bulk_order_payment_line = bulk_payment_line_list

                self.cash_receiving_amount = cash_receiving_amount
                self.bank_receiving_amount = bank_receiving_amount
                self.receiving_amount = bank_receiving_amount + cash_receiving_amount





        return "xXxXxXxXxX"

    @api.onchange('bulk_order_payment_line')
    def test_code(self):
        total_recv_sum = 0
        total_due_sum = 0
        total_cash_sum= 0
        total_bank_sum = 0
        for line_item in self.bulk_order_payment_line:
            if 'Cash On Delivery' in line_item.inv_pay_type:
                total_cash_sum = total_cash_sum+ line_item.receiving_amount
            if 'PAID' in line_item.inv_pay_type or 'Mobile Banking' in line_item.inv_pay_type or 'Credit/Debit Cards' in line_item.inv_pay_type:
                total_bank_sum = total_bank_sum + line_item.receiving_amount

            total_recv_sum = total_recv_sum + line_item.receiving_amount
            total_due_sum = total_due_sum + line_item.due_amount
        self.cash_receiving_amount = total_cash_sum
        self.bank_receiving_amount = total_bank_sum
        self.receiving_amount = total_recv_sum
        self.due_amount = total_due_sum
        return 'ss'


class bulk_order_payment_line(osv.osv):
    _name = 'bulk.order.payment.line'

    _columns = {

        'bulk_order_payment_id': fields.many2one('bulk.order.payment', 'Bulk Order Payment ID', required=True,
                                            ondelete='cascade', select=True, readonly=True),

        'invoice_id': fields.many2one('account.invoice', string="Invoice No"),
        'mag_no': fields.char('Magento No'),
        'order_id': fields.many2one('sale.order', string="Order No."),
        'invoiced_amount': fields.float('Invoiced Amount'),
        'receiving_amount': fields.float('Receiving Amount'),
        'due_amount': fields.float('Due Amount'),
        'journal_id': fields.many2one('account.move', 'Journal '),
        'manifest_line_id': fields.many2one('wms.manifest.line', 'Manifest Line '),
        'inv_pay_type': fields.char('Invoice Payment Type'),


    }

    @api.onchange('receiving_amount')
    def mag_on_select(self):
        if self.receiving_amount:
            self.due_amount = float(self.invoiced_amount) - float(self.receiving_amount)

        return "xXxXxXxXxX"



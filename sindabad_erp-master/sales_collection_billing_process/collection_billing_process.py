from datetime import datetime, timedelta
import time
from datetime import date
from openerp import SUPERUSER_ID
from openerp.osv import fields, osv
from openerp.tools.translate import _
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT
import openerp.addons.decimal_precision as dp
from openerp.exceptions import except_orm, Warning, RedirectWarning
from openerp import workflow
from openerp import api
import datetime


class sale_order(osv.osv):

    _inherit = 'sale.order'

    _columns = {
        'ass_for_bill_coll': fields.boolean(string="Assigned for Billing Collection"),
    }

    def generate_final_summary_action(self, cr, uid, ids, context=None):

        ids = context['active_ids']
        sos = self.browse(cr, uid, ids, context=context)
        bill_pro_obj = self.pool.get('bill.collection.billing.process')
        acc_inv_obj = self.pool.get('account.invoice')

        # first check if any one of the so final invoice stored in "bill.collection.billing.process"
        # if so, do not proceed. show warning
        for so in sos:

            bill_pro_list = bill_pro_obj.search(cr, uid, ['&', ('mag_name', '=', str(so.client_order_ref)), ('state', '!=', 'cancel')])
            if bill_pro_list:
                raise Warning(_('Final invoice already been created for {0}. Can not create again!!!').format(str(so.client_order_ref)))
            else:
                # create final invoice

                bill_pro_data = {
                    'name': str(so.name),
                    'mag_name': str(so.client_order_ref),
                    'warehouse_name': str(so.warehouse_id.name),
                    'order_amount': str(so.amount_total),
                    'billing_amount': str(so.invoiced_amount),
                    'order_id': str(so.id),
                    'return_amount': 0.0,
                    'delivered_amount': 0.0,
                    'unpaid_amount': str(so.amount_total),
                    'paid_amount': 0.0,
                    'state': 'draft',
                    'partner_id': so.partner_id.id,
                }

                stock_obj = self.pool.get('stock.picking')
                stock_list = stock_obj.search(cr, uid, [('origin', '=', str(so.name)),('state', '!=', 'cancel')])
                stock_list.sort(reverse=True)

                if stock_list:
                    for stock in stock_obj.browse(cr, uid, stock_list, context):
                        if stock.date_done:
                            att_file_date = stock.date_done.split(" ")[0]
                            assigned_time = stock.date_done
                            break

                else:
                    att_file_date = datetime.datetime.today().date().strftime("%Y-%m-%d")
                    assigned_time = datetime.datetime.today()

                if so.so_billing_pattern == 'on_delivery':
                    bill_pro_data.update({
                        'state': 'submitted',
                        'assigned_rm': so.custom_salesperson.id,
                        'assigned_time': assigned_time,
                        'exp_sub_date': att_file_date,
                        'actual_sub_date':att_file_date,
                        'exp_pay_date': (datetime.datetime.strptime(att_file_date, '%Y-%m-%d').date()+timedelta(days=int(so.partner_id.credit_days))).isoformat(),
                        'submitted_by': uid,
                        'submitted_time':att_file_date,
                    })

                invoice_line_list = list()

                # acc_inv_list = acc_inv_obj.search(cr, uid, [('name', '=', str(so.client_order_ref))])
                acc_inv_list = [inv.id for inv in so.invoice_ids]
                acc_invs = acc_inv_obj.browse(cr, uid, acc_inv_list, context=context)

                returned_amount = 0
                delivered_amount = 0

                for inv in acc_invs:
                    inv_type = 'delivered'

                    if str(inv.type) == str('out_invoice'):
                        inv_type = 'delivered'
                        delivered_amount = delivered_amount + inv.amount_total
                    elif str(inv.type) == str('out_refund'):
                        inv_type = 'return'
                        returned_amount = returned_amount + inv.amount_total

                    for line_item in inv.invoice_line:

                        if "Free Shipping" not in str(line_item.name) and "Discount" not in str(line_item.name):

                            invoice_line_list.append([0, False,
                                 {
                                     'type': inv_type, # delivered / return
                                     'product_id': line_item.product_id.id,
                                     'invoice': inv.id,
                                     'invoice_no': inv.number,
                                     'description': str(line_item.name),
                                     'date': line_item.create_date,
                                     'quantity': line_item.quantity,
                                     'uom': str(line_item.product_id.prod_uom),
                                     'unit_price': line_item.price_unit,
                                     'discount': line_item.x_discount,
                                     'tax': 0.0,
                                     'amount': line_item.price_subtotal,
                                     'state': 'draft' if so.so_billing_pattern == 'on_demand' else 'submitted',

                                 }])

                bill_pro_data['return_amount'] = returned_amount
                bill_pro_data['delivered_amount'] = delivered_amount

                bill_pro_data['final_invoice'] = invoice_line_list

                res = bill_pro_obj.create(cr, uid, bill_pro_data, context=context)

                if res:
                    number_st = 'FS-10' + str(res)
                    cr.execute("update sale_order set ass_for_bill_coll=TRUE where id=%s", ([so.id]))
                    cr.commit()
                    cr.execute("update bill_collection_billing_process set name=%s where id=%s", ([number_st, res]))
                    cr.commit()

                    # insert into log list view
                    bill_log_obj = self.pool.get('coll.bill.process.log')
                    bill_log_data = {
                        'name': number_st,
                        'mag_name': str(so.client_order_ref),
                        'warehouse_name': str(so.warehouse_id.name),
                        'order_amount':  str(so.amount_total),
                        'billing_amount': str(so.invoiced_amount),
                        'return_amount': str(returned_amount),
                        'delivered_amount': str(delivered_amount),
                        'unpaid_amount': str(so.invoiced_amount),
                        'state': 'draft',
                        'bill_process_id': res,
                        'order_id': so.id,
                    }
                    bill_log_obj.create(cr, uid, bill_log_data, context=context)

                # search the selected orders "open" invoices and create final invoice.
                # Final invoice stored in "bill.collection.billing.process"
                # SO37415

                if so.so_billing_pattern == 'on_delivery':
                    ass_obj = self.pool.get("bill.assignee.line")
                    assignee_data = {
                        'col_bill_pro_id': res,
                        'assigned_date': assigned_time,
                        'assigned_rm': so.custom_salesperson.id,
                        'actual_sub_date': att_file_date,
                        'exp_sub_date': att_file_date,
                        'exp_pay_date': (datetime.datetime.strptime(att_file_date, '%Y-%m-%d').date() + timedelta(
                            days=int(so.partner_id.credit_days))).isoformat(),
                        'assignee_status': True,
                        'state': 'assigned'
                    }

                    ass_id = ass_obj.create(cr, uid, assignee_data, context=context)
                    # create in payment line
                    payment_data = {
                        'col_bill_pro_id': res,
                        'assignee_line_id': ass_id,
                        'paid_amount': 0.00,
                    }

                    payment_line = self.pool.get("bill.payment.line")
                    pay_id = payment_line.create(cr, uid, payment_data, context=context)

        return True

    def prepare_final_invoice(self, cr, uid, ids, context=None):

        final_invoice = dict()

        return final_invoice


class collection_billing_process(osv.osv):
    _name = "bill.collection.billing.process"
    _description = "Collection Billing Process"

    _columns = {
        'name': fields.char('Name'),
        'mag_name': fields.char('Magento Order No'),
        'order_amount': fields.float('Order Amount'),
        'warehouse_name': fields.char('Warehouse Name'),
        'billing_amount': fields.float('Final Amount'),
        'return_amount': fields.float('Total Return Amount'),
        'delivered_amount': fields.float('Total Delivered Amount'),
        'unpaid_amount': fields.float('Unpaid Amount'),
        'paid_amount': fields.float('Total Paid Amount'),
        'final_invoice': fields.one2many('bill.final.invoice.line', 'col_bill_pro_id', 'Final Invoice'),  # foreign model
        'payment': fields.one2many('bill.payment.line', 'col_bill_pro_id', 'Payment'),  # foreign model
        'assignee': fields.one2many('bill.assignee.line', 'col_bill_pro_id', 'Assignee'),  # foreign model
        # 're_attempt': fields.one2many('bill.re.attempt.line', 'col_bill_pro_id', 'Re-attempt'),  # foreign model
        'order_id': fields.many2one('sale.order','Sale Order'),
        'so_salesperson': fields.related('order_id', 'custom_salesperson', 'name', type='char', string='Sales Person'),
        'partner_id': fields.many2one('res.partner','Customer'),
        'assigned_rm': fields.many2one('res.users', 'Assigned RM '),
        'actual_sub_date': fields.date('Actual Submitted Date'),
        'exp_sub_date': fields.date('Expected Submission Date'),
        'exp_pay_date': fields.date('Expected Payment Date'),
        'payment_assigned_rm': fields.many2one('res.users', 'Assigned RM (Payment)'),
        'state': fields.selection([
            ('draft', 'Draft'),
            ('assigned', 'Assigned'),
            ('submitted', 'Submitted'),
            ('pending_payment', 'Pending Payment'),
            ('partially_paid', 'Partially Paid'),
            ('paid', 'Paid'),
            ('cancel', 'Cancel'),
        ], 'Status', select=True),
        'assigned_by': fields.many2one('res.users', 'Assigned By'),
        'assigned_time': fields.datetime('Assigned Date'),
        'submitted_by': fields.many2one('res.users', 'Submitted By'),
        'submitted_time': fields.datetime('Submitted Date'),
        'pending_payment_by': fields.many2one('res.users', 'Pending Payment By'),
        'pending_payment_time': fields.datetime('Pending Payment Date'),
        'paid_by': fields.many2one('res.users', 'Paid By'),
        'paid_time': fields.datetime('Paid Date'),
        'cancel_by': fields.many2one('res.users', 'Cancel By'),
        'cancel_time': fields.datetime('Cancel Date'),
        'partially_paid_by': fields.many2one('res.users', 'Partially Paid By'),
        'partially_paid_time': fields.datetime('Partially Paid Date'),

        'cancel_bill_process_date': fields.datetime('Cancel bill Process Date'),
        'cancel_bill_process_by': fields.many2one('res.users', 'Cancel bill Process By'),
        'cancel_bill_process_reason': fields.text('Cancel bill Process Reason'),

    }



    def cancel_bill(self, cr, uid, ids, context=None):

        for single_id in ids:

            cancel_bill_query = "UPDATE bill_collection_billing_process SET state='cancel', cancel_by={0}, cancel_time='{1}' WHERE id={2}".format(uid,
                str(fields.datetime.now()), single_id)
            cr.execute(cancel_bill_query)
            cr.commit()

            fs_bill = self.browse(cr, uid, single_id, context=context)

            so_obj = self.pool.get("sale.order")

            so = so_obj.browse(cr, uid, [fs_bill.order_id.id], context=context)
            so_obj.write(cr, uid, so.id, {"ass_for_bill_coll": False}, context=context)

        return True


    def re_calculate_invoice(self, cr, uid, ids, context=None):

        return True

    def rm_unique(self, cr, uid, ids, context=None):
        assigned_rm=[]
        #ass_line_obj = self.pool.get("bill.assignee.line")
        #ass_line_list = ass_line_obj.search(cr, uid, ['&', ('col_bill_pro_id', '=', ids), ('assignee_status', '=', True)], context=context)
        bill_pro = self.browse(cr, uid, ids, context=context)
        for fs_bill in bill_pro:
            for row in fs_bill.assignee:
                if row.assignee_status == True:
                    assigned_rm.append(row.assigned_rm.id)

        assigned_rm=set(assigned_rm)

        return assigned_rm



    def dashboard_data(self, cr, uid, start_date=None, end_date=None, context=None):

        v1 = v2 = v3 = v4 = v5 = v6 = v7 = v8 = 0


        cr.execute("select sum(billing_amount) as total, state from bill_collection_billing_process group by state")


        all_data = cr.fetchall()

        collection_overview_data_lebel =['Pending','Assigned','Pending Submission','Overdue Submission','Pending Payment','Over Due Payment','Total Pending Collection',"Collected"]

        for items in all_data:
            if items[1] == 'draft':
                v1 +=items[0]
            elif items[1] == 'assigned':
                v2 +=items[0]
            elif items[1] == 'submitted':
                v3 +=items[0]
            elif items[1] == 'submitted':
                v4 +=items[0]
            elif items[1] == 'pending_payment':
                v5 +=items[0]
            elif items[1] == 'pending_payment':
                v6 +=items[0]
            elif items[1] == 'pending_payment':
                v7 +=items[0]
            elif items[1] == 'partially_paid' or items[1] == 'paid':
                v8 +=items[0]

        pending_for_assignment = "Pending : " +str(v1)
        assigned = "Assigned : "+str(v2)
        pending_submit = "Pending Submission : " +str(v3)
        overdue_submit = "Overdue Submission : " +str(v4)
        pending_payment = "Pending Payment : " +str(v5)
        overdue_payment = "Over Due Payment : " +str(v6)
        pending_collection = "Total Pending Collection : " +str(v7)
        total_collected = "Total Collected : " +str(v8)

        collection_overview_data = [v1,v2,v3,v4,v5,v6,v7,v8]

        cr.execute(
            "select sum(billing_amount) as total, assigned_by from bill_collection_billing_process where state != 'cancel' group by assigned_by")

        all_sold_data = cr.fetchall()

        all_user_list=[]
        all_user_value_list=[]
        person_wise_sales_detail = []
        count = 1

        for items in all_sold_data:
            if items[1] is not None or items[0] is not False:
                image_url = "/web/binary/image?model=hr.employee&amp;field=image_small&amp;id="
                try:
                     string_uid = str(items[1])
                except:
                    string_uid = str('1')

                image_url = image_url + string_uid

                all_user_list.append(items[1])
                all_user_value_list.append(items[0])


                person_wise_sales_detail.append({'id': count, 'name': "Mufti Muntasir Ahmed", 'sales': items[0], 'img': image_url})
                count = count +1




        result_list =[pending_for_assignment,assigned,pending_submit,overdue_submit,pending_payment,overdue_payment,pending_collection,total_collected,collection_overview_data_lebel,collection_overview_data,all_user_list,all_user_value_list,person_wise_sales_detail]
        return result_list




class bill_final_invoice(osv.osv):
    _name = "bill.final.invoice.line"
    _description = "Final Summary"

    _columns = {
        'col_bill_pro_id': fields.many2one('bill.collection.billing.process', 'Billing Collection ID', ondelete='cascade', select=True, readonly=True),
        'type': fields.char('Type'),  # delivered / return
        'product_id': fields.many2one('product.product', 'Product', readonly=True),
        'invoice': fields.many2one('account.invoice', 'Invoice', readonly=True),
        'invoice_no': fields.char('Invoice No'),
        'description': fields.text('Memo'),
        'date': fields.date('Date'),
        'quantity': fields.float('Quantity'),
        'uom': fields.char('UOM'),
        'unit_price': fields.float('Unit Price'),
        'discount': fields.float('Discount'),
        'tax': fields.float('Tax'),
        'amount': fields.float('Amount'),
        'state': fields.selection([
            ('draft', 'Draft'),
            ('assigned', 'Assigned'),
            ('submitted', 'Submitted'),
            ('pending_payment', 'Pending Payment'),
            ('partially_paid', 'Partially Paid'),
            ('paid', 'Paid'),
            ('cancel', 'Cancel'),
        ], 'Status', select=True),
    }


class payment_line(osv.osv):
    _name = "bill.payment.line"
    _description = "Payment Line"

    _columns = {
        'col_bill_pro_id': fields.many2one('bill.collection.billing.process', 'Billing Collection ID', ondelete='cascade', select=True, readonly=True),
        'assignee_line_id': fields.many2one('bill.assignee.line', 'Assignee', ondelete='cascade', select=True, readonly=True),
        'paid_amount': fields.float('Paid Amount'),
        'memo': fields.text('Memo'),
        'paid_date': fields.date('Paid Date'),
        'type': fields.selection([
            ('cash', 'Cash'),
            ('check', 'Check'),
        ], 'Type', select=True),
        'state': fields.selection([
            ('draft', 'Draft'),
            ('assigned', 'Assigned'),
            ('submitted', 'Submitted'),
            ('pending_payment', 'Pending Payment'),
            ('partially_paid', 'Partially Paid'),
            ('paid', 'Paid'),
            ('cancel', 'Cancel'),
        ], 'Status', select=True),
    }


class assignee_line(osv.osv):
    _name = "bill.assignee.line"
    _description = "Assignee Line"

    _columns = {
        'col_bill_pro_id': fields.many2one('bill.collection.billing.process', 'Billing Collection ID', ondelete='cascade', select=True, readonly=True),
        # 're_att_line_id': fields.many2one('bill.re.attempt.line', 'Re-attempt ID', ondelete='cascade', select=True, readonly=True),
        'payment': fields.one2many('bill.payment.line', 'assignee_line_id', 'Payment'),
        'assigned_rm': fields.many2one('res.users', 'Assigned RM'),
        'assigned_date': fields.date('Assigned Date'),
        'actual_sub_date': fields.date('Actual Submitted Date'),
        'exp_sub_date': fields.date('Expected Submission Date'),
        'exp_pay_date': fields.date('Expected Payment Date'),
        'comment': fields.text('Memo'),
        'state': fields.selection([
            ('draft', 'Draft'),
            ('assigned', 'Assigned'),
            ('submitted', 'Submitted'),
            ('pending_payment', 'Pending Payment'),
            ('partially_paid', 'Partially Paid'),
            ('paid', 'Paid'),
            ('cancel', 'Cancel'),
        ], 'Status', select=True),
        'assignee_status': fields.boolean("Assignee Status"),
    }

"""
class re_attempt_line(osv.osv):
    _name = "bill.re.attempt.line"
    _description = "Re-attempt Line"

    _columns = {
        'col_bill_pro_id': fields.many2one('bill.collection.billing.process', 'Billing Collection ID', ondelete='cascade', select=True, readonly=True),
        # foreign key assignee_line
        'assignee': fields.one2many('bill.assignee.line', 're_att_line_id', 'Assignee'),
        # 're_attempt_count': fields.integer('Re-attempt Count'),
        're_attempt_date': fields.date('Re-Submission'),
        're_attempt_reason': fields.text('Re-Submission Reason'),

    }
"""


class bill_wiz_assign_rm(osv.osv):
    _name = "bill.wiz.assign.rm"
    _description = "Assign RM Wizard"

    _columns = {
        'name': fields.char('Order No'),
        'mag_name': fields.char('Magento Order No'),
        'order_amount': fields.float('Order Amount'),
        'assigned_date': fields.date('Assigned Date'),
        'assigned_rm': fields.many2one('res.users', 'Assigned RM'),
        'actual_sub_date': fields.date('Actual Submitted Date'),
        'exp_sub_date': fields.date('Expected Submission Date'),
        'state': fields.selection([
            ('draft', 'Draft'),
            ('assigned', 'Assigned'),
            ('submitted', 'Submitted'),
            ('pending_payment', 'Pending Payment'),
            ('partially_paid', 'Partially Paid'),
            ('paid', 'Paid'),
            ('cancel', 'Cancel'),
        ], 'Status', select=True),
    }

    def default_get(self, cr, uid, fields, context=None):

        if context is None:
            context = {}
        res = super(bill_wiz_assign_rm, self).default_get(cr, uid, fields, context=context)

        ids = context['active_ids']
        bill_pro_obj = self.pool.get('bill.collection.billing.process')
        bill_pro = bill_pro_obj.browse(cr, uid, ids, context=context)
        assign_rm_unique = bill_pro_obj.rm_unique(cr, uid, ids, context=context)
        if len(assign_rm_unique)>0:
            raise osv.except_osv(_('Warning!'),
                                 _('Already assign RM.Please re-assign RM.'))

        if len(bill_pro)==1:
            res['name'] = str(bill_pro.name)
            res['mag_name'] = str(bill_pro.mag_name)
            res['order_amount'] = bill_pro.order_amount
        res['assigned_date'] = datetime.datetime.today().date().strftime("%Y-%m-%d")
        res['state'] = 'assigned'

        return res

    def save_assign_rm(self, cr, uid, ids, context=None):
        ass_obj = self.pool.get("bill.assignee.line")
        save_assign = self.browse(cr, uid, ids, context=context)

        for fs_id in context['active_ids']:
            assignee_data = {
                'col_bill_pro_id': fs_id,
                'assigned_date': save_assign.assigned_date,
                'assigned_rm': save_assign.assigned_rm.id,
                'exp_sub_date': save_assign.exp_sub_date,
                'assignee_status': True,
                'state': save_assign.state
            }

            ass_id = ass_obj.create(cr, uid, assignee_data, context=context)

            state_update_query = "UPDATE bill_collection_billing_process SET state='assigned', assigned_by={0}, assigned_time='{1}', assigned_rm='{2}',exp_sub_date='{3}' WHERE id={4}".format(uid, str(fields.datetime.now()), save_assign.assigned_rm.id,save_assign.exp_sub_date, fs_id)
            cr.execute(state_update_query)
            cr.commit()

            bill_log_obj = self.pool.get('coll.bill.process.log')
            log_id = bill_log_obj.search(cr, uid, [('bill_process_id', '=', fs_id)])
            bill_log_data = {
                'assigned_to': save_assign.assigned_rm.id,
                'assigned_time': save_assign.exp_sub_date,
                'state': 'assigned',
            }
            bill_log_obj.write(cr, uid, log_id, bill_log_data)

            bill_pro_data = {
                'exp_sub_date': save_assign.exp_sub_date
            }
            bill_pro_obj = self.pool.get("bill.collection.billing.process")
            bill_pro_obj.write(cr, uid, [fs_id], bill_pro_data, context=context)

        return True


class bill_wiz_re_assign_rm(osv.osv):
    _name = "bill.wiz.re.assign.rm"
    _description = "Re-assign RM Wizard"

    _columns = {
        'name': fields.char('Order No'),
        'mag_name': fields.char('Magento Order No'),
        'order_amount': fields.float('Order Amount'),
        'assigned_date': fields.date('Assigned Date'),
        'assigned_rm': fields.many2one('res.users', 'Assigned RM'),
        'actual_sub_date': fields.date('Actual Submitted Date'),
        'exp_sub_date': fields.date('Expected Submission Date'),
    }

    def default_get(self, cr, uid, fields, context=None):

        if context is None:
            context = {}
        res = super(bill_wiz_re_assign_rm, self).default_get(cr, uid, fields, context=context)

        ids = context['active_ids']
        bill_pro_obj = self.pool.get('bill.collection.billing.process')
        bill_pro = bill_pro_obj.browse(cr, uid, ids, context=context)
        assign_rm_unique = bill_pro_obj.rm_unique(cr, uid, ids, context=context)
        if len(assign_rm_unique)==0:
            raise osv.except_osv(_('Warning!'),
                                 _('Please assign RM.'))

        if len(bill_pro) == 1:
            res['name'] = str(bill_pro.name)
            res['mag_name'] = str(bill_pro.mag_name)
            res['order_amount'] = bill_pro.order_amount
        res['assigned_date'] = datetime.datetime.today().date().strftime("%Y-%m-%d")

        return res

    def save_re_assign_rm(self, cr, uid, ids, context=None):
        ass_obj = self.pool.get("bill.assignee.line")
        save_assign = self.browse(cr, uid, ids, context=context)
        for fs_id in context['active_ids']:
            re_assignee_data = {
                'col_bill_pro_id': fs_id,
                'assigned_date': save_assign.exp_sub_date,
                'assigned_rm': save_assign.assigned_rm.id,
                'exp_sub_date': save_assign.exp_sub_date,
                'assignee_status': True
            }

            ass_line_list = ass_obj.search(cr, uid, [('col_bill_pro_id', '=', fs_id)], context=context)
            for line in ass_obj.browse(cr, uid, ass_line_list, context=context):
                ass_obj.write(cr, uid, line.id, {'assignee_status': False}, context=context)

            ass_obj.create(cr, uid, re_assignee_data, context=context)

        return True


class bill_wiz_assign_rm_submission(osv.osv):
    _name = "bill.wiz.assign.rm.submission"
    _description = "Assign RM Submission Wizard"

    _columns = {
        'name': fields.char('Order No'),
        'mag_name': fields.char('Magento Order No'),
        'order_amount': fields.float('Order Amount'),
        'assigned_date': fields.date('Assigned Date'),
        'assigned_rm': fields.many2one('res.users', 'Assigned RM'),
        'actual_sub_date': fields.date('Actual Submitted Date'),
        'exp_sub_date': fields.date('Expected Submission Date'),
        'exp_pay_date': fields.date('Expected Payment Date'),
        'comment': fields.text('Comment'),
        'state': fields.selection([
            ('draft', 'Draft'),
            ('assigned', 'Assigned'),
            ('submitted', 'Submitted'),
            ('pending_payment', 'Pending Payment'),
            ('partially_paid', 'Partially Paid'),
            ('paid', 'Paid'),
            ('cancel', 'Cancel'),
        ], 'Status', select=True),
    }

    def default_get(self, cr, uid, fields, context=None):

        if context is None:
            context = {}
        res = super(bill_wiz_assign_rm_submission, self).default_get(cr, uid, fields, context=context)

        ids = context['active_ids']
        bill_pro_obj = self.pool.get('bill.collection.billing.process')
        bill_pro_data = bill_pro_obj.browse(cr, uid, ids, context=context)
        assign_rm_unique = bill_pro_obj.rm_unique(cr, uid, ids, context=context)
        if len(assign_rm_unique)==1:
            for bill_pro in bill_pro_data:

                for line in bill_pro.assignee:
                    if line.assignee_status:
                        res['name'] = str(bill_pro.name)
                        res['assigned_rm'] = line.assigned_rm.id
                        res['mag_name'] = str(bill_pro.mag_name)
                        res['order_amount'] = bill_pro.order_amount
                        res['actual_sub_date'] = line.actual_sub_date
                        res['exp_sub_date'] = line.exp_sub_date
                res['name'] = str(bill_pro.name)
                res['mag_name'] = str(bill_pro.mag_name)
                res['order_amount'] = bill_pro.order_amount
                res['state'] = 'submitted'

        elif len(assign_rm_unique)==0:
            raise osv.except_osv(_('Warning!'),
                                 _('Please assign the RM'))
        else:
            raise osv.except_osv(_('Warning!'),
                                 _('Please select the same RM order'))

        return res

    def save_assign_rm_submission(self, cr, uid, ids, context=None):

        rm_submission = self.browse(cr, uid, ids, context=context)

        rm_sub_data = {
            'actual_sub_date': rm_submission.actual_sub_date,
            'exp_sub_date': rm_submission.exp_sub_date,
            'exp_pay_date': rm_submission.exp_pay_date,
            'assigned_rm': rm_submission.assigned_rm.id,
            'comment': rm_submission.comment,
            'state': rm_submission.state
        }

        bill_pro_ids = context['active_ids']
        bill_pro_obj = self.pool.get('bill.collection.billing.process')
        bill_pro_data = bill_pro_obj.browse(cr, uid, bill_pro_ids, context=context)

        assignee_line_obj = self.pool.get("bill.assignee.line")
        for bill_pro in bill_pro_data:
            assignee_id = assignee_line_obj.search(cr, uid, ['&', ('col_bill_pro_id', '=', bill_pro.id), ('assignee_status', '=', True)], context=context)
            ass_line = assignee_line_obj.write(cr, uid, assignee_id, rm_sub_data, context=context )

            # create in payment line
            payment_data = {
                'col_bill_pro_id': bill_pro.id,
                'assignee_line_id': assignee_id[0],
                'paid_amount': 0.00,
            }

            payment_line = self.pool.get("bill.payment.line")
            pay_id = payment_line.create(cr, uid, payment_data, context=context)

            state_update_query = "UPDATE bill_collection_billing_process SET state='submitted', submitted_by={0},submitted_time='{1}', payment_assigned_rm='{2}' WHERE id={3}".format(uid, str(fields.datetime.now()), rm_submission.assigned_rm.id, bill_pro.id)
            cr.execute(state_update_query)
            cr.commit()

            bill_log_obj = self.pool.get('coll.bill.process.log')
            log_id = bill_log_obj.search(cr, uid, [('bill_process_id', '=', bill_pro.id)])
            bill_log_data = {
                'submitted_to': rm_submission.assigned_rm.id,
                'actual_sub_date': rm_submission.actual_sub_date,
                'submitted_time': rm_submission.exp_pay_date,
                'state': 'submitted',

            }
            bill_log_obj.write(cr, uid, log_id, bill_log_data)

            bill_pro_data1 = {
                'exp_pay_date': rm_submission.exp_pay_date,
                'actual_sub_date': rm_submission.actual_sub_date
            }
            bill_pro_obj.write(cr, uid, [bill_pro.id], bill_pro_data1, context=context)

        return True


class bill_wiz_assign_rm_reschedule_submission(osv.osv):
    _name = "bill.wiz.assign.rm.reschedule.submission"
    _description = "Assign RM Reschedule Submission Wizard"

    _columns = {
        'name': fields.char('Order No'),
        'mag_name': fields.char('Magento Order No'),
        'order_amount': fields.float('Order Amount'),
        'assigned_date': fields.date('Assigned Date'),
        'assigned_rm': fields.many2one('res.users', 'Assigned RM'),
        'actual_sub_date': fields.date('Actual Submitted Date'),
        'exp_sub_date': fields.date('Expected Submission Date'),
        'exp_pay_date': fields.date('Expected Payment Date'),
        'comment': fields.text('Comment'),
    }

    def default_get(self, cr, uid, fields, context=None):

        if context is None:
            context = {}
        res = super(bill_wiz_assign_rm_reschedule_submission, self).default_get(cr, uid, fields, context=context)

        ids = context['active_ids']
        bill_pro_obj = self.pool.get('bill.collection.billing.process')
        bill_pro_data = bill_pro_obj.browse(cr, uid, ids, context=context)
        assign_rm_unique = bill_pro_obj.rm_unique(cr, uid, ids, context=context)
        if len(assign_rm_unique) == 1:
            for bill_pro in bill_pro_data:
                res['name'] = str(bill_pro.name)
                res['mag_name'] = str(bill_pro.mag_name)
                res['order_amount'] = bill_pro.order_amount

        elif len(assign_rm_unique) == 0:
            raise osv.except_osv(_('Warning!'),
                                 _('Please assign the RM'))
        else:
            raise osv.except_osv(_('Warning!'),
                                 _('Please select the same RM order'))

        return res

    def save_assign_rm_reschedule_submission(self, cr, uid, ids, context=None):

        rm_submission = self.browse(cr, uid, ids, context=context)

        rm_re_sub_data = {
            'col_bill_pro_id': context['active_ids'][0],
            'exp_sub_date': rm_submission.exp_sub_date,
            'exp_pay_date': rm_submission.exp_pay_date,
            'assigned_rm': rm_submission.assigned_rm.id,
            'comment': rm_submission.comment,
            'state': 'submitted',
            'assignee_status': True
        }

        bill_pro_ids = context['active_ids']
        bill_pro_obj = self.pool.get('bill.collection.billing.process')
        bill_pro_data = bill_pro_obj.browse(cr, uid, bill_pro_ids, context=context)

        assignee_line_obj = self.pool.get("bill.assignee.line")
        for bill_pro in bill_pro_data:

            assignee_id = assignee_line_obj.search(cr, uid, ['&', ('col_bill_pro_id', '=', bill_pro.id), ('assignee_status', '=', True)], context=context)

            assignee_line_obj.write(cr, uid, assignee_id, {'assignee_status': False}, context=context)

            ass_line = assignee_line_obj.create(cr, uid, rm_re_sub_data, context=context)

            # create in payment line
            payment_data = {
                'col_bill_pro_id': bill_pro.id,
                'assignee_line_id': assignee_id[0],
                'paid_amount': 0.00,
            }

            payment_line = self.pool.get("bill.payment.line")
            pay_id = payment_line.create(cr, uid, payment_data, context=context)

            state_update_query = "UPDATE bill_collection_billing_process SET state='submitted', submitted_by={0},submitted_time='{1}', payment_assigned_rm='{2}' WHERE id={3}".format(
                uid, str(fields.datetime.now()), rm_submission.assigned_rm.id, bill_pro.id)
            cr.execute(state_update_query)
            cr.commit()

            bill_log_obj = self.pool.get('coll.bill.process.log')
            log_id = bill_log_obj.search(cr, uid, [('bill_process_id', '=', bill_pro.id)])
            bill_log_data = {
                'submitted_to': rm_submission.assigned_rm.id,
                'submitted_time': rm_submission.exp_pay_date,
                'state': 'submitted',

            }
            bill_log_obj.write(cr, uid, log_id, bill_log_data)

            bill_pro_data1 = {
                'exp_sub_date': rm_submission.exp_sub_date,
                'exp_pay_date': rm_submission.exp_pay_date,
            }
            bill_pro_obj.write(cr, uid, [bill_pro.id], bill_pro_data1, context=context)

        return True


class bill_wiz_payment_collection(osv.osv):
    _name = "bill.wiz.payment.collection"
    _description = "Payment Collection Wizard"

    _columns = {

        'paid_amount': fields.float('Paid Amount'),
        'memo': fields.text('Memo'),
        'paid_date': fields.date('Paid Date'),
        'type': fields.selection([
            ('cash', 'Cash'),
            ('check', 'Check'),
        ], 'Type', select=True),
        'state': fields.selection([
            ('draft', 'Draft'),
            ('assigned', 'Assigned'),
            ('submitted', 'Submitted'),
            ('pending_payment', 'Pending Payment'),
            ('partially_paid', 'Partially Paid'),
            ('paid', 'Paid'),
            ('cancel', 'Cancel'),
        ], 'Status', select=True),

    }

    def default_get(self, cr, uid, fields, context=None):

        if context is None:
            context = {}
        res = super(bill_wiz_payment_collection, self).default_get(cr, uid, fields, context=context)

        ids = context['active_ids']
        bill_pro_obj = self.pool.get('bill.collection.billing.process')
        bill_pro_data = bill_pro_obj.browse(cr, uid, ids, context=context)

        for bill_pro in bill_pro_data:

            for line in bill_pro.assignee:
                if line.assignee_status:
                    res['name'] = str(bill_pro.name)
                    res['assigned_rm'] = line.assigned_rm.id
                    res['mag_name'] = str(bill_pro.mag_name)
                    res['order_amount'] = bill_pro.order_amount

        return res

    def save_payment_collection(self, cr, uid, ids, context=None):

        # pay_obj = self.pool.get("bill.payment.line")
        save_payment = self.browse(cr, uid, ids, context=context)

        ids = context['active_ids']
        bill_pro_obj = self.pool.get('bill.collection.billing.process')
        bill_pro_data = bill_pro_obj.browse(cr, uid, ids, context=context)

        bill_payment_line_obj = self.pool.get('bill.payment.line')
        pay_line_list = bill_payment_line_obj.search(cr, uid, [('col_bill_pro_id', '=', context['active_ids'][0])], context=context)

        total_paid_amount = 0.0
        for pay_line in bill_payment_line_obj.browse(cr, uid, pay_line_list, context=context):
            total_paid_amount += pay_line.paid_amount

        if total_paid_amount == 0.0:
            total_paid_amount = save_payment.paid_amount
        else:
            total_paid_amount = total_paid_amount + save_payment.paid_amount

        payment_line_status = ""
        if total_paid_amount == bill_pro_data.order_amount:
            payment_line_status = 'paid'
        else:
            payment_line_status = 'partially_paid'

        # payment_line_status need to save
        state_update_query = "UPDATE bill_collection_billing_process SET state='{0}', pending_payment_by={1},pending_payment_time='{2}', paid_amount='{3}' WHERE id={4}".format(payment_line_status, uid, str(fields.datetime.now()), total_paid_amount, context['active_ids'][0])
        cr.execute(state_update_query)
        cr.commit()

        # payment_update_query = "UPDATE bill_payment_line SET state='{0}', paid_amount='{1}', memo='{2}', paid_date='{3}',type='{4}' WHERE col_bill_pro_id={5}".format(payment_line_status, save_payment.paid_amount, save_payment.memo, save_payment.paid_date, save_payment.type, context['active_ids'][0])
        # cr.execute(payment_update_query)
        # cr.commit()

        assignee_line_obj = self.pool.get('bill.assignee.line')

        zero_paid = bill_payment_line_obj.search(cr, uid, [('paid_amount','=', 0.00),('col_bill_pro_id','=', bill_pro_data.id)], context=context)

        if zero_paid:
            bill_payment_line_obj.unlink(cr, uid, zero_paid, context=context)

        values = {
            'state': payment_line_status,
            'paid_amount': save_payment.paid_amount,
            'memo': save_payment.memo,
            'paid_date': save_payment.paid_date,
            'type': save_payment.type,
            'col_bill_pro_id': ids[0],
            'assignee_line_id': assignee_line_obj.search(cr, uid, [('col_bill_pro_id','=', bill_pro_data.id), ('assignee_status', '=', True)], context=context)[0],

         }

        bill_payment_line_obj.create(cr, uid, values)

        bill_log_obj = self.pool.get('coll.bill.process.log')
        log_id = bill_log_obj.search(cr, uid, [('bill_process_id', '=', bill_pro_data.id)])
        bill_log_data = {
            'paid_amount': total_paid_amount,
            'unpaid_amount': bill_pro_data.order_amount - total_paid_amount,
            'state': payment_line_status,
        }
        bill_log_obj.write(cr, uid, log_id, bill_log_data)

        return True


class CancelBillProcessReason(osv.osv):
    _name = "cancel.bill.process.reason"
    _description = "Cancel Bill Process Reason"

    _columns = {
        'cancel_bill_process_date': fields.datetime('Cancel Bill Process Date'),
        'cancel_bill_process_by': fields.many2one('res.users', 'Cancel Bill Process By'),
        'cancel_bill_process_reason': fields.text('Cancel Bill Process Reason', required=True),

    }

    def cancel_bill_process_reason_def(self, cr, uid, ids, context=None):
        ids = context['active_ids']
        cancel_bill_process_reason = str(context['cancel_bill_process_reason'])
        cancel_bill_process_by = uid
        cancel_bill_process_date = str(fields.datetime.now())

        bill_obj = self.pool.get('bill.collection.billing.process')

        for s_id in ids:
            cancel_bill_proces_query = "UPDATE bill_collection_billing_process SET cancel_bill_process_reason='{0}', cancel_bill_process_by={1}, cancel_bill_process_date='{2}' WHERE id={3}".format(
                cancel_bill_process_reason, cancel_bill_process_by, cancel_bill_process_date, s_id)
            cr.execute(cancel_bill_proces_query)
            cr.commit()

            bill_obj.cancel_bill(cr, uid, [s_id], context)

            bill_log_obj = self.pool.get('coll.bill.process.log')
            log_id = bill_log_obj.search(cr, uid, [('bill_process_id', '=', s_id)])
            bill_log_data = {
                'state': 'cancel',
                'cancel_by': uid,
                'cancel_time': datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'),

            }
            bill_log_obj.write(cr, uid, log_id, bill_log_data)

        return True


class coll_bill_process_log(osv.osv):
    _name = "coll.bill.process.log"
    _description = "Collection Billing Process Log"

    _columns = {
        'name': fields.char('Name'),
        'mag_name': fields.char('Magento Order No'),
        'partner_id': fields.many2one('res.partner', 'Customer'),
        'warehouse_name': fields.char('Warehouse Name'),
        'order_amount': fields.float('Order Amount'),
        'billing_amount': fields.float('Final Amount'),
        'return_amount': fields.float('Total Return Amount'),
        'delivered_amount': fields.float('Total Delivered Amount'),
        'unpaid_amount': fields.float('Unpaid Amount'),
        'paid_amount': fields.float('Paid Amount'),
        'order_id': fields.many2one('sale.order', 'Sale Order'),
        'bill_process_id': fields.many2one('bill.collection.billing.process', 'Bill Process ID'),
        'state': fields.char('Status'),
        'assigned_to': fields.many2one('res.users', 'Assigned To'),
        'assigned_time': fields.datetime('Assigned Date'),
        'actual_sub_date': fields.date('Actual Submitted Date'),
        'submitted_to': fields.many2one('res.users', 'Submitted To'),
        'submitted_time': fields.datetime('Submitted Date'),
        'pending_payment_by': fields.many2one('res.users', 'Pending Payment By'),
        'pending_payment_time': fields.datetime('Pending Payment Date'),
        'paid_by': fields.many2one('res.users', 'Paid By'),
        'paid_time': fields.datetime('Paid Date'),
        'cancel_by': fields.many2one('res.users', 'Cancel By'),
        'cancel_time': fields.datetime('Cancel Date'),
        'partially_paid_by': fields.many2one('res.users', 'Partially Paid By'),
        'partially_paid_time': fields.datetime('Partially Paid Date'),
    }



class bill_wiz_accounts_approve(osv.osv):
    _name = "bill.wiz.accounts.approve"
    _description = "Accounts Approve For Bill"

    _columns = {
        'reason': fields.text('Reason'),
    }

    def save_account_approve(self, cr, uid, ids, context=None):

        if context.get('active_id'):
            billing_id =context.get('active_id')
            # cr.execute("update account_invoice set state='partially_paid' where id=%s", ([billing_id]))
            cr.execute("update bill_collection_billing_process set state='paid' where id=%s", ([billing_id]))
        cr.commit()
        return True

# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2009 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import time
from openerp.report import report_sxw
from openerp.osv import osv


class billcollectinvoicedata(report_sxw.rml_parse):
    def __init__(self, cr, uid, name, context):
        super(billcollectinvoicedata, self).__init__(cr, uid, name, context)
        self.localcontext.update({
            'get_all_invoices_data': self.get_all_invoices_data,
            'get_invoices_list': self.get_invoices_list,

        })

    def get_all_invoices_data(self, obj):

        try:
            order_numbers = [obj.split(',')[0]]
        except:
            order_numbers = [str(obj)]

        group_id = None
        data_list = []

        self.cr.execute("SELECT id FROM bill_collection_billing_process WHERE name=%s", (order_numbers))
        for item in self.cr.fetchall():
            group_id = [item[0]]

        if group_id is not None:

            order_data = self.pool.get('bill.collection.billing.process').browse(self.cr, self.uid, group_id)

            account_invoice_ids = order_data.final_invoice

            for it in account_invoice_ids:

                data_list.append({'name': 'cool',
                                  'product_name': it.product_id.name,
                                  'date': it.date,
                                  'quantity': it.quantity,
                                  'uom': it.uom,
                                  'unit_price': it.unit_price,
                                  'discount': it.discount,
                                  'tax': it.tax,
                                  'amount': it.amount,
                                  'type': it.type,
                                  'invoice_no': it.invoice_no,
                                  'invoice': it.invoice,
                                  })

        return data_list

    def get_invoices_list(self, obj):

        try:
            order_numbers = [obj.split(',')[0]]
        except:
            order_numbers = [str(obj)]

        group_id = None
        data_list = []
        data_dict = dict()

        self.cr.execute("SELECT id FROM bill_collection_billing_process WHERE name=%s", (order_numbers))
        for item in self.cr.fetchall():
            group_id = [item[0]]

        if group_id is not None:

            order_data = self.pool.get('bill.collection.billing.process').browse(self.cr, self.uid, group_id)

            parent = order_data.order_id.partner_id
            rm_mobile_numbers = ""
            rm_name = ""
            for i in range(5):
                if len(parent.parent_id) == 1:
                    parent = parent.parent_id
                else:
                    rm_mobile_numbers = parent.user_id.partner_id.phone
                    rm_name = parent.user_id.partner_id.name
                    # company_id = parent.id
                    break

            inv_obj = self.pool.get('account.invoice')
            inv_list = inv_obj.search(self.cr, self.uid, [('name','=', str(order_data.mag_name))])

            invoices = ''
            date = ''

            total_amount = 0.00
            total_amount_tax = 0.00
            net_total = 0.00

            delivered_amount_without_tax = 0.00
            delivered_amount_tax = 0.00
            delivered_amount = 0.00

            total_delivered = str(order_data.delivered_amount)

            refund_amount_without_tax = 0.00
            refund_amount_tax = 0.00
            refund_amount = 0.00

            total_return = str(order_data.return_amount)

            for inv in inv_obj.browse(self.cr, self.uid, inv_list):
                if str(inv.state) != 'draft' and str(inv.state != 'cancel'):
                    invoices += str(inv.number) + ", " if inv.number else ''
                    date += str(inv.date_invoice) + ", " if inv.date_invoice else ''

                    if str(inv.state) != 'cancel' and str(inv.type) == 'out_refund':

                        refund_amount_without_tax += inv.amount_untaxed
                        refund_amount_tax += inv.amount_tax
                        refund_amount += inv.amount_total
                    else:
                        total_amount += inv.amount_untaxed
                        total_amount_tax += inv.amount_tax
                        net_total += inv.amount_total
                        delivered_amount_without_tax += inv.amount_untaxed
                        delivered_amount_tax += inv.amount_tax
                        delivered_amount += inv.amount_total

        return [{
            'invoices': invoices,
            'date': date,

            'delivered_amount_without_tax': delivered_amount_without_tax,
            'delivered_amount_tax': delivered_amount_tax,
            'delivered_amount': delivered_amount,
            'total_delivered': total_delivered,

            'refund_amount_without_tax': refund_amount_without_tax,
            'refund_amount_tax': refund_amount_tax,
            'refund_amount': refund_amount,
            'total_return': total_return,
            'rm_info': "RM Name : " + rm_name + " and Phone : " + rm_mobile_numbers if rm_name and rm_mobile_numbers else False,
        }]




class report_final_invoice_layout(osv.AbstractModel):
    _name = 'report.sales_collection_billing_process.billing_invoice_report'
    _inherit = 'report.abstract_report'
    _template = 'sales_collection_billing_process.billing_invoice_report'
    _wrapped_report_class = billcollectinvoicedata

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:

# Author Mufti Muntasir Ahmed 26-02-2018

import time
from datetime import datetime
from openerp.report import report_sxw
from openerp.osv import fields, osv


class CompanyGentleReport(report_sxw.rml_parse):

    def __init__(self, cr, uid, name, context):
        super(CompanyGentleReport, self).__init__(cr, uid, name, context)
        self.localcontext.update({
            'get_client_order_ref': self.get_test
        })

        if context is None:
            self.context = {}
        else:
            self.context = context

    def get_test(self):
        return 'dfdf'

    def set_context(self, objects, data, ids, report_type=None):
        ids = self.context.get('active_ids')
        partner_obj = self.pool['res.partner']
        context = self.context
        docs = partner_obj.browse(self.cr, self.uid, ids, context)

        due = {}
        paid = {}
        mat = {}

        init_balance = {}
        pt_date = data.get('form').get('print_date')
        st_date = data.get('form').get('date_from')
        end_date = data.get('form').get('date_to')
        context['start_date'] = st_date
        context['end_date'] = end_date
        present_date = datetime.now().strftime("%m/%d/%Y")

        for partner in docs:
            due[partner.id] = reduce(lambda x, y: x + (
                (y['account_id']['type'] == 'receivable' and y['debit'] or 0) or (
                    y['account_id']['type'] == 'payable' and y['credit'] * -1 or 0)),
                self._lines_get(partner), 0)
            paid[partner.id] = reduce(lambda x, y: x + (
                (y['account_id']['type'] == 'receivable' and y['credit'] or 0) or (
                    y['account_id']['type'] == 'payable' and y['debit'] * -1 or 0)),
                self._lines_get(partner), 0)
            mat[partner.id] = reduce(lambda x, y: x + (y['debit'] - y['credit']),
                                     filter(lambda x: x['date_maturity'] < time.strftime('%Y-%m-%d'),
                                            self._lines_get(partner)), 0)

            init_balance[partner.id] = reduce(lambda x, y: x + (y['debit'] - y['credit']), self._init_lines_get(partner), 0)

        self.localcontext.update({
            'docs': docs,
            'time': time,
            'present_date': present_date,
            'getLines': self._raw_lines_get,
            'rawGetLines': self._raw_lines_get,
            'due': due,
            'paid': paid,
            'prev_balance': init_balance,
            'print_date': str(pt_date),
            'till_date': str(end_date),
            'mat': mat
        })
        self.context = context

        self.partner_ids = [res['partner_id'] for res in self.cr.dictfetchall()]

        return super(CompanyGentleReport, self).set_context(objects, data, self.partner_ids, report_type)

    def _lines_get(self, partner):

        start_date = self.context.get('start_date')
        end_date = self.context.get('end_date')

        moveline_obj = self.pool['account.move.line']


        # order_numbers = [str(obj)]
        # purchase_ref_cus = None
        # self.cr.execute("SELECT x_puchase_ref_cus FROM sale_order WHERE name=%s", (order_numbers))
        parent_list = []


        if partner.parent_id:
            parent_list.append(partner.parent_id.id)

            self.cr.execute("select id from  res_partner where customer=True and id=%s", ([partner.parent_id.id]))

            for items in self.cr.fetchall():
                parent_list.append(items[0])
        else:
            parent_list.append(partner.id)


        self.cr.execute("select id from  res_partner where customer=True and parent_id=%s", ([partner.id]))
        for items in self.cr.fetchall():
            parent_list.append(items[0])



        movelines = moveline_obj.search(self.cr, self.uid,
                                        [('partner_id', 'in', parent_list),
                                         ('account_id.type', 'in', ['receivable', 'payable']),
                                         ('state', '<>', 'draft'), ('date', '<=', end_date), ('date', '>=', start_date)])
                                         # ('state', '<>', 'draft'), ('date', '<=', end_date), ('date', '>=', start_date)], order='id')
        # obj.read_group(cr, uid, subquery_domain, fields=[field_name], groupby=[field_name], context=context)
        movelines = moveline_obj.browse(self.cr, self.uid, movelines)

        return movelines

    def _raw_lines_get(self, partner):

        start_date = self.context.get('start_date')
        end_date = self.context.get('end_date')

        moveline_obj = self.pool['account.move.line']

        parent_list = []
        parent_name_list = []


        if partner.parent_id:
            parent_list.append(partner.parent_id.id)

            self.cr.execute("select id from  res_partner where  id=%s", ([partner.parent_id.id]))

            for items in self.cr.fetchall():
                parent_list.append(items[0])
        else:
            parent_list.append(partner.id)


        self.cr.execute("select id, name from  res_partner where parent_id=%s", ([partner.id]))
        for items in self.cr.fetchall():
            parent_list.append(items[0])
            parent_name_list.append(items[1])

        print "Name list=", parent_name_list

        query = "SELECT DISTINCT partner.name, partner.street, partner.parent_id, \
                    (SELECT SUM(debit) \
                        FROM account_move_line\
                        INNER JOIN account_account ON account_account.id = account_move_line.account_id\
                        WHERE \
                        account_account.type IN ('receivable', 'payable') AND\
                        account_move_line.state != 'draft' AND\
                        account_move_line.date >= \'" + start_date + "\' AND\
                        account_move_line.date <= \'" + end_date + "\' AND\
                        partner_id = partner.id) debit\
                FROM \
                    res_partner partner\
                WHERE \
                   partner.id IN (" + ','.join(map(str, parent_list)) + ") \
                GROUP BY \
                   partner.id \
                ORDER BY \
                    partner.parent_id DESC"
        # print '---------------Company_Summary-------------------'
        # print query
        # print '---------------Company_Summary-------------------'
        self.cr.execute(query)
        result = []
        key = 1
        for name, street, parent, debit in self.cr.fetchall():
            result.append({
                'name': name,
                'street': street,
                'parent': parent,
                'debit': debit or 0,
            })
            key = key + 1
        print self.cr.fetchall()
        # result = self.cr.fetchall()
        print result
        return result

    def _init_balance_get(self, partner):

        parent_list = []

        if partner.parent_id:
            parent_list.append(partner.parent_id.id)

            self.cr.execute("select id from  res_partner where customer=True and id=%s", ([partner.parent_id.id]))

            for items in self.cr.fetchall():
                parent_list.append(items[0])
        else:
            parent_list.append(partner.id)

        self.cr.execute("select id from  res_partner where customer=True and parent_id=%s", ([partner.id]))
        for items in self.cr.fetchall():
            parent_list.append(items[0])

        start_date = self.context.get('start_date')

        moveline_obj = self.pool['account.move.line']
        movelines = moveline_obj.search(self.cr, self.uid,
                                        [('partner_id', 'in', parent_list),
                                         ('account_id.type', 'in', ['receivable', 'payable']),
                                         ('state', '<>', 'draft'), ('date', '<', start_date)])

        movelines = moveline_obj.browse(self.cr, self.uid, movelines)

        previous_balance = 0

        for items in movelines:
            previous_balance += (items.debit - items.credit)

        return previous_balance

    def _init_lines_get(self, partner):

        parent_list = []

        if partner.parent_id:
            parent_list.append(partner.parent_id.id)

            self.cr.execute("select id from  res_partner where customer=True and id=%s", ([partner.parent_id.id]))

            for items in self.cr.fetchall():
                parent_list.append(items[0])
        else:
            parent_list.append(partner.id)

        self.cr.execute("select id from  res_partner where customer=True and parent_id=%s", ([partner.id]))
        for items in self.cr.fetchall():
            parent_list.append(items[0])

        start_date = self.context.get('start_date')

        moveline_obj = self.pool['account.move.line']
        movelines = moveline_obj.search(self.cr, self.uid,
                                        [('partner_id', 'in', parent_list),
                                         ('account_id.type', 'in', ['receivable', 'payable']),
                                            ('state', '<>', 'draft'), ('date', '<', start_date)])

        movelines = moveline_obj.browse(self.cr, self.uid, movelines)

        previous_balance = 0

        for items in movelines:
            previous_balance += (items.debit - items.credit)

        return movelines


class report_company_gentle(osv.AbstractModel):
    _name = 'report.company_gentle_report.report_company_gentle'
    _inherit = 'report.abstract_report'
    _template = 'company_gentle_report.report_company_gentle'
    _wrapped_report_class = CompanyGentleReport

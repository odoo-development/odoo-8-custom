# Author Mufti Muntasir Ahmed 26-02-2018

from openerp.osv import fields, osv
import datetime


class CompanyGentleSummary(osv.osv_memory):
    """
    This wizard will provide the partner Ledger report by periods, between any two dates.
    """
    _name = 'company.gentle.summary'
    _description = 'Partner Statement'

    _columns = {
        'date_from': fields.date("Start Date"),
        'date_to': fields.date("Till Date"),
        'print_date': fields.date("Print Date"),

    }

    _defaults = {
        'date_from': datetime.datetime.today(),
        'date_to': datetime.datetime.today(),
        'print_date': datetime.datetime.today()
    }




    def _build_contexts(self, cr, uid, ids, data, context=None):
        if context is None:
            context = {}
        result = {}

        result['date_from'] = data['form']['date_from']
        result['date_to'] = data['form']['date_to']
        result['print_date'] = data['form']['print_date']

        return result


    def check_report(self, cr, uid, ids, context=None):
        if context is None:
            context = {}

        data = {}

        data = self.read(cr, uid, ids, ['date_from', 'date_to','print_date'], context=context)[0]

        datas = {
            'ids': context.get('active_ids', []),
            'model': 'company.gentle.summary',
            'form': data
        }
        return self.pool['report'].get_action(cr, uid, [], 'company_gentle_report.report_company_gentle', data=datas, context=context)


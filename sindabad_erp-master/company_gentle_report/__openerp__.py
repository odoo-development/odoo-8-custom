# -*- coding: utf-8 -*-
{
    'name': "Company Gentle Reminder Report",

    'summary': """
       Company Gentle Reminder Report""",

    'description': """
        Company Gentle Reminder Report
    """,

    'author': "Mufti Muntasir Ahmed",
    'website': "http://zerogravity.com.bd/",
    'depends': ['account'],
    'data': [
        'company_gentle_menu.xml',
        'wizard/company_gentle_report_statement_view.xml',
        'views/report_company_gentle.xml',
    ],

    'installable': True,
    'auto_install': False,
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:

{
    'name': 'Product Category DropDown List in Purchase Order',
    'version': '1.0',
    'category': 'Purchase',
    'author': 'Rocky',
    'summary': 'Product Category DropDown List in Purchase Order',
    'description': 'Product Category DropDown List in Purchase Order',
    'depends': ['base', 'purchase'],
    'data': [
        'purchase_view.xml',
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}

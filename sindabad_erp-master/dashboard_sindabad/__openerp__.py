{
    'name': "Sindabad Dashboard Module",
    'version': '1.0',
    'category': 'Information Technology',
    'author': "Md Rockibul Alam Babu",
    'description': """
        Sindabad All Operations Dashboard Module
    """,
    'website': "http://www.zerogravity.com.bd",
    'data': [
        'security/dashboard_sindabad_security.xml',
        'security/ir.model.access.csv',
        'views/all_dashboard_view.xml',
        'sindabad_dashboard.xml',
        'warehouse_dashboard.xml',
    ],
    'depends': [
        'sale', 'stock', 'account', 'partial_delivery_in_so_list'
    ],
    'installable': True,
    'auto_install': False,
}

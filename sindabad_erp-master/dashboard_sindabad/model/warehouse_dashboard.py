from openerp import models, fields, api
from datetime import datetime, timedelta


class DashboardWarehouse(models.Model):
    _inherit = 'dashboard.warehouse'

    @api.one
    def _get_count(self):

        d = 90
        d_days_ago = datetime.now() - timedelta(days=d)
        # a_days_ago = datetime.now() - timedelta(days=91)

        # --- Start Order Flow Count and mapping -----
        draft_quotation_count = self.env['sale.order'].search_count(
            [('state', '=', 'draft'), ('date_order', '>=', d_days_ago.strftime('%Y-%m-%d 00:00:00'))])

        confirm_sale_count = self.env['sale.order'].search(
            [('state', '=', ('progress', 'manual', 'shipping_except', 'done')),
             ('date_order', '>=', d_days_ago.strftime('%Y-%m-%d 00:00:00'))])

        cancel_sale_count = self.env['sale.order'].search(
            [('state', '=', 'cancel'), ('date_order', '>=', d_days_ago.strftime('%Y-%m-%d 00:00:00'))])

        delivered_sale_count = self.env['sale.order'].search(
            [('state', '=', 'done'),
             ('date_order', '>=', d_days_ago.strftime('%Y-%m-%d 00:00:00'))])

        undelivered_sale_count = self.env['sale.order'].search(
            [('partial_delivered', '=', False), ('state', 'in', ('progress', 'manual', 'shipping_except')),
             ('date_order', '>=', d_days_ago.strftime('%Y-%m-%d 00:00:00'))])

        partially_delivered_order = self.env['sale.order'].search(
            [('partial_delivered', '=', True), ('state', '=', ('progress', 'manual', 'shipping_except')),
             ('date_order', '>=', d_days_ago.strftime('%Y-%m-%d 00:00:00'))])

        full_delivery_count = self.env['sale.order'].search(
            [('partial_delivered', '=', False), ('state', '=', 'done'),
             ('date_order', '>=', d_days_ago.strftime('%Y-%m-%d 00:00:00'))])

        closed_orders_count = self.env['sale.order'].search(
            [('state', '=', 'done'), ('invoiced', '=', True),
             ('date_order', '>=', d_days_ago.strftime('%Y-%m-%d 00:00:00'))])

        pending_procurement_orders = self.env['sale.order'].search(
            [('state', '=', ('progress', 'manual', 'shipping_except')), ('waiting_availability', '=', True),
             ('date_order', '>=', d_days_ago.strftime('%Y-%m-%d 00:00:00'))])

        afp_count = self.env['sale.order'].search(
            [('state', '=', ('progress', 'manual', 'shipping_except')), ('waiting_availability', '=', False),
             ('partially_available', '=', False), ('date_order', '>=', d_days_ago.strftime('%Y-%m-%d 00:00:00'))])

        partially_available_for_processing = self.env['sale.order'].search(
            [('state', '=', ('progress', 'manual', 'shipping_except')), ('partially_available', '=', True),
             ('date_order', '>=', d_days_ago.strftime('%Y-%m-%d 00:00:00'))])

        # -- End --

        # -- Start Warehouse Sales ----

        ready_for_transfer_count = self.env['stock.picking'].search(
            [('state', '=', 'assigned'), ('origin', 'like', 'SO'),
             ('date', '>=', d_days_ago.strftime('%Y-%m-%d 00:00:00')),
             ('picking_type_id.code', '=', 'outgoing')])

        ready_for_transfer_count_1 = self.env['stock.picking'].search(
            [('state', '=', 'assigned'), ('origin', 'like', 'SO'),
             ('date', '>=', d_days_ago.strftime('%Y-%m-%d 00:00:00')), ('picked', '=', False)])

        cancelled_delivery = self.env['stock.picking'].search(
            [('state', '=', 'cancel'), ('date', '>=', d_days_ago.strftime('%Y-%m-%d 00:00:00')),
             ('picking_type_id.code', '=', 'outgoing')])

        waiting_for_procurement_count = self.env['stock.picking'].search(
            [('state', '=', ('confirmed', 'partially_available')),
             ('date', '>=', d_days_ago.strftime('%Y-%m-%d 00:00:00'))]
        )

        transferred_delivery = self.env['stock.picking'].search(
            [('state', '=', 'done'), ('date', '>=', d_days_ago.strftime('%Y-%m-%d 00:00:00')),
             ('picking_type_id.code', '=', 'outgoing')])

        partially_available = self.env['stock.picking'].search(
            [('state', '=', 'partially_available'), ('date', '>=', d_days_ago.strftime('%Y-%m-%d 00:00:00'))])

        available = self.env['stock.picking'].search(
            [('state', '=', 'assigned'), ('origin', 'like', 'PO'),
             ('date', '>=', d_days_ago.strftime('%Y-%m-%d 00:00:00'))])

        # -- End Warehouse --

        # -- Start Warehouse Purchase ----

        transferred_received = self.env['stock.picking'].search(
            [('state', '=', 'done'), ('date', '>=', d_days_ago.strftime('%Y-%m-%d 00:00:00')),
             ('picking_type_id.code', '=', 'incoming')])

        cancelled_receipt = self.env['stock.picking'].search(
            [('state', '=', 'cancel'), ('date', '>=', d_days_ago.strftime('%Y-%m-%d 00:00:00')),
             ('picking_type_id.code', '=', 'incoming')])

        # End Warehouse ---

        # --- Start Picking Count and mapping ..........
        picking_pending_count = self.env['picking.list'].search(
            [('state', '=', 'pending')])
        picking_done_count = self.env['picking.list'].search(
            [('state', '=', 'done')])
        picking_cancel_count = self.env['picking.list'].search(
            [('state', '=', 'cancel')])

        # --- Start Packing Count and mapping ..........
        packing_pending_count = self.env['packing.list'].search(
            [('state', '=', 'pending')])
        packing_done_count = self.env['packing.list'].search(
            [('state', '=', 'done')])
        packing_cancel_count = self.env['packing.list'].search(
            [('state', '=', 'cancel')])

        invoice_generated_count = self.env['account.invoice'].search(
            [('type','=','out_invoice'), ('state', '=', 'draft')])

        # ------- Purchase -------------

        purchase_confirmation_count = self.env['purchase.order'].search(
            [('state', '=', 'approved'), ('shipped', '=', False), ('date_order', '>=', d_days_ago.strftime('%Y-%m-%d 00:00:00'))])

        purchase_created_count = self.env['purchase.order'].search(
            [('state', '=', 'draft'), ('date_order', '>=', d_days_ago.strftime('%Y-%m-%d 00:00:00'))])

        purchase_cancelled_count = self.env['purchase.order'].search(
            [('state', '=', 'cancel'), ('date_order', '>=', d_days_ago.strftime('%Y-%m-%d 00:00:00'))])

        purchase_closed_order_count = self.env['purchase.order'].search(
            [('shipped', '=', True), ('invoiced', '=', True),
             ('date_order', '>=', d_days_ago.strftime('%Y-%m-%d 00:00:00'))])

        # ------------------ End Purchase --------------------------

        # ------- Invoice -------------

        pending_invoice = self.env['account.invoice'].search(
            [('state', '=', 'draft'), ('name', 'like', 100),
             ('date_invoice', '>=', d_days_ago.strftime('%Y-%m-%d 00:00:00'))])

        valid_invoice = self.env['account.invoice'].search(
            [('state', '=', 'open'), ('date_invoice', '>=', d_days_ago.strftime('%Y-%m-%d 00:00:00'))])

        paid_invoice = self.env['account.invoice'].search(
            [('state', '=', 'paid'), ('date_invoice', '>=', d_days_ago.strftime('%Y-%m-%d 00:00:00'))])

        cancelled_invoice = self.env['account.invoice'].search(
            [('state', '=', 'cancel'), ('date_invoice', '>=', d_days_ago.strftime('%Y-%m-%d 00:00:00'))])

        # ------------------ End Invoice --------------------------

        # ---- sale order
        self.draft_quotation_count = draft_quotation_count
        self.confirm_sale_count = len(confirm_sale_count)
        self.cancel_sale_count = len(cancel_sale_count)
        self.delivered_sale_count = len(delivered_sale_count)
        self.undelivered_sale_count = len(undelivered_sale_count)
        self.partially_delivered_order = len(partially_delivered_order)
        self.full_delivery_count = len(full_delivery_count)
        self.closed_orders_count = len(closed_orders_count)
        self.pending_procurement_orders = len(pending_procurement_orders)
        self.afp_count = len(afp_count)
        # self.afp_count = afp_count
        self.partially_available_for_processing = len(partially_available_for_processing)

        # ---- warehouse
        self.ready_for_transfer_count = len(ready_for_transfer_count)
        self.ready_for_transfer_count_1 = len(ready_for_transfer_count_1)
        self.cancelled_delivery = len(cancelled_delivery)
        self.waiting_for_procurement_count = len(waiting_for_procurement_count)
        self.transferred_delivery = len(transferred_delivery)
        self.partially_available = len(partially_available)
        self.available = len(available)

        self.transferred_received = len(transferred_received)
        self.cancelled_receipt = len(cancelled_receipt)

        # ---- picking order
        self.picking_pending_count = len(picking_pending_count)
        self.picking_done_count = len(picking_done_count)
        self.picking_cancel_count = len(picking_cancel_count)

        # ---- packing order
        self.packing_pending_count = len(packing_pending_count)
        self.packing_done_count = len(packing_done_count)
        self.packing_cancel_count = len(packing_cancel_count)

        # ---- invoice
        self.invoice_generated_count = len(invoice_generated_count)

        # ---- purchase
        self.purchase_created_count = len(purchase_created_count)
        self.purchase_confirmation_count = len(purchase_confirmation_count)
        self.purchase_cancelled_count = len(purchase_cancelled_count)
        self.purchase_closed_order_count = len(purchase_closed_order_count)

        # ---- invoice
        self.pending_invoice = len(pending_invoice)
        self.valid_invoice = len(valid_invoice)
        self.paid_invoice = len(paid_invoice)
        self.cancelled_invoice = len(cancelled_invoice)

    # ---- sale order
    draft_quotation_count = fields.Integer(compute='_get_count')
    confirm_sale_count = fields.Integer(compute='_get_count')
    cancel_sale_count = fields.Integer(compute='_get_count')
    delivered_sale_count = fields.Integer(compute='_get_count')
    undelivered_sale_count = fields.Integer(compute='_get_count')
    partially_delivered_order = fields.Integer(compute='_get_count')
    full_delivery_count = fields.Integer(compute='_get_count')
    closed_orders_count = fields.Integer(compute='_get_count')
    pending_procurement_orders = fields.Integer(compute='_get_count')
    afp_count = fields.Integer(compute='_get_count')
    partially_available_for_processing = fields.Integer(compute='_get_count')

    # ---- warehouse
    ready_for_transfer_count = fields.Integer(compute='_get_count')
    ready_for_transfer_count_1 = fields.Integer(compute='_get_count')
    cancelled_delivery = fields.Integer(compute='_get_count')
    waiting_for_procurement_count = fields.Integer(compute='_get_count')
    transferred_delivery = fields.Integer(compute='_get_count')
    partially_available = fields.Integer(compute='_get_count')
    available = fields.Integer(compute='_get_count')

    transferred_received = fields.Integer(compute='_get_count')
    cancelled_receipt = fields.Integer(compute='_get_count')

    # ---- order picking
    picking_pending_count = fields.Integer(compute='_get_count')
    picking_done_count = fields.Integer(compute='_get_count')
    picking_cancel_count = fields.Integer(compute='_get_count')

    # ---- order picking
    packing_pending_count = fields.Integer(compute='_get_count')
    packing_done_count = fields.Integer(compute='_get_count')
    packing_cancel_count = fields.Integer(compute='_get_count')

    # ---- invoice
    invoice_generated_count = fields.Integer(compute='_get_count')

    # ---- purchase
    purchase_created_count = fields.Integer(compute='_get_count')
    purchase_confirmation_count = fields.Integer(compute='_get_count')
    purchase_cancelled_count = fields.Integer(compute='_get_count')
    purchase_closed_order_count = fields.Integer(compute='_get_count')

    # ---- invoice
    pending_invoice = fields.Integer(compute='_get_count')
    valid_invoice = fields.Integer(compute='_get_count')
    paid_invoice = fields.Integer(compute='_get_count')
    cancelled_invoice = fields.Integer(compute='_get_count')

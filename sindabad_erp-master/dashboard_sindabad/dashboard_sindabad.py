from openerp import models, fields
import datetime


class Dashboard(models.Model):
    _name = "dashboard.sindabad"

    name = fields.Char(string="Name", required=True)


class DashboardWarehouse(models.Model):
    _name = "dashboard.warehouse"

    name = fields.Char(string="Name", required=True)
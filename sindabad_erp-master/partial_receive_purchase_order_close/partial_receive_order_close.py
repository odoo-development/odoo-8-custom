from datetime import date, datetime
from dateutil import relativedelta
import json
import time

import openerp
from openerp.osv import fields, osv
from openerp.tools.float_utils import float_compare, float_round
from openerp.tools.translate import _
from openerp.tools import DEFAULT_SERVER_DATETIME_FORMAT, DEFAULT_SERVER_DATE_FORMAT
from openerp.exceptions import Warning
from openerp import SUPERUSER_ID, api
import openerp.addons.decimal_precision as dp
from openerp.addons.procurement import procurement
import logging
from dateutil.relativedelta import relativedelta
from operator import itemgetter
from openerp.osv import fields, osv, expression
from openerp.tools.float_utils import float_round as round


class purchase_order(osv.osv):
    _inherit = "purchase.order"

    # Received amount
    def _get_received_amount(self, cr, uid, ids, name, arg, context=None):
        res = False
        res = {}

        inv_ids = []
        for po in self.browse(cr, uid, ids, context=context):
            total_sum = 0.00
            for invoice in po.invoice_ids:
                if invoice.type == 'in_invoice':
                    total_sum = total_sum + invoice.amount_total
                if invoice.type == 'in_refund':
                    total_sum = total_sum + invoice.amount_total
            res[po.id] = total_sum

        return res

    _columns = {
        'received_amount': fields.function(_get_received_amount, string='Received Amount', type='float'),
        'order_close_reason': fields.char('Purchase Order Close Reason', required=True),
    }


class partially_received_order_close(osv.osv):
    _name = "partially.received.order.close"
    _description = "Partially received order close"

    _columns = {

        'order_close_reason': fields.char('Order Close Reason', required=True),
        'order_name': fields.char('Order Name'),
        'order_ref': fields.char('Order Ref.'),
    }

    def partial_receive_close(self, cr, uid, ids, context=None):

        close_order_tbl_ids = ids

        ids = context['active_ids']
        order_close_reason = context['order_close_reason']
        po_obj = self.pool.get('purchase.order')

        for po in po_obj.browse(cr, uid, ids, context=context):

            stock_picking_query = "SELECT id FROM stock_picking WHERE origin='{0}' and state!='done'".format(str(po.name))
            cr.execute(stock_picking_query)

            for sp_id in cr.fetchall():
                # unreserve
                self.pool.get('stock.picking').do_unreserve(cr, uid, int(sp_id[0]), context=context)

                # action cancel
                self.pool.get('stock.picking').action_cancel(cr, uid, int(sp_id[0]), context=context)

        # Action Done
        po_obj.wkf_po_done(cr, uid, ids, context=context)

        for po_id in ids:
            po_state_query = "UPDATE purchase_order SET shipped=TRUE, order_close_reason='{0}' WHERE id='{1}'".format(order_close_reason, po_id)

            cr.execute(po_state_query)
            cr.commit()

            for po in po_obj.browse(cr, uid, po_id, context=context):
                order_name = str(po.name)
                order_ref = str(po.client_order_ref)

                for co_id in close_order_tbl_ids:
                    po_state_query = "UPDATE partially_received_order_close SET order_name='{0}', order_ref='{1}' WHERE id='{2}'".format(
                        order_name, order_ref, co_id)

                    cr.execute(po_state_query)
                    cr.commit()

        return True

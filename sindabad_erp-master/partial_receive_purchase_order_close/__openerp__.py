{
    'name': 'Close Partially Received Order',
    'summary': """
    This will Help to close Partial Received Order """,

    'description': """
        * You will get a button to close purchase order in purchase order. It will help you to close a partially purchase order
    """,

    'author': "S. M. Sazedul Haque - Zero Gravity Ventures Ltd",
    'website': "http://zerogravity.com.bd/",
    'version': '1.0.0',
    'category': 'Purchase',
    'depends': ['purchase', 'account', 'stock', 'stock_account'],
    'data': [
            'security/partial_receive_order_close_security.xml',
            'security/ir.model.access.csv',
            'wizard/order_close.xml',
            'purchase_view.xml'
    ],
    # 'css': ['static/src/css/partial_receive_order_close.css'],
    'installable': True,
    'auto_install': False,


}

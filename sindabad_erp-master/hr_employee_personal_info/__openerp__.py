{
    'name': "Employee Information Details",
    'version': '1.0',
    'category': 'Human Resources',
    'author': "Md Rockibul Alam Babu",
    'description': """
        Employee information like. children, education, other qualification, previous employment history and reference details are include this module. 
    """,
    'website': "http://www.zerogravity.com.bd",
    'data': [
        'security/emp_personal_security.xml',
        'security/ir.model.access.csv',
        'report/emp_personal_report.xml',
        'emp_personal_info.xml',
        'hr_employee_info_view.xml',
    ],
    'depends': [
        'hr'
    ],
    'installable': True,
    'auto_install': False,
}

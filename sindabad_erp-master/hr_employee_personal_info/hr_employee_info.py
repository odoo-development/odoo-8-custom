from openerp.osv import fields, osv


class hr_employee(osv.osv):
    _inherit = 'hr.employee'

    _columns = {
        'father_name': fields.char("Father's Name"),
        'mother_name': fields.char("Mother's Name"),
        'date_start': fields.date("Joining Date"),
        'birth_certificate_no': fields.char("Birth Certificate No"),

        'spouse_name': fields.char("Spouse Name"),
        'marriage_date': fields.date("Marriage Date"),
        'spouse_birth_date': fields.date("Birth Date"),
        'spouse_bld_grp': fields.char("Blood Group"),
        'spouse_occupation': fields.char("Occupation"),

        'children_details_line': fields.one2many('children.details', 'chld_id', 'Children Details (if any)'),
        'educational_details_line': fields.one2many('educational.details', 'edu_qua_id', 'Education Qualification'),
        'other_qualification_line': fields.one2many('other.qualification', 'other_qua_id', 'Other Qualification'),
        'previous_job_history_line': fields.one2many('previous.job.history', 'pre_job_id', 'Previous Employment History'),
        'reference_details_line': fields.one2many('reference.details', 'reference_id', 'Reference Details'),
    }


class ChildrenDetails(osv.osv):
    _name = 'children.details'
    _description = "Children Details"

    _columns = {
        'chld_id': fields.many2one('hr.employee'),
        'gender': fields.selection([('male', 'Male'), ('female', 'Female')], 'Gender'),
        'chld_name': fields.char('Name'),
        'chld_date': fields.date('Date of Birth'),
        'chld_edu_qua': fields.char('Education Qualification'),
        'chld_bld_grp': fields.char('Blood Group'),
    }


class EducationalDetails(osv.osv):
    _name = 'educational.details'
    _description = "Education Qualification Details"

    _columns = {
        'edu_qua_id': fields.many2one('hr.employee'),

        'edu_qua_degree': fields.char('Name of Degree'),
        'edu_qua_pass_year': fields.char('Passing Year'),
        'edu_qua_board': fields.char('Board / Institution / Professional Body'),
        'edu_qua_group': fields.char('Major Subject / Group'),
        'edu_qua_cgpa': fields.char('Class / Division / CGPA'),
    }


class OtherQualificationDetails(osv.osv):
    _name = 'other.qualification'
    _description = "Other Qualification Details"

    _columns = {
        'other_qua_id': fields.many2one('hr.employee'),

        'other_qua_course': fields.char('Course attended, Training Received or Certificate Obtained'),
        'other_qua_year': fields.char('Year'),
        'other_qua_result': fields.char('Result'),
    }


class PreviousJobDetails(osv.osv):
    _name = 'previous.job.history'
    _description = "Previous Employment History"

    _columns = {
        'pre_job_id': fields.many2one('hr.employee'),

        'pre_job_organizer': fields.char('Employer / Organization'),
        'pre_job_position': fields.char('Position'),
        'pre_job_duration': fields.char('Duration'),
        'pre_job_salary': fields.float('Last Gross Salary'),
        'pre_job_reference': fields.char('Reference name with Contact number'),
    }


class ReferenceDetails(osv.osv):
    _name = 'reference.details'
    _description = "Reference Details"

    _columns = {
        'reference_id': fields.many2one('hr.employee'),

        'refer_name': fields.char('Name'),
        'refer_designation': fields.char('Designation'),
        'refer_organization': fields.char('Organization'),
        'refer_contact': fields.char('Contact Number'),
        'refer_relationship': fields.char('Relationship'),
    }
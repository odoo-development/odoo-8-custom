{
    'name': "Sale Order Invoice Created and Full Refund Mark Data",
    'version': '1.0',
    'category': 'Sale',
    'author': "Shuvarthi Dhar",
    'description': """
Invoice Create and Full Refund Sale Order Mark Data
    """,
    'website': "http://www.sindabad.com",
    'depends': ['base', 'odoo_magento_connect', 'account', 'sale', 'stock','order_approval_process','send_sms_on_demand'],
    'data': [
        'views/so_invoice_create_full_refund_script.xml'
    ],

    'installable': True,
    'application': True,
    'auto_install': False,
}

from openerp.osv import fields, osv
import datetime

class sale_order(osv.osv):
    _inherit = "sale.order"


    def _so_invoice_create_full_refund_mark(self, cursor, uid, ids = None, context = None):


        for order in self.browse(cursor, uid, self.search(cursor, uid, [('state', '!=', 'cancel'),('date_order','>','2019-12-15')]), context=context):

            if order.invoice_ids:
                out_refund_amount = 0
                out_invoice_amount = 0

                invoice_created_query = "UPDATE sale_order SET invoice_created= TRUE WHERE id={0}".format(order.id)
                cursor.execute(invoice_created_query)
                cursor.commit()

                for invoice in order.invoice_ids:

                    if invoice.type == 'out_invoice':
                        out_invoice_amount += invoice.amount_total

                    if invoice.type == 'out_refund':
                        out_refund_amount += invoice.amount_total

                if out_invoice_amount == out_refund_amount:
                    full_refund_invoiced_query = "UPDATE sale_order SET full_refund_invoiced = TRUE WHERE id={0}".format(order.id)
                    cursor.execute(full_refund_invoiced_query)
                    cursor.commit()

        return True


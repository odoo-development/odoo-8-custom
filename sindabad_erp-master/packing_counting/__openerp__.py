{
    'name': "Packing Counting Process",
    'version': '1.0',
    'category': 'Sale',
    'author': "Shuvarthi Dhar",
    'description': """
Packing Counting Process
    """,
    'website': "http://www.sindabad.com",
    'depends': ['packings_invoice_challan','send_sms_on_demand'],
    'data': [

        'views/packing_counting_wizard.xml',
        'views/packing_counting_view.xml',
        'views/packing_view.xml',
    ],

    'installable': True,
    'application': True,
    'auto_install': False,
}

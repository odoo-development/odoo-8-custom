from openerp.osv import osv, fields
from openerp import SUPERUSER_ID, api
from openerp.tools.translate import _
from datetime import datetime
import datetime
from time import gmtime, strftime

class PackingCount(osv.osv):
    _name = 'packing.count'
    _description = "Packing Counting"

    _columns = {
        'packing_id': fields.many2one('packing.list', 'Packing Reference'),
        'packing_id_char': fields.char('Packing'),
        'name': fields.char('Name'),
        'total_packing': fields.integer('Total Packing'),
        'not_required': fields.boolean('Not Required'),
        'warehouse_id': fields.selection([
            (1, 'Central Warehouse Uttara'),
            (2, 'Nodda Warehouse'),
            (3, 'Nodda Retail Warehouse'),
            (6, 'Uttara Retail Warehouse'),
        ], 'Warehouse', copy=False, help="Gives the List of the Warehouses", select=True),

        'count_line': fields.one2many('packing.count.line', 'packing_count_id', 'Count Line'),

           }

    def default_get(self, cr, uid, fields, context=None):

        if context is None:
            context = {}
        res = super(PackingCount, self).default_get(cr, uid, fields, context=context)
        res['packing_id']=context['active_id']
        res['packing_id_char']=context['active_id']

        return res

    def packing_confirm(self, cr, uid, ids, context=None):

        if context is None:
            context = {}
        get_data = self.read(cr, uid, ids)[0]

        if get_data['not_required']==False and not get_data['count_line'] :
            cr.rollback()
            raise osv.except_osv (_('Error!'), _('Enter the proper counting'))
        elif get_data['count_line'] :
            if get_data['warehouse_id']:
                total = 0
                for counting in self.pool.get('packing.count.line').browse(cr, uid, get_data['count_line'], context=None):
                    total+= counting.packing_quantity

                update_query = "UPDATE packing_count SET total_packing={0} WHERE id={1}".format(total,ids[0])

                cr.execute(update_query)
                cr.commit()
                stock_id = self.create_challan(cr, uid, ids, context=None)
                self.pool.get('packing.list.line').create(cr, uid, {'packing_id':get_data['packing_id'][0],'stock_id': stock_id, 'magento_no': False, 'details': False,
                                'receive_date': datetime.datetime.now(), 'order_number': False, 'return_date': False}, context=None)

                self.pool.get('packing.list').make_confirm(cr, uid, [get_data['packing_id'][0]], context=None)
            else:
                cr.rollback()
                raise osv.except_osv(_('Error!'), _('Select the correct warehouse'))
        else:
            self.pool.get('packing.list').make_confirm(cr, uid, [get_data['packing_id'][0]], context=None)

        return True

    def create_challan(self, cr, uid, ids, context=None):

        id = ids[0] if len(ids) > 0 else None
        stn_data = self.browse(cr, uid, id, context=context)
        product_ids = self.pool['product.product'].search(cr, uid, [('product_name', '=', 'Packet')],
                                                                        context=context)
        stock_picking_type_ids = self.pool['stock.picking.type'].search(cr, uid, [('name', 'like', 'Delivery Orders')],
                                                                        context=context)
        stock_picking_type_data = self.pool['stock.picking.type'].browse(cr, uid, stock_picking_type_ids,
                                                                         context=context)
        location_id = None
        location_dest_id = None
        picking_type_id = None

        for items in stock_picking_type_data:


            if items.warehouse_id.id == stn_data.warehouse_id:
                location_id = items.default_location_src_id.id
                picking_type_id = items.id
                location_dest_id = items.default_location_dest_id.id

        if location_id is None:
            cr.rollback()
            raise osv.except_osv(_('Error!'), _('Select the correct warehouse'))

        move_line = []
        stock_data = {}

        if location_id is not None and location_dest_id is not None:
            stock_data = {'origin': False, 'message_follower_ids': False, 'carrier_tracking_ref': False,
                          'number_of_packages': 0, 'date_done': False, 'carrier_id': False, 'write_uid': False,
                          'partner_id': False, 'message_ids': False, 'note': False, 'picking_type_id': picking_type_id,
                          'move_type': 'one', 'company_id': 1, 'priority': '1', 'picking_type_code': False,
                          'owner_id': False, 'min_date': False, 'date': strftime("%Y-%m-%d %H:%M:%S", gmtime()),
                          'pack_operation_ids': [],
                          'carrier_code': 'custom', 'invoice_state': 'none'}


            move_line.append([0, False,
                              {'product_uos_qty': stn_data.total_packing,
                               'date_expected': strftime("%Y-%m-%d %H:%M:%S", gmtime()),
                               'date': strftime("%Y-%m-%d %H:%M:%S", gmtime()),
                               'product_id': product_ids[0] , 'product_uom': 1,
                               'picking_type_id': picking_type_id, 'product_uom_qty': stn_data.total_packing,
                               'invoice_state': '2binvoiced', 'product_tmpl_id': False, 'product_uos': False,
                               'reserved_quant_ids': [], 'location_dest_id': location_dest_id,
                               'procure_method': 'make_to_stock',
                               'product_packaging': False, 'group_id': False, 'location_id': location_id,
                               'name': 'Packet'}])

            stock_data['move_lines'] = move_line

        stock_obj = self.pool.get('stock.picking')
        save_the_data = stock_obj.create(cr, uid, stock_data, context=context)
        if save_the_data:
            stock_obj_confirmation = stock_obj.action_confirm(cr, uid, [save_the_data], context=context)
            stock_obj_force_assign = stock_obj.force_assign(cr, uid, [save_the_data], context=context)
            stock_obj.action_assign(cr, uid, [save_the_data], context=context)

            stock_picking = self.pool.get('stock.picking').do_enter_transfer_details(cr, uid, [save_the_data],
                                                                                     context=context)

            trans_obj = self.pool.get('stock.transfer_details')
            trans_search = trans_obj.search(cr, uid, [('picking_id', '=', save_the_data)], context=context)

            trans_search = [trans_search[len(trans_search) - 1]] if len(trans_search) > 1 else trans_search

            trans_browse = self.pool.get('stock.transfer_details').browse(cr, uid, trans_search, context=context)

            trans_browse.do_detailed_transfer()
            #
            # picking = [save_the_data]
            #
            # context.update({
            #
            #     'active_model': 'stock.picking',
            #     'active_ids': picking,
            #     'active_id': len(picking) and picking[0] or False,
            #     'rtv': True,
            #     'rtv_id': rtv_odj.id
            #
            # })

            # created_id = self.pool['stock.transfer_details'].create(cr, uid, {
            #     'picking_id': len(picking) and picking[0] or False}, context)
            #
            # if created_id:
            created_id = self.pool.get('stock.transfer_details').browse(cr, uid, save_the_data, context=context)
            created_id.picking_id.do_transfer()
                # if created_id.picking_id.state in ['assigned', 'partially_available']:
                #     # raise Warning(_('You cannot transfer a picking in state \'%s\'.') % created_id.picking_id.state)
                #
                #     processed_ids = []
                #     # Create new and update existing rtv operations
                #     for lstits in [created_id.item_ids, created_id.packop_ids]:
                #         for prod in lstits:
                #             for rtv_line in rtv_odj.rtv_process_line:
                #                 if prod.product_id.id == rtv_line.product_id.id:
                #                     pack_datas = {
                #                         'product_id': prod.product_id.id,
                #                         'product_uom_id': prod.product_uom_id.id,
                #                         'product_qty': rtv_line.return_qty,
                #                         'package_id': prod.package_id.id,
                #                         'lot_id': prod.lot_id.id,
                #                         'location_id': prod.sourceloc_id.id,
                #                         'location_dest_id': prod.destinationloc_id.id,
                #                         'result_package_id': prod.result_package_id.id,
                #                         'date': prod.date if prod.date else datetime.datetime.now(),
                #                         'owner_id': prod.owner_id.id,
                #                     }
                #                     if prod.packop_id:
                #                         prod.packop_id.with_context(no_recompute=True).write(pack_datas)
                #                         processed_ids.append(prod.packop_id.id)
                #                     else:
                #                         pack_datas['picking_id'] = created_id.picking_id.id
                #                         packop_id = created_id.env['stock.pack.operation'].create(pack_datas)
                #                         processed_ids.append(packop_id.id)
                #     # Delete the others
                #     packops = created_id.env['stock.pack.operation'].search(
                #         ['&', ('picking_id', '=', created_id.picking_id.id), '!', ('id', 'in', processed_ids)])
                #     packops.unlink()
                #     # Execute the transfer of the picking
                #     created_id.picking_id.do_transfer()

        return save_the_data


class PackingCountLine(osv.osv):
    _name = 'packing.count.line'
    _description = "Packing Count List"

    _columns = {
        'packing_count_id': fields.many2one('packing.count', 'Count Reference', required=True, ondelete='cascade', select=True,
                                      readonly=True),
        'size': fields.selection([
            ('small', 'Small'),
            ('medium', 'Medium'),
            ('large', 'Large'),
            ('xl', 'XL'),

        ], 'Size', copy=False, help="Packing Size", select=True),
        'packing_quantity': fields.integer('Packing Quantity'),
    }

class PackingList(osv.osv):
    _inherit = "packing.list"

    _columns = {
        'packing_count_ids': fields.one2many('packing.count', 'packing_id', 'Packing Count')
    }
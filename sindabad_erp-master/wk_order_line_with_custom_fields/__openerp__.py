# -*- coding: utf-8 -*-
#################################################################################
#
#    Copyright (c) 2016-Present Webkul Software Pvt. Ltd. (<https://webkul.com/>)
#
#################################################################################
{
    "name": "Order Lines with Images",
    "category": 'Webkul Software Pvt. Ltd.',
    "summary": """
        Add Custome field and Prodcut Image in Order Lines of Sale, Purcahse and Also in Report.""",


    "sequence": 1,
    "author": "Webkul Software Pvt. Ltd.",
    "website": "http://www.webkul.com",
    "version": '1.0',
    "depends": [ 'purchase'],
    "data": [
        'views/order_list_view_image_assets.xml',
        'views/core_inherited_view.xml',
        
        
    ],
    "installable": True,
    "application": True,
    "auto_install": False,
}

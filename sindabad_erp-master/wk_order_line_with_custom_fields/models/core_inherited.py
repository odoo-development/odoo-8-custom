# -*- coding: utf-8 -*-
##########################################################################
#
#    Copyright (c) 2016-Present Webkul Software Pvt. Ltd. (<https://webkul.com/>)
#
##########################################################################


from openerp.osv import fields, osv
from openerp.tools.translate import _




################## .............Sale Order Line inherit.............###########


class sale_order_line(osv.Model):

    _name = 'sale.order.line'
    _inherit = 'sale.order.line'

    def _get_so_line_number(self, cr, uid, ids, fields, arg, context=None):
        if context is None: 
            context = {}
        res = {}
        so_line = 1
        so_line_recs = self.browse(cr, uid, ids, context=context)[0]
        for line_rec in so_line_recs.order_id.order_line:
            res[line_rec.id] = so_line
            so_line += 1

        return res

    _columns = {
        'so_line_no': fields.function(_get_so_line_number, type='integer', string='S.N'),
        'image_small' : fields.related("product_id","image_small",type="binary",string='Product Image'),
        'so_line_prod_uom' : fields.related("product_id","prod_uom",type="char",string='UOM'),
    }

    def product_id_change(self, cr, uid, ids, pricelist, product, qty=0,
                          uom=False, qty_uos=0, uos=False, name='', partner_id=False,
                          lang=False, update_tax=True, date_order=False, packaging=False, fiscal_position=False, flag=False, context=None):
        
        context = context or {}
        res = super(sale_order_line, self).product_id_change(cr, uid, ids, pricelist, product, qty=qty,
                                                             uom=uom, qty_uos=qty_uos, uos=uos, name=name, partner_id=partner_id,
                                                             lang=lang, update_tax=update_tax, date_order=date_order, packaging=packaging, fiscal_position=fiscal_position, flag=flag, context=context)
        if product:
            product_obj = self.pool.get('product.product')
            product_obj = product_obj.browse(cr, uid, product, context=context)
            res['value'].update({'image_small': product_obj.image_small or False})
            res['value'].update({'so_line_prod_uom': product_obj.prod_uom or False})
        return res


sale_order_line()

################## .............Purchase order Line inherit.............###########

class purchase_order_line(osv.Model):

    _name = 'purchase.order.line'
    _inherit = 'purchase.order.line'

    def _get_po_line_number(self, cr, uid, ids, fields, arg, context=None):
        if context is None: 
            context = {}
        res = {}
        po_line = 1
        po_line_recs = self.browse(cr, uid, ids, context=context)[0]
        for line_rec in po_line_recs.order_id.order_line:
            res[line_rec.id] = po_line
            po_line += 1

        return res

    _columns = {
        'po_line_no': fields.function(_get_po_line_number, type='integer', string='S.N'),
        'image_small' : fields.related("product_id","image_small",type="binary",string='Product Image'),
        'po_line_prod_uom': fields.related("product_id","prod_uom",type="char",string='UOM'),
    }

    def onchange_product_id(self, cr, uid, ids, pricelist_id, product_id, qty, uom_id,
            partner_id, date_order=False, fiscal_position_id=False, date_planned=False,
            name=False, price_unit=False, state='draft', context=None):
        
        context = context or {}
        res = super(purchase_order_line, self).onchange_product_id(cr, uid, ids, pricelist_id, product_id, qty=qty,
                                                             uom_id=uom_id, partner_id=partner_id, date_order=date_order, fiscal_position_id=fiscal_position_id, date_planned=date_planned,
                                                             name=name, price_unit=price_unit, state=state, context=context)
        if product_id:
            product_obj = self.pool.get('product.product')
            product_obj = product_obj.browse(cr, uid, product_id, context=context)
            res['value'].update({'image_small': product_obj.image_small or False})
            res['value'].update({'po_line_prod_uom': product_obj.prod_uom or False})
        return res


purchase_order_line()


# END
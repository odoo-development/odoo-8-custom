openerp.wk_order_line_with_custom_fields = function(openerp) {
    var _t = openerp.web._t;

    openerp.web.list.Binary.include({
        placeholder: "/web/static/src/img/placeholder.png",
        _format: function (row_data, options) {
            var value = row_data[this.id].value;
            var download_url;
            if (value && value.substr(0, 10).indexOf(' ') == -1) {
                download_url = "data:image/png;base64," + value;
            } else {
                download_url = openerp.session.url('/web/binary/image', {model: options.model, field: this.id, id: options.id});
            }

            return _.template("<img width='50' height='50' src='<%-href%>'/>", {
                href: download_url,
            });
        }

    });
}

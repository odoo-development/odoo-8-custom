# Author Mufti Muntasir Ahmed 22/03/2018
import time
from openerp.report import report_sxw
from openerp.osv import osv


class HistoryData(report_sxw.rml_parse):

    def __init__(self, cr, uid, name, context):
        super(HistoryData, self).__init__(cr, uid, name, context)
        self.localcontext.update({
            'get_client_order_ref': self.get_client_order_ref,


        })
    def get_client_order_ref(self, obj):

        return 'THis is a tese case for all'


    def set_context(self, objects, data, ids, report_type=None):

        test_data={1:'100'}


        start_date = data.get('form').get('date_from')
        end_date = data.get('form').get('date_to')
        purchase_order = self.pool['purchase.order']
        purchase_orders = purchase_order.search(self.cr, self.uid,
                                             [('date_order', '<=', end_date),
                                              ('date_order', '>=', start_date)])

        purchase_list = purchase_order.browse(self.cr, self.uid, purchase_orders)

        data_list =[]

        for po_items in purchase_list:


            temp_dict ={}
            temp_dict['vendor_name']=po_items.partner_id.name
            temp_dict['po_number']=po_items.name
            temp_dict['po_date']=po_items.date_order
            mail_message_list = self.pool['mail.message']
            histories = mail_message_list.search(self.cr, self.uid,
                                                 [('model', '=', 'purchase.order'), ('res_id', '=', po_items.id),
                                                  ('record_name', '=', po_items.name)])

            movelines = mail_message_list.browse(self.cr, self.uid, histories)


            rfq_created = ''
            rfq_confirm = ''
            inv_paid = ''
            grn_rec = ''
            inv_rec = ''

            for items in movelines:

                if 'Invoice paid' in items.body:
                    inv_paid +=str(items.write_date) + ', '

                if 'RFQ created' in items.body:
                    rfq_created +=str(items.write_date) + ', '

                if 'RFQ Confirmed' in items.body:
                    rfq_confirm +=str(items.write_date) + ', '

                if 'Products received' in items.body:
                    grn_rec +=str(items.write_date) + ', '

                if 'Invoice received' in items.body:
                    inv_rec +=str(items.write_date) + ', '

            temp_dict['rfq_created']=rfq_created
            temp_dict['rfq_confirm']=rfq_confirm
            temp_dict['inv_paid']=inv_paid
            temp_dict['grn_rec']=grn_rec
            temp_dict['inv_rec']=inv_rec

            data_list.append(temp_dict)




        self.localcontext.update({

            'getLines': {1:data_list},

        })


        return super(HistoryData, self).set_context(objects, data, ids, report_type)

    def _history_data_get(self,test=None):

        start_date = self.context.get('start_date')
        end_date = self.context.get('end_date')

        mail_message_list = self.pool['mail.message']
        histories = mail_message_list.search(self.cr, self.uid,
                [('model', '=', 'purchase.order'),('write_date','<=', end_date), ('write_date','>=',start_date)])

        movelines = mail_message_list.browse(self.cr, self.uid, histories)
        
        return 'Cooooooooooooooooooooooool'




class report_procurementhistory(osv.AbstractModel):
    _name = 'report.procurement_history_report.report_procurementhistory'
    _inherit = 'report.abstract_report'
    _template = 'procurement_history_report.report_procurementhistory'
    _wrapped_report_class = HistoryData

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:

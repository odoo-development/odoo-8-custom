#!/usr/bin/python
from __future__ import print_function
from openerp.modules.registry import RegistryManager
from openerp.api import Environment
from openerp.http import request
import datetime
import oerplib
# from addons.odoo_to_magento_api_connect.api_connect import get_magento_token, submit_request
import re
import xmlrpclib
import json
import requests
import logging

TOKEN_API = '/integration/admin/token'

# def get_magento_token(oerp, cr, uid, order_ids):
def get_magento_token(oerp, order_ids):
    # order_ids = [13673, 13672, 13671, 13670, 13669, 13668]
    instance_id = 1

    """
    if order_ids:
        for mag_ord_id in self_obj.pool.get('magento.orders').search(cr, uid, [('order_ref', '=', order_ids[0])]):
            for mag_obj in self_obj.pool.get('magento.orders').browse(cr, uid, [mag_ord_id]):
                instance_id = mag_obj.instance_id.id
    """
    obj = oerp.get('magento.configure').browse([instance_id])

    """
    obj
    magento.configure(1, )
    """
    """
    url = obj.name + "/index.php/rest/V1" + TOKEN_API
    user = obj.user
    pwd = obj.pwd
    """

    url = "https://sindabad.com" + "/index.php/rest/V1" + TOKEN_API
    user = "odoomagento"
    pwd = "admin1234"

    cred_dict = {"username": user, "password": pwd}
    cred = json.dumps(cred_dict)

    header = {"Content-type": "application/json"}
    res = requests.post(str(url), data=cred, headers=header)

    token = None
    if res.status_code == 200:
        token = "Bearer " + str(res.json())

    return token, "https://sindabad.com"

def chunks(l, n):
    """Yield successive n-sized chunks from l."""
    for i in xrange(0, len(l), n):
        yield l[i:i + n]

# def stock_synch_from_script(cr, uid, env, context=None):
def stock_synch_from_script():

    oerp = oerplib.OERP('192.168.9.88', protocol='xmlrpc', port=6224)

    # Login (the object returned is a browsable record)
    user = oerp.login('admin.it', 'admin1234', '12345')

    # Start product list query From here

    product_obj = oerp.get('product.product')

    # product_id_lists_all = list()
    # if not all_data:
        # product_id_lists = product_obj.search(cr, uid, [('stock_sync_req', '=', True)])
    # product_id_query = "SELECT id FROM product_product WHERE stock_sync_req = TRUE"
    # cr.execute(product_id_query)
    #for p_id in cr.fetchall():
    #    product_id_lists_all.append(p_id[0])
        
    # else:
        # product_id_lists = product_obj.search(cr, uid, [('id', '=', all_data['ids'][0])])
    #     product_id_lists_all.append(all_data['ids'][0])

    product_id_lists_all = product_obj.search(['&', ('qty_available', '>', 0), ('stock_sync_req', '=', True)])
    product_id_lists_chunks = [x for x in chunks(product_id_lists_all, 25)]

    # sms_text = "Counting ({0}) : Starts with Button".format(len(product_id_lists_all))
    # name = "stock.sync"
    # self.pool.get("send.sms.on.demand").send_sms_on_demand(cr, uid, product_id_lists_all, sms_text, "+8801817535299, +8801716520313", name, context=context)

    sms_resp = requests.get('http://api.rmlconnect.net:8080/bulksms/bulksms?username=SindabadSGNmask&password=sin@1234&type=0&dlr=1&destination=8801716520313,8801817535299&source=Sindabad&message=Started%20crontab')

    counter = 1
    for product_id_lists in product_id_lists_chunks:

        products_obj_line = product_obj.browse(product_id_lists)

        
        mapping_ids = oerp.get('magento.product').search([('oe_product_id', 'in', product_id_lists)])

        odoo_magento_dict ={} # keys are odoo id and values are magento ID

        for map_id in mapping_ids:

            try:
                items = oerp.get('magento.product').browse(map_id)
                odoo_magento_dict[items.oe_product_id] = items.mag_product_id
            except:
                pass

        # for items in mapping_objects:
        #    odoo_magento_dict[items.oe_product_id] = items.mag_product_id

        magento_updated_qty = {} # keys are magento id and values are qty to update

        stored_list =[]
	
	# import pdb;pdb.set_trace()

        for prod_item in products_obj_line:
            qty=0

            if odoo_magento_dict.get(prod_item.id) is not None:
                tmp={}
                # qty = prod_item.qty_available - prod_item.pending_qty - prod_item.outgoing_qty
                # qty = (prod_item.qty_available - prod_item.pending_qty - prod_item.outgoing_qty) - prod_item.damage_quantity
                qty = prod_item.synch_qty
		qty =qty if qty >0 else 0

                tmp['qty']=qty
                tmp['threshold_qty']=prod_item.reorder_qty_level

                stored_list.append({
                    'sku':prod_item.default_code,
                    'qty':qty,
                    'threshold_qty':prod_item.reorder_qty
                })

                magento_updated_qty[odoo_magento_dict.get(prod_item.id)] = tmp

        qty=0
        stock=True
        count=0

        success_story = []
        success_story_2 = []
        sale_order_ids = []
        
        # token, url_root = get_magento_token(oerp, cr, uid, sale_order_ids)
	token, url_root = get_magento_token(oerp, sale_order_ids)
	# token = "Bearer 907u7j9xpsqvfo6dku1s6tzieidf1bf0"
        # url_root = "https://odootest.sindabad.com" 

        token = token.replace('"', "")
        headers = {'Authorization': token, 'Content-Type': 'application/json'}
        stock_list =[]

        for mage_product_id,tmp in magento_updated_qty.items():

            ## Calling server starts from
            sale_order_ids=[]

            if int(tmp.get('qty')) > 0:
                stock_list.append({
                    "product_id": mage_product_id,
                    "stock_item": {

                                "qty": tmp.get('qty'),
                                "is_in_stock": stock,
                                "extension_attributes": {
                                    "threshold_qty": tmp.get('threshold_qty')
                                }
                            }
                })
            else:
                stock_list.append({
                    "product_id": mage_product_id,
                    "stock_item": {

                                "qty": tmp.get('qty'),
                                "extension_attributes": {
                                    "threshold_qty": tmp.get('threshold_qty')
                                }
                            }
                })

        product_stock_data = {"stockItems": stock_list}

        url = url_root + "/index.php/rest/V1/odoomagentoconnect/productStockBulk/"
        data = json.dumps(product_stock_data)

        try:
            resp = requests.post(url, data=data, headers=headers)

            # sms_text = "Count {0} ({1}, {2}) : sucess with Button".format(counter, len(product_id_lists_all), len(product_id_lists))
            # name = "stock.sync"
            # self.pool.get("send.sms.on.demand").send_sms_on_demand(cr, uid, product_id_lists, sms_text, "+8801817535299, +8801716520313", name, context=context)
        except:
            # sms_text = "Count {0} ({1}, {2}) : fail with Button".format(counter, len(product_id_lists_all), len(product_id_lists))
            # name = "stock.sync"
            # self.pool.get("send.sms.on.demand").send_sms_on_demand(cr, uid, product_id_lists, sms_text, "+8801817535299, +8801716520313", name, context=context)

            pass
        
        # SMS
        counter += 1
        
        print("-------------------------")
        print("{0} products syncing response: {1} at {2}".format(str(len(product_id_lists)), str(resp), str(datetime.datetime.now() + datetime.timedelta(hours=6))))
        print("-------------------------")

        log_data ={
            'entry_log_time':datetime.datetime.now(),
            'stock_sync_log': str(stored_list),
            'response_from_magento': str(resp),
            'total_sku_from_odoo': str(len(stored_list))
        }
        
        try:
            stock_log = env['stock.synch.log'].create(log_data)
        except:
            # sms_text = "Log entry error with Button"
            # name = "stock.sync"
            # self.pool.get("send.sms.on.demand").send_sms_on_demand(cr, uid, product_id_lists, sms_text, "+8801817535299, +8801716520313", name, context=context)

            pass
            # success_story.append(mage_product_id)
            # success_story_2.append(resp)


            # By getting rsponses data ends here

        # break
    
    # sms_text = "Counting ({0}) : Ends with Button".format(len(product_id_lists_all))
    # name = "stock.sync"
    # self.pool.get("send.sms.on.demand").send_sms_on_demand(cr, uid, product_id_lists_all, sms_text, "+8801817535299, +8801716520313", name, context=context)

    sms_resp = requests.get('http://api.rmlconnect.net:8080/bulksms/bulksms?username=SindabadSGNmask&password=sin@1234&type=0&dlr=1&destination=8801716520313,8801817535299&source=Sindabad&message=Completed%20crontab')

    return True

def connect(dbname='sindabad_db09032020', uid=1, context=None):
    r = RegistryManager.get(dbname)
    cr = r.cursor()
    Environment.reset()
    env = Environment(cr, uid, context or {})
    # print('Connected to %s with user %s %s' % (dbname, env.uid, env.user.name))

    # env['ir.ui.menu'].create({'name': name,   'action': 'ir.actions.client,%d' % action.id})
    # sms_text = "Test msg from script"
    # name = "stock.sync"
    
    # product_ids = [64011, 64010]
    # self.pool.get("send.sms.on.demand").send_sms_on_demand(cr, uid, product_id_lists_all, sms_text, "+8801817535299, +8801716520313", name, context=context)
    # env["send.sms.on.demand"].send_sms_on_demand(uid, [64011, 64010], sms_text, "+8801817535299", name)
    # env["send.sms.on.demand"].send_sms_on_demand([64011, 64010], sms_text, "+8801817535299, +8801716520313", name, context=env.context)
    # products = env["product.product"].browse(product_ids)

    # log_data ={'stock_sync_log': str("stored_list"), 'response_from_magento': str("resp"), 'total_sku_from_odoo': str(10) }

    # stock_log = env['stock.synch.log'].create(log_data)
    stock_synch_from_script(cr, uid, env, context=context)
    
    return env

if __name__ == '__main__':
    
    # connect()

    stock_synch_from_script()
    
    # print(__doc__)

# python -i odoo-sh.py

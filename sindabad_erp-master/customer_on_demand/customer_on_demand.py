from openerp.osv import fields, osv


class res_partner(osv.osv):
    _inherit = "res.partner"


    _columns = {

        'cus_on_demand': fields.boolean('On Demand'),

    }




class sale_order(osv.osv):
    _inherit = "sale.order"


    def create(self, cr, uid, vals, context=None):

        so_id = super(sale_order, self).create(cr, uid, vals, context=context)

        order_info = self.browse(cr, uid, [so_id], context=context)

        parent = order_info.partner_id

        company = False

        for i in range(5):
            if len(parent.parent_id) == 1:
                parent = parent.parent_id

            else:
                company = parent
                break

        if company:

            if company.cus_on_demand or parent.cus_on_demand == True:
                so_billing_pattern = 'on_demand'

            else:
                so_billing_pattern = 'on_delivery'

            order_info.write({'so_billing_pattern': so_billing_pattern})

        return so_id



    _columns = {
        'so_billing_pattern': fields.selection([('on_delivery', 'On Delivery'), ('on_demand', 'On Demand')], 'Billing Pattern'),
    }


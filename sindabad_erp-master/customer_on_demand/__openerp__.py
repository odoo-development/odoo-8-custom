{
    'name': "Customer on demand",
    'version': '1.0',
    'category': 'Sale',
    'author': "Md Rockibul Alam Babu",
    'description': """
Customer on demand in customer form
    """,
    'website': "http://www.sindabad.com",
    'depends': [
        'sale', 'order_approval_process', 'sales_collection_billing_process','send_sms_on_demand'
    ],
    'data': [
        'security/on_demand_security.xml',
        'views/so_billing_pattern.xml',
        'views/customer_on_demand.xml',
    ],

    'installable': True,
    'application': True,
    'auto_install': False,
}

from datetime import datetime, timedelta
import time
from datetime import date
from openerp import SUPERUSER_ID
from openerp.osv import fields, osv
from openerp.tools.translate import _
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT
import openerp.addons.decimal_precision as dp
from openerp import workflow
from openerp import api
import datetime
import re

import logging
_logger = logging.getLogger(__name__)


class send_sms_on_demand(osv.osv):

    _name = "send.sms.on.demand"

    def send_sms_on_demand(self, cr, uid, ids, sms_text, mobile_numbers, name, context=None):

        """
        ids: purchase: ids: [12745]
        sms_text: u'Hello Fair Electronics Limited,\nThe purchase order : PO12743,\nhas been sent to your email. Thank you'
        mobile_numbers: u'01817535299'
        pr_obj: purchase.order(12745,)
        name: pr_obj.name: u'PO12743'
        context: {'lang': 'en_US', 'params': {'action': 429}, 'tz': 'Asia/Dhaka', 'uid': 1}
        """
        try:

            new_r = self.pool.get("sms.sms").send_sms_by_ssl(cr, uid, ids, body_sms=sms_text, mob_no=mobile_numbers, uniqueid=name, context=context)
        except:
            pass

        return True

    def get_customer_mobile(self, cr, uid, so, context=None):
        mobile = ''

        # res_partner_obj = self.pool.get("res.partner")
        # parent = res_partner_obj.browse(cr, uid, customer_id, context=context)

        parent = so.partner_id

        for i in range(5):
            if len(parent.parent_id) == 1:
                parent = parent.parent_id
            else:
                mobile = parent.mobile
                # company_id = parent.id
                break

        return mobile

class PackingList(osv.osv):

    _inherit = 'packing.list'

    def make_confirm(self, cr, uid, ids, context=None):
        resp = super(PackingList, self).make_confirm(cr, uid, ids, context=context)
        so_env = self.pool.get('sale.order')
        if resp:
            pack = self.pool.get("packing.list").browse(cr, uid, ids, context=context)
            sms_obj = self.pool.get("send.sms.on.demand")

            for line in pack.packing_line:
                try:
                    so_id = so_env.search(cr, uid,[('client_order_ref', '=', str(line.magento_no))],context=context)
                    so = so_env.browse(cr, uid, so_id, context=context)
                    mobile_numbers = so.partner_shipping_id.mobile

                    name = "packing.list." + str(pack.id)

                    order = so.client_order_ref

                    sms_text = "Your order #{0} is being packed and will be delivered within next 24 hours.".format(order)

                    try:
                        if mobile_numbers:
                            sms_obj.send_sms_on_demand(cr, uid, ids, sms_text, mobile_numbers, name,context=context)

                    except:
                        pass

                except:
                    pass
        return True


class wms_manifest_process(osv.osv):

    _inherit = "wms.manifest.process"

    def confirm_wms_manifest(self, cr, uid, ids, context=None):
        resp = super(wms_manifest_process, self).confirm_wms_manifest(cr, uid, ids, context=context)

        if resp:
            # send sms
            wms_man = self.pool.get("wms.manifest.process").browse(cr, uid, ids, context=context)
            # account_inv = self.pool["account.invoice"]
            so_obj = self.pool.get("sale.order")
            res_partner_obj = self.pool.get("res.partner")
            try:
                for line in wms_man.picking_line:

                    customer_name = line.customer_name
                    name = "wms.manifest.process." + str(wms_man.id)
                    order = line.magento_no
                    mobile_numbers = line.phone
                    amount = line.amount
                    so_list = so_obj.search(cr, uid, [('client_order_ref', '=', str(order))], context=context)
                    so = so_obj.browse(cr, uid, so_list, context=context)
                    sms_text = ""
                    sms_text_rm = ""
                    rm_mobile_numbers = ''

                    company_id = ""
                    parent = so.partner_id
                    classification = str(so.customer_classification)
                    email = ''

                    for i in range(5):
                        if len(parent.parent_id) == 1:
                            parent = parent.parent_id
                        else:
                            rm_mobile_numbers = parent.user_id.partner_id.phone
                            email = parent.email
                            # company_id = parent.id
                            break

                    order_warehouse = str(so.warehouse_id.name)
                    # Partial delivery messages
                    total_inv_amount = 0

                    for inv in so.invoice_ids:
                        total_inv_amount += inv.amount_total

                    if wms_man.assigned_to:
                        assigned_to = str(wms_man.assigned_to)
                    else:
                        assigned_to = ''

                    if total_inv_amount < so.amount_total:

                        try:
                            compare_date_obj = datetime.datetime.now() + datetime.timedelta(days=7)
                            expected_shipment_date = compare_date_obj.strftime('%Y-%m-%d')

                            if 'Corporate' in classification :
                                partial_delivery_msg = "Your order #{0} has been shipped partially and will be delivered today by Sindabad Wish Master {1}, the rest will be shipped soon. Sorry for your inconveniences.".format(
                                    order, assigned_to)
                            else:

                                partial_delivery_msg = "Your order #{0} has been shipped partially and will be delivered today by Sindabad Wish Master {1}, the rest will be shipped soon. Sorry for your inconveniences. Please keep Tk {2} ready.".format(order,assigned_to, format(int(round(total_inv_amount)), ','))

                            self.pool.get("send.sms.on.demand").send_sms_on_demand(cr, uid, ids, partial_delivery_msg,
                                                                                   mobile_numbers, name, context=context)
                        except:
                            pass
                    elif "Cash On Delivery" in str(so.note):
                        if 'Corporate' in classification:

                            sms_text = "Your order #{0} has been shipped and will be delivered today by Sindabad Wish Master {1}.".format(order, assigned_to)

                        else:
                            sms_text = "Your order #{0} has been shipped and will be delivered today by Sindabad Wish Master {1}. Please keep Tk {2} ready.".format(order, assigned_to, format(int(round(amount)), ','))

                    else:
                        if 'Corporate' in classification:
                            sms_text = "Your order #{0} has been shipped and will be delivered today by Sindabad Wish Master {1}.".format(order, assigned_to)
                        else:
                            sms_text = "Your order #{0} has been shipped and will be delivered today by Sindabad Wish Master {1}. Please keep Tk {2} ready.".format(order, assigned_to, format(int(round(amount)), ','))

                    sms_text_rm = "Dear RM, Order {0} of your client {1} has been shipped with Value Tk {2} from {3}.".format(order, customer_name, format(int(round(amount)), ','), order_warehouse)

                    # only retail customer will get the SMS
                    # if "Retail" in str(so.customer_classification):
                    self.pool.get("send.sms.on.demand").send_sms_on_demand(cr, uid, ids, sms_text, mobile_numbers, name, context=context)

                    if rm_mobile_numbers:
                        self.pool.get("send.sms.on.demand").send_sms_on_demand(cr, uid, ids, sms_text_rm, rm_mobile_numbers, name, context=context)

                    try:
                        # send email
                        if not email[0].isdigit() and sms_text!='':
                            # tmp_data = e_log_obj.browse(cr, uid, email_log_id, context=context)
                            # send email
                            if re.search("^(\d+)@sindabad.com$", email):
                                sin_email_match = re.search("^(\d+)@sindabad.com$", email)
                            elif re.search("^sin(\d+)@sindabad.com$", email):
                                sin_email_match = re.search("^sin(\d+)@sindabad.com$", email)
                            elif re.search("@sindabadretail.com$", email):
                                sin_email_match = re.search("@sindabadretail.com$", email)
                            else:
                                sin_email_match = None

                            if sin_email_match is None:
                                e_log_obj = self.pool.get('send.email.on.demand.log')

                                email_data = {
                                    'mail_text': sms_text,
                                    'model': "sale.order",
                                    'so_id': so.id,
                                    'email': email
                                }

                                email_log_id = e_log_obj.create(cr, uid, email_data, context=context)
                                email_template_obj = self.pool.get('email.template')
                                template_ids = email_template_obj.search(cr, uid, [('name', '=', 'Shipped')], context=context)
                                email_template_obj.send_mail(cr, uid, template_ids[0], so.id, force_send=True, context=context)
                    except:
                        pass

                    try:
                        context['status'] = 'dispatched'
                        self.pool.get('order.status.synch.log').offer_notification_api(cr, uid, order, context)
                    except:
                        pass
            except:
                pass

        return True


class wms_manifest_outbound(osv.osv):

    _inherit = "wms.manifest.outbound"

    def create(self, cr, uid, vals, context=None):

        outbound = super(wms_manifest_outbound, self).create(cr, uid, vals, context=context)
        try:
            self.deliverd_dispatched_msg(cr, 1, [outbound], context=None)
        except:
            pass
        return outbound

    def receipt_return(self, cr, uid, ids, context=None):
        resp = super(wms_manifest_outbound, self).receipt_return(cr, uid, ids, context=context)

        if resp:
            """
            Delivered: Your order #[Order ID] has been delivered successfully . Thanks for staying with us at sindabad.com. Helpline: 09612-002244
            Return: Your shipment of [Tk amount] is being returned due to [Reason]. For any query please call us on Helpline: 09612-002244
            """

            wms_out = self.pool.get("wms.manifest.outbound").browse(cr, uid, ids, context=context)
            so_obj = self.pool.get("sale.order")

            sms_obj = self.pool.get("send.sms.on.demand")
            order_id = wms_out.name
            invoice_id = wms_out.outbound_line[0].invoice_id if wms_out.outbound_line else False
            if invoice_id:
                inv = self.pool.get("account.invoice").browse(cr, uid, [int(invoice_id)], context=context)

                mobile_numbers = inv.partner_id.phone
                name = "wms.manifest.outbound." + str(wms_out.id)
                so_list = so_obj.search(cr, uid, [('client_order_ref', '=', str(order_id))], context=context)
                so = so_obj.browse(cr, uid, so_list, context=context)

                order = so.client_order_ref
                amount = inv.amount_total
                customer_name = ''
                rm_mobile_numbers = ''
                rm_name = ''
                parent = so.partner_id

                for i in range(5):
                    if len(parent.parent_id) == 1:
                        parent = parent.parent_id
                    else:
                        rm_mobile_numbers = parent.user_id.partner_id.phone
                        rm_name = parent.user_id.partner_id.name
                        customer_name = parent.name
                        # company_id = parent.id
                        break

                # only retail customer will get the SMS
                # sms_text_delivered = "Your order %23{0} has been delivered successfully. Thank you for staying with us. Try our mobile app to get offers at https://rebrand.ly/tar7z".format(order_id)
                # sms_text_rm = "Dear {4}, Order %23{0} of your client {1} - Mob- {2} has been delivered with Value Tk {3}.".format(order, customer_name, mobile_numbers, format(int(round(amount)), ','),rm_name)
                #
                # # if "Retail" in str(so.customer_classification):
                # sms_obj.send_sms_on_demand(cr, uid, ids, sms_text_delivered, mobile_numbers, name, context=context)
                # if rm_mobile_numbers:
                #     sms_obj.send_sms_on_demand(cr, uid, ids, sms_text_rm, rm_mobile_numbers, name, context=context)

                returned_amount = 0
                for line in wms_out.outbound_line:
                    if line.return_qty > 0:
                        single_price = line.amount / line.quantity
                        returned_amount += line.return_qty * single_price

                sms_text_return = "Your shipment of Tk #{0} of order {1} has been returned. For any query please call us on Helpline: 09612002244".format(format(int(round(returned_amount)), ','), order)

                sms_text_return_rm = "Dear {4}, Order # {0} of your client {1} - Mob- {2} has been returned with Value Tk {3}.".format(
                    order, customer_name, mobile_numbers, format(int(round(returned_amount)), ','), rm_name)

                # only retail customer will get the SMS
                # if returned_amount > 0 and "Retail" in str(so.customer_classification):
                # if returned_amount > 0:
                    # sms_obj.send_sms_on_demand(cr, uid, ids, sms_text_return, mobile_numbers, name, context=context)
                    # if rm_mobile_numbers:
                    #     sms_obj.send_sms_on_demand(cr, uid, ids, sms_text_return_rm, rm_mobile_numbers, name, context=context)

            try:
                if context.has_key('full_return') and context.get('full_return'):
                    context['status'] = 'full return'
                else:
                    context['status'] = 'partially delivered'
                self.pool.get('order.status.synch.log').offer_notification_api(cr, uid, order_id, context)
            except:
                pass

        return True

    def deliverd_dispatched_msg(self, cr, uid, ids, context=None):

        resp = self.browse(cr, uid, ids, context=context)
        if resp:

            wms_out = self.pool.get("wms.manifest.outbound").browse(cr, uid, ids, context=context)
            order_id = wms_out.name
            try:

                """
                Delivered: Your order #[Order ID] has been delivered successfully . Thanks for staying with us at sindabad.com. Helpline: 09612-002244
                Return: Your shipment of [Tk amount] is being returned due to [Reason]. For any query please call us on helpline: 09612-002244
                
                Partial Delivery: Hi <Customer First Name>, your order <order number> has been dispatched partially amounting <amount>. Rest item/s will be dispatched within few days. You will be notified before dispatch. A list of items which could not be shipped are already shared in your email. We are sorry for any inconveniences for partial delivery. To enquire more, please call 09612-002244
                """

                so_obj = self.pool.get("sale.order")

                sms_obj = self.pool.get("send.sms.on.demand")

                invoice_id = wms_out.outbound_line[0].invoice_id if wms_out.outbound_line else False
                if invoice_id:
                    try:
                        inv = self.pool.get("account.invoice").browse(cr, uid, [int(invoice_id)], context=context)

                        mobile_numbers = inv.partner_id.phone
                        name = "wms.manifest.outbound." + str(wms_out.id)
                        so_list = so_obj.search(cr, uid, [('client_order_ref', '=', str(order_id))], context=context)
                        so = so_obj.browse(cr, uid, so_list, context=context)
                        mobile_numbers = self.pool.get("send.sms.on.demand").get_customer_mobile(cr, 1, so, context=context)

                        order = so.client_order_ref
                        amount = inv.amount_total
                        customer_name = ''
                        rm_mobile_numbers = ''
                        parent = so.partner_id
                        email = ''
                        rm_name = ''
                        classification = str(so.customer_classification)

                        for i in range(5):
                            if len(parent.parent_id) == 1:
                                parent = parent.parent_id
                            else:
                                rm_mobile_numbers = parent.user_id.partner_id.phone
                                customer_name = parent.name
                                rm_name = parent.user_id.partner_id.name
                                email = str(parent.email)
                                # company_id = parent.id
                                break

                        # only retail customer will get the SMS
                        sms_text_delivered = "Your order #{0} has been delivered successfully. Thank you for staying with us. Try our mobile app to get offers at https://rebrand.ly/tar7z".format(order_id)

                        sms_text_rm = "Dear {4}, Order #{0} of your client {1} - Mob- {2} has been delivered with Value Tk {3}.".format(order, customer_name, mobile_numbers, format(int(round(amount)), ','),rm_name)




                        # Partial delivery messages
                        total_inv_amount = 0

                        for inv in so.invoice_ids:

                            total_inv_amount += inv.amount_total

                        if total_inv_amount < so.amount_total:

                            try:
                                compare_date_obj = datetime.datetime.now() + datetime.timedelta(days=7)
                                expected_shipment_date = compare_date_obj.strftime('%Y-%m-%d')
                                if 'Corporate' in classification:
                                    partial_delivery_msg = "Your order #{0} has been shipped partially, the rest will be shipped soon. Sorry for your inconveniences.".format(order)
                                else:

                                    partial_delivery_msg = "Your order #{0} has been shipped partially, the rest will be shipped soon. Sorry for your inconveniences. Please keep Tk {1} ready.".format(order, format(int(round(total_inv_amount)), ','))

                                sms_obj.send_sms_on_demand(cr, uid, ids, partial_delivery_msg, mobile_numbers, name, context=context)
                            except:
                                pass

                        # if "Retail" in str(so.customer_classification):
                        try:
                            try:
                                if total_inv_amount >= so.amount_total:
                                    sms_obj.send_sms_on_demand(cr, uid, ids, sms_text_delivered, mobile_numbers, name, context=context)
                                    sms_obj.send_sms_on_demand(cr, uid, ids, sms_text_rm, rm_mobile_numbers, name, context=context)
                            except:
                                pass
                            try:
                                if not email[0].isdigit():
                                    if re.search("^(\d+)@sindabad.com$", email):
                                        sin_email_match = re.search("^(\d+)@sindabad.com$", email)
                                    elif re.search("^sin(\d+)@sindabad.com$", email):
                                        sin_email_match = re.search("^sin(\d+)@sindabad.com$", email)
                                    elif re.search("@sindabadretail.com$", email):
                                        sin_email_match = re.search("@sindabadretail.com$", email)
                                    else:
                                        sin_email_match = None

                                    if sin_email_match is None:
                                        email_template_obj = self.pool.get('email.template')
                                        template_ids = email_template_obj.search(cr, uid, [('name', '=', 'Order Delivery')], context=context)
                                        email_template_obj.send_mail(cr, uid, template_ids[0], so.id, force_send=True, context=context)
                            except:
                                pass


                            returned_amount = 0
                            for line in wms_out.outbound_line:
                                if line.return_qty > 0:
                                    single_price = line.amount / line.quantity
                                    returned_amount += line.return_qty * single_price

                            sms_text_return = "Your shipment of Tk {0} of order #{1} has been returned. For any query please call us on Helpline: 09612002244".format(format(int(round(returned_amount)), ','), order)

                            # only retail customer will get the SMS
                            # if returned_amount > 0 and "Retail" in str(so.customer_classification):
                            # if returned_amount > 0:
                            #     sms_obj.send_sms_on_demand(cr, uid, ids, sms_text_return, mobile_numbers, name, context=context)
                        except:
                            pass

                        # send email
                        if not email[0].isdigit():

                            e_log_obj = self.pool.get('send.email.on.demand.log')

                            email_data = {
                                'mail_text': sms_text_delivered,
                                'model': "sale.order",
                                'so_id': so.id,
                                'email': email
                            }

                            email_log_id = e_log_obj.create(cr, uid, email_data, context=context)
                            # tmp_data = e_log_obj.browse(cr, uid, email_log_id, context=context)

                            # send email
                    except:
                        pass
            except:
                pass
            try:

                context={'status': 'delivered'}
                self.pool.get('order.status.synch.log').offer_notification_api(cr, uid, order_id, context)
            except:
                pass
        return True

# (a) Welcome Message:
"""
Hello
Welcome to Sindabad.com Ltd! We are happy to have you on
board.
Please visit Sindabad.com to discover thousands of products.
"""
class res_partner(osv.osv):

    _inherit = 'res.partner'

    @api.model
    def create(self, vals):

        try:
            if vals.has_key('mobile'):
                vals['phone'] = vals['mobile']
            elif vals.has_key('phone'):
                vals['mobile'] = vals['phone']
        except:
            pass
        partner = super(res_partner, self).create(vals)

        try:
            phone_number = ''
            if vals.has_key('mobile'):
                phone_number = vals['mobile']
            if not phone_number:
                phone_number = vals['phone']
            name = vals['name']

            email = ''
            if vals.has_key('email'):
                email = vals['email']
            """
            sms_text = ''
            if phone_number:
                sms_text = "Hello,\n Welcome to Sindabad.com Ltd! We are happy to have you on board. Please visit Sindabad.com to discover thousands of products."

                self.pool.get("send.sms.on.demand").send_sms_on_demand(self._cr, self._uid, self._ids, sms_text, phone_number, name, context=self._context)

            # send email
            if email:
                e_log_obj = self.pool.get('send.email.on.demand.log')

                email_data = {
                    'mail_text': sms_text,
                    'model': "res.partner",
                    # 'so_id': ids[0],
                    'email': email
                }

                email_log_id = e_log_obj.create(self._cr, self._uid, email_data, context=self._context)
                # tmp_data = e_log_obj.browse(cr, uid, email_log_id, context=context)

                # send email
                email_template_obj = self.pool.get('email.template')
                template_ids = email_template_obj.search(self._cr, self._uid, [('name', '=', 'SO process mail')], context=self._context)

                email_template_obj.send_mail(self._cr, self._uid, template_ids[0], email_log_id, force_send=True, context=self._context)
            """
        except:
            pass

        return partner


# (b) Order hold for due Message
"""
Dear Valued Customer,
Your credit amount 'amount' has exceed the limit. Please
clear the previous dues so that process the order. For
further details, please contact your relationship manager.
"""
# implemented in order_approval_process

# (c) Order Confirmation Message
"""
Hello "Company Name" ,
Your order "order number" has been placed on "Sunday, date
and time" via "Cash On Delivery/on credit". You will be
updated with another email after your item(s) has been
dispatched.
"""
class sale_order(osv.osv):
    _inherit = "sale.order"

    def action_button_confirm(self, cr, uid, ids, context=None):

        super(sale_order, self).action_button_confirm(cr, uid, ids, context)

       


        return True

    def action_cancel(self, cr, uid, ids, context=None):
        super(sale_order, self).action_cancel(cr, uid, ids, context)

        try:
            so = self.browse(cr, uid, ids, context=context)

            parent = so.partner_id

            for i in range(5):
                if len(parent.parent_id) == 1:
                    parent = parent.parent_id
                else:
                    parent = parent

                    break

            company_phone = parent.mobile
            phone_number = str(so.partner_id.mobile) if so.partner_id.mobile else company_phone
            so_cancel_date = datetime.datetime.now().strftime('%A, %d-%m-%Y')
            if not phone_number:
                phone_number = str(so.partner_id.phone)

            email = str(so.partner_id.email)
            cancel_reason = ""
            if so.cancel_co_reason:
                cancel_reason += str(so.cancel_co_reason).replace("_", " ")
            if so.cancel_quotation_reason:
                cancel_reason += str(so.cancel_quotation_reason).replace("_", " ")

            if cancel_reason == 'apnd':
                cancel_reason = "Advance payment not done"

            sms_text = ''
            if phone_number and cancel_reason:
                sms_text = "Hello {0}, we are sorry to inform that your order #{1} has been canceled due to '{2}'".format(str(parent.name), str(so.client_order_ref), cancel_reason)

                self.pool.get("send.sms.on.demand").send_sms_on_demand(cr, uid, ids, sms_text, phone_number, str(parent.name), context=context)

            # send email
            try:
                if email and cancel_reason:

                    # tmp_data = e_log_obj.browse(cr, uid, email_log_id, context=context)

                    # send email
                    if re.search("^(\d+)@sindabad.com$", email):
                        sin_email_match = re.search("^(\d+)@sindabad.com$", email)
                    elif re.search("^sin(\d+)@sindabad.com$", email):
                        sin_email_match = re.search("^sin(\d+)@sindabad.com$", email)
                    elif re.search("@sindabadretail.com$", email):
                        sin_email_match = re.search("@sindabadretail.com$", email)
                    else:
                        sin_email_match = None

                    if sin_email_match is None:
                        e_log_obj = self.pool.get('send.email.on.demand.log')

                        email_data = {
                            'mail_text': sms_text,
                            'model': "sale.order",
                            'so_id': ids[0],
                            'email': email
                        }

                        email_log_id = e_log_obj.create(cr, uid, email_data, context=context)
                        email_template_obj = self.pool.get('email.template')
                        template_ids = email_template_obj.search(cr, uid, [('name', '=', 'Order Canceled')], context=context)

                        email_template_obj.send_mail(cr, uid, template_ids[0], ids[0], force_send=True, context=context)
            except:
                pass
        except:
            pass

        return True

# Order Dispatch and Delivery Message
"""
Same as email and SMS send to retail customer
"""

# (d) Order Cancelled message
"""
Hello "Company Name" ,
Your order "order number" has been cancelled on "Sunday,
date and time" due to "cancel reason".
"""
# implemented in def action_cancel(self, cr, uid, ids, context=None):


# Delay Message (Category Head)
"""
Order number "Number" is pending for 2/4 days
due to unavailability of product "Product name".
"""
class category_head_mapping(osv.osv):

    _name = "category.head.mapping"

    _columns = {
        'ch_mapping': fields.text("Category Head Mapping {'category_id':user_id,'category_id':user_id}"),
    }

    def product_unavailable_order_process_ch(self, cr, uid, ids, context=None):
        import json
        uid = 1

        try:
            cat_head_obj = self.pool.get('category.head.mapping')
            cat_head = cat_head_obj.browse(cr, uid, [1], context=context)
            # mapping_data = json.load(cat_head.mapping)
            mapping_data = eval(str(cat_head.ch_mapping))

            comm_heads = dict()

            date_4 = datetime.datetime.now() - datetime.timedelta(days=4)
            date_2 = datetime.datetime.now() - datetime.timedelta(days=2)

            date_before_4 = date_4.strftime('%Y-%m-%d')
            date_before_2 = date_2.strftime('%Y-%m-%d')

            so_obj = self.pool.get("sale.order")
            so_list = so_obj.search(cr, uid, [("date_order", ">", date_before_4), ("date_order", "<", date_before_2), ("state", "!=", "cancel"), ("state", "!=", "done")], context=context)
            sos = so_obj.browse(cr, uid, so_list, context=context)

            users_obj = self.pool.get("res.users")
            for order in sos:
                for line in order.order_line:
                    sms_text = "Order number %23{0} is pending for 2/4 days due to unavailability of product {1}.".format(str(order.client_order_ref), str(line.product_id.product_name))
                    categ_id = str(line.product_id.categ_id.id)
                    categ_head_id = mapping_data[categ_id]

                    # if not comm_heads.has_key(categ_id) and mapping_data.has_key(categ_id):
                    #    comm_heads[categ_id] = mapping_data[categ_id]

                    partner = users_obj.browse(cr, uid, categ_head_id, context=context)

                    phone_number = str(partner.partner_id.mobile)
                    email = str(partner.partner_id.email)

                    # send sms
                    if phone_number:
                        self.pool.get("send.sms.on.demand").send_sms_on_demand(cr, uid, ids, sms_text, phone_number, str(partner.partner_id.name), context=context)

                    # send email
                    if email:
                        e_log_obj = self.pool.get('send.email.on.demand.log')

                        email_data = {
                            'mail_text': sms_text,
                            'model': "sale.order",
                            'so_id': order.id,
                            'email': email
                        }

                        email_log_id = e_log_obj.create(cr, uid, email_data, context=context)
                        # tmp_data = e_log_obj.browse(cr, uid, email_log_id, context=context)

                        # send email
                        email_template_obj = self.pool.get('email.template')
                        template_ids = email_template_obj.search(cr, uid, [('name', '=', 'SO process mail')], context=context)

                        email_template_obj.send_mail(cr, uid, template_ids[0], email_log_id, force_send=True, context=context)
            """
            res_user_obj = self.pool.get('res.users')
            for key in comm_heads:
                cat_heads = res_user_obj.browse(cr, uid, [comm_heads[key]], context=context).partner_id

                # sms & email through cat_heads
            """
        except:
            pass

        return True


# Delay Message (Finance) (hold it for as discussed with Salman vai)
"""
Order number "Number" is pending for 2/4 days.
Please Pay PO "Number" to get the products.
"""


# Delay Message (Warehouse)
"""
Order number "number" is pending for 2/4 days
and ready to deliver. Please deliver the order.
"""
class warehouse_head_mapping(osv.osv):
    _name = "warehouse.head.mapping"

    _columns = {
        'wh_mapping': fields.text("Warehouse Head Mapping {'warehouse_id':user_id,'warehouse_id':user_id}"),
    }

    def product_unavailable_order_process_wh(self, cr, uid, ids, context=None):
        import json
        uid = 1
        try:
            wh_head_obj = self.pool.get('warehouse.head.mapping')
            wh_head = wh_head_obj.browse(cr, uid, 1, context=context)
            # mapping_data = json.loads(str(wh_head.wh_mapping))
            mapping_data = eval(str(wh_head.wh_mapping))

            comm_heads = dict()

            date_4 = datetime.datetime.now() - datetime.timedelta(days=4)
            date_2 = datetime.datetime.now() - datetime.timedelta(days=2)

            date_before_4 = date_4.strftime('%Y-%m-%d')
            date_before_2 = date_2.strftime('%Y-%m-%d')
            
            so_obj = self.pool.get("sale.order")
            so_list = so_obj.search(cr, uid,  [("date_order", ">", date_before_4), ("date_order", "<", date_before_2), ("state", "!=", "cancel"), ("state", "!=", "done")], context=context)
            sos = so_obj.browse(cr, uid, so_list, context=context)
            
            users_obj = self.pool.get("res.users")
            for order in sos:
                wh_id = str(order.warehouse_id.id)
                wh_head_id = mapping_data[wh_id]

                sms_text = "Order number %23{0} is pending for 2/4 days and ready to deliver. Please deliver the order.".format(str(order.client_order_ref))

                partner = users_obj.browse(cr, uid, wh_head_id, context=context)

                phone_number = str(partner.partner_id.mobile)
                email = str(partner.partner_id.email)

                # send sms
                if phone_number:
                    self.pool.get("send.sms.on.demand").send_sms_on_demand(cr, uid, ids, sms_text, phone_number, str(partner.partner_id.name), context=context)

                # send email
                if email:
                    e_log_obj = self.pool.get('send.email.on.demand.log')

                    email_data = {
                        'mail_text': sms_text,
                        'model': "sale.order",
                        'so_id': order.id,
                        'email': email
                    }

                    email_log_id = e_log_obj.create(cr, uid, email_data, context=context)
                    # tmp_data = e_log_obj.browse(cr, uid, email_log_id, context=context)

                    # send email
                    email_template_obj = self.pool.get('email.template')
                    template_ids = email_template_obj.search(cr, uid, [('name', '=', 'SO process mail')], context=context)

                    email_template_obj.send_mail(cr, uid, template_ids[0], email_log_id, force_send=True, context=context)
        except:
            pass

        return True

class send_email_on_demand_log(osv.osv):
    _name = "send.email.on.demand.log"
    _description = "Send Email on Demand Log"

    _columns = {
        'mail_text': fields.text('Email text)'),
        'model': fields.char("Model"),
        'so_id': fields.integer("Model Obj ID"),
        'email': fields.char("Email"),
    }


class bridge_backbone(osv.osv_memory):
    _inherit = "bridge.backbone"

    def order_cancel(self, cr, uid, order_id, context=None):

        try:
            so_obj= self.pool.get("sale.order")

            so = so_obj.browse(cr, uid, order_id, context=context)
            parent = so.partner_id
            email = ''
            parent_name = ''

            for i in range(5):
                if len(parent.parent_id) == 1:
                    parent = parent.parent_id
                else:
                    parent_name = parent.name
                    email = parent.email
                    # company_id = parent.id
                    break

            try:
                if email:
                    suborder_date = datetime.datetime.now().strftime('%A, %d-%m-%Y')

                    """
                    Hello Zeeshan, Your order 1000090122 has been cancelled and a suborder has been created on Tuesday, 21-07-2020 due to modification in your order.  You will shortly receive an email with the new order id
                    """
                    cancel_reason = ""
                    if so.cancel_co_reason:
                        cancel_reason += str(so.cancel_co_reason).replace("_", " ")
                    if so.cancel_quotation_reason:
                        cancel_reason += str(so.cancel_quotation_reason).replace("_", " ")

                    if not cancel_reason:
                        cancel_reason = "modification in your order"

                    msg_text = "Hello {0}, we are sorry to inform that your order #{1} has been canceled due to {2}.".format(parent_name, so.client_order_ref, cancel_reason)

                    e_log_obj = self.pool.get('send.email.on.demand.log')

                    email_data = {
                        'mail_text': msg_text,
                        'model': "sale.order",
                        'so_id': so.id,
                        'email': email
                    }

                    email_log_id = e_log_obj.create(cr, uid, email_data, context=context)
                    # tmp_data = e_log_obj.browse(cr, uid, email_log_id, context=context)

                    # send email
                    if re.search("^(\d+)@sindabad.com$", email):
                        sin_email_match = re.search("^(\d+)@sindabad.com$", email)
                    elif re.search("^sin(\d+)@sindabad.com$", email):
                        sin_email_match = re.search("^sin(\d+)@sindabad.com$", email)
                    elif re.search("@sindabadretail.com$", email):
                        sin_email_match = re.search("@sindabadretail.com$", email)
                    else:
                        sin_email_match = None

                    if sin_email_match is None:
                        email_template_obj = self.pool.get('email.template')
                        template_ids = email_template_obj.search(cr, uid, [('name', '=', 'Order Canceled')], context=context)

                        email_template_obj.send_mail(cr, uid, template_ids[0], so.id, force_send=True, context=context)
            except:
                pass

        except:
            pass

        super(bridge_backbone, self).order_cancel(cr, uid, order_id, context=context)

        return False
{
    'name': 'Send SMS on Demand',
    'version': '8.0.0.6.0',
    'author': "Shahjalal Hossain",
    'category': 'SMS',
    'summary': 'Send SMS on Demand',
    'depends': ['base', 'odoo_magento_connect', 'account', 'sale', 'purchase', 'wms_manifest', 'order_cancel_reasons'],
    'data': [
        'security/head_mapping_security.xml',
        'security/ir.model.access.csv',
        'views/c_head_mapping_view.xml',
        'views/w_head_mapping_view.xml',
        'head_mapping_menu.xml',
        'ch_wh_com_sync_scheduler.xml',

    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}

import binascii
import json
import re
import xmlrpclib

import requests
import urllib
from openerp import SUPERUSER_ID, api, workflow
from openerp.http import request
from openerp.osv import fields, osv
from openerp.tools.translate import _


class product_product(osv.osv):
    _inherit = 'product.product'

    _columns = {
        'product_name': fields.char('Product Name'),
    }

    def get_max_website_sequence(self, cr, uid, context=None):

        max_website_sequence = 0

        max_sequence_query = "SELECT MAX(website_sequence) from product_template"
        cr.execute(max_sequence_query)
        for max_seq in cr.fetchall():
            max_website_sequence = max_seq[0]

        return max_website_sequence

    def create_simple_from_config(self, cr, uid, product_id, product_name, context=None):

        product = self.browse(cr, uid, [product_id])
        try:
            old_product_tmpl_id = product.product_tmpl_id.id

            # search old_product_tmpl_id contains how many products
            # if count 1 no need to create new product template
            # if count more than 1, then create new template with the reference if old_product_tmpl_id

            product_id_list = self.search(cr, uid, [('product_tmpl_id', '=', old_product_tmpl_id)])

            if (len(product_id_list) > 1) or (len(product_id_list) == 1 and product.id != product_id_list[0]):
                # create new template with old_product_tmpl_id
                new_prod_tmpl_id = None
                prod_tmpl_vals = dict()

                # self.pool.get('hr.employee').browse(cr, uid, ids, context=context)
                # prod_tmpl_env = self.env['product.template']
                # prod_tmpl_obj = prod_tmpl_env.search([('id', '=', old_product_tmpl_id)])
                prod_tmpl_obj = self.pool.get('product.template').browse(cr, uid, [old_product_tmpl_id], context=context)

                prod_tmpl_vals["warranty"] = prod_tmpl_obj.warranty
                prod_tmpl_vals["uos_id"] = prod_tmpl_obj.uos_id.id
                prod_tmpl_vals["list_price"] = prod_tmpl_obj.list_price
                prod_tmpl_vals["weight"] = prod_tmpl_obj.weight
                prod_tmpl_vals["color"] = prod_tmpl_obj.color
                prod_tmpl_vals["image"] = prod_tmpl_obj.image
                prod_tmpl_vals["mes_type"] = prod_tmpl_obj.mes_type
                prod_tmpl_vals["uom_id"] = prod_tmpl_obj.uom_id.id
                prod_tmpl_vals["description_purchase"] = prod_tmpl_obj.description_purchase
                prod_tmpl_vals["uos_coeff"] = prod_tmpl_obj.uos_coeff
                prod_tmpl_vals["sale_ok"] = prod_tmpl_obj.sale_ok
                prod_tmpl_vals["categ_id"] = prod_tmpl_obj.categ_id.id
                prod_tmpl_vals["product_manager"] = prod_tmpl_obj.product_manager

                prod_tmpl_vals["company_id"] = prod_tmpl_obj.company_id.id
                prod_tmpl_vals["uom_po_id"] = prod_tmpl_obj.uom_po_id.id
                prod_tmpl_vals["description_sale"] = prod_tmpl_obj.description_sale
                prod_tmpl_vals["description"] = prod_tmpl_obj.description
                prod_tmpl_vals["active"] = prod_tmpl_obj.active
                prod_tmpl_vals["rental"] = prod_tmpl_obj.rental
                prod_tmpl_vals["image_medium"] = prod_tmpl_obj.image_medium
                prod_tmpl_vals["name"] = product_name
                prod_tmpl_vals["type"] = prod_tmpl_obj.type
                prod_tmpl_vals["image_small"] = prod_tmpl_obj.image_small
                prod_tmpl_vals["track_all"] = prod_tmpl_obj.track_all

                prod_tmpl_vals["track_outgoing"] = prod_tmpl_obj.track_outgoing

                prod_tmpl_vals["track_incoming"] = prod_tmpl_obj.track_incoming
                prod_tmpl_vals["sale_delay"] = prod_tmpl_obj.sale_delay
                prod_tmpl_vals["expense_pdt"] = prod_tmpl_obj.expense_pdt
                prod_tmpl_vals["income_pdt"] = prod_tmpl_obj.income_pdt
                prod_tmpl_vals["available_in_pos"] = prod_tmpl_obj.available_in_pos
                prod_tmpl_vals["to_weight"] = prod_tmpl_obj.to_weight
                prod_tmpl_vals["purchase_ok"] = prod_tmpl_obj.purchase_ok
                prod_tmpl_vals["track_production"] = prod_tmpl_obj.track_production
                prod_tmpl_vals["produce_delay"] = prod_tmpl_obj.produce_delay
                prod_tmpl_vals["hr_expense_ok"] = prod_tmpl_obj.hr_expense_ok
                prod_tmpl_vals["event_ok"] = prod_tmpl_obj.event_ok
                prod_tmpl_vals["website_sequence"] = self.get_max_website_sequence(cr, uid, context=context)

                prod_tmpl_vals["website_published"] = prod_tmpl_obj.website_published
                prod_tmpl_vals["website_size_x"] = prod_tmpl_obj.website_size_x
                prod_tmpl_vals["attribute_set_id"] = prod_tmpl_obj.attribute_set_id.id
                prod_tmpl_vals["prod_type"] = prod_tmpl_obj.prod_type

                # create template with prod_tmpl_vals
                new_prod_tmpl_id = self.pool.get('product.template').create(cr, uid, prod_tmpl_vals, context=context)

                # at the time of template creation odoo creates it's corresponding child product
                # so delete duplicate products
                duplicate_prod_ids = self.search(cr, uid, [('product_tmpl_id', '=', new_prod_tmpl_id)])
                for dup_prod in self.browse(cr, uid, duplicate_prod_ids, context=context):
                    if not dup_prod.default_code:
                        # delete row
                        del_row_query = "DELETE FROM product_product WHERE id={0}".format(dup_prod.id)
                        cr.execute(del_row_query)
                        cr.commit()

                return new_prod_tmpl_id
            elif len(product_id_list) == 1 and product.id == product_id_list[0]:

                prod_template_id = product.product_tmpl_id.id
                del_attribute_query = "DELETE FROM product_attribute_line WHERE product_tmpl_id={0}".format(prod_template_id)
                cr.execute(del_attribute_query)
                cr.commit()

                prod_name_up_query = "UPDATE product_template SET name=$${0}$$ WHERE id={1}".format(product_name, prod_template_id)
                cr.execute(prod_name_up_query)
                cr.commit()

                return None

            else:

                return None
        except:
            return None

    def write(self, cr, uid, ids, vals, context=None):

        product_name = None
        if vals.has_key('product_name') and vals['product_name']:
            product_name = str(urllib.unquote(str(vals['product_name'])).decode('utf8')).replace("+", " ")
            vals['product_name'] = product_name

        if vals.has_key('product_tmpl_id'):
            if vals['product_tmpl_id'] == 0:

                del vals['product_tmpl_id']

                product_id = 0
                if type(ids) is list and len(ids) == 1:
                    product_id = ids[0]
                else:
                    product_id = ids

                # the method returns None or value
                simple_product_tmpl_id = self.create_simple_from_config(cr, uid, product_id, product_name, context=context)
                if simple_product_tmpl_id:

                    vals['product_tmpl_id'] = simple_product_tmpl_id

        # default works
        try:
            record = super(product_product, self).write(cr, uid, ids, vals, context=context)
        except:
            record = None
            pass

        return record

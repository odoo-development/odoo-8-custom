{
    'name': 'Product simple sync',
    'version': '1.0',
    'category': 'Product simple sync',
    'author': 'Shahjalal Hossain',
    'summary': 'Product simple sync',
    'description': 'Product simple sync',
    'depends': ['base', 'odoo_magento_connect', 'stock'],

    'installable': True,
    'application': True,
    'auto_install': False,
}

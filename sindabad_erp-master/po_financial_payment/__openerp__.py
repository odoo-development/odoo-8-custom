{
    'name': 'PO Financial Payment',
    'version': '1.0',
    'category': 'Finance',
    'author': 'Shahjalal',
    'summary': 'PO Financial Payment',
    'description': 'PO Financial Payment',
    'depends': ['base', 'purchase', 'product', 'saad_po'],
    'data': [
        'security/po_financial_payment_security.xml',
        'security/ir.model.access.csv',
        'wizard/financial_payment_wizard.xml',
        'purchase_view.xml',
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}

from openerp.osv import fields, osv, expression



class purchase_order(osv.osv):
    _inherit = "purchase.order"

    _columns = {
        'payment_from':fields.many2one('account.account', 'Payment From'),## CR
        'payment_to':fields.many2one('account.account', 'Payment To'), ## DR
        'check_no': fields.char('Check Number'),  # must need to give if check no if "payment_type:check"
        'ref': fields.char('Reference'),
        'adv_amount': fields.float('Advance Amount'),
        'journal_entry_id': fields.many2one('account.move', 'Journal Entry'), ## DR

    }


class po_financial_payment(osv.osv):
    _name = "po.financial.payment"
    _description = "PO Financial Payment"

    _columns = {
        'payment_from':fields.many2one('account.account', 'Payment From', required=True),## CR
        'payment_to':fields.many2one('account.account', 'Payment To', required=True), ## DR
        'period_id':fields.many2one('account.period', 'Period ', required=True),
        'check_no': fields.char('Check Number'),  # must need to give if check no if "payment_type:check"
        'ref': fields.char('Reference'),
        'amount': fields.float('Amount', required=True),
    }

    def save_financial_payment(self, cr, uid, ids, context=None):
        jv_data={}
        jv_data['ref'] = context.get('ref')
        jv_data['journal_id'] = 919

        jv_data['period_id'] = context.get('period_id')

        jv_data['line_id'] = [[0, False, {'analytic_account_id': False, 'tax_code_id': False, 'tax_amount': 0, 'name': 'dfgdfg',
                                'currency_id': False, 'credit': context.get('amount'), 'date_maturity': False, 'debit': 0,
                                'amount_currency': 0, 'partner_id': 3324, 'account_id': context.get('payment_from')}], [0, False, {
            'analytic_account_id': False, 'tax_code_id': False, 'tax_amount': 0, 'name': 'dfgdfg', 'currency_id': False,
            'credit': 0, 'date_maturity': False, 'debit': context.get('amount'), 'amount_currency': 0, 'partner_id': False,
            'account_id': context.get('payment_to')}]]



        jv_entry = self.pool.get('account.move')
        saved_jv_id = jv_entry.create(cr, uid, jv_data, context=context)
        po_ids = context['active_ids']
        if saved_jv_id:

            for po_id in po_ids:
                po_payment_query = "UPDATE purchase_order SET payment_from='{0}', payment_to='{1}', check_no='{2}', adv_amount='{3}', journal_entry_id='{4}' WHERE id='{5}'".format(context.get('payment_from'),context.get('payment_to'),context.get('check_no'),context.get('amount'),saved_jv_id,po_id)
                cr.execute(po_payment_query)
                cr.commit()

        return True

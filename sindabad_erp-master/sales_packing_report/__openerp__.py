### Mufti Muntasir Ahmed
{
    'name': 'Packing Slip report Generation ',
    'author': 'Mufti Muntasir Ahmed',
    'website':'http://www.sinabad.com',
    'depends': ['sale'],
    'version': '8.0.0',
    'description': """
Print Packig Slip From Sale Order """,
    'active': False,
    'data': [
         'report/report_packingslip.xml',
     ],



    'installable': True
}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:

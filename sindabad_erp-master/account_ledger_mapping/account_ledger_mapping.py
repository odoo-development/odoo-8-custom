# Author Mufti Muntasir Ahmed




from datetime import datetime, timedelta, date
from dateutil import relativedelta
import json
import time

from openerp import SUPERUSER_ID, models, fields, api
from openerp.osv import fields, osv
from openerp.tools.translate import _
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT
import openerp.addons.decimal_precision as dp
from openerp import workflow

from openerp.tools.float_utils import float_compare, float_round
from openerp.exceptions import Warning
from openerp import SUPERUSER_ID, api
from openerp.addons.procurement import procurement
import logging



class acount_invoice(osv.osv):
    _inherit = "account.invoice"

    def accounting_mapping(self, cr, uid, ids, context=None):

        for invoice_id in context.get('active_ids'):

            inv_pool = self.pool.get('account.invoice')
            inv_object = inv_pool.browse(cr, uid, invoice_id, context=context)
            previous_account_id = inv_object.account_id.id

            partner_id=None
            account_id=None
            created_move_id = inv_object.move_id.id



            if inv_object.type == 'in_invoice' or inv_object.type == 'in_refund':
                if inv_object.partner_id.parent_id:
                    supplier_id = inv_object.partner_id.parent_id.id
                    account_id = inv_object.partner_id.parent_id.property_account_payable.id
                else:
                    supplier_id = inv_object.partner_id.id
                    account_id = inv_object.partner_id.property_account_payable.id

                if account_id:

                    cr.execute('update account_invoice set account_id=%s  where id=%s', (account_id, invoice_id))
                    cr.commit()

                    if created_move_id:
                        cr.execute('update account_move_line set account_id=%s  where move_id=%s and account_id=%s',
                                   (account_id, created_move_id, previous_account_id))
                        cr.commit()

                    for tes in inv_object.payment_ids:
                        new_move_id = tes.move_id.id
                        cr.execute(
                            'update account_move_line set account_id=%s  where move_id=%s and reconcile_ref IS NOT NULL',
                            (account_id, new_move_id))
                        cr.commit()

                        cr.execute('Select id from account_voucher where move_id=%s', ([new_move_id]))
                        voucher_list = cr.fetchall()
                        for voucher_id in voucher_list:
                            cr.execute('update account_voucher_line set account_id=%s  where voucher_id=%s',
                                       (account_id, voucher_id))
                            cr.commit()

            else:


                if inv_object.partner_id.wk_address == True:
                    customer_id = inv_object.partner_id.parent_id
                    partner_id=customer_id.parent_id.id if customer_id.parent_id.id else customer_id.id
                    account_id = customer_id.parent_id.property_account_receivable.id if customer_id.parent_id.id else customer_id.property_account_receivable.id

                elif inv_object.partner_id.supplier == True:
                    customer_id = inv_object.parent_id
                    partner_id = customer_id.parent_id.id if customer_id.parent_id.id else customer_id.id
                    # account_id = customer_id.parent_id.property_account_payable.id if customer_id.parent_id.id else customer_id.property_account_payable.id
                    account_id = False

                elif inv_object.partner_id.customer == True:
                    customer_id = inv_object.partner_id
                    partner_id = customer_id.parent_id.id if customer_id.parent_id.id else customer_id.id
                    account_id = customer_id.parent_id.property_account_receivable.id if customer_id.parent_id.id else customer_id.property_account_receivable.id


                elif inv_object.partner_id.is_company == True:
                    partner_id = inv_object.partner_id.id
                    account_id = inv_object.partner_id.property_account_receivable.id
                else:
                    customer_id = inv_object.partner_id.parent_id
                    partner_id = customer_id.parent_id.id if customer_id.parent_id.id else customer_id.id
                    account_id = customer_id.parent_id.property_account_receivable.id if customer_id.parent_id.id else customer_id.property_account_receivable.id

                if account_id:

                    cr.execute('update account_invoice set account_id=%s  where id=%s', (account_id, invoice_id))
                    cr.commit()

                    if created_move_id:
                        cr.execute('update account_move_line set account_id=%s  where move_id=%s and account_id=%s', (account_id,created_move_id, previous_account_id))
                        cr.commit()

                    for tes in inv_object.payment_ids:
                        new_move_id = tes.move_id.id
                        cr.execute('update account_move_line set account_id=%s  where move_id=%s and reconcile_ref IS NOT NULL',
                                   (account_id, new_move_id))
                        cr.commit()

                        cr.execute('Select id from account_voucher where move_id=%s', ([new_move_id]))
                        voucher_list =cr.fetchall()
                        for voucher_id in voucher_list:
                            cr.execute('update account_voucher_line set account_id=%s  where voucher_id=%s',
                                       (account_id, voucher_id))
                            cr.commit()



        return ''


class account_move_line(osv.osv):
    _inherit = "account.move.line"

    def accounting_mapping_correction(self, cr, uid, ids, context=None):

        for move_line_id in context.get('active_ids'):

            mv_line_pool = self.pool.get('account.move.line')
            mv_line_pool_object = mv_line_pool.browse(cr, uid, move_line_id, context=context)
            previous_account_id = mv_line_pool_object.account_id.id

            partner_id = mv_line_pool_object.partner_id.id

            current_account_id = mv_line_pool_object.partner_id.property_account_receivable.id

            if current_account_id is not False:

                if previous_account_id == 7074:
                    if current_account_id != 7074:

                        cr.execute('update account_move_line set account_id=%s  where id=%s', (current_account_id, move_line_id))
                        cr.commit()
                elif previous_account_id == 8278:
                    current_account_id = mv_line_pool_object.partner_id.property_account_payable.id
                    cr.execute('update account_move_line set account_id=%s  where id=%s',
                               (current_account_id, move_line_id))
                    cr.commit()




        return ''


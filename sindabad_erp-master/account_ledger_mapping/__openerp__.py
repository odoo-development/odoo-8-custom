# Author Mufti Muntasir Ahmed


{
    'name': 'Accounting Mapping With Partner',
    'version': '1.0',
    'category': 'Accounting Mapping With Partner',
    'author': 'Mufti Muntasir Ahmed',
    'summary': 'Account',
    'description': 'Accounting Mapping With Partner',
    'depends': ['account'],
    'data': [
        'account_ledger_mapping_view.xml',
        'account_ledger_moveline_mapping_view.xml'
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}

{
    'name': 'Stock/Inventory Lock',
    'version': '1.0',
    'category': 'Stock',
    'author': 'Mufti Muntasir Ahmed',
    'summary': 'Each Month till 5th can modify only last inventory quantity adjustment',
    'description': 'Each Month till 5th can modify only last inventory quantity adjustment',
    'depends': ['stock'],
    'data': [
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}




from openerp.osv import fields, osv

from openerp.tools.translate import _

from openerp import SUPERUSER_ID, api
import openerp
import dateutil.relativedelta




class stock_inventory(osv.osv):
    _inherit = "stock.inventory"

    def get_last_month_period(self):
        return True

    def build_ctx_periods_2(self, cr, uid, period_from_id, period_to_id):
        if period_from_id == period_to_id:
            return [period_from_id]
        period_from = self.browse(cr, uid, period_from_id)
        period_date_start = period_from.date_start
        company1_id = period_from.company_id.id
        period_to = self.browse(cr, uid, period_to_id)
        period_date_stop = period_to.date_stop
        company2_id = period_to.company_id.id
        if company1_id != company2_id:
            raise osv.except_osv(_('Error!'), _('You should choose the periods that belong to the same company.'))
        if period_date_start > period_date_stop:
            raise osv.except_osv(_('Error!'), _('Start period should precede then end period.'))

        # /!\ We do not include a criterion on the company_id field below, to allow producing consolidated reports
        # on multiple companies. It will only work when start/end periods are selected and no fiscal year is chosen.

        # for period from = january, we want to exclude the opening period (but it has same date_from, so we have to check if period_from is special or not to include that clause or not in the search).
        if period_from.special:
            return self.search(cr, uid,
                               [('date_start', '>=', period_date_start), ('date_stop', '<=', period_date_stop)])
        return self.search(cr, uid, [('date_start', '>=', period_date_start), ('date_stop', '<=', period_date_stop),
                                     ('special', '=', False)])

    @api.returns('self')
    def find_section(self, cr, uid, dt=None, context=None):
        if context is None: context = {}
        if not dt:
            dt = fields.date.context_today(self, cr, uid, context=context)
        args = [('date_start', '<=' ,dt), ('date_stop', '>=', dt)]
        if context.get('company_id', False):
            args.append(('company_id', '=', context['company_id']))
        else:
            company_id = self.pool.get('res.users').browse(cr, uid, uid, context=context).company_id.id
            args.append(('company_id', '=', company_id))
        result = []
        if context.get('account_period_prefer_normal', True):
            # look for non-special periods first, and fallback to all if no result is found
            result = self.search(cr, uid, args + [('special', '=', False)], context=context)
        if not result:
            result = self.search(cr, uid, args, context=context)
        if not result:
            model, action_id = self.pool['ir.model.data'].get_object_reference(cr, uid, 'account', 'action_account_period')
            msg = _('There is no period defined for this date: %s.\nPlease go to Configuration/Periods.') % dt
            raise openerp.exceptions.RedirectWarning(msg, action_id, _('Go to the configuration panel'))
        return result

    def create(self, cr, uid, vals, context=None):
        period_selected_id = vals.get('period_id')
        periods = self.pool.get('account.period').find(cr, uid, context=context)
        current_period_id =periods[0] if len(periods)>0 else False

        today_date = fields.date.today()
        import datetime
        day_name = datetime.datetime.today().day
        previous_period_id=False

        allwoed_periods_list = []

        # day_name=6
        if 0 < day_name < 6:
            today_date = datetime.datetime.today()
            previous_month_date = today_date - dateutil.relativedelta.relativedelta(months=1)

            previous_periods = self.pool.get('account.period').find(cr, uid,dt=previous_month_date, context=context)
            previous_period_id = previous_periods[0] if len(previous_periods) > 0 else False


        if period_selected_id:
            if previous_period_id is not False:
                allwoed_periods_list.append(previous_period_id)
            if current_period_id is not False:
                allwoed_periods_list.append(current_period_id)
                if period_selected_id not in allwoed_periods_list:
                    raise osv.except_osv(_('Warning'), _('Your selected Period should be current Month'))
        else:
            raise osv.except_osv(_('Warning'), _('Please select Force Valuation Period'))


        res = super(stock_inventory, self).create(cr, uid, vals, context=context)

        return res
from openerp import models, fields


class MenuList(models.Model):
    _name = 'wms.menulist'

    name = fields.Char(string="Name", required=True)
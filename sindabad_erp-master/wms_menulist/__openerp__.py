{
    'name': "WMS Menu List Module",
    'version': '1.0',
    'category': 'Information Technology',
    'author': "Md Rockibul Alam Babu",
    'description': """
WMS menu list for warehouse people.
    """,
    'website': "http://www.sindabad.com",
    'data': [
        'security/wms_security.xml',
        'security/ir.model.access.csv',
        'views/wms_view.xml',
        'views/wms_inbound_view.xml',
        'views/reporting_view.xml',
        'menulist_view.xml'
    ],
    'depends': [
        'sale', 'stock', 'account', 'purchase', 'pickings', 'packings', 'wms_inbound', 'product_ageing_report'
    ],
    'installable': True,
    'auto_install': False,
}

# -*- coding: utf-8 -*-
{
    'name': "Order Sync",

    'summary': """
		Using this module we can sync our order status to module""",

    'description': """
		Using this module we can sync our order status to module
	""",

    'author': "S. M. Sazedul Haque - Zero Gravity Ventures Ltd",
    'website': "http://zerogravity.com.bd/",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/openerp/addons/base/module/module_data.xml
    # for the full list
    'category': 'custom',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': [
            'base',
            'base_vat',
            'sale',
            'stock',
            'account_accountant',
            'delivery',
            'wk_base_partner_patch',
    ],

    # always loaded
    'data': [
    ],
    'application': True,
    'installable': True,
    'auto_install': False,
}

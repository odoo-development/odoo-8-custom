# -*- coding: utf-8 -*-
##########################################################################
#
#    Copyright (c) 2015-Present Webkul Software Pvt. Ltd. (<https://webkul.com/>)
#
##########################################################################


import xmlrpclib
from openerp.osv import fields, osv
from openerp.tools.translate import _
from openerp import SUPERUSER_ID, api
import openerp.addons.decimal_precision as dp

XMLRPC_API = '/index.php/api/xmlrpc'


class sales_order_custom(osv.osv):
    _inherit = "sale.order"

    def magento_order_status_change(self, cr, uid, order_name, increment_id, status, instance_id,  context=None):
        text = ''
        session = 0
        obj = self.pool.get('magento.configure').browse(cr, uid, instance_id)
        if obj.state != 'enable':
            text = 'Please create the configuration part for connection!!!'
        else:
            url = obj.name + XMLRPC_API
            user = obj.user
            pwd = obj.pwd
            try:
                server = xmlrpclib.Server(url)
                session = server.login(user, pwd)
            except xmlrpclib.Fault, e:
                text = 'Error, %s Magento details are Invalid.' % e
            except IOError, e:
                text = 'Error, %s.' % e
            except Exception, e:
                text = 'Error in Magento Connection.'
            if session:
                # self.pool.get('bridge.backbone').release_mage_order_from_hold(
                #     cr, uid, increment_id,  url, session)
                try:
                    tx = server.call(session, 'sales_order.addComment', [increment_id, status])
                    # tx = server.call(session, 'sales_order.addComment', increment_id, status)

                    text = 'sales order %s has been sucessfully confirmed from magento.' % order_name

                    cr.commit()
                    self.pool.get('magento.sync.history').create(cr, uid, {
                        'status': 'yes', 'action_on': 'order', 'action': 'b', 'error_message': text})
                    return True
                except Exception, e:
                    text = 'Order %s cannot be confirmed from magento, Because Magento order %s is in different state.' % (
                        order_name, increment_id)
                    self.pool.get('magento.sync.history').create(cr, uid, {
                        'status': 'no', 'action_on': 'order', 'action': 'b', 'error_message': text})
                    return False

    def action_button_confirm(self, cr, uid, ids, context=None):
        # import pdb
        # pdb.set_trace()
        super(sales_order_custom, self).action_button_confirm(cr, uid, ids, context)

        # magento_order_status_change method is used to confirm an order
        for order_id in ids:
            order_ids = self.pool.get('magento.orders').search(
                cr, uid, [('order_ref', '=', order_id)])
            if order_ids:
                map_obj = self.pool.get('magento.orders').browse(
                    cr, uid, order_ids[0])
                order_name = map_obj.order_ref.name
                instance_id = map_obj.instance_id.id
                increment_id = map_obj.mage_increment_id
                # status = 'pending'
                status = 'processing'
                self.magento_order_status_change(
                    cr, uid, order_name, increment_id, status, instance_id, context)
                return True
        return True

    # def magento_order_update(self, cr, uid, ids, vals, context=None):
    #     text = ''
    #     session = 0
    #     config_id = self.pool.get('magento.configure').search(cr, uid, [('active', '=', True)])
    #     if len(config_id) > 1:
    #         raise osv.except_osv(_('Error'), _(
    #             "Sorry, only one Active Configuration setting is allowed."))
    #     if not config_id:
    #         raise osv.except_osv(_('Error'), _(
    #             "Please create the configuration part for connection!!!"))
    #     else:
    #         obj = self.pool.get('magento.configure').browse(cr, uid, config_id[0])
    #         if obj.state != 'enable':
    #             text = 'Please create the configuration part for connection!!!'
    #         else:
    #             url = obj.name + XMLRPC_API
    #             user = obj.user
    #             pwd = obj.pwd
    #             try:
    #                 server = xmlrpclib.Server(url)
    #                 session = server.login(user, pwd)
    #             except xmlrpclib.Fault, e:
    #                 text = 'Error, %s Magento details are Invalid.' % e
    #             except IOError, e:
    #                 text = 'Error, %s.' % e
    #             except Exception, e:
    #                 text = 'Error in Magento Connection.'
    #             if session:
    #                 try:
    #                     tx = server.call(session, 'magerpsync.order_update_sync', (True, ))
    #
    #                     # cr.commit()
    #                     # self.pool.get('magento.sync.history').create(cr, uid, {
    #                     # 'status': 'yes', 'action_on': 'customer', 'action': 'b', 'error_message': text})
    #                     return tx
    #                 except Exception, e:
    #                     # text = 'Order %s cannot be confirmed from magento, Because Magento order %s is in different state.' % ( order_name, increment_id )
    #                     # self.pool.get('magento.sync.history').create(cr, uid, {
    #                     # 'status': 'no', 'action_on': 'customer', 'action': 'b', 'error_message': text})
    #                     return {'msg': e}
    #
    # def write(self, cr, uid, ids, vals, context=None):
    #     if context is None:
    #         context = {}
    #     self.magento_order_update(
    #         cr, uid, ids, vals, context=context)
    #     new_id = super(sales_order_custom, self).write(cr, uid, ids, vals, context=context)
    #     return new_id

    def write(self, cr, uid, ids, vals, context=None):

        if context is None:
            context = {}
        # if vals.has_key('state') and vals.get('state') == 'done':
        #     for order_id in ids:
        #         order_ids = self.pool.get('magento.orders').search(
        #             cr, uid, [('order_ref', '=', order_id)])
        #         if order_ids:
        #             map_obj = self.pool.get('magento.orders').browse(
        #                 cr, uid, order_ids[0])
        #             order_name = map_obj.order_ref.name
        #             instance_id = map_obj.instance_id.id
        #             increment_id = map_obj.mage_increment_id
        #             status = 'complete'
        #             self.magento_order_status_change(
        #                 cr, uid, order_name, increment_id, status, instance_id, context)
        if vals.has_key('magetno_delivery_at'):
            vals['magetno_confirm_date'] = datetime.now()
        new_id = super(sales_order_custom, self).write(cr, uid, ids, vals, context=context)
        return new_id


sales_order_custom()


def common_status_change(self, cr, uid, order_name, increment_id, status, instance_id,  context=None):
    text = ''
    session = 0
    obj = self.pool.get('magento.configure').browse(cr, uid, instance_id)
    if obj.state != 'enable':
        text = 'Please create the configuration part for connection!!!'
    else:
        url = obj.name + XMLRPC_API
        user = obj.user
        pwd = obj.pwd
        try:
            server = xmlrpclib.Server(url)
            session = server.login(user, pwd)
        except xmlrpclib.Fault, e:
            text = 'Error, %s Magento details are Invalid.' % e
        except IOError, e:
            text = 'Error, %s.' % e
        except Exception, e:
            text = 'Error in Magento Connection.'
        if session:
            # self.pool.get('bridge.backbone').release_mage_order_from_hold(
            #     cr, uid, increment_id,  url, session)
            try:
                tx = server.call(session, 'sales_order.addComment', [increment_id, status])
                # tx = server.call(session, 'sales_order.addComment', increment_id, status)

                text = 'sales order %s has been sucessfully confirmed from magento.' % order_name

                cr.commit()
                self.pool.get('magento.sync.history').create(cr, uid, {
                    'status': 'yes', 'action_on': 'order', 'action': 'b', 'error_message': text})
                return True
            except Exception, e:
                text = 'Order %s cannot be confirmed from magento, Because Magento order %s is in different state.' % (
                    order_name, increment_id)
                self.pool.get('magento.sync.history').create(cr, uid, {
                    'status': 'no', 'action_on': 'order', 'action': 'b', 'error_message': text})
                return False

from datetime import datetime, timedelta
import time
from datetime import date
from openerp import SUPERUSER_ID
from openerp.osv import fields, osv
from openerp.tools.translate import _
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT
import openerp.addons.decimal_precision as dp
from openerp import workflow
from openerp import api
import datetime


class pending_quality_control(osv.osv):
    _name = "pending.quality.control"
    _description = "Pending Quality Control"
    _order = 'id desc'

    _columns = {
        'scan': fields.char('Scan'),
        'product_scan': fields.char('Product Scan'),
        'warehouse_id': fields.many2one('stock.warehouse', string='Warehouse Location', required=True),
        'pqc_number': fields.char('PQC Number'),
        'qc_number': fields.char('QC Number'),
        'qc_id': fields.char('QC ID'),
        'irn_number': fields.char('IRN Number'),
        'irn_id': fields.integer('IRN ID'),
        'po_number': fields.char('PO Number'),
        'po_id': fields.integer('Purchase ID'),
        'remark': fields.text('Remark'),
        'received_by': fields.many2one('res.users', 'Received By'),
        'pqc_processed': fields.boolean('PQC Processed'),
        'date_confirm': fields.datetime('Confirmation Date', readonly=True, select=False),
        'pqc_reject_reason': fields.char('Rejection Reason'),
        'pqc_reject_by': fields.many2one('res.users', 'PQC Rejection By'),
        'pqc_reject_date': fields.datetime('PQC Rejection Date'),
        'pqc_accept_by': fields.many2one('res.users', 'PQC Accept By'),
        'pqc_accept_date': fields.datetime('PQC Accept Date'),
        'state': fields.selection([
            ('return_to_vendor', 'Return to Vendor'),
            ('accept', 'Accept'),
            ('reject', 'Reject'),
            ('partially_reject', 'Partially Reject'),
            ('scrap', 'Scrap'),
            ('pending', 'Pending'),

        ], 'Status', readonly=True, copy=False, help="Gives the status of the WMS inbound", select=True),
        'pqc_line': fields.one2many('pending.quality.control.line', 'pqc_id', 'PQC Line', required=True),
    }

    _defaults = {
        'user_id': lambda obj, cr, uid, context: uid,
        'received_by': lambda obj, cr, uid, context: uid,
        'state': 'pending',
    }

    # calls at the time of update record
    @api.multi
    def write(self, vals):

        # only update only if (pending_quantity = pqc_accept_quantity + pqc_reject_quantity)
        pqc_env = self.pool.get('pending.quality.control')
        pqc_obj = pqc_env.browse(self._cr, self._uid, [self.id], context=self._context)
        update_permission = list()

        if vals.has_key('pqc_line'):
            for single_line in vals['pqc_line']:
                if not not single_line[2]:
                    for pqc_l in pqc_obj.pqc_line:

                        if pqc_l.id == single_line[1]:

                            pqc_accept_quantity = single_line[2]['pqc_accept_quantity'] if single_line[2].has_key('pqc_accept_quantity') else pqc_l.pqc_accept_quantity
                            pqc_reject_quantity = single_line[2]['pqc_reject_quantity'] if single_line[2].has_key('pqc_reject_quantity') else pqc_l.pqc_reject_quantity

                            if pqc_l.pending_quantity == pqc_accept_quantity + pqc_reject_quantity:
                                update_permission.append(True)
                            else:
                                update_permission.append(False)

        if False in update_permission:

            raise osv.except_osv(_('PQC line adjustment ERROR!'), _('Pending quantity should be equal to (accepted quantity + rejected quantity)!!!'))

        else:

            record = super(pending_quality_control, self).write(vals)

            return record

    @api.model
    def create(self, vals):
        # generate PQC number
        # PQC0123456

        record = super(pending_quality_control, self).create(vals)

        pqc_number = "PQC0" + str(record.id)
        record.pqc_number = pqc_number

        self.pool.get('inb.end.to.end').pqc_data_create(self._cr, self._uid, record.po_number, record.po_id, record.id, pqc_number, record.create_date, context=self._context)

        return record

    @api.onchange('product_scan')
    def pqc_product_scan(self):

        try:
            po_number = str(self.po_number)
            product_scan = str(self.product_scan)
            product_not_in_po_order = True


            if product_scan != 'False':
                qty = 1
                if 'Prod' not in product_scan:

                    product_env = self.env['product.product']
                    product_obj = product_env.search(
                        ['|', '|', '|', '|', ('ean13', '=', str(product_scan)), ('gift_ean', '=', str(product_scan)),
                         ('box_ean', '=', str(product_scan)), ('case_ean', '=', str(product_scan)),
                         ('pallet_ean', '=', str(product_scan))])

                    if not product_obj.id:
                        raise Warning(_(
                            'Product Not found.Please add EAN number in Product Configuration. Or contact with Your department head'))

                    if str(product_obj.gift_ean) == str(product_scan):
                        qty = 1
                    elif str(product_obj.box_ean) == str(product_scan):
                        qty = product_obj.holding_box_qty
                    elif str(product_obj.case_ean) == str(product_scan):
                        qty = product_obj.holding_case_qty
                    elif str(product_obj.pallet_ean) == str(product_scan):
                        qty = product_obj.holding_pallet_qty
                else:
                    get_list = product_scan.split('Prod')
                    product_id = int(get_list[1])

                    product_env = self.env['product.product']
                    product_obj = product_env.search([('id', '=', product_id)])
                    if not product_obj.id:
                        raise Warning(_('Product Not found.Please scan properly with proper product'))

                po_env = self.env['purchase.order']
                po_obj = po_env.search([('name', '=', po_number)])

                po_line_env = self.env['purchase.order.line']
                po_line_obj = po_line_env.search([('order_id', '=', po_obj.id)])

                # po_line_obj[0].product_id.ean13
                # self.irn_line[0].product_ean
                for single_line in self.pqc_line:

                    # if str(single_line.product_id.id) == str(product_obj.id) and single_line.pending_quantity >= (single_line.pqc_accept_quantity + qty):
                    if str(single_line.product_id.id) == str(product_obj.id) and single_line.pqc_accept_quantity < single_line.pending_quantity:
                        # import pdb;pdb.set_trace()
                        single_line.pqc_accept_quantity += qty
                        single_line.pqc_reject_quantity -= qty
                        product_not_in_po_order = False
                        break
                # self.irn_line[0].received_quantity

                if product_not_in_po_order:
                    raise Warning(_('Product Not found in this Order.Wrong product has been scanned'))

                # import pdb;pdb.set_trace()
                self.product_scan = ""

        except:

            self.product_scan = ""

        return "xXxXxXxXxX"


    def wms_inbound_pqc_total_receive(self, cr, uid, ids, context=None):

        pqc_env = self.pool.get('pending.quality.control')
        pqc_obj = pqc_env.browse(cr, uid, ids, context=context)

        put_data = aqc_data = {
            'pqc_number': str(pqc_obj.pqc_number),
            'pqc_id': pqc_obj.id,
            'qc_number': str(pqc_obj.qc_number),
            'qc_id': str(pqc_obj.qc_id),
            'warehouse_id': pqc_obj.warehouse_id.id,
            'irn_number': str(pqc_obj.irn_number),
            'irn_id': pqc_obj.irn_id,
            'po_number': str(pqc_obj.po_number),
            'po_id': pqc_obj.po_id,
            'remark': str(pqc_obj.remark),
            'received_by': pqc_obj.received_by.id,
        }
        aqc_line_list = list()

        for aqc_line_item in pqc_obj.pqc_line:

            if aqc_line_item.pending_quantity > 0:
                aqc_line_list.append([0, False,
                                     {
                                         'product': aqc_line_item.product,
                                         'product_id': aqc_line_item.product_id.id,
                                         'description': aqc_line_item.description,
                                         'purchase_quantity': aqc_line_item.purchase_quantity,
                                         'received_quantity': aqc_line_item.received_quantity,
                                         'accepted_quantity': aqc_line_item.pending_quantity,
                                         'pending_quantity': 0,
                                     }])

        aqc_data['aqc_line'] = aqc_line_list

        if len(aqc_line_list) > 0:
            # Create aqc
            aqc_env = self.pool.get('accepted.quality.control')
            saved_aqc_id = aqc_env.create(cr, uid, aqc_data, context=context)

            # update qc pqc_processed and status to true
            pqc_state_update_query = "UPDATE pending_quality_control SET pqc_processed= TRUE, state='accept' WHERE id={0}".format(pqc_obj.id)
            cr.execute(pqc_state_update_query)
            cr.commit()

            pqc_processed_pqc_line_update_query = "UPDATE pending_quality_control_line SET pqc_processed=TRUE WHERE pqc_id={0}".format(pqc_obj.id)
            cr.execute(pqc_processed_pqc_line_update_query)
            cr.commit()

            # Confirm the aqc

            aqc_confirm_query = "UPDATE accepted_quality_control SET state='confirm', date_confirm='{0}' WHERE id={1}".format(
                str(fields.datetime.now()), saved_aqc_id)
            cr.execute(aqc_confirm_query)
            cr.commit()

            aqc_confirm_query_line = "UPDATE accepted_quality_control_line SET state='confirm', date_confirm='{0}' WHERE aqc_id={1}".format(
                str(fields.datetime.now()), saved_aqc_id)
            cr.execute(aqc_confirm_query_line)
            cr.commit()

            # Generate Put away Process
            put_line_list = []

            for aqc_line in aqc_line_list:
                put_line_list.append([0, False, {
                    'product_id': aqc_line[2]['product_id'],
                    'qc_id': saved_aqc_id,
                    'location': '',
                    'product': aqc_line[2]['product'],
                    'remarks': '',
                    'quantity': aqc_line[2]['accepted_quantity'],
                    'po_number': str(pqc_obj.po_number)
                }])

            """
            for pqc_line_item in pqc_obj.pqc_line:
                if pqc_line_item.accepted_quantity > 0:
                    put_line_list.append([0, False,
                                          {
                                              'product_id': pqc_line_item.product_id,
                                              'qc_id': saved_aqc_id,
                                              'location': '',
                                              'product': pqc_line_item.product,
                                              'remarks': '',
                                              'quantity': pqc_line_item.accepted_quantity,
                                              'po_number': str(pqc_obj.po_number)

                                          }])
            """

            put_data['aqc_id'] = saved_aqc_id
            put_data['putaway_line_view'] = put_line_list
            put_env = self.pool.get('putaway.process')
            saved_put_id = put_env.create(cr, uid, put_data, context=context)

        return True

    # not need. need to delete.
    def wms_inbound_pqc_reject(self, cr, uid, ids, context=None):

        # need to check line items if state is set, before process
        # already set item will not process

        for pqc_id in ids:
            # update qc pqc_processed and status to true
            pqc_state_update_query = "UPDATE pending_quality_control SET pqc_processed= TRUE, state='reject' WHERE id={0}".format(pqc_id)
            cr.execute(pqc_state_update_query)
            cr.commit()

            pqc_processed_pqc_line_update_query = "UPDATE pending_quality_control_line SET pqc_processed=TRUE, state='reject' WHERE pqc_id={0}".format(pqc_id)
            cr.execute(pqc_processed_pqc_line_update_query)
            cr.commit()

        return True

    def wms_inbound_pqc_return_to_vendor(self, cr, uid, ids, context=None):

        for pqc_id in ids:
            # update qc pqc_processed and status to true
            pqc_state_update_query = "UPDATE pending_quality_control SET pqc_processed= TRUE, state='return_to_vendor' WHERE id={0}".format(pqc_id)
            cr.execute(pqc_state_update_query)
            cr.commit()

            pqc_processed_pqc_line_update_query = "UPDATE pending_quality_control_line SET pqc_processed=TRUE, state='return_to_vendor' WHERE pqc_id={0}".format(pqc_id)
            cr.execute(pqc_processed_pqc_line_update_query)
            cr.commit()

        return True

    def wms_inbound_pqc_scrap(self, cr, uid, ids, context=None):

        for pqc_id in ids:
            # update qc pqc_processed and status to true
            pqc_state_update_query = "UPDATE pending_quality_control SET pqc_processed= TRUE, state='scrap' WHERE id={0}".format(pqc_id)
            cr.execute(pqc_state_update_query)
            cr.commit()

            pqc_processed_pqc_line_update_query = "UPDATE pending_quality_control_line SET pqc_processed=TRUE, state='scrap' WHERE pqc_id={0}".format(pqc_id)
            cr.execute(pqc_processed_pqc_line_update_query)
            cr.commit()

        return True


class pending_quality_control_line(osv.osv):
    _name = "pending.quality.control.line"
    _description = "Pending Quality Control Line"

    _columns = {
        'pqc_id': fields.many2one('pending.quality.control', 'PQC ID', required=True,
                                      ondelete='cascade', select=True, readonly=True),
        'qc_id': fields.many2one('quality.control', 'QC ID', required=False,
                                 ondelete='cascade', select=True, readonly=True),
        'product': fields.char('Product'),
        # 'product_id': fields.integer('Product ID'),
        'product_id': fields.many2one('product.product', 'Product', required=True),
        'product_ean': fields.char('Product EAN'),
        'description': fields.char('Description'),
        'purchase_quantity': fields.float('Purchase Qty'),
        'received_quantity': fields.float('Received Qty'),
        'accepted_quantity': fields.float('Accepted Qty'),
        'pending_quantity': fields.float('Pending Qty'),
        'pqc_accept_quantity': fields.float('Accept Qty'),
        'pqc_reject_quantity': fields.float('Reject Qty'),
        'pqc_processed': fields.boolean('PQC Processed'),
        'remark': fields.char('Remark'),
        'pqc_reject_reason': fields.char('Rejection Reason'),
        'pqc_reject_by': fields.many2one('res.users', 'PQC Rejection By'),
        'pqc_reject_date': fields.datetime('PQC Rejection Date'),
        'state': fields.selection([
            ('return_to_vendor', 'Return to Vendor'),
            ('accept', 'Accept'),
            ('reject', 'Reject'),
            ('partially_reject', 'Partially Reject'),
            ('scrap', 'Scrap'),
            ('pending', 'Pending'),
        ], 'Status', help="Gives the status of the WMS inbound line", select=True),
    }

    def pqc_processed_status(self, self_obj, cr, uid, pqc_id, context):

        pqc_processed_status_list = list()
        pqc_line_status_list = list()
        status = 'partially_reject'
        pqc_line_id_list = self_obj.search(cr, uid, [('pqc_id', '=', pqc_id)])

        pqc_line_obj = self_obj.browse(cr, uid, pqc_line_id_list, context=context)
        if len(pqc_line_id_list) == 1:

            if pqc_line_obj.pqc_accept_quantity == 0:
                status = 'reject'
            self.update_pqc_status(cr, uid, pqc_line_obj.pqc_id.id, status, context=context)
        else:
            for pqc_l in pqc_line_obj:
                pqc_line_status_list.append(pqc_l.pqc_processed)

            if pqc_line_status_list.count(False) == 1:
                self.update_pqc_status(cr, uid, pqc_line_obj[0].pqc_id.id, status, context=context)

        return True

    def update_pqc_status(self, cr, uid, pqc_id, status, context):
        pqc_process_status_update_query = "UPDATE pending_quality_control SET pqc_processed=TRUE, state='{0}' WHERE id='{1}'".format(
            status, pqc_id)
        cr.execute(pqc_process_status_update_query)
        cr.commit()

        return True

    def wms_inbound_pqc_line_process(self, cr, uid, ids, context=None):

        pqc_line_obj = self.browse(cr, uid, ids, context=context)
        pqc_obj = self.pool.get('pending.quality.control')
        pqc_data = pqc_obj.browse(cr, uid, [pqc_line_obj.pqc_id.id], context=context)

        pending_quantity = pqc_line_obj.pending_quantity
        pqc_reject_quantity = context['pqc_reject_quantity'] if not context['pqc_reject_quantity'] == 'pqc_reject_quantity' else pqc_line_obj.pqc_reject_quantity
        pqc_accept_quantity = context['pqc_accept_quantity'] if not context['pqc_accept_quantity'] == 'pqc_accept_quantity' else pqc_line_obj.pqc_accept_quantity
        pqc_reject_reason = context['pqc_reject_reason'] if not context['pqc_reject_reason'] == 'pqc_reject_reason' else pqc_line_obj.pqc_reject_reason

        if pqc_reject_reason == False:
            raise osv.except_osv(_('QC line adjustment ERROR!'),
                                 _('Must need to give reject reason!!!'))
        elif pending_quantity != pqc_reject_quantity + pqc_accept_quantity:
            raise osv.except_osv(_('QC line adjustment ERROR!'),
                                 _('Received quantity should be equal to (accepted quantity + pending quantity)!!!'))
        elif pending_quantity == pqc_reject_quantity + pqc_accept_quantity:

            # update pqc reject reason
            for pqc in pqc_data:
                state = "reject"

                # create AQC from pqc_line accept product
                put_data = aqc_data = {
                    'pqc_number': str(pqc.pqc_number),
                    'pqc_id': pqc.id,
                    'qc_number': str(pqc.qc_number),
                    'qc_id': str(pqc.qc_id),
                    'warehouse_id': pqc.warehouse_id.id,
                    'irn_number': str(pqc.irn_number),
                    'irn_id': pqc.irn_id,
                    'po_number': str(pqc.po_number),
                    'po_id': pqc.po_id,
                    'remark': str(pqc.remark),
                    'received_by': uid,
                }
                aqc_line_list = list()

                # update pqc line reject reason
                # update only if pqc_processed is not TRUE
                # irn_obj = self.pool.get('irn.control')
                if not pqc_line_obj.pqc_processed:

                    new_product_received_qty = pqc_line_obj.received_quantity - pqc_reject_quantity

                    # check partially_reject or reject
                    if pqc_accept_quantity > 0:
                        state = "partially_reject"

                        aqc_line_list.append([0, False,
                              {
                                  'qc_id': pqc_line_obj.qc_id.id,
                                  'product': pqc_line_obj.product,
                                  'product_id': pqc_line_obj.product_id.id,
                                  'description': pqc_line_obj.description,
                                  'purchase_quantity': pqc_line_obj.purchase_quantity,
                                  'received_quantity': new_product_received_qty,
                                  'accepted_quantity': pqc_accept_quantity,
                                  'pending_quantity': pending_quantity,
                              }])

                    else:
                        state = "reject"

                    # pending_quality_control_line
                    pqc_line_reject_query = "UPDATE pending_quality_control_line SET pqc_reject_reason='{0}', pqc_reject_by='{1}', pqc_reject_date='{2}', state='{3}', pqc_processed=TRUE, received_quantity='{4}' WHERE id={5}".format(pqc_reject_reason, uid, str(fields.datetime.now()), state, new_product_received_qty, pqc_line_obj.id)
                    cr.execute(pqc_line_reject_query)
                    cr.commit()

                    # update irn_control
                    irn_update_query = "UPDATE irn_control SET state='partial_received' WHERE id='{0}'".format(
                        pqc.irn_id)
                    cr.execute(irn_update_query)
                    cr.commit()

                    # update irn_control_line
                    irn_line_update_query = "UPDATE irn_control_line SET state='partially_received', received_quantity='{0}' WHERE irn_line_id='{1}' AND product_id='{2}'".format(
                        new_product_received_qty, pqc.irn_id, pqc_line_obj.product_id.id)
                    cr.execute(irn_line_update_query)
                    cr.commit()

                    # update quality_control_line
                    qc_line_update_query = "UPDATE quality_control_line SET received_quantity='{0}' WHERE qc_line_id='{1}' AND product_id='{2}'".format(
                        new_product_received_qty, int(pqc.qc_id), pqc_line_obj.product_id.id)
                    cr.execute(qc_line_update_query)
                    cr.commit()

                    # update accepted_quality_control_line
                    aqc_line_update_query = "UPDATE accepted_quality_control_line SET received_quantity='{0}' WHERE qc_id='{1}' AND product_id='{2}'".format(new_product_received_qty, int(pqc.qc_id), pqc_line_obj.product_id.id)
                    cr.execute(aqc_line_update_query)
                    cr.commit()

                    # update pending_quality_control_line
                    pqc_line_update_query = "UPDATE pending_quality_control_line SET received_quantity='{0}' WHERE qc_id='{1}' AND product_id='{2}'".format(
                        new_product_received_qty, int(pqc.qc_id), pqc_line_obj.product_id.id)
                    cr.execute(pqc_line_update_query)
                    cr.commit()

                    aqc_data['aqc_line'] = aqc_line_list
                    if len(aqc_line_list) > 0:
                        # Create aqc
                        aqc_env = self.pool.get('accepted.quality.control')
                        saved_aqc_id = aqc_env.create(cr, uid, aqc_data, context=context)

                        # Confirm the aqc
                        aqc_confirm_query = "UPDATE accepted_quality_control SET state='confirm', date_confirm='{0}' WHERE id={1}".format(
                            str(fields.datetime.now()), saved_aqc_id)
                        cr.execute(aqc_confirm_query)
                        cr.commit()

                        aqc_confirm_query_line = "UPDATE accepted_quality_control_line SET state='confirm', date_confirm='{0}' WHERE aqc_id={1}".format(
                            str(fields.datetime.now()), saved_aqc_id)
                        cr.execute(aqc_confirm_query_line)
                        cr.commit()

                        # Generate Put away Process
                        put_line_list = []

                        for aqc_line in aqc_line_list:
                            put_line_list.append([0, False, {
                                'product_id': aqc_line[2]['product_id'],
                                'qc_id': saved_aqc_id,
                                'location': '',
                                'product': aqc_line[2]['product'],
                                'remarks': '',
                                'quantity': pqc_accept_quantity,
                                'po_number': str(pqc.po_number)
                            }])

                        put_data['aqc_id'] = saved_aqc_id
                        put_data['putaway_line_view'] = put_line_list
                        put_env = self.pool.get('putaway.process')
                        saved_put_id = put_env.create(cr, uid, put_data, context=context)

                    self.pqc_processed_status(self, cr, uid, pqc.id, context)

        # return True
        return {
            'type': 'ir.actions.client',
            'tag': 'reload',
        }


class pending_quality_control_reject(osv.osv):
    _name = "pending.quality.control.reject"
    _description = "Pending quality control reject"

    _columns = {

        'pqc_reject_reason': fields.char('Rejection Reason', required=True),
        'pqc_reject_by': fields.many2one('res.users', 'PQC Rejection By'),
        'pqc_reject_date': fields.datetime('PQC Rejection Date'),
    }

    _defaults = {
        'pqc_reject_by': lambda obj, cr, uid, context: uid,
        'pqc_reject_date': fields.datetime.now,
    }

    def pqc_reject(self, cr, uid, ids, context=None):

        pqc_reject_reason = str(context['pqc_reject_reason'])
        pqc_reject_by = uid
        reject_ids = context['active_ids']

        pqc_obj = self.pool.get('pending.quality.control')
        pqc_data = pqc_obj.browse(cr, uid, reject_ids, context=context)

        # update pqc reject reason
        for pqc in pqc_data:
            state = "reject"

            # create AQC from pqc_line accept product
            put_data = aqc_data = {
                'pqc_number': str(pqc.pqc_number),
                'pqc_id': pqc.id,
                'qc_number': str(pqc.qc_number),
                'qc_id': str(pqc.qc_id),
                'warehouse_id': pqc.warehouse_id.id,
                'irn_number': str(pqc.irn_number),
                'irn_id': pqc.irn_id,
                'po_number': str(pqc.po_number),
                'po_id': pqc.po_id,
                'remark': str(pqc.remark),
                'received_by': uid,
            }
            aqc_line_list = list()

            # update pqc line reject reason
            # update only if pqc_processed is not TRUE
            # irn_obj = self.pool.get('irn.control')
            for pqc_line_single in pqc.pqc_line:

                if not pqc_line_single.pqc_processed:

                    new_product_received_qty = pqc_line_single.received_quantity - pqc_line_single.pqc_reject_quantity

                    # check partially_reject or reject
                    if pqc_line_single.pqc_accept_quantity > 0:
                        state = "partially_reject"

                        aqc_line_list.append([0, False,
                              {

                                  'qc_id': pqc_line_single.qc_id.id,
                                  'product': pqc_line_single.product,
                                  'product_id': pqc_line_single.product_id.id,
                                  'description': pqc_line_single.description,
                                  'purchase_quantity': pqc_line_single.purchase_quantity,
                                  'received_quantity': new_product_received_qty,
                                  'accepted_quantity': pqc_line_single.pqc_accept_quantity,
                                  'pending_quantity': pqc_line_single.pending_quantity,

                              }])

                    else:
                        state = "reject"

                    pqc_line_reject_query = "UPDATE pending_quality_control_line SET pqc_reject_reason='{0}', pqc_reject_by='{1}', pqc_reject_date='{2}', state='{3}', pqc_processed=TRUE, received_quantity='{4}' WHERE id={5}".format(pqc_reject_reason, pqc_reject_by, str(fields.datetime.now()), state, new_product_received_qty, pqc_line_single.id)
                    cr.execute(pqc_line_reject_query)
                    cr.commit()

                    # what will happen to the rejected quantity!!!
                    # update to irn
                    irn_update_query = "UPDATE irn_control SET state='partial_received' WHERE id='{0}'".format(pqc.irn_id)
                    cr.execute(irn_update_query)
                    cr.commit()

                    # update to irn_line
                    irn_line_update_query = "UPDATE irn_control_line SET state='partially_received', received_quantity='{0}' WHERE irn_line_id='{1}' AND product_id='{2}'".format(new_product_received_qty, pqc.irn_id, pqc_line_single.product_id.id)
                    cr.execute(irn_line_update_query)
                    cr.commit()

                    # QC line receive quantity update
                    qc_line_update_query = "UPDATE quality_control_line SET received_quantity='{0}' WHERE qc_line_id='{1}' AND product_id='{2}'".format(new_product_received_qty, int(pqc.qc_id), pqc_line_single.product_id.id)
                    cr.execute(qc_line_update_query)
                    cr.commit()

                    # AQC line receive quantity update
                    aqc_line_update_query = "UPDATE accepted_quality_control_line SET received_quantity='{0}' WHERE qc_id='{1}' AND product_id='{2}'".format(new_product_received_qty, int(pqc.qc_id), pqc_line_single.product_id.id)
                    cr.execute(aqc_line_update_query)
                    cr.commit()

                    # PQC line receive quantity update
                    pqc_line_update_query = "UPDATE pending_quality_control_line SET received_quantity='{0}' WHERE qc_id='{1}' AND product_id='{2}'".format(new_product_received_qty, int(pqc.qc_id), pqc_line_single.product_id.id)
                    cr.execute(pqc_line_update_query)
                    cr.commit()

            # update pqc reason, status
            pqc_reject_query = "UPDATE pending_quality_control SET pqc_reject_reason='{0}', pqc_reject_by='{1}', pqc_reject_date='{2}', pqc_processed=TRUE, state='{3}' WHERE id={4}".format(pqc_reject_reason, pqc_reject_by, str(fields.datetime.now()), state, pqc.id)
            cr.execute(pqc_reject_query)
            cr.commit()

            # new AQC will be created with accepted quantity
            aqc_data['aqc_line'] = aqc_line_list
            if len(aqc_line_list) > 0:
                # Create aqc
                aqc_env = self.pool.get('accepted.quality.control')
                saved_aqc_id = aqc_env.create(cr, uid, aqc_data, context=context)

                # Confirm the aqc
                aqc_confirm_query = "UPDATE accepted_quality_control SET state='confirm', date_confirm='{0}' WHERE id={1}".format(
                    str(fields.datetime.now()), saved_aqc_id)
                cr.execute(aqc_confirm_query)
                cr.commit()

                aqc_confirm_query_line = "UPDATE accepted_quality_control_line SET state='confirm', date_confirm='{0}' WHERE aqc_id={1}".format(
                    str(fields.datetime.now()), saved_aqc_id)
                cr.execute(aqc_confirm_query_line)
                cr.commit()

                # Generate Put away Process
                put_line_list = []

                for aqc_line in aqc_line_list:
                    put_line_list.append([0, False, {
                        'product_id': aqc_line[2]['product_id'],
                        'qc_id': saved_aqc_id,
                        'location': '',
                        'product': aqc_line[2]['product'],
                        'remarks': '',
                        'quantity': aqc_line[2]['accepted_quantity'],
                        'po_number': str(pqc.po_number)
                    }])
                """
                for pqc_line_item in pqc_obj.pqc_line:
                    if pqc_line_item.accepted_quantity > 0:
                        put_line_list.append([0, False,
                                              {
                                                  'product_id': pqc_line_item.product_id,
                                                  'qc_id': saved_aqc_id,
                                                  'location': '',
                                                  'product': pqc_line_item.product,
                                                  'remarks': '',
                                                  'quantity': pqc_line_item.accepted_quantity,
                                                  'po_number': str(pqc.po_number)

                                              }])
                """
                put_data['aqc_id'] = saved_aqc_id
                put_data['putaway_line_view'] = put_line_list
                put_env = self.pool.get('putaway.process')
                saved_put_id = put_env.create(cr, uid, put_data, context=context)


        return True

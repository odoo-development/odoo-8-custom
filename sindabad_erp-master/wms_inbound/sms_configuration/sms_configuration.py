
from openerp.osv import fields, osv

import datetime
from openerp.tools import ustr, DEFAULT_SERVER_DATE_FORMAT as DF
from datetime import datetime, timedelta

class po_sms_configuration(osv.osv):
    _name = "po.sms.configuration"
    _description = "PO SMS Configuration"
    _order = 'id desc'

    _columns = {
        'sms_to': fields.char('TO',required=True),
        'subject_to': fields.selection([
            ('pending_pos_more_10_days', 'Pending PO More than 10 Days'),
            ('pending_pos_more_30_days', 'Pending PO More than 30 Days'),
            ('confirmed_pos_more_10_days', 'Confirmed PO More than 10 Days (Not Received)'),
            ('confirmed_pos_more_30_days', 'Confirmed PO More than 30 Days (Not Received)'),

        ], 'Status', help="Select the Logic to send", select=True),

    }

    def sms_trigger_with_conditional_po(self, cr, uid, ids, context=None):
        cr.execute("select sms_to, subject_to from po_sms_configuration")

        sms_conf_data = cr.fetchall()
        more_10_days = datetime.now() - timedelta(days=10)
        more_10_days = more_10_days.strftime(DF)

        more_30_days = datetime.now() - timedelta(days=30)
        more_30_days = more_30_days.strftime(DF)


        if len(sms_conf_data) >0:
            for query_data in sms_conf_data:
                total_amount = total_count = 0
                text='Test'
                mobile_number = str(query_data[0]).split(',')
                if str(query_data[1]) =='pending_pos_more_10_days':
                    purchase_ids = self.pool.get('purchase.order').search(cr, uid, [('state', '=', 'draft'),
                                                                                    ('create_date', '<', more_10_days)])
                    total_count = len(purchase_ids)
                    purchase_data = self.pool.get('purchase.order').browse(cr, uid, purchase_ids)
                    for po_data in purchase_data:
                        total_amount += po_data.amount_total



                    text = 'Dear Procurment Team,Your Pending PO history which are more than 10 days. Pending total amount : ' + str(
                        total_amount) + ' BDT and Total number of PO Order Pending ' + str(
                        total_count) + ' Please pay attention.'

                if str(query_data[1]) =='pending_pos_more_30_days':
                    purchase_ids = self.pool.get('purchase.order').search(cr, uid, [('state', '=', 'draft'),
                                                                                    ('create_date', '<', more_30_days)])
                    total_count = len(purchase_ids)
                    purchase_data = self.pool.get('purchase.order').browse(cr, uid, purchase_ids)
                    for po_data in purchase_data:
                        total_amount += po_data.amount_total



                    text = 'Dear Procurment Team,Your Pending PO history which are more than 30 days. Pending total amount : ' + str(
                        total_amount) + ' BDT and Total number of PO Order Pending ' + str(
                        total_count) + ' Please pay attention.'

                if str(query_data[1]) == 'confirmed_pos_more_10_days':
                    purchase_ids = self.pool.get('purchase.order').search(cr, uid, [('state', '=', 'approved'),('shipped', '=', False),
                                                                                    ('date_approve', '<=', more_10_days)])
                    total_count = len(purchase_ids)
                    purchase_data = self.pool.get('purchase.order').browse(cr, uid, purchase_ids)
                    for po_data in purchase_data:
                        total_amount += po_data.amount_total



                    text = 'Dear Procurment Team,Your Confirmed PO but warehouse which are not receved by Operations more than 10 days. Pending total amount : ' + str(
                        total_amount) + ' BDT and Total number of PO Order Pending ' + str(
                        total_count) + ' Please pay attention.'

                if str(query_data[1]) == 'confirmed_pos_more_30_days':

                    purchase_ids = self.pool.get('purchase.order').search(cr, uid, [('state', '=', 'approved'),
                                                                                    ('shipped', '=', False),('date_approve', '<=', more_30_days)])
                    total_count = len(purchase_ids)
                    purchase_data = self.pool.get('purchase.order').browse(cr, uid, purchase_ids)
                    for po_data in purchase_data:
                        total_amount += po_data.amount_total



                    text = '!!!!ALERT!!!! Dear Procurment Team,Your Confirmed PO but warehouse which are not receved by Operations more than 30 days. Pending total amount : ' + str(
                        total_amount) + ' BDT and Total number of PO Order Pending ' + str(
                        total_count) + ' Please pay attention.'

                for num in mobile_number:
                    if len(num)>10 and len(num) <14:
                        try:
                            data = {'msg': text, 'to': str(num)}
                            new_r = self.pool["common.sms"].create(cr, uid, data)
                        except:
                            pass





        return True




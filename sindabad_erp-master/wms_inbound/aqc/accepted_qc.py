from datetime import datetime, timedelta
import time
from datetime import date
from openerp import SUPERUSER_ID
from openerp.osv import fields, osv
from openerp.tools.translate import _
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT
import openerp.addons.decimal_precision as dp
from openerp import workflow
from openerp import api
import datetime


class accepted_quality_control(osv.osv):
    _name = "accepted.quality.control"
    _description = "Accepted Quality Control"
    _order = 'id desc'

    _columns = {
        'scan': fields.char('Scan'),
        'product_scan': fields.char('Product Scan'),
        'warehouse_id': fields.many2one('stock.warehouse', string='Warehouse Location', required=True),
        'aqc_number': fields.char('AQC Number'),
        'qc_number': fields.char('QC Number'),
        'qc_id': fields.char('QC ID'),
        'pqc_number': fields.char('PQC Number'),
        'pqc_id': fields.integer('PQC ID'),
        'irn_number': fields.char('IRN Number'),
        'irn_id': fields.integer('IRN ID'),
        'po_number': fields.char('PO Number'),
        'po_id': fields.integer('Purchase ID'),
        'remark': fields.text('Remark'),
        'assigned': fields.boolean('Assigned'),
        'received_by': fields.many2one('res.users', 'Received By'),
        'date_confirm': fields.datetime('Confirmation Date', readonly=True, select=False),
        'state': fields.selection([
            ('pending', 'Pending'),
            ('confirm', 'Confirmed'),
            ('cancel', 'Cancelled'),

        ], 'Status', readonly=True, copy=False, help="Gives the status of the WMS inbound", select=True),
        'aqc_line': fields.one2many('accepted.quality.control.line', 'aqc_id', 'AQC Line', required=True),
    }

    _defaults = {
        'user_id': lambda obj, cr, uid, context: uid,
        'received_by': lambda obj, cr, uid, context: uid,
        'state': 'pending',
    }

    @api.model
    def create(self, vals):
        # generate AQC number
        # AQC0123456

        record = super(accepted_quality_control, self).create(vals)

        aqc_number = "AQC0" + str(record.id)
        record.aqc_number = aqc_number

        self.pool.get('inb.end.to.end').aqc_data_create(self._cr, self._uid, record.po_number, record.po_id, record.id, aqc_number, record.create_date, context=self._context)

        return record

    def confirm_wms_inbound_aqc(self, cr, uid, ids, context=None):

        for single_id in ids:
            aqc_confirm_query = "UPDATE accepted_quality_control SET state='confirm', date_confirm='{0}' WHERE id={1}".format(str(fields.datetime.now()), single_id)
            cr.execute(aqc_confirm_query)
            cr.commit()

            aqc_confirm_query_line = "UPDATE accepted_quality_control_line SET state='confirm', date_confirm='{0}' WHERE aqc_id={1}".format(
                str(fields.datetime.now()), single_id)
            cr.execute(aqc_confirm_query_line)
            cr.commit()

        # update inbound end-to-end
        self.pool.get('inb.end.to.end').aqc_confirm_update(cr, uid, str(self.po_number), self.po_id, str(fields.datetime.now()), context=context)

        return True


class accepted_quality_control_line(osv.osv):
    _name = "accepted.quality.control.line"
    _description = "Accepted Quality Control Line"

    _columns = {
        'aqc_id': fields.many2one('accepted.quality.control', 'AQC ID', required=True,
                                      ondelete='cascade', select=True, readonly=True),
        'qc_id': fields.many2one('quality.control', 'QC ID', required=False,
                                  ondelete='cascade', select=True, readonly=True),
        'product': fields.char('Product'),
        # 'product_id': fields.integer('Product ID'),
        'product_id': fields.many2one('product.product', 'Product', required=True),
        'product_ean': fields.char('Product EAN'),
        'description': fields.char('Description'),
        'purchase_quantity': fields.float('Purchase Qty'),
        'received_quantity': fields.float('Received Qty'),
        'accepted_quantity': fields.float('Accepted Qty'),
        'date_confirm': fields.datetime('Confirmation Date', readonly=True, select=False),
        'pending_quantity': fields.float('Pending Qty'),
        'remaining_quantity': fields.float('Remaining GRN Received Qty'), # This column added by mufti. It will calculate how many quantities are pending for GRN Receive. And This column is filling up from Put away process.

        'state': fields.selection([
            ('pending', 'Pending'),
            ('confirm', 'Confirmed'),
            ('cancel', 'Cancelled'),
        ], 'Status', help="Gives the status of the WMS inbound line", select=True),
    }


from openerp.osv import fields, osv
from datetime import datetime, timedelta
from openerp.tools import ustr, DEFAULT_SERVER_DATE_FORMAT as DF


class product_config(osv.osv):
    _inherit = 'product.product'

    def _get_60_days_lowest_purchase_price(self, cr, uid, ids, name, arg, context=None):
        pol_obj = self.pool.get('purchase.order.line')
        res = {}


        compare_date = datetime.now() - timedelta(days=61)

        compare_date = compare_date.strftime(DF)


        for product_id in ids:

            #get all purchase order lines with current product product_id
            res[product_id] = qty = subtot = others = others_qty = 0.0
            lowest_price = 999999999.0

                
            line_ids = pol_obj.search(cr, uid, [('product_id', '=', product_id),('create_date', '>', compare_date)], context=context)
            for line in pol_obj.browse(cr, uid, line_ids, context):
                if line.state == 'confirmed' or line.state == 'done':
                    if line.price_unit < lowest_price:
                        lowest_price = line.price_unit

            res[product_id]=lowest_price if lowest_price != 999999999.0 else 0
        return res



    _columns = {
        'gift_ean': fields.char('EAN 2'),
        'prod_uom': fields.char('UOM'),
        'has_box': fields.boolean('Has Box'),
        'holding_box_qty': fields.float('Box Qty'),
        'box_ean': fields.char('BOX EAN'),
        'has_case': fields.boolean('Has Case'),
        'holding_case_qty': fields.float('Pallet Qty'),
        'case_ean': fields.char('Case EAN'),
        'has_pallet': fields.boolean('Has Pallet'),
        'holding_pallet_qty': fields.float('Pallet Qty'),
        'pallet_ean': fields.char('Pallet EAN'),
        'reorder_qty_level': fields.float('Re Order Qty Level'),
        'uttwh_location': fields.char('Uttara Location'),
        'nodda_location': fields.char('Nodda Location'),
        'motijil_location': fields.char('Motijil Location'),
        'retail_location': fields.char('Retail Location'),
        'auto_po_generation': fields.boolean('Auto PO Genration'),
        'last_60_days_lowest_price': fields.function(_get_60_days_lowest_purchase_price, type='float', string='Last 60 Days Lowest Purchase Price'),


    }
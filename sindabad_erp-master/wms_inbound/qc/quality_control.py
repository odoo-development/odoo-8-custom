from datetime import datetime, timedelta
import time
from datetime import date
from openerp import SUPERUSER_ID
from openerp.osv import fields, osv
from openerp.tools.translate import _
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT
import openerp.addons.decimal_precision as dp
from openerp import workflow
from openerp import api
import datetime


class quality_control(osv.osv):
    _name = "quality.control"
    _description = "Quality Control"
    _order = 'id desc'

    _columns = {
        'scan': fields.char('Scan'),
        'product_scan': fields.char('Product Scan'),
        'warehouse_id': fields.many2one('stock.warehouse', string='Warehouse Location', required=True),
        'qc_number': fields.char('QC Number'),
        'irn_number': fields.char('IRN Number'),
        'irn_id': fields.integer('IRN ID'),
        'po_number': fields.char('PO Number'),
        'po_id': fields.integer('Purchase ID'),
        'remark': fields.text('Remark'),
        'received_by': fields.many2one('res.users', 'Received By'),
        'apqc_pushed': fields.boolean('A/P QC Pushed'),
        'date_confirm': fields.datetime('Confirmation Date', readonly=True, select=False),
        'state': fields.selection([
            ('pending', 'Pending'),
            ('confirmed', 'Confirmed'),
            ('cancel', 'Cancelled'),

        ], 'Status', readonly=True, copy=False, help="Gives the status of the WMS inbound", select=True),
        'qc_line': fields.one2many('quality.control.line', 'qc_line_id', 'QC Line', required=True),
        'aqc_line': fields.one2many('accepted.quality.control.line', 'qc_id', 'AQC Line', required=False),
        'pqc_line': fields.one2many('pending.quality.control.line', 'qc_id', 'PQC Line', required=False),
    }

    _defaults = {
        'user_id': lambda obj, cr, uid, context: uid,
        'received_by': lambda obj, cr, uid, context: uid,
        'state': 'pending',
    }

    def _set_po_id_in_line(self, record, po_id):

        for line_item in record.qc_line:
            line_item.po_id = po_id

        return record

    # calls at the time of create new record
    @api.model
    def create(self, vals):
        # generate QC number
        # QC0123456

        record = super(quality_control, self).create(vals)

        qc_number = "QC0" + str(record.id)
        record.qc_number = qc_number

        record = self._set_po_id_in_line(record, record.po_id)

        self.pool.get('inb.end.to.end').qc_data_create(self._cr, self._uid, record.po_number, record.po_id, record.id, qc_number, record.create_date, context=self._context)

        return record

    # calls at the time of update record
    @api.multi
    def write(self, vals):

        # only update only if (received_quantity = accepted_quantity + pending_quantity)
        qc_env = self.pool.get('quality.control')
        qc_obj = qc_env.browse(self._cr, self._uid, [self.id], context=self._context)
        update_permission = list()

        if vals.has_key('qc_line'):
            for single_line in vals['qc_line']:
                if not not single_line[2]:
                    for qc_l in self.qc_line:

                        if qc_l.id == single_line[1]:

                            accepted_quantity = single_line[2]['accepted_quantity'] if single_line[2].has_key('accepted_quantity') else qc_l.accepted_quantity
                            pending_quantity = single_line[2]['pending_quantity'] if single_line[2].has_key('pending_quantity') else qc_l.pending_quantity

                            pending_quantity = pending_quantity * -1 if pending_quantity < 0 else pending_quantity

                            if qc_l.received_quantity == accepted_quantity + pending_quantity:
                                update_permission.append(True)
                            else:
                                update_permission.append(False)

        if False in update_permission:

            raise osv.except_osv(_('QC line adjustment ERROR!'), _('Received quantity should be equal to (accepted quantity + pending quantity)!!!'))

        else:

            record = super(quality_control, self).write(vals)

            return record

    def confirm_wms_inbound_qc(self, cr, uid, ids, context=None):

        qc_env = self.pool.get('quality.control')
        qc_obj = qc_env.browse(cr, uid, ids, context=context)

        # 1. AQC will create with accepted quantity
        put_data = aqc_data = {
            'qc_number': str(qc_obj.qc_number),
            'qc_id': qc_obj.id,
            'warehouse_id': qc_obj.warehouse_id.id,
            'irn_number': str(qc_obj.irn_number),
            'irn_id': qc_obj.irn_id,
            'po_number': str(qc_obj.po_number),
            'po_id': qc_obj.po_id,
            'remark': str(qc_obj.remark),
            'received_by': qc_obj.received_by.id,
        }
        aqc_line_list = list()

        for aqc_line_item in qc_obj.qc_line:
            if aqc_line_item.accepted_quantity > 0:
                aqc_line_list.append([0, False,
                                     {
                                         'qc_id': qc_obj.id,
                                         'product': aqc_line_item.product,
                                         'product_id': aqc_line_item.product_id,
                                         'product_ean': aqc_line_item.product_ean,
                                         'description': aqc_line_item.description,
                                         'purchase_quantity': aqc_line_item.purchase_quantity,
                                         'received_quantity': aqc_line_item.received_quantity,
                                         'accepted_quantity': aqc_line_item.accepted_quantity,
                                         'pending_quantity': aqc_line_item.pending_quantity,
                                         'product_exp_date': aqc_line_item.product_exp_date,

                                     }])

        aqc_data['aqc_line'] = aqc_line_list

        if len(aqc_line_list) > 0:
            # Create aqc
            aqc_env = self.pool.get('accepted.quality.control')
            saved_aqc_id = aqc_env.create(cr, uid, aqc_data, context=context)

            # update qc status and apqc_pushed to true
            qc_state_update_query = "UPDATE quality_control SET state='confirmed', apqc_pushed=TRUE, date_confirm='{0}' WHERE id={1}".format(str(fields.datetime.now()), qc_obj.id)
            cr.execute(qc_state_update_query)
            cr.commit()

            apqc_pushed_qc_line_update_query = "UPDATE quality_control_line SET apqc_pushed=TRUE WHERE qc_line_id={0}".format(qc_obj.id)
            cr.execute(apqc_pushed_qc_line_update_query)
            cr.commit()


            # Confirm the aqc

            aqc_confirm_query = "UPDATE accepted_quality_control SET state='confirm', date_confirm='{0}' WHERE id={1}".format(
                str(fields.datetime.now()), saved_aqc_id)
            cr.execute(aqc_confirm_query)
            cr.commit()

            aqc_confirm_query_line = "UPDATE accepted_quality_control_line SET state='confirm', date_confirm='{0}' WHERE aqc_id={1}".format(
                str(fields.datetime.now()), saved_aqc_id)
            cr.execute(aqc_confirm_query_line)
            cr.commit()

            # import pdb
            # pdb.set_trace()

            # Generate Put away Process
            put_line_list = []

            for aqc_line_item in qc_obj.qc_line:
                if aqc_line_item.accepted_quantity > 0:
                    put_line_list.append([0, False,
                                          {
                                              'product_id': aqc_line_item.product_id,
                                              'qc_id': saved_aqc_id,
                                              'location': '',
                                              'product': aqc_line_item.product,
                                              'remarks': '',
                                              'quantity': aqc_line_item.accepted_quantity,
                                              'product_exp_date': aqc_line_item.product_exp_date,
                                              'po_number': str(qc_obj.po_number)




                                          }])

            put_data['aqc_id']=saved_aqc_id
            put_data['putaway_line_view']=put_line_list
            put_env = self.pool.get('putaway.process')
            saved_put_id = put_env.create(cr, uid, put_data, context=context)







        # 2. PQC will create with pending quantity
        pqc_data = {
            'qc_number': str(qc_obj.qc_number),
            'qc_id': qc_obj.id,
            'warehouse_id': qc_obj.warehouse_id.id,
            'irn_number': str(qc_obj.irn_number),
            'irn_id': qc_obj.irn_id,
            'po_number': str(qc_obj.po_number),
            'po_id': qc_obj.po_id,
            'remark': str(qc_obj.remark),
            'received_by': qc_obj.received_by.id,
        }
        pqc_line_list = list()

        for pqc_line_item in qc_obj.qc_line:

            if pqc_line_item.pending_quantity > 0:
                pqc_line_list.append([0, False,
                                     {
                                         'qc_id': qc_obj.id,
                                         'product': pqc_line_item.product,
                                         'product_id': pqc_line_item.product_id,
                                         'product_ean': pqc_line_item.product_ean,
                                         'description': pqc_line_item.description,
                                         'purchase_quantity': pqc_line_item.purchase_quantity,
                                         'received_quantity': pqc_line_item.received_quantity,
                                         'accepted_quantity': pqc_line_item.accepted_quantity,
                                         'pending_quantity': pqc_line_item.pending_quantity,
                                         'pqc_reject_quantity': pqc_line_item.pending_quantity,
                                     }])

        pqc_data['pqc_line'] = pqc_line_list

        if len(pqc_line_list) > 0:
            # Create pqc
            pqc_env = self.pool.get('pending.quality.control')
            saved_pqc_id = pqc_env.create(cr, uid, pqc_data, context=context)

        # update inbound end-to-end
        self.pool.get('inb.end.to.end').qc_confirm_update(cr, uid, str(qc_obj.po_number), qc_obj.po_id, str(fields.datetime.now()), context=context)

        return True

    """
    @api.onchange('scan')
    def po_barcode_onchange(self):

        import pdb;pdb.set_trace()

        return "xXxXxXxXxX"
    """

    @api.onchange('product_scan')
    def po_qc_product_scan(self):

        try:

            po_number = str(self.po_number)

            product_scan = str(self.product_scan)

            if product_scan != 'False':
                qc_env = self.env['quality.control']
                qc_obj = qc_env.search([('qc_number', '=', self.qc_number)])

                # po_line_env = self.env['purchase.order.line']
                # po_line_obj = po_line_env.search([('order_id', '=', po_obj.id)])

                # po_line_obj[0].product_id.ean13
                # self.irn_line[0].product_ean

                for single_line in qc_obj.qc_line:

                    if str(single_line.product_ean) == product_scan:
                        if single_line.accepted_quantity < single_line.received_quantity:
                            single_line.accepted_quantity += 1
                        else:
                            raise osv.except_osv(_('QC line adjustment ERROR!'), _(
                                'Received quantity should be equal to (accepted quantity + pending quantity)!!!'))
                        if single_line.pending_quantity > 0:
                            single_line.pending_quantity -= 1
                        else:
                            raise osv.except_osv(_('QC line adjustment ERROR!'), _(
                                'Received quantity should be equal to (accepted quantity + pending quantity)!!!'))
                        break
                # self.irn_line[0].received_quantity
                """
                if self.pending_quantity < 0:
                    # raise error
                    raise osv.except_osv(_('QC line adjustment ERROR!'), _(
                        'Received quantity should be equal to (accepted quantity + pending quantity)!!!'))
                """
                self.product_scan = ""

        except:

            self.product_scan = ""

        return "xXxXxXxXxX"


class quality_control_line(osv.osv):
    _name = "quality.control.line"
    _description = "Quality Control Line"

    _columns = {
        'qc_line_id': fields.many2one('quality.control', 'QC Line ID', required=True,
                                           ondelete='cascade', select=True, readonly=True),
        'po_id': fields.many2one('purchase.order', 'PO ID', required=False,
                                 ondelete='cascade', select=True, readonly=True),
        'product': fields.char('Product'),
        'product_id': fields.integer('Product ID'),
        'product_ean': fields.char('Product EAN'),
        'description': fields.char('Description'),
        'purchase_quantity': fields.float('Purchase Qty'),
        'received_quantity': fields.float('Received Qty'),
        'accepted_quantity': fields.float('Accepted Qty'),
        'pending_quantity': fields.float('Pending Qty'),
        'apqc_pushed': fields.boolean('A/P QC Pushed'),
        'state': fields.selection([
            ('accept', 'Accept'),
            ('reject', 'Reject'),
            ('cancel', 'Cancelled'),
        ], 'Status', help="Gives the status of the WMS inbound line", select=True),
    }

    @api.onchange('accepted_quantity')
    def po_qc_product_scan(self):

        try:

            if self.accepted_quantity > 0:

                # po_line_obj[0].product_id.ean13
                # self.irn_line[0].product_ean

                # self.pending_quantity = self.pending_quantity - self.accepted_quantity
                self.pending_quantity = self.received_quantity - self.accepted_quantity
                self.save()
                # self.irn_line[0].received_quantity

                if self.pending_quantity < 0:
                    # raise error
                    raise osv.except_osv(_('QC line adjustment ERROR!'), _(
                        'Received quantity should be equal to (accepted quantity + pending quantity)!!!'))

                self.product_scan = ""

        except:

            self.product_scan = ""

        return "xXxXxXxXxX"


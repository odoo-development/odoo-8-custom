from datetime import datetime, timedelta
import time
from datetime import date
from openerp import SUPERUSER_ID
from openerp.osv import fields, osv
from openerp.tools.translate import _
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT
import openerp.addons.decimal_precision as dp
from openerp import workflow
from openerp import api
import datetime
from openerp.exceptions import except_orm, Warning, RedirectWarning

# "2019-01-15 07:20:15.351546"


class inb_end_to_end(osv.osv):
    _name = "inb.end.to.end"
    _description = "Inbound End-to-End"

    _columns = {
        'po_number': fields.char('PO Number'),
        'po_id': fields.integer('Purchase ID'),
        'irn_id': fields.char('IRN IDs'),
        'irn_number': fields.char('IRN Numbers'),
        'irn_date_create': fields.text('IRN Created Date'),
        'irn_date_confirm': fields.text('IRN Confirm Date'),
        'qc_id': fields.char('QC IDs'),
        'qc_number': fields.char('QC Numbers'),
        'qc_date_create': fields.text('QC Created Date'),
        'qc_date_confirm': fields.text('QC Confirm Date'),
        'aqc_id': fields.char('AQC IDs'),
        'aqc_number': fields.char('AQC Number'),
        'aqc_date_create': fields.text('AQC Created Date'),
        'aqc_date_confirm': fields.text('AQC Confirm Date'),
        'pqc_id': fields.char('PQC IDs'),
        'pqc_number': fields.char('PQC Number'),
        'pqc_date_create': fields.text('PQC Created Date'),
        'pqc_date_confirm': fields.text('PQC Confirm Date'),
        'putaway_id': fields.char('Putaway IDs'),
        'putaway_number': fields.char('Putaway Number'),
        'putaway_date_create': fields.text('Putaway Created Date'),
        'putaway_date_confirm': fields.text('Putaway Confirm Date'),
    }

    def irn_data_create(self, cr, uid, po_number, po_id, irn_id, irn_number, irn_date_create, context=None):

        # check po_number and po_id already exists

        po_count = self.search_count(cr, uid, [('po_number','=',po_number), ('po_id', '=', po_id)])

        if int(po_count) == 0:
            created_id = self.create(cr, uid, {
                'po_number': po_number,
                'po_id': po_id,
                'irn_id': irn_id,
                'irn_number': irn_number,
                'irn_date_create': irn_date_create,
            }, context=context)
        else:

            inb_etoe_id = self.search(cr, uid, [('po_number', '=', po_number), ('po_id', '=', po_id)])

            # browse and then update
            for etoe in self.browse(cr, uid, inb_etoe_id, context):
                self.write(cr, uid, inb_etoe_id, {
                    'irn_id': str(etoe.irn_id) + "," + str(irn_id),
                    'irn_number': str(etoe.irn_number) + "," + str(irn_number),
                    'irn_date_create': str(etoe.irn_date_create) + "," + str(irn_date_create),
                })

        return True

    def irn_confirm_update(self, cr, uid, po_number, po_id, irn_date_confirm, context=None):

        inb_etoe_id = self.search(cr, uid, [('po_number', '=', po_number), ('po_id', '=', po_id)])

        # browse and then update
        for etoe in self.browse(cr, uid, inb_etoe_id, context):

            self.write(cr, uid, inb_etoe_id, {
                'irn_date_confirm': str(irn_date_confirm) if str(etoe.irn_date_confirm) == 'False' else str(etoe.irn_date_confirm) + "," + str(irn_date_confirm),
            })

        return True

    def qc_data_create(self, cr, uid, po_number, po_id, qc_id, qc_number, qc_date_create, context=None):

        inb_etoe_id = self.search(cr, uid, [('po_number', '=', po_number), ('po_id', '=', po_id)])

        # browse and then update
        for etoe in self.browse(cr, uid, inb_etoe_id, context):
            self.write(cr, uid, inb_etoe_id, {
                'qc_id': str(qc_id) if str(etoe.qc_id) == 'False' else str(etoe.qc_id) + "," + str(qc_id),
                'qc_number': str(qc_number) if str(etoe.qc_number) == 'False' else str(etoe.qc_number) + "," + str(qc_number),
                'qc_date_create': str(qc_date_create) if str(etoe.qc_date_create) == 'False' else str(etoe.qc_date_create) + "," + str(qc_date_create),
            })

        return True

    def qc_confirm_update(self, cr, uid, po_number, po_id, qc_date_confirm, context=None):

        inb_etoe_id = self.search(cr, uid, [('po_number', '=', po_number), ('po_id', '=', po_id)])

        # browse and then update
        for etoe in self.browse(cr, uid, inb_etoe_id, context):
            self.write(cr, uid, inb_etoe_id, {
                'qc_date_confirm': str(qc_date_confirm) if str(etoe.qc_date_confirm) == 'False' else str(etoe.qc_date_confirm) + "," + str(qc_date_confirm),
            })

        return True

    def aqc_data_create(self, cr, uid, po_number, po_id, aqc_id, aqc_number, aqc_date_create, context=None):

        inb_etoe_id = self.search(cr, uid, [('po_number', '=', po_number), ('po_id', '=', po_id)])

        # browse and then update
        for etoe in self.browse(cr, uid, inb_etoe_id, context):
            self.write(cr, uid, inb_etoe_id, {
                'aqc_id': str(aqc_id) if str(etoe.aqc_id) == 'False' else str(etoe.aqc_id) + "," + str(aqc_id),
                'aqc_number': str(aqc_number) if str(etoe.aqc_number) == 'False' else str(etoe.aqc_number) + "," + str(aqc_number),
                'aqc_date_create': str(aqc_date_create) if str(etoe.aqc_date_create) == 'False' else str(etoe.aqc_date_create) + "," + str(aqc_date_create),
            })

        return True

    def aqc_confirm_update(self, cr, uid, po_number, po_id, aqc_date_confirm, context=None):

        inb_etoe_id = self.search(cr, uid, [('po_number', '=', po_number), ('po_id', '=', po_id)])

        # browse and then update
        for etoe in self.browse(cr, uid, inb_etoe_id, context):
            self.write(cr, uid, inb_etoe_id, {
                'aqc_date_confirm': str(aqc_date_confirm) if str(etoe.aqc_date_confirm) == 'False' else str(etoe.aqc_date_confirm) + "," + str(aqc_date_confirm),
            })

        return True

    def pqc_data_create(self, cr, uid, po_number, po_id, pqc_id, pqc_number, pqc_date_create, context=None):

        inb_etoe_id = self.search(cr, uid, [('po_number', '=', po_number), ('po_id', '=', po_id)])

        # browse and then update
        for etoe in self.browse(cr, uid, inb_etoe_id, context):
            self.write(cr, uid, inb_etoe_id, {
                'pqc_id': str(pqc_id) if str(etoe.pqc_id) == 'False' else str(etoe.pqc_id) + "," + str(pqc_id),
                'pqc_number': str(pqc_number) if str(etoe.pqc_number) == 'False' else str(etoe.pqc_number) + "," + str(pqc_number),
                'pqc_date_create': str(pqc_date_create) if str(etoe.pqc_date_create) == 'False' else str(etoe.pqc_date_create) + "," + str(pqc_date_create),
            })

        return True

    def pqc_confirm_update(self, cr, uid, po_number, po_id, pqc_date_confirm, context=None):

        inb_etoe_id = self.search(cr, uid, [('po_number', '=', po_number), ('po_id', '=', po_id)])

        # browse and then update
        for etoe in self.browse(cr, uid, inb_etoe_id, context):
            self.write(cr, uid, inb_etoe_id, {
                'pqc_date_confirm': str(pqc_date_confirm) if str(etoe.pqc_date_confirm) == 'False' else str(etoe.pqc_date_confirm) + "," + str(pqc_date_confirm),
            })

        return True

    def putaway_data_create(self, cr, uid, po_number, po_id, putaway_id, putaway_number, putaway_date_create, context=None):

        inb_etoe_id = self.search(cr, uid, [('po_number', '=', po_number), ('po_id', '=', po_id)])

        # browse and then update
        for etoe in self.browse(cr, uid, inb_etoe_id, context):
            self.write(cr, uid, inb_etoe_id, {
                'putaway_id': str(putaway_id) if str(etoe.putaway_id) == 'False' else str(etoe.putaway_id) + "," + str(putaway_id),
                'putaway_number': str(putaway_number) if str(etoe.putaway_number) == 'False' else str(etoe.putaway_number) + "," + str(putaway_number),
                'putaway_date_create': str(putaway_date_create) if str(etoe.putaway_date_create) == 'False' else str(etoe.putaway_date_create) + "," + str(putaway_date_create),
            })

        return True

    def putaway_confirm_update(self, cr, uid, po_number, po_id, putaway_date_confirm, context=None):

        inb_etoe_id = self.search(cr, uid, [('po_number', '=', po_number), ('po_id', '=', po_id)])

        # browse and then update
        for etoe in self.browse(cr, uid, inb_etoe_id, context):
            self.write(cr, uid, inb_etoe_id, {
                'putaway_date_confirm': str(putaway_date_confirm) if str(etoe.putaway_date_confirm) == 'False' else str(etoe.putaway_date_confirm) + "," + str(putaway_date_confirm),
            })

        return True

from datetime import datetime, timedelta
import time
from datetime import date, datetime
from openerp import SUPERUSER_ID
from openerp.osv import fields, osv
from openerp.tools.translate import _
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT
import openerp.addons.decimal_precision as dp
from openerp import workflow
from openerp import api
import datetime
from openerp.exceptions import except_orm, Warning, RedirectWarning

class relation_put_line_to_aqc_line(osv.osv):
    _name = 'relation.put.line.to.aqc.line'

    _columns = {
        'product_id': fields.integer('Product ID'),
        'put_line_id': fields.integer('Put Line ID'),
        'aqc_line_id': fields.integer('AQC Line ID'),
        'assigned_qty': fields.float('Assigned Quantity'),
        'location': fields.char('Location'),
        'grn_id': fields.integer('GRN ID'),

    }

class putaway_process(osv.osv):
    _name = 'putaway.process'

    _description = "Putaway Process For Inbound"

    _columns = {
        'scan': fields.char('Scan'),
        'location_scan': fields.char('Product Location Scan'),
        'location': fields.char('Your Current Location'),
        'product_scan': fields.char('Product Scan'),
        'product_location': fields.char('Product Location'),
        'name': fields.char('Name'),
        'warehouse_id': fields.many2one('stock.warehouse', string='Warehouse ID', required=True),
        'aqc_id': fields.many2one('accepted.quality.control', string='AQC ID'),

        'assign_id': fields.char('Assignee'),
        'description': fields.char('Description'),
        'confirmation_date': fields.datetime('Confirmation Date'),
        'state': fields.selection([
            ('confirmed', 'Confirmed'),
            ('pending', 'Pending'),
            ('cancel', 'Cancelled'),
        ], 'Status', help="Gives the status of the Put Away control", select=True),

        'putaway_line_view': fields.one2many('putaway.process.details.view', 'putaway_line_ids', 'Putaway Line',
                                             required=True),



    }

    _defaults = {
        'state': 'pending',

    }

    _order = 'id desc'

    def put_cancel(self, cr, uid, ids, context=None):
        id_list = ids
        put_data = self.browse(cr, uid, ids, context)

        if put_data.state == 'confirmed':
            raise osv.except_osv(_('Confirmed'), _('This Put Away already confirmed. You can not cancel it'))

        if put_data.state == 'cancel':
            raise osv.except_osv(_('Cancelled'), _('This Put Away already Cancelled'))


        put_line_id = [item.id for item in put_data.putaway_line_view]
        put_line_ids = self.pool.get('relation.put.line.to.aqc.line').search(cr, uid,
                                                                             [('put_line_id', 'in', put_line_id)])
        assigned_put = self.pool.get('relation.put.line.to.aqc.line').browse(cr, uid, put_line_ids)

        for rel_item in assigned_put:

            cr.execute(
                "select accepted_quality_control.id,accepted_quality_control_line.remaining_quantity from  accepted_quality_control,accepted_quality_control_line where accepted_quality_control.id =accepted_quality_control_line.aqc_id and accepted_quality_control_line.id=%s",
                ([rel_item.aqc_line_id]))
            qc_ids = cr.fetchall()
            if len(qc_ids)>0:
                remaining_qty = qc_ids[0][1]+rel_item.assigned_qty
                cr.execute("update accepted_quality_control set assigned=FALSE where id=%s", ([qc_ids[0][0]]))
                cr.commit()
                cr.execute("update accepted_quality_control_line set remaining_quantity=%s where id=%s", ([remaining_qty,rel_item.aqc_line_id]))
                cr.commit()

        for element in id_list:
            ids = [element]

            if ids is not None:
                cr.execute("update putaway_process set state='cancel' where id=%s", (ids))
                cr.commit()

        return ''

    def put_confirm(self, cr, uid, ids, context=None):
        id_list = ids
        aqc_list=[]
        put_data = self.browse(cr, uid, ids, context)


        product_dict = {}

        for items in put_data.putaway_line_view:
            product_dict[items.product_id]=[items.quantity,items.location]

        for item in put_data.putaway_line_view:
            if len(item.location) <2:
                raise osv.except_osv(_('Warning'), _('You have to specify the location.'))


        if put_data.state == 'confirmed':
            raise osv.except_osv(_('Confirmed'), _('This Put Away already confirmed'))
        if put_data.state == 'cancel':
            raise osv.except_osv(_('Cancelled'), _('This Put Away already Cancelled. You can not confirm it. Please create new one.'))




        from datetime import date, datetime
        current_time = datetime.now()

        for element in id_list:
            ids = [element]

            if ids is not None:
                try:
                    for p_id in ids:
                        confirm_wms_query = "UPDATE putaway_process SET state='confirmed', confirmation_date='{0}' WHERE id='{1}'".format(str(fields.datetime.now()), p_id)
                        cr.execute(confirm_wms_query)
                        cr.commit()
                except:
                    pass

        # put_line_id= [item.id for item in put_data.putaway_line_view]
        # put_line_ids = self.pool.get('relation.put.line.to.aqc.line').search(cr, uid, [('put_line_id', 'in', put_line_id)])
        # assigned_put = self.pool.get('relation.put.line.to.aqc.line').browse(cr, uid, put_line_ids)


        update_grn_id_dict={}

        created_id = None

        for item in put_data.putaway_line_view:

            # cr.execute("select accepted_quality_control.po_id from  accepted_quality_control,accepted_quality_control_line where accepted_quality_control.id =accepted_quality_control_line.aqc_id and accepted_quality_control_line.id=%s", ([item.aqc_line_id]))
            # po_ids=cr.fetchall()
            #
            # po_id=None
            #
            # if len(po_ids)>0:
            po_id=item.po_number

            po_id = self.pool.get('purchase.order').search(cr, uid,[('name', '=', item.po_number)])

            purchase_order = self.pool.get('purchase.order').browse(cr, uid, po_id, context=context)


            picking_ids=[]

            for po in purchase_order:
                for picking in po.picking_ids:
                    if picking.state in ['assigned', 'partially_available']:
                        picking_ids.append(picking.id)



            if len(picking_ids)>0:
                picking = [picking_ids[0]]


            context.update({

                'active_model': 'stock.picking',
                'active_ids': picking,
                'active_id': len(picking) and picking[0] or False
            })
            created_id = self.pool['stock.transfer_details'].create(cr, uid, {
                'picking_id': len(picking) and picking[0] or False}, context)

            if created_id is not None:
                break

        if created_id:
            created_id = self.pool.get('stock.transfer_details').browse(cr, uid, created_id, context=context)
            if created_id.picking_id.state in ['assigned', 'partially_available']:
                # raise Warning(_('You cannot transfer a picking in state \'%s\'.') % created_id.picking_id.state)

                processed_ids = []


                # Create new and update existing pack operations
                for lstits in [created_id.item_ids, created_id.packop_ids]:
                    for prod in lstits:
                        if product_dict.get(prod.product_id.id):

                            pack_datas = {
                                'product_id': prod.product_id.id,
                                'product_uom_id': prod.product_uom_id.id,
                                'product_qty': product_dict.get(prod.product_id.id)[0],
                                'package_id': prod.package_id.id,
                                'lot_id': prod.lot_id.id,
                                'location_id': prod.sourceloc_id.id,
                                'location_dest_id': prod.destinationloc_id.id,
                                'result_package_id': prod.result_package_id.id,
                                'date': prod.date if prod.date else datetime.now(),
                                'owner_id': prod.owner_id.id,
                                'product_location': product_dict.get(prod.product_id.id)[1]
                            }

                            if prod.packop_id:
                                prod.packop_id.with_context(no_recompute=True).write(pack_datas)
                                processed_ids.append(prod.packop_id.id)
                            else:
                                pack_datas['picking_id'] = created_id.picking_id.id
                                packop_id = created_id.env['stock.pack.operation'].create(pack_datas)
                                processed_ids.append(packop_id.id)
                # Delete the others
                packops = created_id.env['stock.pack.operation'].search(
                    ['&', ('picking_id', '=', created_id.picking_id.id), '!', ('id', 'in', processed_ids)])
                packops.unlink()

                # Execute the transfer of the picking
                created_id.picking_id.do_transfer()

        self.pool.get('inb.end.to.end').putaway_confirm_update(cr, uid, str(put_data.aqc_id.po_number), put_data.aqc_id.po_id, str(fields.datetime.now()), context=context)


        return ''

    @api.onchange('location_scan')
    def put_away_location_onchange(self):
        location = str(self.location_scan)
        self.location=location
        self.location_scan=""
        return 'Cool'

    def check_total_qty_put_line(self,get_all_pending_aqc_list=[],submitted_put_line_value=[]):

        product_list = [items[2].get('product_id') for items in submitted_put_line_value]

        product_list = list(set(product_list))

        submitted_dict = {}

        for p_id in product_list:
            qty = 0
            for items in submitted_put_line_value:
                if items[2].get('product_id') == p_id:
                    qty += items[2].get('quantity')
        submitted_dict[p_id] = qty

        query_product_list = [items[2] for items in get_all_pending_aqc_list]
        query_product_list = list(set(query_product_list))

        query_dict = {}

        for qp_id in query_product_list:
            qty = 0
            for item in get_all_pending_aqc_list:
                if qp_id == item[2]:
                    qty += item[4]
            query_dict[qp_id] = qty

        message = 0

        for key, item in submitted_dict.iteritems():

            if query_dict.get(key) is not None:
                if item > query_dict.get(key):
                    message = 1
                    break
            else:
                message = 2
                break
        return message

    @api.onchange('product_scan')
    def put_away_product_onchange(self):
        ean_number = str(self.product_scan)
        location = str(self.location)
        self.product_scan = ""
        if location == 'False':
            raise Warning(_(
                'At first scan the location then product'))



        found_product_id=None

        if self.putaway_line_view:
            new_line=[]
            for items in self.putaway_line_view:
                new_line.append({
                    'qc_id':items['qc_id'],
                    'product_id':items['product_id'],
                    'product':items['product'],
                    'location':items['location'],
                    'quantity':items['quantity'],
                    'remarks':items['remarks'],
                    'po_number':items['po_number'],
                })
        else:
            new_line=[]

        if ean_number != 'False':
            qty = 1
            if 'Prod' not in ean_number:
                
                product_env = self.env['product.product']
                product_obj = product_env.search(['|','|','|','|',('ean13', '=', str(ean_number)),('gift_ean', '=', str(ean_number)),('box_ean', '=', str(ean_number)),('case_ean', '=', str(ean_number)),('pallet_ean', '=', str(ean_number))])
                if not product_obj.id:
                    raise Warning(_('Product Not found.Please add EAN number in Product Configuration. Or contact with Your department head'))



                # if str(product_obj.gift_ean) == str(ean_number):
                #     qty = 1
                # elif str(product_obj.box_ean) == str(ean_number):
                #     qty = product_obj.holding_box_qty
                # elif str(product_obj.case_ean) == str(ean_number):
                #     qty = product_obj.holding_case_qty
                # elif str(product_obj.pallet_ean) == str(ean_number):
                #     qty = product_obj.holding_pallet_qty
            else:
                get_list = ean_number.split('Prod')
                product_id = int(get_list[1])

                product_env = self.env['product.product']
                product_obj = product_env.search([('id', '=', product_id)])

                if product_obj.id:
                    qty=1
                else:
                    raise Warning(_('Product Not found.Please scan properly with proper product'))




            same_location_product_found = False

            for items in self.putaway_line_view:
                if items['product_id'] == product_obj.id:
                    items['location'] = location


                    # items['quantity'] +=qty
            #         same_location_product_found=True
            #         break
            # if same_location_product_found == False:
            #     if product_obj.id:
            #
            #
            #         new_line.append({
            #             'product_id': product_obj.id,
            #             'product': product_obj.name,
            #             'location': self.location,
            #             'quantity': qty,
            #             'remarks': '',
            #             'po_number': '',
            #         })
            # self.putaway_line_view = new_line
        self.product_scan = ""


        return 'Cool'


    @api.onchange('scan')
    def put_away_barcode_onchange(self):

        qc_number = str(self.scan)
        qc_ids=[]


        if self.putaway_line_view:
            new_putaway_line=[]
            for items in self.putaway_line_view:
                qc_ids.append(items['qc_id'])
                new_putaway_line.append({

                    'qc_id':items['qc_id'],
                    'product_id':items['product_id'],
                    'product':items['product'],
                    'location':items['location'],
                    'quantity':items['quantity'],
                    'remarks':items['remarks'],
                    'po_number':items['po_number'],
                })
        else:
            new_putaway_line = []

        if qc_number != 'False':
            qc_number = str(qc_number.upper())
            mod_obj = self.env['accepted.quality.control']
            model_data_ids = mod_obj.search([('aqc_number', '=', str(qc_number))])

            for items in model_data_ids:
                if not items.assigned:
                    for qc_items in items.aqc_line:

                        # Assign Product Location By Default

                        p_loc =''
                        if qc_items.product_id.x_products_location:
                            p_loc=qc_items.product_id.x_products_location

                        if qc_items.aqc_id not in qc_ids:

                            new_putaway_line.append({
                                'qc_id': qc_items.aqc_id,
                                'product_id': qc_items.product_id,
                                'product': qc_items.product,
                                'po_number': items.po_number,
                                'location': p_loc,
                                'remarks': '',
                                'quantity': qc_items.accepted_quantity,
                            })
                    self.putaway_line_view = new_putaway_line
            self.scan = ""



        return "xXxXxXxXxX"

    ## This code has ben commented due to write is not working.
    # def write(self, cr, uid, ids, vals, context=None):
    #     
    #     res={}
    #     test=None
    #     if test is None:
    # 
    #         raise osv.except_osv(_('Edit Restricted'),
    #                              _(
    #                                  'You can not edit. If you made mistake. Then cancel it and create new put away'))
    # 
    #     res = super(putaway_process, self).write(cr, uid, ids, vals, context=context)
    # 
    #     return res


    def create(self, cr, uid, vals, context=None):
        # Generate PUT AWAY Number



        # Get All data which are accepted in AQC Line

        # cr.execute("select id,aqc_id,product_id,remaining_quantity,accepted_quantity from accepted_quality_control_line WHERE state='confirm' and remaining_quantity > 0")
        #
        # get_all_pending_aqc_list = cr.fetchall()
        # submitted_put_line_value = vals.get('putaway_line_view')

        # message = self.check_total_qty_put_line(get_all_pending_aqc_list,submitted_put_line_value)
        # if message == 1:
        #     raise Warning(_('Quantity Is Greater Than Accepeted Quanitity'))
        # elif message == 2:
        #     raise Warning(_('Product you have scanned is still in the process bucket / pending state.'))

        qc_ids=[]

        # Warehouse ID

        record = super(putaway_process, self).create(cr, uid, vals, context=context)


        put_number = "PUT0" + str(record)

        cr.execute("update putaway_process set name=%s where id=%s", ([put_number,record]))
        cr.commit()

        # if record:
        #     cr.execute(
        #         "select id,product_id,quantity,location,qc_id from  putaway_process_details_view where putaway_line_ids = %s",
        #         ([record]))
        #     get_all_put_line_list = cr.fetchall()
        #     new_get_all_pending_aqc_list = [list(item) for item in get_all_pending_aqc_list]
        #     data_pool = self.pool.get('relation.put.line.to.aqc.line')
        #     for items in get_all_put_line_list:
        #         product_dir_id = data_pool.create(cr, uid, {
        #             'put_line_id': items[0],
        #             'aqc_line_id': items[4],
        #             'assigned_qty': items[2],
        #             'location': items[3],
        #             'product_id': items[1]
        #         })
        #         qty=0
        #         # cr.execute("update putaway_process_details_view set qc_id=%s where id=%s", (aq_item[0], items[0]))
        #         for aq_items in new_get_all_pending_aqc_list:
        #             qty=0
        #             cr.execute("update accepted_quality_control_line set remaining_quantity=%s where id=%s",
        #                        (qty, aq_items[0]))
        #             cr.commit()

        # Following condition has been commented due to reduce the allocation of aqc data. From now (21-01-19 with gauroV) it will be auto

        # if record:
        #     cr.execute("select id,product_id,quantity,location from  putaway_process_details_view where putaway_line_ids = %s",([record]))
        #     get_all_put_line_list = cr.fetchall()
        #     new_get_all_pending_aqc_list =[list(item) for item in get_all_pending_aqc_list]
        #     data_pool = self.pool.get('relation.put.line.to.aqc.line')
        #
        #
        #     for items in get_all_put_line_list:
        #         qty = items[2]
        #         for aq_item in new_get_all_pending_aqc_list:
        #             if aq_item[3] >0:
        #                 if qty ==0:
        #                     break
        #                 elif items[1] == aq_item[2] and qty == aq_item[3]:
        #                     assigned_qty = qty
        #                     qty=0
        #                     aq_item[3]=0
        #                     product_dir_id = data_pool.create(cr, uid, {
        #                         'put_line_id': items[0],
        #                         'aqc_line_id': aq_item[0],
        #                         'assigned_qty': assigned_qty,
        #                         'location': items[3],
        #                         'product_id': items[1]
        #                     })
        #
        #
        #
        #                     cr.execute("update putaway_process_details_view set qc_id=%s where id=%s", (aq_item[0],items[0]))
        #                     cr.execute("update accepted_quality_control_line set remaining_quantity=%s where id=%s", (qty,aq_item[0]))
        #                     cr.commit()
        #
        #                     break
        #                 elif items[1] == aq_item[2] and qty > aq_item[3]:
        #                     assigned_qty = aq_item[3]
        #                     qty = qty-aq_item[3]
        #                     remaining_qty=0
        #                     aq_item[3] = 0
        #                     product_dir_id = data_pool.create(cr, uid, {
        #                         'put_line_id': items[0],
        #                         'aqc_line_id': aq_item[0],
        #                         'assigned_qty': assigned_qty,
        #                         'location': items[3],
        #                         'product_id': items[1]
        #                     })
        #
        #                     cr.execute("update putaway_process_details_view set qc_id=%s where id=%s",
        #                                (aq_item[0], items[0]))
        #                     cr.execute("update accepted_quality_control_line set remaining_quantity=%s where id=%s",
        #                                (remaining_qty, aq_item[0]))
        #                     cr.commit()
        #
        #                 elif items[1] == aq_item[2] and qty < aq_item[3]:
        #                     assigned_qty = qty
        #                     remaining_qty=aq_item[3] - qty
        #                     aq_item[3] = remaining_qty
        #                     qty = 0
        #                     product_dir_id = data_pool.create(cr, uid, {
        #                         'put_line_id': items[0],
        #                         'aqc_line_id': aq_item[0],
        #                         'assigned_qty': assigned_qty,
        #                         'location': items[3],
        #                         'product_id': items[1]
        #                     })
        #
        #                     cr.execute("update putaway_process_details_view set qc_id=%s where id=%s",
        #                                (aq_item[0], items[0]))
        #                     cr.execute("update accepted_quality_control_line set remaining_quantity=%s where id=%s",
        #                                (remaining_qty, aq_item[0]))
        #                     cr.commit()
        #

        putaway_obj = self.browse(cr, uid, [record], context=context)
        self.pool.get('inb.end.to.end').putaway_data_create(cr, uid, str(putaway_obj.aqc_id.po_number), putaway_obj.aqc_id.po_id, record, put_number, putaway_obj.create_date, context=context)

        # for qc_id in qc_ids:
        #
        #     cr.execute("update accepted_quality_control set assigned=TRUE where id=%s", ([qc_id]))
        #     cr.commit()

        return record



class putaway_process_details(osv.osv):
    _name = 'putaway.process.details'

    _columns = {
        'putaway_line_ids': fields.many2one('putaway.process', 'Put Away Process ID', required=True,
                                                    ondelete='cascade', select=True, readonly=True),
        'qc_id': fields.many2one('accepted.quality.control', string='AQC ID'),
        'product_id': fields.integer('Product'),
        'location': fields.char('Location'),
        'product': fields.char('Product'),
        'remarks': fields.char('Remarks'),
        'po_number': fields.char('PO Number'),
        'quantity': fields.float('Quantity'),
    }

class putaway_process_details_view(osv.osv):
    _name = 'putaway.process.details.view'

    _columns = {
        'putaway_line_ids': fields.many2one('putaway.process', 'Put Away Process ID', required=True,
                                                    ondelete='cascade', select=True, readonly=True),
        'product_id': fields.integer('Product',required=True),
        'qc_id': fields.integer('AQC ID'),
        'location': fields.char('Location'),
        'product': fields.char('Product'),
        'remarks': fields.char('Remarks'),
        'po_number': fields.char('PO Number'),
        'quantity': fields.float('Quantity',required=True),


    }




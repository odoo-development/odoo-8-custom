# Mufti muntasir Ahmed

from openerp.osv import osv, fields, expression

class StockPicking(osv.osv):
    _inherit = 'stock.picking'

    def _get_picking_creation_time(self, cr, user, ids, name, arg, context=None):


        res = {}
        for id in ids:
            res[id]=''

        cr.execute('select picking_list.date_confirm, picking_list_line.stock_id, picking_list_line.id from picking_list,picking_list_line where picking_list.id = picking_list_line.picking_id and picking_list_line.stock_id IN %s', (tuple(ids),))

        abc_id= cr.fetchall()


        for items in abc_id:
            if items[1] in ids:
                res[items[1]]= items[0]



        return res

    def _get_packing_creation_time(self, cr, user, ids, name, arg, context=None):
        res = {}
        for id in ids:
            res[id]=''


        cr.execute('select packing_list.date_confirm, packing_list_line.stock_id, packing_list_line.id from packing_list,packing_list_line where packing_list.id = packing_list_line.packing_id and packing_list_line.stock_id IN %s', (tuple(ids),))
        abc_id= cr.fetchall()
        for items in abc_id:
            if items[1] in ids:
                res[items[1]]= items[0]



        return res

    _columns = {

    'picking_time': fields.function(_get_picking_creation_time, string='Picking Date', type='char'),
    'packing_time': fields.function(_get_packing_creation_time, string='Packing Date', type='char'),

    }



class account_invoice(osv.osv):
    _inherit ='account.invoice'

    def _get_dispatch_date_from_manifest(self, cr, user, ids, name, arg, context=None):


        res = {}
        for id in ids:
            res[id]=''

        cr.execute('select wms_manifest_process.confirm_time, wms_manifest_line.invoice_id from wms_manifest_process,wms_manifest_line where wms_manifest_process.id = wms_manifest_line.wms_manifest_id and wms_manifest_line.invoice_id IN %s', (tuple(ids),))

        abc_id= cr.fetchall()


        for items in abc_id:
            if items[1] in ids:
                res[items[1]]= items[0]



        return res


    _columns ={
        'dispatch_date': fields.function(_get_dispatch_date_from_manifest, string='Dispatch Date', type='char'),


    }

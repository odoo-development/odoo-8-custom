# Author Mufti Muntasir Ahmed 06-01-2019


import xlwt
from datetime import datetime
from openerp.osv import orm
from openerp.report import report_sxw
from openerp.addons.report_xls.report_xls import report_xls
from openerp.addons.report_xls.utils import rowcol_to_cell, _render
from openerp.tools.translate import translate, _
import logging
_logger = logging.getLogger(__name__)


_ir_translation_name = 'kpi.stock.xls'


class kpi_stock_xls_parser(report_sxw.rml_parse):

    def __init__(self, cr, uid, name, context):
        super(kpi_stock_xls_parser, self).__init__(
            cr, uid, name, context=context)
        move_obj = self.pool.get('stock.picking')
        self.context = context
        wanted_list = move_obj._report_xls_fields(cr, uid, context)
        template_changes = move_obj._report_xls_template(cr, uid, context)
        self.localcontext.update({
            'datetime': datetime,
            'wanted_list': wanted_list,
            'template_changes': template_changes,
            '_': self._,
        })

    def _(self, src):
        lang = self.context.get('lang', 'en_US')
        return translate(self.cr, _ir_translation_name, 'report', lang, src) \
            or src


class kpi_stock_xls(report_xls):

    def __init__(self, name, table, rml=False, parser=False, header=True,
                 store=False):
        super(kpi_stock_xls, self).__init__(
            name, table, rml, parser, header, store)

        # Cell Styles
        _xs = self.xls_styles
        # header
        rh_cell_format = _xs['bold'] + _xs['fill'] + _xs['borders_all']
        self.rh_cell_style = xlwt.easyxf(rh_cell_format)
        self.rh_cell_style_center = xlwt.easyxf(rh_cell_format + _xs['center'])
        self.rh_cell_style_right = xlwt.easyxf(rh_cell_format + _xs['right'])
        # lines
        aml_cell_format = _xs['borders_all']
        self.aml_cell_style = xlwt.easyxf(aml_cell_format)
        self.aml_cell_style_center = xlwt.easyxf(
            aml_cell_format + _xs['center'])
        self.aml_cell_style_date = xlwt.easyxf(
            aml_cell_format + _xs['left'],
            num_format_str=report_xls.date_format)
        self.aml_cell_style_decimal = xlwt.easyxf(
            aml_cell_format + _xs['right'],
            num_format_str=report_xls.decimal_format)
        # totals
        rt_cell_format = _xs['bold'] + _xs['fill'] + _xs['borders_all']
        self.rt_cell_style = xlwt.easyxf(rt_cell_format)
        self.rt_cell_style_right = xlwt.easyxf(rt_cell_format + _xs['right'])
        self.rt_cell_style_decimal = xlwt.easyxf(
            rt_cell_format + _xs['right'],
            num_format_str=report_xls.decimal_format)

        # XLS Template
        self.col_specs_template = {

            'mag_no': {
                'header': [1, 20, 'text', _render("_('Order NO')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'reference': {
                'header': [1, 20, 'text', _render("_('Reference')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'reservation_time': {
                'header': [1, 20, 'text', _render("_('Reservation Time')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'picking_creation': {
                'header': [1, 20, 'text', _render("_('Picking Creation Time')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'picking_confirmation': {
                'header': [1, 20, 'text', _render("_('Picking Confirmation Time')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'packing_creation': {
                'header': [1, 20, 'text', _render("_('Packing Creation Time')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'packing_confirmation': {
                'header': [1, 20, 'text', _render("_('Packing Confirmation Time')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'invoice_generation': {
                'header': [1, 20, 'text', _render("_('Invoice Creation')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'manifest_creation': {
                'header': [1, 20, 'text', _render("_('Manifest Creation Time')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'manifest_confirmation': {
                'header': [1, 20, 'text', _render("_('Manifest Confirmation Time')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'delivery_time': {
                'header': [1, 20, 'text', _render("_('Delivery Time')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},




        }



    def generate_xls_report(self, _p, _xs, data, objects, wb):

        wanted_list = _p.wanted_list
        self.col_specs_template.update(_p.template_changes)
        _ = _p._

        # report_name = objects[0]._description or objects[0]._name
        report_name = _("Delivery KPI Report ")
        ws = wb.add_sheet(report_name[:31])
        ws.panes_frozen = True
        ws.remove_splits = True
        ws.portrait = 0  # Landscape
        ws.fit_width_to_pages = 1
        row_pos = 0

        # set print header/footer
        ws.header_str = self.xls_headers['standard']
        ws.footer_str = self.xls_footers['standard']

        # Title
        cell_style = xlwt.easyxf(_xs['xls_title'])
        c_specs = [
            ('report_name', 4, 3, 'text', report_name),
        ]
        row_data = self.xls_row_template(c_specs, ['report_name'])
        row_pos = self.xls_write_row(
            ws, row_pos, row_data, row_style=cell_style)
        row_pos += 1

        # Column headers
        c_specs = map(lambda x: self.render(
            x, self.col_specs_template, 'header', render_space={'_': _p._}),
            wanted_list)
        row_data = self.xls_row_template(c_specs, [x[0] for x in c_specs])
        row_pos = self.xls_write_row(
            ws, row_pos, row_data, row_style=self.rh_cell_style,
            set_column_size=True)
        ws.set_horz_split_pos(row_pos)


        # select picking_list.id, picking_list.create_date,picking_list.date_confirm
        # from picking_list_line, picking_list
        # where picking_list_line.picking_id=picking_list.id and picking_list_line.stock_id=2367

        # Stock Order lines
        for line in objects:

            self.cr.execute(
                'select picking_list.id, picking_list.create_date,picking_list.date_confirm from picking_list_line, picking_list'
                ' where picking_list_line.picking_id=picking_list.id and picking_list_line.stock_id=%s',
                (tuple(line.id),))
            res = self.cr.fetchall()
            c_specs = map(
                lambda x: self.render(x, self.col_specs_template, 'lines'),
                wanted_list)
            for list_data in c_specs:

                if str(list_data[0]) == str('reservation_time'):
                    list_data[4] = str('Mufti')



            row_data = self.xls_row_template(c_specs, [x[0] for x in c_specs])
            row_pos = self.xls_write_row(
                ws, row_pos, row_data, row_style=self.aml_cell_style)






kpi_stock_xls('report.kpi.stock.xls',
                    'stock.picking',
                    parser=kpi_stock_xls_parser)

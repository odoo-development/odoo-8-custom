
{
    'name': 'Delivered KPI',
    'version': '2.0.0.0',
    'summary': """Quantity Delivered and Invoiced on Sale Line
    """,
    'author': 'Mufti Muntasir Ahmed',

    'website': 'http://www.sindabad.com',
    'depends': [
       'stock'
    ],
    'data': [

    ],
}
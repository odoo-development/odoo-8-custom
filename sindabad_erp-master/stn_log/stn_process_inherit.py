from datetime import datetime, timedelta
import time
from datetime import date
from openerp import SUPERUSER_ID
from openerp.osv import fields, osv
from openerp.tools.translate import _
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT
import openerp.addons.decimal_precision as dp
from openerp import workflow
from openerp import api
import datetime
from time import gmtime, strftime
from openerp.exceptions import except_orm, Warning, RedirectWarning


class stn_process(osv.osv):
    _inherit = 'stn.process'


    def send_stn(self, cr, uid, ids, context=None):

        new_id = super(stn_process, self).send_stn(cr, uid, ids, context=None)

        self.message_post(cr, uid, ids, body=_("STN Confirmed"), context=context)

        return new_id





    def cancel_stn(self, cr, uid, ids, context=None):

        new_id = super(stn_process, self).cancel_stn(cr, uid, ids, context=None)

        self.message_post(cr, uid, ids, body=_("STN Cancelled"), context=context)

        return new_id





    def receive_stn(self, cr, uid, ids, context=None):

        new_id = super(stn_process, self).receive_stn(cr, uid, ids, context=None)

        self.message_post(cr, uid, ids, body=_("STN Received"), context=context)

        return new_id





    def request_stn(self, cr, uid, ids, context=None):

        new_id = super(stn_process, self).request_stn(cr, uid, ids, context=None)

        self.message_post(cr, uid, ids, body=_("STN Requested"), context=context)

        return new_id







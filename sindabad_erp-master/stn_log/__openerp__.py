{
    'name': 'STN Log Entry',
    'version': '1.0',
    'category': 'wms',
    'author': 'Md. Rockibul Alam',
    'summary': 'Stock Transfer Note Process Start to End Log Entry',
    'description': 'Stock Transfer Note Process Start to End Log Entry',
    'depends': ['stn_process'],
    'data': [
        'stn_inherit_view.xml'
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}

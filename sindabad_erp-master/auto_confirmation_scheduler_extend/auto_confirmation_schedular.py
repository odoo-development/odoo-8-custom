from datetime import datetime, timedelta
import time
from datetime import date
from openerp import SUPERUSER_ID
from openerp.osv import fields, osv
from openerp.tools.translate import _
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT
import openerp.addons.decimal_precision as dp
from openerp import workflow
from openerp import api
from ..base.res.res_partner import format_address
from openerp.exceptions import Warning, except_orm
import dateutil.parser



class auto_confirmation(osv.osv):
    _name = "auto.confirmation"

    _columns = {
        'name': fields.char('Name'),

    }



    def auto_confirmation(self, cr, uid, context=None):
        SUPERUSER_ID=1
        sales_order = self.pool['sale.order']
        sale_ids = sales_order.search(cr, uid, [('state', '=', 'draft')],limit=50)

        sale_obj = sales_order.browse(cr, uid, sale_ids, context=context)



        for so_items in sale_obj:
            try:

                classification=so_items.customer_classification
                order_amount=so_items.amount_total
                payment_type=so_items.note
                order_id=so_items.id
                next_date = dateutil.parser.parse(so_items.create_date).date() + timedelta(days=1)
                zone=so_items.so_inside_outside
                magento_amount = so_items.x_sin_total_magento
                gap_amount = round((magento_amount - order_amount), 2)

                if 'Retail' in classification or 'SME' in classification or 'SOHO' in classification or 'HORECA' in classification:
                    if 'Cash' in payment_type:
                        if order_amount > 900:
                            if gap_amount != 0:
                                self.pool['sale.order'].order_re_sync(cr, SUPERUSER_ID, [order_id],context=context)
                            self.pool['sale.order'].action_button_confirm(cr, SUPERUSER_ID, [order_id],context=context)
                            update_so_query = "UPDATE sale_order SET magetno_delivery_at='{0}' WHERE id='{1}'".format(next_date, str(order_id))
                            cr.execute(update_so_query)
                            cr.commit()

                        ## Call Order confirmation
                if 'General' in classification or classification == '' or classification is None:
                    if zone == 'inside' and 'Cash' in payment_type:
                        if order_amount <10000:
                            if gap_amount != 0:
                                self.pool['sale.order'].order_re_sync(cr, SUPERUSER_ID, [order_id], context=context)
                            self.pool['sale.order'].action_button_confirm(cr, SUPERUSER_ID, [order_id],context=context)
                            update_so_query = "UPDATE sale_order SET magetno_delivery_at='{0}' WHERE id='{1}'".format(next_date,str(order_id))
                            cr.execute(update_so_query)
                            cr.commit()
            except:
                pass






        return True





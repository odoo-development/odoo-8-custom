{
    'name': 'Order Auto Confirmation Process Scheduler',
    'version': '1.0',
    'category': 'Order Auto Confirmation Process Scheduler',
    'author': 'Mufti',
    'summary': 'Order Auto Confirmation Process Scheduler',
    'description': 'Order Approval Process',
    'depends': ['base', 'sale', 'stock'],
    'data': [

    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}

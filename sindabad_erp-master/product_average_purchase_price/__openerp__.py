# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2009 Tiny SPRL (<http://tiny.be>).
#

{
    'name': 'Product Purchase Average Price',
    'version': '0.2',
    'author': 'Mufti Muntasir Ahmed',
    'website': 'http://www.openerp.com',
    'sequence':130,
    'category': 'Project Management',
    'depends': [
                'product',
                'purchase',
                ],
    'description': """
        This module adds purchase average price on product template form and tree view
        
        Features:
        It computes average price by searching prices in purchaes order lines in all states
    """,
    'demo': [],
    'test': [
    ],
    'data': [
       'product_view.xml',
    ],
    'installable': True,
    'auto_install': False,
}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:

# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.osv import osv, fields

class product_template(osv.osv):
    _inherit = 'product.product'
    
    def _get_average_purchase_price(self, cr, uid, ids, name, arg, context=None):
        pol_obj = self.pool.get('purchase.order.line')
        res = {}

        for product_id in ids:

            # get all purchase order lines with current product product_id
            res[product_id] = qty = subtot = others = others_qty = 0.0


            line_ids = pol_obj.search(cr, uid, [('product_id', '=', product_id)],context=context)

            for line in pol_obj.browse(cr, uid, line_ids, context):
                if line.state == 'confirmed' or line.state == 'done':
                    qty += line.product_qty
                    subtot += line.price_subtotal
                else:
                    others_qty += line.product_qty
                    others += line.price_subtotal

                try:
                    res[product_id] = subtot / qty
                except:
                    if (qty + others_qty) > 0:
                        res[product_id] = (subtot + others) / (qty + others_qty)




        return res 
    
    _columns = {
                'average_purchaes_price' : fields.function(_get_average_purchase_price, string= 'Average Purchase Price', type ='float')
                }
    
    
    
   
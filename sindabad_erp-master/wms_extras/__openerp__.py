{
    'name': 'WMS Extras',
    'version': '1.0',
    'category': 'WMS Extras',
    'author': 'Shahjalal',
    'summary': 'WMS Extras',
    'description': 'WMS Extras',
    'depends': ['base', 'wms_menulist'],

    'installable': True,
    'application': True,
    'auto_install': False,
}

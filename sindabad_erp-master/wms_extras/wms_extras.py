from datetime import date, datetime
from dateutil import relativedelta
import json
import time

from openerp.osv import fields, osv
from openerp.tools.float_utils import float_compare, float_round
from openerp.tools.translate import _
from openerp.tools import DEFAULT_SERVER_DATETIME_FORMAT, DEFAULT_SERVER_DATE_FORMAT
from openerp.exceptions import Warning
from openerp import SUPERUSER_ID, api
import openerp.addons.decimal_precision as dp
from openerp.addons.procurement import procurement
import logging


class stock_picking(osv.osv):
    _inherit = 'stock.picking'



    def write(self, cr, uid, ids, vals, context=None):

        return super(stock_picking, self).write(cr, uid, ids, vals, context=context)

    def create(self, cr, uid, vals, context=None):

        classification=''
        area=''
        new_customer = False
        try:

            order_origin = vals.get('origin')
            if str(order_origin).startswith("SO"):
                so_obj = self.pool.get('sale.order')

                so_list = so_obj.search(cr, uid, [('name', '=', str(order_origin))])
                so = so_obj.browse(cr, uid, so_list)
                area = str(so.partner_shipping_id.state_id.name)
                classification = str(so.customer_classification)
                new_customer = so.new_customer
        except:
            pass

        vals['area']=area
        vals['classification']=classification
        vals['x_priority_customer']= new_customer
        return super(stock_picking, self).create(cr, uid, vals, context)

    def _get_customer_classification(self, cr, uid, ids, field_name, arg, context=None):
        res = {}
        for pick in self.browse(cr, uid, ids, context=context):

            res[pick.id] = False
            if str(pick.origin).startswith("SO"):
                so_obj = self.pool.get('sale.order')

                so_list = so_obj.search(cr, uid, [('name', '=', str(pick.origin))])
                so = so_obj.browse(cr, uid, so_list)

                parent = so.partner_id

                for i in range(5):
                    if len(parent.parent_id) == 1:
                        parent = parent.parent_id
                    else:
                        classification = parent.x_classification
                        break

                if classification:
                    res[pick.id] = str(classification)
        return res

    def _get_customer_area(self, cr, uid, ids, field_name, arg, context=None):
        res = {}
        for pick in self.browse(cr, uid, ids, context=context):
            res[pick.id] = False
            if pick.partner_id.state_id:
                res[pick.id] = str(pick.partner_id.state_id.name)
        return res

    _columns = {
        'classification': fields.char('Classification'),
        'area': fields.char('Area'),
    }

import time
from lxml import etree

from openerp.osv import fields, osv
import openerp.addons.decimal_precision as dp
from openerp.tools.translate import _
from openerp.tools import float_compare
from openerp.report import report_sxw
import openerp
import time, datetime, json
from openerp.exceptions import except_orm, Warning, RedirectWarning
from ..odoo_to_magento_api_connect.api_connect import get_magento_token, submit_request


class account_voucher(osv.osv):

    _inherit = "account.voucher"

    def button_proforma_voucher(self, cr, uid, ids, context=None):

        payment = super(account_voucher, self).button_proforma_voucher(cr, uid, ids, context)

        magento_no = str(context['default_reference'])
        invoice_obj = self.pool.get('account.invoice')
        inv_list = invoice_obj.search(cr, uid, [('name', '=', magento_no)])
        invs = invoice_obj.browse(cr, uid, inv_list, context=context)

        so_obj = self.pool.get('sale.order')
        so_list = so_obj.search(cr, uid, [('client_order_ref', '=', magento_no)], context=context)
        if so_list:
            so = so_obj.browse(cr, uid, [so_list[0]], context=context)
            # so.amount_total
            # so.state

            inv_states = list()
            total_inv_amount = 0

            for inv in invs:
                total_inv_amount += inv.amount_total
                inv_states.append(str(inv.state))

            so_obj = self.pool.get('sale.order')
            so_ids = so_obj.search(cr, uid, [('client_order_ref', '=', magento_no)], context=context)

            order_status = ''
            if str(so.state) == "done" and len(invs) != inv_states.count('paid'):
                order_status = "delivered"

                if len(order_status) > 0:
                    self.sync_order_status(cr, uid, magento_no, order_status, so_ids, context=context)

            # paid
            if len(invs) == inv_states.count('paid') and str(so.state) == "done" and so.amount_total == total_inv_amount:
                # sync order status 'complete'
                order_status = 'complete'

                if len(order_status) > 0:
                    self.sync_order_status(cr, uid, magento_no, order_status, so_ids, context=context)

        return payment

    def sync_order_status(self, cr, uid, magento_no, order_status, so_ids, context=None):

        # get token
        token = None
        url_root = None
        if so_ids:
            # sale_order_ids = [13673, 13672, 13671, 13670, 13669, 13668]
            try:
                token, url_root = get_magento_token(self, cr, uid, so_ids)
            except:
                token = None
                url_root = None

        if token:
            url = url_root + "/index.php/rest/V1/odoomagentoconnect/OrderStatus"
            map_order_data = {'orderId': magento_no, 'status': order_status}
            statusData = dict()
            statusData['statusData'] = map_order_data
            data = json.dumps(statusData)

            resp = submit_request(url, token, data)
        """
        else:
            raise Warning(
                _('Order has been paid. But due to network problem Order Status has not been synced with Magento!!!'))
        """

        return True

class sale_order(osv.osv):
    _inherit = "sale.order"

    def copy(self, cr, uid, id, default=None, context=None):
        raise osv.except_osv(_('Duplicate is Forbbiden'), _('Duplicate order creation is restricted. Please create new order from Magento.'))

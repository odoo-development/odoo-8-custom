{
    'name': 'Order Status Sync Fix',
    'version': '1.0',
    'category': 'Order Status Sync Fix',
    'author': 'Shahjalal Hossain',
    'summary': 'Order Status Sync Fix',
    'description': 'Order Status Sync Fix',
    'depends': ['base', 'odoo_magento_connect', 'sale', 'stock', 'wms_manifest', 'order_status_sync', 'odoo_to_magento_api_connect', 'account'],
    'installable': True,
    'application': True,
    'auto_install': False,
}

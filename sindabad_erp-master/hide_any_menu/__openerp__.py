{
    'name': 'Hide Any Menu',
    'version': '1.0',
    'category': 'Any',
    'author': 'Rocky',
    'summary': 'Hide Any Menu',
    'description': 'Hide Any Menu',
    'depends': [],
    'data': [
        'hide_any_menu.xml',
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}

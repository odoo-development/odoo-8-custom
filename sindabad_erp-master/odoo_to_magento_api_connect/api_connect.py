import re
import xmlrpclib
import json
import requests
import logging
from openerp.osv import fields, osv
from openerp.tools.translate import _
from openerp import SUPERUSER_ID, api
from openerp.addons.base.res.res_partner import _lang_get
from openerp.exceptions import Warning


TOKEN_API = '/integration/admin/token'

# passes magento.configure object
def get_magento_token(self_obj, cr, uid, order_ids):
    # order_ids = [13673, 13672, 13671, 13670, 13669, 13668]
    instance_id = 1

    if order_ids:
        for mag_ord_id in self_obj.pool.get('magento.orders').search(cr, uid, [('order_ref', '=', order_ids[0])]):
            for mag_obj in self_obj.pool.get('magento.orders').browse(cr, uid, [mag_ord_id]):
                instance_id = mag_obj.instance_id.id

    obj = self_obj.pool.get('magento.configure').browse(cr, uid, [instance_id])

    """
    obj
    magento.configure(1, )
    """
    url = obj.name + "/index.php/rest/V1" + TOKEN_API
    user = obj.user
    pwd = obj.pwd

    cred_dict = {"username": user, "password": pwd}
    cred = json.dumps(cred_dict)

    header = {"Content-type": "application/json"}
    res = requests.post(str(url), data=cred, headers=header)

    token = None
    if res.status_code == 200:
        token = "Bearer " + str(res.json())

    return token, obj.name


def submit_request(url, token, data, obj=None):

    token = token.replace('"', "")
    headers = {'Authorization': token, 'Content-Type': 'application/json'}

    res = requests.post(url, data=data, headers=headers)

    return res.status_code






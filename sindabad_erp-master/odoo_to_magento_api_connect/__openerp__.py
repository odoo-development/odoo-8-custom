{
    'name': 'Odoo To Magento API Connection',
    'version': '1.0',
    'category': 'Odoo To Magento API Connection',
    'author': 'Shahjalal Hossain',
    'summary': 'Odoo To Magento API Connection',
    'description': 'Odoo To Magento API Connection',
    'depends': ['base', 'odoo_magento_connect'],

    'installable': True,
    'application': True,
    'auto_install': False,
}

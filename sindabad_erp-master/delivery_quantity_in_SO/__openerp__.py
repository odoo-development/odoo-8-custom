
{
    'name': 'Delivered and Return Quantity In Sale Order',
    'version': '2.0.0.0',
    'summary': """Quantity Delivered and Invoiced on Sale Line
    """,
    'author': 'Mufti Muntasir Ahmed',

    'website': 'http://www.sindabad.com',
    'depends': [
        'odoo_magento_connect',
        'sale_stock'
    ],
    'data': [
        'views/sale_order.xml'
    ],
}
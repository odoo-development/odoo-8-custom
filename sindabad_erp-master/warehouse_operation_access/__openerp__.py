{
    'name': "Warehouse Access Control",

    'summary': """
         Warehouse and Stock Location Access on Users.""",

    'description': """
        This Module Restricts the User from Accessing Warehouse and Process Stock Moves other than allowed to Warehouses and Stock Locations.
    """,

    'author': "Md Rockibul Alam Babu",

    'category': 'Warehouse',
    'version': '0.1',

    'depends': ['base', 'stock', 'sale'],

    'data': [

        'users_view.xml',
        'security/security.xml',
    ],
}
from openerp import models, fields, api, _
from openerp.exceptions import Warning


class ResUsers(models.Model):
    _inherit = 'res.users'

    restrict_locations = fields.Boolean('Restrict Location')

    stock_location_ids = fields.Many2many(
        'stock.location',
        'location_security_stock_location_users',
        'user_id',
        'location_id',
        'Stock Locations')

    default_picking_type_ids = fields.Many2many(
        'stock.picking.type', 'stock_picking_type_users_rel',
        'user_id', 'picking_type_id', string='Default Warehouse Operations')

    warehouse_id = fields.Many2many(
        string='Warehouse',
        comodel_name='stock.warehouse',
        help="You can choose an existent warehouse or create a new from "
             "'More/Create warehouse' menu.")

    @api.multi
    def write(self, vals):
        ret = super(ResUsers, self).write(vals)

        if 'default_picking_type_ids' in vals:
            # clear default ir values when default_picking_type_ids changes
            self.env['ir.values'].get_defaults_dict.clear_cache(self.env['ir.values'])
        return ret

    @api.multi
    def write(self, vals):
        ret = super(ResUsers, self).write(vals)

        if 'warehouse_id' in vals:
            # clear default ir values when warehouse_id changes
            self.env['ir.values'].get_defaults_dict.clear_cache(self.env['ir.values'])
        return ret


class stock_move(models.Model):
    _inherit = 'stock.move'

    @api.one
    @api.constrains('state', 'location_id', 'location_dest_id')
    def check_user_location_rights(self):
        if self.state == 'draft':
            return True
        user_locations = self.env.user.stock_location_ids
        if self.env.user.restrict_locations:
            message = _(
                'Invalid Location. You cannot process this move since you do '
                'not control the location "%s". '
                'Please contact your Adminstrator.')
            if self.location_id not in user_locations:
                raise Warning(message % self.location_id.name)
            elif self.location_dest_id not in user_locations:
                raise Warning(message % self.location_dest_id.name)

from openerp.osv import fields, osv
from openerp.tools.translate import _
import openerp.addons.decimal_precision as dp


class purchase_order(osv.osv):
    _inherit = "stock.picking"

    def _mag_no(self, cr, uid, ids, field_name, arg, context):
        res = {}
        stock_objects_id = self.pool['stock.picking'].browse(cr, uid, ids, context=context)
        count=0

        for obj in stock_objects_id:
            res[obj.id] = ''

            if obj.origin is not False:
                if 'SO' in obj.origin:
                    try:
                        sale_ids = self.pool['sale.order'].search(
                            cr, uid, [('name', '=', obj.origin)], context=context)

                        obj_data = self.pool['sale.order'].browse(cr, uid, sale_ids, context=context)
                        res[obj.id] = obj_data.client_order_ref
                    except:
                        res[obj.id] = obj.origin

                else:
                    res[obj.id] = ''

        return res

    def _magento_number_search(self, cursor, user, obj, name, args, context=None):
        if not len(args):
            return []

        filtered_ids = [('id', '=', 0)]

        for tuple_items in args:
            searched_data = tuple_items[2]

            sale_ids = self.pool['sale.order'].search(
                cursor, user, [('client_order_ref', 'like', searched_data)], context=context)
            obj_data = self.pool['sale.order'].browse(cursor, user, sale_ids, context=context)

            order_list = [items.name for items in obj_data]

            picking_ids = self.pool['stock.picking'].search(
                cursor, user, [('origin', 'in', order_list)], context=context)
            filtered_ids = [('id', 'in', [ids for ids in picking_ids])]

        return filtered_ids

    def _shipment_search(self, cursor, user, obj, name, args, context=None):
        selection_dict = dict()
        selection_dict[2029] = '5 Star'
        selection_dict[2028] = 'Aramex'
        selection_dict[2051] = 'In House'
        selection_dict[2026] = 'S A Paribahan'
        selection_dict[2025] = 'Sundarban'
        selection_dict[3645] = 'GoFetch'
        selection_dict[3647] = 'eCourier'
        selection_dict[3700] = 'GoGo'
        selection_dict[3730] = 'Biddyut'
        selection_dict[3731] = 'Pathao'
        selection_dict[0] = 'Not Set'
        selection_dict[2027] = 'Not Yet Defined'

        if not len(args):
            return []

        filtered_ids = [('id', '=', 0)]
        shipment_ids = []

        for tuple_items in args:
            searched_data = tuple_items[2]
            for k, v in selection_dict.iteritems():
                if searched_data in v:
                    shipment_ids.append(k)

            sale_ids = self.pool['sale.order'].search(
                cursor, user, [('magetno_shipment_by', 'in', shipment_ids)], context=context)
            obj_data = self.pool['sale.order'].browse(cursor, user, sale_ids, context=context)

            order_list = [items.name for items in obj_data]

            picking_ids = self.pool['stock.picking'].search(
                cursor, user, [('origin', 'in', order_list)], context=context)
            filtered_ids = [('id', 'in', [ids for ids in picking_ids])]

        return filtered_ids

    def _shipping_name(self, cr, uid, ids, field_name, arg, context):
        res = {}
        stock_objects_id = self.pool['stock.picking'].browse(cr, uid, ids, context=context)
        selection_dict = dict()
        selection_dict[2029] = '5 Star'
        selection_dict[2028] = 'Aramex'
        selection_dict[2051] = 'In House'
        selection_dict[2026] = 'S A Paribahan'
        selection_dict[2025] = 'Sundarban'
        selection_dict[3645] = 'GoFetch'
        selection_dict[3647] = 'eCourier'
        selection_dict[3700] = 'GoGo'
        selection_dict[3730] = 'Biddyut'
        selection_dict[3731] = 'Pathao'
        selection_dict[0] = 'Not Set'
        selection_dict[2027] = 'Not Yet Defined'
        try:


            for obj in stock_objects_id:
                if 'SO' in obj.origin:
                    try:
                        sale_ids = self.pool['sale.order'].search(
                            cr, uid, [('name', '=', obj.origin)], context=context)

                        obj_data = self.pool['sale.order'].browse(cr, uid, sale_ids, context=context)

                        if int(obj_data.magetno_shipment_by) in selection_dict.keys():
                            res[obj.id] = selection_dict[int(obj_data.magetno_shipment_by)]
                    except:
                        res[obj.id] = ''

                else:
                    res[obj.id] = ''
        except:
            pass

        return res

    _columns = {

        'mag_no': fields.function(_mag_no, string='Magento Number', fnct_search=_magento_number_search, type='char'),
        'shipment_by': fields.function(_shipping_name, string='Shipment By', fnct_search=_shipment_search, type='char'),

    }

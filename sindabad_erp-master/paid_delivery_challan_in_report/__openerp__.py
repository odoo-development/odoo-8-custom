{
    "name": "Add Payment Image on Delivery Challan Report",
    "version": "1.0",
    "depends": [
        "base",
        "delivery_challan_report_print"
    ],
    "author": "Rocky",
    "category": "Sales",
    "description": """
Features
======================================================================

* Add payment image on Delivery Challan Report depending it's status.

""",
    "data": [
        "report/paid_delivery_challan_report_view.xml",
    ],
    "installable": True,
    "application": True,
    "auto_install": False,
}

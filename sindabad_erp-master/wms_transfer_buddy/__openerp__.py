{
    'name': 'WMS Transfer Buddy',
    'version': '1.0',
    'category': 'WMS Transfer Buddy',
    'author': 'Shahjalal',
    'summary': 'WMS, Inventory, Logistic, Storage',
    'description': 'WMS Transfer Buddy',
    'depends': ['stock'],
    'installable': True,
    'application': True,
    'auto_install': False,
}

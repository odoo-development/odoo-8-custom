from openerp import models, fields, api
from openerp.exceptions import Warning
from openerp.tools.translate import _
from openerp import tools
import openerp.addons.decimal_precision as dp
from datetime import datetime


class stock_transfer_details(models.TransientModel):
    _inherit = 'stock.transfer_details'

    @api.one
    def do_detailed_transfer(self):

        if self.picking_id.state not in ['assigned', 'partially_available']:
            raise Warning(_('You cannot transfer a picking in state \'%s\'.') % self.picking_id.state)

        processed_ids = []
        # Create new and update existing pack operations
        for lstits in [self.item_ids, self.packop_ids]:
            for prod in lstits:
                # import pdb;pdb.set_trace()
                pack_datas = {
                    'product_id': prod.product_id.id,
                    'product_uom_id': prod.product_uom_id.id,
                    'product_qty': prod.quantity,
                    'package_id': prod.package_id.id,
                    'lot_id': prod.lot_id.id,
                    'location_id': prod.sourceloc_id.id,
                    'location_dest_id': prod.destinationloc_id.id,
                    'result_package_id': prod.result_package_id.id,
                    'date': prod.date if prod.date else datetime.now(),
                    'owner_id': prod.owner_id.id,
                }
                if prod.packop_id:
                    prod.packop_id.with_context(no_recompute=True).write(pack_datas)
                    processed_ids.append(prod.packop_id.id)
                else:
                    pack_datas['picking_id'] = self.picking_id.id
                    packop_id = self.env['stock.pack.operation'].create(pack_datas)
                    processed_ids.append(packop_id.id)
        # Delete the others
        packops = self.env['stock.pack.operation'].search(
            ['&', ('picking_id', '=', self.picking_id.id), '!', ('id', 'in', processed_ids)])
        packops.unlink()

        # Execute the transfer of the picking
        self.picking_id.do_transfer()

        # Set information for transfer buddy
        try:

            picking_data = self.env['stock.picking'].search([('id', '=', self.picking_id.id)])
            po_number = str(picking_data.origin)  # po_number or record_name
            if po_number and po_number.startswith("PO"):
                user_data = self.env['res.users'].search([('id', '=', self._uid)])
                partner_data = self.env['res.partner'].search([('id', '=', user_data.partner_id.id)])
                mail_message = self.env['mail.message'].search(
                    ['&', ('record_name', '=', str(picking_data.origin)), ('body', 'like', '<span>RFQ Approved</span>')])

                message_id = tools.generate_tracking_message_id(str(mail_message.res_id) + '-' + 'purchase.order')

                if mail_message:

                    self.env.cr.execute("""
                                INSERT INTO mail_message 
                                  (create_date, write_date, write_uid, create_uid, parent_id, res_id, message_id, body, model, record_name, no_auto_thread, date, author_id, type, reply_to, email_from, website_published) 
                                VALUES 
                                  ((%s), (%s), (%s), (%s), (%s), (%s), (%s), (%s), (%s), (%s), (%s), (%s), (%s), (%s), (%s), (%s), (%s))""",
                                        (datetime.now(), datetime.now(), int(user_data.id), int(user_data.id),
                                         int(mail_message.parent_id), int(mail_message.res_id), str(message_id),
                                         '<p>Products received</p>', 'purchase.order', str(picking_data.origin), bool(False),
                                         datetime.now(), int(partner_data.commercial_partner_id.id), 'notification',
                                         str(partner_data.email), str(partner_data.email), bool(True)))
                    self.env.cr.commit()
        except:
            pass

        return True

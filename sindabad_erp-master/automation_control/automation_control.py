from datetime import datetime, timedelta
import time
from datetime import date
from openerp import SUPERUSER_ID
from openerp.osv import fields, osv
from openerp.tools.translate import _
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT
import openerp.addons.decimal_precision as dp
from openerp import workflow
from openerp import api
import datetime

import logging
_logger = logging.getLogger(__name__)


class automation_control(osv.osv):

    _name = "automation.control"

    _columns = {
        'model_name': fields.char('Model Name'),
        'method_name': fields.char('Method Name'),
        'running_date': fields.datetime('Running Date'),
        'max_call': fields.integer('Max call in a day'),
        'num_call': fields.integer('Number of call'),
    }

    def scheduler_action_logs(self, cr, uid, data, context=None):

        """
        data = {
            "model_name":
            "method_name":
            "max_call":
            "num_call":
         }
        """

        run_scheduler = True

        check_list = self.search(cr, uid, ['&', ("model_name", "=", data["model_name"]),
                                           ("method_name", "=", data["method_name"])])

        controls = check_list.browse(cr, uid, check_list, context=context)

        today_date = datetime.datetime.today().date().strftime('%Y-%m-%d')

        for control in controls:
            if control.running_date == today_date and control.max_call < data["max_call"]:
                run_scheduler = False

        return run_scheduler

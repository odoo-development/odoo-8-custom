{
    'name': 'Custom Automation Control',
    'version': '1.0',
    'category': 'Settings',
    'author': 'Shahjalal Hossain',
    'summary': 'Custom Automation Control',
    'description': 'Custom Automation Control',
    'depends': ['base', 'sale', 'purchase', 'account', 'odoo_magento_connect'],
    'data': [

        'security/automation_control_security.xml',
        'security/ir.model.access.csv',

        'views/automation_control_view.xml',

    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}

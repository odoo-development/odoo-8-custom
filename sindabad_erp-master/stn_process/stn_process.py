from datetime import datetime, timedelta
import time
from datetime import date
from openerp import SUPERUSER_ID
from openerp.osv import fields, osv
from openerp.tools.translate import _
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT
import openerp.addons.decimal_precision as dp
from openerp import workflow
from openerp import api
import datetime
from time import gmtime, strftime
from openerp.exceptions import except_orm, Warning, RedirectWarning


class stn_process(osv.osv):
    _name = "stn.process"
    _inherit = ['mail.thread', 'ir.needaction_mixin']
    _description = "STN Process"

    _columns = {
        'warehouse_id_from': fields.many2one('stock.warehouse', string='Requested From', required=True),
        'warehouse_id_to': fields.selection([
            (1, 'Central Warehouse Uttara'),
            (2, 'Nodda Warehouse'),
            (3, 'Nodda Retail Warehouse'),
            (4, 'Motijheel Warehouse'),
            (6, 'Uttara Retail Warehouse'),
            (7, 'Motijheel Retail Warehouse'),
            (8, 'Damage Warehouse'),
            (19, 'Comilla Warehouse'),
            (27, 'Badda Warehouse'),
            (21, 'Signboard Warehouse'),
            (34, 'Badda HORECA Warehouse'),
            (37, 'Savar Warehouse'),

        ], 'Requested To', readonly=True, copy=False, help="Gives the List of the Warehouses", select=True),
        'remark': fields.text('Remark'),
        'received': fields.boolean('Received'),
        'received_by': fields.many2one('res.users', 'Received By'),
        'received_time': fields.datetime('Received Time'),
        'send': fields.boolean('Send'),
        'send_by': fields.many2one('res.users', 'Sending By'),
        'send_time': fields.datetime('Sending Time'),
        'cancel': fields.boolean('Cancel'),
        'cancel_by': fields.many2one('res.users', 'Cancel By'),
        'cancel_time': fields.datetime('Cancel Time'),
        'requested': fields.boolean('Requested'),
        'requested_by': fields.many2one('res.users', 'Requested By'),
        'requested_time': fields.datetime('Requested Time'),
        'name': fields.char('STN Number'),
        'state': fields.selection([
            ('draft', 'Created'),
            ('requested', 'Requested'),
            ('send', 'Confirmed'),
            ('receive', 'Received'),
            ('cancel', 'Cancelled'),

        ], 'Status', readonly=True, copy=False, help="Gives the status of the STN Process", select=True),
        'stn_process_line': fields.one2many('stn.process.line', 'stn_process_id', 'STN Process Line', required=True),

        'cancel_stn_process_date': fields.datetime('Cancel STN Process Date'),
        'cancel_stn_process_by': fields.many2one('res.users', 'Cancel STN Process By'),
        'stock_id': fields.many2one('stock.picking', 'Stock Number'),
        'cancel_stn_process_reason': fields.text('Cancel STN Process Reason'),


    }

    _defaults = {
        'user_id': lambda obj, cr, uid, context: uid,
        'state': 'draft',
    }

    @api.model
    def create(self, vals):

        warehouse_code_dict= {1:'UttWH',2:'NODDA',3:'NODRT',4:'MTWH',6:'UTTRT',7:'MOTRT',8:'DamWH',19:'ComWH',
                              27:'Badda',21:'SINWH',34:'BdHoW',37:'SavWH'}

        if vals['warehouse_id_from'] == vals['warehouse_id_to']:
            raise osv.except_osv(_("Warning!!!"), _(
                "Source Location and Destination Location will be different. \n They will not be the same."))

        warehouse_id_to = vals['warehouse_id_to']

        for items in vals.get('stn_process_line'):

            values = dict()

            product_id = items[2]['product_id']

            prod_obj = self.pool.get('product.product')
            uid=1
            prod_search = prod_obj.search(self._cr, uid, [('id', '=', product_id)], context=self._context)
            prod_browse = self.pool.get('product.product').browse(self._cr, uid, prod_search, context=self._context)

            if warehouse_id_to == 1:  # Uttara
                items[2]['available_qty'] = prod_browse.uttwh_free_quantity
            elif warehouse_id_to == 2:  # Nodda
                items[2]['available_qty'] = prod_browse.nodda_free_quantity
            elif warehouse_id_to == 4:  # Motijeel
                items[2]['available_qty'] = prod_browse.moti_free_quantity
            elif warehouse_id_to == 3:  # Nodda Retail
                items[2]['available_qty'] = prod_browse.nodda_retail_free_quantity
            elif warehouse_id_to == 6:  # Uttara Retail
                items[2]['available_qty'] = prod_browse.uttwh_retail_free_quantity
            elif warehouse_id_to == 7:  # Motijeel Retail
                items[2]['available_qty'] = prod_browse.moti_retail_quantity
            elif warehouse_id_to == 19:  # Motijeel Retail
                items[2]['available_qty'] = prod_browse.comilla_free_quantity
            # elif warehouse_id_to ==8: # Damage
            #     values['available_qty'] = prod_browse.moti_free_quantity
            elif warehouse_id_to == 27:  # Badda
                items[2]['available_qty'] = prod_browse.Badda_free_quantity
            elif warehouse_id_to == 21:  # Signboard
                items[2]['available_qty'] = prod_browse.signboard_free_quantity

            elif warehouse_id_to == 34:  # Badda HORECA
                items[2]['available_qty'] = prod_browse.badda_horeca_free_quantity
            elif warehouse_id_to == 37:  # Savar
                items[2]['available_qty'] = prod_browse.saver_free_quantity

            if items[2].get('transfer_quantity') > items[2]['available_qty']:
                raise osv.except_osv(_("Warning!!!"), _(
                    "Requested/ Transferred Quantity can not greater than Free Quantity"))
            else:
                items[2]['requested_qty'] = items[2].get('transfer_quantity')

        record = super(stn_process, self).create(vals)

        stn_number = "STN/"+str(warehouse_code_dict.get(record.warehouse_id_to))+'/'+str(warehouse_code_dict.get(record.warehouse_id_from.id)) +'/0'+ str(record.id)
        record.name = stn_number

        return record

    # calls at the time of update record

    def write(self, cr, uid, ids, vals, context=None):




        if vals.get('warehouse_id_from') is not None and vals.get('warehouse_id_to') is not None:
            if vals.get('warehouse_id_from') == vals.get('warehouse_id_to'):
                raise osv.except_osv(_("Warning!!!"), _(
                    "Source Location and Destination Location will be different. \n They will not be the same."))


        # if vals.get('stn_process_line') is not None:
        #
        
        #
        #     for items in vals.get('stn_process_line'):
        #         if items[2].get('transfer_quantity') > items[2].get('available_qty'):
        #             raise osv.except_osv(_("Warning!!!"), _(
        #                 "Requested/ Transferred Quantity can not greater than Free Quantity"))

        record = super(stn_process, self).write(cr, uid, ids, vals, context=context)

        stn_info = self.browse(cr, uid, ids[0], context=context)

        if vals.get('stn_process_line') is not None and len(vals.get('stn_process_line')) > 0 and str(stn_info.state) != str('draft'):
            if stn_info.stock_id:
                #     # unreserve
                self.pool.get('stock.picking').do_unreserve(cr, uid, int(stn_info.stock_id.id), context=context)

                # Cancel Action
                self.pool.get('stock.picking').action_cancel(cr, uid, int(stn_info.stock_id.id), context)

            ## Generate Challan From Here
            id = ids[0] if len(ids) > 0 else None
            stn_data = self.browse(cr, uid, id, context=context)
            stock_picking_type_ids = self.pool['stock.picking.type'].search(cr, uid,
                                                                            [('name', 'like', 'Delivery Orders')],
                                                                            context=context)
            stock_picking_type_data = self.pool['stock.picking.type'].browse(cr, uid, stock_picking_type_ids,
                                                                             context=context)
            location_id = None
            location_dest_id = None
            picking_type_id = None

            for items in stock_picking_type_data:

                if items.warehouse_id.id == stn_data.warehouse_id_to:
                    location_id = items.default_location_src_id.id
                    picking_type_id = items.id

                elif items.warehouse_id.id == stn_data.warehouse_id_from.id:
                    location_dest_id = items.default_location_src_id.id
            move_line = []

            stock_data = {}

            if location_id is not None and location_dest_id is not None:
                stock_data = {'origin': False, 'message_follower_ids': False, 'carrier_tracking_ref': False,
                              'number_of_packages': 0, 'date_done': False, 'carrier_id': False, 'write_uid': False,
                              'partner_id': False, 'message_ids': False, 'note': False,
                              'picking_type_id': picking_type_id,
                              'move_type': 'one', 'company_id': 1, 'priority': '1', 'picking_type_code': False,
                              'owner_id': False, 'min_date': False, 'date': strftime("%Y-%m-%d %H:%M:%S", gmtime()),
                              'pack_operation_ids': [],
                              'carrier_code': 'custom', 'invoice_state': 'none'}
                tmp_dict = {}
                for stn_line in stn_data.stn_process_line:
                    move_line.append([0, False,
                                      {'product_uos_qty': stn_line.transfer_quantity,
                                       'date_expected': strftime("%Y-%m-%d %H:%M:%S", gmtime()),
                                       'date': strftime("%Y-%m-%d %H:%M:%S", gmtime()),
                                       'product_id': stn_line.product_id.id, 'product_uom': 1,
                                       'picking_type_id': picking_type_id,
                                       'product_uom_qty': stn_line.transfer_quantity,
                                       'invoice_state': 'none', 'product_tmpl_id': False, 'product_uos': False,
                                       'reserved_quant_ids': [], 'location_dest_id': location_dest_id,
                                       'procure_method': 'make_to_stock',
                                       'product_packaging': False, 'group_id': False, 'location_id': location_id,
                                       'name': str(stn_line.product_id.name)}])

                stock_data['move_lines'] = move_line

            stock_obj = self.pool.get('stock.picking')
            save_the_data = stock_obj.create(cr, uid, stock_data, context=context)

            ## End Challan From Here

            ## Assign Challan From Here
            if save_the_data:
                stock_obj_confirmation = stock_obj.action_confirm(cr, uid, [save_the_data], context=context)
                stock_obj = stock_obj.action_assign(cr, uid, [save_the_data], context=context)

                requested_stn_query = "UPDATE stn_process SET stock_id='{0}' WHERE id={1}".format(
                   save_the_data, id)
                cr.execute(requested_stn_query)
                cr.commit()

        return record

    def send_stn(self, cr, uid, ids, context=None):

        stn_obj = self.browse(cr, uid, ids[0], context=context)
        sender = uid

        if stn_obj.create_uid.id == uid:
            raise osv.except_osv(_("Warning!!!"), _(
                "You can not send it. Only Other warehouse people can do it."))



        for single_id in ids:
            send_stn_query = "UPDATE stn_process SET state='send', send_time='{0}',send_by='{1}'  WHERE id={2}".format(
                str(fields.datetime.now()), sender, single_id)
            cr.execute(send_stn_query)
            cr.commit()

            send_stn_query_line = "UPDATE stn_process_line SET state='send', send_time='{0}' WHERE stn_process_id={1}".format(
                str(fields.datetime.now()), single_id)
            cr.execute(send_stn_query_line)
            cr.commit()

        return True

    def cancel_stn(self, cr, uid, ids, context=None):

        stn_obj = self.browse(cr, uid, ids[0], context=context)

        if stn_obj['state'] != 'draft':

            if stn_obj.stock_id.id is not None or False:
                # Cancel Action
                self.pool.get('stock.picking').action_cancel(cr, uid, int(stn_obj.stock_id.id), context)

        for single_id in ids:
            cancel_stn_query = "UPDATE stn_process SET state='cancel', cancel_time='{0}' WHERE id={1}".format(
                str(fields.datetime.now()), single_id)
            cr.execute(cancel_stn_query)
            cr.commit()

            cancel_stn_query_line = "UPDATE stn_process_line SET state='cancel', cancel_time='{0}' WHERE stn_process_id={1}".format(
                str(fields.datetime.now()), single_id)
            cr.execute(cancel_stn_query_line)
            cr.commit()

        return True

    def receive_stn(self, cr, uid, ids, context=None):
        receiver = uid

        uid=1

        # call transfer
        stn_obj = self.browse(cr, uid, ids, context=context)
        stock_picking = self.pool.get('stock.picking').do_enter_transfer_details(cr, uid, [stn_obj.stock_id.id],
                                                                                 context=context)

        if str(stn_obj.stock_id.state) == 'draft' or str(stn_obj.stock_id.state) == 'confirmed':
            #     # unreserve
            self.pool.get('stock.picking').do_unreserve(cr, uid, int(stn_obj.stock_id.id), context=context)

            # action cancel
            self.pool.get('stock.picking').action_assign(cr, uid, int(stn_obj.stock_id.id), context=context)

        if str(stn_obj.stock_id.state) == 'waiting':
            # Rereservation
            self.pool.get('stock.picking').rereserve_pick(cr, uid, [stn_obj.stock_id.id], context=context)



        if stn_obj.stock_id.state != 'assigned':
            raise except_orm(_('Sorry'), _("Your Product is not reserved for this stn. It is already assigned to other orders"))




        trans_obj = self.pool.get('stock.transfer_details')
        trans_search = trans_obj.search(cr, uid, [('picking_id', '=', stn_obj.stock_id.id)], context=context)

        trans_search = [trans_search[len(trans_search)-1]] if len(trans_search)>1 else trans_search

        trans_browse = self.pool.get('stock.transfer_details').browse(cr, uid, trans_search, context=context)

        trans_browse.do_detailed_transfer()

        for single_id in ids:
            received_stn_query = "UPDATE stn_process SET state='receive', received_time='{0}', received_by='{1}' WHERE id={2}".format(
                str(fields.datetime.now()), receiver, single_id)
            cr.execute(received_stn_query)
            cr.commit()

            received_stn_query_line = "UPDATE stn_process_line SET state='receive', received_time='{0}' WHERE stn_process_id={1}".format(
                str(fields.datetime.now()), single_id)
            cr.execute(received_stn_query_line)
            cr.commit()

        return True

    def request_stn(self, cr, uid, ids, context=None):

        request_user = uid

        id = ids[0] if len(ids) > 0 else None
        stn_data = self.browse(cr, uid, id, context=context)
        stock_picking_type_ids = self.pool['stock.picking.type'].search(cr, uid, [('name', 'like', 'Delivery Orders')],
                                                                        context=context)
        stock_picking_type_data = self.pool['stock.picking.type'].browse(cr, uid, stock_picking_type_ids,
                                                                         context=context)
        location_id = None
        location_dest_id = None
        picking_type_id = None

        for items in stock_picking_type_data:

            if items.warehouse_id.id == stn_data.warehouse_id_to:
                location_id = items.default_location_src_id.id
                picking_type_id = items.id

            elif items.warehouse_id.id == stn_data.warehouse_id_from.id:
                location_dest_id = items.default_location_src_id.id
        move_line = []

        stock_data = {}

        if location_id is not None and location_dest_id is not None:
            stock_data = {'origin': False, 'message_follower_ids': False, 'carrier_tracking_ref': False,
                          'number_of_packages': 0, 'date_done': False, 'carrier_id': False, 'write_uid': False,
                          'partner_id': False, 'message_ids': False, 'note': False, 'picking_type_id': picking_type_id,
                          'move_type': 'one', 'company_id': 1, 'priority': '1', 'picking_type_code': False,
                          'owner_id': False, 'min_date': False, 'date': strftime("%Y-%m-%d %H:%M:%S", gmtime()),
                          'pack_operation_ids': [],
                          'carrier_code': 'custom', 'invoice_state': 'none'}
            tmp_dict = {}
            for stn_line in stn_data.stn_process_line:
                move_line.append([0, False,
                                  {'product_uos_qty': stn_line.transfer_quantity,
                                   'date_expected': strftime("%Y-%m-%d %H:%M:%S", gmtime()),
                                   'date': strftime("%Y-%m-%d %H:%M:%S", gmtime()),
                                   'product_id': stn_line.product_id.id, 'product_uom': 1,
                                   'picking_type_id': picking_type_id, 'product_uom_qty': stn_line.transfer_quantity,
                                   'invoice_state': 'none', 'product_tmpl_id': False, 'product_uos': False,
                                   'reserved_quant_ids': [], 'location_dest_id': location_dest_id,
                                   'procure_method': 'make_to_stock',
                                   'product_packaging': False, 'group_id': False, 'location_id': location_id,
                                   'name': str(stn_line.product_id.name)}])

            stock_data['move_lines'] = move_line

        stock_obj = self.pool.get('stock.picking')
        save_the_data = stock_obj.create(cr, uid, stock_data, context=context)
        if save_the_data:
            stock_obj_confirmation = stock_obj.action_confirm(cr, uid, [save_the_data], context=context)
            stock_obj = stock_obj.action_assign(cr, uid, [save_the_data], context=context)


        for single_id in ids:
            requested_stn_query = "UPDATE stn_process SET state='requested', requested_time='{0}', stock_id='{1}', requested_by='{2}' WHERE id={3}".format(
                str(fields.datetime.now()),save_the_data, request_user, single_id)
            cr.execute(requested_stn_query)
            cr.commit()

            requested_stn_query_line = "UPDATE stn_process_line SET state='requested', requested_time='{0}' WHERE stn_process_id={1}".format(
                str(fields.datetime.now()), single_id)
            cr.execute(requested_stn_query_line)
            cr.commit()

        return True




class stn_process_line(osv.osv):
    _name = "stn.process.line"
    _description = "STN Process Line"

    _columns = {
        'stn_process_id': fields.many2one('stn.processs', 'STN Process ID', required=True,
                                          ondelete='cascade', select=True, readonly=True),

        'product_id': fields.many2one('product.product', 'Product', required=True),

        'send_time': fields.datetime('Sending Time'),
        'cancel_time': fields.datetime('Cancel Time'),
        'received_time': fields.datetime('Received Time'),
        'requested_time': fields.datetime('Requested Time'),

        'transfer_quantity': fields.float('Transfer Qty', required=True),
        'available_qty': fields.float('Free Qty'),
        'requested_qty': fields.float('Requested Qty'),

        'state': fields.selection([
            ('draft', 'Created'),
            ('requested', 'Requested'),
            ('send', 'Confirmed'),
            ('receive', 'Received'),
            ('cancel', 'Cancelled'),
        ], 'Status', help="Gives the status of the STN Process line", select=True),
    }

    def onchange_product_id(self, cr, uid, ids, product_id=False, warehouse_id_to=False, context=None):

        warehouse_id_to = context.get('warehouse_id_to')

        if not warehouse_id_to:
            raise except_orm(_('No Warehouse Defined!'), _("You must first select a Destination Warehouse"))
        values = {}

        if product_id is not False:
            prod_obj = self.pool.get('product.product')
            uid=1
            prod_search = prod_obj.search(cr, uid, [('id', '=', product_id)], context=context)
            prod_browse = self.pool.get('product.product').browse(cr, uid, prod_search, context=context)

            if warehouse_id_to == 1:  # Uttara
                values['available_qty'] = prod_browse.uttwh_free_quantity
            elif warehouse_id_to == 2:  # Nodda
                values['available_qty'] = prod_browse.nodda_free_quantity
            elif warehouse_id_to == 4:  # Motijeel
                values['available_qty'] = prod_browse.moti_free_quantity
            elif warehouse_id_to == 3:  # Nodda Retail
                values['available_qty'] = prod_browse.nodda_retail_free_quantity
            elif warehouse_id_to == 6:  # Uttara Retail
                values['available_qty'] = prod_browse.uttwh_retail_free_quantity
            elif warehouse_id_to == 7:  # Motijeel Retail
                values['available_qty'] = prod_browse.moti_retail_quantity
            elif warehouse_id_to == 19:  # Motijeel Retail
                values['available_qty'] = prod_browse.comilla_free_quantity
            # elif warehouse_id_to ==8: # Damage
            #     values['available_qty'] = prod_browse.moti_free_quantity
            elif warehouse_id_to == 27:  # Badda
                values['available_qty'] = prod_browse.Badda_free_quantity
            elif warehouse_id_to == 21:  # Signboard
                values['available_qty'] = prod_browse.signboard_free_quantity

            elif warehouse_id_to == 34:  # Badda HORECA
                values['available_qty'] = prod_browse.badda_horeca_free_quantity
            elif warehouse_id_to == 37:  # Savar
                values['available_qty'] = prod_browse.saver_free_quantity

        return {'value': values}


class CancelStnProcessReason(osv.osv):
    _name = "cancel.stn.process.reason"
    _description = "Cancel STN Process Reason"

    _columns = {
        'cancel_stn_process_date': fields.datetime('Cancel STN Process Date'),
        'cancel_stn_process_by': fields.many2one('res.users', 'Cancel STN Process By'),
        'cancel_stn_process_reason': fields.text('Cancel STN Process Reason', required=True),

    }

    def cancel_stn_process_reason_def(self, cr, uid, ids, context=None):
        ids = context['active_ids']
        cancel_stn_process_reason = str(context['cancel_stn_process_reason'])
        cancel_stn_process_by = uid
        cancel_stn_process_date = str(fields.datetime.now())

        stn_obj = self.pool.get('stn.process')

        for s_id in ids:
            cancel_stn_proces_query = "UPDATE stn_process SET cancel_stn_process_reason='{0}', cancel_stn_process_by={1}, cancel_stn_process_date='{2}' WHERE id={3}".format(
                cancel_stn_process_reason, cancel_stn_process_by, cancel_stn_process_date, s_id)
            cr.execute(cancel_stn_proces_query)
            cr.commit()

            stn_obj.cancel_stn(cr, uid, [s_id], context)

        return True


class product_product(osv.osv):
    _inherit = "product.product"

    def _get_stn_intransit_qty(self, cr, uid, ids, name, arg, context=None):
        c_uid= uid
        uid = 1
        res = {}


        stn_obj = self.pool.get('stn.process')
        stn_search = stn_obj.search(cr, uid, [('state', 'in', ['requested', 'send'])], context=context)
        stn_browse = self.pool.get('stn.process').browse(cr, uid, stn_search, context=context)

        for p_id in ids:
            t_qty =0
            for items in stn_browse:
                for stn_line in items.stn_process_line:
                    if p_id == stn_line.product_id.id:
                        t_qty += stn_line.transfer_quantity
            res[p_id]=t_qty




        return res


    _columns = {

        'stn_in_transit_qty': fields.function(_get_stn_intransit_qty, string='In transit Qty',
                                                 type='float'),
    }




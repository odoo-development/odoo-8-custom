{
    'name': 'SO Delivery Inside Outside Field',
    'version': '1.0',
    'category': 'Sale',
    'author': 'Rocky',
    'summary': 'SO Delivery Inside Outside Field',
    'description': 'SO Delivery Inside Outside Field',
    'depends': ['base', 'sale', 'odoo_magento_connect', 'order_approval_process', 'sales_collection_billing_process'],
    'data': ['so_inside_outside_view.xml'],
    'installable': True,
    'application': True,
    'auto_install': False,
}

from openerp.osv import fields, osv, expression




class sale_order(osv.osv):
    _inherit = "sale.order"



    def create(self, cr, uid, vals, context=None):

        so_id = super(sale_order, self).create(cr, uid, vals, context=context)

        order_info = self.browse(cr, uid, [so_id], context=context)

        if 'Dhaka' in order_info.partner_shipping_id.city:
            so_inside_outside = 'inside'

        else:
            so_inside_outside = 'outside'

        order_info.write({'so_inside_outside': so_inside_outside})

        return so_id




    _columns = {
        'so_inside_outside': fields.selection([('inside', 'Inside'), ('outside', 'Outside')], 'Address Zone'),
    }

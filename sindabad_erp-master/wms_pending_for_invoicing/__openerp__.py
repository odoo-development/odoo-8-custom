{
    'name': 'WMS Pending for Invoicing',
    'version': '1.0',
    'category': 'WMS',
    'author': 'Rocky',
    'summary': 'WMS Pending for Invoicing for warehouse team',
    'description': 'WMS Pending for Invoicing for warehouse team',
    'depends': [
        'stock',
    ],
    'data': [
        'wms_pfinvoicing_view.xml',
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}
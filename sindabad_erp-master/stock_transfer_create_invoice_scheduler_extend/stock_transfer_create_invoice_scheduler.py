from datetime import datetime, timedelta
from datetime import date
from openerp import SUPERUSER_ID
from openerp.osv import fields, osv
from openerp.tools.translate import _
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT
import openerp.addons.decimal_precision as dp
from openerp import workflow
from openerp import api
from ..base.res.res_partner import format_address
from openerp.exceptions import Warning, except_orm
import dateutil.parser


class PackingList(osv.osv):
    _inherit = 'packing.list'

    def make_confirm(self, cr, uid, ids, context=None):

        super(PackingList, self).make_confirm(cr, uid, ids, context=context)
        # packing_order_pool = self.pool.get('packing.order.transfer.invoice.list')
        # for packing in self.pool.get('packing.list').browse(cr, uid, ids, context=context):
        #     for data_res in packing.packing_line:
        #         so_obj = self.pool.get('sale.order')
        #         so_list = so_obj.search(cr, uid, [('client_order_ref', '=', data_res.magento_no)])
        #
        #         data = {
        #             'packing_id':packing.id,
        #             'picking_id':int(packing.picking_id),
        #             'stock_id':data_res.stock_id.id,
        #             'packing_create_date':data_res.create_date,
        #             'magento_no':data_res.magento_no,
        #             'name':data_res.magento_no,
        #             'state':'pending',
        #             'order_id':so_list[0],
        #             'order_number':data_res.order_number,
        #
        #         }
        #
        #         saved_id = packing_order_pool.create(cr, uid, data, context=None)

        return True


class PackingOrderTransferInvoiceList(osv.osv):
    _name = 'packing.order.transfer.invoice.list'
    _description = "Packing Line List"

    _columns = {
        'packing_id': fields.many2one('packing.list',string='Packing'),
        'picking_id': fields.many2one('picking.list', string='Picking'),

        'stock_id': fields.many2one('stock.picking', string='Stock Packing'),
        'invoice_id': fields.many2one('account.invoice', string='Invoice'),
        'packing_create_date': fields.date('Packing Create Date'),
        'stock_transfer_date': fields.datetime('Stock Transfer Date'),
        'invoice_create_date': fields.datetime('Invoice Create Date'),

        'order_number': fields.char('Order Number'),
        'magento_no': fields.char('Magento No'),
        'name': fields.char('Name'),
        'order_id': fields.many2one('sale.order', string='Order'),
        'state': fields.selection([
            ('pending', 'Pending'),
            ('stock_transfer', 'Transfer Done'),
            ('invoiced', 'Invoice Done'),

        ], 'Status', readonly=True, copy=False, help="Gives the status of the quotation or sales order", select=True),
        'stock_transfer':fields.boolean('Stock Transfer'),
        'invoiced':fields.boolean('Invoiced'),
        'exception_log':fields.text('Exception Log'),
    }

    _defaults = {
        'state': 'pending',

    }


class auto_stock_transfer_from_pack(osv.osv):
    _name = "auto.stock.transfer.from.pack"

    _columns = {
        'name': fields.char('Name'),

    }

    def auto_stock_transfer_from_pack(self,cr,uid,context=None):
        req_magento_no = []
        done_magento_no = []
        uid = 1
        count =0
        start_date = datetime.now()
        transfer_invoice = self.pool.get('packing.order.transfer.invoice.list')

        sale_ids = transfer_invoice.search(cr, uid, [('stock_transfer', '=', False),('invoiced', '=', False)], limit=1000)

        stock_list = transfer_invoice.browse(cr, uid, sale_ids, context=context)

        for picking_id in stock_list:

            # picking = [picking_id.stock_id.id]
            order_id = str(picking_id.magento_no)
            req_magento_no.append(order_id)
            stock_picking_id=picking_id.stock_id.id

            stock_picking_obj = self.pool.get('stock.picking').browse(cr, uid, [stock_picking_id], context=context)[0]

            if str(stock_picking_obj.state) not in ('done','cancel'):
                count+=1

                stock_picking = self.pool.get('stock.picking').do_enter_transfer_details(cr, uid, [stock_picking_id],
                                                                                     context=context)

                trans_obj = self.pool.get('stock.transfer_details')
                trans_search = trans_obj.search(cr, uid, [('picking_id', '=', stock_picking_id)], context=context)

                trans_search = [trans_search[len(trans_search) - 1]] if len(trans_search) > 1 else trans_search

                trans_browse = self.pool.get('stock.transfer_details').browse(cr, uid, trans_search, context=context)

                trans_browse.do_detailed_transfer()
                done_magento_no.append(order_id)
                cr.commit()

                cr.execute("update packing_order_transfer_invoice_list set stock_transfer=True,state='stock_transfer'"
                           ",stock_transfer_date=%s where id=%s", ([str(fields.datetime.now()),picking_id.id]))
                cr.commit()

        end_date=datetime.now()
        time_delta = ((end_date+timedelta(hours=6)) - (start_date+timedelta(hours=6)))
        total_seconds = time_delta.total_seconds()
        dur_minutes = total_seconds / 60

        transfer_log = self.pool.get('auto.stock.transfer.from.pack.log').create(cr, uid,{
            'name': 'stock-'+(start_date+timedelta(hours=6)).strftime("%Y-%m-%d %H:%M:%S")+'-'+ (end_date+timedelta(hours=6)).strftime("%Y-%m-%d %H:%M:%S"),
            'start_date':start_date,
            'end_date':end_date,
            'duration':dur_minutes,
            'total_count':len(sale_ids),
            'done_count':count,
            'req_magento_no': req_magento_no,
            'done_magento_no': done_magento_no,
        },context)

        return True

    def auto_create_invoice(self, cr, uid, context=None):

        start_date = datetime.now()
        count = 0
        uid = 1
        req_magento_no = []
        done_magento_no = []
        if context is None:
            context ={}
        transfer_invoice = self.pool.get('packing.order.transfer.invoice.list')

        sale_id = transfer_invoice.search(cr, uid, [('stock_transfer', '=', True), ('invoiced', '=', False)],
                                           limit=1000)

        pick_list = transfer_invoice.browse(cr, uid, sale_id, context=context)

        # for packing_id in ids:
        #     cr.execute("select stock_id,magento_no from  packing_list_line where packing_id=%s", ([packing_id]))
        #     pick_list = cr.fetchall()
        for picking_id in pick_list:
            order_id = str(picking_id.magento_no)
            stock_picking_id = picking_id.stock_id.id
            req_magento_no.append(order_id)

            sale_ids = self.pool.get('sale.order').search(cr, uid, [('client_order_ref', '=', order_id)],
                                                          context=context)
            sale_object = self.pool.get('sale.order').browse(cr, uid, sale_ids, context=context)[0]
            new_customer = sale_object.new_customer

            stock_inv_obj = self.pool.get('stock.picking').browse(cr, uid, stock_picking_id, context=context)

            if stock_inv_obj.invoice_state != 'invoiced':
                # Now Create the Invoice for this picking
                picking_pool = self.pool.get('stock.picking')
                context['date_inv'] = date.today()
                context['inv_type'] = 'out_invoice'
                active_ids = [picking_id.stock_id.id]

                ## Get customer A/R I

                try:
                    res = picking_pool.action_invoice_create(cr, uid, active_ids,
                                                             journal_id=1146,
                                                             group=True,
                                                             type='out_invoice',
                                                             context=context)
                except:
                    cr.execute(
                        "update packing_order_transfer_invoice_list set exception_log='Accounting Info missing The operation can not be completed.' id=%s", ([ picking_id.id]))
                    cr.commit()
                    self.cursor.rollback()

                    #
                    # raise osv.except_osv(_('Warning!'),
                    #                      _(
                    #                          'Accounting Info missing The operation can not be completed, Probably due to the Customer Account information properly not assigned. Please, contact the finance team.!!!'))

                # Following Query flag that invoice and transfer will set boolean value
                if res:
                    done_magento_no.append(order_id)
                    count+=1
                    inv_id = res[0]
                    cr.execute("DELETE FROM account_invoice_tax WHERE invoice_id=%s", ([res[0]]))
                    cr.commit()

                    tax_obj = self.pool.get('account.tax')
                    tax_data = tax_obj.browse(cr, uid, [5], context=context)[0]

                    cr.execute(
                        "select sum(five_vat) as total_vat from account_invoice_line where invoice_id=%s group by invoice_id",
                        ([res[0]]))
                    total_vat_amount = cr.fetchall()
                    if len(total_vat_amount) > 0:
                        total_vat_amount = total_vat_amount[0][0]

                        cr.execute("select amount_untaxed from account_invoice where id=%s", ([res[0]]))
                        base_amount = cr.fetchall()[0][0]

                        tax_line_data = {
                            'account_id': tax_data.account_collected_id.id,
                            'sequence': 1,
                            'company_id': 1,
                            'manual': False,
                            'base_amount': base_amount,
                            'amount': total_vat_amount,
                            'base': base_amount,
                            'tax_amount': total_vat_amount,
                            'name': tax_data.name,
                        }

                        account_invoice = self.pool.get('account.invoice')
                        account_invoice.write(cr, uid, inv_id, {'tax_line': [(0, 0, tax_line_data)]},
                                              context=context)

                        cr.commit()

                        ### Check for Product Warranty exist or not
                        found_warranty_items = False

                        if inv_id:
                            invoice_data = \
                            self.pool.get('account.invoice').browse(cr, uid, inv_id, context=context)[0]
                            try:
                                invoice_data.signal_workflow('invoice_open')
                                for items in invoice_data.invoice_line:
                                    if items.product_id.warranty_applicable == True:
                                        found_warranty_items = True
                                        break
                            except:
                                pass

                    cr.execute("update account_invoice set pack_id=%s,from_packed='True', x_priority_customer=%s where id=%s", ([ picking_id.packing_id.id,new_customer, res[0]]))
                    cr.commit()

                    cr.commit()
                    if found_warranty_items == True:
                        cr.execute(
                            "update packing_list_line set invoice_id=%s,warranty_serials=%s where stock_id=%s and packing_id=%s",
                            ([res[0], found_warranty_items, picking_id.stock_id.id,  picking_id.packing_id.id]))
                        cr.commit()
                    else:

                        cr.execute("update packing_list_line set invoice_id=%s where stock_id=%s and packing_id=%s",
                                   ([res[0], picking_id.stock_id.id, picking_id.packing_id.id]))
                        cr.commit()
                    cr.commit()

                    cr.execute(
                        "update packing_order_transfer_invoice_list set invoiced=True,state='invoiced'"
                        ",invoice_create_date=%s,invoice_id=%s where id=%s", ([str(fields.datetime.now()),res[0],
                                                                               picking_id.id]))
                    cr.commit()

        end_date = datetime.now()
        time_delta = (end_date - start_date)
        total_seconds = time_delta.total_seconds()
        dur_minutes = total_seconds / 60


        invoice_log = self.pool.get('auto.invoice.create.from.pack.log').create(cr, uid,{
            'name': 'Inv-'+(start_date+timedelta(hours=6)).strftime("%Y-%m-%d %H:%M:%S") + '-' + (end_date+timedelta(hours=6)).strftime("%Y-%m-%d %H:%M:%S"),
            'start_date': start_date,
            'end_date': end_date,
            'duration': dur_minutes,
            'total_count': len(sale_id),
            'done_count': count,
            'req_magento_no': req_magento_no,
            'done_magento_no': done_magento_no,
        },context)

        return True


class auto_stock_transfer_from_pack_log(osv.osv):
    _name = "auto.stock.transfer.from.pack.log"

    _columns = {
        'name': fields.char('Name'),
        'start_date': fields.datetime('Start Date'),
        'end_date': fields.datetime('End Date'),
        'duration': fields.text('Duration(Minutes)'),
        'total_count':fields.integer('Total Order'),
        'done_count':fields.integer('Total Order Done'),
        'req_magento_no':fields.text('Req Order'),
        'done_magento_no':fields.text('Done Order'),

    }


class auto_invoice_create_from_pack_log(osv.osv):
    _name = "auto.invoice.create.from.pack.log"

    _columns = {
        'name': fields.char('Name'),
        'start_date': fields.datetime('Start Date'),
        'end_date': fields.datetime('End Date'),
        'duration': fields.text('Duration(Minutes)'),
        'total_count':fields.integer('Total Order'),
        'done_count':fields.integer('Total Order Done'),
        'req_magento_no': fields.text('Req Order'),
        'done_magento_no': fields.text('Done Order'),

    }

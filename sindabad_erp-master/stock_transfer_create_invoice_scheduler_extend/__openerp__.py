{
    'name': 'Stock Transfer & Create Invoice Process Scheduler',
    'version': '1.0',
    'category': 'Stock Transfer & Create Invoice Process Scheduler',
    'author': 'Shuvarthi',
    'summary': 'Stock Transfer & Create Invoice Process Scheduler',
    'description': 'Stock Transfer & Create Invoice Process Scheduler',
    'depends': ['base', 'stock','pickings', 'packings', 'product_simple_sync',],
    'data': [
        'security/stock_transfer_scheduler_security.xml',
        'security/ir.model.access.csv',
        'stock_transfer_create_invoice_view.xml',
        'auto_order_create_invoice_log_view.xml',
        'auto_order_stock_transfer_create_log_view.xml'
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}

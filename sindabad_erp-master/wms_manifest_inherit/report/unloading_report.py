import time
from openerp.report import report_sxw
from openerp.osv import osv


class UnLoadingListReport(report_sxw.rml_parse):

    def __init__(self, cr, uid, name, context):
        super(UnLoadingListReport, self).__init__(cr, uid, name, context)
        self.localcontext.update({
            'get_magento_order_numbers': self.get_magento_order_numbers,
            'get_invoice_numbers': self.get_invoice_numbers,
            'get_product_list': self.get_product_list,
            'get_loading_number': self.get_loading_number,

        })

    def get_loading_number(self,obj):
        loading_name = ''

        loading_obj = "SELECT name FROM loading_master WHERE manifest_id={0}".format(obj.id)
        self.cr.execute(loading_obj)
        loading = self.cr.fetchone()
        if loading:
            loading_name = loading[0]

        return loading_name

    def get_magento_order_numbers(self, obj):

        magento_numbers = ''

        for items in obj.picking_line:
            if items.magento_no and (items.reschedule==True or items.partial_delivered==True or items.full_return==True):
                magento_numbers = magento_numbers + str(items.magento_no) + ', '

        return magento_numbers

    def get_invoice_numbers(self, obj):

        invoice_numbers = ''

        for items in obj.picking_line:
            if items.invoice_id and (items.reschedule==True or items.partial_delivered==True or items.full_return==True):
                invoice_numbers = invoice_numbers + str(items.invoice_number) + ', '

        return invoice_numbers


    def get_product_list(self, obj):
        p_list = []
        reschedule_invoice_number = []
        return_order_number = []

        for items in obj.picking_line:
            if items.magento_no and items.reschedule:
                reschedule_invoice_number.append(items.invoice_id)

            if items.magento_no and (items.full_return or items.partial_delivered):
                return_order_number.append(items.magento_no)

        # reschedule_invoice = self.pool.get('wms.manifest.outbound').browse(self.cr, self.uid, reschedule_invoice_number)
        invoice_line_env = self.pool.get('account.invoice.line')
        total_moveline_id_list = invoice_line_env.search(self.cr, self.uid, [('invoice_id', 'in', reschedule_invoice_number)])
        total_moveline_list = invoice_line_env.browse(self.cr, self.uid, total_moveline_id_list)



        return_order_env = self.pool.get('wms.manifest.outbound')
        total_retn_moveline_id_list = return_order_env.search(self.cr, self.uid,[('man_name', '=', obj.name),('name', 'in', return_order_number),('has_return','=',True)])
        # total_retn_moveline_list = return_order_env.browse(self.cr, self.uid, total_retn_moveline_id_list).outbound_line
        total_retn_moveline_list = return_order_env.browse(self.cr, self.uid, total_retn_moveline_id_list)

        product_id_lists = []
        product_obj_lists = []
        moves_list = []
        retn_moves_list = []

        for att1 in total_moveline_list:
            product_id_lists.append(att1.product_id.id)
            product_obj_lists.append(att1.product_id)
            moves_list.append(att1)

        for retn_moveline_id_list in total_retn_moveline_list:
            for att2 in retn_moveline_id_list.outbound_line:
                product_id_lists.append(att2.product_id)
                product_obj_lists.append(self.pool.get('product.product').browse(self.cr, self.uid, att2.product_id))
                retn_moves_list.append(att2)


        product_id_lists = list(set(product_id_lists))
        product_obj_lists = list(set(product_obj_lists))

        for product_id in product_id_lists:
            ndr_qty_count=0
            retn_qty_count=0
            product_info_dict ={}
            for mov in moves_list:
                if mov.product_id.id == product_id:
                    ndr_qty_count += mov.quantity

            for retn_mov in retn_moves_list:
                if retn_mov.product_id == product_id:
                    retn_qty_count += retn_mov.return_qty

            for p_id in product_obj_lists:

                if p_id.id == product_id and ndr_qty_count >0 and product_id not in (20045,19968):
                    product_info_dict['product_name'] = str(p_id.default_code) + ' ' + str(
                        p_id.name)
                    product_info_dict['default_code'] = p_id.default_code
                    product_info_dict['mov'] = p_id
                    product_info_dict['qty'] = ndr_qty_count
                    product_info_dict['retn_qty'] = retn_qty_count
                    product_info_dict['total_qty'] = float(ndr_qty_count) + float(retn_qty_count)
                    break
                if p_id.id == product_id and retn_qty_count >0 and product_id not in (20045,19968):
                    product_info_dict['product_name'] = str(p_id.default_code) + ' ' + str(
                        p_id.name)
                    product_info_dict['default_code'] = p_id.default_code
                    product_info_dict['mov'] = p_id
                    product_info_dict['qty'] = ndr_qty_count
                    product_info_dict['retn_qty'] = retn_qty_count
                    product_info_dict['total_qty'] = float(ndr_qty_count) + float(retn_qty_count)
                    break
            if product_info_dict:
                p_list.append(product_info_dict)

        return p_list



class report_unloadinglayout(osv.AbstractModel):
    _name = 'report.wms_manifest_inherit.report_unloadinglayout'
    _inherit = 'report.abstract_report'
    _template = 'wms_manifest_inherit.report_unloadinglayout'
    _wrapped_report_class = UnLoadingListReport

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:

{
     'name': 'WMS Manifest Inherit',
     'description': 'WMS Manifest Inherit',
     'author': 'Mehedi Hasan',
     'depends': ['wms_manifest','ndr_process', 'send_sms_on_demand','wms_manifest_extends','cash_collection'],
     'data': [
         # 'serial_number_view.xml',
         'wms_manifest_view.xml',
         'wms_outbound_view.xml',
         'account_invoice_view.xml',
         'report/report_unloading.xml',
         'report_menu.xml',

     ]

 }
from datetime import datetime, timedelta
import time
from datetime import date
from openerp import SUPERUSER_ID
from openerp.osv import fields, osv
from openerp.tools.translate import _
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT
import openerp.addons.decimal_precision as dp
from openerp import workflow
from openerp import api
import datetime

# class wms_manifest_line(osv.osv):
#
#     _inherit = "wms.manifest.line"
#
#     def _get_line_numbers(self, cr, uid, ids,fields, arg, context=None):
#
#         if context is None: context = {}
#
#         res = {}
#         line_num = 1
#         first_line_rec = self.browse(cr, uid, ids, context=context)
#         for line_rec in first_line_rec:
#             res[line_rec.id] = line_num
#             line_num += 1
#         return res
#     _columns = {
#         'line_no': fields.function(_get_line_numbers, type='integer', string='Line Number')
#     }

class wms_manifest_outbound(osv.osv):

    _inherit = "wms.manifest.outbound"

    _columns = {
        'source': fields.char('Source'),
        'delivery_done_by': fields.many2one('res.users', 'Delivered Done By'),
        'delivery_done_datetime': fields.datetime('Delivered Done Date'),
    }

    _defaults = {
        'state': 'pending',

    }

    def receipt_return_stock(self, cr, uid, ids, context=None):
        try:
            stock_picking_name = None
            invoice_name = None
            return_list = self.pool.get('wms.manifest.outbound').browse(cr, uid, ids, context=context)[0]

            if return_list.stock_return_id:
                raise osv.except_osv(_('Already Received!'), _('Already Received'))

            cr.execute("select invoice_id from  wms_manifest_outbound_line where wms_manifest_outbound_id=%s", (ids))

            invoice_ids = cr.fetchall()

            new_inv_ids = [items[0] for items in invoice_ids]

            if len(new_inv_ids) > 0:
                invoice_id = new_inv_ids[0]

                cr.execute("select origin,number from  account_invoice where id=%s", ([invoice_id]))
                origin = cr.fetchall()

                for item in origin:
                    if item:
                        stock_picking_name = item[0]
                        invoice_name = item[1]

            cr.execute("select id from  stock_picking where name=%s", ([stock_picking_name]))

            stock_id_lists = [items[0] for items in cr.fetchall()]
            stock_object = self.pool.get('stock.picking').browse(cr, uid, stock_id_lists, context=context)[0]

            product_dict = {}
            product_return_moves = [[5, False, False]]
            for items in return_list.outbound_line:

                if items.quantity > items.delivered:
                    product_dict = {}
                    product_dict['product_id'] = items.product_id
                    product_dict['quantity'] = items.quantity - items.delivered
                    product_dict['lot_id'] = False

                    for st_items in stock_object.move_lines:
                        if st_items.product_id.id == items.product_id:
                            product_dict['move_id'] = st_items.id
                            product_return_moves.append([0, False, product_dict])
                            product_dict = {}

            prepare_data = {'product_return_moves': product_return_moves,
                            'invoice_state': '2binvoiced'}

            return_pool = self.pool['stock.return.picking']
            data = prepare_data

            values = prepare_data
            context.update({
                'active_model': 'stock.picking',
                'active_ids': [stock_object.id],
                'active_id': stock_object.id,
                'refund': True
            })

            created_id = self.pool['stock.return.picking'].create(cr, uid, values, context=context)

            return_stock_id = None
            return_inv_id = None

            id_list = [created_id]
            context['active_id'] = stock_object.id
            stock_picking_id = return_pool._create_returns(cr, uid, id_list, context=context)
            # 2 id is coming

            # cr.commit()
            try:

                if stock_picking_id:
                    return_stock_id = stock_picking_id[0]

                    # Stock Transfer Starts from Here
                    picking = [stock_picking_id[0]]

                    context.update({

                        'active_model': 'stock.picking',
                        'active_ids': picking,
                        'active_id': len(picking) and picking[0] or False
                    })

                    stock_transfer_created_id = self.pool['stock.transfer_details'].create(cr, uid, {
                        'picking_id': len(picking) and picking[0] or False}, context)

                    if stock_transfer_created_id:
                        stock_transfer_id = self.pool.get('stock.transfer_details').browse(cr, uid, stock_transfer_created_id, context=context)
                        if stock_transfer_id.picking_id.state in ['assigned', 'partially_available']:
                            # raise Warning(_('You cannot transfer a picking in state \'%s\'.') % created_id.picking_id.state)

                            processed_ids = []
                            # Create new and update existing pack operations
                            for lstits in [stock_transfer_id.item_ids, stock_transfer_id.packop_ids]:
                                for prod in lstits:
                                    pack_datas = {
                                        'product_id': prod.product_id.id,
                                        'product_uom_id': prod.product_uom_id.id,
                                        'product_qty': prod.quantity,
                                        'package_id': prod.package_id.id,
                                        'lot_id': prod.lot_id.id,
                                        'location_id': prod.sourceloc_id.id,
                                        'location_dest_id': prod.destinationloc_id.id,
                                        'result_package_id': prod.result_package_id.id,
                                        'date': prod.date if prod.date else datetime.now(),
                                        'owner_id': prod.owner_id.id,
                                    }
                                    if prod.packop_id:
                                        prod.packop_id.with_context(no_recompute=True).write(pack_datas)
                                        processed_ids.append(prod.packop_id.id)
                                    else:
                                        pack_datas['picking_id'] = stock_transfer_id.picking_id.id
                                        packop_id = stock_transfer_id.env['stock.pack.operation'].create(pack_datas)
                                        processed_ids.append(packop_id.id)
                            # Delete the others
                            packops = stock_transfer_id.env['stock.pack.operation'].search(
                                ['&', ('picking_id', '=', stock_transfer_id.picking_id.id), '!', ('id', 'in', processed_ids)])
                            packops.unlink()

                            # Execute the transfer of the picking
                            stock_transfer_id.picking_id.do_transfer()

                            # Stock Transfer End Here

                        # Invoice start Here

                        if stock_transfer_id.picking_id.invoice_state != 'invoiced' and stock_transfer_id.picking_id.state =='done':
                            # Now Create the Invoice for this picking
                            res = []
                            picking_pool = self.pool.get('stock.picking')
                            from datetime import date
                            context['date_inv'] = date.today()
                            context['inv_type'] = 'in_invoice'
                            active_ids = [stock_picking_id[0]]

                            stock_invoice_onshipping_data ={
                                'journal_id': 1147,
                                'journal_type': 'sale_refund',
                                'invoice_date': date.today(),
                                'invoice_number': invoice_name
                            }


                            stock_invoice_onshipping_obj = self.pool.get("stock.invoice.onshipping")
                            stock_invoice_onshipping_id = stock_invoice_onshipping_obj.create(cr, uid, stock_invoice_onshipping_data, context=context)

                            res = stock_invoice_onshipping_obj.open_invoice(cr, uid, [stock_invoice_onshipping_id], context)

                            return_inv_id = res if res else None

                        else:
                            raise osv.except_osv(_('Warning!'), _('Something wrong!!.Please check your return move is create or not'))
            except:
                raise osv.except_osv(_('Error!!!'), _('Something wrong!!.'))

            stock_return_id = return_stock_id
            invoice_return_id = return_inv_id

            # cr.commit()
            for wms_id in ids:
                cr.execute("update wms_manifest_outbound set invoice_return_id=%s,stock_return_id=%s,state='confirm',"
                           "confirm_by=%s,confirm=TRUE where id=%s", ([invoice_return_id, stock_return_id,uid, wms_id]))
                cr.commit()

                cr.execute("update wms_manifest_outbound_line set invoice_return_id=%s where wms_manifest_outbound_id=%s",
                ([invoice_return_id, wms_id]))

                cr.commit()

            # Dispatch Delivered Flag update
        except:
            raise osv.except_osv(_('Error!'), _('Something wrong!!.'))
        
        return True


class wms_manifest_outbound_line(osv.osv):
    _inherit = "wms.manifest.outbound.line"

    _columns = {
        'invoice_return_id': fields.integer('Invoice Return ID'),
        # 'return_reason': fields.selection([
        #     ('cash_problems', 'Cash Problems'),
        #     ('customer_did_not_ordered', 'Customer Did Not Ordered'),
        #     ('customer_not_interested', 'Customer Not Interested'),
        #     ('customer_not_reachable', 'Customer Not Reachable'),
        #     ('customer_wants_full_delivery', 'Customer Wants Full Delivery'),
        #     ('customer_wants_high_quality', 'Customer Wants High Quality'),
        #     ('customer_wants_low_quality', 'Customer Wants Low Quality'),
        #     ('fake_order', 'Fake Order'),
        #     ('late_delivery', 'Late Delivery'),
        #     ('less_qty_required', 'Less Qty Required'),
        #     ('more_qty_required', 'More Qty Required'),
        #     ('office_closed', 'Office Closed'),
        #     ('pack_size_issue', 'Pack Size Issue'),
        #     ('price_issue', 'Price Issue'),
        #     ('product_damaged', 'Product Damaged'),
        #     ('product_mismatch', 'Product Mismatch'),
        #     ('returned_due_to_other_products', 'Returned Due to Other Products'),
        #     ('wrong_address', 'Wrong Address'),
        #     ('wrong_order_placed', 'Wrong Order Placed'),
        #     ('wrongly_packed', 'Wrongly Packed'),
        # ], 'Return Reason', select=True),
    }


class account_invoice(osv.osv):
    _inherit = "account.invoice"

    _columns = {
        'ndr_inv_ids': fields.one2many('ndr.process', 'invoice_id', 'NDR'),
        'inv_ids': fields.one2many('wms.manifest.outbound.line', 'invoice_id', 'Return Info'),
        'return_inv_ids': fields.one2many('wms.manifest.outbound.line', 'invoice_return_id', 'Return'),
    }

class wms_manifest_line(osv.osv):

    _inherit = "wms.manifest.line"

    _columns = {
        'source': fields.char('Source'),
        'priority_customer': fields.boolean('Priority Customer',index=True ),
        'delivery_done_by': fields.many2one('res.users', 'Delivered Done By'),
        'delivery_done_datetime': fields.datetime('Delivered Done Date'),
    }

    _defaults = {
        'status': 'pending'
    }


class ndr_process(osv.osv):
    _inherit = "ndr.process"
    _description = "NDR Process"

    _columns = {
        'source': fields.char('Source'),
        'delivery_done_by': fields.many2one('res.users', 'Delivered Done By'),
        'delivery_done_datetime': fields.datetime('Delivered Done Date'),
    }

    _defaults = {
        'state': 'pending'
    }


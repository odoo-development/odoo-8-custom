# Author S. M. Sazedul Haque 2019-01-01

import time
from dateutil.relativedelta import relativedelta
from openerp.report import report_sxw
from openerp.osv import fields, osv
from datetime import datetime, timedelta
import calendar


class customer_statement(osv.osv):
    """
    This Method will help to send customer statement via email using
    """

    _inherit = 'res.partner'

    _columns = {
        'last_customer_statement_date': fields.date("Last Customer Statement Date"),
    }

    def send_statement_mail(self, cr, uid, active=False, ids=None, cron_mode=True, context=None):
        # print 'send_statement_mail >> active==', active
        if active:
            # print 'send_statement_mail >>  if active== True', active
            template_id = \
            self.pool.get('email.template').search(cr, uid, [('name', '=', 'Customer Statement - Send by Email')],
                                                   context=context)[0]
            today = datetime.today().date()
            first_date_of_month = today.strftime('%Y-%m-')+'01'
            partner_obj = self.pool.get('res.partner')
            res_partner_obj = partner_obj.search(cr, uid, [('is_company', '=', True),
                                                            '|', ('last_customer_statement_date', '=', None),
                                                           ('last_customer_statement_date', '<=', first_date_of_month)
                                                           ],
                                                   context=context)
            i = 1
            email_lists=[]
            for id in res_partner_obj:
                c_obj = partner_obj.browse(cr, uid, [id])
                # print 'SL#', i
                i =i+1
                # print 'company name==', c_obj.name

                email_lists.append(c_obj.email)
                if c_obj.email:
                    try:
                        email_obj = self.pool.get('email.template').send_mail(cr, uid, template_id, id, force_send=True)
                        cr.execute("""  UPDATE res_partner
                                        SET last_customer_statement_date = %s
                                        WHERE id = %s
                                """, (today, id))
                        cr.commit()
                    except:
                        pass
            return True



class CustomerStatementReportEmail(report_sxw.rml_parse):

    def __init__(self, cr, uid, name, context):
        super(CustomerStatementReportEmail, self).__init__(cr, uid, name, context)
        self.localcontext.update({
            'get_client_order_ref': self.get_test
        })

        self.context = context

    def get_test(self):
        return 'dfdf'

    def set_context(self, objects, data, ids, report_type=None):



        ids = self.context.get('active_ids')
        partner_obj = self.pool['res.partner']
        context = self.context
        docs = partner_obj.browse(self.cr, self.uid, ids, context)

        due = {}
        paid = {}
        mat = {}
        text =''
        text_output = {}

        currentDay = datetime.now().day
        currentMonth = datetime.now().month
        currentYear = datetime.now().year


        if docs.monthly_limit:
            days = docs.credit_days
            if days == 0:
                days = 1
            if days >30:
                days = days - 30
            text = 'Please Pay within '+ str(days)+'-'+str(currentMonth)+'-'+str(currentYear)
        else:
            credit_days = docs.credit_days
            first_inv_date = ''
            all_ids=[docs.id]
            for item in docs.child_ids:

                all_ids.append(item.id)

            if len(all_ids) ==1:
                all_ids.appned(all_ids[0])

            self.cr.execute("SELECT date_invoice FROM account_invoice WHERE partner_id IN {0} and state='open' ORDER BY date_invoice LIMIT 1".format(tuple(all_ids)))


            for items in self.cr.fetchall():

                first_inv_date = str(items[0])

            if len(first_inv_date) >4:

                # first_inv_date = datetime.today().date()

                date_1 = datetime.strptime(first_inv_date, '%Y-%m-%d')
                limit_date = date_1 + timedelta(days=credit_days)

                text ='Please Pay within '+str(limit_date)

        init_balance = {}
        today_date = datetime.today().date()
        date_before_month = datetime.today() - relativedelta(months=1)
        # st_date = today_date - timedelta(days=90)
        start_date = date_before_month.date()
        st_date = start_date.strftime('%Y-%m-')+'01'
        end_date = start_date.strftime('%Y-%m-')+str(calendar.monthrange(start_date.year,start_date.month)[1])
        present_date = datetime.now().strftime("%m/%d/%Y")

        context['start_date'] = st_date
        context['end_date'] = end_date

        for partner in docs:
            text_output[partner.id] = text
            due[partner.id] = reduce(lambda x, y: x + (
                (y['account_id']['type'] == 'receivable' and y['debit'] or 0) or (
                    y['account_id']['type'] == 'payable' and y['credit'] * -1 or 0)),
                self._lines_get(partner), 0)
            paid[partner.id] = reduce(lambda x, y: x + (
                (y['account_id']['type'] == 'receivable' and y['credit'] or 0) or (
                    y['account_id']['type'] == 'payable' and y['debit'] * -1 or 0)),
                self._lines_get(partner), 0)
            mat[partner.id] = reduce(lambda x, y: x + (y['debit'] - y['credit']),
                                     filter(lambda x: x['date_maturity'] < end_date,
                                            self._lines_get(partner)), 0)

            init_balance[partner.id] = reduce(lambda x, y: x + (y['debit'] - y['credit']), self._init_lines_get(partner), 0)

        self.localcontext.update({
            'docs': docs,
            'time': time,
            'present_date': present_date,
            'getLines': self._lines_get,
            'due': due,
            'paid': paid,
            'prev_balance': init_balance,
            'date_range': str(st_date) + ' to ' + str(end_date),
            'mat': mat,
            'text':text_output
        })
        self.context = context


        # self.partner_ids = partner.id
        self.partner_ids = [res['partner_id'] for res in self.cr.dictfetchall()]

        return super(CustomerStatementReportEmail, self).set_context(objects, data, self.partner_ids, report_type)

    def _lines_get(self, partner):

        start_date = self.context.get('start_date')
        end_date = self.context.get('end_date')

        # print 'start_date & end_date =======', start_date, end_date

        moveline_obj = self.pool['account.move.line']

        parent_list = []


        if partner.parent_id:
            parent_list.append(partner.parent_id.id)

            self.cr.execute("select id from  res_partner where customer=True and id=%s", ([partner.parent_id.id]))

            for items in self.cr.fetchall():
                parent_list.append(items[0])
        else:
            parent_list.append(partner.id)


        self.cr.execute("select id from  res_partner where customer=True and parent_id=%s", ([partner.id]))
        for items in self.cr.fetchall():
            parent_list.append(items[0])



        movelines = moveline_obj.search(self.cr, self.uid,
                                        [('partner_id', 'in', parent_list),
                                         ('account_id.type', 'in', ['receivable', 'payable']),
                                         ('state', '<>', 'draft'), ('date', '<=', end_date), ('date', '>=', start_date)])
                                         # ('state', '<>', 'draft'), ('date', '<=', end_date), ('date', '>=', start_date)], order='id')

        movelines = moveline_obj.browse(self.cr, self.uid, movelines)


        return movelines

    def _init_balance_get(self, partner):

        parent_list = []

        if partner.parent_id:
            parent_list.append(partner.parent_id.id)

            self.cr.execute("select id from  res_partner where customer=True and id=%s", ([partner.parent_id.id]))

            for items in self.cr.fetchall():
                parent_list.append(items[0])
        else:
            parent_list.append(partner.id)

        self.cr.execute("select id from  res_partner where customer=True and parent_id=%s", ([partner.id]))
        for items in self.cr.fetchall():
            parent_list.append(items[0])

        start_date = self.context.get('start_date')

        moveline_obj = self.pool['account.move.line']
        movelines = moveline_obj.search(self.cr, self.uid,
                                        [('partner_id', 'in', parent_list),
                                         ('account_id.type', 'in', ['receivable', 'payable']),
                                         ('state', '<>', 'draft'), ('date', '<', start_date)])

        movelines = moveline_obj.browse(self.cr, self.uid, movelines)

        previous_balance = 0

        for items in movelines:
            previous_balance += (items.debit - items.credit)

        return previous_balance

    def _init_lines_get(self, partner):

        parent_list = []

        if partner.parent_id:
            parent_list.append(partner.parent_id.id)

            self.cr.execute("select id from  res_partner where customer=True and id=%s", ([partner.parent_id.id]))

            for items in self.cr.fetchall():
                parent_list.append(items[0])
        else:
            parent_list.append(partner.id)

        self.cr.execute("select id from  res_partner where customer=True and parent_id=%s", ([partner.id]))
        for items in self.cr.fetchall():
            parent_list.append(items[0])

        start_date = self.context.get('start_date')

        moveline_obj = self.pool['account.move.line']
        movelines = moveline_obj.search(self.cr, self.uid,
                                        [('partner_id', 'in', parent_list),
                                         ('account_id.type', 'in', ['receivable', 'payable']),
                                            ('state', '<>', 'draft'), ('date', '<', start_date)])

        movelines = moveline_obj.browse(self.cr, self.uid, movelines)

        previous_balance = 0

        for items in movelines:
            previous_balance += (items.debit - items.credit)

        return movelines

#
class report_customerstatement(osv.AbstractModel):
    _name = 'report.customer_statement.email_send'
    _inherit = 'report.abstract_report'
    _template = 'customer_statement.email_send'
    _wrapped_report_class = CustomerStatementReportEmail

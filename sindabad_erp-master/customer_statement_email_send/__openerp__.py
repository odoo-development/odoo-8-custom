# Author S. M. Sazedul Haque 2019-01-01

{
    'name': 'Customer Statement Email Send',
    'version': '1.0.0',
    'category': 'Accounts',
    'description': """
Allows you to Send Customer Statement    
==============================================================

""",
    'author': 'S. M. Sazedul Haque',
    'depends': ['account', 'report'],
    'data': [
        'customer_statement_email_send_menu.xml',
        'customer_statement_email_template.xml',
        'report/statement_layout.xml',
        'report/report_customerstatement.xml',
        # 'wizard/customer_statement_report_email_send_view.xml',
    ],

    'installable': True,
    'auto_install': False,
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:

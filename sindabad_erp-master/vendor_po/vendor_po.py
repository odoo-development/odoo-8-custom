from datetime import datetime, timedelta
import time
from datetime import date
from openerp import SUPERUSER_ID
from openerp.osv import fields, osv
from openerp.tools.translate import _
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT
import openerp.addons.decimal_precision as dp
from openerp import workflow
from openerp import api
import datetime
import re
import xmlrpclib
import json
import requests
import logging
from ..base.res.res_partner import format_address
from openerp.exceptions import Warning, except_orm
from ..odoo_to_magento_api_connect.api_connect import get_magento_token, submit_request


XMLRPC_API = '/index.php/api/xmlrpc'

_logger = logging.getLogger(__name__)

class purchase_order(osv.osv):
    _inherit = "purchase.order"

    def po_sync_to_magento(self, cr, uid, ids, context=None):

        po = self.browse(cr, uid, ids, context=context)

        # before sync check if the po is already in mapping tabel
        po_map_obj = self.pool.get('purchase.order.mapping')
        po_map_ids = po_map_obj.search(cr, uid, [('odoo_po_id', '=', po.id)])
        vendor_id = po.partner_id.vendor_id
        
        # brand new po will sync to magento
        # Method POST:
        # https://vendor.sindabad.com/rest/V1/purchaseorder
        if vendor_id:
            if not po_map_ids:
                po_data = dict()
                po_data["vendor_id"] = vendor_id
                po_data["po_number"] = str(po.name)
                po_data["po_date"] = str(po.create_date)
                po_data["delivery_method"] = 'direct' # need to talk
                po_data["payment_type"] = str(po.adv_po_snd)
                po_data["delivery_to"] = str(po.picking_type_id.warehouse_id.name)
                po_data["exp_date"] = str(po.minimum_planned_date) # need to talk
                po_data["order_reference"] = str(po.client_order_ref)
                po_data["validator"] = str(po.write_uid.name) # need to talk
                po_data["is_vendor_pickup"] = 0 if str(po.vendor_pickup) == 'no' else 1
                po_data["total_amount"] = po.amount_total
                po_data["receive_amount"] = po.received_amount 
                po_data["is_invoiced"] = 0 if len(po.invoice_ids) == 0 else 1
                po_data["is_received"] = 1 if po.shipped else 0
                po_data["not_pick_up_reason"] = str(po.x_not_pick_up_reason) if po.x_not_pick_up_reason else ''
                po_data["status"] = str(po.state)

                po_info = dict()
                po_info["purchase_order"] = po_data

                # getting token
                token, url_root = get_magento_token(self, cr, uid, ids)
                
                if token:
                    token = token.replace('"', "")
                    headers = {'Authorization': token, 'Content-Type': 'application/json'}

                    url = url_root + "/rest/V1/purchaseorder"
                    data = json.dumps(po_info)

                    resp = requests.post(url, data=data, headers=headers)
                    print("--------------------")
                    print("PO created: " + str(resp))
                    print("--------------------")
                    """
                    (Pdb) resp.text
                        u'{"entity_id":6,"vendor_id":12,"po_date":"2020-03-09 05:10:18","po_number":"PO18948","delivery_method":"direct","payment_type":"advance","delivery_to":"Central Warehouse Uttara","exp_date":"2020-03-09","order_reference":"1000064688-1","validator":"Administrator","total_amount":"26400","receive_amount":"0","is_vendor_pickup":0,"is_invoiced":0,"is_received":0,"not_pick_up_reason":"","status":"approved","created_at":null,"updated_at":null}'
                    """
                    if resp.status_code == 200:
                        # creaty an entry into mapping table
                        resp_data = json.loads(resp.text)

                        mapping_data = {
                            'odoo_po_id': po.id,
                            'mage_po_id': str(resp_data['entity_id']),
                            'po_amount': po.amount_total,
                            'paid': po.paid,
                            'received': True if po.state == 'done' else False,
                            'update_required': True
                        }

                        po_map_obj.create(cr, 1, mapping_data)

                        mag_prod_obj = self.pool.get('magento.product')
                        # create order items to magento
                        # Method POST:
                        for product in po.order_line:

                            map_ids = mag_prod_obj.search(cr, uid, [('oe_product_id', '=', product.id)])
                            map_prod = mag_prod_obj.browse(cr, uid, map_ids)

                            product_data = dict()
                            product_data["po_id"] = resp_data['entity_id']
                            product_data["product_id"] = str(map_prod.mag_product_id)
                            product_data["product_name"] = str(product.name)
                            product_data["qty"] = product.product_qty
                            product_data["uom"] = str(product.product_id.prod_uom)
                            product_data["unit_price"] = product.price_unit
                            product_data["subtotal"] = product.product_qty * product.price_unit
                            product_data["schedule_date"] = str(product.date_planned)

                            product_info = dict()
                            product_info["purchase_order_item"] = product_data

                            prod_data = json.dumps(product_info)
                            prod_url = url_root + "/rest/V1/purchaseorder-item"
                            line_resp = requests.post(prod_url, data=prod_data, headers=headers)

                            print("--------------------")
                            print("PO item created: " + str(line_resp))
                            print("--------------------")

            else:
                # call po_update_sync_to_magento
                self.po_update_sync_to_magento(cr, uid, ids, context=context)

        return True

    def po_status_sync_to_magento(self, cr, uid, ids, context=None):

        # direct call to status update
        # Method GET:
        # https://vendor.sindabad.com/rest/V1/purchaseorder-status/4/draft

        po_status = dict()
        po_status['category_head_approval_pending'] = "Category%20Head%20Approval%20Pending"
        po_status['draft'] = "Draft%20PO"
        po_status['sent'] = "RFQ"
        po_status['bid'] = "Bid%20Received"
        po_status['category_head_approval_pending'] = "Category%20Head%20Approval%20Pending"
        po_status['category_vp_approval_pending'] = "Category%20VP%20Approval%20Pending"
        po_status['coo_approval_pending'] = "COO%20Approval%20Pending"
        po_status['ceo_approval_pending'] = "CEO%20Approval%20Pending"
        po_status['confirmed'] = "Waiting%20Approval"
        po_status['approved'] = "Purchase%20Confirmed"
        po_status['except_picking'] = "Shipping%20Exception"
        po_status['except_invoice'] = "Invoice%20Exception"
        po_status['done'] = "Done"
        po_status['cancel'] = "Cancelled"


        po = self.browse(cr, uid, ids, context=context)

        po_map_obj = self.pool.get('purchase.order.mapping')
        po_map_ids = po_map_obj.search(cr, uid, [('odoo_po_id', '=', po.id)])
        po_map = po_map_obj.browse(cr, uid, po_map_ids, context=context)
        vendor_id = po.partner_id.vendor_id

        # getting token
        token, url_root = get_magento_token(self, cr, uid, ids)

        if token and vendor_id:
            token = token.replace('"', "")
            headers = {'Authorization': token, 'Content-Type': 'application/json'}

            url = url_root + "/rest/V1/purchaseorder-status/"+str(po_map.mage_po_id) + "/" + po_status.get(str(po.state), '')

            status_resp = requests.get(url, headers=headers)

            print("--------------------")
            print("Status Updated: " + str(status_resp))
            print("--------------------")

        return True
    
    def po_update_sync_to_magento(self, cr, uid, ids, context=None):

        po = self.browse(cr, uid, ids, context=context)

        po_map_obj = self.pool.get('purchase.order.mapping')
        po_map_ids = po_map_obj.search(cr, uid, [('odoo_po_id', '=', po.id)])
        po_map = po_map_obj.browse(cr, uid, po_map_ids, context=context)
        vendor_id = po.partner_id.vendor_id

        # first get items entity_id from magento with api
        # https://vendor.sindabad.com/rest/V1/purchaseorder-items-bypoid/4

        # getting token
        token, url_root = get_magento_token(self, cr, uid, ids)
        
        if token and vendor_id:
            token = token.replace('"', "")
            headers = {'Authorization': token, 'Content-Type': 'application/json'}

            po_items_url = url_root + "/rest/V1/purchaseorder-items-bypoid/" + str(po_map.mage_po_id)

            po_items_resp = requests.get(po_items_url, headers=headers)
            
            if po_items_resp.status_code == 200:

                po_items_data = json.loads(po_items_resp.text)['items']

                # delete items 
                # Method DELETE:
                # https://vendor.sindabad.com/rest/V1/purchaseorder-item/2
                for item in po_items_data:
                    entity_id = item['entity_id']

                    item_del_url = url_root + "/rest/V1/purchaseorder-item/" + str(entity_id)
                    del_resp = requests.delete(item_del_url, headers=headers)

                    print("--------------------")
                    print("PO item deleted: " + str(del_resp))
                    print("--------------------")

                # update po infos
                # Method PUT:
                # https://vendor.sindabad.com/rest/V1/purchaseorder
                po_data = dict()
                po_data["entity_id"] = int(po_map.mage_po_id)
                po_data["vendor_id"] = vendor_id
                po_data["po_number"] = str(po.name)
                po_data["po_date"] = str(po.create_date)
                po_data["delivery_method"] = 'direct' # need to talk
                po_data["payment_type"] = str(po.adv_po_snd)
                po_data["delivery_to"] = str(po.picking_type_id.warehouse_id.name)
                po_data["exp_date"] = str(po.minimum_planned_date) # need to talk
                po_data["order_reference"] = str(po.client_order_ref)
                po_data["validator"] = str(po.write_uid.name) # need to talk
                po_data["is_vendor_pickup"] = 0 if str(po.vendor_pickup) == 'no' else 1
                po_data["total_amount"] = po.amount_total
                po_data["receive_amount"] = po.received_amount 
                po_data["is_invoiced"] = 0 if len(po.invoice_ids) == 0 else 1
                po_data["is_received"] = 1 if po.shipped else 0
                po_data["not_pick_up_reason"] = str(po.x_not_pick_up_reason) if po.x_not_pick_up_reason else ''
                po_data["status"] = str(po.state)

                po_info = dict()
                po_info["purchase_order"] = po_data

                url = url_root + "/rest/V1/purchaseorder"
                data = json.dumps(po_info)

                po_update_resp = requests.put(url, data=data, headers=headers)

                print("--------------------")
                print("PO updated: " + str(po_update_resp))
                print("--------------------")

                if po_update_resp.status_code == 200:

                    # update mapping table
                    mapping_data = {
                        'po_amount': po.amount_total,
                        'paid': po.paid,
                        'received': True if po.state == 'done' else False,
                        'update_required': True
                    }
                    po_map_obj.write(cr, 1, po_map.id, mapping_data, context=context)

                    # creaty an entry into mapping table
                    resp_data = json.loads(po_update_resp.text)

                    mag_prod_obj = self.pool.get('magento.product')
                    # create order items to magento
                    # Method POST:
                    for product in po.order_line:

                        map_ids = mag_prod_obj.search(cr, uid, [('oe_product_id', '=', product.id)])
                        map_prod = mag_prod_obj.browse(cr, uid, map_ids)

                        product_data = dict()
                        product_data["po_id"] = resp_data['entity_id']
                        product_data["product_id"] = str(map_prod.mag_product_id)
                        product_data["product_name"] = str(product.name)
                        product_data["qty"] = product.product_qty
                        product_data["uom"] = str(product.product_id.prod_uom)
                        product_data["unit_price"] = product.price_unit
                        product_data["subtotal"] = product.product_qty * product.price_unit
                        product_data["schedule_date"] = str(product.date_planned)

                        product_info = dict()
                        product_info["purchase_order_item"] = product_data

                        prod_data = json.dumps(product_info)
                        prod_url = url_root + "/rest/V1/purchaseorder-item"
                        line_resp = requests.post(prod_url, data=prod_data, headers=headers)

                        print("--------------------")
                        print("PO item updated: " + str(line_resp))
                        print("--------------------")

        return True

    def category_head_approval(self, cr, uid, ids, context=None):

        res = super(purchase_order, self).category_head_approval(cr, uid, ids, context=context)

        # status updated to magento
        
        self.po_status_sync_to_magento(cr, uid, ids, context=context)

        return True
    
    def action_cancel(self, cr, uid, ids, context=None):

        res = super(purchase_order, self).action_cancel(cr, uid, ids, context=context)

        # status updated to magento
        self.po_status_sync_to_magento(cr, uid, ids, context=context)

        return True
    
    def po_confirm_order(self, cr, uid, ids, context=None):

        res = super(purchase_order, self).po_confirm_order(cr, uid, ids, context=context)

        # status updated to magento
        self.po_status_sync_to_magento(cr, uid, ids, context=context)

        return True

class purchase_order_mapping(osv.osv):
    _name = "purchase.order.mapping"
    _description = "Purchase Order Mapping"

    _columns = {
        'odoo_po_id': fields.many2one('purchase.order', 'Purchase Order'),
        'mage_po_id': fields.char('Magento PO'),
        'po_amount': fields.float('PO Amount'),
        'paid': fields.boolean('Paid'),
        'received': fields.boolean('Received'),
        'update_required': fields.boolean("Update Required")
    }
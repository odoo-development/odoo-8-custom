{
    'name': 'Vendor PO to Magento',
    'version': '1.0',
    'category': 'Generic Modules',
    'author': 'Shahjalal Hossain',
    'summary': 'Vendor PO to Magento',
    'description': 'Vendor PO to Magento',
    'depends': ['base', 'purchase', 'po_approval_process', 'purchase_order_line_state'],
    'data': [
        'security/vendor_po_security.xml',
        'security/ir.model.access.csv',

        'views/vendor_po.xml',
        'views/vendor_mapping_view.xml',
        'vendor_mapping_menus.xml'
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}

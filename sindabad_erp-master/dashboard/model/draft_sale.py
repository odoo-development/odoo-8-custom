from openerp import models, fields, api
from datetime import datetime, timedelta


class DraftSale(models.Model):
    _inherit = 'dashboard.sample'

    @api.one
    def _get_count(self):

        d = 90
        d_days_ago = datetime.now() - timedelta(days=d)
        a_days_ago = datetime.now() - timedelta(days=91)

        # --- Start Order Flow Count and mapping ..........

        draft_quotation_count = self.env['sale.order'].search_count(
            [('state', '=', 'draft'), ('date_order', '>=', d_days_ago.strftime('%Y-%m-%d 00:00:00'))])

        confirm_sale_count = self.env['sale.order'].search(
            [('state', '=', ('progress', 'manual', 'shipping_except', 'done')),
             ('date_order', '>=', d_days_ago.strftime('%Y-%m-%d 00:00:00'))])

        cancel_sale_count = self.env['sale.order'].search(
            [('state', '=', 'cancel'), ('date_order', '>=', d_days_ago.strftime('%Y-%m-%d 00:00:00'))])

        delivered_sale_count = self.env['sale.order'].search(
            [('shipped', '=', True), ('date_order', '>=', d_days_ago.strftime('%Y-%m-%d 00:00:00'))])

        pending_delivery_count = self.env['sale.order'].search(
            [('state', '=', ('progress', 'manual')), ('date_order', '>=', d_days_ago.strftime('%Y-%m-%d 00:00:00'))])

        full_delivery_count = self.env['sale.order'].search(
            [('shipped', '=', True), ('date_order', '>=', a_days_ago.strftime('%Y-%m-%d 00:00:00'))])


        # ------End -------------------------------------------

        # -- Start Warehouse Sales ----

        waiting_for_procurement_count = self.env['stock.picking'].search(
            [('state', '=', ('confirmed', 'partially_available')), ('date', '>=', d_days_ago.strftime('%Y-%m-%d 00:00:00'))]
        )

        ready_for_transfer_count = self.env['stock.picking'].search(
            [('state', '=', 'assigned'), ('origin', 'like', 'SO'),
             ('date', '>=', d_days_ago.strftime('%Y-%m-%d 00:00:00')), ('picking_type_id.code', '=', 'outgoing')])

        cancelled_delivery = self.env['stock.picking'].search(
            [('state', '=', 'cancel'), ('date', '>=', d_days_ago.strftime('%Y-%m-%d 00:00:00')),
             ('picking_type_id.code', '=', 'outgoing')])

        transferred_delivery = self.env['stock.picking'].search(
            [('state', '=', 'done'), ('date', '>=', d_days_ago.strftime('%Y-%m-%d 00:00:00')),
             ('picking_type_id.code', '=', 'outgoing')])

        partially_available = self.env['stock.picking'].search(
            [('state', '=', 'partially_available'), ('date', '>=', d_days_ago.strftime('%Y-%m-%d 00:00:00'))])

        # -- End Warehouse --

        # -- Start Warehouse Purchase ----

        transferred_received = self.env['stock.picking'].search(
            [('state', '=', 'done'), ('date', '>=', d_days_ago.strftime('%Y-%m-%d 00:00:00')), ('picking_type_id.code', '=', 'incoming')])

        cancelled_receipt = self.env['stock.picking'].search(
            [('state', '=', 'cancel'), ('date', '>=', d_days_ago.strftime('%Y-%m-%d 00:00:00')), ('picking_type_id.code', '=', 'incoming')])

        # End Warehouse ---

        purchase_confirmation_count = self.env['purchase.order'].search(
            [('state', '=', 'approved'), ('date_order', '>=', d_days_ago.strftime('%Y-%m-%d 00:00:00'))])

        purchase_created_count = self.env['purchase.order'].search(
            [('state', '=', 'draft'), ('date_order', '>=', d_days_ago.strftime('%Y-%m-%d 00:00:00'))])

        purchase_cancelled_count = self.env['purchase.order'].search(
            [('state', '=', 'cancel'), ('date_order', '>=', d_days_ago.strftime('%Y-%m-%d 00:00:00'))])

        # ------- Invoice -------------

        pending_invoice = self.env['account.invoice'].search(
            [('state', '=', 'draft'), ('name', 'like', 100),
             ('date_invoice', '>=', a_days_ago.strftime('%Y-%m-%d 00:00:00'))])

        valid_invoice = self.env['account.invoice'].search(
            [('state', '=', 'open'), ('date_invoice', '>=', a_days_ago.strftime('%Y-%m-%d 00:00:00'))])

        paid_invoice = self.env['account.invoice'].search(
            [('state', '=', 'paid'), ('date_invoice', '>=', a_days_ago.strftime('%Y-%m-%d 00:00:00'))])

        cancelled_invoice = self.env['account.invoice'].search(
            [('state', '=', 'cancel'), ('date_invoice', '>=', a_days_ago.strftime('%Y-%m-%d 00:00:00'))])

        # ------------------ End Invoice --------------------------

        # ---- sale order
        self.draft_quotation_count = draft_quotation_count
        self.confirm_sale_count = len(confirm_sale_count)
        self.cancel_sale_count = len(cancel_sale_count)
        self.delivered_sale_count = len(delivered_sale_count)
        self.pending_delivery_count = len(pending_delivery_count)
        self.full_delivery_count = len(full_delivery_count)

        # ---- warehouse
        self.ready_for_transfer_count = len(ready_for_transfer_count)
        self.cancelled_delivery = len(cancelled_delivery)
        self.waiting_for_procurement_count = len(waiting_for_procurement_count)
        self.transferred_delivery = len(transferred_delivery)
        self.partially_available = len(partially_available)

        self.transferred_received = len(transferred_received)
        self.cancelled_receipt = len(cancelled_receipt)

        # ---- purchase
        self.purchase_created_count = len(purchase_created_count)
        self.purchase_confirmation_count = len(purchase_confirmation_count)
        self.purchase_cancelled_count = len(purchase_cancelled_count)

        # ---- invoice
        self.pending_invoice = len(pending_invoice)
        self.valid_invoice = len(valid_invoice)
        self.paid_invoice = len(paid_invoice)
        self.cancelled_invoice = len(cancelled_invoice)

    # ---- sale order
    draft_quotation_count = fields.Integer(compute='_get_count')
    confirm_sale_count = fields.Integer(compute='_get_count')
    cancel_sale_count = fields.Integer(compute='_get_count')
    delivered_sale_count = fields.Integer(compute='_get_count')
    pending_delivery_count = fields.Integer(compute='_get_count')
    full_delivery_count = fields.Integer(compute='_get_count')

    # ---- warehouse
    ready_for_transfer_count = fields.Integer(compute='_get_count')
    cancelled_delivery = fields.Integer(compute='_get_count')
    waiting_for_procurement_count = fields.Integer(compute='_get_count')
    transferred_delivery = fields.Integer(compute='_get_count')
    partially_available = fields.Integer(compute='_get_count')

    transferred_received = fields.Integer(compute='_get_count')
    cancelled_receipt = fields.Integer(compute='_get_count')

    # ---- purchase
    purchase_created_count = fields.Integer(compute='_get_count')
    purchase_confirmation_count = fields.Integer(compute='_get_count')
    purchase_cancelled_count = fields.Integer(compute='_get_count')

    # ---- invoice
    pending_invoice = fields.Integer(compute='_get_count')
    valid_invoice = fields.Integer(compute='_get_count')
    paid_invoice = fields.Integer(compute='_get_count')
    cancelled_invoice = fields.Integer(compute='_get_count')

from openerp import models, fields
import datetime


class Dashboard(models.Model):
    _name = 'dashboard.sample'

    name = fields.Char(string="Name", required=True)

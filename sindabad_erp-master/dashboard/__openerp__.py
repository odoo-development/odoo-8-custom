{
    'name': "Sample Dashboard Module",
    'version': '1.0',
    'category': 'Information Technology',
    'author': "Md Rockibul Alam Babu",
    'description': """
Sample Dashboard
    """,
    'website': "http://www.zerogravity.com.bd",
    'data': [


        'security/dashboard_security.xml',
        'security/ir.model.access.csv',
        'views/draft_sale_view.xml',
        'dashboard_view.xml',


    ],
    'depends': [
        'sale', 'stock', 'account',
    ],
    'installable': True,
    'auto_install': False,
}

# Author Mufti Muntasir Ahmed 02-09-2020

{
    'name': 'Purchase GRN Report Xls',
    'version': '8.0.1',
    'author': "Shuvarthi",
    'category': 'Purchase',
    'summary': 'Actual GRN Report xls',
    'depends': ['product', 'stock', 'sale','purchase', 'report_xls'],
    'data': [
            # 'security/actual_grn_report_security.xml',
            # 'security/ir.model.access.csv',
            'wizard/actual_grn_report_xlsx_filter.xml',
            'report/actual_grn_report_xlsx.xml',
    ],
    'installable': True,
    'auto_install': False,
    'application': False,
}


from openerp.osv import fields, osv
import datetime
from openerp import api



class ActualGRNReportXlsWiz(osv.osv_memory):

    _name = 'actual.grn.report.xls.wiz'
    _description = 'Actual GRN Report Xls Wiz'


    _columns = {
        'from_date': fields.date("From Date"),
        'to_date': fields.date("To Date")
    }
    _defaults = {
        'from_date': datetime.date.today(),
        'to_date': datetime.date.today()
    }


    def build_contexts(self, cr, uid, ids, data, context=None):
        if context is None:
            context = {}
        result = {'from_date': data['form']['from_date'],
                  'to_date': data['form']['to_date']
                  }

        return result

    def check_report(self, cr, uid, ids, context=None):
        if context is None:
            context = {}

        data = self.read(cr, uid, ids, ['from_date','to_date'], context=context)[0]
        datas = {
            'ids': context.get('active_ids', []),
            'model': 'actual.grn.report.xls.wiz',
            'form': data
        }

        return {'type': 'ir.actions.report.xml',
                'report_name': 'actual.grn.xls',
                'datas': datas
                }


# Author Mufti Muntasir Ahmed 02-09-2020

from openerp import models, api


class grn_stock_picking_xls(models.Model):
    _name = 'grn.stock.picking.report'

    @api.model
    def _report_xls_fields_stock(self):

        return [
            'po_no','po_confirmation_date','payment_status','grn_no','supplier','received_date','type',
            'product_category','product_name','received_qty','price_unit','total_price','po_credit_days',
            'po_sales_classification_type'

        ]

    # Change/Add Template entries
    @api.model
    def _report_xls_template_stock(self):

        return {}

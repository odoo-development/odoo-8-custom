import xlwt
from datetime import datetime,timedelta
from openerp.report import report_sxw
from openerp.addons.report_xls.report_xls import report_xls
from openerp.addons.report_xls.utils import rowcol_to_cell, _render
from openerp.tools.translate import translate, _
import logging
_logger = logging.getLogger(__name__)


_ir_translation_name = 'actual.grn.xls'


class actual_grn_xls_parser(report_sxw.rml_parse):

    def __init__(self, cr, uid, name, context):
        super(actual_grn_xls_parser, self).__init__(
            cr, uid, name, context=context)
        move_obj = self.pool.get('grn.stock.picking.report')
        self.context = context
        wanted_list = move_obj._report_xls_fields_stock(cr, uid, context)
        template_changes = move_obj._report_xls_template_stock(cr, uid, context)
        self.localcontext.update({
            'datetime': datetime,
            'wanted_list': wanted_list,
            'template_changes': template_changes,
            '_': self._,
        })

    def _(self, src):
        lang = self.context.get('lang', 'en_US')
        return translate(self.cr, _ir_translation_name, 'report', lang, src) \
            or src


class actual_grn_xls(report_xls):

    def __init__(self, name, table, rml=False, parser=False, header=True,
                 store=False):
        super(actual_grn_xls, self).__init__(
            name, table, rml, parser, header, store)

        # Cell Styles
        _xs = self.xls_styles
        # header
        rh_cell_format = _xs['bold'] + _xs['fill'] + _xs['borders_all']
        self.rh_cell_style = xlwt.easyxf(rh_cell_format)
        self.rh_cell_style_center = xlwt.easyxf(rh_cell_format + _xs['center'])
        self.rh_cell_style_right = xlwt.easyxf(rh_cell_format + _xs['right'])
        # lines
        aml_cell_format = _xs['borders_all']
        self.aml_cell_style = xlwt.easyxf(aml_cell_format)
        self.aml_cell_style_center = xlwt.easyxf(
            aml_cell_format + _xs['center'])
        self.aml_cell_style_date = xlwt.easyxf(
            aml_cell_format + _xs['left'],
            num_format_str=report_xls.date_format)
        self.aml_cell_style_decimal = xlwt.easyxf(
            aml_cell_format + _xs['right'],
            num_format_str=report_xls.decimal_format)
        # totals
        rt_cell_format = _xs['bold'] + _xs['fill'] + _xs['borders_all']
        self.rt_cell_style = xlwt.easyxf(rt_cell_format)
        self.rt_cell_style_right = xlwt.easyxf(rt_cell_format + _xs['right'])
        self.rt_cell_style_decimal = xlwt.easyxf(
            rt_cell_format + _xs['right'],
            num_format_str=report_xls.decimal_format)

        # XLS Template
        self.col_specs_template = {

            'po_no': {
                'header': [1, 20, 'text', _render("_('PO No')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'po_confirmation_date': {
                'header': [1, 20, 'text', _render("_('PO Confirmation')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'payment_status': {
                'header': [1, 20, 'text', _render("_('Payment Status')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'grn_no': {
                'header': [1, 20, 'text', _render("_('GRN No')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'supplier': {
                'header': [1, 20, 'text', _render("_('Supplier Name')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'received_date': {
                'header': [1, 20, 'text', _render("_('Receiving Date')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'type': {
                'header': [1, 20, 'text', _render("_('Type')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'product_category': {
                'header': [1, 20, 'text', _render("_('Product Category')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'product_name': {
                'header': [1, 20, 'text', _render("_('Product Name')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'received_qty': {
                'header': [1, 20, 'text', _render("_('Received QTY')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'price_unit': {
                'header': [1, 20, 'text', _render("_('Unit Price')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'total_price': {
                'header': [1, 20, 'text', _render("_('Total Amount')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'po_credit_days': {
                'header': [1, 20, 'text', _render("_('Credit Days')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'po_sales_classification_type': {
                'header': [1, 20, 'text', _render("_('Sales Classification Type')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

        }


    def generate_xls_report(self, _p, _xs, data, objects, wb):
        wanted_list = _p.wanted_list
        self.col_specs_template.update(_p.template_changes)
        _ = _p._
        context = self.context

        st_date = data.get('form').get('from_date')
        end_date = data.get('form').get('to_date')

        # DATETIME_FORMAT = "%Y-%m-%d"
        # st_date = datetime.strptime(start_date, DATETIME_FORMAT)
        # end_date = datetime.strptime(ends_date, DATETIME_FORMAT)

        context['from_date'] = st_date
        context['to_date'] = end_date
        self.context = context
        date_range = _("Date: %s to %s" % (st_date, end_date))

        # report_name = objects[0]._description or objects[0]._name
        report_name = _("Actual GRN Report")
        ws = wb.add_sheet(report_name[:31])
        ws.panes_frozen = True
        ws.remove_splits = True
        ws.portrait = 0  # Landscape
        ws.fit_width_to_pages = 1
        row_pos = 0

        # set print header/footer
        ws.header_str = self.xls_headers['standard']
        ws.footer_str = self.xls_footers['standard']

        # Title
        cell_style = xlwt.easyxf(_xs['xls_title'])
        c_specs = [
            ('report_name', 4, 3, 'text', report_name),
        ]
        row_data = self.xls_row_template(c_specs, ['report_name'])
        row_pos = self.xls_write_row(
            ws, row_pos, row_data, row_style=cell_style)
        row_pos += 1

        # Date Ranges
        cell_style = xlwt.easyxf(_xs['xls_title'])
        c_specs = [
            ('date_range', 4, 3, 'text', date_range),
        ]
        row_data = self.xls_row_template(c_specs, ['date_range'])
        row_pos = self.xls_write_row(
            ws, row_pos, row_data, row_style=cell_style)
        row_pos += 1

        # Column headers
        c_specs = map(lambda x: self.render(
            x, self.col_specs_template, 'header', render_space={'_': _p._}),
            wanted_list)
        row_data = self.xls_row_template(c_specs, [x[0] for x in c_specs])
        row_pos = self.xls_write_row(
            ws, row_pos, row_data, row_style=self.rh_cell_style,
            set_column_size=True)
        ws.set_horz_split_pos(row_pos)

        objects = self.get_pickings(self.context)


        for line in objects:

            c_specs = map(
                lambda x: self.render(x, self.col_specs_template, 'lines'),
                wanted_list)
            cal_cost=0

            for list_data in c_specs:

                if str(list_data[0]) == str('po_no'):
                    list_data[4] = str(line.get('po_no'))


                if str(list_data[0]) == str('po_confirmation_date'):
                    list_data[4] = str(line.get('po_confirmation_date'))

                if str(list_data[0]) == str('payment_status'):
                    list_data[4] = str(line.get('payment_status'))

                if str(list_data[0]) == str('grn_no'):
                    list_data[4] = str(line.get('grn_no'))

                if str(list_data[0]) == str('supplier'):
                    list_data[4] = str(line.get('supplier'))

                if str(list_data[0]) == str('received_date'):
                    list_data[4] = str(line.get('received_date'))

                if str(list_data[0]) == str('type'):
                    list_data[4] = str(line.get('type'))

                if str(list_data[0]) == str('product_category'):
                    list_data[4] = str(line.get('product_category'))

                if str(list_data[0]) == str('product_name'):
                    list_data[4] = str(line.get('product_name'))

                if str(list_data[0]) == str('received_qty'):
                    list_data[4] = str(line.get('received_qty'))

                if str(list_data[0]) == str('price_unit'):
                    list_data[4] = str(line.get('price_unit'))

                if str(list_data[0]) == str('total_price'):
                    list_data[4] = str(line.get('total_price'))

                if str(list_data[0]) == str('po_credit_days'):
                    list_data[4] = str(line.get('po_credit_days'))

                if str(list_data[0]) == str('po_sales_classification_type'):
                    list_data[4] = str(line.get('po_sales_classification_type'))


            row_data = self.xls_row_template(c_specs, [x[0] for x in c_specs])
            row_pos = self.xls_write_row(
                ws, row_pos, row_data, row_style=self.aml_cell_style)



    def get_pickings(self, docs):
        pickings_list = []

        query = "select (select name from res_partner where res_partner.id=stock_picking.partner_id) as customer_name,stock_picking.origin,stock_picking.partner_id,stock_picking.id,stock_picking.name,stock_picking.partner_id,stock_move.product_id,stock_picking.state,stock_move.name as move_name,stock_picking.date_done,stock_move.product_uom_qty,purchase_order.date_order as po_confirmation_date,purchase_order.adv_po_snd as payment_status,purchase_order.x_credit_days as credit_days,purchase_order.x_sale_classification_type as sale_classification_type,(select product_category.name from product_category, product_product,product_template where product_category.id=product_template.categ_id and  product_product.product_tmpl_id=product_template.id and  product_product.id=stock_move.product_id) as product_category, purchase_order_line.price_unit, (stock_move.product_uom_qty*purchase_order_line.price_unit) as total_price from stock_picking,stock_move,purchase_order_line,purchase_order where stock_picking.id=stock_move.picking_id and stock_picking.state='done' and stock_move.purchase_line_id=purchase_order_line.id and purchase_order.id = purchase_order_line.order_id and stock_picking.date_done >=%s and stock_picking.date_done <=%s"

        if docs:

            from_date = docs['from_date']
            end_date = docs['to_date']
            date_obj = datetime.strptime(end_date, '%Y-%m-%d') + timedelta(days=1)

            increased_date = date_obj.strftime('%Y-%m-%d')
            end_date = increased_date

            self.cr.execute(query, (from_date, end_date))
            grn_list = self.cr.dictfetchall()

            for items in grn_list:
                total_price = items.get('total_price')

                mov_name = items.get('name')
                grn_type = 'Received'

                if 'IN' not in mov_name:
                    grn_type = 'Return'
                    total_price = total_price * -1

                tmp_line = {
                    'po_no': items.get('origin'),
                    'grn_no': items.get('name'),
                    'supplier': items.get('customer_name'),
                    'received_date': items.get('date_done'),
                    'type': grn_type,
                    'product_name': items.get('move_name'),
                    'received_qty': items.get('product_uom_qty'),
                    'price_unit': items.get('price_unit'),
                    'total_price': total_price,
                    'po_confirmation_date': items.get('po_confirmation_date'),
                    'payment_status': items.get('payment_status'),
                    'product_category': items.get('product_category'),
                    'po_credit_days': items.get('credit_days'),
                    'po_sales_classification_type': items.get('sale_classification_type'),

                }
                pickings_list.append(tmp_line)

        return pickings_list


actual_grn_xls('report.actual.grn.xls',
                    'stock.picking',
                    parser=actual_grn_xls_parser)


# -*- coding: utf-8 -*-
##########################################################################
#
#    Copyright (c) 2018-Present Zero Gravity ventures Limited (<https://webkul.com/>)
#
##########################################################################

from datetime import date, datetime
from dateutil import relativedelta
import json
import time

from openerp.osv import fields, osv
from openerp.tools.float_utils import float_compare, float_round
from openerp.tools.translate import _
from openerp.tools import DEFAULT_SERVER_DATETIME_FORMAT, DEFAULT_SERVER_DATE_FORMAT
from openerp.exceptions import Warning
from openerp import SUPERUSER_ID, api, models
import openerp.addons.decimal_precision as dp
from openerp.addons.procurement import procurement
import logging
import sys

reload(sys)
sys.setdefaultencoding('utf8')


class stock_pack_operation(osv.osv):
    _inherit = "stock.pack.operation"

    _columns = {
        'product_location': fields.char('Product Location'),
    }


class stock_move_picking(osv.osv):
    _inherit = "stock.move"

    def _get_product_location(self, cr, uid, ids, field_name, args, context=None):
        res = dict.fromkeys(ids, 0)
        data = []
        for move in self.browse(cr, uid, ids, context=context):

            data = []
            for q in move.reserved_quant_ids:
                for r in q.history_ids:
                    if r.origin and 'PO' in r.origin:
                        smol = self.pool.get('stock.move.operation.link')
                        smol_obj = smol.search(cr, uid, [('move_id', '=', r.id)], context=context)
                        po_location = smol.browse(cr, uid, smol_obj[0]).operation_id.product_location
                        if po_location:
                            data.append(po_location)

            # res[move.id] = len(data) > 0 and ",".join(str(r) for r in data)
            res[move.id] = ", ".join(map(str, data)) if len(data) > 0 else ''
            # print '_get_product_location', res[move.id]
        return res

    _columns = {
        'product_location_list': fields.function(_get_product_location, string='Product Location', type='char'),
    }

# ----------------------------------------------------
# Stock Move Details
# ----------------------------------------------------


class stock_transfer_details(osv.osv):
    _inherit = 'stock.transfer_details'

    def default_get(self, cr, uid, fields, context=None):
        if context is None:
            context = {}
        res = super(stock_transfer_details, self).default_get(cr, uid, fields, context=context)
        picking_ids = context.get('active_ids', [])
        active_model = context.get('active_model')

        if not picking_ids or len(picking_ids) != 1:
            # Partial Picking Processing may only be done for one picking at a time
            return res
        assert active_model in ('stock.picking'), 'Bad context propagation'
        picking_id, = picking_ids
        picking = self.pool.get('stock.picking').browse(cr, uid, picking_id, context=context)
        items = []
        packs = []
        if not picking.pack_operation_ids:
            picking.do_prepare_partial()
        for op in picking.pack_operation_ids:
            if op.product_location:
                product_location = op.product_location
            else:
                product_location = op.product_id.x_products_location
            item = {
                'packop_id': op.id,
                'product_id': op.product_id.id,
                'product_uom_id': op.product_uom_id.id,
                'quantity': op.product_qty,
                'product_location': product_location,
                'package_id': op.package_id.id,
                'lot_id': op.lot_id.id,
                'sourceloc_id': op.location_id.id,
                'destinationloc_id': op.location_dest_id.id,
                'result_package_id': op.result_package_id.id,
                'date': op.date,
                'owner_id': op.owner_id.id,
            }
            if op.product_id:
                items.append(item)
            elif op.package_id:
                packs.append(item)
        res.update(item_ids=items)
        res.update(packop_ids=packs)
        return res

    @api.one
    def do_detailed_transfer(self):
        if self.picking_id.state not in ['assigned', 'partially_available']:
            raise Warning(_('You cannot transfer a picking in state \'%s\'.') % self.picking_id.state)
        processed_ids = []
        # Create new and update existing pack operations
        for lstits in [self.item_ids, self.packop_ids]:
            for prod in lstits:
                pack_datas = {
                    'product_id': prod.product_id.id,
                    'product_uom_id': prod.product_uom_id.id,
                    'product_qty': prod.quantity,
                    'product_location': prod.product_location,
                    'package_id': prod.package_id.id,
                    'lot_id': prod.lot_id.id,
                    'location_id': prod.sourceloc_id.id,
                    'location_dest_id': prod.destinationloc_id.id,
                    'result_package_id': prod.result_package_id.id,
                    'date': prod.date if prod.date else datetime.now(),
                    'owner_id': prod.owner_id.id,
                }
                if prod.packop_id:
                    prod.packop_id.with_context(no_recompute=True).write(pack_datas)
                    processed_ids.append(prod.packop_id.id)
                else:
                    pack_datas['picking_id'] = self.picking_id.id
                    packop_id = self.env['stock.pack.operation'].create(pack_datas)
                    processed_ids.append(packop_id.id)
        # Delete the others
        packops = self.env['stock.pack.operation'].search(['&', ('picking_id', '=', self.picking_id.id), '!', ('id', 'in', processed_ids)])
        packops.unlink()

        # Execute the transfer of the picking
        self.picking_id.do_transfer()


class stock_transfer_details_items(osv.osv):
    _inherit = 'stock.transfer_details_items'

    _columns = {
        'product_location': fields.char('Product Location'),
    }

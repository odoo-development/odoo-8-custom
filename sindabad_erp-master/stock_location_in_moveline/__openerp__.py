# -*- coding: utf-8 -*-
{
    'name': "Stock Location in Move_line",

    'summary': """
        This module will help to link stock location in each stock move """,

    'description': """
        This module will help to link stock location in each stock move
    """,

    'author': "S. M. Sazedul Haque - Zero Gravity Ventures Ltd",
    'website': "http://zerogravity.com.bd/",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/openerp/addons/base/module/module_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': [
            'odoo_magento_connect', 'stock',
    ],

    # always loaded
    'data': [
        'security/stock_location_in_moveline.xml',
        'security/ir.model.access.csv',
        'stock_location_in_moveline.xml',
    ],

}

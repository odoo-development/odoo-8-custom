

{
    'name': 'SO reference of Customer ID in view page',
    'version': '8.0',
    'author': 'Mufti Muntasir Ahmed',
    'category': 'sale',
    'depends': ['sale'],
    'data': [],
    'demo': [],
    'installable': True,
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:

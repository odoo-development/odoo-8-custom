# Author Mufti Muntasir Ahmed 29-07-2019

from openerp.osv import fields, osv
class SO(osv.osv):

    _inherit = 'sale.order'
    _columns = {
        'customer_ref_id': fields.many2one('res.partner', 'Customer reference on Order')
    
    }
# -*- coding: utf-8 -*-
##########################################################################
#
#    Copyright (c) 2018-Present Zero Gravity ventures Limited (<https://webkul.com/>)
#
##########################################################################

from datetime import date, datetime
from dateutil import relativedelta
import json
import time

from openerp.osv import fields, osv
from openerp.tools.float_utils import float_compare, float_round
from openerp.tools.translate import _
from openerp.tools import DEFAULT_SERVER_DATETIME_FORMAT, DEFAULT_SERVER_DATE_FORMAT
from openerp.exceptions import Warning
from openerp import SUPERUSER_ID, api
import openerp.addons.decimal_precision as dp
from openerp.addons.procurement import procurement
import logging
import sys

reload(sys)
sys.setdefaultencoding('utf8')


class stock_picking(osv.osv):
	_inherit = "stock.move"

	_columns = {
		 'reserved_date': fields.datetime('Date of Reserve', help="Date of Reserve"),
	}


class stock_move_picking(osv.osv):
	_inherit = "stock.picking"

	def _reservation_date(self, cursor, user, ids, name, arg, context=None):
		res = False
		res = {}

		for stock_pick in self.browse(cursor, user, ids, context=context):
			first_data=''
			stock_move_details_obj = self.pool.get("stock.move.details")
			init_data = stock_move_details_obj.search(cursor, user, [('picking_id','=', stock_pick.id)], order='reserved_date desc', context=context)
			if len(init_data) >0:
				first_data = stock_move_details_obj.browse(cursor, user, init_data[0], context=context).reserved_date
				res[stock_pick.id] = first_data
			else:
				res[stock_pick.id] = ''
		return res

	def _date_confirm(self, cursor, user, ids, name, arg, context=None):
		res = False
		res = {}
		sale_order_obj = self.pool.get("sale.order")

		for stock_pick in self.browse(cursor, user, ids, context=context):
			first_data=''
			init_data = sale_order_obj.search(cursor, user, [('name','=', stock_pick.origin)], context=context)
			first_data = sale_order_obj.browse(cursor, user, init_data[0], context=context).date_confirm
			res[stock_pick.id] = first_data
		return res



	_columns = {
	  'reserved_date': fields.function(_reservation_date, string='Reservation Date', type='datetime'),
	  'date_confirm': fields.function(_date_confirm, string='Confirmation Date', type='datetime'),

	  
	}

# ----------------------------------------------------
# Move
# ----------------------------------------------------

class stock_move_details(osv.osv):
	_name = "stock.move.details"
	_description = "Stock Move Details"
	# _order = 'date_expected desc, id'
	_log_create = False

	_columns = {
	  'backorder_id': fields.related('picking_id', 'backorder_id', type='many2one', relation="stock.picking", string="Back Order of", select=True),
	  'company_id': fields.many2one('res.company', 'Company', required=True, select=True),
	  'forcast_qty': fields.float('Forcast quantity', help="Forcast quantity"),
	  'inventory_id': fields.many2one('stock.inventory', 'Inventory'),
	  'location_id': fields.many2one('stock.location', 'Source Location', required=True, select=True, auto_join=True,
										 help="Sets a location if you produce at a fixed location. This can be a partner location if you subcontract the manufacturing operations."),
	  'move_orig_ids': fields.one2many('stock.move', 'move_dest_id', 'Original Move', help="Optional: previous stock move when chaining them", select=True),
	  'onhand_qty': fields.float('Quantity on hand'),
	  'ordered_qty': fields.float('Ordered Quantity'),
	  'origin': fields.char("Source"),
	  'partially_available': fields.boolean('Partially Available', readonly=True, help="Checks if the move has some stock reserved", copy=False),
	  'partner_id': fields.many2one('res.partner', 'Destination Address ', help="Optional address where goods are to be delivered, specifically used for allotment"),
	  'picking_id': fields.many2one('stock.picking', 'Reference', select=True),
	  'picking_type_id': fields.many2one('stock.picking.type', 'Picking Type'),
	  'price_unit': fields.float('Unit Price', help="Technical field used to record the product cost set by the user during a picking confirmation (when costing method used is 'average price' or 'real'). Value given in company currency and in product uom."),  # as it's a technical field, we intentionally don't provide the digits attribute
	  'product_id': fields.many2one('product.product', 'Product', required=True, select=True, domain=[('type', '<>', 'service')]),
	  # 'quant_ids': fields.many2many('stock.quant', 'stock_quant_move_rel', 'move_id', 'quant_id', 'Moved Quants', copy=False),
	  'remaining_qty': fields.float('Remain Quantity', help="Remaining Quantity in default UoM according to operations matched with this move"),
	  'reserved_date': fields.datetime('Date of Reserve', help="Date of Reserve"),
	  'reserved_qty': fields.float('Reserve availability', help="Reserved Quantity"),
	  'reserved_quant_ids': fields.one2many('stock.quant', 'reservation_id', 'Reserved quants'),
	  'returned_move_ids': fields.one2many('stock.move', 'origin_returned_move_id', 'All returned moves', help='Optional: all returned moves created from this move'),
	  'route_ids': fields.many2many('stock.location.route', 'stock_location_route_move', 'move_id', 'route_id', 'Destination route', help="Preferred route to be followed by the procurement order"),
	  'stock_move_id': fields.many2one('stock.move', 'Stock Move', select=True, search=True),
	  'unreserved_qty': fields.float('Unreserve availability', help="Un Reserved Quantity"),
	  'unreserve': fields.boolean('unreserved', readonly=True, help="Checks if the move has stock unreserved"),
	  'warehouse_id': fields.many2one('stock.warehouse', 'Warehouse', help="Technical field depicting the warehouse to consider for the route selection on the next procurement (if any)."),
	}

	_defaults = {
		'reserved_qty': 0,
		'unreserve': False,
		'unreserved_qty': 0,
	}

class stock_quant(osv.osv):
	"""
	Quants are the smallest unit of stock physical instances
	"""
	_inherit = "stock.quant"

	def quants_reserve(self, cr, uid, quants, move, link=False, context=None):

		toreserve = []
		reserved_availability = move.reserved_availability
		# split quants if needed
		for quant, qty in quants:
			if qty <= 0.0 or (quant and quant.qty <= 0.0):
				raise osv.except_osv(_('Error!'), _('You can not reserve a negative quantity or a negative quant.'))
			if not quant:
				continue
			self._quant_split(cr, uid, quant, qty, context=context)
			toreserve.append(quant.id)
			reserved_availability += quant.qty
		# reserve quants
		if toreserve:
			self.write(cr, SUPERUSER_ID, toreserve, {'reservation_id': move.id}, context=context)
			# if move has a picking_id, write on that picking that pack_operation might have changed and need to be recomputed
			if move.picking_id:
				self.pool.get('stock.picking').write(cr, uid, [move.picking_id.id], {'recompute_pack_op': True}, context=context)
		# check if move'state needs to be set as 'assigned'

		# Putting reserve_date during move reserve action------in stock.mov------
		reserved_date = datetime.now().strftime(DEFAULT_SERVER_DATETIME_FORMAT)

		rounding = move.product_id.uom_id.rounding
		stock_move_details = False
		if float_compare(reserved_availability, move.product_qty, precision_rounding=rounding) == 0 and move.state in ('confirmed', 'waiting'):
			self.pool.get('stock.move').write(cr, uid, [move.id], {'state': 'assigned', 'reserved_date': reserved_date}, context=context)
			stock_move_details = True
		elif float_compare(reserved_availability, 0, precision_rounding=rounding) > 0 and not move.partially_available:
			self.pool.get('stock.move').write(cr, uid, [move.id], {'partially_available': True, 'reserved_date': reserved_date}, context=context)
			stock_move_details = True

		if stock_move_details is True:
			stock_move = self.pool.get('stock.move').browse(cr, uid, move.id)
			print 'stock_move=', vars(stock_move)

			vals = {
				'backorder_id': stock_move.backorder_id.id,
				'company_id': stock_move.company_id.id,
				'forcast_qty': stock_move.product_id.virtual_available,
				'inventory_id': stock_move.inventory_id.id,
				'location_id': stock_move.location_id.id,
				'move_orig_ids': stock_move.move_orig_ids,
				'onhand_qty': stock_move.product_id.qty_available,
				'ordered_qty': stock_move.product_qty,
				'origin': stock_move.origin,
				'partially_available': stock_move.partially_available,
				'partner_id': stock_move.partner_id.id,
				'picking_id': stock_move.picking_id.id,
				'picking_type_id': stock_move.picking_type_id.id,
				'price_unit': stock_move.price_unit,
				'product_id': stock_move.product_id.id,
				'remaining_qty': (stock_move.product_id.qty_available-reserved_availability),
				'reserved_date': reserved_date,
				'reserved_qty': reserved_availability,
				'reserved_quant_ids': stock_move.reserved_quant_ids,
				'returned_move_ids': stock_move.returned_move_ids,
				'route_ids': stock_move.route_ids,
				'stock_move_id': move.id,
				'unreserved_qty': 0,
				'warehouse_id': stock_move.warehouse_id.id,
			}

			stock_move_details = self.pool.get('stock.move.details').search(cr, uid, [('stock_move_id', '=', move.id), ('unreserve', '=', False), ('reserved_qty', '=', reserved_availability)], context=context)

			if len(stock_move_details) == 0:
				self.pool.get('stock.move.details').create(cr, uid, vals, context=context)

	def quants_unreserve(self, cr, uid, move, context=None):
		# Putting reserve_date during move reserve action------in stock.mov------
		unreserved_date = datetime.now().strftime(DEFAULT_SERVER_DATETIME_FORMAT)
		related_quants = [x.id for x in move.reserved_quant_ids]
		if related_quants:
			#if move has a picking_id, write on that picking that pack_operation might have changed and need to be recomputed
			if move.picking_id:
				 self.pool.get('stock.picking').write(cr, uid, [move.picking_id.id], {'recompute_pack_op': True}, context=context)
			if move.partially_available:
				 self.pool.get("stock.move").write(cr, uid, [move.id], {'partially_available': False}, context=context)
			self.write(cr, SUPERUSER_ID, related_quants, {'reservation_id': False}, context=context)

			stock_move = self.pool.get('stock.move').browse(cr, uid, move.id)
			vals = {
				'backorder_id': stock_move.backorder_id.id,
				'company_id': stock_move.company_id.id,
				'forcast_qty': stock_move.product_id.virtual_available,
				'inventory_id': stock_move.inventory_id.id,
				'location_id': stock_move.location_id.id,
				'move_orig_ids': stock_move.move_orig_ids,
				'onhand_qty': stock_move.product_id.qty_available,
				'ordered_qty': stock_move.product_qty,
				'origin': stock_move.origin,
				'origin_returned_move_id': stock_move.origin_returned_move_id.id,
				'partially_available': stock_move.partially_available,
				'partner_id': stock_move.partner_id.id,
				'picking_id': stock_move.picking_id.id,
				'picking_type_id': stock_move.picking_type_id.id,
				'price_unit': stock_move.price_unit,
				'product_id': stock_move.product_id.id,
				'reserved_date': unreserved_date,
				'reserved_qty': 0,
				'reserved_quant_ids': stock_move.reserved_quant_ids,
				'restrict_lot_id': stock_move.restrict_lot_id.id,
				'restrict_partner_id': stock_move.restrict_partner_id.id,
				'returned_move_ids': stock_move.returned_move_ids,
				'route_ids': stock_move.route_ids,
				'stock_move_id': move.id,
				'unreserve': True,
				'unreserved_qty': stock_move.product_qty,
				'warehouse_id': stock_move.warehouse_id.id,
				}

			stock_move_details = self.pool.get('stock.move.details').search(cr, uid, [('stock_move_id', '=', move.id), ('unreserve', '=', True), ('unreserved_qty', '=', stock_move.product_qty)], context=context)

			if len(stock_move_details) == 0:
				self.pool.get('stock.move.details').create(cr, uid, vals, context=context)




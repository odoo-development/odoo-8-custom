# -*- coding: utf-8 -*-
{
    'name': "Reserve Date for stock product",

    'summary': """
        This module will create stock reserve date """,

    'description': """
        It will create stock reserve date when someone hit check availability button
    """,

    'author': "S. M. Sazedul Haque - Zero Gravity Ventures Ltd",
    'website': "http://zerogravity.com.bd/",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/openerp/addons/base/module/module_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': [
            'stock',
    ],

    # always loaded
    'data': [
        'security/stock_move_details_security.xml',
        'security/ir.model.access.csv',
        'stock_move_history_view.xml',
    ],

}

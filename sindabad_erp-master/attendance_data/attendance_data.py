from openerp.osv import fields, osv


class attendance_data(osv.osv):
    _name = "attendance.data"
    _description = "Attendance Data"
    _order = 'id desc'

    _columns = {
        'emp_id': fields.char('Employee ID'),
        'name': fields.char('User Name'),
        'shift': fields.char('Shift'),
        'department': fields.char('Department'),
        'first_in_time': fields.float('First-In Time'),
        'last_out_time': fields.float('Last-Out Time'),
        'date': fields.date('Date'),
        'result': fields.text('Result'),
        'worktime': fields.char(string='WorkTime'),
        'confirm_time': fields.datetime('Confirmation Time'),
        'cancel_time': fields.datetime('Cancel Time'),
        'state': fields.selection([
            ('pending', 'Pending'),
            ('confirm', 'Confirmed'),
            ('cancel', 'Cancelled')

        ], 'Status', readonly=True, copy=False, help="Gives the status of the Attendance Data control", select=True),

        'cancel_attendance_date': fields.datetime('Cancel Attendance Date'),
        'cancel_attendance_by': fields.many2one('res.users', 'Cancel Attendance By'),
        'cancel_attendance_reason': fields.text('Cancel Attendance Reason'),
    }

    _defaults = {
        'user_id': lambda obj, cr, uid, context: uid,
        'state': 'pending',
    }

    def confirm_attend(self, cr, uid, ids, context=None):

        for single_id in ids:
            confirm_attend_query = "UPDATE attendance_data SET state='confirm', confirm_time='{0}' WHERE id={1}".format(
                str(fields.datetime.now()), single_id)
            cr.execute(confirm_attend_query)
            cr.commit()

        return True

    def cancel_attend(self, cr, uid, ids, context=None):

        for single_id in ids:
            cancel_attend_query = "UPDATE attendance_data SET state='cancel', cancel_time='{0}' WHERE id={1}".format(
                str(fields.datetime.now()), single_id)
            cr.execute(cancel_attend_query)
            cr.commit()

        return True


class CancelAttendanceReason(osv.osv):
    _name = "cancel.attendance.reason"
    _description = "Cancel Attendance Reason"

    _columns = {
        'cancel_attendance_date': fields.datetime('Cancel Attendance Date'),
        'cancel_attendance_by': fields.many2one('res.users', 'Cancel Attendance By'),
        'cancel_attendance_reason': fields.text('Cancel Attendance Reason', required=True),
    }

    def cancel_attendance_reason_def(self, cr, uid, ids, context=None):
        ids = context['active_ids']
        cancel_attendance_reason = str(context['cancel_attendance_reason'])
        cancel_attendance_by = uid
        cancel_attendance_date = str(fields.datetime.now())

        attendance_obj = self.pool.get('attendance.data')

        for s_id in ids:
            cancel_attendance_query = "UPDATE attendance_data SET cancel_attendance_reason='{0}', cancel_attendance_by={1}, cancel_attendance_date='{2}' WHERE id={3}".format(
                cancel_attendance_reason, cancel_attendance_by, cancel_attendance_date, s_id)
            cr.execute(cancel_attendance_query)
            cr.commit()

            attendance_obj.cancel_attend(cr, uid, [s_id], context)

        return True

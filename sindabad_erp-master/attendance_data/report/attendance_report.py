# Author Mufti Muntasir Ahmed 26-02-2018

import time
from openerp.report import report_sxw
from openerp.osv import fields, osv
from datetime import datetime, timedelta


class MTAttendanceReport(report_sxw.rml_parse):
    def __init__(self, cr, uid, name, context):
        super(MTAttendanceReport, self).__init__(cr, uid, name, context=context)
        self.localcontext.update({
            'get_client_order_ref': self.get_test
        })

        self.context = context

    def get_test(self):
        return 'dfdf'

    def set_context(self, objects, data, ids, report_type=None):
        ids = self.context.get('active_ids')
        attend_obj = self.pool['attendance.data']
        context = self.context
        docs = attend_obj.browse(self.cr, self.uid, ids, context)

        text = ''
        text_output = {}

        st_date = data.get('form').get('date_from')
        end_date = data.get('form').get('date_to')
        context['start_date'] = st_date
        context['end_date'] = end_date

        for emp in docs:
            text_output[emp.id] = text

        self.localcontext.update({
            'docs': docs,
            'time': time,
            'getLines': self._lines_get,
            'date_range': str(st_date) + ' to ' + str(end_date),
            'text': text_output
        })

        self.context = context

        self.emp_ids = [res['emp_id'] for res in self.cr.dictfetchall()]

        return super(MTAttendanceReport, self).set_context(objects, data, self.emp_ids, report_type)

    def _lines_get(self, emp):

        start_date = self.context.get('start_date')
        end_date = self.context.get('end_date')
        att_data_obj = self.pool['attendance.data']
        att_lines = att_data_obj.search(self.cr, self.uid,[('date', '<=', end_date),('date', '>=', start_date)])
        att_lines = att_data_obj.browse(self.cr, self.uid, att_lines)

        emp_id_list =[]

        final_result = []

        for items in att_lines:
            if items.emp_id not in emp_id_list:
                emp_id_list.append(items.emp_id)

        emp_id_list = list(dict.fromkeys(emp_id_list))


        for emp_id in emp_id_list:
            emp_dict ={'emp_id':emp_id}
            tmp_ei = 0
            tmp_eo = 0
            tmp_li = 0
            tmp_lo = 0
            tmp_ab = 0
            tmp_lv = 0
            tmp_twd = 0
            tmp_wkh = 0
            tmp_oth = 0
            tmp_missing=0
            tmp_non_wk_day=0
            for items in att_lines:
                if str(items.emp_id) == str(emp_id):
                    try:
                        emp_dict['name']=items.name
                        emp_dict['department']=items.department
                        emp_dict['shift']=items.shift
                        if str(items.result) == str('Absence'):
                            tmp_ab +=1
                        elif str(items.result) == str('Late In'):
                            tmp_li +=1

                            if str(items.shift) == str('Staff ') or str(items.shift) == str('Staff'):
                                worked = abs(items.first_in_time - items.last_out_time)

                                tmp_wkh = tmp_wkh + abs(items.first_in_time - items.last_out_time)

                                tmp_oth = tmp_oth + round((worked % 10), 2)
                            else:

                                start_in_time = str(items.shift).split('Regular ')[1].split('-')[0]
                                end_time = str(items.shift).split('Regular ')[1].split('-')[1]
                                last_time = float(float(end_time) + 12.00)


                                worked = abs(items.first_in_time - items.last_out_time)

                                tmp_wkh = tmp_wkh + abs(items.first_in_time - items.last_out_time)

                                tmp_oth = tmp_oth + round((worked % 9), 2)


                        elif str(items.result) == str('Missing Out'):
                            tmp_missing +=1
                        elif str(items.result) == str('Non Working day'):
                            tmp_non_wk_day +=1

                        elif str(items.result) == str('Normal'):
                            if str(items.shift) ==str('Staff ') or str(items.shift) ==str('Staff'):
                                worked = abs(items.first_in_time - items.last_out_time)

                                tmp_wkh = tmp_wkh + abs(items.first_in_time - items.last_out_time)

                                tmp_oth = tmp_oth + round((worked % 10), 2)
                            else:

                                start_in_time = str(items.shift).split('Regular ')[1].split('-')[0]
                                end_time = str(items.shift).split('Regular ')[1].split('-')[1]
                                last_time = float(float(end_time) + 12.00)


                                if float(start_in_time) > items.first_in_time:
                                    tmp_ei +=1

                                if last_time  > items.last_out_time:
                                    tmp_eo +=1

                                if float(last_time) < items.last_out_time:
                                    tmp_lo += 1

                                worked = abs(items.first_in_time - items.last_out_time)

                                tmp_wkh = tmp_wkh + abs(items.first_in_time - items.last_out_time)

                                tmp_oth = tmp_oth + round((worked%9),2)
                    except:
                        pass

            emp_dict['tmp_ei'] = tmp_ei
            emp_dict['tmp_eo'] = tmp_eo
            emp_dict['tmp_li'] = tmp_li
            emp_dict['tmp_lo'] = tmp_lo
            emp_dict['tmp_ab'] = tmp_ab
            emp_dict['tmp_lv'] = tmp_lv
            emp_dict['tmp_twd'] = tmp_twd
            emp_dict['tmp_wkh'] = tmp_wkh
            emp_dict['tmp_oth'] = tmp_oth
            emp_dict['tmp_missing'] = tmp_missing
            emp_dict['tmp_non_wk_day'] = tmp_non_wk_day

            final_result.append(emp_dict)


        return final_result


class mt_attendance_report(osv.AbstractModel):
    _name = 'report.attendance_data.mt_attendance_report'
    _inherit = 'report.abstract_report'
    _template = 'attendance_data.mt_attendance_report'
    _wrapped_report_class = MTAttendanceReport

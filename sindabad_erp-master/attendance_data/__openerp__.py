{
    'name': "Attendance Data Module",
    'version': '1.0',
    'category': 'Information Technology',
    'author': "Md Rockibul Alam Babu",
    'description': """
Attendance Data Module for all users.
    """,
    'website': "http://www.sindabad.com",

    'depends': [
        'base', 'report'
    ],

    'data': [
        'security/attendance_data_security.xml',
        'security/ir.model.access.csv',
        'wizard/cancel_attendance_reason_view.xml',
        'views/attendance_data_view.xml',
        'views/cancel_attendance_button.xml',
        'attendance_menu.xml',
        'attendance_report_menu.xml',
        'wizard/mt_attendance_report_filter_view.xml',
        'views/mt_attendance_report_view.xml',
    ],
    'installable': True,
    'auto_install': False,
}

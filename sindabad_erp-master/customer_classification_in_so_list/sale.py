# Mufti Muntasir Ahmed 10-05-2018
from openerp.osv import fields, osv
from openerp.tools.translate import _
import openerp.addons.decimal_precision as dp

from openerp import models, api
from openerp.exceptions import Warning
from datetime import datetime



class sale_order(osv.osv):
    _inherit = "sale.order"

    # def _search_customer_classification(self, cr, uid, obj, name, args, context=None):
    #
    #     res = []
    #     ids = obj.search(cr, uid, [ ('state', 'not in', ['draft', 'sent', 'cancel', 'new_cus_approval_pending', 'approval_pending', 'check_pending'])])  # ids of visits
    #     for field, operator, value in args:
    #         for v in  self.browse(cr, uid, ids, context=context):  # foreach visit
    #             classification = v.partner_id.x_classification.lower() if v.partner_id.x_classification else ''
    #
    #             var_classification = value.lower() if value else ''
    #             if operator=='ilike':
    #                 if var_classification in classification:
    #                     res.append(v.id)
    #             elif operator=='=':
    #                 if var_classification == classification:
    #                     res.append(v.id)
    #     return [('id', 'in', res)]



    # delivered amount
    def _get_customer_classification(self, cursor, user, ids, name, arg, context=None):
        res = False
        res = {}

        for sale in self.browse(cursor, user, ids, context=context):
            res[sale.id] = sale.partner_id.x_classification
        return res

    def _get_customer_inddustry(self, cursor, user, ids, name, arg, context=None):
        res = False
        res = {}

        for sale in self.browse(cursor, user, ids, context=context):
            res[sale.id] = sale.partner_id.x_industry
        return res



    _columns = {

        # 'customer_classification': fields.function(_get_customer_classification, string='Customer Classification',
        #                                    type='char', fnct_search=_search_customer_classification),
        'customer_classification': fields.function(_get_customer_classification, string='Customer Classification', type='char',store=True),
        'customer_industry': fields.function(_get_customer_inddustry, string='Customer Industry', type='char'),


    }

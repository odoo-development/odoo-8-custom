from datetime import datetime, timedelta
import time
from datetime import date
from openerp import SUPERUSER_ID
from openerp.tools.translate import _
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT
import openerp.addons.decimal_precision as dp
from openerp import workflow
from openerp import models, fields, api
import datetime
from time import gmtime, strftime
from openerp.exceptions import except_orm, Warning, RedirectWarning
from openerp.osv import fields, osv


class partial_delivery_create(osv.osv_memory):
    _inherit = "partial.delivery.create"

    def create(self, cr, uid, vals, context=None):

        sp_obj = self.pool.get('stock.picking')
        stock_picking = sp_obj.browse(cr, 1, context['active_ids'], context=context)

        odoo_so = str(stock_picking.origin)

        so_obj = self.pool.get('sale.order')
        so_list = so_obj.search(cr, 1, [('name', '=', odoo_so)])
        so = so_obj.browse(cr, 1, so_list)

        res_partial_obj = self.pool.get('restricts.partial.delivery')
        res_partial = res_partial_obj.browse(cr, 1, 1)

        if so.amount_total < res_partial.amount:
                # show warning
                raise Warning(_('You cannot transfer partial. Order amount less than {0}.').format(res_partial.amount))
        else:
            
            res = super(partial_delivery_create, self).create(cr, uid, vals, context=context)

            return res


class stock_transfer_details(models.TransientModel):
    _inherit = 'stock.transfer_details'

    @api.one
    def do_detailed_transfer(self):

        # self.picking_id.origin
        # u'SO75209'
        odoo_order_number = str(self.picking_id.origin)

        if odoo_order_number.startswith('SO'):

            res_partial_obj = self.pool.get('restricts.partial.delivery')
            res_partial = res_partial_obj.browse(self._cr, 1, 1)

            """
            {"sku": order_quantity}
            """
            order_picking_map = dict()
            
            so_obj = self.pool.get('sale.order')
            so_list = so_obj.search(self._cr, 1, [('name', '=', odoo_order_number)])
            so = so_obj.browse(self._cr, 1, so_list)

            for product in so.order_line:

                order_picking_map[str(product.default_code)] = product.product_uom_qty

            # self.item_ids total price
            total_picking_price = 0
            restrict_order = False

            for item in self.item_ids:

                price = item.product_id.lst_price
                sku = str(item.product_id.default_code)
                quantity = item.quantity

                total_picking_price += price * quantity

                if order_picking_map[sku] < quantity:
                    restrict_order = True

            if so.amount_total < res_partial.amount and restrict_order:
                # show warning
                raise Warning(_('You cannot transfer partial. Order amount less than {0}.').format(res_partial.amount))
            else:
                super(stock_transfer_details, self).do_detailed_transfer()
        else:
            super(stock_transfer_details, self).do_detailed_transfer()

        return True

class restricts_partial_delivery(osv.osv):
    _name = 'restricts.partial.delivery'
    _description = "Restricts Partial Delivery"

    _columns = {
        'amount': fields.float('Amount', help="Partial Delivery restrict amount"),
        'line_item_count': fields.float('Line Item Count', help="Partial Delivery restrict Line Item Count"),
        'item_quantity': fields.float('Item Quantity', help="Partial Delivery restrict Item Quantity"),
        }

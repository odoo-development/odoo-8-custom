{
    'name': 'Partial Delivery Restricts',
    'version': '8.0.0.6.0',
    'author': "Shahjalal Hossain",
    'category': 'SMS',
    'summary': 'WMS Manifest Extends',
    'depends': ['base', 'odoo_magento_connect', 'account', 'sale', 'purchase', 'wms_manifest', 'send_sms_on_demand'],
    'data': [
        'restricts_partial_delivery.xml',
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}

import time
from openerp.osv import osv
from openerp.report import report_sxw


class reo_quantity(report_sxw.rml_parse):

    def __init__(self, cr, uid, name, context):
        super(reo_quantity, self).__init__(cr, uid, name, context)
        self.localcontext.update({
            'get_reorder': self._get_reorder_qty,
        })

    def _get_reorder_qty(self, obj):

        all_products = self.pool['product.product'].browse(self.cr, self.uid,self.pool['product.product'].search(self.cr, self.uid, []))

        return all_products if all_products else '-'


class report_roq(osv.AbstractModel):
    _name = 'report.reorder_quantity_report.report_roq'
    _inherit = 'report.abstract_report'
    _template = 'reorder_quantity_report.report_roq'
    _wrapped_report_class = reo_quantity



from openerp.osv import fields, osv
import openerp.addons.decimal_precision as dp


class reorder_quantity(osv.osv):
    _inherit = 'product.template'

    _columns = {
        'reorder_qty': fields.float('Reorder Quantity', digits_compute=dp.get_precision('Product Quantity'), help="Products reorder quantity."),
        }


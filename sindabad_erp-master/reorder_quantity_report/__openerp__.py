{
    'name': 'Reorder Quantity Report',
    'version': '1.0',
    'author': 'Md Rockibul Alam Babu',
    'description': """
        Product variants reorder quantity field and report generation .
    """,
    'category': 'Product',
    'depends': ['product'],
    'data': [
        'views/reorder_qty.xml',
        'report_menu.xml',
        'views/report_roq.xml'
    ],
    'auto_install': False,
    'installable': True,
}

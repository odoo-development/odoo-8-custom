{
    'name': 'WMS RTV',
    'version': '1.0',
    'category': 'WMS',
    'author': 'Shuvarthi Dhar',
    'summary': 'WMS RTV Process',
    'description': 'WMS RTV Process',
    'depends': ['base', 'wms_inbound'],
    'data': [
        'security/wms_rtv_process_security.xml',
        'security/ir.model.access.csv',

        'rtv/wizard/cancel_rtv_process.xml',
        'rtv/wizard/cancel_pending_rtv_process.xml',
        'rtv/wizard/resolve_rtv_process.xml',

        'srn/wizard/cancel_srn_view.xml',
        'srn/wizard/not_receive_srn_view.xml',
        'srn/wizard/not_receive_srn_line_view.xml',
        'srn/wizard/receive_rtv_view.xml',

        'rtv/rtv_view.xml',
        'rtv/srn_create.xml',
        'rtv/pqc_to_rtv_create.xml',

        'rtv/report/rtv_report_menu.xml',
        'rtv/report/report_rtv_ticket_layout.xml',
        'rtv/report/report_rtv_layout.xml',

        'srn/report/srn_report_menu.xml',
        'srn/report/srn_report_view.xml',

        'srn/srn_view.xml',

        'rtv_end_to_end/rtv_end_to_end_view.xml',

        'rtv_process_menu.xml',

    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}

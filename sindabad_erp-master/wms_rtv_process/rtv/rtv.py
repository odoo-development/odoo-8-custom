import itertools
from openerp.osv import fields, osv
from openerp.tools.translate import _
from openerp import api
from time import gmtime, strftime
from openerp.exceptions import except_orm, Warning, RedirectWarning


class rtv_process(osv.osv):
    _name = "rtv.process"
    _description = "RTV Process"


    def _count_all(self, cr, uid, ids, field_name, arg, context=None):
        return {
            rtv.id: {
                'invoice_count': len(rtv.invoice_id),
            }
            for rtv in self.browse(cr, uid, ids, context=context)
        }


    _columns = {
        'name': fields.char('RTV Number'),
        'warehouse_id': fields.selection([
            (1, 'Central Warehouse Uttara'),
            (2, 'Nodda Warehouse'),
            (3, 'Nodda Retail Warehouse'),
            (4, 'Motijheel Warehouse'),
            (6, 'Uttara Retail Warehouse'),
            (7, 'Motijheel Retail Warehouse'),
            (8, 'Damage Warehouse'),
            (19, 'Comilla Warehouse'),
            (27, 'Badda Warehouse'),
            (21, 'Signboard Warehouse'),
            (37, 'Savar Warehouse'),

        ], 'Warehouse', readonly=True, copy=False, help="Gives the List of the Warehouses", select=True),
        'warehouse_name': fields.char('Warehouse Name'),
        'rtv_type': fields.selection([
           ('pqc', 'PQC'),
           ('stock', 'Stock')

        ], 'RTV Type', readonly = True, copy = False, help = "Gives the RTV Type ", select = True),
        'vendor': fields.many2one('res.partner', 'Supplier', domain="[('supplier','=',True)]"),
        'vendor_address': fields.char('Address'),
        'vendor_contact': fields.char('Contact Number'),
        'area': fields.many2one("res.country.state", 'Area'),
        'remark': fields.text('Remark'),
        'po_number': fields.char('Ref PO Number'),
        'po_id': fields.many2one("purchase.order", 'PO ID'),
        'pqc': fields.boolean('PQC'),
        'pqc_id': fields.many2one("pending.quality.control", 'PQC ID'),
        'pqc_number': fields.char('Ref PQC Number'),
        'pqc_processed': fields.boolean('PQC Processed'),
        'delivered': fields.boolean('Delivered'),
        'reattempted': fields.boolean('Reattempted'),
        'reattempt_count': fields.integer(string="Re-attempt Count"),
        'reattempt_by': fields.many2one('res.users', 'Reattempt By'),
        'reattempt_time': fields.datetime('Reattempt Time'),
        'reattempted_cancel': fields.boolean('Reattempted Cancel'),
        'received_by': fields.many2one('res.users', 'Received By'),
        'received_time': fields.datetime('Received Time'),
        'confirmed_by': fields.many2one('res.users', 'Confirmed By'),
        'confirmed_time': fields.datetime('Confirmed Time'),
        'requested_by': fields.many2one('res.users', 'Requested By'),
        'requested_time': fields.datetime('Requested Time'),
        'resolved': fields.boolean('Resolved'),
        'resolved_by': fields.many2one('res.users', 'Resolved By'),
        'resolved_time': fields.datetime('Resolved Time'),
        'approved_by': fields.many2one('res.users', 'Approved By'),
        'approved_time': fields.datetime('Approved Time'),
        'damage': fields.boolean('Damage'),
        'cancel': fields.boolean('Cancel'),
        'cancel_by': fields.many2one('res.users', 'Cancel By'),
        'cancel_time': fields.datetime('Cancel Time'),
        'state': fields.selection([
            ('pending', 'Pending'),
            ('requested', 'Requested'),
            ('resolved', 'Resolved'),
            ('confirmed', 'Ready for Return'),
            ('create_srn', 'SRN'),
            ('received', 'Received'),
            ('not_received', 'Not Received'),
            ('pending_approval', 'Pending Approval'),
            ('approved', 'Approved'),
            ('cancel', 'Cancelled'),

        ], 'Status', readonly=True, copy=False, help="Gives the status of the RTV Process", select=True),

        'rtv_process_line': fields.one2many('rtv.process.line', 'rtv_process_id', 'RTV Process Line', required=True),
        'stock_id': fields.many2one('stock.picking', 'Stock Number'),
        'invoice_id': fields.many2one('account.invoice', 'Invoice Number'),
        'aqc_id': fields.many2one('accepted.quality.control', 'AQC Number'),
        'reverse_stock_id': fields.many2one('stock.picking', 'Reverse Stock Number'),
        'reverse_pqc_id': fields.many2one('pending.quality.control', 'Reverse PQC Number'),

        'resolve_rtv_process_date': fields.datetime('Resolve rtv Process Date'),
        'resolve_rtv_process_by': fields.many2one('res.users', 'Resolve rtv Process By'),
        'resolve_rtv_process_reason': fields.text('Resolve rtv Process Reason'),

        'cancel_rtv_process_date': fields.datetime('Cancel rtv Process Date'),
        'cancel_rtv_process_by': fields.many2one('res.users', 'Cancel rtv Process By'),
        'cancel_rtv_process_reason': fields.text('Cancel rtv Process Reason'),

        'not_received_by': fields.many2one('res.users', 'Not Received By'),
        'not_received_time': fields.datetime('Not Received Time'),

        # 'not_receive_reason': fields.selection([
        #     ("Vendor was unreachable", "Vendor was unreachable"),
        #     ("Vendor refused to provide", "Vendor refused to provide"),
        #     ("Unable to reach", "Unable to reach"),
        #     ("Vendors place was closed", "Vendors place was closed"),
        #     ("Cash problem", "Cash problem"),
        #     ("Vendor rescheduled", "Vendor rescheduled"),
        #     ("Wrong address given", "Wrong address given"),
        #     ("Unable to reach on Time", "Unable to reach on Time"),
        #     ("Quality issue", "Quality issue"),
        #     ("Due to traffic issue", "Due to traffic issue")
        #
        # ], 'Not Receive Reason', copy=False, help="Reason", select=True),

        'not_receive_reason': fields.selection([
            ("Shop off", "Shop off"),
            ("Vendor unreachable", "Vendor unreachable"),
            ("Vendor Rescheduled", "Vendor Rescheduled"),
            ("Does not agree to take", "Does not agree to take"),
            ("Not received due to quality", "Not received due to quality"),
            ("Vendor is not aware of", "Vendor is not aware of")

        ], 'Not Receive Reason', copy=False, help="Reason", select=True),

        'srn_number': fields.char('SRN Number'),
        'srn_rtv_number': fields.char('Challan Number'),

        'invoice_count': fields.function(_count_all, type='integer', string='Invoices', multi=True)
    }


    _defaults = {
        'user_id': lambda obj, cr, uid, context: uid,
        'state': 'pending',
        'rtv_type': 'stock',
    }


    def onchange_vendor_info(self, cr, uid, ids, vendor_id=False, context=None):
        values={}
        values['vendor_address']=''
        if vendor_id is not False:
            vendor_obj = self.pool.get('res.partner')
            uid = 1
            vendor_search = vendor_obj.search(cr, uid, [('id', '=', vendor_id)], context=context)
            vendor = vendor_obj.browse(cr, uid, vendor_search, context=context)

            if vendor.street:
                values['vendor_address'] = values['vendor_address'] + vendor.street + ", "
            if vendor.street2:
                values['vendor_address'] = values['vendor_address'] + vendor.street2 + ", "
            if vendor.city:
                values['vendor_address'] = values['vendor_address'] + vendor.city + ", "
            if vendor.state_id:
                values['vendor_address'] = values['vendor_address'] + vendor.state_id.name + ", "
            if vendor.zip:
                values['vendor_address'] = values['vendor_address'] + vendor.zip + ", "
            if vendor.country_id:
                values['vendor_address'] = values['vendor_address'] + vendor.country_id.name
            if vendor.mobile:
                values['vendor_contact'] = vendor.mobile if vendor.mobile  else vendor.phone

        return {'value': values}

    def invoice_open(self, cr, uid, ids, context=None):
        mod_obj = self.pool.get('ir.model.data')
        act_obj = self.pool.get('ir.actions.act_window')

        result = mod_obj.get_object_reference(cr, uid, 'account', 'action_invoice_tree2')
        id = result and result[1] or False
        result = act_obj.read(cr, uid, [id], context=context)[0]

        for rtv in self.browse(cr, uid, ids, context=context):
            inv_ids= rtv.invoice_id.id
            if not inv_ids:
                raise osv.except_osv(_('Error!'), _('Please create Invoices.'))

            else:
                res = mod_obj.get_object_reference(cr, uid, 'account', 'invoice_supplier_form')
                result['views'] = [(res and res[1] or False, 'form')]
                result['res_id'] = inv_ids or False
        return result

    def request_rtv(self, cr, uid, ids, context=None):

        request_user = uid

        id = ids[0] if len(ids) > 0 else None
        rtv_data = self.browse(cr, uid, id, context=context)

        if rtv_data.pqc==False:
            stock_picking_type_ids = self.pool['stock.picking.type'].search(cr, uid, [('name', 'like', 'Delivery Orders')], context=context)
            stock_picking_type_data = self.pool['stock.picking.type'].browse(cr, uid, stock_picking_type_ids, context=context)
            location_id = None
            location_dest_id = None
            picking_type_id = None

            for items in stock_picking_type_data:

                if items.warehouse_id.id == rtv_data.warehouse_id:
                    location_id = items.default_location_src_id.id
                    picking_type_id = items.id
                    location_dest_id = self.pool['stock.location'].search(cr, uid, [('name', '=', 'Suppliers')],
                                                                            context=context)[0]
            move_line = []

            stock_data = {}

            if location_id is not None and location_dest_id is not None:
                stock_data = {'origin': rtv_data.name,
                              'message_follower_ids': False,
                              'carrier_tracking_ref': False,
                              'number_of_packages': 0,
                              'date_done': False,
                              'carrier_id': False,
                              'write_uid': False,
                              'partner_id':  rtv_data.vendor.id,
                              'message_ids': False,
                              'note': False,
                              'picking_type_id': picking_type_id,
                              'move_type': 'one',
                              'company_id': 1,
                              'priority': '1',
                              'picking_type_code': False,
                              'owner_id': False,
                              'min_date': False,
                              'date': strftime("%Y-%m-%d %H:%M:%S", gmtime()),
                              'pack_operation_ids': [],
                              'rtv':True,
                              'carrier_code': 'custom',
                              'invoice_state': '2binvoiced'}
                tmp_dict = {}
                for rtv_line in rtv_data.rtv_process_line:
                    move_line_data = {
                               'origin': rtv_data.name,
                               'product_uos_qty': rtv_line.requested_qty,
                               'date_expected': strftime("%Y-%m-%d %H:%M:%S", gmtime()),
                               'date': strftime("%Y-%m-%d %H:%M:%S", gmtime()),
                               'product_id': rtv_line.product_id.id,
                               'product_uom': 1,
                               'picking_type_id': picking_type_id,
                               'product_uom_qty': rtv_line.requested_qty,
                               'invoice_state': '2binvoiced',
                               'product_tmpl_id': False,
                               'product_uos': False,
                               'reserved_quant_ids': [],
                               'location_dest_id': location_dest_id,
                               'procure_method': 'make_to_stock',
                               'product_packaging': False,
                               'group_id': False,
                               'location_id': location_id,
                               'price_unit': rtv_line.unit_cost,
                               'name': str(rtv_line.product_id.name)}
                    # if rtv_line.unit_cost > float(0):
                    #     move_line_data.update({'price_unit':rtv_line.unit_cost})
                    move_line.append([0, False,move_line_data])

                stock_data['move_lines'] = move_line

            stock_obj = self.pool.get('stock.picking')
            save_the_data = stock_obj.create(cr, uid, stock_data, context=context)
            if save_the_data:
                stock_obj_confirmation = stock_obj.action_confirm(cr, uid, [save_the_data], context=context)
                stock_obj = stock_obj.action_assign(cr, uid, [save_the_data], context=context)


            for single_id in ids:
                requested_rtv_query = "UPDATE rtv_process SET state='requested', requested_time='{0}', stock_id='{1}', requested_by='{2}' WHERE id={3}".format(
                    str(fields.datetime.now()),save_the_data, request_user, single_id)
                cr.execute(requested_rtv_query)
                cr.commit()

                requested_rtv_query_line = "UPDATE rtv_process_line SET state='requested', requested_time='{0}' WHERE rtv_process_id={1}".format(
                    str(fields.datetime.now()), single_id)
                cr.execute(requested_rtv_query_line)
                cr.commit()

        else:
            for single_id in ids:
                requested_rtv_query = "UPDATE rtv_process SET state='requested', requested_by={0},requested_time='{1}' WHERE id={2}".format(
                    uid,str(fields.datetime.now()), single_id)
                cr.execute(requested_rtv_query)
                cr.commit()

                requested_rtv_query_line = "UPDATE rtv_process_line SET state='requested', confirmed_time='{0}' WHERE rtv_process_id={1}".format(
                    str(fields.datetime.now()), single_id)
                cr.execute(requested_rtv_query_line)
                cr.commit()

        self.pool.get('rtv.end.to.end').rtv_request_update(cr, uid, rtv_data.name, rtv_data.id,str(fields.datetime.now()),'requested',context=context)
        self.email_template(cr, uid, 'RTV Ticket', id, context)
        return True

    def create_aqc(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        aqc_line = []

        rtv_obj = self.pool.get('rtv.process').browse(cr, uid, ids, context=context)

        for rtv_data in rtv_obj:

            pqc_obj = self.pool.get('pending.quality.control').browse(cr, uid,self.pool['pending.quality.control'].search(cr,uid, [('pqc_number','=',str(rtv_data.pqc_number))]))

            put_data = data = {'po_id': pqc_obj.po_id,
                               'po_number': str(pqc_obj.po_number),
                               'irn_number': rtv_data.pqc_id.irn_number,
                               'irn_id': rtv_data.pqc_id.irn_id,
                               'qc_id': rtv_data.pqc_id.qc_id,
                               'warehouse_id': rtv_data.warehouse_id,
                               'vendor': rtv_data.vendor.id,
                               'area': rtv_data.area.id,
                               'state': 'confirm',
                               'qc_number': pqc_obj.qc_number,
                               'received_by': uid,
                               'pqc_number': str(rtv_data.pqc_number),
                               'date_confirm': strftime("%Y-%m-%d %H:%M:%S", gmtime()),
                               'pqc_id': rtv_data.pqc_id
                               }

            for rtv_product in rtv_data.rtv_process_line:
                for pqc_data in pqc_obj.pqc_line:
                    if rtv_product.product_id == pqc_data.product_id:
                        aqc_line.append([0, False, {
                            'rtv_id': rtv_data.id,
                            'rtv_name': rtv_data.name,
                            'qc_id': pqc_obj.qc_id,
                            'product': rtv_product.product_id.name,
                            'product_id': rtv_product.product_id.id,
                            'purchase_quantity': pqc_data.purchase_quantity,
                            'received_quantity': rtv_product.return_qty,
                            'accepted_quantity': rtv_product.return_qty,
                            'pending_quantity': rtv_product.requested_qty,
                            'state': 'confirm'
                        }])

            data['aqc_line'] = aqc_line

            if len(aqc_line) > 0:
                # Create aqc
                save_the_data = self.pool.get('accepted.quality.control').create(cr, uid, data, context=context)

                # Generate Put away Process
                put_line_list = []

                for rtv_product in rtv_data.rtv_process_line:
                    if rtv_product.return_qty > 0:
                        put_line_list.append([0, False,
                                              {
                                                  'product_id': rtv_product.product_id.id,
                                                  'qc_id': save_the_data,
                                                  'location': '',
                                                  'product': rtv_product.product_id.name,
                                                  'remarks': '',
                                                  'quantity': rtv_product.return_qty,
                                                  'po_number': str(pqc_obj.po_number)

                                              }])

                put_data['aqc_id'] = save_the_data
                put_data['state'] = 'pending'
                put_data['putaway_line_view'] = put_line_list
                put_env = self.pool.get('putaway.process')
                saved_put_id = put_env.create(cr, uid, put_data, context=context)

                rtv_query = "UPDATE rtv_process SET aqc_id='{0}' WHERE id={1}".format(
                    save_the_data, rtv_data.id)
                cr.execute(rtv_query)
                cr.commit()

        return True

    def resolve_rtv(self, cr, uid, ids, context=None):

        rtv_obj = self.browse(cr, uid, ids[0], context=context)

        if rtv_obj.pqc==False:
            if rtv_obj.stock_id.id is not None or False:
                # Cancel Action
                self.pool.get('stock.picking').do_unreserve(cr, uid, int(rtv_obj.stock_id.id), context=context)

                self.pool.get('stock.picking').action_cancel(cr, uid, int(rtv_obj.stock_id.id), context)

        else:
            self.create_aqc(cr, uid,ids, context=context)

        for single_id in ids:
            resolve_rtv_query = "UPDATE rtv_process SET state='resolved',resolved=True,stock_id=Null, resolved_by={0},resolved_time='{1}' WHERE id={2}".format(
                uid,str(fields.datetime.now()), single_id)
            cr.execute(resolve_rtv_query)
            cr.commit()

            resolve_rtv_query_line = "UPDATE rtv_process_line SET state='resolved', resolved_time='{0}' WHERE rtv_process_id={1}".format(
                str(fields.datetime.now()), single_id)
            cr.execute(resolve_rtv_query_line)
            cr.commit()

        self.pool.get('rtv.end.to.end').rtv_resolve_update(cr, uid, rtv_obj.name, rtv_obj.id,str(fields.datetime.now()), 'resolved',context=context)

        self.email_template(cr, uid, 'RTV Ticket', ids[0], context)

        return True

    def confirm_rtv(self, cr, uid, ids, context=None):
        rtv_obj = self.browse(cr, uid, ids[0], context=context)

        for single_id in ids:
            confirm_rtv_query = "UPDATE rtv_process SET state='confirmed', confirmed_by={0},confirmed_time='{1}' WHERE id={2}".format(
                uid,str(fields.datetime.now()), single_id)
            cr.execute(confirm_rtv_query)
            cr.commit()

            confirm_rtv_query_line = "UPDATE rtv_process_line SET state='confirmed', confirmed_time='{0}' WHERE rtv_process_id={1}".format(
                str(fields.datetime.now()), single_id)
            cr.execute(confirm_rtv_query_line)
            cr.commit()

        self.pool.get('rtv.end.to.end').rtv_confirm_update(cr, uid, rtv_obj.name, rtv_obj.id,str(fields.datetime.now()), 'confirmed',context=context)

        self.email_template(cr, uid, 'RTV Ticket', ids[0], context)
        return True

    def srn_rtv(self,cr,uid,ids,context=None):

        if context is None:
            context={}
        data={}
        rtv_obj = self.pool.get('rtv.process').browse(cr, uid, ids, context=context)

        dict_data=[{'warehouse_id':row.warehouse_id,'vendor_id':row.vendor.id,'rtv_id':row.id} for row in rtv_obj]

        groupby_warehouse_vendor =[]
        i=0
        for key, group in itertools.groupby(dict_data, key=lambda x: (x['warehouse_id'],x['vendor_id'])):
            groupby_warehouse_vendor.append({i:list(group)})
            i+=1
        z=0
        for row in groupby_warehouse_vendor:
            srn_line = []
            srn_product_line = []
            for rtv_obj in row[z]:

                rtv_data = self.pool.get('rtv.process').browse(cr, uid, rtv_obj['rtv_id'], context=context)
                srn_number = rtv_data.srn_number if rtv_data.srn_number else 'False'
                if rtv_data.state!='confirmed':
                    raise osv.except_osv(_('Warning!'),
                                         _('Please confirm the RTV'))
                if rtv_data.vendor:
                    data['vendor']= rtv_data.vendor.id
                    data['vendor_address']= rtv_data.vendor_address if rtv_data.vendor_address else ''
                    data['vendor_contact']= rtv_data.vendor_contact if rtv_data.vendor_contact else ''

                data['warehouse_id']= rtv_data.warehouse_id
                if rtv_data.area:
                    data['area']= rtv_data.area
                srn_line.append([0, False, {
                    'rtv_name': str(rtv_data.name),
                    'rtv_id': int(rtv_data.id),
                    'stock_id': int(rtv_data.stock_id.id) if rtv_data.stock_id.id else '',
                    'pqc': int(rtv_data.pqc) if rtv_data.pqc else False,
                    'pqc_id': int(rtv_data.pqc_id.id) if rtv_data.pqc_id.id else '',
                    'pqc_number': str(rtv_data.pqc_number) if rtv_data.pqc_number else '',
                    'state': 'pending'
                }])
                for rtv_product in rtv_data.rtv_process_line:
                    srn_product_line.append([0, False, {
                        'rtv_id':rtv_data.id,
                        'rtv_name':rtv_data.name,
                        'stock_id':int(rtv_data.stock_id.id)if rtv_data.stock_id.id else '',
                        'pqc': int(rtv_data.pqc) if rtv_data.pqc else False,
                        'pqc_id': int(rtv_data.pqc_id.id) if rtv_data.pqc_id.id else '',
                        'pqc_number': str(rtv_data.pqc_number) if rtv_data.pqc_number else '',
                        'product_id':rtv_product.product_id.id,
                        'return_qty':rtv_product.return_qty,
                        'requested_qty':rtv_product.requested_qty,
                        'remaining_qty':rtv_product.remaining_qty,
                        'state':'pending',
                        'amount':rtv_product.amount,
                        'unit_cost':rtv_product.unit_cost,
                    }])

                data['srn_product_line'] = srn_product_line
            data['srn_line'] = srn_line
            save_the_data = self.pool.get('srn.process').create(cr,uid, data, context=context)
            z+=1

            srn_obj = self.pool.get('srn.process').browse(cr, uid, save_the_data, context=context)

            srn = srn_obj.name if srn_number == 'False' else srn_number + "," + srn_obj.name
            for srn_line_rtv in srn_obj.srn_line:
                warehouse_code = {1: 'UttWH',
                                  2: 'NODDA',
                                  3: 'NODRT',
                                  4: 'MTWH',
                                  6: 'UTTRT',
                                  7: 'MOTRT',
                                  8: 'DamWH',
                                  19: 'ComWH',
                                  27: 'BADDA',
                                  21:'SINWH',
                                  37:'SAVWH',
                                  }

                srn_rtv_number = srn_obj.name + '/' + warehouse_code[srn_obj.warehouse_id] + '/' + srn_line_rtv.rtv_id.name
                update_srn_number = "UPDATE srn_rtv_line SET srn_rtv_number='{0}' WHERE id ={1}".format(
                    srn_rtv_number, srn_line_rtv.id)
                cr.execute(update_srn_number)
                cr.commit()

                update_srn_number_product = "UPDATE srn_rtv_product SET srn_rtv_number='{0}' WHERE rtv_id ={1} and srn_rtv_product_id={2}".format(
                    srn_rtv_number, srn_line_rtv.rtv_id.id, srn_obj.id)
                cr.execute(update_srn_number_product)
                cr.commit()

                update_query_srn = "UPDATE rtv_process SET srn_number='{0}',srn_rtv_number='{1}' WHERE id ={2}".format(
                    srn,srn_rtv_number, srn_line_rtv.rtv_id.id)
                cr.execute(update_query_srn)
                cr.commit()


                self.pool.get('rtv.end.to.end').rtv_srn_update(cr, uid, srn_line_rtv.rtv_id.name, srn_line_rtv.rtv_id.id,srn,srn_rtv_number, 'create_srn',str(fields.datetime.now()),context=context)

        for id in ids:
            update_query = "UPDATE rtv_process SET state='create_srn' WHERE id ={0}".format(id)
            cr.execute(update_query)
            cr.commit()

            update_query_pickup_req_line = "UPDATE rtv_process_line SET state='create_srn' WHERE rtv_process_id = {0}".format(id)
            cr.execute(update_query_pickup_req_line)
            cr.commit()

        return True

    def cancel_rtv(self, cr, uid, ids, context=None):

        rtv_obj = self.browse(cr, uid, ids[0], context=context)

        if rtv_obj.pqc == False:
            if rtv_obj.stock_id.id is not None or False:

                # Cancel Action
                self.pool.get('stock.picking').do_unreserve(cr, uid, int(rtv_obj.stock_id.id), context=context)
                self.pool.get('stock.picking').action_cancel(cr, uid, int(rtv_obj.stock_id.id), context)

        for single_id in ids:
            cancel_rtv_query = "UPDATE rtv_process SET state='cancel', cancel_by={0}, cancel_time='{1}' WHERE id={2}".format(uid,
                str(fields.datetime.now()), single_id)
            cr.execute(cancel_rtv_query)
            cr.commit()

            cancel_rtv_query_line = "UPDATE rtv_process_line SET state='cancel', cancel_time='{0}' WHERE rtv_process_id={1}".format(
                str(fields.datetime.now()), single_id)
            cr.execute(cancel_rtv_query_line)
            cr.commit()

        self.pool.get('rtv.end.to.end').rtv_cancel_update(cr, uid, rtv_obj.name, rtv_obj.id,str(fields.datetime.now()), 'cancel',context=context)
        return True

    def cancel_pending_rtv(self, cr, uid, ids, context=None):

        rtv_obj = self.browse(cr, uid, ids[0], context=context)

        for single_id in ids:
            cancel_rtv_query = "UPDATE rtv_process SET state='cancel', cancel_by={0}, cancel_time='{1}' WHERE id={2}".format(uid,
                str(fields.datetime.now()), single_id)
            cr.execute(cancel_rtv_query)
            cr.commit()

            cancel_rtv_query_line = "UPDATE rtv_process_line SET state='cancel', cancel_time='{0}' WHERE rtv_process_id={1}".format(
                str(fields.datetime.now()), single_id)
            cr.execute(cancel_rtv_query_line)
            cr.commit()

        self.pool.get('rtv.end.to.end').rtv_cancel_update(cr, uid, rtv_obj.name, rtv_obj.id,str(fields.datetime.now()), 'cancel',context=context)
        return True

    def _reattempt_count(self,cr, uid, ids, context=None):
        rtv_obj = self.pool.get('rtv.process').browse(cr, uid, ids, context=context)
        reattempt = rtv_obj.reattempt_count if rtv_obj.reattempt_count else 0
        reattempted = reattempt+1

        return reattempted

    def reattempt_rtv(self, cr, uid, ids, context=None):
        single_id = ids[0]
        reattempt = self._reattempt_count(cr, uid, single_id, context=None)
        rtv_obj = self.browse(cr, uid, ids[0], context=context)

        if reattempt <3:
            state = 'pending'
        else:
            state = 'pending_approval'

        reattempt_srn_query = "UPDATE rtv_process SET reattempted=True,state='{0}', reattempt_by={1},reattempt_time='{2}',reattempt_count={3} WHERE id={4}".format(state, uid, str(fields.datetime.now()),reattempt, single_id)
        cr.execute(reattempt_srn_query)
        cr.commit()

        reattempt_srn_rtv_line_query = "UPDATE rtv_process_line SET state='{0}' WHERE rtv_process_id={1}".format(
            state, single_id)
        cr.execute(reattempt_srn_rtv_line_query)
        cr.commit()

        self.pool.get('rtv.end.to.end').rtv_reattempt_update(cr, uid, rtv_obj.name, rtv_obj.id,str(fields.datetime.now()), state, context=context)

        self.email_template(cr, uid,'RTV Reattempted', ids[0], context)

        return True

    def approved_reattempt_rtv(self, cr, uid, ids, context=None):
        rtv_obj = self.browse(cr, uid, ids[0], context=context)

        if rtv_obj.pqc == False:
            if rtv_obj.stock_id.id is not None or False:
                # Cancel Action
                self.pool.get('stock.picking').do_unreserve(cr, uid, int(rtv_obj.stock_id.id), context=context)

                self.pool.get('stock.picking').action_cancel(cr, uid, int(rtv_obj.stock_id.id), context)

        else:
            self.create_aqc(cr, uid, ids, context=context)

        for single_id in ids:
            resolve_rtv_query = "UPDATE rtv_process SET state='approved',approved_by={0},approved_time='{1}' WHERE id={2}".format(
                uid, str(fields.datetime.now()), single_id)
            cr.execute(resolve_rtv_query)
            cr.commit()

            resolve_rtv_query_line = "UPDATE rtv_process_line SET state='approved' WHERE rtv_process_id={0}".format(
                single_id)
            cr.execute(resolve_rtv_query_line)
            cr.commit()

        self.pool.get('rtv.end.to.end').rtv_approval_update(cr, uid, rtv_obj.name, rtv_obj.id, str(fields.datetime.now()), 'approved', context=context)

        self.email_template(cr, uid, 'RTV Ticket', ids[0], context)

        return True

    def send_rtv_to_damage(self, cr, uid, ids, context=None):

        id = ids[0] if len(ids) > 0 else None
        rtv_obj = self.browse(cr, uid, ids[0], context=context)
        uid = 1

        if not rtv_obj.pqc:
            if rtv_obj.stock_id.id is not None or False:
                # Cancel Action
                self.pool.get('stock.picking').do_unreserve(cr, uid, int(rtv_obj.stock_id.id), context=context)

                self.pool.get('stock.picking').action_cancel(cr, uid, int(rtv_obj.stock_id.id), context)

            stock_picking_type_ids = self.pool['stock.picking.type'].search(cr, uid, [('name', 'like', 'Delivery Orders')],
                                                                            context=context)
            stock_picking_type_data = self.pool['stock.picking.type'].browse(cr, uid, stock_picking_type_ids,
                                                                             context=context)
            location_id = None
            location_dest_id = None
            picking_type_id = None

            for items in stock_picking_type_data:

                if items.warehouse_id.id == rtv_obj.warehouse_id:
                    location_id = items.default_location_src_id.id
                    picking_type_id = items.id
                    location_dest_id = 68

            move_line = []

            stock_data = {}

            if location_id is not None and location_dest_id is not None:
                stock_data = {'origin': False, 'message_follower_ids': False, 'carrier_tracking_ref': False,
                              'number_of_packages': 0, 'date_done': False, 'carrier_id': False, 'write_uid': False,
                              'partner_id': False, 'message_ids': False, 'note': False, 'picking_type_id': picking_type_id,
                              'move_type': 'one', 'company_id': 1, 'priority': '1', 'picking_type_code': False,
                              'owner_id': False, 'min_date': False, 'date': strftime("%Y-%m-%d %H:%M:%S", gmtime()),
                              'pack_operation_ids': [],
                              'carrier_code': 'custom', 'invoice_state': 'none'}
                tmp_dict = {}
                for rtv_line in rtv_obj.rtv_process_line:
                    move_line.append([0, False,
                                      {'product_uos_qty': rtv_line.requested_qty,
                                       'date_expected': strftime("%Y-%m-%d %H:%M:%S", gmtime()),
                                       'date': strftime("%Y-%m-%d %H:%M:%S", gmtime()),
                                       'product_id': rtv_line.product_id.id, 'product_uom': 1,
                                       'picking_type_id': picking_type_id, 'product_uom_qty': rtv_line.requested_qty,'price_unit': rtv_line.unit_cost,
                                       'invoice_state': 'none', 'product_tmpl_id': False, 'product_uos': False,
                                       'reserved_quant_ids': [], 'location_dest_id': location_dest_id,
                                       'procure_method': 'make_to_stock',
                                       'product_packaging': False, 'group_id': False, 'location_id': location_id,
                                       'name': str(rtv_line.product_id.name)}])

                stock_data['move_lines'] = move_line

            stock_obj = self.pool.get('stock.picking')
            save_the_data = stock_obj.create(cr, uid, stock_data, context=context)
            if save_the_data:
                stock_obj_confirmation = stock_obj.action_confirm(cr, uid, [save_the_data], context=context)
                stock_obj = stock_obj.action_assign(cr, uid, [save_the_data], context=context)

                # call transfer
                stock_picking = self.pool.get('stock.picking').do_enter_transfer_details(cr, uid, [save_the_data],
                                                                       context=context)

                trans_obj = self.pool.get('stock.transfer_details')
                trans_search = trans_obj.search(cr, uid, [('picking_id', '=',save_the_data)], context=context)

                trans_search = [trans_search[len(trans_search) - 1]] if len(trans_search) > 1 else trans_search

                trans_browse = self.pool.get('stock.transfer_details').browse(cr, uid, trans_search, context=context)

                trans_browse.do_detailed_transfer()

                for single_id in ids:
                    resolve_rtv_query = "UPDATE rtv_process SET state='approved',damage=TRUE ,stock_id='{0}',approved_by={1},approved_time='{2}' WHERE id={3}".format(save_the_data, uid, str(fields.datetime.now()), single_id)
                    cr.execute(resolve_rtv_query)
                    cr.commit()

                    resolve_rtv_query_line = "UPDATE rtv_process_line SET state='approved'"
                    cr.execute(resolve_rtv_query_line)
                    cr.commit()

                self.pool.get('rtv.end.to.end').rtv_approval_update(cr, uid, rtv_obj.name, rtv_obj.id,
                                                                     str(fields.datetime.now()), 'approved',
                                                                     context=context)
                self.email_template(cr, uid, 'RTV Ticket', ids[0], context)
            return True
        else:
            raise osv.except_osv(_('Warning!'),
                                 _('You have to receive products in PO W/H.'
                                   'Please follow rtv process to transfer to Damage W/H'))

    def email_template(self,cr, uid, template_name, id,context=None):
        self.pool.get('rtv.process').send_email(cr, uid, template_name, id,
                                                context=context)
        return True

    def send_email(self, cr, uid,template_name, ids, context=None):
        uid=1
        email_template_obj = self.pool.get('email.template')

        template_ids = email_template_obj.search(cr, uid, [('name', '=',template_name )], context=context)

        if template_ids:
            try:
                email_template_obj.send_mail(cr, uid, template_ids[0], ids, force_send=True,context=context)
            except:
                pass
        return True

    @api.model
    def create(self, vals):
        warehouse_id = vals['warehouse_id']

        for items in vals.get('rtv_process_line'):
            if not vals['pqc']:
                product_id = items[2]['product_id']

                prod_obj = self.pool.get('product.product')
                uid=1
                prod_search = prod_obj.search(self._cr, uid, [('id', '=', product_id)], context=self._context)
                prod_browse = self.pool.get('product.product').browse(self._cr, uid, prod_search, context=self._context)

                if warehouse_id == 1:  # Uttara
                    items[2]['available_qty'] = prod_browse.uttwh_free_quantity
                elif warehouse_id == 2:  # Nodda
                    items[2]['available_qty'] = prod_browse.nodda_free_quantity
                elif warehouse_id == 4:  # Motijeel
                    items[2]['available_qty'] = prod_browse.moti_free_quantity
                elif warehouse_id == 3:  # Nodda Retail
                    items[2]['available_qty'] = prod_browse.nodda_retail_free_quantity
                elif warehouse_id == 6:  # Uttara Retail
                    items[2]['available_qty'] = prod_browse.uttwh_retail_free_quantity
                elif warehouse_id == 7:  # Motijeel Retail
                    items[2]['available_qty'] = prod_browse.moti_retail_quantity
                elif warehouse_id ==8: # Damage
                    items[2]['available_qty'] = prod_browse.damage_quantity
                elif warehouse_id ==19: # Comilla
                    items[2]['available_qty'] = prod_browse.comilla_free_quantity
                elif warehouse_id ==27: # Badda
                    items[2]['available_qty'] = prod_browse.Badda_free_quantity
                elif warehouse_id ==21: # Signboard
                    items[2]['available_qty'] = prod_browse.signboard_free_quantity
                elif warehouse_id ==37: # Savar
                    items[2]['available_qty'] = prod_browse.saver_free_quantity

                items[2]['amount'] = items[2]['cal_amount']
                items[2]['free_qty'] = items[2]['available_qty']

                if items[2]['available_qty'] == float(0):
                    raise except_orm(_('Free Quantity Error!'), _("This Product is not available in Stock"))

                if items[2].get('requested_qty') > items[2]['available_qty']:
                    raise osv.except_osv(_("Warning!!!"), _(
                        "Requested Quantity can not greater than Available Quantity"))
                else:
                    items[2]['return_qty'] = items[2].get('requested_qty')
            else:
                if items[2].get('requested_qty') != items[2]['available_qty']:
                    raise osv.except_osv(_("Warning!!!"), _(
                        "Requested Quantity must be equal to Available Quantity"))
                else:
                    items[2]['return_qty'] = items[2].get('requested_qty')

        if vals['po_number']:
            po_number = vals['po_number']
            po_obj = self.pool.get('purchase.order').browse(self._cr, self._uid, self.pool['purchase.order'].search(self._cr, self._uid, [
                ('name', '=', str(po_number))]))

            vals['po_id']= po_obj.id

        record = super(rtv_process, self).create(vals)

        rtv_number = "RTV0"+ str(record.id)
        record.name = rtv_number

        warehouse_code = {1: 'UttWH',
                          2: 'NODDA',
                          3: 'NODRT',
                          4: 'MTWH',
                          6: 'UTTRT',
                          7: 'MOTRT',
                          8: 'DamWH',
                          19: 'ComWH',
                          27: 'BADDA',
                          21: 'SINWH',
                          37: 'SAVWH',
                          }
        record.warehouse_name = warehouse_code[warehouse_id]

        self.pool.get('rtv.end.to.end').rtv_data_create(self._cr, self._uid, rtv_number, record.id, value={'rtv_date_create':record.create_date,'vendor_name':record.vendor.name,
  'warehouse_name': warehouse_code[warehouse_id],'ref_po_number':record.po_number, 'ref_pqc_number':record.pqc_number, 'state': record.state}, context=self._context)
        return record

    @api.multi
    def write(self, vals):

        update_permission = list()
        if vals.has_key('rtv_process_line'):
            for single_line in vals['rtv_process_line']:
                if not not single_line[2]:
                    for req_l in self.rtv_process_line:

                        if req_l.id == single_line[1]:

                            request_quantity = single_line[2]['requested_qty'] if single_line[2].has_key(
                                'requested_qty') else req_l.requested_qty

                            single_line[2]['amount'] = req_l['cal_amount']

                            if request_quantity > req_l.free_qty:
                                update_permission.append(False)
                            else:
                                update_permission.append(True)
                        else:
                            single_line[2]['free_qty'] = req_l['available_qty']
                            single_line[2]['amount'] = req_l['cal_amount']
                            request_quantity = single_line[2]['requested_qty'] if single_line[2].has_key(
                                'requested_qty') else req_l.requested_qty

                            if request_quantity > req_l.free_qty:
                                update_permission.append(False)
                            else:
                                update_permission.append(True)


        if False in update_permission:

            raise osv.except_osv(_('RTV Process line adjustment ERROR!'),
                                 _('Request quantity should be less then or equal to (available quantity)!!!'))

        else:
            record = super(rtv_process, self).write(vals)

            return record


class rtv_process_line(osv.osv):
    _name = "rtv.process.line"
    _description = "rtv Process Line"

    _columns = {
        'rtv_process_id': fields.many2one('rtv.processs', 'RTV Process ID', required=True,
                                          ondelete='cascade', select=True, readonly=True),

        'product_id': fields.many2one('product.product', 'Product'),
        'po_number': fields.char('Ref PO Number'),
        'pqc': fields.boolean('PQC'),
        'pqc_processed': fields.boolean('PQC Processed'),
        'cancel_time': fields.datetime('Cancel Time'),
        'resolved_time': fields.datetime('Resolved Time'),
        'received_time': fields.datetime('Received Time'),
        'requested_time': fields.datetime('Requested Time'),
        'confirmed_time': fields.datetime('Confirmed Time'),
        'return_qty': fields.float('Return Qty'),
        'remaining_qty': fields.float('Remaining Qty'),
        'available_qty': fields.float(''),
        'free_qty': fields.float('Available Qty'),
        'requested_qty': fields.float('Request Qty'),
        'unit_cost': fields.float('Unit Rate'),
        'amount': fields.float('Amount'),
        'cal_amount': fields.float(''),
        'state': fields.selection([
            ('pending', 'Pending'),
            ('requested', 'Requested'),
            ('resolved', 'Resolved'),
            ('confirmed', 'Ready for Return'),
            ('create_srn', 'SRN'),
            ('received', 'Received'),
            ('not_received', 'Not Received'),
            ('pending_approval', 'pending_approval'),
            ('approved', 'Approved'),
            ('cancel', 'Cancelled'),

        ], 'Status', readonly=True, copy=False, help="Gives the status of the RTV Process", select=True),

        'not_received_by': fields.many2one('res.users', 'Not Received By'),
        'not_received_time': fields.datetime('Not Received Time'),

        # 'not_receive_reason': fields.selection([
        #     ("Vendor was unreachable", "Vendor was unreachable"),
        #     ("Vendor refused to provide", "Vendor refused to provide"),
        #     ("Unable to reach", "Unable to reach"),
        #     ("Vendors place was closed", "Vendors place was closed"),
        #     ("Cash problem", "Cash problem"),
        #     ("Vendor rescheduled", "Vendor rescheduled"),
        #     ("Wrong address given", "Wrong address given"),
        #     ("Unable to reach on Time", "Unable to reach on Time"),
        #     ("Quality issue", "Quality issue"),
        #     ("Due to traffic issue", "Due to traffic issue")
        #
        # ], 'Not Receive Reason', copy=False, help="Reason", select=True),

        'not_receive_reason': fields.selection([
            ("Shop off", "Shop off"),
            ("Vendor unreachable", "Vendor unreachable"),
            ("Vendor Rescheduled", "Vendor Rescheduled"),
            ("Does not agree to take", "Does not agree to take"),
            ("Not received due to quality", "Not received due to quality"),
            ("Vendor is not aware of", "Vendor is not aware of")

        ], 'Not Receive Reason', copy=False, help="Reason", select=True),

        'srn_number': fields.char("SRN Number"),
    }

    _defaults = {
        'state': 'pending'
    }

    def onchange_product_id(self, cr, uid, ids, product_id=False, warehouse_id=False, context=None):

        warehouse_id = context.get('warehouse_id')

        if not warehouse_id:
            raise except_orm(_('No Warehouse Defined!'), _("You must first select a Destination Warehouse"))
        values = {}

        if product_id is not False:
            prod_obj = self.pool.get('product.product')
            uid=1
            prod_search = prod_obj.search(cr, uid, [('id', '=', product_id)], context=context)
            prod_browse = self.pool.get('product.product').browse(cr, uid, prod_search, context=context)

            if warehouse_id == 1:  # Uttara
                values['available_qty'] = prod_browse.uttwh_free_quantity
            elif warehouse_id == 2:  # Nodda
                values['available_qty'] = prod_browse.nodda_free_quantity
            elif warehouse_id == 4:  # Motijeel
                values['available_qty'] = prod_browse.moti_free_quantity
            elif warehouse_id == 3:  # Nodda Retail
                values['available_qty'] = prod_browse.nodda_retail_free_quantity
            elif warehouse_id == 6:  # Uttara Retail
                values['available_qty'] = prod_browse.uttwh_retail_free_quantity
            elif warehouse_id == 7:  # Motijeel Retail
                values['available_qty'] = prod_browse.moti_retail_quantity
            elif warehouse_id ==8: # Damage
                values['available_qty'] = prod_browse.damage_quantity
            elif warehouse_id == 19:  # Comilla
                values['available_qty'] = prod_browse.comilla_free_quantity
            elif warehouse_id == 27:  # Badda
                values['available_qty'] = prod_browse.Badda_free_quantity
            elif warehouse_id == 21:  # SINWH
                values['available_qty'] = prod_browse.signboard_free_quantity
            elif warehouse_id == 37:  # SAVWH
                values['available_qty'] = prod_browse.saver_free_quantity

            values['free_qty'] = values['available_qty']


            if values['available_qty'] ==float(0):
                raise except_orm(_('Free Quantity Error!'), _("This Product is not available in Stock"))

        return {'value': values}

    def onchange_product_amount_cal(self, cr, uid, ids, requested_qty=False,unit_cost=False, context=None):
        values = {}
        if requested_qty and unit_cost is not False :
            values['amount'] = float(requested_qty)*float(unit_cost)
            values['cal_amount'] = float(requested_qty)*float(unit_cost)
        else:
            values['amount'] = 0.00
            values['cal_amount'] = 0.00
        return {'value':values}


class ResolveRtvProcessReason(osv.osv):
    _name = "resolve.rtv.process.reason"
    _description = "Resolve rtv Process Reason"

    _columns = {
        'resolve_rtv_process_date': fields.datetime('Resolve rtv Process Date'),
        'resolve_rtv_process_by': fields.many2one('res.users', 'Resolve rtv Process By'),
        'resolve_rtv_process_reason': fields.text('Resolve rtv Process Reason', required=True),

    }

    def resolve_rtv_process_reason_def(self, cr, uid, ids, context=None):
        ids = context['active_ids']
        resolve_rtv_process_reason = str(context['resolve_rtv_process_reason'])
        resolve_rtv_process_by = uid
        resolve_rtv_process_date = str(fields.datetime.now())

        rtv_obj = self.pool.get('rtv.process')

        for s_id in ids:
            resolve_rtv_proces_query = "UPDATE rtv_process SET resolve_rtv_process_reason='{0}', resolve_rtv_process_by={1}, resolve_rtv_process_date='{2}' WHERE id={3}".format(
                resolve_rtv_process_reason, resolve_rtv_process_by, resolve_rtv_process_date, s_id)
            cr.execute(resolve_rtv_proces_query)
            cr.commit()

            rtv_obj.resolve_rtv(cr, uid, [s_id], context)

        return True


class CancelRtvProcessReason(osv.osv):
    _name = "cancel.rtv.process.reason"
    _description = "Cancel rtv Process Reason"

    _columns = {
        'cancel_rtv_process_date': fields.datetime('Cancel rtv Process Date'),
        'cancel_rtv_process_by': fields.many2one('res.users', 'Cancel rtv Process By'),
        'cancel_rtv_process_reason': fields.text('Cancel RTV Process Reason', required=True),

    }

    def cancel_rtv_process_reason_def(self, cr, uid, ids, context=None):
        ids = context['active_ids']
        cancel_rtv_process_reason = str(context['cancel_rtv_process_reason'])
        cancel_rtv_process_by = uid
        cancel_rtv_process_date = str(fields.datetime.now())

        rtv_obj = self.pool.get('rtv.process')

        for s_id in ids:
            cancel_rtv_proces_query = "UPDATE rtv_process SET cancel_rtv_process_reason='{0}', cancel_rtv_process_by={1}, cancel_rtv_process_date='{2}' WHERE id={3}".format(
                cancel_rtv_process_reason, cancel_rtv_process_by, cancel_rtv_process_date, s_id)
            cr.execute(cancel_rtv_proces_query)
            cr.commit()

            rtv_obj.cancel_rtv(cr, uid, [s_id], context)

        return True


class CancelPendingRtvProcessReason(osv.osv):
    _name = "cancel.pending.rtv.process.reason"
    _description = "Cancel rtv Process Reason"

    _columns = {
        'cancel_rtv_process_date': fields.datetime('Cancel rtv Process Date'),
        'cancel_rtv_process_by': fields.many2one('res.users', 'Cancel rtv Process By'),
        'cancel_rtv_process_reason': fields.text('Cancel RTV Process Reason', required=True),

    }

    def cancel_pending_rtv_process_reason_def(self, cr, uid, ids, context=None):
        ids = context['active_ids']
        cancel_rtv_process_reason = str(context['cancel_rtv_process_reason'])
        cancel_rtv_process_by = uid
        cancel_rtv_process_date = str(fields.datetime.now())

        rtv_obj = self.pool.get('rtv.process')

        for s_id in ids:
            cancel_rtv_proces_query = "UPDATE rtv_process SET cancel_rtv_process_reason='{0}', cancel_rtv_process_by={1}, cancel_rtv_process_date='{2}' WHERE id={3}".format(
                cancel_rtv_process_reason, cancel_rtv_process_by, cancel_rtv_process_date, s_id)
            cr.execute(cancel_rtv_proces_query)
            cr.commit()

            rtv_obj.cancel_pending_rtv(cr, uid, [s_id], context)

        return True


class pending_quality_control(osv.osv):
    _inherit = "pending.quality.control"

    _columns = {

        'stock_id': fields.many2one('stock.picking', 'Stock Number'),
        'rtv_name': fields.char('RTV Number'),
        'rtv_id': fields.many2one('rtv.processs', 'RTV Process ID'),

        }



    def wms_inbound_pqc_rtv(self,cr,uid,ids,context=None):

        if context is None:
            context={}

        data={}
        rtv_process_line=[]

        for req_id in ids:
            rtv_obj = self.pool.get('pending.quality.control').browse(cr, uid, req_id, context=context)

            if str(rtv_obj.po_number):
                po_obj = self.pool.get('purchase.order').browse(cr, uid, self.pool['purchase.order'].search(cr, uid, [('name', '=', str(rtv_obj.po_number))]))


                data['po_id']= po_obj.id
                data['vendor']= po_obj.partner_id.id

                vendor_data = self.pool.get('rtv.process').onchange_vendor_info(cr, uid, ids,po_obj.partner_id.id, context=None)

                data['vendor_address']=vendor_data['value']['vendor_address'] if vendor_data['value'].has_key('vendor_address') else ''
                data['vendor_contact']=vendor_data['value']['vendor_contact'] if vendor_data['value'].has_key('vendor_contact') else ''

                data['area'] = po_obj.partner_id.state_id.id

            data['warehouse_id'] = rtv_obj.warehouse_id.id
            data['rtv_type'] = 'pqc'
            data['pqc'] = True
            data['pqc_id'] = rtv_obj.id
            data['pqc_number'] = str(rtv_obj.pqc_number)

            data['po_number'] = str(rtv_obj.po_number)

            data['pqc_processed'] = rtv_obj.pqc_processed

            data['remark'] = ''
            data['state'] = 'pending'


            for rtv_product in rtv_obj.pqc_line:
                if rtv_product.pqc_reject_quantity > float(0):
                    pqc_reject_quantity=rtv_product.pqc_reject_quantity
                else:
                    pqc_reject_quantity = rtv_product.pending_quantity

                if rtv_product.pqc_processed == False:

                    rtv_process_line.append([0, False, {
                        'rtv_id':rtv_obj.rtv_id,
                        'rtv_name':rtv_obj.rtv_name,
                        'return_qty': 0,
                        'product_id': rtv_product.product_id.id,
                        'available_qty': pqc_reject_quantity,
                        'rtv_process_id': rtv_obj.id,
                        'amount': 0,
                        'unit_cost': 0,
                        'requested_qty': pqc_reject_quantity,
                        'cal_amount': 0,
                        'state':'pending',
                        'pqc_id': rtv_obj.id,
                    }])

            data['rtv_process_line'] = rtv_process_line

        save_the_data = self.pool.get('rtv.process').create(cr,uid, data, context=context)

        self.pool.get('rtv.process').email_template(cr, uid, 'RTV Ticket', save_the_data, context)


        for pqc_id in ids:
            # update qc pqc_processed and status to true
            pqc_state_update_query = "UPDATE pending_quality_control SET pqc_processed= TRUE, state='return_to_vendor' WHERE id={0}".format(pqc_id)
            cr.execute(pqc_state_update_query)
            cr.commit()

            pqc_processed_pqc_line_update_query = "UPDATE pending_quality_control_line SET pqc_processed=TRUE, state='return_to_vendor' WHERE pqc_id={0}".format(pqc_id)
            cr.execute(pqc_processed_pqc_line_update_query)
            cr.commit()

        return True


class pending_quality_control_line(osv.osv):
    _inherit = "pending.quality.control.line"

    _columns = {

        'stock_id': fields.many2one('stock.picking', 'Stock Number'),
        'rtv_name': fields.char('RTV Number'),
        'rtv_id': fields.many2one('rtv.processs', 'RTV Process ID'),
    }


    def wms_inbound_pqc_line_rtv(self,cr,uid,ids,context=None):

        pqc_line_obj = self.browse(cr, uid, ids, context=context)
        pqc_obj = self.pool.get('pending.quality.control')
        pqc_data = pqc_obj.browse(cr, uid, [pqc_line_obj.pqc_id.id], context=context)

        data = {}
        rtv_process_line = []

        for pqc in pqc_data:

            if str(pqc.po_number):
                po_obj = self.pool.get('purchase.order').browse(cr, uid, self.pool['purchase.order'].search(cr, uid, [('name', '=', str(pqc.po_number))]))

                data['po_id'] = po_obj.id
                data['vendor'] = po_obj.partner_id.id

                vendor_data = self.pool.get('rtv.process').onchange_vendor_info(cr, uid, ids,po_obj.partner_id.id, context=None)
                data['vendor_address']=vendor_data['value']['vendor_address'] if vendor_data['value'].has_key('vendor_address') else ''
                data['vendor_contact']=vendor_data['value']['vendor_contact'] if vendor_data['value'].has_key('vendor_contact') else ''

                data['area'] = po_obj.partner_id.state_id.id

            data['warehouse_id'] = pqc.warehouse_id.id
            data['rtv_type'] = 'pqc'
            data['pqc'] = True
            data['pqc_id'] = pqc.id
            data['pqc_number'] = str(pqc.pqc_number)


            data['po_number'] = str(pqc.po_number)

            data['pqc_processed'] = pqc.pqc_processed

            data['remark'] = ''
            data['state'] = 'pending'

            if pqc_line_obj.pqc_reject_quantity > float(0):
                pqc_reject_quantity = pqc_line_obj.pqc_reject_quantity
            else:
                pqc_reject_quantity = pqc_line_obj.pending_quantity

            rtv_process_line.append([0, False,{
                'rtv_id': pqc_data.rtv_id,
                'rtv_name': pqc_data.rtv_name,
                'stock_id': int(pqc_data.stock_id.id),

                'product': pqc_line_obj.product,
                'product_id': pqc_line_obj.product_id.id,

                'return_qty': pqc_reject_quantity,
                'available_qty': pqc_reject_quantity,
                'rtv_process_id': pqc_data.id,
                'amount': 0,
                'unit_cost': 0,
                'requested_qty': pqc_reject_quantity,
                'cal_amount': 0,
                'state': 'pending',
            }])

            data['rtv_process_line'] = rtv_process_line


        save_the_data = self.pool.get('rtv.process').create(cr, uid, data, context=context)

        self.pool.get('rtv.process').email_template(cr, uid, 'RTV Ticket', save_the_data, context)

        pqc_processed_pqc_line_update_query = "UPDATE pending_quality_control_line SET pqc_processed=TRUE, state='return_to_vendor' WHERE id={0}".format(pqc_line_obj.id)
        cr.execute(pqc_processed_pqc_line_update_query)
        cr.commit()

        # pqc_processed_pqc_update_query = "UPDATE pending_quality_control SET pqc_processed=TRUE, state='return_to_vendor' WHERE id={0}".format(
        #     pqc.id)
        # cr.execute(pqc_processed_pqc_update_query)
        # cr.commit()

        return True



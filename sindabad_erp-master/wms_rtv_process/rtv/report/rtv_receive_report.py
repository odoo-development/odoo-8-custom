import time
from openerp.report import report_sxw
from openerp.osv import osv

class rtvreportdata(report_sxw.rml_parse):

    def __init__(self, cr, uid, name, context):
        super(rtvreportdata, self).__init__(cr, uid, name, context)
        self.localcontext.update({
            'get_warehouse': self.get_warehouse,

        })

    def get_warehouse(self, obj):

        origin = [(obj)]
        warehouse = self.pool.get('stock.warehouse').browse(self.cr, self.uid, origin).partner_id
        warehouse_address=''

        if warehouse.street:
            warehouse_address = warehouse_address + warehouse.street + ", "
        if warehouse.street2:
            warehouse_address = warehouse_address + warehouse.street2 + ", "

        if warehouse.city:
            warehouse_address = warehouse_address + warehouse.city + ", "
        if warehouse.state_id:
            warehouse_address = warehouse_address + warehouse.state_id.name + ", "
        if warehouse.zip:
            warehouse_address = warehouse_address + warehouse.zip + ", "

        if warehouse.country_id:
            warehouse_address = warehouse_address + warehouse.country_id.name + ", "

        if warehouse.phone:
            warehouse_address = warehouse_address + warehouse.phone + ", "
        if warehouse.mobile:
            warehouse_address = warehouse_address + warehouse.mobile


        return warehouse_address




class report_rtv_layout(osv.AbstractModel):
    _name = 'report.wms_rtv_process.report_rtv_layout'
    _inherit = 'report.abstract_report'
    _template = 'wms_rtv_process.report_rtv_layout'
    _wrapped_report_class = rtvreportdata
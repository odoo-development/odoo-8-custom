from openerp.osv import fields, osv
from openerp.tools.translate import _
from openerp import api
from time import gmtime, strftime
import datetime


class stock_picking(osv.osv):
    _inherit = 'stock.picking'

    _columns = {
        'rtv': fields.boolean('RTV')

    }


class stock_move(osv.osv):
    _inherit = "stock.move"

    def action_done(self, cr, uid, ids, context=None):

        if context.get('rtv',False):

            res = super(stock_move, self).action_done(cr, uid, ids, context=context)
            for move in self.browse(cr, uid, ids, context=context):

                for rtv_data in self.pool.get('rtv.process').browse(cr, uid, context.get('rtv_id'),
                                                                    context=context).rtv_process_line:
                    if rtv_data.unit_cost > float(0) and move.product_id == rtv_data.product_id:
                        self.write(cr, uid, [move.id], {'price_unit': rtv_data.unit_cost}, context=context)

        else:
            res = super(stock_move, self).action_done(cr, uid, ids, context=context)

        return res

class srn_process(osv.osv):
    _name = "srn.process"
    _description = "SRN Process"

    _columns = {
        'name': fields.char('SRN Number'),
        'warehouse_id': fields.selection([
            (1, 'Central Warehouse Uttara'),
            (2, 'Nodda Warehouse'),
            (3, 'Nodda Retail Warehouse'),
            (4, 'Motijheel Warehouse'),
            (6, 'Uttara Retail Warehouse'),
            (7, 'Motijheel Retail Warehouse'),
            (8, 'Damage Warehouse'),
            (27, 'Badda Warehouse'),
            (21, 'Signboard Warehouse'),
            (37, 'Savar Warehouse'),

        ], 'Warehouse', readonly=True, copy=False, help="Gives the List of the Warehouses", select=True),

        'vendor': fields.many2one('res.partner', 'Supplier', domain="[('supplier','=',True)]"),
        'vendor_address': fields.char('Address'),
        'vendor_contact': fields.char('Contact Number'),
        'remark': fields.text('Remark'),
        'delivered': fields.boolean('Delivered'),

        'received': fields.boolean('Received'),
        'received_by': fields.many2one('res.users', 'Received By'),
        'received_time': fields.datetime('Received Time'),

        'not_received': fields.boolean('Not Received'),
        'not_received_by': fields.many2one('res.users', 'Not Received By'),
        'not_received_time': fields.datetime('Not Received Time'),

        'confirmed_by': fields.many2one('res.users', 'Confirmed By'),
        'confirmed_time': fields.datetime('Confirmed Time'),

        'requested_by': fields.many2one('res.users', 'Requested By'),
        'requested_time': fields.datetime('Requested Time'),

        'resolved': fields.boolean('Resolved'),
        'resolved_by': fields.many2one('res.users', 'Resolved By'),
        'resolved_time': fields.datetime('Resolved Time'),

        'cancel': fields.boolean('Cancel'),
        'cancel_by': fields.many2one('res.users', 'Cancel By'),
        'cancel_time': fields.datetime('Cancel Time'),

        'state': fields.selection([
            ('pending', 'Pending'),
            ('confirmed', 'Confirmed'),
            ('received', 'Received'),
            ('not_received', 'Not Received'),
            ('partial_received', 'Partial Received'),
            ('cancel', 'Cancelled'),

        ], 'Status', readonly=True, copy=False, help="Gives the status of the SRN Process", select=True),

        'srn_line': fields.one2many('srn.rtv.line', 'srn_rtv_line_id', 'SRN Line ID'),
        'srn_product_line': fields.one2many('srn.rtv.product', 'srn_rtv_product_id', 'SRN RTV Product', required=True),

        'stock_id': fields.many2one('stock.picking', 'Stock Number'),

        'pqc': fields.boolean('PQC'),
        'pqc_id': fields.many2one("pending.quality.control", 'PQC ID'),
        'pqc_number': fields.char('PQC Number'),

        'cancel_reason': fields.text('Cancel Reason'),

        'not_receive_reason_date': fields.datetime('Not Receive Reason Date'),
        'not_receive_reason_by': fields.many2one('res.users', 'Not Receive Reason By'),
        # 'not_receive_reason': fields.text('Not Receive SRN RTV Reason'),

        # 'not_receive_reason': fields.selection([
        #     ("Vendor was unreachable", "Vendor was unreachable"),
        #     ("Vendor refused to provide", "Vendor refused to provide"),
        #     ("Unable to reach", "Unable to reach"),
        #     ("Vendors place was closed", "Vendors place was closed"),
        #     ("Cash problem", "Cash problem"),
        #     ("Vendor rescheduled", "Vendor rescheduled"),
        #     ("Wrong address given", "Wrong address given"),
        #     ("Unable to reach on Time", "Unable to reach on Time"),
        #     ("Quality issue", "Quality issue"),
        #     ("Due to traffic issue", "Due to traffic issue")
        #
        # ], 'Not Receive Reason', copy=False, help="Reason", select=True),

        'not_receive_reason': fields.selection([
            ("Shop off", "Shop off"),
            ("Vendor unreachable", "Vendor unreachable"),
            ("Vendor Rescheduled", "Vendor Rescheduled"),
            ("Does not agree to take", "Does not agree to take"),
            ("Not received due to quality", "Not received due to quality"),
            ("Vendor is not aware of", "Vendor is not aware of")

        ], 'Not Receive Reason', copy=False, help="Reason", select=True),

        'cancel_srn_rtv_date': fields.datetime('Cancel SRN RTV Date'),
        'cancel_srn_rtv_by': fields.many2one('res.users', 'Cancel SRN RTV By'),
        'cancel_srn_rtv_reason': fields.text('Cancel SRN RTV Reason'),

    }

    _defaults = {
        'user_id': lambda obj, cr, uid, context: uid,
        'state': 'pending',
    }

    @api.model
    def create(self, vals):
        record = super(srn_process, self).create(vals)

        srn_number = "SRN0" + str(record.id)
        record.name = srn_number

        return record

    def confirm_srn(self, cr, uid, ids, context=None):

        for single_id in ids:
            confirmed_srn_query = "UPDATE srn_process SET state='confirmed', confirmed_by={0},confirmed_time='{1}' WHERE id={2}".format(
                uid, str(fields.datetime.now()), single_id)
            cr.execute(confirmed_srn_query)
            cr.commit()

            confirmed_srn_rtv_line_query = "UPDATE srn_rtv_line SET state='confirmed', confirmed_by={0}, confirmed_time='{1}' WHERE srn_rtv_line_id={2}".format(
                uid, str(fields.datetime.now()), single_id)
            cr.execute(confirmed_srn_rtv_line_query)
            cr.commit()

            confirmed_srn_rtv_product_query = "UPDATE srn_rtv_product SET state='confirmed', confirmed_time='{0}' WHERE srn_rtv_product_id={1}".format(
                str(fields.datetime.now()), single_id)
            cr.execute(confirmed_srn_rtv_product_query)
            cr.commit()

        return True

    def transfer_stock(self, cr, uid, ids,rtv_id, reverse=False, context=None):
        receiver = uid
        uid = 1
        # call transfer
        rtv_data = self.pool.get('rtv.process').browse(cr, uid, rtv_id, context=context)

        for rtv_obj in rtv_data:

            if reverse is True:
                stock_picking_id = rtv_obj.reverse_stock_id.id
            else:
                stock_picking_id = rtv_obj.stock_id.id

            stock_picking = self.pool.get('stock.picking').do_enter_transfer_details(cr, uid, [stock_picking_id],
                                                                                     context=context)

            if str(rtv_obj.stock_id.state) == 'draft' or str(rtv_obj.stock_id.state) == 'confirmed':
                #     # unreserve
                self.pool.get('stock.picking').do_unreserve(cr, uid, int(stock_picking_id), context=context)

                # action cancel
                self.pool.get('stock.picking').action_assign(cr, uid, int(stock_picking_id), context=context)

            if str(rtv_obj.stock_id.state) == 'waiting':
                # Rereservation
                self.pool.get('stock.picking').rereserve_pick(cr, uid, [stock_picking_id], context=context)

            trans_obj = self.pool.get('stock.transfer_details')
            trans_search = trans_obj.search(cr, uid, [('picking_id', '=', stock_picking_id)], context=context)

            trans_search = [trans_search[len(trans_search) - 1]] if len(trans_search) > 1 else trans_search

            trans_browse = self.pool.get('stock.transfer_details').browse(cr, uid, trans_search, context=context)

            trans_browse.do_detailed_transfer()

            # for single_id in ids:
            if reverse is False:
                received_rtv_query = "UPDATE rtv_process SET state='received',delivered = TRUE ,received_time='{0}', received_by='{1}' WHERE id={2}".format(
                    str(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')), receiver, rtv_obj.id)
                cr.execute(received_rtv_query)
                cr.commit()

                received_rtv_query_line = "UPDATE rtv_process_line SET state='received', received_time='{0}' WHERE rtv_process_id={1}".format(
                    str(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')), rtv_obj.id)
                cr.execute(received_rtv_query_line)
                cr.commit()

                self.pool.get('rtv.end.to.end').rtv_received_update(cr, uid, rtv_obj.name, rtv_obj.id,str(datetime.datetime.now()), 'received',context=context)

                self.pool.get('rtv.process').send_email(cr, uid, 'RTV Received Challan', rtv_obj.id, context=context)

        return True

    def rtv_to_aqc(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        aqc_line = []

        rtv_obj = self.pool.get('rtv.process').browse(cr, uid, ids, context=context)

        for rtv_data in rtv_obj:

            pqc_obj = self.pool.get('pending.quality.control').browse(cr, uid,
                                                                      self.pool['pending.quality.control'].search(cr,
                                                                                                                  uid, [
                                                                                                                      (
                                                                                                                      'pqc_number',
                                                                                                                      '=',
                                                                                                                      str(
                                                                                                                          rtv_data.pqc_number))]))

            put_data = data = {'po_id': pqc_obj.po_id,
                               'po_number': str(pqc_obj.po_number),
                               'irn_number': rtv_data.pqc_id.irn_number,
                               'irn_id': rtv_data.pqc_id.irn_id,
                               'qc_id': rtv_data.pqc_id.qc_id,
                               'warehouse_id': rtv_data.warehouse_id,
                               'vendor': rtv_data.vendor.id,
                               'area': rtv_data.area.id,
                               'state': 'confirm',
                               'qc_number': pqc_obj.qc_number,
                               'received_by': uid,
                               'pqc_number': str(rtv_data.pqc_number),
                               'date_confirm': strftime("%Y-%m-%d %H:%M:%S", gmtime()),
                               'pqc_id': rtv_data.pqc_id
                               }

            for rtv_product in rtv_data.rtv_process_line:
                for pqc_data in pqc_obj.pqc_line:
                    if rtv_product.product_id == pqc_data.product_id:
                        aqc_line.append([0, False, {
                            'rtv_id': rtv_data.id,
                            'rtv_name': rtv_data.name,
                            'qc_id': pqc_obj.qc_id,
                            'product': rtv_product.product_id.name,
                            'product_id': rtv_product.product_id.id,
                            'purchase_quantity': pqc_data.purchase_quantity,
                            'received_quantity': rtv_product.return_qty,
                            'accepted_quantity': rtv_product.return_qty,
                            'pending_quantity': rtv_product.requested_qty,
                            'state': 'confirm'
                        }])

            data['aqc_line'] = aqc_line

            if len(aqc_line) > 0:
                # Create aqc
                save_the_data = self.pool.get('accepted.quality.control').create(cr, uid, data, context=context)

                # Generate Put away Process
                put_line_list = []

                for rtv_product in rtv_data.rtv_process_line:
                    if rtv_product.return_qty > 0:
                        put_line_list.append([0, False,
                                              {
                                                  'product_id': rtv_product.product_id.id,
                                                  'qc_id': save_the_data,
                                                  'location': '',
                                                  'product': rtv_product.product_id.name,
                                                  'remarks': '',
                                                  'quantity': rtv_product.return_qty,
                                                  'po_number': str(pqc_obj.po_number)

                                              }])

                put_data['aqc_id'] = save_the_data
                put_data['state'] = 'pending'
                put_data['putaway_line_view'] = put_line_list
                put_env = self.pool.get('putaway.process')
                saved_put_id = put_env.create(cr, uid, put_data, context=context)

                received_rtv_query = "UPDATE rtv_process SET state='received',delivered = TRUE ,received_time='{0}', received_by='{1}' WHERE id={2}".format(
                    str(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')), uid, rtv_obj.id)
                cr.execute(received_rtv_query)
                cr.commit()

                received_rtv_query_line = "UPDATE rtv_process_line SET state='received', received_time='{0}' WHERE rtv_process_id={1}".format(
                    str(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')), rtv_obj.id)
                cr.execute(received_rtv_query_line)
                cr.commit()

        return True

    def rtv_pqc_status_change(self, cr, uid, ids, rtv_id, context=None):
        if context is None:
            context = {}

        rtv_obj = self.pool.get('rtv.process').browse(cr, uid, rtv_id, context=context)
        for singel_id in rtv_obj:
            received_rtv_query = "UPDATE rtv_process SET state='received',delivered = TRUE ,received_time='{0}', received_by='{1}' WHERE id={2}".format(
                str(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')), uid, singel_id.id)
            cr.execute(received_rtv_query)
            cr.commit()

            received_rtv_query_line = "UPDATE rtv_process_line SET state='received', received_time='{0}' WHERE rtv_process_id={1}".format(
                str(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')), singel_id.id)
            cr.execute(received_rtv_query_line)
            cr.commit()

            self.pool.get('rtv.end.to.end').rtv_received_update(cr, uid, singel_id.name, singel_id.id,str(datetime.datetime.now()), 'received',context=context)

            self.pool.get('rtv.process').send_email(cr, uid, 'RTV Received Challan', singel_id.id, context=context)

        return True

    def receive_srn(self, cr, uid, ids, context=None):
        if self.browse(cr, uid, ids, context=context)[0].state=='confirmed':
            for single_id in ids:
                srn_rtv_ids = self.browse(cr, uid, single_id, context=context).srn_line
                stock_rtv = [i.rtv_id.id for i in srn_rtv_ids if not i.pqc and i.delivered == False]
                pqc_rtv = [i.rtv_id.id for i in srn_rtv_ids if i.pqc and i.delivered == False]

                if len(stock_rtv) > 0:
                    # self.transfer_stock(cr, uid,ids, stock_rtv, reverse=False, context=None)
                    self.create_invoice(cr, uid, ids, stock_rtv,nrr='', context=context)
                if len(pqc_rtv) > 0:
                    self.rtv_pqc_status_change(cr, uid,ids, pqc_rtv, context=None)

                rtv_ids = stock_rtv + pqc_rtv
                if len(rtv_ids) > 1:

                    received_srn_rtv_line_query = "UPDATE srn_rtv_line SET state='received', delivered=TRUE ,received_by={0}, received_time='{1}' WHERE srn_rtv_line_id={2} and rtv_id IN {3}".format(
                        uid, str(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')), single_id, tuple(rtv_ids))
                    cr.execute(received_srn_rtv_line_query)
                    cr.commit()

                    received_srn_rtv_product_query = "UPDATE srn_rtv_product SET state='received', received_time='{0}' WHERE srn_rtv_product_id={1} and rtv_id IN {2}".format(
                        str(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')), single_id, tuple(rtv_ids))
                    cr.execute(received_srn_rtv_product_query)
                    cr.commit()

                else:
                    received_srn_rtv_line_query = "UPDATE srn_rtv_line SET state='received',delivered=TRUE , received_by={0}, received_time='{1}' WHERE srn_rtv_line_id={2} and rtv_id = {3}".format(
                        uid, str(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')), single_id, rtv_ids[0])
                    cr.execute(received_srn_rtv_line_query)
                    cr.commit()

                    received_srn_rtv_product_query = "UPDATE srn_rtv_product SET state='received', received_time='{0}' WHERE srn_rtv_product_id={1} and rtv_id = {2}".format(
                        str(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')), single_id, rtv_ids[0])
                    cr.execute(received_srn_rtv_product_query)
                    cr.commit()

                received_srn_query = "UPDATE srn_process SET state='received',delivered = TRUE ,received_by={0},received_time='{1}' " \
                                     "WHERE id={2}".format(uid, str(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')), single_id)
                cr.execute(received_srn_query)
                cr.commit()
        else:
            raise osv.except_osv(_('Warning!'),
                                 _('This SRN already processed.'))
        return True

    def not_receive_srn(self, cr, uid, ids,rtv_id, not_receive_reason, context=None):
        rtv_data = self.pool.get('rtv.process').browse(cr, uid, rtv_id, context=context)

        for rtv_obj in rtv_data:
            received_rtv_query = "UPDATE rtv_process SET state='not_received', not_receive_reason='{0}', not_received_time='{1}', not_received_by='{2}',srn_rtv_number= NULL WHERE id={3}".format(
                not_receive_reason,
                str(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')), uid, rtv_obj.id)
            cr.execute(received_rtv_query)
            cr.commit()

            received_rtv_query_line = "UPDATE rtv_process_line SET state='not_received', not_received_time='{0}' WHERE rtv_process_id={1}".format(
                str(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')), rtv_obj.id)
            cr.execute(received_rtv_query_line)
            cr.commit()

            self.pool.get('rtv.end.to.end').rtv_not_received_update(cr, uid,  rtv_obj.name,  rtv_obj.id,str(datetime.datetime.now()), 'not_received',context=context)

            self.email_template(cr, uid, rtv_obj.id, context)

        return True

    def cancel_srn(self, cr, uid, ids, context=None):

        for single_id in ids:
            cancel_srn_query = "UPDATE srn_process SET state='cancel', cancel_by={0},cancel_time='{1}' WHERE id={2}".format(
                uid, str(fields.datetime.now()), single_id)
            cr.execute(cancel_srn_query)
            cr.commit()

            cancel_srn_rtv_line_query = "UPDATE srn_rtv_line SET state='cancel', cancel_by={0}, cancel_time='{1}' WHERE srn_rtv_line_id={2}".format(
                uid, str(fields.datetime.now()), single_id)
            cr.execute(cancel_srn_rtv_line_query)
            cr.commit()

            cancel_srn_rtv_product_query = "UPDATE srn_rtv_product SET state='cancel', cancel_time='{0}' WHERE srn_rtv_product_id={1}".format(
                str(fields.datetime.now()), single_id)
            cr.execute(cancel_srn_rtv_product_query)
            cr.commit()

        return True


    def create_invoice(self, cr, uid, ids,rtv_id, nrr=None, context=None):

        receiver = uid
        uid = 1
        rtv_info = self.pool.get('rtv.process').browse(cr, uid, rtv_id, context=context)
        for rtv_odj in rtv_info:

            if 'cancel' not in [rtv_odj.state]:
                picking = [rtv_odj.stock_id.id]

                context.update({

                    'active_model': 'stock.picking',
                    'active_ids': picking,
                    'active_id': len(picking) and picking[0] or False,
                    'rtv':True,
                    'rtv_id':rtv_odj.id

                })

                created_id = self.pool['stock.transfer_details'].create(cr, uid, {'picking_id': len(picking) and picking[0] or False}, context)

                if created_id:
                    created_id = self.pool.get('stock.transfer_details').browse(cr, uid, created_id, context=context)
                    if created_id.picking_id.state in ['assigned', 'partially_available']:
                        # raise Warning(_('You cannot transfer a picking in state \'%s\'.') % created_id.picking_id.state)

                        processed_ids = []
                        # Create new and update existing rtv operations
                        for lstits in [created_id.item_ids, created_id.packop_ids]:
                            for prod in lstits:
                                for rtv_line in rtv_odj.rtv_process_line:
                                    if prod.product_id.id== rtv_line.product_id.id:
                                        pack_datas = {
                                            'product_id': prod.product_id.id,
                                            'product_uom_id': prod.product_uom_id.id,
                                            'product_qty': rtv_line.return_qty,
                                            'package_id': prod.package_id.id,
                                            'lot_id': prod.lot_id.id,
                                            'location_id': prod.sourceloc_id.id,
                                            'location_dest_id': prod.destinationloc_id.id,
                                            'result_package_id': prod.result_package_id.id,
                                            'date': prod.date if prod.date else datetime.datetime.now(),
                                            'owner_id': prod.owner_id.id,
                                        }
                                        if prod.packop_id:
                                            prod.packop_id.with_context(no_recompute=True).write(pack_datas)
                                            processed_ids.append(prod.packop_id.id)
                                        else:
                                            pack_datas['picking_id'] = created_id.picking_id.id
                                            packop_id = created_id.env['stock.pack.operation'].create(pack_datas)
                                            processed_ids.append(packop_id.id)
                        # Delete the others
                        packops = created_id.env['stock.pack.operation'].search(
                            ['&', ('picking_id', '=', created_id.picking_id.id), '!', ('id', 'in', processed_ids)])
                        packops.unlink()
                        # Execute the transfer of the picking
                        created_id.picking_id.do_transfer()

                    if created_id.picking_id.invoice_state != 'invoiced':
                        # Now Create the Invoice for this picking
                        picking_pool = self.pool.get('stock.picking')
                        from datetime import date
                        context['date_inv'] = date.today()
                        context['inv_type'] = 'in_refund'
                        active_ids = [rtv_odj.stock_id.id]
                        res = picking_pool.action_invoice_create(cr, uid, active_ids,
                                                                 journal_id=1136,
                                                                 group=True,
                                                                 type='in_refund',
                                                               context=context)
                        invoice_line=[]
                        total = 0
                        if res:
                        #     # invoice amount from rtv amount
                            invoice_data = self.pool.get('account.invoice').browse(cr, uid, res[0], context=context)[0]
                        #     for rtv_line_data in rtv_odj.rtv_process_line:
                        #         if rtv_line_data.unit_cost > float(0):
                        #             for invoice in invoice_data.invoice_line:
                        #                 if invoice.product_id ==rtv_line_data.product_id:
                        #                     invoice['price_unit'] = rtv_line_data.unit_cost
                        #                     invoice['price_subtotal'] = rtv_line_data.amount
                        #                     total += rtv_line_data.amount
                        #                     invoice_line.append(invoice)
                        #
                        #     if len(invoice_line) > 0:
                        #         self.pool.get('account.invoice').write(cr, uid, res[0], {'amount_total': total,'currency_id':56,'invoice_line': invoice_line})
                            invoice_data.signal_workflow('invoice_open')

                            # Following Query flag that invoice and transfer will set boolean value
                            cr.execute("update account_invoice set rtv_id=%s where id=%s", ([rtv_odj.id, res[0]]))
                            cr.execute("update account_invoice set rtv='True' where id=%s", ([res[0]]))

                            received_rtv_query = "UPDATE rtv_process SET state='received',delivered = TRUE ,received_time='{0}', received_by='{1}',not_receive_reason='{2}',invoice_id={3} WHERE id={4}".format(
                                str(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')), receiver,nrr, res[0],rtv_odj.id)
                            cr.execute(received_rtv_query)
                            cr.commit()

                            received_rtv_query_line = "UPDATE rtv_process_line SET state='received', received_time='{0}' WHERE rtv_process_id={1}".format(
                                str(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')), rtv_odj.id)
                            cr.execute(received_rtv_query_line)
                            cr.commit()

                            self.pool.get('rtv.end.to.end').rtv_received_update(cr, uid, rtv_odj.name, rtv_odj.id,
                                                                                str(datetime.datetime.now()), 'received',
                                                                                context=context)
                            self.email_template(cr, uid,rtv_odj.id,context)

                    remaining_stock_id = self.pool['stock.picking'].search(cr, uid,
                                                                                    [('backorder_id', '=',picking[0] )],
                                                                                    context=context)
                    if remaining_stock_id:
                        self.pool.get('stock.picking').do_unreserve(cr, uid, remaining_stock_id, context=context)

                        self.pool.get('stock.picking').action_cancel(cr, uid, remaining_stock_id, context)


        return True


    def email_template(self,cr, uid,id,context=None):
        self.pool.get('rtv.process').send_email(cr, uid, 'RTV Received Challan', id,
                                                context=context)
        return True


class srn_rtv_line(osv.osv):
    _name = "srn.rtv.line"
    _description = "SRN RTV Line"

    _columns = {

        'srn_rtv_line_id': fields.many2one('srn.process', 'SRN Process ID'),

        'stock_id': fields.many2one('stock.picking', 'Stock Number'),

        'reverse_stock_id': fields.many2one('stock.picking', 'Reverse Stock Number'),

        'pqc': fields.boolean('PQC'),
        'pqc_id': fields.many2one("pending.quality.control", 'PQC ID'),
        'pqc_number': fields.char('PQC Number'),

        'rtv_name': fields.char('RTV Number'),
        'rtv_id': fields.many2one('rtv.process', 'RTV Process ID'),
        'delivered': fields.boolean('Delivered'),

        'received': fields.boolean('Received'),
        'received_by': fields.many2one('res.users', 'Received By'),
        'received_time': fields.datetime('Received Time'),

        'not_received': fields.boolean('Not Received'),
        'not_received_by': fields.many2one('res.users', 'Not Received By'),
        'not_received_time': fields.datetime('Not Received Time'),

        'confirmed_by': fields.many2one('res.users', 'Confirmed By'),
        'confirmed_time': fields.datetime('Confirmed Time'),

        'requested_by': fields.many2one('res.users', 'Requested By'),
        'requested_time': fields.datetime('Requested Time'),

        'resolved': fields.boolean('Resolved'),
        'resolved_by': fields.many2one('res.users', 'Resolved By'),
        'resolved_time': fields.datetime('Resolved Time'),

        'cancel': fields.boolean('Cancel'),
        'cancel_by': fields.many2one('res.users', 'Cancel By'),
        'cancel_time': fields.datetime('Cancel Time'),

        'state': fields.selection([
            ('pending', 'Pending'),
            ('confirmed', 'Confirmed'),
            ('received', 'Received'),
            ('not_received', 'Not Received'),
            ('partial_received', 'Partial Received'),
            ('cancel', 'Cancelled'),

        ], 'Status', readonly=True, copy=False, help="Gives the status of the SRN Process", select=True),

        'cancel_reason': fields.text('Cancel Reason'),

        'not_receive_reason_date': fields.datetime('Not Receive Reason Date'),
        'not_receive_reason_by': fields.many2one('res.users', 'Not Receive Reason By'),
        # 'not_receive_reason': fields.text('Not Receive SRN RTV Reason'),

        # 'not_receive_reason': fields.selection([
        #     ("Vendor was unreachable", "Vendor was unreachable"),
        #     ("Vendor refused to provide", "Vendor refused to provide"),
        #     ("Unable to reach", "Unable to reach"),
        #     ("Vendors place was closed", "Vendors place was closed"),
        #     ("Cash problem", "Cash problem"),
        #     ("Vendor rescheduled", "Vendor rescheduled"),
        #     ("Wrong address given", "Wrong address given"),
        #     ("Unable to reach on Time", "Unable to reach on Time"),
        #     ("Quality issue", "Quality issue"),
        #     ("Due to traffic issue", "Due to traffic issue")
        #
        # ], 'Not Receive Reason', copy=False, help="Reason", select=True),

        'not_receive_reason': fields.selection([
            ("Shop off", "Shop off"),
            ("Vendor unreachable", "Vendor unreachable"),
            ("Vendor Rescheduled", "Vendor Rescheduled"),
            ("Does not agree to take", "Does not agree to take"),
            ("Not received due to quality", "Not received due to quality"),
            ("Vendor is not aware of", "Vendor is not aware of")

        ], 'Not Receive Reason', copy=False, help="Reason", select=True),

        'cancel_srn_rtv_date': fields.datetime('Cancel SRN RTV Date'),
        'cancel_srn_rtv_by': fields.many2one('res.users', 'Cancel SRN RTV By'),
        'cancel_srn_rtv_reason': fields.text('Cancel SRN RTV Reason'),
        'srn_rtv_number': fields.char('Challan Number'),

    }

    def not_receive_rtv_srn(self, cr, uid, ids, context=None):
        for single_id in ids:
            not_received_srn_query = "UPDATE srn_process SET state='not_received', not_received_by={0}, not_received_time='{1}' WHERE id={2}".format(
                uid, str(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')), single_id)
            cr.execute(not_received_srn_query)
            cr.commit()

            not_received_srn_rtv_line_query = "UPDATE srn_rtv_line SET state='not_received', not_received_by={0}, not_received_time='{1}' WHERE srn_rtv_line_id={2}".format(
                uid, str(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')), single_id)
            cr.execute(not_received_srn_rtv_line_query)
            cr.commit()

            not_received_srn_rtv_product_query = "UPDATE srn_rtv_product SET state='not_received', not_received_time='{0}' WHERE srn_rtv_product_id={1}".format(
                str(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')), single_id)
            cr.execute(not_received_srn_rtv_product_query)
            cr.commit()

        return True


class srn_rtv_product(osv.osv):
    _name = "srn.rtv.product"
    _description = "SRN RTV Product"

    _columns = {
        # 'srn_process_id': fields.many2one('srn.process', 'SRN Process ID', ondelete='cascade', select=True, readonly=True),

        'srn_rtv_product_id': fields.many2one('srn.process', 'SRN RTV Product', required=True, ondelete='cascade',
                                              select=True, readonly=True),

        'stock_id': fields.many2one('stock.picking', 'Stock Number'),
        'pqc': fields.boolean('PQC'),
        'pqc_id': fields.many2one("pending.quality.control", 'PQC ID'),
        'pqc_number': fields.char('PQC Number'),

        'rtv_name': fields.char('RTV Number'),
        'rtv_id': fields.many2one('rtv.processs', 'RTV Process ID'),

        'product_id': fields.many2one('product.product', 'Product'),
        'cancel_time': fields.datetime('Cancel Time'),
        'resolved_time': fields.datetime('Resolved Time'),
        'received_time': fields.datetime('Received Time'),
        'not_received_time': fields.datetime('Not Received Time'),
        'requested_time': fields.datetime('Requested Time'),
        'confirmed_time': fields.datetime('Confirmed Time'),
        'return_qty': fields.float('Return Qty'),
        'remaining_qty': fields.float('Remaining Qty'),
        'available_qty': fields.float('Free Qty'),
        'requested_qty': fields.float('Request Qty'),
        'unit_cost': fields.float('Unit Rate'),
        'amount': fields.float('Amount'),
        'cal_amount': fields.float(''),
        'state': fields.selection([
            ('pending', 'Pending'),
            ('confirmed', 'Confirmed'),
            ('received', 'Received'),
            ('not_received', 'Not Received'),
            ('partial_received', 'Partial Received'),
            ('cancel', 'Cancelled'),

        ], 'Status', readonly=True, copy=False, help="Gives the status of the SRN Process", select=True),

        'not_receive_reason_date': fields.datetime('Not Receive Reason Date'),
        'not_receive_reason_by': fields.many2one('res.users', 'Not Receive Reason By'),
        # 'not_receive_reason': fields.text('Not Receive SRN RTV Reason'),

        # 'not_receive_reason': fields.selection([
        #     ("Vendor was unreachable", "Vendor was unreachable"),
        #     ("Vendor refused to provide", "Vendor refused to provide"),
        #     ("Unable to reach", "Unable to reach"),
        #     ("Vendors place was closed", "Vendors place was closed"),
        #     ("Cash problem", "Cash problem"),
        #     ("Vendor rescheduled", "Vendor rescheduled"),
        #     ("Wrong address given", "Wrong address given"),
        #     ("Unable to reach on Time", "Unable to reach on Time"),
        #     ("Quality issue", "Quality issue"),
        #     ("Due to traffic issue", "Due to traffic issue")
        #
        # ], 'Not Receive Reason', copy=False, help="Reason", select=True),

        'not_receive_reason': fields.selection([
            ("Shop off", "Shop off"),
            ("Vendor unreachable", "Vendor unreachable"),
            ("Vendor Rescheduled", "Vendor Rescheduled"),
            ("Does not agree to take", "Does not agree to take"),
            ("Not received due to quality", "Not received due to quality"),
            ("Vendor is not aware of", "Vendor is not aware of")

        ], 'Not Receive Reason', copy=False, help="Reason", select=True),

        'cancel_srn_rtv_date': fields.datetime('Cancel SRN RTV Date'),
        'cancel_srn_rtv_by': fields.many2one('res.users', 'Cancel SRN RTV By'),
        'cancel_srn_rtv_reason': fields.text('Cancel SRN RTV Reason'),

        'srn_rtv_number': fields.char('Challan Number'),

    }


class NotReceiveSRNRTVReason(osv.osv):
    _name = "not.receive.srn.rtv.reason"
    _description = "Not Receive Reason"

    _columns = {
        'not_receive_reason_date': fields.datetime('Not Receive Reason Date'),
        'not_receive_reason_by': fields.many2one('res.users', 'Not Receive Reason By'),

        # 'not_receive_reason': fields.selection([
        #     ("Vendor was unreachable", "Vendor was unreachable"),
        #     ("Vendor refused to provide", "Vendor refused to provide"),
        #     ("Unable to reach", "Unable to reach"),
        #     ("Vendors place was closed", "Vendors place was closed"),
        #     ("Cash problem", "Cash problem"),
        #     ("Vendor rescheduled", "Vendor rescheduled"),
        #     ("Wrong address given", "Wrong address given"),
        #     ("Unable to reach on Time", "Unable to reach on Time"),
        #     ("Quality issue", "Quality issue"),
        #     ("Due to traffic issue", "Due to traffic issue")
        #
        # ], 'Not Receive Reason', copy=False, help="Reason", select=True),

        'not_receive_reason': fields.selection([
            ("Shop off", "Shop off"),
            ("Vendor unreachable", "Vendor unreachable"),
            ("Vendor Rescheduled", "Vendor Rescheduled"),
            ("Does not agree to take", "Does not agree to take"),
            ("Not received due to quality", "Not received due to quality"),
            ("Vendor is not aware of", "Vendor is not aware of")

        ], 'Not Receive Reason', copy=False, help="Reason", select=True),

    }

    def not_receive_reason_def(self, cr, uid, ids, context=None):
        ids = context['active_ids']
        not_receive_reason = str(context['not_receive_reason'])
        not_receive_reason_by = uid
        not_receive_reason_date = str(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'))

        srn_pro_obj = self.pool.get('srn.process')

        srn_obj = srn_pro_obj.browse(cr, uid, ids, context=context)
        if srn_obj[0].state=='confirmed':

            rtv_ids = [i.rtv_id.id for i in srn_obj.srn_line if i.delivered == False]

            for s_id in srn_obj:

                for srn_line_id in srn_obj.srn_line:
                    not_receive_srn_rtv_query = "UPDATE srn_rtv_line SET state='not_received', not_receive_reason='{0}', not_receive_reason_by={1},                                                 not_receive_reason_date='{2}' WHERE srn_rtv_line_id={3} and id={4}".format(
                                                not_receive_reason, not_receive_reason_by, not_receive_reason_date, s_id.id, srn_line_id.id)
                    cr.execute(not_receive_srn_rtv_query)
                    cr.commit()

                for srn_product_line_id in srn_obj.srn_product_line:
                    not_receive_srn_product_query = "UPDATE srn_rtv_product SET state='not_received',not_receive_reason='{0}', not_receive_reason_by={1},                                                not_receive_reason_date='{2}' WHERE srn_rtv_product_id={3} and id={4}".format(
                        not_receive_reason, not_receive_reason_by, not_receive_reason_date, s_id.id, srn_product_line_id.id)
                    cr.execute(not_receive_srn_product_query)
                    cr.commit()

                not_receive_srn_query = "UPDATE srn_process SET state='not_received', not_receive_reason='{0}', not_receive_reason_by={1},                                                   not_receive_reason_date='{2}' WHERE id={3}".format(
                    not_receive_reason, not_receive_reason_by, not_receive_reason_date, s_id.id)
                cr.execute(not_receive_srn_query)
                cr.commit()

            srn_pro_obj.not_receive_srn(cr, uid,ids, rtv_ids, not_receive_reason, context)
        else:
            raise osv.except_osv(_('Warning!'),
                                 _('This SRN already processed.'))

        return True


class NotReceiveRTVReason(osv.osv):
    _name = "not.receive.rtv.reason"
    _description = "Not Receive Reason"

    _columns = {
        'not_receive_reason_date': fields.datetime('Not Receive Reason Date'),
        'not_receive_reason_by': fields.many2one('res.users', 'Not Receive Reason By'),

        # 'not_receive_reason': fields.selection([
        #     ("Vendor was unreachable", "Vendor was unreachable"),
        #     ("Vendor refused to provide", "Vendor refused to provide"),
        #     ("Unable to reach", "Unable to reach"),
        #     ("Vendors place was closed", "Vendors place was closed"),
        #     ("Cash problem", "Cash problem"),
        #     ("Vendor rescheduled", "Vendor rescheduled"),
        #     ("Wrong address given", "Wrong address given"),
        #     ("Unable to reach on Time", "Unable to reach on Time"),
        #     ("Quality issue", "Quality issue"),
        #     ("Due to traffic issue", "Due to traffic issue")
        #
        # ], 'Not Receive Reason', copy=False, help="Reason", select=True),

        'not_receive_reason': fields.selection([
            ("Shop off", "Shop off"),
            ("Vendor unreachable", "Vendor unreachable"),
            ("Vendor Rescheduled", "Vendor Rescheduled"),
            ("Does not agree to take", "Does not agree to take"),
            ("Not received due to quality", "Not received due to quality"),
            ("Vendor is not aware of", "Vendor is not aware of")

        ], 'Not Receive Reason', copy=False, help="Reason", select=True),

        'srn_id': fields.integer('SRN ID'),
        'rtv_id': fields.integer('RTV ID'),
        'rtv_srn_id': fields.integer(' SRN RTV Line ID'),

    }


    def default_get(self, cr, uid, fields, context=None):
        if context is None:
            context = {}
        res = super(NotReceiveRTVReason, self).default_get(cr, uid, fields, context=context)
        srn_rtv = self.pool.get('srn.rtv.line').browse(cr, uid, context['active_id'], context=context)

        res['srn_id'] = int(srn_rtv.srn_rtv_line_id)
        res['rtv_id'] = int(srn_rtv.rtv_id)
        res['rtv_srn_id'] = context['active_id']

        return res

    def rtv_wise_not_receive(self, cr, uid, ids, fields, context=None):

        if context is None:
            context = {}
        get_data = self.read(cr, uid, ids)[0]
        nrr = fields['not_receive_reason']

        srn_pro_obj = self.pool.get('srn.process')

        srn_obj = srn_pro_obj.browse(cr, uid, int(get_data['srn_id']), context=context)

        if nrr != ' ':
            srn_pro_obj.not_receive_srn(cr, uid, ids, [int(get_data['rtv_id'])],nrr, context)

            received_srn_rtv_line_query = "UPDATE srn_rtv_line SET state='not_received', not_receive_reason='{0}', received_by={1}, received_time='{2}' WHERE srn_rtv_line_id={3} and id = {4}".format(nrr, uid, str(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')), int(get_data['srn_id']), int(get_data['rtv_srn_id']))
            cr.execute(received_srn_rtv_line_query)
            cr.commit()

            received_srn_rtv_product_query = "UPDATE srn_rtv_product SET state='not_received', received_time='{0}' WHERE srn_rtv_product_id={1} and rtv_id = {2}".format(str(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')), int(get_data['srn_id']), int(get_data['rtv_id']))
            cr.execute(received_srn_rtv_product_query)
            cr.commit()

            res = [srn_line.state for srn_line in srn_obj.srn_line]

            if len(list(set(res))) == 1:
                not_received_srn_query = "UPDATE srn_process SET state='not_received', delivered = TRUE , received_by={0}, received_time='{1}' WHERE id={2}".format(
                    uid, str(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')), int(get_data['srn_id']))
                cr.execute(not_received_srn_query)
                cr.commit()

            elif 'confirmed' in res:
                not_received_srn_query = "UPDATE srn_process SET state='confirmed', delivered = TRUE , received_by={0}, received_time='{1}' WHERE id={2}".format(
                    uid, str(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')), int(get_data['srn_id']))
                cr.execute(not_received_srn_query)
                cr.commit()

            else:
                partial_received_srn_query = "UPDATE srn_process SET state='partial_received', delivered = TRUE , received_by={0}, received_time='{1}' WHERE id={2}".format(
                    uid, str(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')), int(get_data['srn_id']))
                cr.execute(partial_received_srn_query)
                cr.commit()


        else:
            raise osv.except_osv(_('Warning!'),
                                 _('Please, Given actual reason.'))
        return True


class CancelSRNRTVReason(osv.osv):
    _name = "cancel.srn.rtv.reason"
    _description = "Cancel SRN RTV Reason"

    _columns = {
        'cancel_srn_rtv_date': fields.datetime('Cancel SRN RTV Date'),
        'cancel_srn_rtv_by': fields.many2one('res.users', 'Cancel SRN RTV By'),
        'cancel_srn_rtv_reason': fields.text('Cancel SRN RTV Reason', required=True),

    }

    def cancel_srn_rtv_reason_def(self, cr, uid, ids, context=None):
        ids = context['active_ids']
        cancel_srn_rtv_reason = str(context['cancel_srn_rtv_reason'])
        cancel_srn_rtv_by = uid
        cancel_srn_rtv_date = str(fields.datetime.now())

        srn_pro_obj = self.pool.get('srn.process')

        for s_id in ids:
            srn_obj = srn_pro_obj.browse(cr, uid, s_id, context=context)

            cancel_srn_rtv_query = "UPDATE srn_process SET cancel_srn_rtv_reason='{0}', cancel_srn_rtv_by={1}, cancel_srn_rtv_date='{2}' WHERE id={3}".format(
                cancel_srn_rtv_reason, cancel_srn_rtv_by, cancel_srn_rtv_date, s_id)
            cr.execute(cancel_srn_rtv_query)
            cr.commit()

            for srn_line_id in srn_obj.srn_line:
                cancel_srn_rtv_query = "UPDATE srn_rtv_line SET cancel_srn_rtv_reason='{0}', cancel_srn_rtv_by={1}, cancel_srn_rtv_date='{2}' WHERE srn_rtv_line_id={3} and id={4}".format(
                    cancel_srn_rtv_reason, cancel_srn_rtv_by, cancel_srn_rtv_date, srn_line_id.id, s_id)
                cr.execute(cancel_srn_rtv_query)
                cr.commit()

                self.pool.get('rtv.process').cancel_rtv(cr, uid, [srn_line_id.rtv_id.id], context=context)

            for srn_product_line_id in srn_obj.srn_product_line:
                cancel_srn_rtv_query = "UPDATE srn_rtv_product SET cancel_srn_rtv_reason='{0}', cancel_srn_rtv_by={1}, cancel_srn_rtv_date='{2}' WHERE srn_rtv_product_id={3} and id={4}".format(
                    cancel_srn_rtv_reason, cancel_srn_rtv_by, cancel_srn_rtv_date, srn_product_line_id.id, s_id)
                cr.execute(cancel_srn_rtv_query)
                cr.commit()

            srn_reason = srn_pro_obj.cancel_srn(cr, uid, [s_id], context)


        return True


class ReceiveSRNRTVWizard(osv.osv):
    _name = "receive.srn.rtv.wizard"

    _columns = {
        'name': fields.char('RTV Number'),
        'warehouse_id': fields.selection([
            (1, 'Central Warehouse Uttara'),
            (2, 'Nodda Warehouse'),
            (3, 'Nodda Retail Warehouse'),
            (4, 'Motijheel Warehouse'),
            (6, 'Uttara Retail Warehouse'),
            (7, 'Motijheel Retail Warehouse'),
            (8, 'Damage Warehouse'),

        ], 'Warehouse', readonly=True, copy=False, help="Gives the List of the Warehouses", select=True),

        # 'not_receive_reason': fields.selection([
        #
        #     ("Vendor was unreachable", "Vendor was unreachable"),
        #     ("Vendor refused to provide", "Vendor refused to provide"),
        #     ("Unable to reach", "Unable to reach"),
        #     ("Vendors place was closed", "Vendors place was closed"),
        #     ("Cash problem", "Cash problem"),
        #     ("Vendor rescheduled", "Vendor rescheduled"),
        #     ("Wrong address given", "Wrong address given"),
        #     ("Unable to reach on Time", "Unable to reach on Time"),
        #     ("Quality issue", "Quality issue"),
        #     ("Due to traffic issue", "Due to traffic issue"),
        #     ("Invoice/Cash Memo not provided", "Invoice/Cash Memo not provided")
        #
        # ], 'Not Received Reason', copy=False, help="Reason", select=True),

        'not_receive_reason': fields.selection([
            ("Shop off", "Shop off"),
            ("Vendor unreachable", "Vendor unreachable"),
            ("Vendor Rescheduled", "Vendor Rescheduled"),
            ("Does not agree to take", "Does not agree to take"),
            ("Not received due to quality", "Not received due to quality"),
            ("Vendor is not aware of", "Vendor is not aware of")

        ], 'Not Receive Reason', copy=False, help="Reason", select=True),

        'rtv_type': fields.selection([
            ('pqc', 'PQC'),
            ('stock', 'Stock')

        ], 'RTV Type', readonly=True, copy=False, help="Gives the RTV Type ", select=True),
        'vendor_id': fields.integer('Vendor Id'),
        'rtv_srn_id': fields.integer(''),
        'rtv_id': fields.integer(''),
        'srn_rtv_line_id': fields.integer(''),
        'vendor_name': fields.char('Vendor'),
        'area': fields.char('Area'),
        'remark': fields.text('Remark'),
        'ref_po_number': fields.char('Ref PO Number'),
        'pqc': fields.boolean('PQC'),
        'pqc_id': fields.many2one("pending.quality.control", 'PQC ID'),
        'pqc_number': fields.char('PQC Number'),
        'state': fields.selection([
            ('pending', 'Pending'),
            ('requested', 'Requested'),
            ('resolved', 'Resolved'),
            ('confirmed', 'Ready for Return'),
            ('create_srn', 'SRN'),
            ('received', 'Received'),
            ('cancel', 'Cancelled'),

        ], 'Status', readonly=True, copy=False, help="Gives the status of the RTV Process", select=True),
        'rtv_product_line': fields.one2many('rtv.product.line.wizard', 'rtv_process_id', 'RTV Process Line',
                                            required=True),
        'stock_id': fields.many2one('stock.picking', 'Stock Number'),

        'resolve_rtv_process_date': fields.datetime('Resolve rtv Process Date'),
        'resolve_rtv_process_by': fields.many2one('res.users', 'Resolve rtv Process By'),
        'resolve_rtv_process_reason': fields.text('Resolve rtv Process Reason'),

        'cancel_rtv_process_date': fields.datetime('Cancel rtv Process Date'),
        'cancel_rtv_process_by': fields.many2one('res.users', 'Cancel rtv Process By'),
        'cancel_rtv_process_reason': fields.text('Cancel rtv Process Reason'),
    }

    def default_get(self, cr, uid, fields, context=None):
        if context is None:
            context = {}
        res = super(ReceiveSRNRTVWizard, self).default_get(cr, uid, fields, context=context)
        srn_rtv = self.pool.get('srn.rtv.line').browse(cr, uid, context['active_id'], context=context)
        rtv_obj = self.pool.get('rtv.process').browse(cr, uid, srn_rtv.rtv_id.id, context=context)
        items = []
        for lines in rtv_obj.rtv_process_line:
            line = lines[0]
            if float(line.requested_qty) > 0.00:
                item = {
                    'rtv_product_id': int(line.id),
                    'product_id': int(line.product_id.id),
                    'requested_qty': float(line.requested_qty),
                    'return_qty': float(line.return_qty),
                    'remaining_qty': float(line.remaining_qty),
                    'amount': float(line.amount),
                    'cal_amount': float(line.cal_amount),
                    'unit_cost': float(line.unit_cost),
                }

                for product_line in self.pool.get('srn.process').browse(cr, uid, srn_rtv.srn_rtv_line_id.id,
                                                                        context=context).srn_product_line:
                    if line.product_id.id == product_line.product_id.id:
                        item['srn_rtv_product_line_id'] = int(product_line.id)

                items.append(item)

        res['rtv_type'] = str(rtv_obj.rtv_type)
        res['warehouse_id'] = int(rtv_obj.warehouse_id)
        res['name'] = str(rtv_obj.name)
        res['pqc'] = (rtv_obj.pqc)
        res['vendor_id'] = int(rtv_obj.vendor.id)
        res['vendor_name'] = str(rtv_obj.vendor.name)
        res['pqc_id'] = int(rtv_obj.pqc_id.id) if rtv_obj.pqc_id else ''
        res['pqc_number'] = str(rtv_obj.pqc_number) if rtv_obj.pqc_number else ''
        res['area'] = str(rtv_obj.area.name) if rtv_obj.area else ''
        res['remark'] = str(rtv_obj.remark) if rtv_obj.remark else ''
        res['po_number'] = str(rtv_obj.po_number) if rtv_obj.po_number else ''
        res['stock_id'] = (rtv_obj.stock_id.id) if rtv_obj.stock_id else ''
        res['pqc'] = rtv_obj.pqc
        res['srn_rtv_line_id'] = int(srn_rtv.srn_rtv_line_id)
        res['rtv_id'] = int(srn_rtv.rtv_id)
        res['rtv_srn_id'] = context['active_id']

        res.update(rtv_product_line=items)

        return res

    def rtv_wise_receive(self, cr, uid, ids, fields, context=None):
        if context is None:
            context = {}
        get_data = self.read(cr, uid, ids)[0]
        sum_rem_qty = 0.0
        sum_req_qty = 0.0
        sum_return_qty = 0.0
        nrr = fields['not_receive_reason']

        for i in get_data.get('rtv_product_line'):
            result = self.pool.get('rtv.product.line.wizard').browse(cr, uid, i, context=context)
            remaining_qty = result.requested_qty - result.return_qty
            sum_rem_qty += remaining_qty
            sum_req_qty += result.requested_qty
            sum_return_qty += result.return_qty

            if remaining_qty < 0.00:
                raise osv.except_osv(_('RTV product line adjustment ERROR!'),
                                     _('Return quantity should be less then or equal to (requested quantity)!!!'))

        if sum_req_qty == sum_rem_qty:
            raise osv.except_osv(_('RTV product line adjustment ERROR!'),
                                 _('Return quantity should be less then or equal to (requested quantity)!!!'))

        elif sum_return_qty < 1.00:
            raise osv.except_osv(_('RTV product line adjustment ERROR!'),
                                 _('Return quantity should be less then or equal to 1 !!!'))

        elif sum_rem_qty > 0.00 and (nrr == ' ' or nrr is False):
            raise osv.except_osv(_('Not Receive Reason ERROR!'),
                                 _('Please give the Not Receive Reason!!!'))

        else:
            for row in get_data.get('rtv_product_line'):
                rtv_product = self.pool.get('rtv.product.line.wizard').browse(cr, uid, row, context=context)
                result_remaining_quantity = rtv_product.requested_qty - rtv_product.return_qty
                rtv_product.remaining_qty = result_remaining_quantity
                rtv_product.cal_amount = rtv_product.return_qty * rtv_product.unit_cost
                rtv_product.amount = rtv_product.return_qty * rtv_product.unit_cost

                update_query_rtv_process_line = "UPDATE rtv_process_line SET return_qty='{0}'" \
                                                ",remaining_qty = '{1}',cal_amount='{2}',amount= '{3}' WHERE " \
                                                "rtv_process_id = {4} and id={5}".format(
                    rtv_product.return_qty, rtv_product.remaining_qty,rtv_product.cal_amount,rtv_product.amount,
                    int(get_data['rtv_id']), rtv_product.rtv_product_id)
                cr.execute(update_query_rtv_process_line)
                cr.commit()

                srn_rtv_product_update_query = "UPDATE srn_rtv_product SET return_qty='{0}'" \
                                               ",remaining_qty = '{1}' WHERE srn_rtv_product_id={2} and rtv_id = {3} and id={4}".format(
                    rtv_product.return_qty, rtv_product.remaining_qty,
                    int(get_data['srn_rtv_line_id']), int(get_data['rtv_id']), int(rtv_product.srn_rtv_product_line_id))
                cr.execute(srn_rtv_product_update_query)
                cr.commit()

            if get_data['pqc'] is False:
                # self.pool.get('srn.process').transfer_stock(cr, uid, ids, [get_data['rtv_id']], reverse=False,
                #                                             context=None)
                self.pool.get('srn.process').create_invoice(cr, uid, ids, [get_data['rtv_id']],nrr=nrr, context=context)
            else:
                self.pool.get('srn.process').rtv_pqc_status_change(cr, uid, ids, [get_data['rtv_id']], context=None)
                if sum_rem_qty > 0.00:
                    self.rtv_to_pqc(cr, uid,ids, get_data['rtv_id'], context=None)

            received_srn_rtv_line_query = "UPDATE srn_rtv_line SET state='received',delivered=TRUE, received_by={0}, received_time='{1}' WHERE srn_rtv_line_id={2} and id = {3}".format(
                uid, str(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')), int(get_data['srn_rtv_line_id']), int(get_data['rtv_srn_id']))
            cr.execute(received_srn_rtv_line_query)
            cr.commit()

            received_srn_rtv_product_query = "UPDATE srn_rtv_product SET state='received', received_time='{0}' WHERE srn_rtv_product_id={1} and rtv_id = {2}".format(str(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')), int(get_data['srn_rtv_line_id']), int(get_data['rtv_id']))
            cr.execute(received_srn_rtv_product_query)
            cr.commit()

            srn_obj = self.pool.get('srn.process').browse(cr, uid, int(get_data['srn_rtv_line_id']),
                                                          context=context)
            res = [srn_line.state for srn_line in srn_obj.srn_line]

            if len(list(set(res))) == 1:
                received_srn_query = "UPDATE srn_process SET state='received', delivered = TRUE , received_by={0}, received_time='{1}' WHERE id={2}".format(
                    uid, str(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')), int(get_data['srn_rtv_line_id']))
                cr.execute(received_srn_query)
                cr.commit()
            elif 'confirmed' in res:
                received_srn_query = "UPDATE srn_process SET state='confirmed' WHERE id={0}".format(
                    uid, str(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')), int(get_data['srn_rtv_line_id']))
                cr.execute(received_srn_query)
                cr.commit()
            else:
                received_srn_query = "UPDATE srn_process SET state='partial_received',delivered = TRUE , received_by={0}, received_time='{1}' WHERE id={2}".format(
                    uid, str(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')), int(get_data['srn_rtv_line_id']))
                cr.execute(received_srn_query)
                cr.commit()

            return True

    def reverse_stock_transfer(self, cr, uid, ids,rtv_id, stock_id, context=None):

        id = rtv_id
        rtv_data = self.pool.get('rtv.process').browse(cr, uid, id, context=context)

        stock_picking_type_ids = self.pool['stock.picking.type'].search(cr, uid,
                                                                        [('name', 'like', 'Receipts')],
                                                                        context=context)
        stock_picking_type_data = self.pool['stock.picking.type'].browse(cr, uid, stock_picking_type_ids,
                                                                         context=context)
        location_id = None
        location_dest_id = None
        picking_type_id = None

        for items in stock_picking_type_data:

            if items.warehouse_id.id == rtv_data.warehouse_id:
                # location_id = items.default_location_src_id.id
                location_dest_id = items.default_location_dest_id.id
                picking_type_id = items.id
                location_id = self.pool['stock.location'].search(cr, uid, [('name', '=', 'Suppliers')],
                                                                 context=context)[0]
        move_line = []

        stock_data = {}

        if location_id is not None and location_dest_id is not None:
            stock_data = {'origin': stock_id[1],
                          'message_follower_ids': False,
                          'carrier_tracking_ref': False,
                          'number_of_packages': 0,
                          'date_done': False,
                          'carrier_id': False,
                          'write_uid': False,
                          'partner_id': rtv_data.vendor.id,
                          'message_ids': False,
                          'note': False,
                          'picking_type_id': picking_type_id,
                          'move_type': 'one',
                          'company_id': 1,
                          'priority': '1',
                          'picking_type_code': False,
                          'owner_id': False,
                          'min_date': False,
                          'date': strftime("%Y-%m-%d %H:%M:%S", gmtime()),
                          'pack_operation_ids': [],
                          'carrier_code': 'custom',
                          'invoice_state': '2binvoiced'}
            tmp_dict = {}
            for rtv_line in rtv_data.rtv_process_line:
                move_line.append([0, False,
                                  {'product_uos_qty': rtv_line.requested_qty,
                                   'date_expected': strftime("%Y-%m-%d %H:%M:%S", gmtime()),
                                   'date': strftime("%Y-%m-%d %H:%M:%S", gmtime()),
                                   'product_id': rtv_line.product_id.id,
                                   'product_uom': 1,
                                   'picking_type_id': picking_type_id,
                                   'product_uom_qty': rtv_line.remaining_qty,
                                   'price_unit': rtv_line.unit_cost,
                                   'invoice_state': '2binvoiced',
                                   'product_tmpl_id': False,
                                   'product_uos': False,
                                   'reserved_quant_ids': [],
                                   'location_dest_id': location_dest_id,
                                   'procure_method': 'make_to_stock',
                                   'product_packaging': False,
                                   'group_id': False,
                                   'location_id': location_id,
                                   'name': str(rtv_line.product_id.name)}])

            stock_data['move_lines'] = move_line

        stock_obj = self.pool.get('stock.picking')
        save_the_data = stock_obj.create(cr, uid, stock_data, context=context)

        stock_obj_confirmation = stock_obj.action_confirm(cr, uid, [save_the_data], context=context)
        stock_obj = stock_obj.action_assign(cr, uid, [save_the_data], context=context)

        requested_rtv_query = "UPDATE rtv_process SET reverse_stock_id='{0}' WHERE id={1}".format(save_the_data, id)
        cr.execute(requested_rtv_query)
        cr.commit()

        self.pool.get('srn.process').transfer_stock(cr, uid,ids, [id], reverse=True, context=None)

        return save_the_data

    def rtv_to_pqc(self, cr, uid, ids,rtv_id, context=None):
        if context is None:
            context = {}

        pqc_line_list = []

        rtv_data = self.pool.get('rtv.process').browse(cr, uid, rtv_id, context=context)

        pqc_obj = self.pool.get('pending.quality.control').browse(cr, uid,
                                                                  self.pool['pending.quality.control'].search(cr, uid, [
                                                                      ('pqc_number', '=', str(rtv_data.pqc_number))]))

        pqc_data_dict = {
            'po_id': pqc_obj.po_id,
            'po_number': str(pqc_obj.po_number),
            'irn_number': rtv_data.pqc_id.irn_number,
            'irn_id': rtv_data.pqc_id.irn_id,
            'qc_id': rtv_data.pqc_id.qc_id,
            'warehouse_id': rtv_data.warehouse_id,
            'vendor': rtv_data.vendor.id,
            'area': rtv_data.area.id,
            'state': 'pending',
            'qc_number': pqc_obj.qc_number,
            'received_by': uid,
            'pqc_number': str(rtv_data.pqc_number),
            'date_confirm': strftime("%Y-%m-%d %H:%M:%S", gmtime()),
            'pqc_id': rtv_data.pqc_id

        }
        for rtv_product in rtv_data.rtv_process_line:
            for pqc_data in pqc_obj.pqc_line:
                if rtv_product.product_id == pqc_data.product_id:
                    pqc_line_list.append([0, False, {
                        'rtv_id': rtv_data.id,
                        'rtv_name': rtv_data.name,
                        'qc_id': pqc_obj.qc_id,
                        'product': rtv_product.product_id.name,
                        'product_id': rtv_product.product_id.id,
                        'purchase_quantity': pqc_data.purchase_quantity,
                        'received_quantity': rtv_product.remaining_qty,
                        'accepted_quantity': float(0),
                        'pqc_reject_quantity': rtv_product.remaining_qty,
                        'pending_quantity': rtv_product.remaining_qty
                    }])
        pqc_data_dict['pqc_line'] = pqc_line_list

        if len(pqc_line_list) > 0:
            # Create pqc
            pqc_env = self.pool.get('pending.quality.control')
            saved_pqc_id = pqc_env.create(cr, uid, pqc_data_dict, context=context)

            rtv_query = "UPDATE rtv_process SET reverse_pqc_id='{0}' WHERE id={1}".format(
                saved_pqc_id, rtv_id)
            cr.execute(rtv_query)
            cr.commit()

            return saved_pqc_id
        else:
            return True


class RTVProductLineWizard(osv.osv):
    _name = "rtv.product.line.wizard"
    _description = "RTV Process Line Wizard"

    _columns = {
        'rtv_process_id': fields.many2one('receive.srn.rtv.wizard', 'RTV Process ID', required=True,
                                          ondelete='cascade', select=True, readonly=True),

        'product_id': fields.many2one('product.product', 'Product', required=True),
        'po_number': fields.char('Ref PO Number'),
        'pqc': fields.boolean('PQC'),
        'rtv_product_id': fields.integer('RTV Product ID'),
        'srn_rtv_product_line_id': fields.integer('SRN RTV Product Line ID'),
        'return_qty': fields.float('Return Qty'),
        'remaining_qty': fields.float('Remaining Qty'),
        'available_qty': fields.float('Free Qty'),
        'requested_qty': fields.float('Request Qty'),
        'unit_cost': fields.float('Unit Rate'),
        'amount': fields.float('Amount'),
        'cal_amount': fields.float(''),
        'state': fields.selection([
            ('pending', 'Pending'),
            ('requested', 'Requested'),
            ('resolved', 'Resolved'),
            ('confirmed', 'Ready for Return'),
            ('create_srn', 'SRN'),
            ('received', 'Received'),
            ('cancel', 'Cancelled'),

        ], 'Status', readonly=True, copy=False, help="Gives the status of the RTV Process", select=True),
    }

    def onchange_remaining_qty(self, cr, uid, ids, requested_qty=False, return_qty=False, context=None):
        values = {}
        if requested_qty and return_qty is not False:
            values['remaining_qty'] = float(requested_qty) - float(return_qty)

        else:
            values['remaining_qty'] = 0.00
        return {'value': values}

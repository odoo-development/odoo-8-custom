import time
from openerp.report import report_sxw
from openerp.osv import osv

class srnreportdata(report_sxw.rml_parse):

    def __init__(self, cr, uid, name, context):
        super(srnreportdata, self).__init__(cr, uid, name, context)
        self.localcontext.update({
            'get_warehouse_data': self.get_warehouse_data,
            'get_product_data': self.get_product_data

        })

    def get_warehouse_data(self, obj):

        origin = [(obj)]
        srn_origin = None
        warehouse = self.pool.get('stock.warehouse').browse(self.cr, self.uid, origin).partner_id
        warehouse_address=''
        phone=''

        if warehouse.street:
            warehouse_address = warehouse_address + warehouse.street + ", "
        if warehouse.street2:
            warehouse_address = warehouse_address + warehouse.street2 + ", "

        if warehouse.city:
            warehouse_address = warehouse_address + warehouse.city + ", "
        if warehouse.state_id:
            warehouse_address = warehouse_address + warehouse.state_id.name + ", "
        if warehouse.zip:
            warehouse_address = warehouse_address + warehouse.zip + ", "

        if warehouse.country_id:
            warehouse_address = warehouse_address + warehouse.country_id.name + ", "

        if warehouse.phone:
            warehouse_address = warehouse_address + warehouse.phone + ", "
        if warehouse.mobile:
            warehouse_address = warehouse_address + warehouse.mobile


        return warehouse_address

    def get_product_data(self, obj):
        report_data = []
        rtv_id = [int(obj)]
        rtv_data = self.pool.get('rtv.process').browse(self.cr, self.uid, rtv_id)
        for it in rtv_data.rtv_process_line:
            report_data.append({
                'product_name': it.product_id.name,
                'product_description': it.product_id.name,
                'product_uom': it.product_id.prod_uom,
                'requested_qty': it.requested_qty,
                'return_qty': it.return_qty,
                'remaining_qty': it.remaining_qty,
                'unit_cost': it.unit_cost,
                'amount': it.amount,
            })

        return report_data




class report_srn_layout(osv.AbstractModel):
    _name = 'report.wms_rtv_process.report_srn_layout'
    _inherit = 'report.abstract_report'
    _template = 'wms_rtv_process.report_srn_layout'
    _wrapped_report_class = srnreportdata
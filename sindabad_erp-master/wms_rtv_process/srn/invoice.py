from openerp import models, fields, api, _

class account_invoice(models.Model):
    _inherit = "account.invoice"

    rtv_id = fields.Many2one('rtv.process', string='RTV Number',required=False, track_visibility='onchange')
    rtv = fields.Boolean(string='RTV')

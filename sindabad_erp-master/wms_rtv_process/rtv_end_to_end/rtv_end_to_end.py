from openerp.osv import fields, osv

class rtv_end_to_end(osv.osv):
    _name = "rtv.end.to.end"
    _description = "Inbound End-to-End"

    _columns = {
        'rtv_number': fields.char('RTV Number'),
        'rtv_id': fields.integer('RTV ID'),
        'warehouse_name': fields.char('Warehouse'),
        'vendor_name': fields.char('Vendor'),
        'ref_po_number': fields.char('Ref PO Number'),
        'ref_pqc_number': fields.char('Ref PO Number'),
        'stock_number': fields.char('Stock Number'),
        'rev_stock_number': fields.char('Reverse Stock Number'),
        'aqc_number': fields.char('Stock'),
        'rev_pqc_number': fields.char('Reverse PQC Number'),
        'rtv_date_create': fields.text('Created Date'),
        'rtv_date_requested': fields.text('Requested Date'),
        'rtv_date_resolved': fields.text('Resolved Date'),
        'rtv_date_received': fields.text('Received Date'),
        'rtv_date_reattempt': fields.text('Reattempt Date'),
        'rtv_date_confirm': fields.text('Ready for Return Date'),
        'rtv_date_not_received': fields.text('Not Received Date'),
        'srn_number': fields.char('SRN Numbers'),
        'challan_number': fields.char('Challan Numbers'),
        'srn_date_create': fields.text('SRN Created Date'),
        'rtv_date_cancel': fields.text('Cancel Date'),
        'rtv_date_approval': fields.text('Approval Date'),
        'srn_date_cancel': fields.text('SRN Cancel Date'),
        'state': fields.selection([
            ('pending', 'Pending'),
            ('requested', 'Requested'),
            ('resolved', 'Resolved'),
            ('confirmed', 'Ready for Return'),
            ('create_srn', 'Create SRN'),
            ('received', 'Received'),
            ('not_received', 'Not Received'),
            ('pending_approval', 'Pending Approval'),
            ('approved', 'Approved'),
            ('cancel', 'Cancelled'),

        ], 'Status', readonly=True, copy=False, help="Gives the status of the RTV Process", select=True),
    }

    def rtv_data_create(self, cr, uid, rtv_number,rtv_id,value, context=None):

        rtv_count = self.search_count(cr, uid, [('rtv_number','=',rtv_number), ('rtv_id', '=', rtv_id)])

        if int(rtv_count) == 0:
            created_id = self.create(cr, uid, {
                'rtv_number': rtv_number,
                'rtv_id': rtv_id,
                'vendor_name': value['vendor_name'],
                'warehouse_name': value['warehouse_name'],
                'rtv_date_create': value['rtv_date_create'],
                'ref_po_number': value['ref_po_number'] if value['ref_po_number'] else '',
                'ref_pqc_number': value['ref_pqc_number'] if value['ref_pqc_number'] else '',
                'state':value['state']
            }, context=context)
        return True

    def rtv_confirm_update(self, cr, uid, rtv_number, rtv_id, rtv_date_confirm,state, context=None):

        etoe_id = self.search(cr, uid, [('rtv_number', '=', rtv_number), ('rtv_id', '=', rtv_id)])

        # browse and then update
        for etoe in self.browse(cr, uid, etoe_id, context):

            self.write(cr, uid, etoe_id, {
                'rtv_date_confirm': str(rtv_date_confirm) if str(etoe.rtv_date_confirm) == 'False' else str(etoe.rtv_date_confirm) + "," + str(rtv_date_confirm),
                'state':state
            })

        return True

    def rtv_request_update(self, cr, uid, rtv_number, rtv_id, rtv_date_requested,state, context=None):

        etoe_id = self.search(cr, uid, [('rtv_number', '=', rtv_number), ('rtv_id', '=', rtv_id)])

        # browse and then update
        for etoe in self.browse(cr, uid, etoe_id, context):

            self.write(cr, uid, etoe_id, {
                'rtv_date_requested': str(rtv_date_requested) if str(etoe.rtv_date_requested) == 'False' else str(etoe.rtv_date_requested) + "," + str(rtv_date_requested),
                'state':state
            })

        return True

    def rtv_resolve_update(self, cr, uid, rtv_number, rtv_id, rtv_date_resolved,state, context=None):

        etoe_id = self.search(cr, uid, [('rtv_number', '=', rtv_number), ('rtv_id', '=', rtv_id)])

        # browse and then update
        for etoe in self.browse(cr, uid, etoe_id, context):

            self.write(cr, uid, etoe_id, {
                'rtv_date_resolved': str(rtv_date_resolved) if str(etoe.rtv_date_resolved) == 'False' else str(etoe.rtv_date_resolved) + "," + str(rtv_date_resolved),
                'state':state
            })

        return True

    def rtv_reattempt_update(self, cr, uid, rtv_number, rtv_id, rtv_date_reattempt,state, context=None):

        etoe_id = self.search(cr, uid, [('rtv_number', '=', rtv_number), ('rtv_id', '=', rtv_id)])

        # browse and then update
        for etoe in self.browse(cr, uid, etoe_id, context):

            self.write(cr, uid, etoe_id, {
                'rtv_date_reattempt': str(rtv_date_reattempt) if str(etoe.rtv_date_reattempt) == 'False' else str(etoe.rtv_date_reattempt) + "," + str(rtv_date_reattempt),
                'state':state
            })

        return True

    def rtv_approval_update(self, cr, uid, rtv_number, rtv_id, rtv_date_approval,state, context=None):

        etoe_id = self.search(cr, uid, [('rtv_number', '=', rtv_number), ('rtv_id', '=', rtv_id)])

        # browse and then update
        for etoe in self.browse(cr, uid, etoe_id, context):

            self.write(cr, uid, etoe_id, {
                'rtv_date_approval': str(rtv_date_approval) if str(etoe.rtv_date_approval) == 'False' else str(etoe.rtv_date_approval) + "," + str(rtv_date_approval),
                'state':state
            })

        return True

    def rtv_received_update(self, cr, uid, rtv_number, rtv_id, rtv_date_received,state, context=None):

        etoe_id = self.search(cr, uid, [('rtv_number', '=', rtv_number), ('rtv_id', '=', rtv_id)])

        # browse and then update
        for etoe in self.browse(cr, uid, etoe_id, context):

            self.write(cr, uid, etoe_id, {
                'rtv_date_received': str(rtv_date_received) if str(etoe.rtv_date_received) == 'False' else str(etoe.rtv_date_received) + "," + str(rtv_date_received),
                'state':state
            })

        return True

    def rtv_not_received_update(self, cr, uid, rtv_number, rtv_id, rtv_date_not_received,state, context=None):

        etoe_id = self.search(cr, uid, [('rtv_number', '=', rtv_number), ('rtv_id', '=', rtv_id)])

        # browse and then update
        for etoe in self.browse(cr, uid, etoe_id, context):

            self.write(cr, uid, etoe_id, {
                'rtv_date_not_received': str(rtv_date_not_received) if str(etoe.rtv_date_not_received) == 'False' else str(etoe.rtv_date_not_received) + "," + str(rtv_date_not_received),
                'state':state
            })

        return True

    def rtv_srn_update(self, cr, uid, rtv_number, rtv_id, srn_number,challan_number,state,srn_date_create, context=None):

        etoe_id = self.search(cr, uid, [('rtv_number', '=', rtv_number), ('rtv_id', '=', rtv_id)])

        # browse and then update
        for etoe in self.browse(cr, uid, etoe_id, context):

            self.write(cr, uid, etoe_id, {
                'srn_number': str(srn_number),
                'challan_number': str(challan_number) if str(
                    etoe.challan_number) == 'False' else str(etoe.challan_number) + "," + str(
                    challan_number),
                'state': str(state),
                'srn_date_create': str(srn_date_create) if str(
                    etoe.srn_date_create) == 'False' else str(etoe.srn_date_create) + "," + str(
                    srn_date_create),
            })

        return True

    def rtv_cancel_update(self, cr, uid, rtv_number, rtv_id, rtv_date_cancel,state, context=None):

        etoe_id = self.search(cr, uid, [('rtv_number', '=', rtv_number), ('rtv_id', '=', rtv_id)])

        # browse and then update
        for etoe in self.browse(cr, uid, etoe_id, context):

            self.write(cr, uid, etoe_id, {
                'rtv_date_cancel': str(rtv_date_cancel) if str(etoe.rtv_date_cancel) == 'False' else str(etoe.rtv_date_cancel) + "," + str(rtv_date_cancel),
                'state':state
            })

        return True

    def srn_cancel_update(self, cr, uid, rtv_number, rtv_id, srn_date_cancel,state, context=None):

        etoe_id = self.search(cr, uid, [('rtv_number', '=', rtv_number), ('rtv_id', '=', rtv_id)])

        # browse and then update
        for etoe in self.browse(cr, uid, etoe_id, context):

            self.write(cr, uid, etoe_id, {
                'srn_date_cancel': str(srn_date_cancel) if str(etoe.srn_date_cancel) == 'False' else str(etoe.srn_date_cancel) + "," + str(srn_date_cancel),
                'state':state
            })

        return True



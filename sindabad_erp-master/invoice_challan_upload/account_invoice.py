# Author Mufti Muntasir Ahmed 20/1/19

from openerp.osv import fields, osv, expression

class account_invoice(osv.osv):
    _inherit='account.invoice'

    def _get_full_partially_available(self, cursor, user, ids, name, arg, context=None):
        res = {}


        for inv_data in self.browse(cursor, user, ids, context=context):
            inv_name = inv_data.name
            attached = False

            attached_file_obj = self.pool.get("ir.attachment")
            attached_file = attached_file_obj.search(cursor, user, [('res_model', '=', str('account.invoice')),
                                                          ('res_id', '=', inv_data.id)
                                                          ])
            attached_file_obj_line = attached_file_obj.browse(cursor, user, attached_file, context=context)

            for sm in attached_file_obj_line:
                if 'INVSALJ' not in sm.name :
                    attached =True
                    break
                elif 'INVSALJ' in sm.name and len(sm.name) >19:
                    if '.pdf(' not in sm.name:
                        attached=True
                        break



            res[inv_data.id] = attached

        return res

    def _get_full_partially_available_search(self, cursor, user, obj, name, args, context=None):
        if not len(args):
            return []
        res = dict()

        for inv_data in self.browse(cursor, user, self.search(cursor, user, [])):
            res[inv_data.id] = False

            attached_file_obj = self.pool.get("ir.attachment")
            attached_file = attached_file_obj.search(cursor, user, [('res_model', '=', str('account.invoice')),
                                                                    ('res_id', '=', inv_data.id)
                                                                    ])
            attached_file_obj_line = attached_file_obj.browse(cursor, user, attached_file, context=context)

            for sm in attached_file_obj_line:
                if 'INVSALJ' not in sm.name:
                    res[inv_data.id] = True
                    break
                elif 'INVSALJ' in sm.name and len(sm.name) >19:
                    if '.pdf(' not in sm.name:
                        res[inv_data.id] = True
                        break




        args_str = eval(str(args[0]))
        arg_status = False
        if args_str[1] == '!=':
            arg_status = False
        else:
            arg_status = True

        filtered_ids = [('id', 'in', [item for item in res if res[item] == arg_status])]

        return filtered_ids

    _columns ={

        'challan_uploded': fields.function(_get_full_partially_available, string='Challan Uploaded',
                                                    type='boolean', fnct_search=_get_full_partially_available_search),
    }

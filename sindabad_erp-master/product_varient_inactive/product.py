# Author Mufti Muntasir Ahmed 22/04-2018

from openerp.osv import osv



class product_product(osv.osv):
    _name="product.product"
    _inherit="product.product"

    def action_inctive_product(self, cr, uid, ids, context=None):

        id_list = ids if ids else []
        cr.execute("update product_product set active=False where id=%s", (id_list))

        return id_list
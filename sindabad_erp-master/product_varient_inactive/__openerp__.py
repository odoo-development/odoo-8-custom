## Author Mufti Muntasir Ahmed 22-04-2018

{
    'name' : 'Product Active/ Inactive Options from varient',
    'version' : '1.0',
    'author' : 'Mufti Muntasir Ahemd',
    'category' : 'Product',
    'description' : """
    Product active/inactive from view and multi selection options
    
    """,
    'website': 'https://www.sindabad.com',

    'depends' : ['product'],
    'data': [

        'product_inactive_view.xml',



    ],

    'installable': True,
    'auto_install': False,
}

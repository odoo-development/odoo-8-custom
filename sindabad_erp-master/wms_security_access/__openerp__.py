{
    'name': "WMS Security Access Control",

    'summary': """
         WMS Security Access on Users.""",

    'description': """
        This Module Restricts the User from Accessing WMS Inbound and Outbound Process.
    """,

    'author': "Md Rockibul Alam Babu",

    'category': 'Users',

    'version': '0.1',

    'depends': ['base'],

    'data': [

        'users_wms_view.xml',
    ],
}
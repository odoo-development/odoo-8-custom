{
    'name': 'Restricts Picking',
    'version': '1.0',
    'author': 'Shahjalal',
    'summary': 'Inventory, Logistic, Storage',
    'description': 'Restricts picking lists print if not transferred',
    'depends': ['stock'],
    'installable': True,
    'application': True,
    'data': [
        'views/picking_list.xml',
    ],
    'auto_install': False,
}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:

from datetime import datetime, timedelta
import time
from datetime import date
from openerp import SUPERUSER_ID
from openerp.osv import fields, osv
from openerp.tools.translate import _
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT
import openerp.addons.decimal_precision as dp
from openerp import workflow
from openerp import api
import datetime
from time import gmtime, strftime
from openerp.exceptions import except_orm, Warning, RedirectWarning

class sale_order(osv.osv):
    _inherit = "sale.order"

    _columns = {
        'stn_created': fields.boolean('STN Created'),
    }

    def check_reserve(self, cr, uid, ids, context=None):

        # ids: sale order ids
        so_obj = self.pool.get('sale.order')
        so_name_list = list()
        for so in so_obj.browse(cr, uid, ids, context=context):
            so_name_list.append(str(so.name))

        sp_obj = self.pool.get('stock.picking')
        sp_list = sp_obj.search(cr, uid, [('origin', 'in', so_name_list)], context=context)

        for sp in sp_obj.browse(cr, uid, sp_list, context=context):

            if str(sp.state) in ['waiting', 'confirmed', 'partially_available']:
                sp_obj.rereserve_pick(cr, uid, [sp.id], context=context)

        return sp_list
    
    def stn_automation_start(self, cr, uid, ids, context=None):

        # select confirmed order products
        # nodda_quantity, uttwh_quantity

        uid=1
        compare_date_obj = datetime.datetime.now() - datetime.timedelta(days=10)
        compare_date = compare_date_obj.strftime('%Y-%m-%d')
        so_obj = self.pool.get('sale.order')
        so_list = so_obj.search(cr, uid, ['&', '&', ('state', '=', 'progress'), ('date_confirm', '>', compare_date), ('stn_created', '=', False)], context=context)

        sp_list = self.check_reserve(cr, uid, so_list, context=context)

        # product dict
        # state: [["draft","Draft"],["cancel","Cancelled"],["waiting","Waiting Another Operation"],["confirmed","Waiting Availability"],["partially_available","Partially Available"],["assigned","Ready to Transfer"],["done","Transferred"]]

        """
        products_dict = {
            'warehouse_code': {product_id: qty}
        }
        """
        products_dict = dict()
        product_list = list()
        exclude_wh = ['NODRT', 'UTTRT']

        sp_obj = self.pool.get('stock.picking')

        for sp in sp_obj.browse(cr, uid, sp_list, context=context):

            for move in sp.move_lines:
                
                if str(move.state) in ['partially_available', 'confirmed']:
                    # move.reserved_availability
                    # move.product_qty
                    prod_need = move.product_qty - move.reserved_availability

                    if prod_need > 0:
                        # take this product
                        # insert to products_dict
                        wh_code = str(move.picking_type_id.warehouse_id.code)

                        if wh_code not in exclude_wh:
                            product_id = int(move.product_id.id)

                            if product_id not in product_list:
                                product_list.append(product_id)

                            if products_dict.has_key(wh_code):
                                # add product_id and prod_need to that warehouse
                                
                                if products_dict[wh_code].has_key(product_id):
                                    products_dict[wh_code][product_id] =  products_dict[wh_code][product_id] + prod_need
                                else:
                                    products_dict[wh_code][product_id] = prod_need

                            else:
                                # add wh_code, product_id and prod_need
                                products_dict[wh_code] = dict()
                                products_dict[wh_code][product_id] = prod_need

        """
        products_dict = {'NODDA': {22006: 1.0}, 'UttWH': {64080: 3.0}}
        """

        uttara_need_prod = list()
        nodda_need_prod = list()

        product_obj = self.pool.get('product.product')

        stn_process_line_list = list()

        # process data for uttara
        uttara_stn_data = {
                'warehouse_id_from': 1,
                'warehouse_id_to': 2,
                'remark': 'System Generated',
            }

        for product in product_obj.browse(cr, uid, products_dict['UttWH'].keys(), context):

            if product.nodda_free_quantity > products_dict['UttWH'][product.id]:

                stn_process_line_list.append([0, False, {
                    'product_id': product.id,
                    'create_uid': uid,
                    'transfer_quantity': products_dict['UttWH'][product.id],
                    'available_qty': product.nodda_free_quantity,
                    'requested_qty': products_dict['UttWH'][product.id],
                    'state': 'draft'
                }])
            
                # product.uttwh_free_quantity
        
        uttara_stn_data['stn_process_line'] = stn_process_line_list

        stn_obj = self.pool.get('stn.process')
        
        stn_id = stn_obj.create(cr, uid, uttara_stn_data, context=context)

        # process data for nodda
        stn_process_line_list = list()

        nodda_stn_data = {
                'warehouse_id_from': 2,
                'warehouse_id_to': 1,
                'remark': 'System Generated',
            }
        
        for product in product_obj.browse(cr, uid, products_dict['NODDA'].keys(), context):

            if product.uttwh_free_quantity > products_dict['NODDA'][product.id]:
                stn_process_line_list.append([0, False, {
                    'product_id': product.id,
                    'create_uid': uid,
                    'transfer_quantity': products_dict['NODDA'][product.id],
                    'write_uid': uid,
                    'available_qty': product.nodda_free_quantity,
                    'requested_qty': products_dict['NODDA'][product.id],
                    'state': 'draft'
                }])

        nodda_stn_data['stn_process_line'] = stn_process_line_list

        stn_id = stn_obj.create(cr, uid, nodda_stn_data, context=context)

        # make True 'stn_created' after STN created
        # sos = so_obj.browse(cr, uid, so_list, context=context)



        return True

    
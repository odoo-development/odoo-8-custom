{
    'name': 'STN Process Automation',
    'version': '1.0',
    'category': 'WMS',
    'author': 'Shahjalal',
    'summary': 'STN Process Automation',
    'description': 'Stock Transfer Note Process for Warehouse Management System Automation',
    'depends': ['base', 'mail', 'stn_process', 'sale', 'stn_log', 'sales_collection_billing_process'],
    'data': [
        'stn_automation_start_so.xml'
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}

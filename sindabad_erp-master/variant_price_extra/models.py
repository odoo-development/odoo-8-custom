# -*- coding: utf-8 -*-
#################################################################################
#
#    Copyright (c) 2015-Present Webkul Software Pvt. Ltd. (<https://webkul.com/>)
#
#################################################################################

from openerp.osv import fields, osv
from openerp import tools
from openerp.tools.translate import _
import openerp.addons.decimal_precision as dp
from openerp.tools.float_utils import float_round, float_compare


class product_product(osv.osv):
	_inherit = 'product.product'

	def _get_price_extra(self, cr, uid, ids, field_names=None, arg=False, context=None):
		result = {}
		if not field_names:
			field_names = []
		for id in ids:
			result[id] = {}.fromkeys(field_names, 0.0)

		for product in self.browse(cr, uid, ids, context=context):
			price_extra = 0.0
			for variant_id in product.attribute_value_ids:
				for price_id in variant_id.price_ids:
					if price_id.product_tmpl_id.id == product.product_tmpl_id.id:
						price_extra += price_id.price_extra
			result[product.id]['price_extra'] = price_extra + product.wk_extra_price
			result[product.id]['attr_price_extra'] = price_extra
		return result

	
		
	_columns = {
		'wk_extra_price': fields.float('Price Extra'),
		'price_extra': fields.function(_get_price_extra, type='float', string='Variant Extra Price', help="This shows the sum of all attribute price and additional price of variant (All Attribute Price+Additional Variant price)", digits_compute=dp.get_precision('Product Price'), multi="evaluate_price"),
		'attr_price_extra': fields.function(_get_price_extra, type='float', string='Variant Extra Price', digits_compute=dp.get_precision('Product Price'), multi="evaluate_price"),
	}
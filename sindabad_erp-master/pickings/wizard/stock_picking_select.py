# Author mufti muntasir ahmed 17-04-2018

##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.osv import fields, osv
from openerp.tools.translate import _
from datetime import date, datetime


class sale_make_invoice(osv.osv_memory):
    _name = "picking.list.multiple"
    _description = "Picking List"
    _columns = {
        'picking_date': fields.datetime('Date', required=True, readonly=True, select=True, copy=False),
        'description': fields.char('Reference/Description'),
        'create_date': fields.datetime('Creation Date', readonly=True, select=True,
                                       help="Date on which sales order is created."),
        'date_confirm': fields.date('Confirmation Date', readonly=True, select=True,
                                    help="Date on which sales order is confirmed.", copy=False),
        'user_id': fields.many2one('res.users', 'Assigned to', select=True, track_visibility='onchange'),

        'picking_line': fields.one2many('picking.list.line', 'picking_id', 'Picking List Lines', required=True),

    }

    _defaults = {
        'picking_date': fields.datetime.now,
        'user_id': lambda obj, cr, uid, context: uid,

    }



    def make_picking(self, cr, uid, ids, context=None):
        picking_obj = self.pool.get('picking.list')

        if context is None:
            context = {}
        get_data = self.read(cr, uid, ids)[0]
        data ={}
        picking_line = []
        today_date = date.today()
        if len(context.get('active_ids')) > 20 :
            raise osv.except_osv(_('Message'), _("Max allowed challan is 20. Please select only 20 challan at a time."))


        data['description'] = get_data.get('description')

        for items in context.get('active_ids'):
            picking_line.append([0, False, {'stock_id': items, 'receive_date': today_date.strftime('%Y-%m-%d'), 'details': False, 'state': 'pending', 'return_date': False}])

        data['picking_line'] = picking_line

        save_the_data = picking_obj.create(cr, uid, data, context=context)

        # create picking_products and picking_products_line
        # save_the_data is the id of the picking_list created

        pl_obj = picking_obj.browse(cr, uid, [save_the_data], context=context)

        pp_data = {
            'picking': pl_obj.id,
            'state': 'pending',
        }

        pp_line_list = list()

        """
        product_dict = {"01234": 10}
        """
        product_dict = dict()
        for pick_line in pl_obj.picking_line:

            # 'stock_id': fields.many2one('stock.picking', string='Stock Picking')
            # if the product_dict does not contains product then add with 0 quantity
            for product in pick_line.stock_id.move_lines:
                product_id = product.product_id.id

                if product_dict.has_key(str(product_id)):
                    product_dict[str(product_id)] = float(product.product_qty) + product_dict[str(product_id)]
                else:
                    product_dict[str(product_id)] = float(product.product_qty)

        # create picking_product_line
        product_obj = self.pool.get('product.product')
        for key, value in product_dict.iteritems():
            if value !=0.00:
                prod = product_obj.browse(cr, uid, [int(key)], context=context)
                pp_line_list.append([0, False,
                                      {
                                          'picking_id': save_the_data,
                                          'product_id': int(key),
                                          'product': str(prod.product_name),
                                          'product_ean': str(prod.ean13),
                                          'total_challan_qty': value ,
                                          'picking_qty': value,
                                          'state': 'pending',
                                      }])

            pp_data['pick_prod_line'] = pp_line_list
            if pp_line_list:
                pick_pro_env = self.pool.get('picking.products')
                saved_pick_pro_id = pick_pro_env.create(cr, uid, pp_data, context=context)


        return save_the_data


# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:

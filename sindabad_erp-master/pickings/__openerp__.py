# Author Mufti Muntasir Ahmed 11/04/2018


{
    'name': 'Picking List Generation',
    'version': '8.0.0',
    'category': 'Sales',
    'description': """
Generate Picking List from Delivery Challan / Sales Order   
==============================================================

""",
    'author': 'Mufti Muntasir Ahmed',
    'depends': ['stock', 'packings', 'product_simple_sync'],
    'data': [

        'wizard/stock_picking_select_view.xml',
        'security/picking_security.xml',
        'security/ir.model.access.csv',
        'picking_view.xml',
        'views/report_picking.xml',
        'picking.xml'



    ],

    'installable': True,
    'auto_install': False,
}
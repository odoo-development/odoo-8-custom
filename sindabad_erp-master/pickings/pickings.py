# Author Mufti Muntasir Ahmed 10-04-2018


from openerp.osv import osv, fields
from openerp import SUPERUSER_ID, api
from openerp.tools.translate import _
from datetime import datetime



class stock_picking(osv.osv):
    _inherit = 'stock.picking'

    _columns = {
        'picked': fields.boolean('Picked'),

    }


class PickingList(osv.osv):
    _name = 'picking.list'
    _description = "Picking List"

    _columns = {
        'name': fields.char('Order Reference', required=True, copy=False,
                            readonly=True, states={'draft': [('readonly', False)], 'sent': [('readonly', False)]},
                            select=True),
        'picking_date': fields.datetime('Date', required=True, readonly=True, select=True, copy=False),
        'description': fields.char('Picker Name'),
        'create_date': fields.datetime('Creation Date', readonly=True, select=True,
                                       help="Date on which sales order is created."),
        'date_confirm': fields.datetime('Confirmation Date', readonly=True, select=True,
                                    help="Date on which sales order is confirmed.", copy=False),
        'user_id': fields.many2one('res.users', 'Assigned to', select=True, track_visibility='onchange'),

        'picking_line': fields.one2many('picking.list.line', 'picking_id', 'Picking List Lines', required=True),
        'picking_products_line': fields.one2many('picking.products.line', 'picking_id', 'Picking Products Line', required=False),
        'product_scan': fields.char('Product Scan'),
        'state': fields.selection([
            ('pending', 'Waiting for Picking'),
            ('done', 'Done'),
            ('cancel', 'Cancelled'),

        ], 'Status', readonly=True, copy=False, help="Gives the status of the quotation or sales order", select=True),

    }

    _defaults = {
        'picking_date': fields.datetime.now,
        'user_id': lambda obj, cr, uid, context: uid,
        'state': 'pending',
        'name': lambda obj, cr, uid, context: '/',

    }

    _order = 'id desc'

    def make_confirm(self, cr, uid, ids, context=None):

        id_list = ids
        stock_picking_ids = []

        for element in id_list:
            ids = [element]

            if ids is not None:
                cr.execute("update picking_list set state='done' where id=%s", (ids))
                cr.execute("update picking_list_line set state='done' where picking_id=%s", (ids))

            cr.execute("select stock_id from  picking_list_line where picking_id=%s", (ids))

            for item in cr.fetchall():
                for tuple_items in list(item):
                    stock_picking_ids.append(tuple_items)
        for items in stock_picking_ids:
            cr.execute("update stock_picking set picked='True' where id=%s", ([items]))

        for single_id in id_list:
            confirm_date_update_query = "UPDATE picking_list SET date_confirm='{0}' WHERE id={1}".format(str(fields.datetime.now()), single_id)
            cr.execute(confirm_date_update_query)
            cr.commit()

        return 'True'

    def make_cancel(self, cr, uid, ids, context=None):
        id_list = ids
        stock_picking_ids = []
        for element in id_list:
            ids = [element]

            if ids is not None:
                cr.execute("update picking_list set state='cancel' where id=%s", (ids))
                cr.execute("update picking_list_line set state='cancel' where picking_id=%s", (ids))

            cr.execute("select stock_id from  picking_list_line where picking_id=%s", (ids))


            for item in cr.fetchall():
                for tuple_items in list(item):
                    stock_picking_ids.append(tuple_items)

        for items in stock_picking_ids:
            cr.execute("update stock_picking set picked='False' where id=%s", ([items]))

        return 'True'


    ## Stock Transfer Code is here

    def stock_transfer_from_picking(self,cr,uid,ids,stock_list=[],context=None):
        for picking_id in stock_list:
            picking = [picking_id[0]]
            order_id = str(picking_id[1])
            stock_picking_id=picking_id[0]

            stock_picking_obj = self.pool.get('stock.picking').browse(cr, uid, [stock_picking_id], context=context)[0]


            if str(stock_picking_obj.state) not in ('done','cancel'):

                stock_picking = self.pool.get('stock.picking').do_enter_transfer_details(cr, uid, [stock_picking_id],
                                                                                     context=context)

                trans_obj = self.pool.get('stock.transfer_details')
                trans_search = trans_obj.search(cr, uid, [('picking_id', '=', stock_picking_id)], context=context)

                trans_search = [trans_search[len(trans_search) - 1]] if len(trans_search) > 1 else trans_search

                trans_browse = self.pool.get('stock.transfer_details').browse(cr, uid, trans_search, context=context)


                trans_browse.do_detailed_transfer()

                # cr.execute("update stock_picking set x_stock_done='True' where id=%s", ([stock_picking_id]))
                cr.commit()


        return True


    ## Ends the Stock transfer Code



    def check_product_has_warranty_or_not(self,cr,uid,stock_id=None,context=None):
        uid=1
        #validating Product Warranty serials
        found_warranty_applicable=False
        if stock_id is not None:
            cr.execute("select product_product.warranty_applicable from stock_move,product_product where stock_move.product_id=product_product.id and  stock_move.picking_id=%s",([stock_id]))
            for w_info in cr.fetchall():
                if w_info[0] == True:
                    found_warranty_applicable=True
                    break



        return found_warranty_applicable


    def make_pack(self, cr, uid, ids, context=None):
        id_list = ids
        stock_picking_ids = []

        for element in id_list:
            ids = [element]

            pack_data = {
                'description': 'System Generated',
                'picking_id': element,

            }

            # cr.execute("select stock_id from  picking_list_line where picking_id=%s", (ids))
            #
            # for item in cr.fetchall():
            #     for tuple_items in list(item):
            #         stock_picking_ids.append(tuple_items)

            packing_line_list=[]

                # cr.execute("update stock_picking set picked='False' where id=%s", ([items]))
            try:
                stock_list = []
                for record in self.browse(cr, uid, ids, context=context):
                    for line_items in record.picking_line:
                        if line_items.stock_id.state!='cancel':
                            stock_list.append((line_items.stock_id.id, line_items.magento_no))
                            stock_picking_ids.append(line_items.stock_id.id)

                self.stock_transfer_from_picking(cr,uid=1,ids=[],stock_list=stock_list,context=None)

            except:
                raise osv.except_osv(_('Message'), _(" Process Time Out.. Please Refresh your browser and try again."))

            for items in stock_picking_ids:
                packing_line_list.append(
                    [0,False,{'stock_id':items,'magento_no': False, 'details': False,
                                                   'receive_date': datetime.now(), 'order_number': False, 'return_date': False}])

            pack_data['packing_line']=packing_line_list



            if stock_picking_ids:
                packing_pool = self.pool.get('packing.list')
                saved_id = packing_pool.create(cr, uid, pack_data, context=None)

        return 'True'



    def create(self, cr, uid, data, context=None):
        context = dict(context or {})

        stock_picking_list=[]
        if data.get('picking_line'):
            picking_line=[]
            for items in data.get('picking_line'):
                att_data={}
                for att_data in items:
                    if type(att_data) is dict:
                        stock_id = att_data.get('stock_id')
                        stock_picking_list.append(stock_id)
                        stock_obj = self.pool.get('stock.picking').browse(cr, uid, stock_id, context=context)

                        att_data['magento_no']=stock_obj.mag_no
                        att_data['order_number']=stock_obj.origin
                picking_line.append([0,False,att_data])


            data['picking_line']=picking_line

        if stock_picking_list:
            abc=[]
            for items in stock_picking_list:
                cr.execute("select picking_id from  picking_list_line where state !='cancel' and stock_id=%s", ([items]))

                abc += cr.fetchall()

            if len(abc)>0:
                raise osv.except_osv(_('Message'), _("Already Pickied the numbers"))

        picking_id = super(PickingList, self).create(cr, uid, data, context=context)


        if picking_id:
            for items in data['picking_line']:
                for att in items:
                    if isinstance(att, dict):
                        cr.execute("update stock_picking set picked='True' where id=%s", (att.get('stock_id'),))

        return picking_id

    @api.onchange('product_scan')
    def picking_products_scan(self):

        scanned_ean = str(self.product_scan)

        if self.picking_products_line and scanned_ean != 'False':
            first_picking_products_line_id = self.picking_products_line[0].id
            picking_id_query = "SELECT picking_id FROM picking_products_line WHERE id={0}".format(first_picking_products_line_id)
            self._cr.execute(picking_id_query)
            picking_id = self._cr.fetchone()[0]

            picking_list = self.pool.get('picking.list').browse(self._cr, self._uid, [picking_id], context=self._context)

            # picking_products_line
            qty_update = False
            # for product in picking_list.picking_products_line:
            for product in self.picking_products_line:

                if product.product_ean == scanned_ean and product.total_challan_qty > product.picking_qty:
                    product.picking_qty += 1
                    qty_update = True
                    break

            if not qty_update:
                # give waring msg
                # EAN dose not exists with the product
                pass

        self.product_scan = ""

        return "xXxXxXxXxX"

    @api.multi
    def write(self, vals):

        # only update only if (picking_qty is less or equal to total_challan_qty)
        update_permission = list()

        if vals.has_key('picking_products_line'):
            for single_line in vals['picking_products_line']:
                if not not single_line[2]:
                    for picking_products_l in self.picking_products_line:
                        if picking_products_l.id == single_line[1]:
                            picking_qty = single_line[2]['picking_qty'] if single_line[2].has_key(
                                'picking_qty') else picking_products_l.picking_qty

                            if picking_qty > picking_products_l.picking_qty:
                                update_permission.append(False)
                            else:
                                update_permission.append(True)

        if False in update_permission:

            raise osv.except_osv(_('Picking line adjustment ERROR!'), _('Picking quantity should be less then or equal to (total challan quantity)!!!'))

        else:
            record = super(PickingList, self).write(vals)

            return record




class PickingListLine(osv.osv):
    _name = 'picking.list.line'
    _description = "Picking Line List"

    _columns = {
        'picking_id': fields.many2one('picking.list', 'Order Reference', required=True, ondelete='cascade', select=True,
                                      readonly=True),

        'stock_id': fields.many2one('stock.picking', string='Stock Picking'),
        'sequence': fields.integer('Sequence',
                                   help="Gives the sequence order when displaying a list of sales order lines."),
        'details': fields.char('Details'),
        'receive_date': fields.date('Receive Date', required=True),
        'return_date': fields.date('Return Date'),
        'order_number': fields.char('Order Number'),
        'magento_no': fields.char('Magento No'),
        'state': fields.selection([
            ('pending', 'Waiting for Picking'),
            ('done', 'Done'),
            ('cancel', 'Cancelled'),

        ], 'Status', readonly=True, copy=False, help="Gives the status of the quotation or sales order", select=True),
    }

    _defaults = {
        'receive_date': fields.datetime.now,
        'state': 'pending',

    }


class picking_products(osv.osv):
    _name = 'picking.products'
    _description = "Picking Products"

    _columns = {
        'picking':  fields.many2one('picking.list', 'Order Reference'),
        'state': fields.selection([
            ('pending', 'Waiting for Picking'),
            ('done', 'Done'),
            ('cancel', 'Cancelled'),

        ], 'Status', readonly=True, copy=False, help="Gives the status of the quotation or sales order", select=True),
        'pick_prod_line': fields.one2many('picking.products.line', 'pick_prod_id', 'Picking Products Lines', required=True),
    }


class picking_products_line(osv.osv):
    _name = 'picking.products.line'
    _description = "Picking Products Line"

    _columns = {
        'pick_prod_id': fields.many2one('picking.products', 'Picking products', required=True, ondelete='cascade', select=True, readonly=True),
        'picking_id': fields.many2one('picking.list', 'Picking ID', required=False,
                                 ondelete='cascade', select=True, readonly=True),
        'product_id': fields.many2one('product.product', 'Product', readonly=True),
        'product': fields.char('Product Name', readonly=True),
        'product_ean': fields.char('Product EAN', readonly=True),
        'total_challan_qty': fields.float('Total Challan Qty', readonly=True),
        'picking_qty': fields.float('Picking Qty'),
        'return_date': fields.date('Return Date'),
        'state': fields.selection([
            ('pending', 'Waiting for Picking'),
            ('done', 'Done'),
            ('cancel', 'Cancelled'),

        ], 'Status', readonly=True, copy=False, help="Gives the status of the quotation or sales order", select=True),

    }

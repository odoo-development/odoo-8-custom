# Author Mufti Muntasir Ahmed 25-04-2018


import xlwt
from datetime import datetime
from openerp.osv import orm
from openerp.report import report_sxw
from openerp.addons.report_xls.report_xls import report_xls
from openerp.addons.report_xls.utils import rowcol_to_cell, _render
from openerp.tools.translate import translate, _
import logging
_logger = logging.getLogger(__name__)


_ir_translation_name = 'sale.order.line.xls'


class sale_order_line_xls_parser(report_sxw.rml_parse):

    def __init__(self, cr, uid, name, context):
        super(sale_order_line_xls_parser, self).__init__(
            cr, uid, name, context=context)
        move_obj = self.pool.get('sale.order')
        self.context = context
        wanted_list = move_obj._report_xls_fields(cr, uid, context)
        template_changes = move_obj._report_xls_template(cr, uid, context)
        self.localcontext.update({
            'datetime': datetime,
            'wanted_list': wanted_list,
            'template_changes': template_changes,
            '_': self._,
        })

    def _(self, src):
        lang = self.context.get('lang', 'en_US')
        return translate(self.cr, _ir_translation_name, 'report', lang, src) \
            or src


class sale_order_line_xls(report_xls):

    def __init__(self, name, table, rml=False, parser=False, header=True,
                 store=False):
        super(sale_order_line_xls, self).__init__(
            name, table, rml, parser, header, store)

        # Cell Styles
        _xs = self.xls_styles
        # header
        rh_cell_format = _xs['bold'] + _xs['fill'] + _xs['borders_all']
        self.rh_cell_style = xlwt.easyxf(rh_cell_format)
        self.rh_cell_style_center = xlwt.easyxf(rh_cell_format + _xs['center'])
        self.rh_cell_style_right = xlwt.easyxf(rh_cell_format + _xs['right'])
        # lines
        aml_cell_format = _xs['borders_all']
        self.aml_cell_style = xlwt.easyxf(aml_cell_format)
        self.aml_cell_style_center = xlwt.easyxf(
            aml_cell_format + _xs['center'])
        self.aml_cell_style_date = xlwt.easyxf(
            aml_cell_format + _xs['left'],
            num_format_str=report_xls.date_format)
        self.aml_cell_style_decimal = xlwt.easyxf(
            aml_cell_format + _xs['right'],
            num_format_str=report_xls.decimal_format)
        # totals
        rt_cell_format = _xs['bold'] + _xs['fill'] + _xs['borders_all']
        self.rt_cell_style = xlwt.easyxf(rt_cell_format)
        self.rt_cell_style_right = xlwt.easyxf(rt_cell_format + _xs['right'])
        self.rt_cell_style_decimal = xlwt.easyxf(
            rt_cell_format + _xs['right'],
            num_format_str=report_xls.decimal_format)

        # XLS Template
        self.col_specs_template = {
            'name': {
                'header': [1, 20, 'text', _render("_('Order Number')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},
            'client_order_ref': {
                'header': [1, 20, 'text', _render("_('Magneto Number')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},
            'status': {
                'header': [1, 20, 'text', _render("_('Status')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},
            'order_date': {
                'header': [1, 20, 'text', _render("_('Order Date')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},
            # 'date_confirm': {
            #     'header': [1, 20, 'text', _render("_('SO Confirmation Date')")],
            #     'lines': [1, 0, 'text', _render("''")],
            #     'totals': [1, 0, 'text', None]},
            'order_confirmation_date': {
                'header': [1, 20, 'text', _render("_('SO Confirmation Date')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},
            'magento_delivery_at': {
                'header': [1, 20, 'text', _render("_('Expected Shipment Date')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},
            'customer': {
                'header': [1, 20, 'text', _render("_('Customer')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},
            'company': {
                'header': [1, 20, 'text', _render("_('Company')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},
            'customer_mail': {
                'header': [1, 20, 'text', _render("_('Customer Email')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},
            'customer_classification': {
                'header': [1, 20, 'text', _render("_('Customer Classification')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},
            'warehouse_name': {
                'header': [1, 20, 'text', _render("_('Warehouse')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},
            'product_name': {
                'header': [1, 20, 'text', _render("_('Product Name')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},
            'default_code': {
                'header': [1, 20, 'text', _render("_('SKU')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},
            'product_category': {
                'header': [1, 20, 'text', _render("_('Product Category')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},
            'product_uom_qty': {
                'header': [1, 20, 'text', _render("_('Order QTY')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},
            'so_reserved_qty': {
                'header': [1, 20, 'text', _render("_('Reserved QTY')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},
            'price_unit': {
                'header': [1, 20, 'text', _render("_('Order Unit Price')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},
            'total_unit_price': {
                'header': [1, 20, 'text', _render("_('Total Price')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},
            'po_number': {
                'header': [1, 20, 'text', _render("_('PO Number')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},
            'po_create_date': {
                'header': [1, 20, 'text', _render("_('PO Create Date')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},
            'po_confirm_date': {
                'header': [1, 20, 'text', _render("_('PO Confirm Date')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},
            'po_qty': {
                'header': [1, 20, 'text', _render("_('Purchase Qty')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},
            'po_unit_price': {
                'header': [1, 20, 'text', _render("_('PO Unit Price')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},
            'po_total_price': {
                'header': [1, 20, 'text', _render("_('PO Total Price')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},
            'po_receive_qty': {
                'header': [1, 20, 'text', _render("_('PO Receive QTY')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},
            'po_receive_date': {
                'header': [1, 20, 'text', _render("_('PO Receive Date')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'po_supplier_name': {
                'header': [1, 20, 'text', _render("_('PO Supplier Name')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},
            'so_delivery_date': {
                'header': [1, 20, 'text', _render("_('SO Delivery Date')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},
            'so_reserved_date': {
                'header': [1, 20, 'text', _render("_('Reserved Date')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},
            'so_delivery_qty': {
                'header': [1, 20, 'text', _render("_('SO Total Delivery QTY')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},
            'so_remaining_qty': {
                'header': [1, 20, 'text', _render("_('SO Remaining/Pending QTY')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},
            'so_return_qty': {
                'header': [1, 20, 'text', _render("_('SO Return QTY')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},
            'so_return_date': {
                'header': [1, 20, 'text', _render("_('SO Return Date')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

        }

    def _get_invoice_data(self, cr, sale_order_line, uid, stock_picking_pool, context):
        invoice_date = ''
        invoice_confirmation_date = ''
        invoice_number = ''

        stock_move_query = "SELECT picking_id FROM stock_move WHERE origin='{0}' AND product_id='{1}'".format(str(sale_order_line.order_id.name), sale_order_line.product_id.id)
        cr.execute(stock_move_query)
        picking_id_list = list()
        for picking_id_info in cr.fetchall():
            picking_id_list.append(picking_id_info[0])

        stock_picking_data = stock_picking_pool.browse(cr, uid, picking_id_list, context=context)

        stock_picking_name_list = list()
        for st_pic_da in stock_picking_data:
            stock_picking_name_list.append(str(st_pic_da.name))

        for stock_pick_name in stock_picking_name_list:
            try:
                name_split_list = stock_pick_name.split("\\")

                if len(name_split_list) == 1:
                    name_split_list = stock_pick_name.split("/")

                if len(name_split_list) == 3:

                    account_invoice_query = "SELECT date_invoice, date_due, number FROM account_invoice WHERE origin LIKE '{0}%' AND origin LIKE '%{1}%' AND origin LIKE '%{2}' ".format(name_split_list[0], name_split_list[1], name_split_list[2])
                    cr.execute(account_invoice_query)
                    invoice_date_inf = ''
                    invoice_conf_date_inf = ''
                    invoice_number_inf = ''
                    for acc_inv_info in cr.fetchall():
                        invoice_date_inf = str(acc_inv_info[0])
                        invoice_conf_date_inf = str(acc_inv_info[1])
                        invoice_number_inf = str(acc_inv_info[2])

                    invoice_date += invoice_date_inf + ', ' if invoice_date_inf != 'None' and invoice_date_inf != '' else ''
                    invoice_confirmation_date += invoice_conf_date_inf + ', ' if invoice_conf_date_inf != 'None' and invoice_conf_date_inf != '' else ''
                    invoice_number += invoice_number_inf + ', ' if invoice_number_inf != 'None' and invoice_number_inf != '' else ''
            except:
                pass

        return invoice_date, invoice_confirmation_date, invoice_number

    def _get_order_confirm_time(self, cr, uid, model, record_name, context):

        order_confirm_time = ''

        if len(model.split('.')) == 2:

            try:

                order_con_time_query = "SELECT create_date FROM mail_message WHERE model='{0}' AND record_name='{1}' AND body LIKE '%Quotation confirmed%'".format(
                    model, record_name)
                cr.execute(order_con_time_query)
                for con_time in cr.fetchall():
                    order_confirm_time = str(con_time[0])

                order_confirm_time = order_confirm_time.split(' ')[1] if order_confirm_time else ''
            except:
                order_confirm_time = ''

        return order_confirm_time

    def _get_order_reserved_date(self, cr, uid, picking_id, context):
        reserved_date = ''
        stock_move_details_obj = self.pool.get("stock.move.details")
        init_data = stock_move_details_obj.search(cr, uid, [('picking_id', '=', picking_id)], context=context)
        if len(init_data) > 0:
            reserved_date = stock_move_details_obj.browse(cr, uid, init_data[0], context=context).reserved_date

        return reserved_date

    def generate_xls_report(self, _p, _xs, data, objects, wb):

        wanted_list = _p.wanted_list
        self.col_specs_template.update(_p.template_changes)
        _ = _p._

        context = self.context
        st_date = data.get('form').get('date_from')
        category = data.get('form').get('category')
        wh_id = data.get('form').get('warehouse_id')
        end_date = data.get('form').get('date_to')
        context['start_date'] = st_date
        context['end_date'] = end_date
        self.context = context
        date_range = _("Date: %s to %s" % (st_date, end_date))

        # report_name = objects[0]._description or objects[0]._name
        report_name = _("Order Process End to End")
        ws = wb.add_sheet(report_name[:31])
        ws.panes_frozen = True
        ws.remove_splits = True
        ws.portrait = 0  # Landscape
        ws.fit_width_to_pages = 1
        row_pos = 0

        # set print header/footer
        ws.header_str = self.xls_headers['standard']
        ws.footer_str = self.xls_footers['standard']

        # Title
        cell_style = xlwt.easyxf(_xs['xls_title'])
        c_specs = [
            ('report_name', 4, 3, 'text', report_name),
        ]
        row_data = self.xls_row_template(c_specs, ['report_name'])
        row_pos = self.xls_write_row(
            ws, row_pos, row_data, row_style=cell_style)
        row_pos += 1

        # Date Ranges
        cell_style = xlwt.easyxf(_xs['xls_title'])
        c_specs = [
            ('date_range', 4, 3, 'text', date_range),
        ]
        row_data = self.xls_row_template(c_specs, ['date_range'])
        row_pos = self.xls_write_row(
            ws, row_pos, row_data, row_style=cell_style)
        row_pos += 1

        # Column headers
        c_specs = map(lambda x: self.render(
            x, self.col_specs_template, 'header', render_space={'_': _p._}),
            wanted_list)
        row_data = self.xls_row_template(c_specs, [x[0] for x in c_specs])
        row_pos = self.xls_write_row(
            ws, row_pos, row_data, row_style=self.rh_cell_style,
            set_column_size=True)
        ws.set_horz_split_pos(row_pos)


        ## New Codes are starting from here
        # order_id = [16698]

        procurement_group_ids = []
        line_obj = self.pool.get('sale.order')



        incoming_picking_id_list =[]

        incoming_picking_query = "select id from stock_picking_type where code='incoming'"
        self.cr.execute(incoming_picking_query)
        incoming_picking_query_objects = self.cr.dictfetchall()

        incoming_picking_id_list =[p_item.get('id') for p_item in incoming_picking_query_objects]

        order_ids=[]
        # domain = [('state', '!=', 'done'),('date_confirm', '>=', st_date),('date_confirm', '<=', end_date)]

        uid = 1
        # order_ids = line_obj.search(self.cr, uid, domain, context=context)
        added_wh_id=False
        if wh_id is not False:
            wh_id = wh_id[0]
            added_wh_id=True

        if category == 2:
            conditions = [('state', 'not in', ['done','cancel']), ('waiting_availability', '=', True), ('date_order','<=',end_date), ('date_order','>=',st_date)]
            if added_wh_id is True:
                conditions.append(('warehouse_id','=',wh_id))
            sale_order_obj = self.pool.get('sale.order')
            order_ids = sale_order_obj.search(self.cr, uid,conditions , context=context)

        elif category == 3:
            conditions = [('state', '=', 'done'),('date_order', '<=', end_date),('date_order', '>=', st_date)]
            if added_wh_id is True:
                conditions.append(('warehouse_id','=',wh_id))
            sale_order_obj = self.pool.get('sale.order')
            order_ids = sale_order_obj.search(self.cr, uid, conditions, context=context)

        elif category == 4:
            conditions = [('state', 'in', ['progress','shipping_except','invoice_except']),('date_order', '<=', end_date),('date_order', '>=', st_date)]
            if added_wh_id is True:
                conditions.append(('warehouse_id','=',wh_id))
            sale_order_obj = self.pool.get('sale.order')
            order_ids = sale_order_obj.search(self.cr, uid, conditions, context=context)
        else:

            order_id_squ = "select id from sale_order where (state='done' or state='progress' or state='manual' or state='shipping_except' or state='invoice_except') and date_confirm <=%s and date_confirm >=%s"
            order_ex = self.cr.execute(order_id_squ,(end_date,st_date))
            order_ex = self.cr.dictfetchall()

            order_ids = [o_items.get('id') for o_items in order_ex]

        # Following query for sale line detailed with product name

        # order_query="select sale_order_line.id as sale_line_id,sale_order.name as so_name, sale_order.client_order_ref,sale_order.state,sale_order.date_order,sale_order.date_confirm,sale_order.magetno_delivery_at,sale_order_line.product_id, sale_order_line.name,sale_order.warehouse_id,sale_order_line.product_uom_qty,sale_order_line.price_unit,sale_order_line.qty_delivered, sale_order_line.qty_return,sale_order.procurement_group_id,sale_order.partner_id,(select stock_warehouse.name from stock_warehouse where stock_warehouse.id =  sale_order.warehouse_id) as wh_name,sale_order.id as sale_id, (select name_template from product_product where id=sale_order_line.product_id) as product_name ,(select default_code from product_product where id=sale_order_line.product_id) as sku,(select product_category.name from product_product,product_template,product_category where product_product.id=sale_order_line.product_id and product_product.product_tmpl_id = product_template.id and product_template.categ_id = product_category.id) as product_categ from sale_order, sale_order_line where sale_order.id=sale_order_line.order_id and sale_order.id in %s"

        body1='%Quotation confirmed%'

        order_query="SELECT sale_order_line.id AS sale_line_id,sale_order.name AS so_name,sale_order.client_order_ref,sale_order.state, sale_order.date_order,sale_order.date_confirm,sale_order.magetno_delivery_at,sale_order_line.product_id,sale_order_line.name,sale_order.warehouse_id,sale_order_line.product_uom_qty,sale_order_line.price_unit,sale_order.date_confirm AS order_confirmation_date,sale_order_line.qty_delivered,sale_order_line.qty_return,sale_order.procurement_group_id,sale_order.partner_id,(SELECT stock_warehouse.name FROM stock_warehouse WHERE stock_warehouse.id = sale_order.warehouse_id)    AS wh_name,sale_order.id AS sale_id,(SELECT name_template FROM product_product WHERE id = sale_order_line.product_id) AS product_name,(SELECT default_code FROM product_product WHERE id = sale_order_line.product_id) AS sku,(SELECT product_category.name FROM product_product, product_template, product_category WHERE product_product.id = sale_order_line.product_id AND product_product.product_tmpl_id = product_template.id AND product_template.categ_id = product_category.id) AS product_categ FROM sale_order, sale_order_line WHERE sale_order.id = sale_order_line.order_id AND sale_order.id IN %s"

        self.cr.execute(order_query,(tuple(order_ids),))
        order_objs = self.cr.dictfetchall()



        ### Get all partner Name and others info

        partner_list = []
        final_company_dict={}
        partner_obj = self.pool("res.partner")
        for items in order_objs:
            partner_list.append(items.get('partner_id'))
        partner_list = list(dict.fromkeys(partner_list))
        p_obj = partner_obj.browse(self.cr, uid, partner_list, context=context)

        for p_items in p_obj:
            if p_items.parent_id:
                final_company_dict[p_items.id] = {
                    'customer_name': p_items.name,
                    'company_name': p_items.parent_id.name,
                    'company_code': p_items.parent_code,
                    'classification': p_items.x_classification,
                    'email': p_items.email,
                }
            else:
                final_company_dict[p_items.id] = {
                    'customer_name': p_items.name,
                    'company_name': p_items.parent_id.name,
                    'classification': p_items.parent_id.x_classification if p_items.parent_id.x_classification else p_items.x_classification,
                    'email': p_items.parent_id.email,
                    'company_code': p_items.parent_id.parent_code,
                }




        ## ENds Here



        # order_ids = [18917]
        ## Get reseved date, and qty sale_line wise

        stock_picking_query = "select sale_order_line.id as sales_line_id ,stock_picking.date_done,stock_move.picking_type_id,(select reserved_date from stock_move_details where stock_move_details.picking_id = stock_picking.id ORDER BY stock_move_details.id DESC LIMIT 1) as reserve_date,(select sum(stock_quant.qty) from stock_quant where stock_quant.reservation_id=stock_move.id group by stock_quant.reservation_id) as total_reserved  from stock_picking, stock_move,sale_order,sale_order_line  where stock_picking.id =stock_move.picking_id and stock_picking.group_id =sale_order.procurement_group_id and stock_move.product_id =sale_order_line.product_id and sale_order.id in %s and sale_order.id = sale_order_line.order_id"

        self.cr.execute(stock_picking_query, (tuple(order_ids),))
        stock_picking_objs = self.cr.dictfetchall()

        stock_picking_dict ={}
        tmp_dict={
            'so_reserved_date':'',
            'so_delivery_date':'',
            'so_return_date':'',
            'reserved_qty':''
        }

        for st_pick in stock_picking_objs:

            if stock_picking_dict.has_key(st_pick.get('sales_line_id')):
                tmp_dict = stock_picking_dict.get(st_pick.get('sales_line_id'))
                # tmp_dict['so_reserved_date'] = tmp_dict['so_reserved_date'] + str(st_pick.get('reserve_date'))
                # tmp_dict['so_delivery_date'] = tmp_dict['so_delivery_date'] + str(st_pick.get('date_done'))
                # tmp_dict['so_return_date'] = tmp_dict['so_return_date'] + str(st_pick.get('so_return_date'))
                if len(str(st_pick.get('reserve_date'))) >4 :
                    tmp_dict['so_reserved_date'] = str(st_pick.get('reserve_date'))
                if len(str(st_pick.get('date_done'))) > 4:
                    tmp_dict['so_delivery_date'] = str(st_pick.get('date_done'))
                if len(str(st_pick.get('so_return_date'))) > 4:
                    tmp_dict['so_return_date'] = str(st_pick.get('so_return_date'))

                tmp_dict['reserved_qty'] =str(st_pick.get('total_reserved'))
                stock_picking_dict[st_pick.get('sales_line_id')]=tmp_dict
            else:
                stock_picking_dict[st_pick.get('sales_line_id')]={
                    'so_reserved_date':  str(st_pick.get('reserve_date')),
                    'so_delivery_date': str(st_pick.get('date_done')) ,
                    'so_return_date': str(st_pick.get('so_return_date')) ,
                    'reserved_qty': str(st_pick.get('total_reserved'))
                }




        ## Ends here


        ## Back to back Po information  for Sales order

        po_query = "select purchase_order.id as po_id,purchase_order.name,purchase_order.create_date,purchase_order.date_approve,purchase_order_line.product_qty,purchase_order_line.price_unit,purchase_order.partner_id,purchase_order_line.product_id, (purchase_order_line.price_unit *purchase_order_line.product_qty ) as sub_total, (select res_partner.name from res_partner where res_partner.id=purchase_order.partner_id) as partner_name ,sale_order_line.id as sale_line_id, (SELECT sum(m.product_uom_qty) FROM stock_picking p, stock_move m, purchase_order_line pol, purchase_order po WHERE po.id =purchase_order.id and po.id = pol.order_id and pol.id = m.purchase_line_id and m.picking_id = p.id and m.product_id=purchase_order_line.product_id and p.state='done' group by m.product_id) as total_recv_qty, (SELECT pk.date_done FROM stock_picking pk, stock_move mk, purchase_order_line polk, purchase_order pok WHERE pok.id =purchase_order.id and pok.id = polk.order_id and polk.id = mk.purchase_line_id and mk.picking_id = pk.id and mk.product_id=purchase_order_line.product_id and pk.state='done' ORDER BY pok.id DESC LIMIT 1) as last_recv_date from purchase_order,purchase_order_line,sale_order,sale_order_line where purchase_order.id = purchase_order_line.order_id and  purchase_order_line.sale_order_id =sale_order.id and purchase_order_line.product_id=sale_order_line.product_id and sale_order.id = sale_order_line.order_id and  sale_order.id in %s"

        self.cr.execute(po_query, (tuple(order_ids),))
        po_query_objs = self.cr.dictfetchall()

        po_query_dict = {}


        for po_pick in po_query_objs:
            if po_query_dict.has_key(po_pick.get('sale_line_id')):
                pass
            else:
                tmp_dict = {
                    'po_number':  str(po_pick.get('name')),
                    'po_create_date': str(po_pick.get('create_date')),
                    'po_confirm_date': str(po_pick.get('date_approve')),
                    'po_qty': str(po_pick.get('product_qty')),
                    'po_unit_price': str(po_pick.get('price_unit')),
                    'po_total_price':str(po_pick.get('sub_total')),
                    'po_supplier_name': str(po_pick.get('partner_name')),
                    'po_receive_qty': str(po_pick.get('total_recv_qty')),
                    'po_receive_date': str(po_pick.get('last_recv_date'))
                }
                po_query_dict[po_pick.get('sale_line_id')] = tmp_dict



        ## Ends Here


        for order_items in order_objs:


            so_remaining_qty = order_items.get('product_uom_qty') - order_items.get('qty_delivered')+ order_items.get('qty_return')


            purchase_query = "select purchase_order.id as po_id,purchase_order.name,purchase_order.create_date,purchase_order.date_approve,purchase_order_line.product_qty,purchase_order_line.price_unit,purchase_order.partner_id,purchase_order_line.product_id, (select res_partner.name from res_partner where res_partner.id=purchase_order.partner_id) as partner_name from purchase_order,purchase_order_line where purchase_order.id = purchase_order_line.order_id and  purchase_order_line.sale_order_id =%s and purchase_order_line.product_id=%s"

            query_execute = self.cr.execute(purchase_query,(order_items.get('sale_id'),order_items.get('product_id')))

            po_objects = self.cr.dictfetchall()


            c_specs = map(
                lambda x: self.render(x, self.col_specs_template, 'lines'),
                wanted_list)
            count=0
            for list_data in c_specs:

                if str(list_data[0]) == str('name'):
                    list_data[4] = str(order_items.get('so_name'))

                if str(list_data[0]) == str('client_order_ref'):
                    client_order_ref = str(order_items.get('client_order_ref'))
                    list_data[4] = str(client_order_ref.replace("None", ""))

                if str(list_data[0]) == str('status'):
                    list_data[4] = str(order_items.get('state'))

                if str(list_data[0]) == str('order_date'):
                    list_data[4] = str(order_items.get('date_order'))

                # if str(list_data[0]) == str('date_confirm'):
                #     date_confirm = str(order_items.get('date_confirm'))
                #     list_data[4] = str(date_confirm.replace("None", ""))


                if str(list_data[0]) == str('order_confirmation_date'):
                    date_confirm = str(order_items.get('order_confirmation_date'))
                    list_data[4] = str(date_confirm.replace("None", ""))


                if str(list_data[0]) == str('magento_delivery_at'):
                    magetno_delivery_at = str(order_items.get('magetno_delivery_at'))
                    list_data[4] = str(magetno_delivery_at.replace("None", ""))

                if str(list_data[0]) == str('customer'):
                    partner_id = order_items.get('partner_id')
                    abc = final_company_dict.get(partner_id)
                    list_data[4] = str(abc.get('customer_name'))



                if str(list_data[0]) == str('company'):
                    partner_id = order_items.get('partner_id')
                    abc = final_company_dict.get(partner_id)
                    list_data[4] = str(abc.get('company_name'))

                if str(list_data[0]) == str('customer_mail'):
                    partner_id = order_items.get('partner_id')
                    abc = final_company_dict.get(partner_id)
                    list_data[4] = str(abc.get('email'))

                if str(list_data[0]) == str('customer_classification'):
                    partner_id = order_items.get('partner_id')
                    abc = final_company_dict.get(partner_id)
                    list_data[4] = str(abc.get('classification'))


                if str(list_data[0]) == str('warehouse_name'):
                    list_data[4] = str(order_items.get('wh_name'))

                if str(list_data[0]) == str('product_name'):
                    list_data[4] = str(order_items.get('product_name'))


                if str(list_data[0]) == str('product_category'):
                    list_data[4] = str(order_items.get('product_categ'))

                if str(list_data[0]) == str('default_code'):
                    sku = str(order_items.get('sku'))
                    list_data[4] = str(sku.replace("None", ""))

                if str(list_data[0]) == str('product_uom_qty'):
                    list_data[4] = str(order_items.get('product_uom_qty'))

                if str(list_data[0]) == str('price_unit'):
                    list_data[4] = str(order_items.get('price_unit'))

                if str(list_data[0]) == str('so_delivery_qty'):
                    list_data[4] = str(order_items.get('qty_delivered'))

                if str(list_data[0]) == str('so_return_qty'):
                    list_data[4] = str(order_items.get('qty_return'))

                if str(list_data[0]) == str('so_remaining_qty'):
                    list_data[4] = str(so_remaining_qty)

                if str(list_data[0]) == str('total_unit_price'):
                    list_data[4] = str(float(order_items.get('price_unit')) * float(order_items.get('product_uom_qty')))

                if str(list_data[0]) == str('so_reserved_date'):
                    reserve_ddate=''
                    if stock_picking_dict.has_key(order_items.get('sale_line_id')):
                        reserve_ddate = stock_picking_dict[order_items.get('sale_line_id')].get('so_reserved_date')

                    list_data[4] = str(reserve_ddate.replace("None", ""))

                if str(list_data[0]) == str('so_reserved_qty'):
                    re_qty = ''
                    if stock_picking_dict.has_key(order_items.get('sale_line_id')):
                        re_qty = stock_picking_dict[order_items.get('sale_line_id')].get('reserved_qty')

                    list_data[4] = str(re_qty.replace("None", ""))

                if str(list_data[0]) == str('so_delivery_date'):
                    so_delivery_date = ''

                    if stock_picking_dict.has_key(order_items.get('sale_line_id')):
                        so_delivery_date = stock_picking_dict[order_items.get('sale_line_id')].get('so_delivery_date')

                    list_data[4] = str(so_delivery_date.replace("None", ""))

                if str(list_data[0]) == str('so_return_date'):
                    so_return_date = ''
                    if stock_picking_dict.has_key(order_items.get('sale_line_id')):
                        so_return_date = stock_picking_dict[order_items.get('sale_line_id')].get('so_return_date')


                    list_data[4] = str(so_return_date.replace("None", ""))

                if po_query_dict.has_key(order_items.get('sale_line_id')):
                    po_items = po_query_dict.get(order_items.get('sale_line_id'))


                    if str(list_data[0]) == str('po_number'):
                        list_data[4] = str(po_items.get('po_number'))

                    if str(list_data[0]) == str('po_create_date'):
                        list_data[4] = str(po_items.get('po_create_date'))

                    if str(list_data[0]) == str('po_confirm_date'):
                        list_data[4] = str(po_items.get('po_confirm_date'))

                    if str(list_data[0]) == str('po_qty'):
                        list_data[4] = str(po_items.get('po_qty'))

                    if str(list_data[0]) == str('po_unit_price'):
                        list_data[4] = str(po_items.get('po_unit_price'))

                    if str(list_data[0]) == str('po_total_price'):
                        list_data[4] = str(po_items.get('po_total_price'))

                    if str(list_data[0]) == str('po_supplier_name'):
                        list_data[4] = str(po_items.get('po_supplier_name'))

                    if str(list_data[0]) == str('po_receive_qty'):
                        po_receive_qty= str(po_items.get('po_receive_qty'))
                        list_data[4] = str(po_receive_qty.replace("None", ""))



                    if str(list_data[0]) == str('po_receive_date'):
                        po_receive_date = str(po_items.get('po_receive_date'))
                        list_data[4] = str(po_receive_date.replace("None", ""))


            row_data = self.xls_row_template(c_specs, [x[0] for x in c_specs])
            row_pos = self.xls_write_row(
                ws, row_pos, row_data, row_style=self.aml_cell_style)










        ## Ends Here





sale_order_line_xls('report.sale.order.line.xls',
                    'sale.order',
                    parser=sale_order_line_xls_parser)

# Author S. M. Sazedul Haque 2018/11/07

from openerp.osv import fields, osv
import datetime


class OrderEndToEndReportWiz(osv.osv_memory):
    """
    This wizard will provide the partner Ledger report by periods, between any two dates.
    """
    _name = 'order.end.to.end.report.wiz'
    _description = 'Order End To End Report Wiz'

    _columns = {
        'date_from': fields.date("Start Date"),
        'date_to': fields.date("End Date"),
        'category':fields.selection([(1, 'All'),
                                     (2, 'Pending for Procurement'),
                                     (3, 'Delivered Order'),
                                     (4, 'STN')],
                                    'Category'),

        'warehouse_id':fields.many2one('stock.warehouse', 'Warehouse')
    }

    _defaults = {
        'date_from': datetime.datetime.today(),
        'date_to': datetime.datetime.today()
    }

    def _build_contexts(self, cr, uid, ids, data, context=None):
        if context is None:
            context = {}
        result = {'date_from': data['form']['date_from'], 'date_to': data['form']['date_to'],
                  'category': data['form']['category'],'warehouse_id': data['form']['warehouse_id']}

        return result

    def check_report(self, cr, uid, ids, context=None):
        if context is None:
            context = {}

        data = {}

        data = self.read(cr, uid, ids, ['date_from', 'date_to','category','warehouse_id'], context=context)[0]

        datas = {
            'ids': context.get('active_ids', []),
            'model': 'order.end.to.end.report.wiz',
            'form': data
        }
        return {'type': 'ir.actions.report.xml',
                'report_name': 'sale.order.line.xls',
                'datas': datas
                }

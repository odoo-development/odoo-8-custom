# Author Mufti Muntasir Ahmed 25-04-2018

from openerp import models, api


class sale_order_line(models.Model):
    _inherit = 'sale.order'

    @api.model
    def _report_xls_fields(self):

        return [
            'name', 'client_order_ref', 'status', 'order_date',
            # 'date_confirm',
            'order_confirmation_date', 'magento_delivery_at', 'warehouse_name',
            'customer', 'company', 'customer_mail','customer_classification', 'product_name','default_code', 'product_category', 'product_uom_qty', 'so_reserved_qty', 'price_unit', 'total_unit_price',
            'po_number', 'po_create_date', 'po_confirm_date', 'po_qty', 'po_unit_price', 'po_total_price',
            'po_receive_qty', 'po_receive_date', 'po_supplier_name', 'so_delivery_date','so_reserved_date', 'so_delivery_qty', 'so_remaining_qty', 'so_return_qty', 'so_return_date'


        ]

    # Change/Add Template entries
    @api.model
    def _report_xls_template(self):

        return {}

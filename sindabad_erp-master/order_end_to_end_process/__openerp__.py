# Author Mufti Muntasir Ahmed 25-04-2018

{
    'name': 'Order to Order End Process',
    'version': '8.0.0.6.0',
    'author': "Mufti Muntasir Ahmed",
    'category': 'Sales',
    'summary': 'SO to Delivery summary',
    'depends': ['sale', 'report_xls'],
    'data': [
        'security/order_end_to_end_security.xml',
        'security/ir.model.access.csv',
        'wizard/order_end_to_end_report_filter.xml',
        'report/sale_order_line_xls.xml',
    ],
}

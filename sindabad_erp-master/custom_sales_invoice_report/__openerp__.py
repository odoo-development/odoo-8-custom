{
    'name': "Custom Sales Invoice Report",
    'version': '1.0',
    'category': 'Information Technology',
    'author': "Shahjalal Hossain",
    'description': """
        Purchase Order Ref. in invoice report
    """,
    'website': "http://www.zerogravity.com.bd",
    'depends': [
        'account', 'sale'
    ],
    'data': [
        'views/report_invoice.xml'
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}

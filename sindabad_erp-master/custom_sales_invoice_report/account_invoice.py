import itertools
from lxml import etree

from openerp import models, fields, api, _
from openerp.exceptions import except_orm, Warning, RedirectWarning
from openerp.tools import float_compare
import openerp.addons.decimal_precision as dp


class account_invoice(models.Model):
    _inherit = "account.invoice"

    def _get_purchase_order_ref(self):

        for id in self:

            purchase_ref_cus=''
            purchase_ref_query = "SELECT x_puchase_ref_cus FROM sale_order WHERE client_order_ref='{0}'".format(str(id.name))
            self._cr.execute(purchase_ref_query)

            for pr in self._cr.fetchall():
                purchase_ref_cus = pr[0]

            id.purchase_order_ref = purchase_ref_cus


    purchase_order_ref = fields.Char(compute='_get_purchase_order_ref', string="Purchase Order Ref.:")

# -*- coding: utf-8 -*-
##############################################################################
#
# Odoo, Open Source Management Solution
# Copyright (C) 2016 Webkul Software Pvt. Ltd.
# Author : www.webkul.com
#
##############################################################################


{
    'name': 'SSL Wirless SMS Gateway',
    'version': '1.0',
    'category': 'Marketing',
    'summary':'Send sms notifications using SSL wireless gateway.',
    'description': """This is a SSL Wireless sms gateway used to send the sms to the Bangladesh mobile numbers, developed for Zero Gravity Ventures.""",
    "sequence": 1,
    "author": "Webkul Software Pvt. Ltd.",
    "website": "http://www.webkul.com",
    "version": '1.0',
    'depends': ['base_setup',
                'sms_notification',
                'sale'],
    'data': [
        'views/ssl_config_view.xml',
        'views/purchase_view.xml',
        'views/res_partner_view.xml',
        ],
    "application": True,
    'installable': True,
    'auto_install': False,
}

#pip install requests
import requests

class Sender:
    '''An Example Class to use for the submission using HTTP API you can
    perform your own validations into this Class For username,
    password,destination, source, dlr, type, message, server and port'''

    def __init__(self, username, password, message, smstype, dlr, destination, source, server, port):
        #Username that is to be used for submission
        self.username = username

        # password that is to be used along with username
        self.password = password

        # Message content that is to be transmitted
        self.message = message
        
        # What type of the message that is to be sent 
        # 0:means plain text
        # 1:means flash
        # 2:means Unicode (Message content should be in Hex)
        # 6:means Unicode Flash (Message content should be in Hex)
        self.smstype = smstype
        
        # Require DLR or not
        # 0:means DLR is not Required
        # 1:means DLR is Required
        self.dlr = dlr

        # Destinations to which message is to be sent for submitting more
        # than one destination at once destinations should be comma separated
        # Like xxxxxxx,xxxxxxx
        self.destination = destination

        # Sender Id to be used for submitting the message
        self.source = source

        # To what server you need to connect to for submission
        self.server = server

        # Port that is to be used like 8080 or 8000
        self.port = port

    def submit_message(self):
        try:
            # Url that will be called to submit the message
            # send_url = f'http://{self.server}:{self.port}/bulksms/bulksms'
            send_url = 'http://{0}:{1}/bulksms/bulksms'.format(self.server, self.port)

            # request_url = f'{send_url}?username={self.username}&password={self.password}&type={self.smstype}&dlr={self.dlr}&destination={self.destination}&source={self.source}&message={self.message}'
            request_url = '{0}?username={1}&password={2}&type={3}&dlr={4}&destination={5}&source={6}&message={7}'.format(send_url, self.username, self.password, self.smstype, self.dlr, self.destination, self.source, self.message)
            print(request_url)
            
            # This method sets the method type to POST so that
            # will be send as a POST request
            resp = requests.post(request_url)
            
            # Here take the output value of the server.
            response_content = resp._content.decode('utf-8')

            print("Response: " + response_content)
            return response_content
        except Exception as ex:
            print(ex)
            return ex

def main():
    try:
        #Below example is for sending Plain text
        s = Sender(server="api.rmlconnect.net", port=8080, 
            username="SindabadSGNmask",password="sin@1234", message="Hi Mufti this msg from Shahjalal end", 
            dlr="1", smstype="0", destination="+8801716520313", source="Sindabad")
            # 01716520313
        s.submit_message()

        # Below example is for sending unicode
        # this will not work in 2.7
        """
        s1 = Sender(server="api.rmlconnect.net", port=8080, 
            username="SindabadSGNmask",password="sin@1234", message=convert_to_unicode("Hi"), 
            dlr="1", smstype="2", destination="01817535299", source="Sindabad")
            # 01716520313
        s1.submit_message()
        """

    except Exception as ex:
        print(ex)

def convert_to_unicode(message):
    return message.encode('utf-32').hex()
    
if __name__ == '__main__':
    main()


# -*- coding: utf-8 -*-
##############################################################################
#
# Odoo, Open Source Management Solution
# Copyright (C) 2016 webkul
# Author : www.webkul.com
#
##############################################################################

from openerp.osv import fields, osv
from openerp.tools.translate import _

class purchase_order(osv.osv):

	_inherit = "purchase.order"

	def send_sms(self, cr, uid, ids, context=None):
		if not context:
			context = {}
		pr_obj = self.browse(cr, uid, ids)
		if pr_obj:
			supplier = pr_obj.partner_id
			for pol in self.pool["purchase.order.line"].browse(cr, uid, pr_obj.order_line.ids):
				if pol.product_id:
					mobile_numbers = supplier.mobile
					if not mobile_numbers:
						break
					sms_for_khiksha = "Hello " + supplier.name + ",\n" + "Product : " + \
								str(pol.product_id.name) + ",\n" + "Qty : " + \
								str(pol.product_qty) + \
								"\n" + "have been ordered."
					sms_for_sindabad = "Hello " + supplier.name + ",\n" + "The purchase order : " + \
								str(pr_obj.name) + ",\n" + "has been sent to your email. Thank you"
					ssl_config = self.pool["sms.mail.server"].search(cr, uid, [("gateway","=","ssl")], order='sequence asc', limit=1, context=context)
					ssl_config_obj = self.pool["sms.mail.server"].browse(cr, uid, ssl_config, context=context)
					if ssl_config_obj.ssl_sid == "Sindabad":
						sms_text = sms_for_sindabad
					else:
						sms_text = sms_for_khiksha

					new_r = self.pool["sms.sms"].send_sms_by_ssl(cr, uid, ids, body_sms=sms_text, mob_no=mobile_numbers, 
						uniqueid=pr_obj.name, context=context)
		return True


# -*- coding: utf-8 -*-
##############################################################################
#
# Odoo, Open Source Management Solution
# Copyright (C) 2016 webkul
# Author : www.webkul.com
#
##############################################################################

from openerp.osv import fields, osv
from openerp.exceptions import except_orm, Warning, RedirectWarning
import requests
import string, time, math, random
from lxml import etree
import xml.etree.ElementTree as ET
from copy import copy
import urllib


import logging
_logger = logging.getLogger(__name__)

 
def uniqid(prefix='', more_entropy=False):
    m = time.time()
    uniqid = '%8x%05x' %(math.floor(m),(m-math.floor(m))*1000000)
    if more_entropy:
        valid_chars = list(set(string.hexdigits.lower()))
        entropy_string = ''
        for i in range(0,10,1):
            entropy_string += random.choice(valid_chars)
        uniqid = uniqid + entropy_string
    uniqid = prefix + uniqid
    return uniqid

def dictify(r,root=True):
    if root:
        return {r.tag : dictify(r, False)}
    d=copy(r.attrib)
    if r.text:
        d["_text"]=r.text
    for x in r.findall("./*"):
        if x.tag not in d:
            d[x.tag]=[]
        d[x.tag].append(dictify(x,False))
    return d


class sms_sms(osv.osv):
	"""SMS sending using SSL SMS Gateway."""

	_inherit = "sms.sms"
	_name = "sms.sms"
	_description = "SSL Wireless SMS"
	_order = "id desc"

	
	def send_sms_by_ssl(self, cr, uid, ids, body_sms, mob_no, uniqueid=False, context=False):
		ssl_config = self.pool["sms.mail.server"].search(cr, uid, [("gateway","=","ssl")], order='sequence asc', limit=1, context=context)
		if not mob_no and not ssl_config:
			return
		if context and context.get("call_by_sale_confirm_button"):
			sms_id = self.create(cr, uid, {'to':mob_no, 'msg':body_sms}, context=context)
			ids = sms_id
		ssl_config_obj = self.pool["sms.mail.server"].browse(cr, uid, ssl_config, context=context)
		if not ssl_config_obj or not ssl_config_obj.ssl_user_name or not ssl_config_obj.ssl_password or not ssl_config_obj.ssl_sid:
			_logger.info("------------------Missing Information for SSL Config object--------------")
			return False
		mob_numbers_list = mob_no.split(";") if mob_no is not False else mob_no
		data = {
			"user" : ssl_config_obj.ssl_user_name,
			"pass" : ssl_config_obj.ssl_password,
			"sid" : ssl_config_obj.ssl_sid,
		}
		for index, mob in enumerate(mob_numbers_list):
			data.update({"sms[%s][0]"%index :mob})
			data.update({"sms[%s][1]"%index :body_sms})
			data.update({"sms[%s][2]"%index :str(uniqueid) if uniqueid else uniqid("TESTING")})


		try:

			send_urls = ssl_config_obj.ssl_url
			dlr = 1
			smstype=0
			mob_no=''
			source='Sindabad'
			body_sms = urllib.quote_plus(body_sms)

			for mob_no in mob_numbers_list:

				if len(mob_no) >= 13:
					mob_no = mob_no
				else:
					tmp_mob = mob_no[-11:]
					mob_no = str('+88') + str(tmp_mob)


				request_url = '{0}?username={1}&password={2}&type={3}&dlr={4}&destination={5}&source={6}&message={7}'.format(send_urls,ssl_config_obj.ssl_user_name,ssl_config_obj.ssl_password,smstype,dlr,mob_no,source,body_sms)

				resp = requests.post(request_url)
				response_content = resp._content.decode('utf-8')


		except Exception, e:
			print e

		# try:
		# 	response = requests.post(ssl_config_obj.ssl_url, data=data, verify=False)
        #
		# 	# If debug is true
		# 	if ssl_config_obj.sms_debug:
		# 		_logger.info("------------------SSL Server Response  %r--------------", response.text)
		#
		# 	if response.status_code == 200:
		# 		respone_dict = dictify(ET.fromstring(response.text))
		# 	else :
		# 		respone_dict = {}
		# 	if respone_dict.has_key("REPLY") and respone_dict["REPLY"].has_key("SMSINFO") and respone_dict["REPLY"]["SMSINFO"][0].has_key("REFERENCEID"):
		# 		self.write(cr, uid, ids, {"state": "sent"})
		# 	elif respone_dict.has_key("REPLY") and respone_dict["REPLY"].has_key("SMSINFO") and respone_dict["REPLY"]["SMSINFO"][0].has_key("MSISDNSTATUS"):
		# 		self.write(cr, uid, ids, {"state": "undelivered"})
		# 	else:
		# 		self.write(cr, uid, ids, {"state": "undelivered"})
		# 	return response.text
		# except Exception, e:
		# 	print e
	
	def send_sms_via_gateway(self, cr, uid, ids, body_sms, mob_no, context=None):
		if context is None:
			context = {}
		gateway_id = super(sms_sms,self).send_sms_via_gateway(cr, uid, ids, body_sms, mob_no, context=context)

		if gateway_id:
			vals = self.pool["sms.mail.server"].browse(cr, uid, gateway_id, context=context)
			if vals.gateway == 'ssl':
				response = self.send_sms_by_ssl(cr, uid, ids, body_sms, mob_no[0], uniqueid=context.get("uniqid"))
				return response
		return gateway_id
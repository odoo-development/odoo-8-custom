# -*- coding: utf-8 -*-
##############################################################################
#
# Odoo, Open Source Management Solution
# Copyright (C) 2016 webkul
# Author : www.webkul.com
#
##############################################################################

import requests
from openerp import models, fields, api, _
from openerp.exceptions import except_orm, Warning, RedirectWarning
import xml.etree.ElementTree as ET
from copy import copy
import string, time, math, random
import urllib

import logging
_logger = logging.getLogger(__name__)
 
def uniqid(prefix='', more_entropy=False):
    m = time.time()
    uniqid = '%8x%05x' %(math.floor(m),(m-math.floor(m))*1000000)
    if more_entropy:
        valid_chars = list(set(string.hexdigits.lower()))
        entropy_string = ''
        for i in range(0,10,1):
            entropy_string += random.choice(valid_chars)
        uniqid = uniqid + entropy_string
    uniqid = prefix + uniqid
    return uniqid


def dictify(r,root=True):
    if root:
        return {r.tag : dictify(r, False)}
    d=copy(r.attrib)
    if r.text:
        d["_text"]=r.text
    for x in r.findall("./*"):
        if x.tag not in d:
            d[x.tag]=[]
        d[x.tag].append(dictify(x,False))
    return d


class sms_mail_server(models.Model):
	"""Configure the ssl wireless sms gateway."""

	_inherit = "sms.mail.server"
	_name = "sms.mail.server"
	_description = "SSL Wireless gateway for sending sms."


	ssl_url = fields.Char(string="SSL URL", widget="url", required=True)
	ssl_user_name = fields.Char(string="SSL User Name", required=True)
	ssl_password = fields.Char(string="SSL Password", required=True)
	ssl_sid = fields.Selection([("Sindabad","Sindabad"),("Biponee","Biponee"),("Khiksha","Khiksha")], string="SSL Sender Id", required=True)

	

	@api.multi
	def test_conn_ssl(self):
		self.ensure_one() 			
		user_obj = self.env['res.users'].browse(self._uid)
		mobile_number = self.user_mobile_no		
		
		mob_numbers_list = mobile_number.split(";")
		sms_text = "SSL SMS Test Connect'ion & Successful....!!!"

		sms_text = urllib.quote_plus(sms_text)

		sms_text = sms_text.encode('utf-8')

		data = {
			"user" : self.ssl_user_name,
			"pass" : self.ssl_password,
			"sid" : self.ssl_sid,
		}


		try:

			send_urls = self.ssl_url
			dlr = 1
			smstype=0
			mob_no=''
			source='Sindabad'
			for mob_no in mob_numbers_list:

				if len(mob_no) >= 13:
					mob_no = mob_no
				else:
					tmp_mob = mob_no[-11:]
					mob_no = str('+88') + str(tmp_mob)

				request_url = '{0}?username={1}&password={2}&type={3}&dlr={4}&destination={5}&source={6}&message={7}'.format(send_urls,self.ssl_user_name,self.ssl_password,smstype,dlr,mob_no,source,sms_text)

				resp = requests.post(request_url)
				response_content = resp._content.decode('utf-8')


		except Exception, e:
			print e
		# for index, mob_no in enumerate(mob_numbers_list):
		# 	data.update({"sms[%s][0]"%index :mob_no})
		# 	data.update({"sms[%s][1]"%index :sms_text})
		# 	data.update({"sms[%s][2]"%index :uniqid("TESTING")})
		# response = requests.post(self.ssl_url, data=data, verify=False)
		# root = ET.fromstring(response.text)
		# if self.sms_debug:
		# 	_logger.info("----------------Test Connection-----SSL Server Response %r", response.text)
		# respone_dict = dictify(ET.fromstring(response.text))
		# if respone_dict.has_key("REPLY") and respone_dict["REPLY"].has_key("SMSINFO") and respone_dict["REPLY"]["SMSINFO"][0].has_key("REFERENCEID"):
		# 	raise Warning(_(sms_text + '\n Test message has been sent on '+ mobile_number +  ' mobile number.'))
		# elif respone_dict.has_key("REPLY") and respone_dict["REPLY"].has_key("SMSINFO") and respone_dict["REPLY"]["SMSINFO"][0].has_key("MSISDNSTATUS"):
		# 	raise Warning(_(respone_dict["REPLY"]["SMSINFO"][0]["MSISDNSTATUS"][0]["_text"]))
		# else:
		# 	raise Warning(_('Connection error one of the information may be wrong.'),_("SSL Server Response"),response.text)
        #

	@api.model
	def get_reference_type(self):
		selection = super(sms_mail_server, self).get_reference_type()
		selection.append(('ssl','SSL Wireless'))
		return selection

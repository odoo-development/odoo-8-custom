from openerp.osv import fields, osv
from openerp.tools.translate import _


class res_partner(osv.Model):
	_inherit = "res.partner"

	_columns = {
			'recieve_auto_sms': fields.boolean(string="Receive Auto SMS"),
		}

# -*- coding: utf-8 -*-
##############################################################################
#
# Odoo, Open Source Management Solution
# Copyright (C) 2016 webkul
# Author : www.webkul.com
#
##############################################################################

from openerp.osv import fields, osv
from openerp.tools.translate import _


class SaleOrder(osv.osv):
    _inherit = "sale.order"

    # def action_button_confirm(self, cr, uid, ids, context=None):
    #     res = super(SaleOrder, self).action_button_confirm(
    #         cr, uid, ids, context=context)
    #     for o in self.browse(cr, uid, ids):
    #         for sol in self.pool["sale.order.line"].browse(cr, uid, o.order_line.ids):
    #             if sol.product_id and sol.product_id.property_stock_inventory.name == "Merchant Inventory" and sol.product_id.seller_ids:
    #                 for seller in self.pool["product.supplierinfo"].browse(cr, uid, sol.product_id.seller_ids.ids):
    #                     mobile_numbers = seller.name.mobile
    #                     if not mobile_numbers:
    #                         break
    #                     mob_numbers_list = mobile_numbers.split(";")
    #                     sms_text = "Hello " + seller.name.name + ",\n" + "Product : " + \
    #                         str(sol.product_id.name) + "\n" + "Qty : " + \
    #                         str(sol.product_uom_qty) + \
    #                         "\n" + "have been sold out."
    #                     context.update({"call_by_sale_confirm_button": True})
    #                     new_r = self.pool["sms.sms"].send_sms_by_ssl(
    #                         cr, uid, ids, body_sms=sms_text, mob_no=mobile_numbers, uniqueid=o.name, context=context)
    #     return res

    # def action_invoice_create(self, cr, uid, ids, grouped=False, states=None, date_invoice = False, context=None):
    #     res = super(SaleOrder, self).action_invoice_create(cr, uid, ids, grouped=grouped, states=states, date_invoice = date_invoice, context=context)
    #     raise Warning(self.browse(cr, uid, ids).state)

    def write(self, cr, uid, ids, vals, context=None):
        if not context:
            context = {}
        res = super(SaleOrder, self).write(cr, uid, ids, vals, context=context)
        if vals.has_key("state") and vals["state"] == "progress":
            for o in self.browse(cr, uid, ids):
                if not o.magetno_delivery_at:
                    for sol in self.pool["sale.order.line"].browse(cr, uid, o.order_line.ids):
                        if sol.product_id and sol.product_id.property_stock_inventory.name == "Merchant Inventory" and sol.product_id.seller_ids:
                            for seller in self.pool["product.supplierinfo"].browse(cr, uid, sol.product_id.seller_ids.ids):
                                if seller.name.recieve_auto_sms == True:
                                    mobile_numbers = seller.name.mobile
                                    if not mobile_numbers:
                                        break
                                    mob_numbers_list = mobile_numbers.split(";")
                                    sms_text = "Hello " + seller.name.name + ",\n" + "Product : " + \
                                        str(sol.product_id.name) + "\n" + "Qty : " + \
                                        str(sol.product_uom_qty) + \
                                        "\n" + "have been ordered."
                                    context.update({"call_by_sale_confirm_button": True})
                                    new_r = self.pool["sms.sms"].send_sms_by_ssl(
                                        cr, uid, ids, body_sms=sms_text, mob_no=mobile_numbers, uniqueid=o.name, context=context)
        return res

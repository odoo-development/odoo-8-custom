{
    'name': 'STN Creation from Need to buy list',
    'version': '1.0',
    'category': 'Product',
    'author': 'Md Rockibul',
    'description': 'STN Creation from Need to buy list',
    'depends': ['sindabad_customization'],
    'data': [
        'wizard/stn_creation_wizard_view.xml',
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}


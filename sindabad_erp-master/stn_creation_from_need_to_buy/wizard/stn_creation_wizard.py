from openerp.osv import fields, osv
from openerp.tools.translate import _
from datetime import date, datetime


class stn_creation_need_to_buy(osv.osv_memory):
    _name = "stn.creation.need.to.buy"
    _description = "STN Creation from Need to buy list"
    _columns = {

        'stn_date': fields.datetime('Date', required=True, readonly=True, select=True, copy=False),

    }

    _defaults = {
        'stn_date': fields.datetime.now,

    }



    def make_stn(self, cr, uid, ids, context=None):

        return True



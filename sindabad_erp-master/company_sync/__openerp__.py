{
    'name': 'Company Sync',
    'version': '1.0',
    'category': 'WMS',
    'author': 'Shahjalal Hossain',
    'summary': 'Company Sync',
    'description': 'NDR Process',
    'depends': ['base', 'delivery_customer_name', 'account', 'odoo_magento_connect'],
    'installable': True,
    'application': True,
    'auto_install': False,
}

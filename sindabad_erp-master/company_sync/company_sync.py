import re
import xmlrpclib
import json
import requests
import logging
import urllib
from openerp.osv import fields, osv
from openerp.tools.translate import _
from openerp import SUPERUSER_ID, api
from openerp.addons.base.res.res_partner import _lang_get
from openerp.exceptions import Warning
from openerp.http import request


class magento_customers(osv.osv):
    _inherit = "magento.customers"

    _columns = {
        'is_company': fields.boolean('Is Company'),
        'parent_id': fields.many2one('res.partner', 'Parent ID', required=False),
        'magento_company_id': fields.integer('Magento Company ID', required=False),
        # 'legal_name': fields.char('Legal Name'),
        # 'active': fields.boolean('Is Company'),
        # 'tin_no': fields.char('TIN No'),
        'customer_ids': fields.char('It is for bridge Only'),
    }

    # create a new
    def create(self, cr, uid, vals, context=None):

        record = super(magento_customers, self).create(cr, uid, vals, context=context)

        return record

    # update an existing one
    def write(self, cr, uid, ids, vals, context=None):

        record = super(magento_customers, self).write(vals)

        return record


class res_partner(osv.osv):
    _inherit = "res.partner"

    _columns = {
        'legal_name': fields.char('Legal Name'),
        'magento_company_id': fields.integer('Magento Company ID', required=False),
        'tin_no': fields.char('TIN No'),
        'trade_license': fields.char('Trade License'),
        'magento_superuser_id': fields.integer('Magento Superuser ID', required=False),
        'magento_sales_rep_id': fields.integer('Magento Sales Representative ID', required=False),
        'odoo_super_customer_id': fields.integer('Company admin odoo id', required=False),
        'customer_ids': fields.char('It is for bridge Only'),

    }

    def create(self, cr, uid, vals, context=None):


        if context is None:
            context = {}
        if context.has_key('magento'):
            vals = self.customer_array(cr, uid, vals, context=context)

        parent_id = vals.get('parent_id')


        # map company
        # vals = self.map_company(cr, uid, vals, context=context)




        ## check the magento_company ID will exist or not

        found_duplicte = False
        found_com_id=None

        if vals.get('is_company') == True:
            if vals.get('magento_company_id'):
                mag_company_id= vals.get('magento_company_id')



            validated_com = self.company_magento_id_unique_check(cr, uid, vals,mag_company_id, context=context)
            if validated_com[0] == True:
                found_duplicte=True
                found_com_id= validated_com[1]


        if found_duplicte == True:
            return found_com_id

        customer_duplicate_found = False



        # if not vals.get('is_company') and not vals.get('wk_address'):
        #     try:
        #
        #         validated_com = self.customer_email_unique_check(cr, uid,[], vals, context=context)
        #         if validated_com[0] == True:
        #             customer_duplicate_found = True
        #
        #         if customer_duplicate_found == True:
        #             return validated_com[1]
        #     except:
        #         pass

        customer_id = super(res_partner, self).create(cr, uid, vals, context=context)



        # # Validation of Compnay Child assignment and it will work only for compny only not child
        # if vals.get('is_company') == True:
        #     self.assign_compnay_ids_to_the_child(cr,uid,customer_id,vals,context=context)




        ## Send SMS To the RM for Account Manager Creation

        ## {0} %s ==> Company Name
        ## {1} %s ==> Company Email
        ## {2} %s ==> Account Manager Email



        if vals.get('name') == 'Account Manager':
            try:

                partner_obj = self.browse(cr, uid, [parent_id], context=context)
                sms_text = "Company " + str(partner_obj.name)+", created successfully,You can place order now. Email: "+str(partner_obj.email)+" A/C Manager: "+str(vals.get('email'))
                ids = [customer_id]
                name = 'mufti'


                if partner_obj.user_id.phone:
                    mobile_numbers = partner_obj.user_id.phone
                else:
                    mobile_numbers = partner_obj.user_id.mobile



                ## Send SMS
                self.pool.get("send.sms.on.demand").send_sms_on_demand(cr, uid, ids, sms_text, mobile_numbers, name,context=context)
            except:
                # try:
                #     partner_obj = self.browse(cr, uid, [parent_id], context=context)
                #
                #
                #
                #     f = open(
                #         "/usr/lib/python2.7/dist-packages/openerp/addons/sindabad_erp/company_sync/customer_log.txt",
                #         "a")
                #
                #     f.write('\n')
                #     text = "Account Name: " + str(partner_obj.name) + " , Email: "+str(partner_obj.email) +'\n'
                #
                #     f.write(text)
                #     f.close()
                # except:
                    pass

        ## Ends Here

        return customer_id

    def write(self, cr, uid, ids, vals, context=None):

        partner_object = self.browse(cr, uid, ids)






        # try:
        #     if partner_object.is_company:
        #         ## Validation of Compnay Child assignment and it will work only for compny only not child
        #         if vals.get('is_company') == True:
        #             self.assign_compnay_ids_to_the_child(cr, uid, partner_object.id, vals,  context=context)
        # except:
        #     pass



        if context is None:
            context = {}
        if isinstance(ids, (int, long)):
            ids = [ids]
        if context.has_key('magento'):
            vals = self.customer_array(cr, uid, vals, context=context)
            partner_object = self.browse(cr, uid, ids)
            # vals['is_company'] = True if partner_object.is_company else False

        # map company
        if context.has_key('magento'):
            if vals.has_key('is_company') and vals['is_company']:
                # odoo_parent_id = self.map_company(cr, uid, ids, vals, context=context)
                pass

        # map child
        if context.has_key('magento'):
            if vals.has_key('is_company') and not vals['is_company']:
                # self.map_child(cr, uid, ids, vals, context=context)
                pass

        return super(res_partner, self).write(cr, uid, ids, vals, context=context)


    def customer_email_unique_check(self,cr, uid, ids, vals, context=None):
        exist=False
        exist_customer_id=None



        email = vals.get('email')

        res_part_parent_id_update_query = "select id from res_partner  WHERE customer=True and email=%s order by id asc"
        cr.execute(res_part_parent_id_update_query, ([email]))
        abc = cr.fetchall()


        if len(abc)>0:
            index_numbe = len(abc)-1
            exist_customer_id=abc[0][0]
            exist=True



        return [exist,exist_customer_id]

    def company_magento_id_unique_check(self,cr, uid, ids, vals, magento_company_id=None, context=None):
        exist=False
        exist_customer_id=None

        res_part_parent_id_update_query = "select id from res_partner  WHERE is_company=True and magento_company_id=%s order by id asc"
        cr.execute(res_part_parent_id_update_query, ([vals]))
        abc = cr.fetchall()


        if len(abc)>0:
            index_numbe = len(abc)-1
            exist_customer_id=abc[0][0]
            exist=True



        return [exist,exist_customer_id]


    def assign_compnay_ids_to_the_child(self,cr, uid, ids, vals,company_id=None, context=None):

        abc = self.browse(cr, uid, [ids], context=context)


        is_company = abc.is_company
        company_id = abc.id


        customer_str_ids = abc.customer_ids
        if customer_str_ids:
            customer_str_ids = customer_str_ids
        else:
            customer_str_ids = vals.get('customer_ids')

        customer_ids =[]

        for item in customer_str_ids.split(','):
            try:
                c_id = int(item)
                customer_ids.append(c_id)
            except:
                pass


        if is_company == True and company_id is not None:
            if len(customer_ids)>0:
                for id in customer_ids:
                    if company_id != id:
                        res_part_parent_id_update_query = "UPDATE res_partner SET parent_id='{0}' WHERE id='{1}' and is_company=FALSE".format(company_id, id)
                        cr.execute(res_part_parent_id_update_query)
                        cr.commit()


        return True

    def map_child(self, cr, uid, ids, vals, context=None):

        """
        vals
        {'super_user_id': 2729, 'name': 'IT Test', 'mage_customer_id': 962, 'mobile': '01844150031', 'x_classification': 'Ananta', 'company_id': 1, 'email': 'it.test@kiksha.com', 'is_company': False, 'x_industry': ''}
        """

        result = self.search(cr, uid, [('magento_superuser_id', '=', vals['super_user_id']), ('customer', '=', True), ('parent_id', '=', None), ('is_company', '=', True)], context=context)

        if len(result) == 1:

            for id in ids:
                res_part_parent_id_update_query = "UPDATE res_partner SET parent_id='{0}' WHERE id='{1}'".format(result[0], id)
                cr.execute(res_part_parent_id_update_query)
                cr.commit()

        return True

    def map_company(self, cr, uid, ids, vals, context=None):

        """
        vals
        {'super_user_id': 2729, 'legal_name': 'Zero Gravity Ventures Ltd5', 'company_postcode': '1213', 'company_street': 'House 43, Road 7, Block G', 'name': 'Kazi+Mehedi', 'mage_customer_id': 2729, 'mobile': '01713150830', 'x_classification': '', 'company_id': 1, 'company_regionid': '587', 'company_email': 'zunayed.chowdhury@zerogravity.com.bd', 'company_status': True, 'company_city': 'Dhaka', 'company_name': 'Zero Gravity Ventures Ltd4', 'company_telephone': '01911389621', 'sales_representative_id': 1, 'email': 'kazi.mehedi%40zerogravity.com.bd', 'is_company': True, 'x_industry': ''}

        """

        company_info = dict()
        company_info.update(vals)

        company_info['is_company'] = vals['is_company']
        company_info['magento_company_id'] = vals['mage_company_id']
        company_info['magento_superuser_id'] = vals['super_user_id']
        company_info['magento_sales_rep_id'] = vals['sales_representative_id'] if vals['sales_representative_id'] != 0 else 'null'
        company_info['email'] = vals['company_email'] if vals.has_key('company_email') else ''
        company_info['tin_no'] = vals['tin_no'] if vals.has_key('tin_no') else ''
        company_info['trade_license'] = vals['trade_license'] if vals.has_key('trade_license') else ''
        company_info['zip'] = vals['company_postcode']
        company_info['street'] = vals['company_street']
        company_info['mobile'] = vals['company_telephone']
        company_info['active'] = str(vals['company_status']).upper()
        company_info['name'] = vals['company_name']
        company_info['city'] = vals['company_city']

        result = self.search(cr, uid, [('email', '=', company_info['email']), ('customer', '=', True)], context=context)

        company_id = 0

        if len(result) == 1 and company_info['is_company'] == True:
            try:

                new_custoner_info = dict()
                new_custoner_info.update(vals)
                new_custoner_info['is_company'] = False
                new_custoner_info['company_id'] = 1
                new_custoner_info['street'] = vals['company_street']
                new_custoner_info['zip'] = vals['company_postcode']
                new_custoner_info['city'] = vals['company_city']
                # new_custoner_info['country_id'] = vals['country_id']
                if vals.has_key('sales_representative_id') and vals['sales_representative_id'] != 0:
                    new_custoner_info['user_id'] = vals['sales_representative_id']

                customer_id = self.create(cr, uid, new_custoner_info, context=context)

                company_id = result[0]

                res_part_parent_id_update_query = "UPDATE res_partner SET parent_id='{0}' WHERE id='{1}'".format(company_id, customer_id)
                cr.execute(res_part_parent_id_update_query)
                cr.commit()
            except:
                pass

        elif len(result) == 0:

            company_id = self.create(cr, uid, company_info, context=context)
            # create with raw query
            # res_partner_create_query = "INSERT INTO TABLE_NAME (is_company, magento_company_id, magento_superuser_id, magento_sales_rep_id, email, tin_no, trade_license, zip, street, mobile, active, name, city, customer) VALUES (TRUE, '{0}', '{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}','{11}',TRUE)".format(vals['magento_company_id'], vals['magento_superuser_id'], vals['magento_sales_rep_id'], vals['email'], vals['tin_no'], vals['trade_license'], vals['zip'], vals['street'], vals['mobile'], vals['active'], vals['name'], vals['city'])

            # create row in magento.customers
            mc_env = self.env['magento.customers']
            mc_obj = mc_env.search([('magento_company_id', '=', company_info['magento_company_id'])])

            if len(mc_obj) == 0:
                mc_env.create(cr, uid, company_info, context=context)

        else:

            for customer in self.browse(cr, uid, result, context=context):

                # check if the customer is_company else oldest id will be company
                # company_id === parent_id
                if customer.is_company:
                    company_id = customer.id
                    break
                else:
                    company_id = sorted(result)[0]

            # existing_cus_obj = self.browse(cr, uid, [company_id], context=context)
            # existing_cus_obj.write(cr, uid, vals, context=context)

        try:

            res_partner_update_query = "UPDATE res_partner SET is_company=TRUE, magento_company_id='{0}', magento_superuser_id='{1}', magento_sales_rep_id={2}, email='{3}', tin_no='{4}', trade_license='{5}', zip='{6}', street='{7}', mobile='{8}', active='{9}', name='{10}', city='{11}', user_id={12}, display_name='{13}' WHERE id={14}".format(company_info['magento_company_id'], company_info['magento_superuser_id'], company_info['magento_sales_rep_id'], company_info['email'], company_info['tin_no'], company_info['trade_license'], company_info['zip'], company_info['street'], company_info['mobile'], company_info['active'], company_info['name'], company_info['city'], company_info['magento_sales_rep_id'], company_info['name'], company_id)
            cr.execute(res_partner_update_query)
            cr.commit()
        except:
            pass

        """
        for id in ids:
            res_part_parent_id_update_query = "UPDATE res_partner SET parent_id='{0}' WHERE id='{1}'".format(company_id, id)
            cr.execute(res_part_parent_id_update_query)
            cr.commit()
        """

        return company_id

    def map_rm(self, cr, uid, user_vals, context=None):

        """
        # kazi.mehedi is not user but employee
        { 'email': 'kazi.mehedi@zerogravity.com.bd' }
        """

        # user_vals = {'email': 'abdul.zilani@sindabad.com'}

        if user_vals.has_key('email'):

            res_user_env = self.pool.get('res.users')
            res_user_list = res_user_env.search(cr, uid, [('login', '=', user_vals['email'])])

            if res_user_list:
                return res_user_list[0]
            else:
                return 0

    def add_company_sync_log(self, data):

        # $ sudo touch /var/log/company_sync_log.txt
        # $ sudo chmod 777 /var/log/company_sync_log.txt

        with open("/var/log/company_sync_log.txt", "a+") as f:
            f.write(data + "\n\n")

        return True

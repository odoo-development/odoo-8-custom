{
    'name': 'AIT/Vat Challn',
    'version': '1.0',
    'category': 'Account',
    'author': 'Mufti Muntasir Ahmed',
    'summary': 'AIT/Vat unpaid invoices will get 45 days more credit limit',
    'description': 'AIT/Vat unpaid invoices will get 45 days more credit limit',
    'depends': [
        'order_approval_process','customer_order_payment','account','send_sms_on_demand'
    ],
    'data': [

    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}
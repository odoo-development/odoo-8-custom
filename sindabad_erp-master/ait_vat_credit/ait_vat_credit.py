from datetime import datetime, timedelta
import time
from datetime import date
from openerp import SUPERUSER_ID
from openerp.osv import fields, osv
from openerp.tools.translate import _
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT
import openerp.addons.decimal_precision as dp
from openerp import workflow
from openerp import api
from ..base.res.res_partner import format_address
from openerp.exceptions import Warning, except_orm
import dateutil.parser


class sale_order(osv.osv):
    _inherit = "sale.order"

    _columns = {
        'limit_exceed_reason': fields.char('Limit Exceed Reason'),

    }


    def order_resync_check(self,cr,uid,data,context=None):

        mag_order_id = None
        if data.get('order_id'):
            mag_order_id = data['order_id']

        select_so_query = "Select id from sale_order WHERE client_order_ref='{0}'".format(str(mag_order_id))
        cr.execute(select_so_query)
        so_id = cr.fetchone()

        if so_id:
            result = {
                'found':True,
                'odoo_id':so_id[0]
            }
        else:
            result = {
                'found': False,
                'odoo_id': ''
            }

        return result


class account_invoice(osv.osv):
    _inherit = "account.invoice"

    _columns = {

        'vat_ait_challan': fields.boolean('VAT/AIT Challan Reason'),
        'ait_date': fields.date('Vat/Challan Date '),

    }



class magento_orders(osv.osv):
    _inherit = "magento.orders"

    def auto_order_confirmation_process(self,cr,uid,classification=None,payment_type=None,zone=None,order_id=None,order_amount=0,next_date=None,context=None):
        SUPERUSER_ID=1

        if 'Retail' in classification or 'SME' in classification or 'SOHO' in classification or 'HORECA' in classification:
            if 'Cash' in payment_type:
                if order_amount > 900:
                    self.pool['sale.order'].action_re_calculate_so_amount(cr, SUPERUSER_ID, [order_id],context=context)
                    self.pool['sale.order'].action_button_confirm(cr, SUPERUSER_ID, [order_id],context=context)
                    update_so_query = "UPDATE sale_order SET magetno_delivery_at='{0}' WHERE id='{1}'".format(next_date, str(order_id))
                    cr.execute(update_so_query)
                    cr.commit()

                ## Call Order confirmation
        if 'General' in classification or classification == '' or classification is None:
            if zone == 'inside' and 'Cash' in payment_type:
                if order_amount <10000:
                    self.pool['sale.order'].action_re_calculate_so_amount(cr, SUPERUSER_ID, [order_id], context=context)
                    self.pool['sale.order'].action_button_confirm(cr, SUPERUSER_ID, [order_id],context=context)
                    update_so_query = "UPDATE sale_order SET magetno_delivery_at='{0}' WHERE id='{1}'".format(next_date,str(order_id))
                    cr.execute(update_so_query)
                    cr.commit()
            ## Call Order confirmation

        return True


    def create(self, cr, uid, vals, context=None):

        st_time = datetime.now()
        start_time = st_time.strftime("%d/%d/%Y %H:%M:%S")

        # warehouse_dict = {}
        partner_classification=''
        customer_id=None
        # total_invoice_amount=0
        so_inside_outside=''
        payment_type=''
        odoo_order_id=None
        order_amount=0
        next_day=''


        # wh_area = self.pool.get('warehouse.area')
        # wh_area_search = wh_area.search(cr, uid, [('code', '=', 'NODDA')], context=context)
        # wh_area_browse = wh_area.browse(cr, uid, wh_area_search, context=context)

        # areas = wh_area_browse.areas

        # nodda_list = areas.split(',')
        # nodda_list = [a.lstrip() for a in nodda_list]
        #
        # warehouse_dict[2] = nodda_list  # Here key is the nodda warehouse ID
        # warehouse_dict[4] = motijheel_list  # Here key is the Motijeel warehouse ID



        if context is None:
            context = {}
        if context.has_key('instance_id'):
            vals['instance_id'] = context.get('instance_id')

        # ----------------------------------------------
        try:
            found_retail = False
            odoo_order_id = vals['order_ref']
            order_info = self.pool.get('sale.order').browse(cr, uid, [odoo_order_id], context=context)
            partner_classification = str(order_info.partner_id.x_classification)
            order_amount = order_info.amount_total
            so_inside_outside = order_info.so_inside_outside
            payment_type=order_info.note
            next_date = dateutil.parser.parse(order_info.create_date).date() + timedelta(days=1)



            wh_id = 1
            # The following code for Area wise Warhouse change Auto
            # for k, v in warehouse_dict.iteritems():
            #
            #     state_name = order_info.partner_shipping_id.state_id.name
            #     for item in v:
            #         if state_name.upper() == item.upper():
            #             wh_id = k
            #             break
            #     if wh_id is not None:
            #         break
            """
            if 'Retail' in str(partner_classification) or "SOHO" in str(partner_classification):
                found_retail = True
                if wh_id == 2:
                    wh_id=3
                elif wh_id == 4:
                    wh_id = 3
                else:
                    wh_id = 6
            """
            ## Classification wise warehouse selection

            retail_string = 'Retail'
            if retail_string.lower() not in partner_classification.lower():
                wh_id=2


            ## Ends here


            # if product contains name "Airtel or Robi"
            # for p in order_info.order_line:
            #     if "Airtel" in p.name:
            #         wh_id = 9
            #         break

            if wh_id is not None:
                update_so_query_area = "UPDATE sale_order SET warehouse_id='{0}' WHERE id='{1}'".format(
                    wh_id, str(odoo_order_id))
                cr.execute(update_so_query_area)
                cr.commit()

            ### Ends here area wise warehouse selection



            """
            For now x_credit will be checked. But order_ref will be checked when the credit limit will come from magento.
            """

            if vals.get('order_ref') and order_info.x_prepaid == False and found_retail == False:

                if order_info.partner_id.parent_id.is_company == True:
                    company_id = order_info.partner_id.parent_id.id
                else:
                    company_id = order_info.partner_id.id



                    # if order_info.x_credit:
                customer_info = self.pool.get('res.partner').browse(cr, uid, [company_id], context=context)

                child_ids = [item.id for item in customer_info]
                child_ids.append(company_id)





                cust_so_count_query = "SELECT COUNT(id) FROM sale_order WHERE partner_id='{0}'".format(
                    str(order_info.partner_id.id))

                cr.execute(cust_so_count_query)
                cust_so_count = 0
                for c in cr.fetchall():
                    cust_so_count = int(c[0])

                new_customer = False
                credit_limit_exceed = False
                state = 'draft'
                if cust_so_count > 1:
                    new_customer = False
                else:
                    new_customer = True

                ### Get AIT/VAT Amount Limit and Days of 45



                ait_invoiced_amount = 0

                today_date = date.today()
                subtructed_date = date.today() - timedelta(45)


                customer_id = customer_info.commercial_partner_id.id
                # total_invoice_amount=customer_info.total_invoiced

                all_partner_ids = self.pool['res.partner'].search(cr, uid,
                                                                  [('id', 'child_of', customer_id)], context={})
                invoice_ids = self.pool['account.invoice'].search(cr, uid,
                                                                  [('partner_id', 'in', all_partner_ids),
                                                                   ('state', '=', 'open'),('x_for_vat_ait_challan', '=', 'True'),
                                                                   ('ait_date','>=',subtructed_date),('ait_date','<=',today_date),
                                                                   ('type', '=', 'out_invoice')], context={})
                invoicelines = self.pool['account.invoice'].browse(cr, uid, invoice_ids)

                ait_invoiced_amount = 0

                for item in invoicelines:
                    ait_invoiced_amount = ait_invoiced_amount + item.balance

                reason_text = ''



                ### AIT invoice limit check

                ait_invoice_ids = self.pool['account.invoice'].search(cr, uid,
                                                                  [('partner_id', 'in', all_partner_ids),
                                                                   ('state', '=', 'open'),
                                                                   ('x_for_vat_ait_challan', '=', 'True'),
                                                                   ('ait_date', '<', subtructed_date),
                                                                   ('type', '=', 'out_invoice')], context={})




                if len(ait_invoice_ids) > 0:
                    state = 'approval_pending'
                    reason_text = reason_text + 'AIT Chalan Limit/Date Exceeded, '

                ### Ends Here


                if customer_info.credit_limit > 0:
                    total_receivable_amount = customer_info.property_account_receivable.balance + order_info.amount_total - ait_invoiced_amount

                    if total_receivable_amount > customer_info.credit_limit:
                        credit_limit_exceed = True
                        state = 'approval_pending'
                        reason_text = reason_text + 'Credit Limit Exceeded, '

                if new_customer and state == 'approval_pending':
                    state = 'approval_pending'
                elif new_customer and state == 'draft':
                    state = 'new_cus_approval_pending'

                # Next Month Validation




                if customer_info.credit_days > 0:
                    # partner_id = str(order_info.partner_id.id)

                    # handling credit days for all child
                    mother_customer = False
                    if not customer_info.parent_id.id:
                        # mother_customer = True

                        # call method to set credit days for all child
                        # call with parent_id
                        parent_id = str(customer_info.id)

                        credit_days = customer_info.credit_days

                        self._set_credit_days(cr, parent_id, credit_days)
                        # check if credit days exceed
                        credit_days_exceed = self._check_credit_days_exceed(cr, uid, parent_id, context)

                    else:
                        # get the parent_id and set credit days to all child
                        # mother_customer = False
                        parent_id = str(order_info.partner_id.id)

                        parent_customer_info = self.pool.get('res.partner').browse(cr, uid, [int(parent_id)],
                                                                                   context=context)

                        self._set_credit_days(cr, parent_id, parent_customer_info.credit_days)
                        # check if credit days exceed
                        credit_days_exceed = self._check_credit_days_exceed(cr, uid, parent_id, context)
                    if credit_days_exceed:
                        state = 'approval_pending'
                        reason_text = reason_text + 'Credit Days Exceeded, '



                try:
                    if "Ananta" not in str(partner_classification) and "Corporate" not in str(partner_classification):
                        new_customer = False
                        state = "draft"
                        reason_text=''
                except:
                    pass

                try:
                    if order_info.payment_code == 'ipdc':
                        new_customer = False
                        state = "draft"
                except:
                    pass

                try:
                    if credit_limit_exceed:
                        # send SMS & mail
                        parent = order_info.partner_id

                        for i in range(5):
                            if len(parent.parent_id) == 1:
                                parent = parent.parent_id
                            else:
                                parent = parent

                                break

                        credit_amount = str(parent.credit_limit)
                        phone_number = parent.mobile

                        if not phone_number:
                            phone_number = str(parent.phone)

                        email = str(parent.email)

                        sms_text = ''
                        if phone_number:
                            sms_text = "Dear Valued Customer,\nYour credit amount '{0}' has exceed the limit. Please clear the previous dues so that process the order. For further details, please contact your relationship manager.".format(
                                credit_amount)

                            self.pool.get("send.sms.on.demand").send_sms_on_demand(cr, uid, [order_info.id], sms_text,
                                                                                   phone_number, str(parent.name),
                                                                                   context=context)

                        # send email
                        if email:
                            e_log_obj = self.pool.get('send.email.on.demand.log')

                            email_data = {
                                'mail_text': sms_text,
                                'model': "sale.order",
                                'so_id': order_info.id,
                                'email': email
                            }

                            email_log_id = e_log_obj.create(cr, uid, email_data, context=context)
                            # tmp_data = e_log_obj.browse(cr, uid, email_log_id, context=context)

                            # send email
                            email_template_obj = self.pool.get('email.template')
                            template_ids = email_template_obj.search(cr, uid, [('name', '=', 'SO process mail')],
                                                                     context=context)

                            email_template_obj.send_mail(cr, uid, template_ids[0], email_log_id, force_send=True,
                                                         context=context)

                except:
                    pass

                state='draft'

                update_so_query = "UPDATE sale_order SET new_customer='{0}', credit_limit_exceed='{1}', state='{2}',limit_exceed_reason='{3}' WHERE id='{4}'".format(
                    new_customer, credit_limit_exceed, state,reason_text, str(odoo_order_id))
                cr.execute(update_so_query)
                cr.commit()
        except:
            pass
        # ----------------------------------------------



        mag_order_id=super(magento_orders, self).create(cr, uid, vals, context=context)
        try:

            end_time_f = datetime.now()
            end_time = end_time_f.strftime("%d/%d/%Y %H:%M:%S")

            f = open("/usr/lib/python2.7/dist-packages/openerp/addons/sindabad_erp/ait_vat_credit/order_log.txt", "a")
            pl = end_time_f - st_time
            res_mil = int(pl.total_seconds() * 1000)
            f.write('\n')
            text = "Order Id: "+ str(mag_order_id) + "Start: "+ str(start_time) + "End: "+ str(end_time)+ 'Spent Time: '+ str(res_mil) + '\n'

            f.write(text)
            f.close()
        except:
            pass


        ### Call Retail Order Confirmation Process


        # if total_invoice_amount >0:
        #     abc = self.auto_order_confirmation_process(cr,uid=1,classification=partner_classification,payment_type=payment_type,zone=so_inside_outside,order_id=odoo_order_id,order_amount=order_amount,next_date=next_date,context=context)

        ### Ends Here

        return mag_order_id

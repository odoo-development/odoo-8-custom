# Author Mufti Muntasir Ahmed 11/04/2018


{
    'name': 'Offer Message',
    'version': '8.0.0',
    'category': 'Sales',
    'description': """
Offer Message
""",
    'author': 'Mufti Muntasir Ahmed',
    'depends': [],
    'data': [

        'security/offer_message_security.xml',
        'security/ir.model.access.csv',
        'offer_message_view.xml',



    ],

    'installable': True,
    'auto_install': False,
}
from openerp import SUPERUSER_ID
from openerp.osv import osv, fields
from openerp.tools import html2plaintext

class offer_message(osv.osv):

    _name = "offer.message"
    _description = "Offer Messages"
    _columns = {
        'name': fields.char('Name'),
        'company_id': fields.many2one('res.partner', 'Company'),
        'company_type': fields.char('Company Type'),
        'company_email': fields.char('Company Email'),
        'delivered_amount': fields.float('Delivered Amount'),
        'total_count': fields.float('Total Count'),
        'total_message_sent': fields.float('Total SMS Sent'),
        'company_phone': fields.text('Company Phone No#'),


    }

    def get_pajnabee_count(self, cr, uid, company_email=None, context=None):
        pan_count=0
        try:

            company_email = company_email.get('company_email')


            cr.execute("select total_count from  offer_message where company_email=%s limit 1",([company_email]))
            for items in cr.fetchall():
                pan_count=items[0]
        except:
            pass



        return pan_count


    def customer_offer_calcualtion(self, cr, uid,context=None):

        get_all_existing_compnay_ids = []
        cr.execute("select company_id from  offer_message")
        for item in cr.fetchall():
            get_all_existing_compnay_ids.append(item[0])



        #### Get All Data Query


        query = "SELECT sum(account_invoice.amount_total) AS Delivered_Amount,res_partner.email,res_partner.name AS Shop_Name, res_partner.x_classification AS Classification,account_invoice.commercial_partner_id, res_partner.phone,(SELECT sum(ac.amount_total) AS refun_amount FROM account_invoice AS ac, res_partner AS rp, sale_order AS so WHERE ac.commercial_partner_id = rp.id AND ac.name = so.client_order_ref AND so.create_date > '2021-05-02' AND so.create_date < '2021-05-05' AND ac.type = 'out_refund' AND ac.create_date > '2021-05-02' AND ac.create_date < '2021-05-05' AND (rp.x_classification LIKE 'Retail%' OR rp.x_classification LIKE 'SME%' OR rp.x_classification LIKE 'SOHO%') AND rp.customer = TRUE AND rp.id = account_invoice.commercial_partner_id) AS refun_amnt FROM account_invoice, res_partner, sale_order WHERE account_invoice.commercial_partner_id = res_partner.id AND account_invoice.name = sale_order.client_order_ref AND sale_order.create_date > '2021-05-02' AND sale_order.create_date < '2021-05-05' AND account_invoice.type = 'out_invoice' AND account_invoice.create_date > '2021-05-02' AND account_invoice.create_date < '2021-05-05' AND (res_partner.x_classification LIKE 'Retail%' OR res_partner.x_classification LIKE 'SME%' OR res_partner.x_classification LIKE 'SOHO%') AND res_partner.customer = TRUE AND account_invoice.delivered = TRUE GROUP BY email, res_partner.name, res_partner.x_classification, account_invoice.commercial_partner_id, res_partner.phone HAVING sum(account_invoice.amount_total) > 30000.0"

        cr.execute(query)

        all_data = cr.fetchall()


        for items in all_data:
            rfnd_amount= items[6] if items[6] else 0
            final_amount=items[0]- rfnd_amount

            if (final_amount) > 30000:


                if items[4] in get_all_existing_compnay_ids:
                    counter= int(final_amount/30000)
                    cr.execute("update offer_message set total_count=%s,delivered_amount=%s where company_id=%s",([counter,final_amount,items[4]]))
                    cr.commit()
                else:
                    counter = int(final_amount/30000)
                    msg=0


                    cr.execute("INSERT INTO offer_message (company_id,company_type,company_email,delivered_amount,total_count,total_message_sent) VALUES ('{0}','{1}','{2}',{3},{4},{5})".format(items[4],items[3],items[1],final_amount, counter,msg))
                    cr.commit()



        #### Ends Here

        return True

    def customer_offer_sms(self, cr,uid,context=None):
        get_all_existing_compnay_ids = []
        cr.execute("select company_id,total_count,total_message_sent,id from offer_message where total_count > 0")
        for item in cr.fetchall():
            if item[1]>item[2]:


                try:
                    admin_user_id = self.pool.get('res.partner').browse(cr, uid, item[0], context=context)[0].odoo_super_customer_id
                    phone_number = self.pool.get('res.partner').browse(cr, uid, admin_user_id, context=context)[0].mobile
                    get_all_existing_compnay_ids.append([item[0],item[1]])
                    # phone_number='01716520313'
                    name = 'sindabad'
                    ## Send SMS to the Customer
                    sms_text = "Congratulations!! You have unlocked  total {0} Panjabee".format(str(item[1]))

                    ids=[]
                    self.pool.get("send.sms.on.demand").send_sms_on_demand(cr, uid, ids, sms_text, phone_number,
                                                                           name, context=context)
                except:
                    pass

        ### Update Into the sytem


        for up_item in get_all_existing_compnay_ids:
            cr.execute("update offer_message set total_message_sent=%s where company_id=%s",
                       ([up_item[1], up_item[0]]))
            cr.commit()



        ### ENds Here the Updation of SMS



        return True



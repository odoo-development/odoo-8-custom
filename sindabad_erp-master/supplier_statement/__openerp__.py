# Author Mufti Muntasir Ahmed 26-02-2018

{
    'name': 'Supplier Statement',
    'version': '1.0.0',
    'category': 'Accounts',
    'description': """
Allows you to print Supplier Statement    
==============================================================



""",
    'author': 'Mufti Muntasir Ahmed',
    'depends': ['account'],
    'data': [

        'supplier_menu.xml',
        'wizard/supplier_report_statement_view.xml',
        'views/report_supplierstatement.xml',


    ],

    'installable': True,
    'auto_install': False,
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:

import time
from openerp.report import report_sxw
from openerp.osv import fields, osv
from datetime import datetime, timedelta



class SupplierStatement(report_sxw.rml_parse):

    def __init__(self, cr, uid, name, context):
        super(SupplierStatement, self).__init__(cr, uid, name, context)
        self.localcontext.update({
            'get_client_order_ref': self.get_test
        })

        self.context = context

    def get_test(self):
        return 'dfdf'

    def set_context(self, objects, data, ids, report_type=None):
        ids = self.context.get('active_ids')
        partner_obj = self.pool['res.partner']
        context = self.context
        docs = partner_obj.browse(self.cr, self.uid, ids, context)

        due = {}
        paid = {}
        mat = {}

        text = ''
        text_output = {}

        currentDay = datetime.now().day
        currentMonth = datetime.now().month
        currentYear = datetime.now().year


        if docs.monthly_limit:
            days = docs.credit_days
            if days == 0:
                days = 1
            if days >30:
                days = days - 30
            text = 'Please Pay within '+ str(days)+'-'+str(currentMonth)+'-'+str(currentYear)
        else:
            credit_days = docs.credit_days
            first_inv_date = ''
            all_ids=[docs.id]
            for item in docs.child_ids:

                all_ids.append(item.id)

            if len(all_ids) ==1:
                all_ids.append(all_ids[0])



            self.cr.execute("SELECT date_invoice FROM account_invoice WHERE partner_id IN {0} and state='open' ORDER BY date_invoice LIMIT 1".format(tuple(all_ids)))


            for items in self.cr.fetchall():

                first_inv_date = str(items[0])

            if len(first_inv_date) >4:

                # first_inv_date = datetime.today().date()

                date_1 = datetime.strptime(first_inv_date, '%Y-%m-%d')
                limit_date = date_1 + timedelta(days=credit_days)

                text = 'Please Pay within '+str(limit_date)

        init_balance = {}
        st_date = data.get('form').get('date_from')
        end_date = data.get('form').get('date_to')
        context['start_date'] = st_date
        context['end_date'] = end_date

        for partner in docs:
            due[partner.id] = reduce(lambda x, y: x + (
                (y['account_id']['type'] == 'receivable' and y['debit'] or 0) or (
                    y['account_id']['type'] == 'payable' and y['credit'] * -1 or 0)),
                self._lines_get(partner), 0)
            paid[partner.id] = reduce(lambda x, y: x + (
                (y['account_id']['type'] == 'receivable' and y['credit'] or 0) or (
                    y['account_id']['type'] == 'payable' and y['debit'] * -1 or 0)),
                self._lines_get(partner), 0)
            mat[partner.id] = reduce(lambda x, y: x + (y['debit'] - y['credit']),
                                     filter(lambda x: x['date_maturity'] < time.strftime('%Y-%m-%d'),
                                            self._lines_get(partner)), 0)

            init_balance[partner.id] = reduce(lambda x, y: x + (y['debit'] - y['credit']), self._init_lines_get(partner), 0)

        self.localcontext.update({
            'docs': docs,
            'time': time,
            'getLines': self._lines_get,
            'due': due,
            'paid': paid,
            'prev_balance': init_balance,
            'date_range': str(st_date) + ' to ' + str(end_date),
            'mat': mat,
            'text': text_output
        })
        self.context = context

        self.partner_ids = [res['partner_id'] for res in self.cr.dictfetchall()]

        return super(SupplierStatement, self).set_context(objects, data, self.partner_ids, report_type)

    def _lines_get(self, partner):

        start_date = self.context.get('start_date')
        end_date = self.context.get('end_date')

        moveline_obj = self.pool['account.move.line']


        # order_numbers = [str(obj)]
        # purchase_ref_cus = None
        # self.cr.execute("SELECT x_puchase_ref_cus FROM sale_order WHERE name=%s", (order_numbers))
        parent_list = []


        if partner.parent_id:
            parent_list.append(partner.parent_id.id)

            self.cr.execute("select id from  res_partner where customer=True and id=%s", ([partner.parent_id.id]))

            for items in self.cr.fetchall():
                parent_list.append(items[0])
        else:
            parent_list.append(partner.id)


        self.cr.execute("select id from  res_partner where customer=True and parent_id=%s", ([partner.id]))
        for items in self.cr.fetchall():
            parent_list.append(items[0])



        movelines = moveline_obj.search(self.cr, self.uid,
                                        [('partner_id', 'in', parent_list),
                                         ('account_id.type', 'in', ['receivable', 'payable']),
                                         ('state', '<>', 'draft'), ('date', '<=', end_date), ('date', '>=', start_date)])
                                         # ('state', '<>', 'draft'), ('date', '<=', end_date), ('date', '>=', start_date)], order='id')

        movelines = moveline_obj.browse(self.cr, self.uid, movelines)


        return movelines

    def _init_balance_get(self, partner):

        parent_list = []

        if partner.parent_id:
            parent_list.append(partner.parent_id.id)

            self.cr.execute("select id from  res_partner where customer=True and id=%s", ([partner.parent_id.id]))

            for items in self.cr.fetchall():
                parent_list.append(items[0])
        else:
            parent_list.append(partner.id)

        self.cr.execute("select id from  res_partner where customer=True and parent_id=%s", ([partner.id]))
        for items in self.cr.fetchall():
            parent_list.append(items[0])

        start_date = self.context.get('start_date')

        moveline_obj = self.pool['account.move.line']
        movelines = moveline_obj.search(self.cr, self.uid,
                                        [('partner_id', 'in', parent_list),
                                         ('account_id.type', 'in', ['receivable', 'payable']),
                                         ('state', '<>', 'draft'), ('date', '<', start_date)])

        movelines = moveline_obj.browse(self.cr, self.uid, movelines)

        previous_balance = 0

        for items in movelines:
            previous_balance += (items.debit - items.credit)

        return previous_balance

    def _init_lines_get(self, partner):

        parent_list = []

        if partner.parent_id:
            parent_list.append(partner.parent_id.id)

            self.cr.execute("select id from  res_partner where customer=True and id=%s", ([partner.parent_id.id]))

            for items in self.cr.fetchall():
                parent_list.append(items[0])
        else:
            parent_list.append(partner.id)

        self.cr.execute("select id from  res_partner where customer=True and parent_id=%s", ([partner.id]))
        for items in self.cr.fetchall():
            parent_list.append(items[0])

        start_date = self.context.get('start_date')

        moveline_obj = self.pool['account.move.line']
        movelines = moveline_obj.search(self.cr, self.uid,
                                        [('partner_id', 'in', parent_list),
                                         ('account_id.type', 'in', ['receivable', 'payable']),
                                            ('state', '<>', 'draft'), ('date', '<', start_date)])

        movelines = moveline_obj.browse(self.cr, self.uid, movelines)

        previous_balance = 0

        for items in movelines:
            previous_balance += (items.debit - items.credit)

        return movelines


class report_supplierstatement(osv.AbstractModel):
    _name = 'report.supplier_statement.report_supplierstatement'
    _inherit = 'report.abstract_report'
    _template = 'supplier_statement.report_supplierstatement'
    _wrapped_report_class = SupplierStatement

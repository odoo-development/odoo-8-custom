{
    'name': 'PO in Supplier Refund',
    'version': '1.0',
    'category': 'PO in Supplier Refund',
    'author': 'Shahjalal',
    'summary': 'Account',
    'description': 'PO in Supplier Refund',
    'depends': ['account'],
    'data': [
        'views/report_invoice.xml'
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}

from datetime import datetime, timedelta
import time
from datetime import date, datetime
from openerp import SUPERUSER_ID
from openerp.osv import fields, osv
from openerp.tools.translate import _
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT
import openerp.addons.decimal_precision as dp
from openerp import workflow
from openerp import api
import datetime
from openerp.exceptions import except_orm, Warning, RedirectWarning


class putaway_process(osv.osv):
    _inherit = 'putaway.process'

    def put_confirm(self, cr, uid, ids, context=None):
        id_list = ids
        aqc_list=[]
        put_data = self.browse(cr, uid, ids, context)

        product_dict = {}

        for items in put_data.putaway_line_view:
            product_dict[items.product_id]=[items.quantity,items.location]

        for item in put_data.putaway_line_view:
            if len(item.location) <2:
                raise osv.except_osv(_('Warning'), _('You have to specify the location.'))

        if put_data.state == 'confirmed':
            raise osv.except_osv(_('Confirmed'), _('This Put Away already confirmed'))
        if put_data.state == 'cancel':
            raise osv.except_osv(_('Cancelled'), _('This Put Away already Cancelled. You can not confirm it. Please create new one.'))

        from datetime import date, datetime
        current_time = datetime.now()

        for element in id_list:
            ids = [element]

            if ids is not None:
                try:
                    for p_id in ids:
                        confirm_wms_query = "UPDATE putaway_process SET state='confirmed', confirmation_date='{0}' WHERE id='{1}'".format(str(fields.datetime.now()), p_id)
                        cr.execute(confirm_wms_query)
                        cr.commit()
                except:
                    pass

        # put_line_id= [item.id for item in put_data.putaway_line_view]
        # put_line_ids = self.pool.get('relation.put.line.to.aqc.line').search(cr, uid, [('put_line_id', 'in', put_line_id)])
        # assigned_put = self.pool.get('relation.put.line.to.aqc.line').browse(cr, uid, put_line_ids)

        update_grn_id_dict={}

        created_id = None

        for item in put_data.putaway_line_view:

            # cr.execute("select accepted_quality_control.po_id from  accepted_quality_control,accepted_quality_control_line where accepted_quality_control.id =accepted_quality_control_line.aqc_id and accepted_quality_control_line.id=%s", ([item.aqc_line_id]))
            # po_ids=cr.fetchall()
            #
            # po_id=None
            #
            # if len(po_ids)>0:
            po_id=item.po_number

            po_id = self.pool.get('purchase.order').search(cr, uid,[('name', '=', item.po_number)])

            purchase_order = self.pool.get('purchase.order').browse(cr, uid, po_id, context=context)

            picking_ids=[]

            for po in purchase_order:
                for picking in po.picking_ids:
                    if picking.state in ['assigned', 'partially_available']:
                        picking_ids.append(picking.id)

            if len(picking_ids)>0:
                picking = [picking_ids[0]]
            # changing invoice control of picking
            picking_obj = self.pool.get('stock.picking')
            operation_id = picking_obj.browse(cr, uid, picking_ids[0], context=context)
            picking_obj.write(cr, uid, [operation_id.id], {'invoice_state': '2binvoiced'}, context=context)

            context.update({

                'active_model': 'stock.picking',
                'active_ids': picking,
                'active_id': len(picking) and picking[0] or False
            })
            created_id = self.pool['stock.transfer_details'].create(cr, uid, {
                'picking_id': len(picking) and picking[0] or False}, context)

            if created_id is not None:
                break

        if created_id:
            created_id = self.pool.get('stock.transfer_details').browse(cr, uid, created_id, context=context)
            if created_id.picking_id.state in ['assigned', 'partially_available']:
                # raise Warning(_('You cannot transfer a picking in state \'%s\'.') % created_id.picking_id.state)

                processed_ids = []

                # Create new and update existing pack operations
                for lstits in [created_id.item_ids, created_id.packop_ids]:
                    for prod in lstits:
                        if product_dict.get(prod.product_id.id):

                            pack_datas = {
                                'product_id': prod.product_id.id,
                                'product_uom_id': prod.product_uom_id.id,
                                'product_qty': product_dict.get(prod.product_id.id)[0],
                                'package_id': prod.package_id.id,
                                'lot_id': prod.lot_id.id,
                                'location_id': prod.sourceloc_id.id,
                                'location_dest_id': prod.destinationloc_id.id,
                                'result_package_id': prod.result_package_id.id,
                                'date': prod.date if prod.date else datetime.now(),
                                'owner_id': prod.owner_id.id,
                                'product_location': product_dict.get(prod.product_id.id)[1]
                            }

                            if prod.packop_id:
                                prod.packop_id.with_context(no_recompute=True).write(pack_datas)
                                processed_ids.append(prod.packop_id.id)
                            else:
                                pack_datas['picking_id'] = created_id.picking_id.id
                                packop_id = created_id.env['stock.pack.operation'].create(pack_datas)
                                processed_ids.append(packop_id.id)
                # Delete the others
                packops = created_id.env['stock.pack.operation'].search(
                    ['&', ('picking_id', '=', created_id.picking_id.id), '!', ('id', 'in', processed_ids)])
                packops.unlink()

                # Execute the transfer of the picking
                abc = created_id.picking_id.do_transfer()

                ## Get quant IDs and assign the date id it exists
                quant_id_list = []
                exp_date=''
                for put_item in put_data.putaway_line_view:
                    exp_date=put_item.product_exp_date
                    for move_item in created_id.picking_id.move_lines:
                        for moved_data in move_item.quant_ids:
                            if put_item.product_id == moved_data.product_id.id:
                                quant_id_list.append({'quant_id':moved_data.id,
                                                      'exp_date':exp_date})

                try:
                    for q_list in quant_id_list:
                        update_expiry_date = "UPDATE stock_quant SET product_exp_date='{0}' WHERE id='{1}'".format(
                            q_list.get('exp_date'),q_list.get('quant_id'))
                        cr.execute(update_expiry_date)
                        cr.commit()
                except:
                    pass



                ## Ends Here



                if created_id.picking_id.invoice_state != 'invoiced':
                    # Now Create the Invoice for this picking
                    picking_pool = self.pool.get('stock.picking')
                    from datetime import date
                    context['date_inv'] = date.today()
                    context['inv_type'] = 'in_invoice'
                    active_ids = [picking[0]]
                    res = picking_pool.action_invoice_create(cr, uid, active_ids,
                                                             journal_id=1135,
                                                             group=True,
                                                             type='in_invoice',
                                                             context=context)
                    if res:
                        invoice_data = self.pool.get('account.invoice').browse(cr, uid, res[0], context=context)[0]

                        invoice_data.signal_workflow('invoice_open')

        self.pool.get('inb.end.to.end').putaway_confirm_update(cr, uid, str(put_data.aqc_id.po_number), put_data.aqc_id.po_id, str(fields.datetime.now()), context=context)

        return ''

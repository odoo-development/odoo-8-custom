{
    'name': 'WMS Inherit',
    'version': '1.0',
    'category': 'WMS',
    'author': 'Rockibul Alam',
    'summary': 'WMS all process inherit here',
    'description': 'WMS all process inherit here',
    'depends': ['base', 'wms_inbound'],
    'data': [
        'wms_inbound_inherit/wms_inbound_aqc_inherit/report/report_sticker_printing_inherit.xml'
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}

import xlwt
from datetime import datetime
from openerp.osv import orm
from openerp.report import report_sxw
from openerp.addons.report_xls.report_xls import report_xls
from openerp.addons.report_xls.utils import rowcol_to_cell, _render
from openerp.tools.translate import translate, _
import logging
_logger = logging.getLogger(__name__)


_ir_translation_name = 'order.overview.xls'


class order_overview_xls_parser(report_sxw.rml_parse):

    def __init__(self, cr, uid, name, context):
        super(order_overview_xls_parser, self).__init__(
            cr, uid, name, context=context)
        move_obj = self.pool.get('sale.order')
        self.context = context
        wanted_list = move_obj._report_xls_fields_order_overview(cr, uid, context)
        template_changes = move_obj._report_xls_template_order_overview(cr, uid, context)
        self.localcontext.update({
            'datetime': datetime,
            'wanted_list': wanted_list,
            'template_changes': template_changes,
            '_': self._,
        })

    def _(self, src):
        lang = self.context.get('lang', 'en_US')
        return translate(self.cr, _ir_translation_name, 'report', lang, src) \
            or src


class order_overview_xls(report_xls):

    def __init__(self, name, table, rml=False, parser=False, header=True,
                 store=False):
        super(order_overview_xls, self).__init__(
            name, table, rml, parser, header, store)

        # Cell Styles
        _xs = self.xls_styles
        # header
        rh_cell_format = _xs['bold'] + _xs['fill'] + _xs['borders_all']
        self.rh_cell_style = xlwt.easyxf(rh_cell_format)
        self.rh_cell_style_center = xlwt.easyxf(rh_cell_format + _xs['center'])
        self.rh_cell_style_right = xlwt.easyxf(rh_cell_format + _xs['right'])
        # lines
        aml_cell_format = _xs['borders_all']
        self.aml_cell_style = xlwt.easyxf(aml_cell_format)
        self.aml_cell_style_center = xlwt.easyxf(
            aml_cell_format + _xs['center'])
        self.aml_cell_style_date = xlwt.easyxf(
            aml_cell_format + _xs['left'],
            num_format_str=report_xls.date_format)
        self.aml_cell_style_decimal = xlwt.easyxf(
            aml_cell_format + _xs['right'],
            num_format_str=report_xls.decimal_format)
        # totals
        rt_cell_format = _xs['bold'] + _xs['fill'] + _xs['borders_all']
        self.rt_cell_style = xlwt.easyxf(rt_cell_format)
        self.rt_cell_style_right = xlwt.easyxf(rt_cell_format + _xs['right'])
        self.rt_cell_style_decimal = xlwt.easyxf(
            rt_cell_format + _xs['right'],
            num_format_str=report_xls.decimal_format)

        # XLS Template
        self.col_specs_template = {

            'order_number': {
                'header': [1, 20, 'text', _render("_('Order Number')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'company_name': {
                'header': [1, 20, 'text', _render("_('Company Name')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'customer_name': {
                'header': [1, 20, 'text', _render("_('Customer Name')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'classification': {
                'header': [1, 20, 'text', _render("_('Classification')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'email': {
                'header': [1, 20, 'text', _render("_('Email')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'company_email': {
                'header': [1, 20, 'text', _render("_('Company Email')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'warehouse': {
                'header': [1, 20, 'text', _render("_('Warehouse')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},


            'rm_name': {
                'header': [1, 20, 'text', _render("_('RM Name')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'date_confirm': {
                'header': [1, 20, 'text', _render("_('Date Confirm')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'invoice_date': {
                'header': [1, 20, 'text', _render("_('Invoiced Date (Creation)')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'return_invoice_date': {
                'header': [1, 20, 'text', _render("_('Return Invoiced Date (Creation)')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'invoice_delivery_date': {
                'header': [1, 20, 'text', _render("_('Delivery Date (Invoice)')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'invoice_number': {
                'header': [1, 20, 'text', _render("_('Invoiced Number')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'return_inv_number': {
                'header': [1, 20, 'text', _render("_('Return Invoice no')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'product_category': {
                'header': [1, 20, 'text', _render("_('Product Category')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'product_sku': {
                'header': [1, 20, 'text', _render("_('Product SKU')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'product_name': {
                'header': [1, 20, 'text', _render("_('Product Name')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'order_qty': {
                'header': [1, 20, 'text', _render("_('Ordered Quantity')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'return_qty': {
                'header': [1, 20, 'text', _render("_('Returned Quantity')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'cancel_qty': {
                'header': [1, 20, 'text', _render("_('Canceled Quantity')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'delivered_qty': {
                'header': [1, 20, 'text', _render("_('Delivery Quantity')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'order_value': {
                'header': [1, 20, 'text', _render("_('Order Value')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'cost_rate_avg': {
                'header': [1, 20, 'text', _render("_('Cost Rate')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'total_cost': {
                'header': [1, 20, 'text', _render("_('Total Cost')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'sales_rate': {
                'header': [1, 20, 'text', _render("_('Sale Rate')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'total_sales': {
                'header': [1, 20, 'text', _render("_('Total Sale')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'profit': {
                'header': [1, 20, 'text', _render("_('Profit/Loss')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'invoice_value': {
                'header': [1, 20, 'text', _render("_('Invoice Value')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'return_amount': {
                'header': [1, 20, 'text', _render("_('Returned Amount')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'cancel_amount': {
                'header': [1, 20, 'text', _render("_('Cancel Amount')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'actual_delivery_amount': {
                'header': [1, 20, 'text', _render("_('Actual Delivered Amount')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'status': {
                'header': [1, 20, 'text', _render("_('Status')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'invoice_state': {
                'header': [1, 20, 'text', _render("_('Invoice State')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'return': {
                'header': [1, 20, 'text', _render("_('Return')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

        }



    def generate_xls_report(self, _p, _xs, data, objects, wb):

        wanted_list = _p.wanted_list
        self.col_specs_template.update(_p.template_changes)
        _ = _p._

        context = self.context
        st_date = data.get('form').get('date_from')
        end_date = data.get('form').get('date_to')
        wh_id = data.get('form').get('warehouse_id')
        context['start_date'] = st_date
        context['end_date'] = end_date
        context['warehouse_id'] = wh_id
        self.context = context
        date_range = _("Date: %s to %s" % (st_date, end_date))

        # report_name = objects[0]._description or objects[0]._name
        report_name = _("Order Overview Report ")
        ws = wb.add_sheet(report_name[:31])
        ws.panes_frozen = True
        ws.remove_splits = True
        ws.portrait = 0  # Landscape
        ws.fit_width_to_pages = 1
        row_pos = 0

        # set print header/footer
        ws.header_str = self.xls_headers['standard']
        ws.footer_str = self.xls_footers['standard']

        # Title
        cell_style = xlwt.easyxf(_xs['xls_title'])
        c_specs = [
            ('report_name', 4, 3, 'text', report_name),
        ]
        row_data = self.xls_row_template(c_specs, ['report_name'])
        row_pos = self.xls_write_row(
            ws, row_pos, row_data, row_style=cell_style)
        row_pos += 1

        # Date Ranges
        cell_style = xlwt.easyxf(_xs['xls_title'])
        c_specs = [
            ('date_range', 4, 3, 'text', date_range),
        ]
        row_data = self.xls_row_template(c_specs, ['date_range'])
        row_pos = self.xls_write_row(
            ws, row_pos, row_data, row_style=cell_style)
        row_pos += 1

        # Column headers
        c_specs = map(lambda x: self.render(
            x, self.col_specs_template, 'header', render_space={'_': _p._}),
            wanted_list)
        row_data = self.xls_row_template(c_specs, [x[0] for x in c_specs])
        row_pos = self.xls_write_row(
            ws, row_pos, row_data, row_style=self.rh_cell_style,
            set_column_size=True)
        ws.set_horz_split_pos(row_pos)
        sale_discount =0
        uid=1

        in_query = "select id from  stock_picking_type where code='incoming'"
        self.cr.execute(in_query)
        in_objects = self.cr.dictfetchall()

        incoming_ids = [in_item.get('id') for in_item in in_objects]


        ### Following code has been added for 0 cost value update

        list_of_zero_cost_value_query='select product_id,count(*) as total_roq from stock_quant where cost =0 group by product_id'
        self.cr.execute(list_of_zero_cost_value_query)
        zero_data = self.cr.dictfetchall()

        added_wh_id = False
        if wh_id is not False:
            wh_id = wh_id[0]
            added_wh_id = True

        for items in zero_data:
            p_id=items.get('product_id')
            partner_obj = self.pool("product.product")
            p_obj = partner_obj.browse(self.cr, uid, [p_id], context=context)[0]

            current_cost_price=p_obj.standard_price

            if current_cost_price == 0:
                current_cost_price = p_obj.average_purchaes_price


            query_for_product = "update stock_quant set cost=%s where cost=0 and product_id=%s "
            self.cr.execute(query_for_product, (current_cost_price, p_id))
            self.cr.commit()

        ### Ends code here for 0 cost value update

        query = "SELECT stock_move.id AS moved_id,stock_move.product_id,stock_move.state as order_status ,stock_picking.date_done,stock_picking.origin,stock_picking.name as stock_picking_name,stock_picking.picking_type_id,stock_move.product_uom_qty,sale_order_line.product_uom_qty AS order_qty,sale_order_line.price_unit,sale_order_line.order_partner_id,sale_order_line.closed_quantity AS closed_quantity,sale_order.client_order_ref,sale_order.date_confirm,sale_order_line.name,sale_order.name  AS odoo_no,sale_order.amount_total AS order_value,(SELECT product_category.name FROM product_category, product_template, product_product WHERE product_category.id = product_template.categ_id AND product_template.id = product_product.product_tmpl_id AND product_product.id = stock_move.product_id) AS category,(SELECT sum((sq.qty * sm.price_unit)) AS total FROM stock_quant_move_rel AS sqm, stock_move AS sm, stock_quant AS sq WHERE sqm.quant_id IN ((SELECT sq2.id FROM stock_quant_move_rel AS sqmr2, stock_quant AS sq2 WHERE sqmr2.move_id = stock_move.id AND sqmr2.quant_id = sq2.id)) AND sqm.move_id = sm.id AND sm.purchase_line_id > 0 AND sq.id = sqm.quant_id GROUP BY sq.product_id) AS total_price,(SELECT name FROM stock_warehouse WHERE id = sale_order.warehouse_id)AS warhouse_name, account_invoice.number AS invoice_number,account_invoice.date_invoice AS invoice_date,account_invoice.delivery_date AS invoice_delivery_date,account_invoice.delivered AS invoice_delivery,account_invoice.amount_total AS invoice_value FROM sale_order join sale_order_line on ( sale_order.id = sale_order_line.order_id)JOIN stock_move on (stock_move.sale_line_id = sale_order_line.id) JOIN stock_picking on (stock_move.picking_id = stock_picking.id) FULL OUTER JOIN account_invoice on (stock_picking.name = account_invoice.origin) WHERE sale_order.date_confirm >= %s AND sale_order.date_confirm <= %s"

        if added_wh_id is True:
            query=query+" AND sale_order.warehouse_id= %s"
            self.cr.execute(query, (st_date, end_date,wh_id))
        else:

            self.cr.execute(query,(st_date,end_date))
        objects =self.cr.dictfetchall()

        partner_list =[]
        product_id_list =[]
        final_product_list ={}
        final_company_list ={}

        partner_obj = self.pool.get("res.partner")

        # user_ids = partner_obj.search(cr, uid, domain, context=context)

        for items in objects:
            partner_list.append(items.get('order_partner_id'))
            product_id_list.append(items.get('product_id'))


        product_id_list = list(dict.fromkeys(product_id_list))
        partner_list = list(dict.fromkeys(partner_list))

        query_for_product = "select id,name_template,default_code from product_product where id in  %s"

        self.cr.execute(query_for_product, (tuple(product_id_list),))
        products = self.cr.dictfetchall()

        for p_items in products:
            final_product_list[p_items.get('id')] = p_items

        p_obj = partner_obj.browse(self.cr, uid, partner_list, context=context)

        for p_items in p_obj:
            if p_items.parent_id:
                final_company_list[p_items.id] = {
                    'customer_name':p_items.name,
                    'company_name':p_items.parent_id.name,
                    'company_code':p_items.parent_code,
                    'classification':p_items.x_classification,
                    'email':p_items.email,
                    'rm_name':p_items.parent_id.user_id.name if p_items.parent_id.user_id.name else p_items.user_id.name,
                    'company_email':p_items.parent_id.email,
                }
            else:
                final_company_list[p_items.id] = {
                    'customer_name': p_items.name,
                    'company_name': p_items.parent_id.name,
                    'classification': p_items.parent_id.x_classification if p_items.parent_id.x_classification else p_items.x_classification,
                    'email': p_items.parent_id.email if p_items.parent_id.email else p_items.email,
                    'rm_name': p_items.parent_id.user_id.name if p_items.parent_id.user_id.name else p_items.user_id.name,
                    'company_code': p_items.parent_id.parent_code,
                    'company_email': p_items.parent_id.email,
                }

        for data_row in objects:
            cal_cost = 0
            product_uom_qty = data_row['product_uom_qty'] if data_row['product_uom_qty'] else 0.0
            closed_quantity = data_row['closed_quantity'] if data_row['closed_quantity'] else 0.0
            state = data_row['order_status']
            status = 'Sales Order'
            order_state = 'Waiting Availability'



            if data_row.get('total_price') is not None:
                cal_cost = data_row.get('total_price')

            if cal_cost ==0:
                get_adjusted_cost_query = "select sum((sq.qty * sm.price_unit)) as total from stock_quant_move_rel as sqm,stock_move as sm, stock_quant as sq where sqm.quant_id in ((select sq2.id from stock_quant_move_rel as sqmr2, stock_quant as sq2 where sqmr2.move_id=%s and sqmr2.quant_id = sq2.id)) and sqm.move_id = sm.id and sm.inventory_id >0 and sq.id=sqm.quant_id group by sq.product_id"
                moved_id = data_row.get('moved_id')
                self.cr.execute(get_adjusted_cost_query, ([moved_id]))
                get_adjusted_cost = self.cr.dictfetchall()
                for a_cost in get_adjusted_cost:
                    if a_cost.get('total') is not None:
                        cal_cost = a_cost.get('total')

            #  Following chekup for sq qty is zero then
            if cal_cost ==0:

                get_adjusted_query="select sq.cost,sm.price_unit from stock_quant_move_rel as sqm,stock_move as sm, stock_quant as sq where sqm.quant_id in (select sq2.id from stock_quant_move_rel as sqmr2, stock_quant as sq2 where sqmr2.move_id=%s and sqmr2.quant_id = sq2.id) and sqm.move_id = sm.id  and sq.id=sqm.quant_id and (sm.purchase_line_id >0 or sm.inventory_id >0) ORDER BY sm.id desc limit 1"
                moved_id = data_row.get('moved_id')
                self.cr.execute(get_adjusted_query, ([moved_id]))
                get_adjusted_cost = self.cr.dictfetchall()
                for a_cost in get_adjusted_cost:
                    if a_cost.get('price_unit') is not None:
                        cal_cost = a_cost.get('price_unit') * data_row.get('product_uom_qty')
                    elif a_cost.get('cost') is not None:
                        cal_cost = a_cost.get('cost') * data_row.get('product_uom_qty')

            if data_row.get('product_uom_qty') > 0:
                calculation_of_rate = cal_cost / data_row.get('product_uom_qty')
            else:
                calculation_of_rate = cal_cost

            data_row['cost_rate_avg'] = str(calculation_of_rate)

            total_sale = data_row.get('price_unit') * data_row.get('product_uom_qty')
            total_cost = cal_cost
            profit = total_sale - total_cost

            order_partner_id = data_row.get('order_partner_id')
            abc = final_company_list.get(order_partner_id)
            data_row['company_name'] = str(abc.get('company_name')) if abc.get('company_name') else ''
            data_row['customer_name'] = str(abc.get('customer_name')) if abc.get('customer_name') else ''
            data_row['email'] = str(abc.get('email')) if abc.get('email') else ''
            data_row['company_email'] = str(abc.get('company_email')) if abc.get('company_email') else ''
            data_row['classification'] = str(abc.get('classification')) if abc.get('classification') else ''
            data_row['rm_name'] = str(abc.get('rm_name')) if abc.get('rm_name') else ''

            if data_row['invoice_delivery']  == False:
                order_state = 'Dispatched'
                status = 'Invoiced'

            if state == 'progress':
                status = 'Sales Order:Confirm Order'

            if state == 'confirmed':
                order_state = 'Waiting Availability'
                status = 'Sales Order:Confirm Order'
            if state == 'assigned':
                order_state = 'Available'
                status = 'Sales Order:Confirm Order'

            if data_row['invoice_delivery'] == True:
                if data_row['order_qty'] - product_uom_qty == 0.0:
                    order_state = 'Delivery Complete'
                    status = 'Done'
                if data_row['order_qty'] - product_uom_qty > 0.0:
                    order_state = 'Partially Delivered'
                    status = 'Delivered'
            if state == 'cancel':
                order_state = 'Cancel'
                status = 'Cancel'

            if (data_row['order_qty']) - closed_quantity == 0.0:
                status = 'Cancel'
                order_state = 'Cancel'

            if data_row.get('picking_type_id') in incoming_ids:

                status = 'Returned'
                order_state = 'Returned Done'

                data_row['total_cost'] = str(cal_cost * -1)
                data_row['profit'] = profit * -1
                data_row['return_inv_number'] = data_row['invoice_number']
                data_row['return_invoice_date'] = data_row['invoice_date']
                data_row['return_amount'] = data_row['invoice_value']
                query_of_parent_inv = 'SELECT number as ref_invoice_number,delivery_date as ref_invoice_delivery_date,' \
                                      'date_invoice as ref_invoice_date,amount_total as ref_invoice_value FROM ' \
                                      'account_invoice WHERE account_invoice.origin = %s'
                self.cr.execute(query_of_parent_inv, ([str(data_row.get('origin'))]))
                parent_inv_data = self.cr.dictfetchall()
                for parent_inv in parent_inv_data:
                    data_row['invoice_delivery_date'] = parent_inv['ref_invoice_delivery_date']
                    data_row['invoice_date'] = parent_inv['ref_invoice_date']
                    data_row['invoice_number'] = parent_inv['ref_invoice_number']
                    data_row['invoice_value'] = parent_inv['ref_invoice_value']

            else:
                data_row['total_cost'] = str(cal_cost)
                data_row['profit'] = profit
                data_row['return_inv_number'] = ''
                data_row['return_invoice_date'] = ''
                data_row['return_amount'] = ''

            data_row['invoice_state'] = order_state
            data_row['status'] = status

        # Sale Order lines
        for line in objects:

            c_specs = map(
                lambda x: self.render(x, self.col_specs_template, 'lines'),
                wanted_list)

            for list_data in c_specs:

                if str(list_data[0]) == str('order_number'):
                    list_data[4] = str(line.get('client_order_ref'))

                if str(list_data[0]) == str('company_name'):
                    list_data[4] = str(line.get('company_name'))

                if str(list_data[0]) == str('customer_name'):
                    list_data[4] = str(line.get('customer_name'))

                if str(list_data[0]) == str('email'):
                    list_data[4] = str(line.get('email'))

                if str(list_data[0]) == str('company_email'):
                    list_data[4] = str(line.get('company_email'))

                if str(list_data[0]) == str('classification'):
                    list_data[4] = str(line.get('classification'))

                if str(list_data[0]) == str('warehouse'):
                    list_data[4] = str(line.get('warhouse_name'))

                if str(list_data[0]) == str('rm_name'):
                    list_data[4] = str(line.get('rm_name'))

                if str(list_data[0]) == str('date_confirm'):
                    date_confirm = str(line.get('date_confirm'))
                    list_data[4] = str(date_confirm.replace("None", ""))

                if str(list_data[0]) == str('invoice_delivery_date'):
                    creation_date = str(line.get('invoice_delivery_date'))
                    list_data[4] = str(creation_date.replace("None", ""))

                if str(list_data[0]) == str('invoice_date'):
                    invoice_date = str(line.get('invoice_date'))
                    list_data[4] = str(invoice_date.replace("None", ""))

                if str(list_data[0]) == str('invoice_number'):
                    invoice_number = str(line.get('invoice_number'))
                    list_data[4] = str(invoice_number.replace("None", ""))

                if str(list_data[0]) == str('return_inv_number'):
                    list_data[4] = str(line.get('return_inv_number'))

                if str(list_data[0]) == str('return_invoice_date'):
                        list_data[4] = str(line.get('return_invoice_date'))

                if str(list_data[0]) == str('return_qty'):
                    list_data[4] = ''
                    if line.get('picking_type_id') in incoming_ids:
                        return_qty = (line.get('product_uom_qty'))
                        list_data[4] = str(line.get('product_uom_qty'))

                if str(list_data[0]) == str('return_amount'):
                    if line.get('picking_type_id') in incoming_ids:
                        list_data[4] = str(line.get('return_amount'))

                if str(list_data[0]) == str('product_category'):
                    list_data[4] = str(line.get('category'))

                if str(list_data[0]) == str('product_name'):
                    product_id = line.get('product_id')
                    abc = final_product_list.get(product_id)
                    list_data[4] = str(abc.get('name_template'))

                if str(list_data[0]) == str('product_sku'):
                    product_id = line.get('product_id')
                    abc = final_product_list.get(product_id)
                    list_data[4] = str(abc.get('default_code'))

                if str(list_data[0]) == str('order_qty'):
                    list_data[4] = str(line.get('order_qty'))

                if str(list_data[0]) == str('cancel_qty'):

                    if str(line.get('order_status')) =='cancel':
                        cancel_qty = (line.get('order_qty'))
                        list_data[4] = str(cancel_qty)
                    elif line.get('closed_quantity'):
                        cancel_qty = (line.get('closed_quantity'))
                        list_data[4] = str(cancel_qty)
                    else:
                        list_data[4] = ''

                if str(list_data[0]) == str('delivered_qty'):
                    list_data[4] = str(line.get('product_uom_qty'))

                if str(list_data[0]) == str('order_value'):
                    list_data[4] = str(line.get('order_value'))

                if str(list_data[0]) == str('cost_rate_avg'):

                    list_data[4] = str(line.get('cost_rate_avg'))

                if str(list_data[0]) == str('total_cost'):
                    list_data[4] = str(line.get('total_cost'))

                if str(list_data[0]) == str('sales_rate'):
                    list_data[4] = str(line.get('price_unit'))

                if str(list_data[0]) == str('total_sales'):
                    if line.get('picking_type_id') in incoming_ids:
                        total_sale = str(line.get('price_unit') * line.get('product_uom_qty') * -1)
                    else:
                        total_sale = str(line.get('price_unit') * line.get('product_uom_qty'))
                    list_data[4] = str(total_sale)

                if str(list_data[0]) == str('profit'):
                    profit = line.get('profit')
                    list_data[4] = str(round(profit,2))

                if str(list_data[0]) == str('invoice_value'):
                    list_data[4] = str(line.get('invoice_value'))

                if str(list_data[0]) == str('cancel_amount'):
                    list_data[4]=''
                    if str(line.get('order_status')) =='cancel':
                        list_data[4] = str(line.get('order_qty') * line.get('price_unit'))
                    if line.get('closed_quantity'):
                        list_data[4] = str(line.get('closed_quantity') * line.get('price_unit'))

                if str(list_data[0]) == str('actual_delivery_amount'):
                    if line.get('picking_type_id') in incoming_ids:
                        total_sale = str(line.get('price_unit') * line.get('product_uom_qty') * -1)
                    else:
                        total_sale = str(line.get('price_unit') * line.get('product_uom_qty'))
                    list_data[4] = str(total_sale)

                if str(list_data[0]) == str('status'):

                    list_data[4] = str(line.get('status'))

                if str(list_data[0]) == str('invoice_state'):

                    list_data[4] = str(line.get('invoice_state'))

                if str(list_data[0]) == str('return'):
                    if line.get('picking_type_id') in incoming_ids:
                        list_data[4] = str('Return')


            row_data = self.xls_row_template(c_specs, [x[0] for x in c_specs])
            row_pos = self.xls_write_row(
                ws, row_pos, row_data, row_style=self.aml_cell_style)


        # For discount Write
        discount_text = 'Total Discount : '+str(sale_discount)

        cell_style = xlwt.easyxf(_xs['xls_title'])
        c_specs = [
            ('sale_discount', 4, 3, 'text', str(discount_text)),
        ]
        row_data = self.xls_row_template(c_specs, ['sale_discount'])
        row_pos = self.xls_write_row(
            ws, row_pos, row_data, row_style=cell_style)
        row_pos += 1


order_overview_xls('report.order.overview.xls',
                    'sale.order',
                    parser=order_overview_xls_parser)

from openerp.osv import fields, osv
import datetime


class OrderOverviewReportWiz(osv.osv_memory):
    """
    This wizard will provide the partner Ledger report by periods, between any two dates.
    """
    _name = 'order.overview.report.wiz'
    _description = 'Order Overview Report Wiz'

    _columns = {
        'date_from': fields.date("Start Date"),
        'date_to': fields.date("End Date"),
        'warehouse_id':fields.many2one('stock.warehouse', 'Warehouse')

    }

    _defaults = {
        'date_from': datetime.date.today(),
        'date_to': datetime.date.today()
    }

    def _build_contexts(self, cr, uid, ids, data, context=None):
        if context is None:
            context = {}
        result = {'date_from': data['form']['date_from'], 'date_to': data['form']['date_to'],'warehouse_id':data['form']['warehouse_id']}

        return result

    def check_report(self, cr, uid, ids, context=None):
        if context is None:
            context = {}

        data = {}

        data = self.read(cr, uid, ids, ['date_from', 'date_to','warehouse_id'], context=context)[0]

        datas = {
            'ids': context.get('active_ids', []),
            'model': 'cogs.report.wiz',
            'form': data
        }
        return {'type': 'ir.actions.report.xml',
                'report_name': 'order.overview.xls',
                'datas': datas
                }

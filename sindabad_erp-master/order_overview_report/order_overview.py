from openerp import models, api


class order_overview(models.Model):
    _inherit = 'sale.order'

    @api.model
    def _report_xls_fields_order_overview(self):
        # 'date_done',
        return [
            'order_number','company_name','customer_name','email','company_email','warehouse','classification',
            'rm_name','date_confirm','invoice_date','invoice_delivery_date','return_invoice_date','invoice_number',
            'return_inv_number','product_category','product_sku','product_name','order_qty','return_qty','cancel_qty','delivered_qty',
            'order_value','cost_rate_avg','total_cost','sales_rate','total_sales','profit','invoice_value',
            'return_amount','cancel_amount','actual_delivery_amount','status','invoice_state','return'
        ]

    # Change/Add Template entries
    @api.model
    def _report_xls_template_order_overview(self):

        return {}

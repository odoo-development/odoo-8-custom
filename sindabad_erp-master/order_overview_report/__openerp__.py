{
    'name': 'Order Overview Report',
    'version': '8.0.1',
    'author': "Shuvarthi Dhar",
    'category': 'Sales',
    'summary': 'Order Overview Report',
    'depends': ['sale','stock', 'report_xls'],
    'data': [
        'security/order_overview_report_security.xml',
        'security/ir.model.access.csv',
        'wizard/order_overview_report_filter.xml',
        'report/order_overview_xls.xml',
    ],
}

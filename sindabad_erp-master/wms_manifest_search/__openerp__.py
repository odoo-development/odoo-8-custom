{
    'name': 'WMS Manifest Custom Search',
    'version': '1.0',
    'category': 'WMS',
    'author': 'Shahjalal Hossain',
    'summary': 'WMS Manifest Custom Search',
    'description': 'WMS Manifest Custom Search',
    'depends': ['base', 'wms_manifest'],
    'installable': True,
    'application': True,
    'auto_install': False,
}

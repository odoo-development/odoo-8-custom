from datetime import datetime, timedelta
import time
from datetime import date
from openerp import SUPERUSER_ID
from openerp.osv import fields, osv
from operator import itemgetter
from openerp.tools.translate import _
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT
import openerp.addons.decimal_precision as dp
from openerp import workflow
from openerp import api
import datetime


class wms_manifest_custom_search(osv.osv):
    _inherit = 'wms.manifest.process'

    def _get_order_no(self, cr, uid, ids, field_name, arg, context=None):

        return True

    def _order_no_search(self, cr, uid, obj, name, args, context=None):

        # args = [('order_on', 'ilike', '1000026832')]
        operator = [item for item in args][0][1]
        order_no = [item for item in args][0][2]

        wms_manifest_line_query = "SELECT DISTINCT wms_manifest_id FROM wms_manifest_line WHERE magento_no LIKE '%{0}%'".format(order_no)
        cr.execute(wms_manifest_line_query)

        res = cr.fetchall()
        if not res:
            return [('id', '=', '0')]
        return [('id', 'in', map(itemgetter(0), res))]

    def _get_invoice_no(self, cr, uid, ids, field_name, arg, context=None):

        return True

    def _invoice_no_search(self, cr, uid, obj, name, args, context=None):
        # operator = [item for item in args][0][1]
        invoice_no = [item for item in args][0][2]

        account_invoice_id_list = self.pool.get('account.invoice').search(cr, uid, [('number','ilike','%'+invoice_no+'%')], context=context)

        if account_invoice_id_list:
            wms_manifest_line_query = "SELECT DISTINCT wms_manifest_id FROM wms_manifest_line WHERE invoice_id = '{0}'".format(account_invoice_id_list[0])
            cr.execute(wms_manifest_line_query)

            res = cr.fetchall()
        else:
            res = None

        # SALJ/2019/10148

        if not res:
            return [('id', '=', '0')]
        return [('id', 'in', map(itemgetter(0), res))]

    def _get_priority_customer(self, cr, uid, ids, field_name, arg, context=None):

        return True

    def _priority_customer_search(self, cr, uid, obj, name, args, context=None):

        # args = [('order_on', 'ilike', '1000026832')]
        operator = [item for item in args][0][1]
        order_no = [item for item in args][0][2]

        wms_manifest_line_query = "SELECT DISTINCT wms_manifest_id FROM wms_manifest_line WHERE priority_customer = '{0}'".format(order_no)
        cr.execute(wms_manifest_line_query)

        res = cr.fetchall()
        if not res:
            return [('id', '=', '0')]
        return [('id', 'in', map(itemgetter(0), res))]


    _columns = {
        'order_on': fields.function(_get_order_no, string='Order Number', fnct_search=_order_no_search, type='text'),
        'invoice_on': fields.function(_get_invoice_no, string='Invoice Number', fnct_search=_invoice_no_search, type='text'),
        'priority_customer_on': fields.function(_get_priority_customer, string='Priority Customer', fnct_search=_priority_customer_search,
                                      type='boolean'),
    }

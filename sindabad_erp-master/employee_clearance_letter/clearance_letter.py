from openerp.osv import fields, osv
from openerp import api
from openerp.tools.translate import _




class clearance_module(osv.osv):
    _name = "clearance.module"
    _description = "Clearance Module"

    _columns = {
        'name': fields.char('Name'),
        'employee_name': fields.many2one('hr.employee', 'Employee Name',required=True),
        'employee_dept_id': fields.many2one('hr.department','Department'),
        'emp_job_title': fields.many2one('hr.job','Designation'),
        'date_of_joining': fields.date('Date Of joining'),
        'issue_date': fields.date('Issue Date',required=True),
        'close_date': fields.date('Close Date',required=True),


        'confirm': fields.boolean('Confirm'),
        'confirm_by': fields.many2one('res.users', 'Confirm By'),
        'confirm_time': fields.datetime('Confirmation Time'),

        'cancel': fields.boolean('Cancel'),
        'cancel_by': fields.many2one('res.users', 'Cancel By'),
        'cancel_time': fields.datetime('Cancel Time'),

        'state': fields.selection([
            ('draft', 'Pending'),
            ('confirm', 'Processing'),
            ('ceo_approved', 'Waiting For CEO Approval'),
            ('approved', 'Approved'),
            ('cancel', 'Cancelled'),

        ], 'Status', readonly=True, copy=False, help="Gives the status of the module", select=True),
        'clearance_line': fields.one2many('clearance.module.line', 'clearance_id', 'Clearance Lines', required=True),

    }

    _defaults = {
        'user_id': lambda obj, cr, uid, context: uid,
        'state': 'draft',
        'issue_date': fields.date.today(),
    }

    @api.onchange('employee_name')
    def employee_dept_id_onchange(self):



        if self.employee_name:

            emp_obj = self.env['hr.employee'].browse(self.employee_name.id)
            self.employee_dept_id=emp_obj.department_id.id
            self.emp_job_title=emp_obj.job_id.id
            self.date_of_joining=emp_obj.date_start


            ## Pull All data of

            config_obj = self.env['clearance.configuration'].search([('active','=',True)])

            dept_list = list()

            for items in config_obj:
                dept_list.append({
                    'name':items.user_id.name,
                    'outstanding_if_any':'',
                    'signature':'',
                    'approve_date':'',
                    'department':items.department_id,
                    'config_department_id':items.id

                })

            self.clearance_line=dept_list




        else:
            self.employee_dept_id=''






    def confirm_btn(self, cr, uid, ids, context=None):

        for single_id in ids:
            confirm_ndr_query = "UPDATE clearance_module SET state='confirm', confirm=TRUE, confirm_time='{0}', confirm_by='{1}' WHERE id={2}".format(
                str(fields.datetime.now()), uid, single_id)
            cr.execute(confirm_ndr_query)
            cr.commit()

        ## Send SMS to complete the clearnce process

        for cnt in self.browse(cr, uid, ids, context=context):
            emp_name = cnt.employee_name.name
            for cl_line in cnt.clearance_line:
                dept_head_name = cl_line.name
                mobile_numbers = cl_line.config_department_id.phone_name


                name = 'Sindabad'
                sms_text = "Dear {0}, our an employee {1} has been requested for resignation clearance. Please approve/notify issue (If any) into our sindabad ERP system. Thanks, Sindabad HR Team.".format(dept_head_name, emp_name)
                self.pool.get("send.sms.on.demand").send_sms_on_demand(cr, uid, ids, sms_text, mobile_numbers, name,
                                                                       context=context)

        ## Ends Here

        return True




    def cancel_btn(self, cr, uid, ids, context=None):

        for single_id in ids:
            cancel_ndr_query = "UPDATE clearance_module SET state='cancel', cancel=TRUE, cancel_time='{0}', cancel_by='{1}' WHERE id={2}".format(
                str(fields.datetime.now()), uid, single_id)
            cr.execute(cancel_ndr_query)
            cr.commit()

        return True

    def ceo_btn(self, cr, uid, ids, context=None):

        for single_id in ids:
            cancel_ndr_query = "UPDATE clearance_module SET state='approved', cancel=TRUE, cancel_time='{0}', cancel_by='{1}' WHERE id={2}".format(
                str(fields.datetime.now()), uid, single_id)
            cr.execute(cancel_ndr_query)
            cr.commit()

        return True

    class clearance_module_line(osv.osv):
        _name = "clearance.module.line"
        _description = "Clearance Module Line"

        _columns = {
            'clearance_id': fields.many2one('clearance.module', 'Clearance', required=True, ondelete='cascade',
                                          select=True,
                                          readonly=True),
            'name': fields.char('Department Head Name'),
            'outstanding_if_any': fields.char('Outstanding Issue (If any)'),
            'signature': fields.char('Signature'),
            'approve_date': fields.date('Approve Date'),
            'department': fields.char('Department'),
            'config_department_id': fields.many2one('clearance.configuration','Department'),
            'state': fields.selection([
                ('draft', 'Pending'),
                ('approved', 'Approved'),
                ('hold', 'Hold')

            ], 'Status', readonly=True, copy=False, help="Gives the status of the module", select=True),


        }
        _defaults = {
            'state': 'draft'
        }




    class clearance_configuration(osv.osv):
        _name = "clearance.configuration"
        _description = "Clearance Configuration"
        _columns = {

            'department_id': fields.selection([
                ('finance', 'Finance & Accounts'),
                ('supply_chain', 'Supply Chain'),
                ('sales_business', 'Sales & Business Development'),
                ('ops_fulfillment', 'Operations & Fulfillment'),
                ('it', 'Information Technology'),
                ('hr_admin', 'HR & Admin'),
                ('ceo', 'CEO & Co-founder'),

            ], 'Department',  required=True,select=True),
            'user_id': fields.many2one('res.users', 'Department User Name', required=True),
            'phone_name': fields.char('Mobile No # ', required=True),
            'active': fields.boolean('Active/Inactive'),
        }

        _defaults = {
            'active': True
        }



    class clearance_issue(osv.osv):
        _name = "add.issue"
        _description = "Clearance Issue"
        _columns = {
            'issue_name': fields.text('Outstanding/Issue (If any)'),

        }

        def add_outstanding_only(self, cr, uid, ids, context=None):

            issue = context['issue_name']
            approve_date = fields.date.today()

            ids = context['active_ids']  # [15526]
            clearance_id = ids[0]



            cr.execute('select id,user_id from clearance_configuration where user_id=%s',(uid,))
            all_data = cr.fetchall()

            if len(all_data) > 0:
                config_department_id = all_data[0][0]
                approve_query = "UPDATE clearance_module_line SET outstanding_if_any='{0}', approve_date='{1}', state='approved' WHERE clearance_id='{2}' and config_department_id= '{3}' ".format(issue,approve_date,ids[0],config_department_id)
                cr.execute(approve_query)
                cr.commit()
                pending = 'draft'
                cr.execute('select id from clearance_module_line where state=%s and clearance_id=%s', (pending,clearance_id,))
                all_data = cr.fetchall()

                if len(all_data) ==0:
                    cr.execute("UPDATE clearance_module set state='approved' where id='{0}'".format(clearance_id,))
                    cr.commit()


            else:
                raise osv.except_osv(_('Sorry You are not authorized to approve'), \
                                     _('Please contact with HR/IT Team'))

            return True






{
    'name': 'Employee Clearance module',
    'version': '8.0',
    'category': 'App',
    'author': 'Mufti Muntasir ahmed',
    'summary': 'Custom module',
    'description': 'Custom module',
    'depends': [],
    'data': [
        'signature_details.xml',
        'clearance_module_view.xml',
        'clearance_configuration_view.xml',
        'clearance_menu.xml',

    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}

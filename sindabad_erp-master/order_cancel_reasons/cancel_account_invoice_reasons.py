from openerp.osv import osv, fields
from openerp import SUPERUSER_ID, api
from openerp.tools.translate import _
from datetime import datetime


class account_invoice(osv.osv):
    _inherit = "account.invoice"

    _columns = {
        'cancel_account_invoice_date': fields.datetime('Cancel Account Invoice Date'),
        'cancel_account_invoice_by': fields.many2one('res.users', 'Cancel Account Invoice By'),
        'cancel_account_invoice_reason': fields.text('Cancel Account Invoice Reason'),
    }


class account_invoice_cancel(osv.osv_memory):
    _inherit = "account.invoice.cancel"

    def cancel_account_invoice(self, cr, uid, ids, context=None):

        super(account_invoice_cancel, self).invoice_cancel(cr, uid, ids, context)

        return True


class cancel_account_invoice_reason(osv.osv):
    _name = "cancel.account.invoice.reason"
    _description = "Cancel Account Invoice Reason"

    _columns = {
        'cancel_account_invoice_date': fields.datetime('Cancel Account Invoice Date'),
        'cancel_account_invoice_by': fields.many2one('res.users', 'Cancel Account Invoice By'),
        'cancel_account_invoice_reason': fields.text('Cancel Account Invoice Reason', required=True),
    }

    def cancel_invoice_reason(self, cr, uid, ids, context=None):

        ids = context['active_ids']
        cancel_account_invoice_reason = str(context['cancel_account_invoice_reason'])
        cancel_account_invoice_by = uid
        cancel_account_invoice_date = str(fields.datetime.now())

        invoice_obj = self.pool.get('account.invoice.cancel')

        for s_id in ids:
            cancel_invoice_query = "UPDATE account_invoice SET cancel_account_invoice_reason='{0}', cancel_account_invoice_by={1}, cancel_account_invoice_date='{2}' WHERE id={3}".format(
                cancel_account_invoice_reason, cancel_account_invoice_by, cancel_account_invoice_date, s_id)
            cr.execute(cancel_invoice_query)
            cr.commit()

            invoice_obj.cancel_account_invoice(cr, uid, [s_id], context)

        return True



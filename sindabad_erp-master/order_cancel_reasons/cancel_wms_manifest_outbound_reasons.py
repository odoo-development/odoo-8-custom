from datetime import datetime, timedelta
import time
from datetime import date
from openerp import SUPERUSER_ID
from openerp.osv import fields, osv
from openerp.tools.translate import _
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT
import openerp.addons.decimal_precision as dp
from openerp import workflow
from openerp import api
import datetime


class wms_manifest_outbound(osv.osv):
    _inherit = 'wms.manifest.outbound'

    _columns = {
        'cancel_manifest_outbound_date': fields.datetime('Cancel Manifest Outbound Date'),
        'cancel_manifest_outbound_by': fields.many2one('res.users', 'Cancel Manifest Outbound By'),
        'cancel_manifest_outbound_reason': fields.text('Cancel Manifest Outbound Reason'),
    }

    def cancel_wms_manifest_outbound_with_reason(self, cr, uid, ids, context=None):

        # super(wms_manifest_process, self).cancel_wms_manifest(cr, uid, ids, context)
        # if confirm is FALSE then state = cancel

        outbound_obj = self.browse(cr, uid, ids, context)
        if not outbound_obj.confirm:

            state_manifest_outbound_query = "UPDATE wms_manifest_outbound SET state='cancel' WHERE id={0}".format(outbound_obj.id)
            cr.execute(state_manifest_outbound_query)
            cr.commit()

        return True


class cancel_wms_manifest_outbound_reasons(osv.osv):
    _name = "cancel.wms.manifest.outbound.reasons"
    _description = "Cancel WMS Manifest Outbound Reasons"

    _columns = {
        'cancel_manifest_outbound_date': fields.datetime('Cancel Manifest Outbound Date'),
        'cancel_manifest_outbound_by': fields.many2one('res.users', 'Cancel Manifest Outbound By'),
        'cancel_manifest_outbound_reason': fields.text('Cancel Manifest Outbound Reason', required=True),
    }

    def cancel_wms_manifest_outbound_reason(self, cr, uid, ids, context=None):

        ids = context['active_ids']
        cancel_manifest_outbound_reason = str(context['cancel_manifest_outbound_reason'])
        cancel_manifest_outbound_by = uid
        cancel_manifest_outbound_date = str(fields.datetime.now())

        manifest_outbound_obj = self.pool.get('wms.manifest.outbound')

        for s_id in ids:
            cancel_manifest_outbound_query = "UPDATE wms_manifest_outbound SET cancel_manifest_outbound_reason='{0}', cancel_manifest_outbound_by={1}, cancel_manifest_outbound_date='{2}' WHERE id={3}".format(
                cancel_manifest_outbound_reason, cancel_manifest_outbound_by, cancel_manifest_outbound_date, s_id)
            cr.execute(cancel_manifest_outbound_query)
            cr.commit()

            manifest_outbound_obj.cancel_wms_manifest_outbound_with_reason(cr, uid, [s_id], context)

        return True

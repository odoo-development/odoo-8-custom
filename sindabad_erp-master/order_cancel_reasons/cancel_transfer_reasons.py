from datetime import date, datetime
from dateutil import relativedelta
import json
import time

from openerp.osv import fields, osv
from openerp.tools.float_utils import float_compare, float_round
from openerp.tools.translate import _
from openerp.tools import DEFAULT_SERVER_DATETIME_FORMAT, DEFAULT_SERVER_DATE_FORMAT
from openerp.exceptions import Warning
from openerp import SUPERUSER_ID, api
import openerp.addons.decimal_precision as dp
from openerp.addons.procurement import procurement
import logging


class stock_picking(osv.osv):
    _inherit = "stock.picking"

    _columns = {
        'cancel_transfer_date': fields.datetime('Cancel Transfer Date'),
        'cancel_transfer_by': fields.many2one('res.users', 'Cancel Transfer By'),
        'cancel_transfer_reason': fields.text('Cancel Transfer Reason'),
    }

    def cancel_stock_picking_transfer(self, cr, uid, ids, context=None):
        super(stock_picking, self).action_cancel(cr, uid, ids, context)

        return True


class stock_picking_cancel_transfer(osv.osv):
    _name = "stock.picking.cancel.transfer"
    _description = "Stock Picking Cancel Transfer"

    _columns = {
        'cancel_transfer_date': fields.datetime('Cancel Transfer Date'),
        'cancel_transfer_by': fields.many2one('res.users', 'Cancel Transfer By'),
        'cancel_transfer_reason': fields.text('Cancel Transfer Reason', required=True),
    }

    def stock_picking_cancel_transfer(self, cr, uid, ids, context=None):
        ids = context['active_ids']
        cancel_transfer_reason = str(context['cancel_transfer_reason'])
        cancel_transfer_by = uid
        cancel_transfer_date = str(fields.datetime.now())

        sp_obj = self.pool.get('stock.picking')

        for s_id in ids:
            cancel_co_query = "UPDATE stock_picking SET cancel_transfer_reason='{0}', cancel_transfer_by={1}, cancel_transfer_date='{2}' WHERE id={3}".format(
                cancel_transfer_reason, cancel_transfer_by, cancel_transfer_date, s_id)
            cr.execute(cancel_co_query)
            cr.commit()

            sp_obj.cancel_stock_picking_transfer(cr, uid, [s_id], context)

        return True

from datetime import datetime, timedelta
import time
from openerp import SUPERUSER_ID
from openerp.osv import fields, osv
from openerp.tools.translate import _
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT
import openerp.addons.decimal_precision as dp
from openerp import workflow
import datetime
import re


class sale_order(osv.osv):
    _inherit = "sale.order"

    # co: Confirmed Order
    _columns = {
        'cancel_co_date': fields.datetime('Confirmed Order Cancel Date'),
        'cancel_co_by': fields.many2one('res.users', 'Confirmed Order Cancel By'),
        'cancel_co_reason': fields.selection([

            ("product_unavailability", "Product unavailability"),
            ("delay_delivery", "Delay delivery"),
            ("pricing_issue", "Pricing issue"),
            ("wrong_order", "Wrong order"),
            ("cash_problem", "Cash problem"),
            ("damage_product", "Damage product"),
            ("double_ordered", "Double ordered"),
            ("sample_not_approved", "Sample not approved"),
            ("wrong_sku", "Wrong SKU"),
            ("not_interested", "Not interested"),
            ("wrong_product", "Wrong product"),
            ("quality_issue", "Quality issue"),
            ("test_order", "Test Order"),
            ("payment_not_done", "Payment not done"),
            ("apnd", "Advance payment not done"),
            ("invalid_information", "Invalid Information"),
            ("customer_unreachable", "Customer unreachable"),
            ("same_day_delivery", "Same day delivery"),
            ("fake_order", "Fake Order"),
            ("sourcing_delay","Sourcing Delay"),
            ("reorder","Reorder"),
            ("discount_issue","Discount Issue"),
            ("product_not_available_customer_will_take_alternative_product", "Product not available, Customer will take alternative product"),

        ], 'Confirmed Order Cancel Reason', copy=False, help="Reasons", select=True)

    }

    def action_cancel_confirmed_order(self, cr, uid, ids, context=None):
        res = super(sale_order, self).action_cancel(cr, uid, ids, context)

        try:
            so = self.browse(cr, uid, ids, context=context)

            parent = so.partner_id

            for i in range(5):
                if len(parent.parent_id) == 1:
                    parent = parent.parent_id
                else:
                    parent = parent

                    break

            company_phone = str(parent.mobile)
            phone_number = str(so.partner_id.mobile) if so.partner_id.mobile else company_phone
            so_cancel_date = datetime.datetime.now().strftime('%A, %d-%m-%Y')

            if not phone_number:
                phone_number = str(so.partner_id.phone)

            email = str(parent.email)
            cancel_reason = ""
            if so.cancel_co_reason:
                cancel_reason += str(so.cancel_co_reason).replace("_", " ")
            if so.cancel_quotation_reason:
                cancel_reason += str(so.cancel_quotation_reason).replace("_", " ")

            sms_text = ''
            if cancel_reason == 'apnd':
                cancel_reason = "Advance payment not done"

            if phone_number:
                sms_text = "Hello {0}, we are sorry to inform that your order #{1} has been canceled.ui due to '{2}'".format(
                    str(parent.name), str(so.client_order_ref), cancel_reason)

                self.pool.get("send.sms.on.demand").send_sms_on_demand(cr, uid, ids, sms_text, phone_number, str(parent.name), context=context)

            # send email
            if email:
                if re.search("^(\d+)@sindabad.com$", email):
                    sin_email_match = re.search("^(\d+)@sindabad.com$", email)
                elif re.search("^sin(\d+)@sindabad.com$", email):
                    sin_email_match = re.search("^sin(\d+)@sindabad.com$", email)
                elif re.search("@sindabadretail.com$", email):
                    sin_email_match = re.search("@sindabadretail.com$", email)
                else:
                    sin_email_match = None

                if sin_email_match is None:
                    e_log_obj = self.pool.get('send.email.on.demand.log')

                    email_data = {
                        'mail_text': sms_text,
                        'model': "sale.order",
                        'so_id': ids[0],
                        'email': email
                    }

                    email_log_id = e_log_obj.create(cr, uid, email_data, context=context)
                    # tmp_data = e_log_obj.browse(cr, uid, email_log_id, context=context)

                    # send email
                    email_template_obj = self.pool.get('email.template')
                    template_ids = email_template_obj.search(cr, uid, [('name', '=','Order Canceled')], context=context)

                    email_template_obj.send_mail(cr, uid, template_ids[0],ids[0],force_send=True, context=context)

        except:
            pass

        return True


class so_cancel_confirmed_order(osv.osv):
    _name = "so.cancel.confirmed.order"
    _description = "SO cancel confirmed order"

    # co: Confirmed Order
    _columns = {
        'cancel_co_date': fields.datetime('Confirmed Order Cancel Date'),
        'cancel_co_by': fields.many2one('res.users', 'Confirmed Order Cancel By'),
        'cancel_co_reason': fields.selection([

            ("product_unavailability", "Product unavailability"),
            ("delay_delivery", "Delay delivery"),
            ("pricing_issue", "Pricing issue"),
            ("wrong_order", "Wrong order"),
            ("cash_problem", "Cash problem"),
            ("damage_product", "Damage product"),
            ("double_ordered", "Double ordered"),
            ("sample_not_approved", "Sample not approved"),
            ("wrong_sku", "Wrong SKU"),
            ("not_interested", "Not interested"),
            ("wrong_product", "Wrong product"),
            ("quality_issue", "Quality issue"),
            ("test_order", "Test Order"),
            ("payment_not_done", "Payment not done"),
            ("apnd", "Advance payment not done"),
            ("invalid_information", "Invalid Information"),
            ("customer_unreachable", "Customer unreachable"),
            ("same_day_delivery", "Same day delivery"),
            ("fake_order", "Fake Order"),
            ("sourcing_delay","Sourcing Delay"),
            ("reorder","Reorder"),
            ("discount_issue","Discount Issue"),
            ("product_not_available_customer_will_take_alternative_product", "Product not available, Customer will take alternative product"),

        ], 'Confirmed Order Cancel Reason', copy=False, help="Reasons", select=True)

    }

    def so_cancel_confirmed_order(self, cr, uid, ids, context=None):
        ids = context['active_ids']
        cancel_co_reason = str(context['cancel_co_reason'])
        cancel_co_by = uid
        cancel_co_date = str(fields.datetime.now())

        so_obj = self.pool.get('sale.order')

        for s_id in ids:
            cancel_co_query = "UPDATE sale_order SET cancel_co_reason='{0}', cancel_co_by={1}, cancel_co_date='{2}' WHERE id={3}".format(
                cancel_co_reason, cancel_co_by, cancel_co_date, s_id)
            cr.execute(cancel_co_query)
            cr.commit()

            so_obj.action_cancel_confirmed_order(cr, uid, [s_id], context)

        return True


from openerp.osv import osv, fields
from openerp import SUPERUSER_ID, api
from openerp.tools.translate import _
from datetime import datetime


class PickingList(osv.osv):
    _inherit = 'picking.list'

    _columns = {
        'cancel_picking_date': fields.datetime('Cancel Picking Date'),
        'cancel_picking_by': fields.many2one('res.users', 'Cancel Picking By'),
        'cancel_picking_reason': fields.selection([
            ("product_not_found","Product not found"),
            ("sku_mismatch","SKU mismatch"),
            ("not_all_qty_available","Not all quantity available"),
            ("sku_damaged","SKU Damaged"),
            ("wrongly_pick_for_other_warehouse","Wrongly pick for other warehouse")

            ], 'Cancel Picking Reason', copy=False, help="Reasons", select=True),

    }

    def cancel_picking_list(self, cr, uid, ids, context=None):
        super(PickingList, self).make_cancel(cr, uid, ids, context)

        return True


class CancelPickingListReason(osv.osv):
    _name = "cancel.picking.list.reason"
    _description = "Cancel Picking Reason"

    _columns = {
        'cancel_picking_date': fields.datetime('Cancel Picking Date'),
        'cancel_picking_by': fields.many2one('res.users', 'Cancel Picking By'),
        'cancel_picking_reason': fields.selection([
            ("product_not_found","Product not found"),
            ("sku_mismatch","SKU mismatch"),
            ("not_all_qty_available","Not all quantity available"),
            ("sku_damaged","SKU Damaged"),
            ("wrongly_pick_for_other_warehouse","Wrongly pick for other warehouse")

            ], 'Cancel Picking Reason', copy=False, help="Reasons", select=True),

    }

    def cancel_picking_list_reason(self, cr, uid, ids, context=None):

        ids = context['active_ids']
        cancel_picking_reason = str(context['cancel_picking_reason'])
        cancel_picking_by = uid
        cancel_picking_date = str(fields.datetime.now())

        picking_obj = self.pool.get('picking.list')

        for s_id in ids:
            cancel_co_query = "UPDATE picking_list SET cancel_picking_reason='{0}', cancel_picking_by={1}, cancel_picking_date='{2}' WHERE id={3}".format(
                cancel_picking_reason, cancel_picking_by, cancel_picking_date, s_id)
            cr.execute(cancel_co_query)
            cr.commit()

            picking_obj.cancel_picking_list(cr, uid, [s_id], context)

        return True

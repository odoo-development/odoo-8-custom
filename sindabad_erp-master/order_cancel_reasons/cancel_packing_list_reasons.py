from openerp.osv import osv, fields
from openerp import SUPERUSER_ID, api
from openerp.tools.translate import _
from datetime import datetime


class PackingList(osv.osv):
    _inherit = 'packing.list'

    _columns = {
        'cancel_packing_date': fields.datetime('Cancel Packing Date'),
        'cancel_packing_by': fields.many2one('res.users', 'Cancel Packing By'),
        'cancel_packing_reason': fields.selection([
            ("product_not_found", "Product not found"),
            ("sku_mismatch", "SKU mismatch"),
            ("not_all_qty_available", "Not all quantity available"),
            ("sku_damaged", "SKU Damaged")


        ], 'Cancel Packing Reason', copy=False, help="Reasons", select=True),

    }

    def cancel_packing_list(self, cr, uid, ids, context=None):

        super(PackingList, self).make_cancel(cr, uid, ids, context)

        return True


class CancelPackingListReason(osv.osv):
    _name = "cancel.packing.list.reason"
    _description = "Cancel Packing Reason"

    _columns = {
        'cancel_packing_date': fields.datetime('Cancel Packing Date'),
        'cancel_packing_by': fields.many2one('res.users', 'Cancel Packing By'),
        'cancel_packing_reason': fields.selection([
            ("product_not_found", "Product not found"),
            ("sku_mismatch", "SKU mismatch"),
            ("not_all_qty_available", "Not all quantity available"),
            ("sku_damaged", "SKU Damaged")


        ], 'Cancel Packing Reason', copy=False, help="Reasons", select=True),

    }

    def cancel_packing_list_reason(self, cr, uid, ids, context=None):

        ids = context['active_ids']
        cancel_packing_reason = str(context['cancel_packing_reason'])
        cancel_packing_by = uid
        cancel_packing_date = str(fields.datetime.now())

        packing_obj = self.pool.get('packing.list')

        for s_id in ids:
            cancel_co_query = "UPDATE packing_list SET cancel_packing_reason='{0}', cancel_packing_by={1}, cancel_packing_date='{2}' WHERE id={3}".format(
                cancel_packing_reason, cancel_packing_by, cancel_packing_date, s_id)
            cr.execute(cancel_co_query)
            cr.commit()

            packing_obj.cancel_packing_list(cr, uid, [s_id], context)

        return True

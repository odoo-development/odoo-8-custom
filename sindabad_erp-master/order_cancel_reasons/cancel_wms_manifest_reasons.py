from datetime import datetime, timedelta
import time
from datetime import date
from openerp import SUPERUSER_ID
from openerp.osv import fields, osv
from openerp.tools.translate import _
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT
import openerp.addons.decimal_precision as dp
from openerp import workflow
from openerp import api
import datetime


class wms_manifest_process(osv.osv):
    _inherit = 'wms.manifest.process'

    _columns = {
        'cancel_manifest_date': fields.datetime('Cancel Manifest Date'),
        'cancel_manifest_by': fields.many2one('res.users', 'Cancel Manifest By'),
        'cancel_manifest_reason': fields.text('Cancel Manifest Reason'),
    }

    def cancel_wms_manifest_with_reason(self, cr, uid, ids, context=None):

        super(wms_manifest_process, self).cancel_wms_manifest(cr, uid, ids, context)

        return True


class cancel_wms_manifest_reasons(osv.osv):
    _name = "cancel.wms.manifest.reasons"
    _description = "Cancel WMS Manifest Reasons"

    _columns = {
        'cancel_manifest_date': fields.datetime('Cancel Manifest Date'),
        'cancel_manifest_by': fields.many2one('res.users', 'Cancel Manifest By'),
        'cancel_manifest_reason': fields.text('Cancel Manifest Reason', required=True),
    }

    def cancel_wms_manifest_reason(self, cr, uid, ids, context=None):

        ids = context['active_ids']
        cancel_manifest_reason = str(context['cancel_manifest_reason'])
        cancel_manifest_by = uid
        cancel_manifest_date = str(fields.datetime.now())

        manifest_obj = self.pool.get('wms.manifest.process')

        for s_id in ids:
            cancel_manifest_query = "UPDATE wms_manifest_process SET cancel_manifest_reason='{0}', cancel_manifest_by={1}, cancel_manifest_date='{2}' WHERE id={3}".format(
                cancel_manifest_reason, cancel_manifest_by, cancel_manifest_date, s_id)
            cr.execute(cancel_manifest_query)
            cr.commit()

            manifest_obj.cancel_wms_manifest_with_reason(cr, uid, [s_id], context)

        return True

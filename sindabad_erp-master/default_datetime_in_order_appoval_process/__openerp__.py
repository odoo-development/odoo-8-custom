{
    'name': 'Default Datetime in Order Approval Process',
    'version': '1.0',
    'category': 'Order Approval Process',
    'author': 'Rocky',
    'summary': 'Sales Order, Limit Exceed, Order Approval',
    'description': 'Order Approval Process',
    'depends': ['order_approval_process'],
    'data': [

    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}

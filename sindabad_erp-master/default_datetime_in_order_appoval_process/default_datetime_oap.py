from datetime import datetime, timedelta
import time
from datetime import date
from openerp import SUPERUSER_ID
from openerp.osv import fields, osv
from openerp.tools.translate import _
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT
import openerp.addons.decimal_precision as dp
from openerp import workflow
from openerp import api
import datetime
from ..base.res.res_partner import format_address


class approve_limit_exceed(osv.osv):
    _inherit = "approve.limit.exceed"

    _columns = {

        'approve_date': fields.datetime('CL Approve Date', default=lambda self: fields.datetime.now()),

    }


class cancel_limit_exceed(osv.osv):
    _inherit = "cancel.limit.exceed"

    _columns = {

        'cancel_date': fields.datetime('CL Cancel Date', default=lambda self: fields.datetime.now()),
    }


class approve_new_customer(osv.osv):
    _inherit = "approve.new.customer"

    _columns = {

        'approve_date': fields.datetime('New Customer Approve Date', default=lambda self: fields.datetime.now()),
    }


class cancel_new_customer(osv.osv):
    _inherit = "cancel.new.customer"

    _columns = {

        'cancel_date': fields.datetime('CL Cancel Date', default=lambda self: fields.datetime.now()),
    }
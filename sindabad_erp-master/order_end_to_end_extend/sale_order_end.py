import xlwt
from datetime import datetime
from openerp.osv import orm
from openerp.report import report_sxw
from openerp.addons.report_xls.report_xls import report_xls
from openerp.addons.report_xls.utils import rowcol_to_cell, _render
from openerp.tools.translate import translate, _
import logging
_logger = logging.getLogger(__name__)

import logging
from datetime import datetime
from dateutil.relativedelta import relativedelta
from operator import itemgetter
import time

import openerp
from openerp import SUPERUSER_ID
from openerp import tools
from openerp.osv import fields, osv, expression
from openerp.tools.translate import _
from openerp.tools.float_utils import float_round as round
from datetime import datetime, timedelta
import datetime

import openerp.addons.decimal_precision as dp
# from ..order_end_to_end_process.report.sale_order_line_xls import sale_order_line_xls


class sale_order_end_to_end(osv.osv):

    _name = "sale.order.end.to.end"

    _columns = {
        'name': fields.char('Order Number'),
        'client_order_ref': fields.char('Magento Number'),
        'status': fields.char('Status'),
        'order_date': fields.datetime('Order Date'),
        'date_confirm': fields.char('Confirmation Date'),
        'order_confirm_time': fields.char('SO Confirmation Time'),

        'magento_delivery_at': fields.char('Expected Shipment Date'),
        'default_code': fields.char('SKU'),
        'warehouse_name': fields.char('Warehouse'),
        'customer': fields.char('Customer'),
        'company': fields.char('Company'),
        'customer_mail': fields.char('Customer Email'),
        'shipped': fields.char('Order Invoiced'),
        'invoiced_dispatch': fields.char('Dispatched'),
        'delivery_dispatch': fields.char('Delivered'),
        'product_name': fields.char('Product Name'),
        'product_category': fields.char('Product Category'),
        'product_uom_qty': fields.char('Order QTY'),
        'so_reserved_qty': fields.char('Reserved QTY'),
        'price_unit': fields.char('Order Unit Price'),
        'total_unit_price': fields.char('Total Price'),
        'po_number': fields.char('PO Number'),
        'po_create_date': fields.char('PO Create Date'),
        'po_confirm_date': fields.char('PO Confirm Date'),
        'po_qty': fields.char('Purchase Qty'),
        'po_unit_price': fields.char('PO Unit Price'),
        'po_total_price': fields.char('PO Total Price'),
        'po_receive_qty': fields.char('PO Receive QTY'),
        'last_po_receive_date': fields.char('Last receive date'),
        'po_receive_date': fields.char('PO Receive Date'),
        'po_supplier_name': fields.char('PO Supplier Name'),
        'so_delivery_date': fields.char('SO Delivery Date'),
        'last_so_delivery_date': fields.char('Last delivery date'),
        'so_reserved_date': fields.char('Reserved Date'),
        'so_delivery_breakdown': fields.char('SO Delivery QTY Breakdown'),
        'so_delivery_qty': fields.char('SO Total Delivery QTY'),
        'so_remaining_qty': fields.char('SO Remaining/Pending QTY'),
        'so_return_qty': fields.char('SO Return QTY'),
        'so_return_date': fields.char('SO Return Date'),
        'invoice_date': fields.char('Invoice Date'),
        'invoice_confirmation_date': fields.char('Invoice Confirmation Date'),
        'invoice_number': fields.char('Invoice Number'),
    }


    def _get_invoice_data(self, cr, sale_order_line, uid, stock_picking_pool, context):
        invoice_date = ''
        invoice_confirmation_date = ''
        invoice_number = ''

        stock_move_query = "SELECT picking_id FROM stock_move WHERE origin='{0}' AND product_id='{1}'".format(str(sale_order_line.order_id.name), sale_order_line.product_id.id)
        cr.execute(stock_move_query)
        picking_id_list = list()
        for picking_id_info in cr.fetchall():
            picking_id_list.append(picking_id_info[0])

        stock_picking_data = stock_picking_pool.browse(cr, uid, picking_id_list, context=context)

        stock_picking_name_list = list()
        for st_pic_da in stock_picking_data:
            stock_picking_name_list.append(str(st_pic_da.name))

        for stock_pick_name in stock_picking_name_list:
            try:
                name_split_list = stock_pick_name.split("\\")

                if len(name_split_list) == 1:
                    name_split_list = stock_pick_name.split("/")

                if len(name_split_list) == 3:

                    account_invoice_query = "SELECT date_invoice, date_due, number FROM account_invoice WHERE origin LIKE '{0}%' AND origin LIKE '%{1}%' AND origin LIKE '%{2}' ".format(name_split_list[0], name_split_list[1], name_split_list[2])
                    cr.execute(account_invoice_query)
                    invoice_date_inf = ''
                    invoice_conf_date_inf = ''
                    invoice_number_inf = ''
                    for acc_inv_info in cr.fetchall():
                        invoice_date_inf = str(acc_inv_info[0])
                        invoice_conf_date_inf = str(acc_inv_info[1])
                        invoice_number_inf = str(acc_inv_info[2])

                    invoice_date += invoice_date_inf + ', ' if invoice_date_inf != 'None' and invoice_date_inf != '' else ''
                    invoice_confirmation_date += invoice_conf_date_inf + ', ' if invoice_conf_date_inf != 'None' and invoice_conf_date_inf != '' else ''
                    invoice_number += invoice_number_inf + ', ' if invoice_number_inf != 'None' and invoice_number_inf != '' else ''
            except:
                pass

        return invoice_date, invoice_confirmation_date, invoice_number

    def _get_order_confirm_time(self, cr, uid, model, record_name, context):

        order_confirm_time = ''

        if len(model.split('.')) == 2:

            try:

                order_con_time_query = "SELECT create_date FROM mail_message WHERE model='{0}' AND record_name='{1}' AND body LIKE '%Quotation confirmed%'".format(
                    model, record_name)
                cr.execute(order_con_time_query)
                for con_time in cr.fetchall():
                    order_confirm_time = str(con_time[0])

                order_confirm_time = order_confirm_time.split(' ')[1] if order_confirm_time else ''
            except:
                order_confirm_time = ''

        return order_confirm_time

    def _get_order_reserved_date(self, cr, uid, move_data, context):
        reserved_date = ''
        stock_move_details_obj = self.pool.get("stock.move.details")
        init_data = stock_move_details_obj.search(cr, uid, [('picking_id', '=', move_data.picking_id.id)], context=context)
        if len(init_data) > 0:
            reserved_date = stock_move_details_obj.browse(cr, uid, init_data[0], context=context).reserved_date

        return reserved_date

    def generate_so_report_data(self, cr, uid, data, objects, context=None):

        c_specs = [
            ['name', 1, 0, 'text', ''],
            ['client_order_ref', 1, 0, 'text', ''],
            ['status', 1, 0, 'text', ''],
            ['order_date', 1, 0, 'text', ''],
            ['date_confirm', 1, 0, 'text', ''],
            ['order_confirm_time', 1, 0, 'text', ''],
            ['magento_delivery_at', 1, 0, 'text', ''],
            ['default_code', 1, 0, 'text', ''],
            ['warehouse_name', 1, 0, 'text', ''],
            ['customer', 1, 0, 'text', ''],
            ['company', 1, 0, 'text', ''],
            ['customer_mail', 1, 0, 'text', ''],
            ['shipped', 1, 0, 'text', ''],

            ['invoiced_dispatch', 1, 0, 'text', ''],
            ['delivery_dispatch', 1, 0, 'text', ''],
            ['product_name', 1, 0, 'text', ''],
            ['product_category', 1, 0, 'text', ''],
            ['product_uom_qty', 1, 0, 'text', ''],
            ['so_reserved_qty', 1, 0, 'text', ''],
            ['price_unit', 1, 0, 'text', ''],
            ['total_unit_price', 1, 0, 'text', ''],
            ['po_number', 1, 0, 'text', ''],
            ['po_create_date', 1, 0, 'text', ''],
            ['po_confirm_date', 1, 0, 'text', ''],
            ['po_qty', 1, 0, 'text', ''],
            ['po_unit_price', 1, 0, 'text', ''],
            ['po_total_price', 1, 0, 'text', ''],
            ['po_receive_qty', 1, 0, 'text', ''],
            ['last_po_receive_date', 1, 0, 'text', ''],
            ['po_receive_date', 1, 0, 'text', ''],
            ['po_supplier_name', 1, 0, 'text', ''],
            ['so_delivery_date', 1, 0, 'text', ''],
            ['last_so_delivery_date', 1, 0, 'text', ''],
            ['so_reserved_date', 1, 0, 'text', ''],
            ['so_delivery_breakdown', 1, 0, 'text', ''],
            ['so_delivery_qty', 1, 0, 'text', ''],
            ['so_remaining_qty', 1, 0, 'text', ''],
            ['so_return_qty', 1, 0, 'text', ''],
            ['so_return_date', 1, 0, 'text', ''],
            ['invoice_date', 1, 0, 'text', ''],
            ['invoice_confirmation_date', 1, 0, 'text', ''],
            ['invoice_number', 1, 0, 'text', '']
        ]

        for c_s in c_specs:
            if c_s[0] == 'name':
                c_specs[0][4] = str(objects.name)
            if c_s[0] == 'client_order_ref':
                c_specs[1][4] = str(objects.client_order_ref)
            if c_s[0] == 'status':
                c_specs[2][4] = str(objects.state)
            if c_s[0] == 'order_date':
                c_specs[3][4] = str(objects.date_order)
            if c_s[0] == 'date_confirm':
                c_specs[4][4] = str(objects.date_confirm)
            if c_s[0] == 'magento_delivery_at':
                c_specs[6][4] = str(objects.magetno_delivery_at)
            if c_s[0] == 'warehouse_name':
                c_specs[8][4] = str(objects.warehouse_id.name)
            if c_s[0] == 'customer':
                c_specs[9][4] = str(objects.partner_id.name)
            if c_s[0] == 'company':
                c_specs[10][4] = str(objects.partner_id.name if objects.partner_id.is_company else objects.partner_id.parent_id.name or '')
            if c_s[0] == 'customer_mail':
                c_specs[11][4] = str(objects.partner_id.email)

            """
            if c_s[0] == 'order_confirm_time':
                c_specs[5][4] = objects.order_confirm_time
            if c_s[0] == 'default_code':
                c_specs[7][4] = objects.default_code
            if c_s[0] == 'shipped':
                c_specs[12][4] = objects.shipped
            """

        # col_specs_templ = sale_order_end_to_end_preprocess('report.sale.order.line','sale.order',parser=sale_order_line_xls_parser)

        # Sale Order lines
        for line in objects:

            # c_specs = map(lambda x: col_specs_templ.render(x, col_specs_templ.col_specs_template, 'lines'), wanted_list)

            purchase_ids = line.purchase_line_ids

            # Order Products
            for items in line.order_line:
                so_delivery_date = ''
                so_reserved_date = ''
                sp_date_done = ''
                last_so_delivery_date = ''
                so_delivery_breakdown = ''
                so_return_date = ''
                so_return_qty = 0
                so_reserved_qty = 0

                invoice_date, invoice_confirmation_date, invoice_number = self._get_invoice_data(cr, items, uid, self.pool.get('stock.picking'), context)

                order_confirm_time = self._get_order_confirm_time(cr, uid, str(line._model), str(line.name), context)

                for move_data in items.move_ids:
                    reserved_date = self._get_order_reserved_date(cr, uid, move_data, context)
                    sp_date_done = move_data.picking_id.date_done

                    if move_data.location_id.usage == 'internal' and move_data.state == 'done':
                        so_delivery_date += str(move_data.date) + ', '
                        last_so_delivery_date = move_data.date
                        so_reserved_date += str(reserved_date) + ', '
                        so_delivery_breakdown += str(move_data.product_qty) + ', '

                    if move_data.location_dest_id.usage == 'internal' and move_data.state == 'done':
                        so_return_date += str(move_data.write_date) + ', '
                        so_return_qty += move_data.product_qty

                for move_data in items.move_ids:
                    if items.product_id == move_data.product_id:
                        unicode_so_reserved_qty = move_data.string_availability_info
                        string_so_reserved_qty = str(unicode_so_reserved_qty)
                        so_reserved_qty = string_so_reserved_qty.replace('(reserved)', '')

                        # so_reserved_qty = move_data.availability if move_data.availability >0 else move_data.reserved_availability

                # ------------------------------------
                for list_data in c_specs:
                    if str(list_data[0]) == str('default_code'):
                        try:
                            decoded_product_code = str(items.product_id.default_code)

                        except:

                            product_code = items.product_id.default_code
                            product_code = product_code.encode('utf-8')

                            decoded_product_code = product_code.decode('utf-8')

                        list_data[4] = decoded_product_code

                    if str(list_data[0]) == str('product_name'):
                        try:
                            decoded_product_name = str(items.product_id.name)

                        except:
                            product_name = items.product_id.name
                            product_name = product_name.encode('utf-8')
                            decoded_product_name = product_name.decode('utf-8')
                        list_data[4] = decoded_product_name

                    if str(list_data[0]) == str('product_category'):
                        try:
                            decoded_product_category = str(items.product_id.categ_id.name)

                        except:
                            product_categ_name = items.product_id.categ_id.name
                            product_categ_name = product_categ_name.encode('utf-8')
                            decoded_product_category = product_categ_name.decode('utf-8')
                        list_data[4] = decoded_product_category

                    if str(list_data[0]) == str('product_uom_qty'):
                        list_data[4] = str(items.product_uom_qty)

                    if str(list_data[0]) == str('so_reserved_qty'):
                        list_data[4] = str(so_reserved_qty)

                    if str(list_data[0]) == str('price_unit'):
                        list_data[4] = str(items.price_unit)

                    if str(list_data[0]) == str('total_unit_price'):
                        list_data[4] = str(items.price_subtotal)

                    if str(list_data[0]) == str('so_delivery_qty'):
                        list_data[4] = str(items.qty_delivered)

                    if str(list_data[0]) == str('so_return_qty'):
                        list_data[4] = str(so_return_qty)
                    if str(list_data[0]) == str('so_return_date'):
                        list_data[4] = str(so_return_date)
                    if str(list_data[0]) == str('so_delivery_date'):
                        list_data[4] = so_delivery_date
                    if str(list_data[0]) == str('last_so_delivery_date'):
                        list_data[4] = last_so_delivery_date
                    if str(list_data[0]) == str('so_reserved_date'):
                        list_data[4] = so_reserved_date
                    if str(list_data[0]) == str('so_delivery_breakdown'):
                        list_data[4] = so_delivery_breakdown
                    if str(list_data[0]) == str('so_remaining_qty'):
                        list_data[4] = str(items.product_uom_qty - items.qty_delivered)
                    if str(list_data[0]) == str('invoice_date'):
                        list_data[4] = invoice_date
                    if str(list_data[0]) == str('invoice_confirmation_date'):
                        list_data[4] = invoice_confirmation_date
                    if str(list_data[0]) == str('order_confirm_time'):
                        list_data[4] = order_confirm_time
                    if str(list_data[0]) == str('invoice_number'):
                        list_data[4] = invoice_number
                    if str(list_data[0]) == str('shipped'):
                        list_data[4] = str(line.shipped)
                    if str(list_data[0]) == str('invoiced_dispatch'):
                        list_data[4] = str(line.invoiced_dispatch)
                    if str(list_data[0]) == str('delivery_dispatch'):
                        list_data[4] = str(line.delivery_dispatch)

                    for p_items in purchase_ids:
                        if items.product_id == p_items.product_id:
                            if str(list_data[0]) == str('po_number'):
                                list_data[4] = str(p_items.order_id.name)
                            if str(list_data[0]) == str('po_confirm_date'):
                                list_data[4] = p_items.order_id.date_approve
                            if str(list_data[0]) == str('po_qty'):
                                list_data[4] = str(p_items.product_qty)
                            if str(list_data[0]) == str('po_create_date'):
                                list_data[4] = str(p_items.create_date)
                            if str(list_data[0]) == str('po_unit_price'):
                                list_data[4] = str(p_items.price_unit)
                            if str(list_data[0]) == str('po_total_price'):
                                list_data[4] = str(p_items.price_subtotal)
                            if str(list_data[0]) == str('po_supplier_name'):
                                list_data[4] = str(p_items.order_id.partner_id.name)
                            if str(list_data[0]) == str('po_receive_date'):
                                po_receive_date = ''
                                for in_move_data in p_items.move_ids:
                                    if in_move_data.state == 'done':
                                        po_receive_date += str(in_move_data.date) + ', '
                                list_data[4] = po_receive_date
                            if str(list_data[0]) == str('last_po_receive_date'):
                                last_po_receive_date = ''
                                for in_move_data in p_items.move_ids:
                                    if in_move_data.state == 'done':
                                        last_po_receive_date = str(in_move_data.date)
                                list_data[4] = last_po_receive_date

                            if str(list_data[0]) == str('po_receive_qty'):
                                po_receive_qty = 0
                                for in_move_data in p_items.move_ids:
                                    if in_move_data.state == 'done':
                                        po_receive_qty += in_move_data.product_qty

                                list_data[4] = str(po_receive_qty)

                # ------------------------

                sold = dict()
                for l in c_specs:
                    sold[l[0]] = l[4]

                order_end_query = "INSERT INTO sale_order_end_to_end (" \
                                  "name, " \
                                  "client_order_ref, " \
                                  "status, " \
                                  "order_date, " \
                                  "date_confirm, " \
                                  "order_confirm_time, " \
                                  "magento_delivery_at, " \
                                  "default_code, " \
                                  "warehouse_name, " \
                                  "customer, " \
                                  "company, " \
                                  "customer_mail, " \
                                  "shipped, " \
                                  "invoiced_dispatch, " \
                                  "delivery_dispatch, " \
                                  "product_name, " \
                                  "product_category, " \
                                  "product_uom_qty, " \
                                  "so_reserved_qty, " \
                                  "price_unit, " \
                                  "total_unit_price, " \
                                  "po_number, " \
                                  "po_create_date, " \
                                  "po_confirm_date, " \
                                  "po_qty, " \
                                  "po_unit_price, " \
                                  "po_total_price, " \
                                  "po_receive_qty, " \
                                  "last_po_receive_date, " \
                                  "po_receive_date, " \
                                  "po_supplier_name, " \
                                  "so_delivery_date, " \
                                  "last_so_delivery_date, " \
                                  "so_reserved_date, " \
                                  "so_delivery_breakdown, " \
                                  "so_delivery_qty, " \
                                  "so_remaining_qty, " \
                                  "so_return_qty, " \
                                  "so_return_date, " \
                                  "invoice_date, " \
                                  "invoice_confirmation_date, " \
                                  "invoice_number) VALUES ($${0}$$, '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', $${7}$$, '{8}', $${9}$$, $${10}$$, '{11}', '{12}', '{13}', '{14}', $${15}$$, '{16}', '{17}', '{18}', '{19}', '{20}', '{21}', '{22}', '{23}', '{24}', '{25}', '{26}', '{27}', '{28}', '{29}', $${30}$$, '{31}', '{32}', '{33}', '{34}', '{35}', '{36}', '{37}', '{38}', '{39}', '{40}', '{41}')".format(sold['name'], sold['client_order_ref'], sold['status'], sold['order_date'], sold['date_confirm'], sold['order_confirm_time'], sold['magento_delivery_at'], sold['default_code'], sold['warehouse_name'], sold['customer'], sold['company'], sold['customer_mail'], sold['shipped'], sold['invoiced_dispatch'], sold['delivery_dispatch'], sold['product_name'], sold['product_category'], sold['product_uom_qty'], sold['so_reserved_qty'], sold['price_unit'], sold['total_unit_price'], sold['po_number'], sold['po_create_date'], sold['po_confirm_date'], sold['po_qty'], sold['po_unit_price'], sold['po_total_price'], sold['po_receive_qty'], sold['last_po_receive_date'], sold['po_receive_date'], sold['po_supplier_name'], sold['so_delivery_date'], sold['last_so_delivery_date'], sold['so_reserved_date'], sold['so_delivery_breakdown'], sold['so_delivery_qty'], sold['so_remaining_qty'], sold['so_return_qty'], sold['so_return_date'], sold['invoice_date'], sold['invoice_confirmation_date'], sold['invoice_number'])

                cr.execute(order_end_query)
                cr.commit()
                # ------------------------

        return True

    def truncate_table(self, cr, uid, table_name, context=None):

        # truncate table
        trunc_query = "TRUNCATE TABLE " + table_name
        cr.execute(trunc_query)
        cr.commit()

        return True

    def chunks(self, l, n):
        """Yield successive n-sized chunks from l."""
        for i in xrange(0, len(l), n):
            yield l[i:i + n]

    def so_end_to_end_seven_months_one(self, cr, uid, ids, context=None):

        # search without cancel
        so_obj = self.pool.get('sale.order')
        order_end_obj = self.pool.get('order.end.to.end.log')

        date_before_180 = datetime.datetime.now() - datetime.timedelta(days=180)
        date_before_180 = date_before_180.strftime('%Y-%m-%d')

        so_list = so_obj.search(cr, uid, [("date_order", ">", date_before_180), ("state", "!=", "cancel")], context=context)
        so_list_chunks = [x for x in self.chunks(so_list, 200)]

        # truncate table
        table_name = "sale_order_end_to_end"
        self.truncate_table(cr, uid, table_name, context=context)

        counter = 0
        data = dict()
        data['name'] = 'so_end_to_end_7_months'
        data['start_time'] = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')

        for so_chunk in so_list_chunks:
            for so in so_obj.browse(cr, uid, so_chunk, context=context):
                try:
                    self.generate_so_report_data(cr, uid, None, so, context)
                    counter += 1
                except:

                    pass

        data['total_inserted'] = counter
        data['end_time'] = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')

        order_end_obj.create(cr, uid, data, context=context)

        self.so_end_to_end_seven_months_two(cr, uid, ids, context=context)

        return True

    def so_end_to_end_seven_months_two(self, cr, uid, ids, context=None):

        # search without cancel
        so_obj = self.pool.get('sale.order')
        order_end_obj = self.pool.get('order.end.to.end.log')

        date_before_180 = datetime.datetime.now() - datetime.timedelta(days=180)
        date_before_180 = date_before_180.strftime('%Y-%m-%d')

        so_list = so_obj.search(cr, uid, [("date_order", "<", date_before_180), ("state", "!=", "cancel")], context=context)
        so_list_chunks = [x for x in self.chunks(so_list, 200)]

        counter = 0
        data = dict()
        data['name'] = 'so_end_to_end_7_months'
        data['start_time'] = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')

        for so_chunk in so_list_chunks:
            for so in so_obj.browse(cr, uid, so_chunk, context=context):
                try:
                    self.generate_so_report_data(cr, uid, None, so, context)
                    counter += 1
                except:
                    pass

        data['total_inserted'] = counter
        data['end_time'] = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        order_end_obj.create(cr, uid, data, context=context)

        return True

    def so_end_to_end_90_days_update_daily(self, cr, uid, ids, context=None):

        # run at 7:00 AM
        # search without cancel
        so_obj = self.pool.get('sale.order')
        order_end_obj = self.pool.get('order.end.to.end.log')

        date_before_90 = datetime.datetime.now() - datetime.timedelta(days=91)
        date_before_90 = date_before_90.strftime('%Y-%m-%d')

        so_list = so_obj.search(cr, uid, [("date_order", ">", date_before_90), ("state", "!=", "cancel")], context=context)
        so_list_chunks = [x for x in self.chunks(so_list, 200)]

        # delete the rows
        delete_qury = "DELETE FROM sale_order_end_to_end WHERE order_date > '{0}'".format(date_before_90)
        cr.execute(delete_qury)
        cr.commit()

        counter = 0
        data = dict()
        data['name'] = 'so_end_to_end_90_days'
        data['start_time'] = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')

        for so_chunk in so_list_chunks:
            for so in so_obj.browse(cr, uid, so_chunk, context=context):
                try:
                    self.generate_so_report_data(cr, uid, None, so, context)
                    counter += 1
                except:
                    pass

        data['total_inserted'] = counter
        data['end_time'] = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        order_end_obj.create(cr, uid, data, context=context)

        return True

    def so_end_to_end_30_days_update_emergency(self, cr, uid, ids, context=None):

        # run from emergency button
        # search without cancel
        so_obj = self.pool.get('sale.order')
        order_end_obj = self.pool.get('order.end.to.end.log')

        date_before_30 = datetime.datetime.now() - datetime.timedelta(days=11)
        date_before_30 = date_before_30.strftime('%Y-%m-%d')

        # delete the rows
        so_list = so_obj.search(cr, uid, [("date_order", ">", date_before_30), ("state", "!=", "cancel")], context=context)

        so_list_chunks = [x for x in self.chunks(so_list, 200)]

        # delete the rows
        delete_qury = "DELETE FROM sale_order_end_to_end WHERE order_date > '{0}'".format(date_before_30)
        cr.execute(delete_qury)
        cr.commit()

        counter = 0
        data = dict()
        data['name'] = 'so_end_to_end_30_days'
        data['start_time'] = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')

        for so_chunk in so_list_chunks:
            for so in so_obj.browse(cr, uid, so_chunk, context=context):
                try:
                    self.generate_so_report_data(cr, uid, None, so, context)
                    counter += 1
                except:
                    pass

        data['total_inserted'] = counter
        data['end_time'] = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        order_end_obj.create(cr, uid, data, context=context)

        return True

    def so_end_to_end(self, cr, uid, ids, context=None):

        so_obj = self.pool.get('sale.order')
        order_end_obj = self.pool.get('order.end.to.end.log')

        date_before_30 = datetime.datetime.now() - datetime.timedelta(days=31)
        date_before_30 = date_before_30.strftime('%Y-%m-%d')

        so_list = so_obj.search(cr, uid, [("date_order", ">", date_before_30)], context=context)

        # truncate table
        """
        trunc_query = "TRUNCATE TABLE sale_order_end_to_end"
        cr.execute(trunc_query)
        cr.commit()
        """
        # table_name = "sale_order_end_to_end"
        # self.truncate_table(cr, uid, table_name, context=context)

        # insert to order_end_to_end_log
        counter = 0
        data = dict()
        data['name'] = 'so_end_to_end_30_days'
        data['start_time'] = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')

        # populate 30 days so data
        for so in so_obj.browse(cr, uid, so_list, context=context):
            try:
                self.generate_so_report_data(cr, uid, None, so, context)
                counter += 1
            except:
                pass

        data['total_inserted'] = counter
        data['end_time'] = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        order_end_obj.create(cr, uid, data, context=context)

        return True

    def so_end_to_end_last_15(self, cr, uid, ids, context=None):

        so_obj = self.pool.get('sale.order')
        order_end_obj = self.pool.get('order.end.to.end.log')

        date_before_15 = datetime.datetime.now() - datetime.timedelta(days=16)
        date_before_15 = date_before_15.strftime('%Y-%m-%d')

        so_list = so_obj.search(cr, uid, [("date_order", ">", date_before_15)], context=context)

        # -------------------------------
        # table_name = "sale_order_end_to_end"
        # self.truncate_table(cr, uid, table_name, context=context)
        # -------------------------------

        # insert to order_end_to_end_log
        counter = 0
        data = dict()
        data['name'] = 'so_end_to_end_15_days'
        data['start_time'] = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')

        for so in so_obj.browse(cr, uid, so_list, context=context):
            try:
                self.generate_so_report_data(cr, uid, None, so, context)
                counter += 1
            except:
                pass

        data['total_inserted'] = counter
        data['end_time'] = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        order_end_obj.create(cr, uid, data, context=context)

        return True

    def so_end_to_end_last_15_1(self, cr, uid, ids, context=None):

        so_obj = self.pool.get('sale.order')
        order_end_obj = self.pool.get('order.end.to.end.log')

        date_before_15 = datetime.datetime.now() - datetime.timedelta(days=16)
        date_before_30 = datetime.datetime.now() - datetime.timedelta(days=31)
        date_before_15 = date_before_15.strftime('%Y-%m-%d')
        date_before_30 = date_before_30.strftime('%Y-%m-%d')

        so_list = so_obj.search(cr, uid, [("date_order", ">", date_before_30), ("date_order", "<", date_before_15)], context=context)

        # insert to order_end_to_end_log
        counter = 0
        data = dict()
        data['name'] = 'so_end_to_end_15_1'
        data['start_time'] = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')

        for so in so_obj.browse(cr, uid, so_list, context=context):
            try:
                self.generate_so_report_data(cr, uid, None, so, context)
                counter += 1
            except:
                pass

        data['total_inserted'] = counter
        data['end_time'] = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        order_end_obj.create(cr, uid, data, context=context)

        return True


class order_end_to_end_log(osv.osv):

    _name = "order.end.to.end.log"

    _columns = {
        'name': fields.char('Scheduler Number'),
        'total_inserted': fields.integer("Total Inserted"),
        'start_time': fields.datetime('Start From'),
        'end_time': fields.datetime('End To'),
    }

    _defaults = {
        'start_time': fields.datetime.now,
    }


class sale_order(osv.osv):
    _inherit = "sale.order"

    def so_end_to_end_seven_months_o(self, cr, uid, ids, context=None):

        soend_obj = self.pool.get('sale.order.end.to.end')
        soend_obj.so_end_to_end_seven_months_one(cr, uid, ids, context=context)

        return True

    def so_end_to_end_seven_months_t(self, cr, uid, ids, context=None):

        soend_obj = self.pool.get('sale.order.end.to.end')
        soend_obj.so_end_to_end_seven_months_two(cr, uid, ids, context=context)

        return True

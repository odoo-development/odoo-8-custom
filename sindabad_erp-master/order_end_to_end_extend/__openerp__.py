
{
    'name': 'Order End to End Process Extends',
    'version': '8.0.0.6.0',
    'author': "Shahjalal Hossain",
    'category': 'Sales',
    'summary': 'SO to Delivery summary',
    'depends': ['sale', 'report_xls', 'sale', 'odoo_magento_connect', 'sindabad_customization'],
    'data': [

        'security/order_e2e_extend_security.xml',
        'security/ir.model.access.csv',
        'views/order_end_to_end_more_menu.xml',
        'views/order_end_to_end.xml',
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}

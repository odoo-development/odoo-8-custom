# Author# S. M. Sazedul Haque 2018-01-22


from openerp.osv import osv
from openerp.report import report_sxw
from datetime import datetime, timedelta

class PurchaseOrderConfirmReport(report_sxw.rml_parse):

    def __init__(self, cr, uid, name, context):
        super(PurchaseOrderConfirmReport, self).__init__(cr, uid, name, context)
        self.localcontext.update({
            'getPOConfirmList': self._get_po_confirm_list,
        })

        self.context = context


    def _get_po_confirm_list(self):

        day = 30
        d_days_ago = datetime.now() - timedelta(days=day)
        po_pool = self.pool['purchase.order']
        po_ids = po_pool.search(self.cr, self.uid,
                                        [ ('state', '=', 'approved'), ('shipped', '=', False), ('date_order', '<', d_days_ago.strftime("%Y-%m-%d 00:00:00"))])

        po_obj = po_pool.browse(self.cr, self.uid, po_ids)

        return po_obj


class purchase_order_confirm_report(osv.AbstractModel):
    _name = 'report.po_report_mail.purchase_order_confirm_report'
    _inherit = 'report.abstract_report'
    _template = 'po_report_mail.purchase_order_confirm_report'
    _wrapped_report_class = PurchaseOrderConfirmReport

# ---------MINI----LIST-----------#
class PurchaseOrderConfirmMiniReport(report_sxw.rml_parse):

    def __init__(self, cr, uid, name, context):
        super(PurchaseOrderConfirmMiniReport, self).__init__(cr, uid, name, context)
        self.localcontext.update({
            'getPOConfirmMiniList': self._get_po_confirm_mini_list,
        })

        self.context = context


    def _get_po_confirm_mini_list(self):

        day = 10
        d_days_ago = datetime.now() - timedelta(days=day)
        po_pool = self.pool['purchase.order']
        po_ids = po_pool.search(self.cr, self.uid,
                                        [ ('state', '=', 'approved'), ('shipped', '=', False), ('date_order', '<', d_days_ago.strftime("%Y-%m-%d 00:00:00"))])

        po_obj = po_pool.browse(self.cr, self.uid, po_ids)

        return po_obj


class purchase_order_confirm_mini_report(osv.AbstractModel):
    _name = 'report.po_report_mail.purchase_order_confirm_mini_report'
    _inherit = 'report.abstract_report'
    _template = 'po_report_mail.purchase_order_confirm_mini_report'
    _wrapped_report_class = PurchaseOrderConfirmMiniReport

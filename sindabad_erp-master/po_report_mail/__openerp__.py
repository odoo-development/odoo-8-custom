# -*- coding: utf-8 -*-
{
    'name': "Purchase Order Report Mail",

    'summary': """
        Purchase Order Report Mail""",

    'description': """
        Purchase Order Report Mail
    """,

    'author': "S. M. Sazedul Haque - Zero Gravity Ventures Ltd",
    'website': "http://zerogravity.com.bd/",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/openerp/addons/base/module/module_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base', 'report', 'purchase'],

    # always loaded
    'data': [
        'po_draft_templates.xml',
        'po_confirm_templates.xml',
        'po_received_templates.xml',
        'po_cancel_templates.xml',
        'product_templates.xml',
    ],
}
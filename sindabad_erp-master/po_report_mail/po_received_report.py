# Author# S. M. Sazedul Haque 2018-01-22


from openerp.osv import osv
from openerp.report import report_sxw
from datetime import datetime, timedelta

class PurchaseOrderReceivedReport(report_sxw.rml_parse):

    def __init__(self, cr, uid, name, context):
        super(PurchaseOrderReceivedReport, self).__init__(cr, uid, name, context)
        self.localcontext.update({
            'getPOReceivedList': self._get_po_received_list,
        })

        self.context = context


    def _get_po_received_list(self):

        day = 30
        d_days_ago = datetime.now() - timedelta(days=day)
        po_pool = self.pool['purchase.order']
        po_ids = po_pool.search(self.cr, self.uid,
                                        [ ('state', '=', 'approved'), ('shipped', '=', True), ('date_order', '<', d_days_ago.strftime("%Y-%m-%d 00:00:00"))])

        po_obj = po_pool.browse(self.cr, self.uid, po_ids)

        return po_obj





class purchase_order_received_report(osv.AbstractModel):
    _name = 'report.po_report_mail.purchase_order_received_report'
    _inherit = 'report.abstract_report'
    _template = 'po_report_mail.purchase_order_received_report'
    _wrapped_report_class = PurchaseOrderReceivedReport

# ---------MINI----LIST-----------#
class PurchaseOrderReceivedMiniReport(report_sxw.rml_parse):

    def __init__(self, cr, uid, name, context):
        super(PurchaseOrderReceivedMiniReport, self).__init__(cr, uid, name, context)
        self.localcontext.update({
            'getPOReceivedMiniList': self._get_po_received_mini_list,
        })

        self.context = context


    def _get_po_received_mini_list(self):

        day = 10
        d_days_ago = datetime.now() - timedelta(days=day)
        po_pool = self.pool['purchase.order']
        po_ids = po_pool.search(self.cr, self.uid,
                                        [ ('state', '=', 'approved'), ('shipped', '=', True), ('date_order', '<', d_days_ago.strftime("%Y-%m-%d 00:00:00"))])

        po_obj = po_pool.browse(self.cr, self.uid, po_ids)

        return po_obj





class purchase_order_received_mini_report(osv.AbstractModel):
    _name = 'report.po_report_mail.purchase_order_received_mini_report'
    _inherit = 'report.abstract_report'
    _template = 'po_report_mail.purchase_order_received_mini_report'
    _wrapped_report_class = PurchaseOrderReceivedMiniReport

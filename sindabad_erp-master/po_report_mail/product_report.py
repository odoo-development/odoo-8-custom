# Author# S. M. Sazedul Haque 2018-01-22


from openerp.osv import osv
from openerp.report import report_sxw
from datetime import datetime, timedelta

# ---------Need-TO-BUY-PRODUCT-LIST-----------#
class ProductNeedToBuy(report_sxw.rml_parse):

    def __init__(self, cr, uid, name, context):
        super(ProductNeedToBuy, self).__init__(cr, uid, name, context)
        self.localcontext.update({
            'getPOMiniList': self._get_po_mini_list,
        })

        self.context = context


    def _get_po_mini_list(self):

        day = 10
        d_days_ago = datetime.now() - timedelta(days=day)
        product_pool = self.pool['product.product']
        product_ids = product_pool.search(self.cr, self.uid,
                                        [ ('state', '=', 'draft'), ('date_order', '<', d_days_ago.strftime("%Y-%m-%d 00:00:00"))])

        product_obj = product_pool.browse(self.cr, self.uid, product_ids)

        return product_obj





class product_need_to_buy(osv.AbstractModel):
    _name = 'report.po_report_mail.product_need_to_buy'
    _inherit = 'report.abstract_report'
    _template = 'po_report_mail.product_need_to_buy'
    _wrapped_report_class = ProductNeedToBuy

# ---------MINI----LIST-----------#
class ProductList60Days(report_sxw.rml_parse):

    def __init__(self, cr, uid, name, context):
        super(ProductList60Days, self).__init__(cr, uid, name, context)
        self.localcontext.update({
            'get60DaysProductList': self._get_60days_product_list,
        })

        self.context = context


    def _get_60days_product_list(self):

        day = 60
        d_days_ago = datetime.now() - timedelta(days=day)
        self.cr.execute("SELECT DISTINCT  \
                                product.name_template,\
                                product.default_code,\
                                product.x_products_location,\
                                product.list_price,\
                                product.stnd_price,\
                                SUM(sq.qty) AS total_qty, \
                                product.prod_uom,\
                                product.ean13 \
                            FROM \
                               stock_quant sq\
                            INNER JOIN \
                               stock_location sl ON sl.id = sq.location_id \
                            INNER JOIN \
                               product_product product ON product.id = sq.product_id \
                            WHERE \
                               sl.usage='internal'  AND sq.in_date <=\'%s\' \
                            GROUP BY \
                               product.id\
                            ORDER BY\
                                total_qty DESC" % d_days_ago.strftime('%Y-%m-%d 00:00:00'))
                   # "where sq.qty > 0  and sq.in_date <=%s", (tuple(docs.product_categ.ids), d_days_ago))

        quant_ids = self.cr.fetchall()

        print 'quant_ids=', len(quant_ids) ,'*'*15

        return quant_ids





class product_list_60_days(osv.AbstractModel):
    _name = 'report.po_report_mail.product_list_60_days'
    _inherit = 'report.abstract_report'
    _template = 'po_report_mail.product_list_60_days'
    _wrapped_report_class = ProductList60Days

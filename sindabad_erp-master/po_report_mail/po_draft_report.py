# Author# S. M. Sazedul Haque 2018-01-22


from openerp.osv import osv
from openerp.report import report_sxw
from datetime import datetime, timedelta

class PurchaseOrderList(report_sxw.rml_parse):

    def __init__(self, cr, uid, name, context):
        super(PurchaseOrderList, self).__init__(cr, uid, name, context)
        self.localcontext.update({
            'getPOList': self._get_po_list,
        })

        self.context = context


    def _get_po_list(self):

        day = 30
        d_days_ago = datetime.now() - timedelta(days=day)
        po_pool = self.pool['purchase.order']
        po_ids = po_pool.search(self.cr, self.uid,
                                        [ ('state', '=', 'draft'), ('date_order', '<', d_days_ago.strftime("%Y-%m-%d 00:00:00"))])

        po_obj = po_pool.browse(self.cr, self.uid, po_ids)

        return po_obj





class purchase_order_draft_report(osv.AbstractModel):
    _name = 'report.po_report_mail.purchase_order_draft_report'
    _inherit = 'report.abstract_report'
    _template = 'po_report_mail.purchase_order_draft_report'
    _wrapped_report_class = PurchaseOrderList

# ---------MINI----LIST-----------#
class PurchaseOrderMiniList(report_sxw.rml_parse):

    def __init__(self, cr, uid, name, context):
        super(PurchaseOrderMiniList, self).__init__(cr, uid, name, context)
        self.localcontext.update({
            'getPOMiniList': self._get_po_mini_list,
        })

        self.context = context


    def _get_po_mini_list(self):

        day = 10
        d_days_ago = datetime.now() - timedelta(days=day)
        po_pool = self.pool['purchase.order']
        po_ids = po_pool.search(self.cr, self.uid,
                                        [ ('state', '=', 'draft'), ('date_order', '<', d_days_ago.strftime("%Y-%m-%d 00:00:00"))])

        po_obj = po_pool.browse(self.cr, self.uid, po_ids)

        return po_obj





class purchase_order_mini_list_report(osv.AbstractModel):
    _name = 'report.po_report_mail.purchase_order_mini_list_report'
    _inherit = 'report.abstract_report'
    _template = 'po_report_mail.purchase_order_mini_list_report'
    _wrapped_report_class = PurchaseOrderMiniList

# Mufti Muntasir Ahmed

from openerp.osv import fields, osv


class hr_payslip(osv.osv):
    _inherit = 'hr.payslip'


    def _get_department_name(self, cr, uid, ids, field_name, arg, context=None):
        res = {}
        for details in self.browse(cr, uid, ids, context=context):
            try:
                res[details.id] = str(details.employee_id.department_id)
            except:
                res[details.id] = ''
        return res

    def _get_employee_id(self, cr, uid, ids, field_name, arg, context=None):
        res = {}
        for details in self.browse(cr, uid, ids, context=context):
            try:
                res[details.id] = str(details.employee_id.x_emid_sin)
            except:
                res[details.id] = ''
        return res

    _columns = {
        'dept_name': fields.function(_get_department_name, type='char', string="Department",store=True),
        'emp_id': fields.function(_get_employee_id, type='char', string="Employee ID",store=True),
    }

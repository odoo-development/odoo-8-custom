
from openerp.addons.web import http
from openerp.addons.web.http import request

class org_chart_dept(http.Controller):
    @http.route(["/sales/chart/"], type='http', auth="public", website=True)
    def view(self, message=False, **post):
        values = {
        }
        return request.website.render('telecom_business_forecasting.sales_chart', values)


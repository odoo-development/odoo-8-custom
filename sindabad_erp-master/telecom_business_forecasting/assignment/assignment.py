from openerp.osv import fields, osv
from openerp import api
from openerp.tools.translate import _
import datetime

class telecom_user_assignment(osv.osv):

    def _name_get_fnc(self, cr, uid, ids,prop, unknow_none, context=None):
        res = self.name_get(cr, uid, ids, context=context)
        return dict(res)

    def _get_sales_value(self, cr, uid, ids, name, arg, context=None):
        res = False
        res = {}
        asignment_wise_value = {}

        relation_dict = {}
        for dept in self.browse(cr, uid, self.search(cr, uid, [], context=context), context=context):
            tmp_list = []
            asignment_wise_value[dept.id] = [0, dept.user_id.id]
            cr.execute(
                "with recursive rel_tree as (select id, name, parent_id, 1 as level, array[id] as path_info from telecom_user_assignment where parent_id=%s union all select c.id, rpad(' ', p.level * 2) || c.name, c.parent_id, p.level + 1, p.path_info||c.id from telecom_user_assignment c join rel_tree p on c.parent_id = p.id )select id, name from rel_tree order by path_info",
                ([dept.id]))
            for item in cr.fetchall():
                tmp_list.append(item[0])
            relation_dict[dept.id] = tmp_list

        # cr.execute(
        #         "select sum(amount_total) as total,custom_salesperson from sale_order where (state = 'done' or state = 'progress')  group by custom_salesperson",
        #        )

        cr.execute("select sum(amount_total) as total,custom_salesperson from sale_order where (state = 'done' or state = 'progress')  group by custom_salesperson")

        all_data = cr.fetchall()

        user_dict = {}
        user_list = []

        for items in all_data:
            user_dict[items[1]] = items[0]
            user_list.append(items[1])

        for k, v in asignment_wise_value.iteritems():
            tmp_list = v
            tmp_value = tmp_list[0]
            if tmp_list[1] in user_list:
                tmp_value += user_dict[tmp_list[1]]

            asignment_wise_value[k] = [tmp_value, tmp_list[1]]

        for dept in self.browse(cr, uid, self.search(cr, uid, [], context=context), context=context):

            sale_value = 0

            for k, v in relation_dict.iteritems():
                if k == dept.id:
                    for l_items in v:
                        sale_value = sale_value + asignment_wise_value[l_items][0]

            if dept.user_id.id is not False:
                user_id = dept.user_id.id
                if user_id in user_list:
                    sale_value = user_dict[user_id]

            res[dept.id] = sale_value
        return res

    _name = "telecom.user.assignment"
    _columns = {
        'name': fields.char('Name'),
        'user_id': fields.many2one('res.users', 'User'),
        'complete_name': fields.function(_name_get_fnc, type="char", string='Name'),
        'parent_id': fields.many2one('telecom.user.assignment', 'Parent', select=True),
        'child_ids': fields.one2many('telecom.user.assignment', 'parent_id', 'Child '),
        'manager_id': fields.many2one('hr.employee', 'Manager'),
        'sales_value': fields.function(_get_sales_value, string='Sales Value', type='char'),

        # 'assign_parent': fields.boolean('Assign Parent', compute="_check_assign_parent"),
        # 'assign_user': fields.boolean('Assign User', compute="_check_assign_user"),

    }



    def _check_recursion(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        level = 100
        while len(ids):
            cr.execute('select distinct parent_id from telecom_user_assignment where id IN %s',(tuple(ids),))
            ids = filter(None, map(lambda x:x[0], cr.fetchall()))
            if not level:
                return False
            level -= 1
        return True

    _constraints = [
        (_check_recursion, 'Error! You cannot create recursive users.', ['parent_id'])
    ]

    def name_get(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        if not ids:
            return []
        if isinstance(ids, (int, long)):
            ids = [ids]
        reads = self.read(cr, uid, ids, ['name','parent_id','user_id'], context=context)
        res = []
        for record in reads:
            name = record['name']
            if record['parent_id']:
                name = record['parent_id'][1]+' / '+name

            res.append((record['id'], name))
        return res

    def ids_get(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        if not ids:
            return []
        if isinstance(ids, (int, long)):
            ids = [ids]
        reads = self.read(cr, uid, ids, ['name','parent_id','user_id'], context=context)
        res = []
        for record in reads:
            name = record['name']
            if record['parent_id']:
                name = record['parent_id'][1]+' / '+name
            elif record['user_id']:
                name = name + ' / ' + record['user_id'][1]
            res.append((record['id'], name))
        return res

    def sales_collection_report(self, cr, uid, start_date=None, end_date=None, context=None):


        result_list =[]

        asignment_wise_value = {}

        ## Start finding the nth childer list
        relation_dict ={}
        for dept in self.browse(cr, uid, self.search(cr, uid, [], context=context), context=context):
            tmp_list = []
            asignment_wise_value[dept.id]=[0,dept.user_id.id]
            cr.execute("with recursive rel_tree as (select id, name, parent_id, 1 as level, array[id] as path_info from telecom_user_assignment where parent_id=%s union all select c.id, rpad(' ', p.level * 2) || c.name, c.parent_id, p.level + 1, p.path_info||c.id from telecom_user_assignment c join rel_tree p on c.parent_id = p.id )select id, name from rel_tree order by path_info",([dept.id]))
            for item in  cr.fetchall():
                tmp_list.append(item[0])
            relation_dict[dept.id]=tmp_list
        ## Ending finding the nth childer list


        today_date = datetime.date.today()

        ## Get All sales data User Wise
        if start_date is not None and end_date is not None:
            cr.execute(
                "select sum(amount_total) as total,custom_salesperson from sale_order where (state = 'done' or state = 'progress') and date_confirm>=%s and date_confirm<=%s group by custom_salesperson",([start_date,end_date]))
        else:
            cr.execute("select sum(amount_total) as total,custom_salesperson from sale_order where (state = 'done' or state = 'progress') and date_confirm=%s group by custom_salesperson",([today_date]))



        all_data = cr.fetchall()

        user_dict = {}
        user_list = []

        for items in all_data:
            user_dict[items[1]] = items[0]
            user_list.append(items[1])



        ## Ends here


        ## update Dictionary Value

        for k,v in asignment_wise_value.iteritems():
            tmp_list = v
            tmp_value = tmp_list[0]
            if tmp_list[1] in user_list:
                tmp_value += user_dict[tmp_list[1]]


            asignment_wise_value[k]= [tmp_value,tmp_list[1]]



        ## Ends Hese update Dictionary Value




        for dept in self.browse(cr, uid, self.search(cr, uid, [], context=context), context=context):

            sale_value = 0
            image_url = "/web/binary/image?model=hr.employee&amp;field=image_small&amp;id="
            if dept.manager_id.id is not False:
                manager_name = dept.manager_id.name
                if dept.user_id.id is not False:
                    image_url = image_url + str('1')
                else:
                    image_url = image_url + str(dept.manager_id.id)
            else:
                manager_name = ''
                image_url = image_url + str('1')
            name = dept.name


            for k,v in relation_dict.iteritems():
                if k == dept.id:
                    for l_items in v :
                        sale_value = sale_value + asignment_wise_value[l_items][0]


            










            if dept.user_id.id is not False:
                user_id = dept.user_id.id



                name = dept.user_id.name
                manager_name = dept.user_id.name

                if user_id in user_list:
                    name = str(user_dict[user_id])

                    sale_value = user_dict[user_id]




                if dept.parent_id.id is not False:
                    result_list.append({ 'id': dept.id,'pid': dept.parent_id.id, 'name': name ,'sales':sale_value, 'manager':manager_name,'img': image_url})
                else:
                    result_list.append({'id': dept.id, 'name': name,'manager':manager_name,'sales':sale_value, 'img':image_url})
            else:
                manager_name = manager_name + ', '+ name

                if dept.parent_id.id is not False:
                    result_list.append({ 'id': dept.id,'pid': dept.parent_id.id, 'name': name ,'sales':sale_value, 'manager':manager_name,'img': image_url})
                else:
                    result_list.append({'id': dept.id, 'name': name,'manager':manager_name,'sales':sale_value, 'img':image_url})



        return result_list





    # @api.depends('parent_id')
    # def _check_assign_parent(self):
    #
    #     if self.parent_id:
    #         self.assign_parent = True
    #     else:
    #         self.assign_parent = False
    #
    #     return True
    #
    #
    # @api.depends('user_id')
    # def _check_assign_user(self):
    #
    #     if self.user_id:
    #         self.assign_user = True
    #     else:
    #         self.assign_user = False
    #
    #     return True





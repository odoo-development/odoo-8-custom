{
    'name': 'Telecom Business Forecasting',
    'version': '1.0',
    'category': 'Sale',
    'author': 'Shuvarthi Dhar',
    'summary': 'Telecom Business Forecasting',
    'description': 'Telecom Business Forecasting',
    'depends': ['base', 'sale','stock','account'],
    'data': [

        'security/telecom_business_forecasting_access.xml',
        'security/ir.model.access.csv',

        'assignment/views/management_assignment_view.xml',
        'assignment/views/dsr_assignment_view.xml',

        'assignment/views/organogram_chart_menu.xml',
        'assignment/views/organogram_chart.xml',

    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}

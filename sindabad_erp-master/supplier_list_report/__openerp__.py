{
    'name': 'Supplier List Report Module',
    'version': '8.0.1',
    'author': "Mufti and Rocky",
    'category': 'Accounting',
    'summary': 'Supplier list report',
    'depends': ['base', 'account', 'report_xls'],
    'data': [

        'wizard/supplier_list_report_filter.xml',

        'report/supplier_list_report_xls.xml',
    ],
}

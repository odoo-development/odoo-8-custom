from openerp import models, api


class SupplierList(models.Model):
    _inherit = 'res.partner'

    @api.model
    def _report_xls_fields_supplier(self):

        return [
            'name', 'total_balance', 'total_debit', 'total_credit', 'balance'

        ]

    # Change/Add Template entries
    @api.model
    def _report_xls_template_supplier(self):

        return {}

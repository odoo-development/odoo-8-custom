# -*- coding: utf-8 -*-
#################################################################################
#
#    Copyright (c) 2017-Present Webkul Software Pvt. Ltd. (<https://webkul.com/>)
#
#################################################################################

{
    'name': 'Base Partner Fixes',
    'version': '1.0.1',
    'category': 'Base',
    'author': 'Webkul Software Pvt. Ltd.',
    'website': 'http://www.webkul.com',
    'summary': 'Some fixes on OpenERP Base module',
    'description': """

Some basic Fixes in order to work Partner as Expected.
======================================================
 
 * Issue 1 - Base Partner can`t have Multi Contacts 
   FIX - This module will enable the "Contact" tab for individual partner as well as company.
 
 * Issue 2 - Empty line(s) in partner`s address when using the method display_address in reports.
   FIX - Removed this empty line.

    """,
    'depends': ['base'],
    'data': ['partner_view.xml'],
    'installable': True,
    'active': False,
}

{
    'name': 'Bulk Order Status Synchronization',
    'version': '1.0',
    'category': 'Order',
    'author': 'Mufti Muntasir Ahmed',
    'summary': 'Order Status Synchronization',
    'description': 'Order Status Synchronization',
    'depends': [
        'sale','stock'
    ],
    'data': [

    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}
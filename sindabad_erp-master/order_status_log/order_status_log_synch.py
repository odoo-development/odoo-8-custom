from datetime import datetime, timedelta
import time
import time, datetime, json
import requests
import re
from datetime import date
from openerp import SUPERUSER_ID
from openerp.osv import fields, osv
from openerp.tools.translate import _
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT
import openerp.addons.decimal_precision as dp
from openerp import workflow
from openerp import api
from ..base.res.res_partner import format_address
from openerp.exceptions import Warning, except_orm
import dateutil.parser
import logging
from openerp.http import request
from ..odoo_to_magento_api_connect.api_connect import get_magento_token, submit_request




XMLRPC_API = '/index.php/api/xmlrpc'

_logger = logging.getLogger(__name__)


class order_status_synch(osv.osv):
    _name = "order.status.synch.log"

    _columns = {
        'odoo_order_id': fields.integer(string="Odoo Order ID"),
        'magento_id': fields.char('Magento No'),
        'order_state': fields.char('Order Status'),
        'state_time': fields.char('State Date and Time'),
        'sent_to_megento_flag': fields.boolean('Sent To Magento'),

    }



    def bulk_order_status_synch_log(self,cr,uid,context=None):


        if context is None:
            context = {
                'lang': 'en_US',
                'params': {'action': 404},
                'tz': 'Asia/Dhaka',
                'uid': uid
            }



        processing_list=[]
        p_dis_list=[]
        dis_list=[]
        p_del_list=[]
        del_list=[]
        complt_list=[]
        point_gain_list=[]

        status_log = self.pool.get('order.status.synch.log')





        statu_log_ids = status_log.search(cr, uid, [('sent_to_megento_flag', '=', False)])

        status_log_obj = status_log.browse(cr, uid, statu_log_ids, context=context)

        for st_items in status_log_obj:
            tmp_dict={
                'orderId':st_items.magento_id,
                'comment':'',
                'createdAt':st_items.state_time,
            }
            if str(st_items.order_state) == str('processing'):
                tmp_dict['status']='processing'
                processing_list.append(tmp_dict)

            if str(st_items.order_state) == str('partially_dispatched'):
                tmp_dict['status']='partially_dispatched'
                p_dis_list.append(tmp_dict)

            if str(st_items.order_state) == str('dispatched'):
                tmp_dict['status']='dispatched' ## Later it will reflect live synch, if we required changes
                dis_list.append(tmp_dict)

            if str(st_items.order_state) == str('partially_delivered'):
                tmp_dict['status']='partially_delivered'
                p_del_list.append(tmp_dict)

            if str(st_items.order_state) == str('delivered'):
                tmp_dict['status']='delivered'
                del_list.append(tmp_dict)

            if str(st_items.order_state) == str('complete'):
                tmp_dict['status']='complete'
                complt_list.append(tmp_dict)

            if str(st_items.order_state) == str('point_gain'):
                tmp_dict['status']='point_gain'
                point_gain_list.append(tmp_dict)

            tmp_dict={}



        final_list= processing_list + p_dis_list + dis_list + p_del_list + del_list + complt_list + point_gain_list

        if len(final_list) >0:

            magento_style_data = {
                      "statusData": final_list
                    }

            try:

                so_obj = self.pool.get('sale.order')
                sale_order_ids = so_obj.search(cr, uid, [('client_order_ref', '!=', None)], limit=3)
                token, url_root = get_magento_token(self, cr, uid, sale_order_ids)

                if token:
                    token = token.replace('"', "")
                    headers = {'Authorization': token, 'Content-Type': 'application/json'}

                    post_url = url_root + "/index.php/rest/V1/odoomagentoconnect/OrderStatusBulk"
                    data = json.dumps(magento_style_data)

                    post_resp = requests.post(post_url, data=data, headers=headers)

                    cr.execute("UPDATE order_status_synch_log SET sent_to_megento_flag=TRUE WHERE id in %s", (tuple(statu_log_ids),))
                    cr.commit()
            except:
                pass
        count =1


        for p_items in processing_list:

            magento_no = str(p_items.get('orderId'))
            try:

                ids = self.pool.get('sale.order').search(cr,uid,[('client_order_ref', '=',magento_no)],limit=1)
                so = self.pool.get('sale.order').browse(cr, uid, ids, context=context)

                parent = so.partner_invoice_id
                inv_address_mobile=''
                inv_address_phone=''


                # for i in range(5):
                #     if len(parent.parent_id) == 1:
                #         parent = parent.parent_id
                #     else:
                #         parent = parent
                #
                #         break

                inv_address_mobile = parent.mobile
                inv_address_phone = parent.phone

                if len(inv_address_mobile) >10:
                    inv_address_phone=inv_address_mobile



                """
                phone_number = str(so.partner_id.mobile) if so.partner_id.mobile else company_phone
                if not phone_number:
                    phone_number = str(so.partner_id.phone)
                """
                so_placed_date = datetime.datetime.now().strftime('%A, %d-%m-%Y')
                payment_type = str(so.note).split(":-")[-1]
                email = str(so.partner_id.email)

                sms_text = ""
                if len(inv_address_phone)>10:
                    sms_text = "Your order #{0} has been confirmed. You will get updates once items has been shipped.".format(
                        str(so.client_order_ref))

                    self.pool.get("send.sms.on.demand").send_sms_on_demand(cr, uid, ids, sms_text, inv_address_phone,
                                                                           str(parent.name), context=context)
                try:
                    # send email
                    if not email[0].isdigit():

                        if re.search("^(\d+)@sindabad.com$", email) :
                            sin_email_match = re.search("^(\d+)@sindabad.com$", email)
                        elif re.search("^sin(\d+)@sindabad.com$", email):
                            sin_email_match = re.search("^sin(\d+)@sindabad.com$", email)
                        elif re.search("@sindabadretail.com$", email):
                            sin_email_match = re.search("@sindabadretail.com$", email)
                        else:
                            sin_email_match = None

                        if sin_email_match is None:
                            e_log_obj = self.pool.get('send.email.on.demand.log')

                            email_data = {
                                'mail_text': sms_text,
                                'model': "sale.order",
                                'so_id': ids[0],
                                'email': email
                            }

                            email_log_id = e_log_obj.create(cr, uid, email_data, context=context)
                            tmp_data = e_log_obj.browse(cr, uid, email_log_id, context=context)

                            # send email
                            email_template_obj = self.pool.get('email.template')
                            # template_ids = email_template_obj.search(cr, uid, [('name', '=', 'SO process mail')], context=context)

                            template_ids = email_template_obj.search(cr, uid, [('name', '=', 'Order Confirmed')], context=context)

                            email_template_obj.send_mail(cr, uid, template_ids[0], ids[0], force_send=True, context=context)

                except:
                    pass
            except:
                pass
            try:
                context['status'] = 'processing'
                self.offer_notification_api(cr, uid, magento_no, context)
            except:
                pass

        return True

    def offer_notification_api(self, cr, uid, ids, context=None):
        uid = 1
        try:
            so_obj = self.pool.get("sale.order")
            so_list = so_obj.search(cr, uid, [('client_order_ref', '=', str(ids))], context=context)
            so = so_obj.browse(cr, uid, so_list, context=context)
            status = context['status']

            headers = {'Authorization': 'jl8t%4Re1C74r7hjWQZv#$Zl9v7dYuz', 'Content-Type': 'application/json'}

            obj_magento = self.pool.get('magento.configure').browse(cr, uid, [1])

            if obj_magento.name == 'https://sindabad.com':

                get_url = "https://offers.sindabad.com/api/customer-notification?" \
                          "customer_id=" + str(so.partner_id.email) + "&order_id=" + \
                          str(so.client_order_ref) + "&order_status=" + str(status) + "&type=order"
            else:
                get_url = "https://offersdev.sindabad.com/api/customer-notification?" \
                          "customer_id=" + str(so.partner_id.email) + "&order_id=" + \
                          str(so.client_order_ref) + "&order_status=" + str(status) + "&type=order"
            # 1323
            # get_url = "https://offers.sindabad.com/api/customer-notification?" \
            #           "customer_id=1323&order_id=" + \
            #           str(1000179140) + "&order_status=" + str('delivered') + "&type=order"

            get_resp = requests.get(get_url, headers=headers)
            order_data = json.loads(get_resp.text)
        except:
            pass
        return True

# if (preg_match("/(\d+)@sindabad.com$/i", $customerEmail, $match)
#                         || preg_match("/^sin(\d+)@sindabad.com$/i", $customerEmail, $match)
#                         || preg_match("/@sindabadretail.com$/i", $customerEmail, $match)
#                     )
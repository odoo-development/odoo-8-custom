# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from datetime import datetime, timedelta
import time
from openerp.osv import fields, osv
from openerp.tools.translate import _
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT
import openerp.addons.decimal_precision as dp


class sale_order_line(osv.osv):
    _inherit = "sale.order.line"
    _columns = {    
        'default_code': fields.related('product_id', 'product_tmpl_id', 'default_code', store=False,readonly=True, string='SKU',type='char'),
        'property_stock_inventory': fields.related('product_id', 'product_tmpl_id', 'property_stock_inventory', store=False,readonly=True,type='many2one',
            relation='stock.location', string='Inventory Location'),
        'loc_row': fields.related('product_id','product_tmpl_id', 'loc_row', store=False,readonly=True, string='Supplier',type='char'),
        'loc_rack': fields.related('product_id', 'product_tmpl_id', 'loc_rack', store=False,readonly=True, string='Rack',type='char'),
        'incoming_qty': fields.related('product_id','incoming_qty', store=False,readonly=True, string='Incoming Quantity'),
        'outgoing_qty': fields.related('product_id','outgoing_qty', store=False,readonly=True, string='Outgoing Quantity'),
        'qty_available': fields.related('product_id','qty_available', store=False,readonly=True, string='Quantity on Hand'),
    }
    def product_id_change_with_wh(self, cr, uid, ids, pricelist, product, qty=0,
            uom=False, qty_uos=0, uos=False, name='', partner_id=False,
            lang=False, update_tax=True, date_order=False, packaging=False, fiscal_position=False, flag=False, warehouse_id=False, context=None):
        context = context or {}
        product_obj = self.pool.get('product.template')
        warning = {}
        #UoM False due to hack which makes sure uom changes price, ... in product_id_change
        res = self.product_id_change(cr, uid, ids, pricelist, product, qty=qty,
            uom=False, qty_uos=qty_uos, uos=uos, name=name, partner_id=partner_id,
            lang=lang, update_tax=update_tax, date_order=date_order, packaging=packaging, fiscal_position=fiscal_position, flag=flag, context=context)

        product_browse = self.pool.get('product.product').browse(cr,uid,product).product_tmpl_id
        tmpl_obj = product_obj.browse(cr, uid, product_browse.id, context=context)
        
        res['value'].update({'default_code': tmpl_obj.default_code})
        res['value'].update({'property_stock_inventory': tmpl_obj.property_stock_inventory})
        res['value'].update({'loc_row': tmpl_obj.loc_row})
        res['value'].update({'loc_rack': tmpl_obj.loc_rack})
        res['value'].update({'incoming_qty': self.pool.get('product.product').browse(cr,uid,product).incoming_qty})
        res['value'].update({'outgoing_qty': self.pool.get('product.product').browse(cr,uid,product).outgoing_qty})    
        res['value'].update({'qty_available': self.pool.get('product.product').browse(cr,uid,product).qty_available})

        
        #update of warning messages
        
        return res
       
        



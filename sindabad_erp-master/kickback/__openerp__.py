# Author Mufti Muntasir Ahmed 11/04/2018


{
    'name': 'Kickback',
    'version': '8.0.0',
    'category': 'Sales',
    'description': """
Generate Picking List from Delivery Challan / Sales Order   
==============================================================

""",
    'author': 'Mufti Muntasir Ahmed',
    'depends': ['sale', 'purchase'],
    'data': [
        'wizard/po_wizard.xml',
        'po_view.xml',
    ],

    'installable': True,
    'auto_install': False,
}
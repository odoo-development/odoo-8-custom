import logging
from datetime import datetime
from dateutil.relativedelta import relativedelta
from operator import itemgetter
import time

import openerp
from openerp import SUPERUSER_ID, api
from openerp import tools
from openerp.osv import fields, osv, expression
from openerp.tools.translate import _
from openerp.tools.float_utils import float_round as round

from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT, DATETIME_FORMATS_MAP
from openerp.tools.float_utils import float_compare
import openerp.addons.decimal_precision as dp

_logger = logging.getLogger(__name__)


class kickback(osv.osv):
    _name = "kickback"
    _description= "Kickback"

    _columns = {

        'name':fields.char('Name'),
        'type': fields.selection([
            ('sale', 'Sale'),
            ('purchase', 'Purchase'),

        ], 'Type'),

        'state': fields.selection([
            ('pending', 'Waiting for Picking'),
            ('done', 'Done'),
            ('cancel', 'Cancelled'),

        ], 'Status', readonly=True, copy=False, help="Gives the status of the quotation or sales order", select=True),

        'kickback_date': fields.datetime('Date', required=True),
        'sale_id': fields.many2one('sale.order', 'Sales Order'),
        'purchase_id': fields.many2one('purchase.order', 'Purchase Order'),
        'total_amount': fields.float('Total Amount'),
        'kickback_line': fields.one2many('kickback.line', 'kickback_id', 'Kickback List Lines', required=True),


    }




    def default_get(self, cr, uid, fields, context=None):

        if context is None:
            context = {}
        res = super(kickback, self).default_get(cr, uid, fields, context=context)

        order_obj = None
        purchase_order_id = None
        sale_order_id = None
        # if context['sale']==True:
        #     order_obj = self.pool.get('sale.order').browse(cr, uid, context['active_id'], context=context)
        #     res['order_type'] = 'sale'
        #     res['sale'] = True
        #     res['order_ref'] = order_obj.client_order_ref
        #     res['order_id'] = context['active_id']
        #     order_item_wise_close_query = "SELECT id FROM order_item_wise_close WHERE order_id={0} ".format(
        #         context['active_id'])
        #     cr.execute(order_item_wise_close_query)
        #
        # elif context['purchase']== True:
        order_obj = self.pool.get('purchase.order').browse(cr, uid, context['active_id'], context=context)
        res['order_type'] = 'purchase'
        res['purchase'] = True
        res['order_ref'] = order_obj.client_order_ref
        res['purchase_order_id'] = context['active_id']

        items=[]

        item = {

            'product_id': 77800,
            'product_sku': 'jlkl' ,
            'order_qty': 10,
            'kickback_qty': 10,
            'unit_price': 0,
            'total_price': 0,
            'purchase_line_id': 0,

        }

        items.append(item)


        res['order_name'] = str(order_obj.name)
        res.update(kickback_line=items)

        return res





class kickbackline(osv.osv):
    _name ="kickback.line"
    _description = "Kickback Line"


    _columns = {
        'kickback_id': fields.many2one('kickback', 'Kickback', required=True, ondelete='cascade', select=True,
                                      readonly=True),

        'product_id': fields.many2one('product.product', 'Product'),
        'product_sku': fields.char('SKU'),
        'order_qty': fields.float('Order QTY'),
        'kickback_qty': fields.float('KickBack QTY'),
        'unit_price': fields.float('KickBack Price'),
        'total_price': fields.float('Total'),
        'sale_line_id': fields.many2one('sale.order.line', 'Sales Order'),
        'purchase_line_id': fields.many2one('purchase.order.line', 'Purchase Order'),
    }





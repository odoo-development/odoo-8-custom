# -*- coding : utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from openerp import api, fields, models, _
from datetime import datetime
from openerp.osv import fields, osv

class SaleOrderUpdate(osv.osv):
	_inherit = 'sale.order'

	# invoiced_amount = fields.Float(String = 'Invoiced Amount' ,compute ='_computetotal')
	# amount_due = fields.Float(String ='Amount Due',compute ='_computedue')
	# paid_amount = fields.Float(String ='Paid Amount',compute ='_computepaid')
	# amount_paid_percent = fields.Float(compute = 'action_amount_paid')
	# currency_id = fields.Many2one('res.currency', string='Currency',default=lambda self: self.env.user.company_id.currency_id)


	def _computetotal(self, cursor, user, ids, name, arg, context=None):
		res = {}

		for sale in self.browse(cursor, user, ids, context=context):
			total = 0

			for inv_items in sale.invoice_ids:
				if str(inv_items.state) == 'open' or str(inv_items.state) == 'paid':
					if str(inv_items.type) == 'out_invoice':
						total += inv_items.amount_total
					else:
						total -= inv_items.amount_total
			res[sale.id] = total
		return res

				
	def _computedue(self, cursor, user, ids, name, arg, context=None):
		self.ensure_one()
		item = self.env['account.invoice'].search(['&',('origin','=', self.name),'|',('state','=','open'),('delivered','=',True),('state','=','paid')])
		aggregate = 0
		for comp in item:
			aggregate += comp.residual

		self.amount_due = aggregate	


	# @api.onchange('invoiced_amount','amount_due')
	# def _computepaid(self, cursor, user, ids, name, arg, context=None):
	# 	self.paid_amount = float(self.invoiced_amount) - float(self.amount_due)
    #
    #
	#
	# @api.multi
	# def action_amount_paid(self, cursor, user, ids, name, arg, context=None):
	# 	if self.invoiced_amount > 0:
	# 		self.amount_paid_percent = round(100 * self.paid_amount / self.invoiced_amount, 3)

	_columns = {
		 'invoiced_amount': fields.function(_computetotal, string='Invoiced Amount', type='float')
	}


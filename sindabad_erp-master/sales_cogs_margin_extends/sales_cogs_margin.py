import logging
from datetime import datetime
from dateutil.relativedelta import relativedelta
from operator import itemgetter
import time

import openerp
from openerp import SUPERUSER_ID
from openerp import tools
from openerp.osv import fields, osv, expression
from openerp.tools.translate import _
from openerp.tools.float_utils import float_round as round
from datetime import datetime, timedelta
from collections import OrderedDict
import datetime


class sales_cogs_margin(osv.osv):

    _name = "sales.cogs.margin"

    _columns = {
        'order_date': fields.date('Order Date'),
        'order_no': fields.char('Order Number'),
        'return': fields.char('Return'),
        'odoo_order': fields.char('Odoo Number'),
        'customer_name': fields.char('Customer Name'),
        'customer_email': fields.char('Customer Email'),
        'company_name': fields.char('Company Name'),
        'product_name': fields.char('Product Name'),
        'product_category': fields.char('Product Category'),
        'delivered_qty': fields.char('Quantity'),
        'cost_rate_avg': fields.char('Cost Rate'),
        'total_cost': fields.char('Cost'),
        'sales_rate': fields.char('Sale Rate'),
        'total_sales': fields.char('Total Sale'),
        'profit': fields.char('Profit/Loss'),
        'status': fields.char('Status'),
        'transfer_date': fields.datetime('Transfer Date'),

    }

    def date_plus_one(self, date_str):

        date_obj = datetime.datetime.strptime(date_str, '%Y-%m-%d') + datetime.timedelta(days=1)

        increased_date = date_obj.strftime('%Y-%m-%d')

        return increased_date

    def last_day_this_month(self):


        today = datetime.datetime.today()
        if today.month == 12:
            last_day_obj=today.replace(day=31, month=today.month)
        else:
            last_day_obj = today.replace(day=1, month=today.month + 1) - datetime.timedelta(days=1)
        last_day = last_day_obj.strftime('%Y-%m-%d')

        return last_day

    def six_month_ago_first_date(self):

        today = datetime.datetime.today()
        (day, month, year) = (today.day, (today.month - 6) % 12 + 1, today.year + (today.month - 6) / 12)

        first_date = str(year)+"-"+str(month)+"-1"

        return first_date

    def present_month_first_date(self):

        today = datetime.datetime.today()

        # (day, month, year) = (today.day, (today.month - 1) % 12 + 1, today.year + (today.month - 6) / 12)
        first_date = str(today.year)+"-"+str(today.month)+"-1"
        return first_date

    def tunr_over_data_calculation(self, cr, uid, start_date=None, end_date=None, sale_type=None, context=None):
        uid=1

        tmp = start_date
        start_date = end_date
        end_date = tmp
        end_date = self.date_plus_one(end_date)

        last_date_of_this_month = self.last_day_this_month()
        start_date_list = last_date_of_this_month.split('-')
        start_date_list[2] = '1'
        start_date = '-'.join(start_date_list)
        end_date = last_date_of_this_month


        ## Start Order to Confirm Data

        sale_query = "select customer_classification,create_date,date_confirm, DATE_PART('day', AGE(date_confirm,date(create_date))) AS days from sale_order where create_date >=%s and create_date <=%s and date_confirm is not Null"
        objects = cr.execute(sale_query,(start_date,end_date))
        sale_objects = cr.dictfetchall()

        anant_days = corp_days = retail_days = general_days = 0
        anna_count = corp_count = retail_count = general_count = 0
        anant_avg = corp_avg = retail_avg = general_avg = 0

        for items in sale_objects:
            if 'Ananta' in items.get('customer_classification'):
                anna_count = anna_count+1
                anant_days = anant_days + items.get('days')
            elif 'Corporate' in items.get('customer_classification') or 'SME' in items.get('customer_classification') \
                    or 'SOHO' in items.get('customer_classification') or 'HORECA' in items.get('customer_classification'):
                corp_count +=1
                corp_days = corp_days + items.get('days')
            elif 'Retail' in items.get('customer_classification') :
                retail_count += 1
                retail_days = retail_days + items.get('days')
            else:
                general_count += 1
                general_days = general_days + items.get('days')

        order_to_confirm =[]

        try:
            order_to_confirm.append(round((anant_days/anna_count),2))
        except:
            order_to_confirm.append(0)

        try:
            order_to_confirm.append(round((corp_days/corp_count),2))
        except:
            order_to_confirm.append(0)

        try:
            order_to_confirm.append(round((retail_days/retail_count),2))
        except:
            order_to_confirm.append(0)

        try:
            order_to_confirm.append(round((general_days/general_count),2))
        except:
            order_to_confirm.append(0)

        ## Ends Here Order To Confirm Calculation



        ## Start Confirm to Invoice
        inv_type='out_invoice'
        state = 'cancel'

        invoice_query ="select sale_order.customer_classification,DATE_PART('day', AGE(date(account_invoice.create_date),sale_order.date_confirm)) AS inv_days from account_invoice,sale_order_invoice_rel,sale_order where account_invoice.id = sale_order_invoice_rel.invoice_id and sale_order_invoice_rel.order_id = sale_order.id and account_invoice.type=%s and account_invoice.state !=%s and account_invoice.create_date >=%s and account_invoice.create_date<=%s"

        c2i = cr.execute(invoice_query,(inv_type,state,start_date,end_date))
        c2i_obj = cr.dictfetchall()

        anant_days = corp_days = retail_days = general_days = 0
        anna_count = corp_count = retail_count = general_count = 0
        anant_avg = corp_avg = retail_avg = general_avg = 0

        for items in c2i_obj:
            days_count=0
            if items.get('inv_days') is not None:
                days_count = items.get('inv_days')
            if 'Ananta' in items.get('customer_classification'):
                anna_count = anna_count+1
                anant_days = anant_days + days_count
            elif 'Corporate' in items.get('customer_classification') or 'SME' in items.get('customer_classification') \
                    or 'SOHO' in items.get('customer_classification') or 'HORECA' in items.get('customer_classification'):
                corp_count +=1
                corp_days = corp_days + days_count
            elif 'Retail' in items.get('customer_classification') :
                retail_count += 1
                retail_days = retail_days + days_count
            else:
                general_count += 1
                general_days = general_days + days_count

        confirm_to_invoice =[]



        try:
            confirm_to_invoice.append(round((anant_days/anna_count),2))
        except:
            confirm_to_invoice.append(0)

        try:
            confirm_to_invoice.append(round((corp_days/corp_count),2))
        except:
            confirm_to_invoice.append(0)

        try:
            confirm_to_invoice.append(round((retail_days/retail_count),2))
        except:
            confirm_to_invoice.append(0)

        try:
            confirm_to_invoice.append(round((general_days/general_count),2))
        except:
            confirm_to_invoice.append(0)


        ## Ends Here



        ### Start Invoice to Deleivery Code

        in2d_query = "select sale_order.customer_classification,DATE_PART('day', AGE(account_invoice.delivery_date,date(account_invoice.create_date))) AS delvery_days from account_invoice,sale_order_invoice_rel,sale_order where account_invoice.id = sale_order_invoice_rel.invoice_id and sale_order_invoice_rel.order_id = sale_order.id and account_invoice.delivery_date is not Null and account_invoice.type=%s and account_invoice.state!=%s and account_invoice.create_date >=%s and account_invoice.create_date<=%s"

        i2d = cr.execute(in2d_query, (inv_type,state, start_date, end_date))
        i2d_obj = cr.dictfetchall()

        anant_days = corp_days = retail_days = general_days = 0
        anna_count = corp_count = retail_count = general_count = 0
        anant_avg = corp_avg = retail_avg = general_avg = 0

        for items in i2d_obj:
            days_count=0
            if items.get('delvery_days') is not None:
                days_count= items.get('delvery_days')

            if 'Ananta' in items.get('customer_classification'):
                anna_count = anna_count + 1
                anant_days = anant_days + days_count
            elif 'Corporate' in items.get('customer_classification') or 'SME' in items.get(
                    'customer_classification') or 'SOHO' in items.get('customer_classification') or 'HORECA' in items.get('customer_classification'):
                corp_count += 1
                corp_days = corp_days + days_count
            elif 'Retail' in items.get('customer_classification') :
                retail_count += 1
                retail_days = retail_days + days_count
            else:
                general_count += 1
                general_days = general_days + days_count

        invoice_to_delivery = []



        try:
            invoice_to_delivery.append(round((anant_days / anna_count), 2))
        except:
            invoice_to_delivery.append(0)

        try:
            invoice_to_delivery.append(round((corp_days / corp_count), 2))
        except:
            invoice_to_delivery.append(0)

        try:
            invoice_to_delivery.append(round((retail_days / retail_count), 2))
        except:
            invoice_to_delivery.append(0)

        try:
            invoice_to_delivery.append(round((general_days / general_count), 2))
        except:
            invoice_to_delivery.append(0)

        ## Ends Invoice to Delivery Code




        return [order_to_confirm,confirm_to_invoice,invoice_to_delivery]


    def category_wise_invoice_margin(self, cr, uid,start_date=None, end_date=None,sale_type=None, context=None):
        uid=1

        tmp = start_date
        start_date = end_date
        end_date = tmp
        end_date = self.date_plus_one(end_date)

        last_date_of_this_month = self.last_day_this_month()
        start_date_list = last_date_of_this_month.split('-')
        start_date_list[2] = '1'
        start_date = '-'.join(start_date_list)
        end_date = last_date_of_this_month

        primary_cogs_query = "select stock_move.id as moved_id,stock_move.product_id,stock_picking.date_done,stock_picking.picking_type_id,stock_move.product_uom_qty,(select sale_order.customer_classification from sale_order,sale_order_invoice_rel where sale_order.id=sale_order_invoice_rel.order_id and  sale_order_invoice_rel.invoice_id=account_invoice.id) as customer_classification,sale_order_line.price_unit,sale_order_line.order_partner_id,sale_order.client_order_ref,sale_order.date_confirm,sale_order_line.name, sale_order.name as odoo_no,(select product_category.name from product_category,product_template,product_product where product_category.id = product_template.categ_id and product_template.id = product_product.product_tmpl_id and product_product.id = stock_move.product_id ) as category,(select sum((sq.qty*sm.price_unit)) as total from stock_quant_move_rel as sqm,stock_move as sm, stock_quant as sq where sqm.quant_id in ((select sq2.id from stock_quant_move_rel as sqmr2, stock_quant as sq2 where sqmr2.move_id=stock_move.id and sqmr2.quant_id = sq2.id)) and sqm.move_id = sm.id and sm.purchase_line_id >0 and sq.id=sqm.quant_id group by sq.product_id)as total_price , (select name from stock_warehouse where id=stock_move.warehouse_id) as warhouse_name,account_invoice.number as invoice_number, account_invoice.date_invoice as invoice_date, account_invoice.create_date as create_date, account_invoice.state as invoice_state from stock_move,stock_picking,sale_order_line,sale_order,account_invoice where stock_move.picking_id=stock_picking.id and stock_move.sale_line_id = sale_order_line.id and sale_order_line.order_id = sale_order.id  and stock_picking.name = account_invoice.origin and account_invoice.state != 'cancel'  and account_invoice.state != 'draft' and type in ('out_invoice', 'out_refund') and account_invoice.create_date >= %s and account_invoice.create_date <= %s"

        st_date = start_date
        end_date = self.date_plus_one(end_date)
        objects = cr.execute(primary_cogs_query,(st_date,end_date))
        objects = cr.dictfetchall()


        incoming_ids = []
        in_query = "select id from  stock_picking_type where code='incoming'"
        cr.execute(in_query)
        in_objects = cr.dictfetchall()

        incoming_ids = [in_item.get('id') for in_item in in_objects]

        categ_list = []
        sale_categ_dict = {}
        margin_cost_categ_dict = {}

        total_sales =0.00
        total_margin_cost =0.00



        anata_sale = corop_sale =  retail_sale = general_sale = 0
        anata_cogs = corop_cogs =  retail_cogs = general_cogs = 0

        for line in objects:
            if line.get('category') not in categ_list:
                categ_list.append(line.get('category'))
                sale_categ_dict[line.get('category')]=0.00
                margin_cost_categ_dict[line.get('category')]=0.00


            cal_cost=0

            if line.get('total_price') is not None:
                cal_cost  = line.get('total_price')

            if cal_cost ==0:
                get_adjusted_cost_query = "select sum((sq.qty * sm.price_unit)) as total from stock_quant_move_rel as sqm,stock_move as sm, stock_quant as sq where sqm.quant_id in ((select sq2.id from stock_quant_move_rel as sqmr2, stock_quant as sq2 where sqmr2.move_id=%s and sqmr2.quant_id = sq2.id)) and sqm.move_id = sm.id and sm.inventory_id >0 and sq.id=sqm.quant_id group by sq.product_id"
                moved_id = line.get('moved_id')
                cr.execute(get_adjusted_cost_query, ([moved_id]))
                get_adjusted_cost = cr.dictfetchall()
                for a_cost in get_adjusted_cost:
                    if a_cost.get('total') is not None:
                        cal_cost = a_cost.get('total')


            #  Floowing chekup for sq qty is zero then
            if cal_cost ==0:

                get_adjusted_query="select sq.cost,sm.price_unit from stock_quant_move_rel as sqm,stock_move as sm, stock_quant as sq where sqm.quant_id in (select sq2.id from stock_quant_move_rel as sqmr2, stock_quant as sq2 where sqmr2.move_id=%s and sqmr2.quant_id = sq2.id) and sqm.move_id = sm.id  and sq.id=sqm.quant_id and (sm.purchase_line_id >0 or sm.inventory_id >0) ORDER BY sm.id desc limit 1"
                moved_id = line.get('moved_id')
                cr.execute(get_adjusted_query, ([moved_id]))
                get_adjusted_cost = cr.dictfetchall()
                for a_cost in get_adjusted_cost:
                    if a_cost.get('price_unit') is not None:
                        cal_cost = a_cost.get('price_unit') * line.get('product_uom_qty')
                    elif a_cost.get('cost') is not None:
                        cal_cost = a_cost.get('cost') * line.get('product_uom_qty')


            ### here the profit and sales will be calculated

            line_total_sale = float(line.get('price_unit') * line.get('product_uom_qty'))

            if line.get('picking_type_id') in incoming_ids:
                line_total_sale = line_total_sale * -1
                cal_cost = cal_cost* -1



            if 'Corporate' in line.get('customer_classification'):
                corop_sale += line_total_sale
                corop_cogs += cal_cost

            elif 'Ananta' in line.get('customer_classification'):
                anata_sale += line_total_sale
                anata_cogs += cal_cost

            elif 'Retail' in line.get('customer_classification'):
                retail_sale += line_total_sale
                retail_cogs += cal_cost

            elif 'SOHO' in line.get('customer_classification'):
                corop_sale += line_total_sale
                corop_cogs += cal_cost

            elif 'HORECA' in line.get('customer_classification'):
                corop_sale += line_total_sale
                corop_cogs += cal_cost

            else:
                general_sale += line_total_sale
                general_cogs += cal_cost


            total_sales = total_sales + line_total_sale ## IT is for total sales

            total_margin_cost = total_margin_cost + cal_cost ## It is for total Cost

            sale_categ_dict[line.get('category')]= sale_categ_dict[line.get('category')] + line_total_sale  ## It is for categ wise sales
            margin_cost_categ_dict[line.get('category')]= margin_cost_categ_dict[line.get('category')] + cal_cost ## It is for categ wise cost




            ### Ends here

        sales_list=[]
        cat_list=[]
        cost_list=[]
        margin_list=[]
        pnl=0

        for item in categ_list:
            try:
                pnl= round((((sale_categ_dict[item]-margin_cost_categ_dict[item])/sale_categ_dict[item])*100),2)
            except ZeroDivisionError:
                pass

            cat_list.append(item)
            cost_list.append(float(margin_cost_categ_dict[item]))
            sales_list.append(float(sale_categ_dict[item]))
            margin_list.append(float(pnl))
        total_sale = anata_sale+corop_sale+retail_sale+general_sale
        total_cost = anata_cogs+corop_cogs+retail_cogs+general_cogs

        margin = round((total_sale-total_cost),2)
        if total_sale>0:
            margin_in_percent= (margin/total_sale)*100
            margin_in_percent = round(margin_in_percent,2)
        else:
            margin_in_percent=0

        classification_wise_cogs_margin = {'Ananta': [anata_sale,anata_cogs], 'Corp': [corop_sale,corop_cogs], 'Retail': [retail_sale,retail_cogs], 'General': [general_sale,general_cogs]}


        return [ cat_list,cost_list,sales_list,margin_list,total_sales, total_margin_cost,classification_wise_cogs_margin,margin,margin_in_percent]


    def today_to_monthly_sales_trending(self, cr, uid, start_date = None, end_date = None, sale_type = None, context = None):
        uid=1
        tmp = start_date
        start_date = end_date
        end_date = tmp

        # if start_date.split("-")[1] != end_date.split("-")[1]:
        #     start_date = self.present_month_first_date()


        #### Sales Data calculation
        sales_query = "select customer_classification,sum(amount_total) as total from sale_order where state in ('done','progress','manual','shipping_except','invoice_except' ) and date_confirm>=%s and date_confirm<=%s group by customer_classification"

        cr.execute(sales_query,(start_date,end_date))

        sales_data = cr.dictfetchall()

        sale_data_dict ={}
        sale_data_dict['Ananta'] = 0
        sale_data_dict['Corporate'] = 0
        sale_data_dict['Retail'] = 0
        sale_data_dict['General'] = 0
        total_sale = 0
        total_count=0
        total_today_label =[]
        total_today_value=[]


        for items in sales_data:

            try:
                total_sale +=items.get('total')
            except:
                pass

            if 'Ananta' in items.get('customer_classification'):
                sale_data_dict['Ananta'] = items.get('total')
            elif 'Corporate' in items.get('customer_classification') or 'SME' in items.get(
                    'customer_classification') or 'SOHO' in items.get('customer_classification') or 'HORECA' in items.get('customer_classification'):
                # sale_data_dict['Corporate'] = items.get('total')
                if sale_data_dict.get('Corporate') is not None:
                    sale_data_dict['Corporate'] += items.get('total')
                else:
                    sale_data_dict['Corporate'] = items.get('total')
            elif 'Retail' in items.get('customer_classification'):

                if sale_data_dict.get('Retail') is not None:
                    sale_data_dict['Retail'] += items.get('total')
                else:
                    sale_data_dict['Retail'] = items.get('total')

            else:
                sale_data_dict['General'] = items.get('total')


        sales_count_query = "select count(id) as total from sale_order where state in ('done','progress','manual','shipping_except','invoice_except' ) and date_confirm>=%s and date_confirm<=%s"

        cr.execute(sales_count_query, (start_date, end_date))

        sales_count = cr.dictfetchall()

        if len(sales_count) >0:
            total_count=sales_count[0].get('total')



        for k,v in sale_data_dict.iteritems():
            total_today_label.append(k)
            total_today_value.append(v)

        #### Sales Data calculation Ends Here



        ### Invoice Calcualtion Starts Here

        total_invoice_query = "select sum(account_invoice.amount_total) as total,sale_order.customer_classification as customer_classification,account_invoice.type from account_invoice,sale_order_invoice_rel,sale_order where account_invoice.id=sale_order_invoice_rel.invoice_id and sale_order_invoice_rel.order_id=sale_order.id and account_invoice.type in ('out_invoice','out_refund') and account_invoice.create_date>=%s and account_invoice.state  in ('open','paid') and account_invoice.create_date <=%s group by account_invoice.type,sale_order.customer_classification"
        end_date = self.date_plus_one(end_date)

        cr.execute(total_invoice_query, (start_date, end_date))



        total_invoice_data = cr.dictfetchall()

        invoice_data_dict = {}
        invoice_data_dict['Ananta'] = 0
        invoice_data_dict['Corporate'] = 0
        invoice_data_dict['Retail'] = 0
        invoice_data_dict['General'] = 0
        total_invoice_amount = 0
        total_invoice_count = 0
        total_invoice_label = []
        total_invoice_value = []

        refund_invoice_dict ={}
        refund_invoice_dict['Ananta'] =0
        refund_invoice_dict['Corporate'] =0
        refund_invoice_dict['Retail'] =0
        refund_invoice_dict['General'] =0
        refund_invoice_amount=0
        refund_invoice_count=0
        refund_invoice_level=[]
        refund_invoice_value=[]

        net_ananta = 0
        net_general=0
        net_corporate = 0
        net_retail = 0



        for items in total_invoice_data:
            if items.get('customer_classification') is None or items.get('customer_classification') is False:
                items_classification='General'
            else:
                items_classification = items.get('customer_classification')

            if items.get('type') == 'out_invoice':
                try:
                    total_invoice_amount += items.get('total')
                except:
                    pass
                if 'Ananta' in items_classification:
                    invoice_data_dict['Ananta'] = items.get('total')
                    net_ananta += items.get('total')
                elif 'Corporate' in items_classification or 'SME' in items.get(
                        'customer_classification') or 'SOHO' in items_classification or 'HORECA' in items_classification:
                    if invoice_data_dict.get('Corporate') is not None:
                        invoice_data_dict['Corporate'] += items.get('total')
                    else:
                        invoice_data_dict['Corporate'] = items.get('total')
                    net_corporate += items.get('total')
                elif 'Retail' in items_classification :

                    if invoice_data_dict.get('Retail') is not None:
                        invoice_data_dict['Retail'] += items.get('total')
                    else:
                        invoice_data_dict['Retail'] = items.get('total')
                    net_retail += items.get('total')

                else:
                    invoice_data_dict['General'] = items.get('total')
                    net_general += items.get('total')

            else:
                try:
                    refund_invoice_amount += items.get('total')
                except:

                    pass



                if 'Ananta' in items_classification:
                    refund_invoice_dict['Ananta'] = items.get('total')
                    net_ananta -=items.get('total')
                elif 'Corporate' in items_classification or 'SME' in items_classification or 'SOHO' in items_classification or 'HORECA' in items_classification:
                    if refund_invoice_dict.get('Corporate') is not None:
                        refund_invoice_dict['Corporate'] += items.get('total')
                    else:
                        refund_invoice_dict['Corporate'] = items.get('total')
                    net_corporate -=items.get('total')
                elif 'Retail' in items_classification :
                    if refund_invoice_dict.get('Retail') is not None:
                        refund_invoice_dict['Retail'] += items.get('total')
                    else:
                        refund_invoice_dict['Retail'] = items.get('total')

                    net_retail -=items.get('total')

                else:
                    refund_invoice_dict['General'] = items.get('total')
                    net_general -=items.get('total')




        for k, v in invoice_data_dict.iteritems():
            total_invoice_label.append(k)
            total_invoice_value.append(v)

        for k, v in refund_invoice_dict.iteritems():
            refund_invoice_level.append(k)
            refund_invoice_value.append(v)

        inv_count_query= "select count(id) as total from account_invoice where type='out_invoice' and state  in ('open','paid') and create_date>=%s and create_date <=%s"

        cr.execute(inv_count_query, (start_date, end_date))


        inv_count_data = cr.dictfetchall()



        if len(inv_count_data) >0:
            total_invoice_count=inv_count_data[0].get('total')

        ref_inv_count_query = "select count(id) as total from account_invoice where type='out_refund' and state  in ('open','paid') and create_date>=%s and create_date <=%s"
        cr.execute(ref_inv_count_query, (start_date, end_date))

        ref_inv_count_data = cr.dictfetchall()


        net_invoice_level=[]
        net_invoice_value=[]


        if len(ref_inv_count_query) > 0:
            refund_invoice_count = ref_inv_count_data[0].get('total')
        net_invoice_amount= net_ananta + net_general + net_retail + net_corporate
        margin=0

        if net_ananta !=0:
            net_invoice_level.append('Ananta')
            net_invoice_value.append(net_ananta)
        else:
            net_invoice_level.append('Ananta')
            net_invoice_value.append(0)
        if net_corporate !=0:
            net_invoice_level.append('Corporate')
            net_invoice_value.append(net_corporate)
        else:
            net_invoice_level.append('Corporate')
            net_invoice_value.append(0)
        if net_retail != 0:
            net_invoice_level.append('Retail')
            net_invoice_value.append(net_retail)
        else:
            net_invoice_level.append('Retail')
            net_invoice_value.append(0)

        if net_retail != 0:
            net_invoice_level.append('General')
            net_invoice_value.append(net_general)
        else:
            net_invoice_level.append('General')
            net_invoice_value.append(0)





            ## Net invoice





            ## Net Invoices Ends here

        ### Invoice Amount Ends here


        return [float(str(total_sale)),total_count,total_today_label,total_today_value,float(str(total_invoice_amount)),total_invoice_count,total_invoice_label,total_invoice_value,float(str(refund_invoice_amount)),
            refund_invoice_count,
            refund_invoice_level,
            refund_invoice_value,
            float(str(net_invoice_amount)),
            margin,
            net_invoice_level,
            net_invoice_value,
                ]


    def monthon_month_value(self, cr, uid,start_date=None, end_date=None,sale_type=None, context=None):
        uid=1
        # tmp=start_date
        # start_date=end_date
        # end_date=tmp

        # start_date='2020-04-01'
        start_date = self.six_month_ago_first_date()
        # end_date='2020-09-30'
        end_date = self.last_day_this_month()

        start_date_year = start_date.split('-')[0]
        end_date_year = end_date.split('-')[0]

        tmp_date = end_date

        current_spltted_data = tmp_date.split('-')
        current_year = int(current_spltted_data[0])%2000
        previous_year = (int(current_spltted_data[0])%2000)-1
        current_month = int(current_spltted_data[1])

        # if current_month > 6 and int(start_date_year)!=int(end_date_year):
        #     previous_year = previous_year
        # else:
        #     previous_year = current_year

        if current_month > 6:
            previous_year = current_month



        customer_classification = ['Ananta','Corporate','Retail','General']
        month_dict={
            1:"Jan' " + str( current_year),
            2:"Feb' "+ str( current_year),
            3:"Mar' "+ str( current_year),
            4:"Apr' "+ str( current_year),
            5:"May' "+ str( current_year),
            6:"Jun' "+ str( current_year),
            7:"Jul' "+ str( previous_year),
            8:"Aug' "+ str( previous_year),
            9:"Sep' "+ str( previous_year),
            10:"Oct' "+ str( previous_year),
            11:"Nov' "+ str( previous_year),
            12:"Dec' "+ str( previous_year)
        }



        month_wise_sale_query = "select sum(amount_total) as total ,customer_classification,  date_part('month',date_confirm) as month_wise from sale_order where state in ('done','progress','manual','shipping_except','invoice_except' ) and date_confirm>=%s and date_confirm<=%s group by month_wise, customer_classification order by month_wise"

        cr.execute(month_wise_sale_query,(start_date,end_date))

        month_sale_list = cr.dictfetchall()


        sale_month_list =[]




        for items in month_sale_list:
            if items.get('month_wise') not in sale_month_list:

                sale_month_list.append(items.get('month_wise'))
        sale_month_list=list(dict.fromkeys(sale_month_list))


        cl_sale_dict ={}

        for sale_month in sale_month_list:
            tmp_value=[]
            ananta_sale=0
            corporat_sale=0
            retail_sale=0
            general_sale=0
            for items in month_sale_list:
                if sale_month == items.get('month_wise'):
                    if 'Ananta' in items.get('customer_classification'):
                        ananta_sale += items.get('total')
                    elif 'Corporate' in items.get('customer_classification') or 'SME' in \
                            items.get('customer_classification') or 'SOHO' in items.get('customer_classification') or 'HORECA' in items.get('customer_classification'):
                        corporat_sale += items.get('total')
                    elif 'Retail' in items.get('customer_classification'):
                        retail_sale += items.get('total')
                    else:
                        general_sale += items.get('total')
            cl_sale_dict[sale_month]=[ananta_sale,corporat_sale,retail_sale,general_sale]

        an_data=[]
        cor_data=[]
        retial_data=[]
        gen_data=[]
        for k,v in cl_sale_dict.iteritems():
            prepare_list =[]
            an_data.append(v[0])
            cor_data.append(v[1])
            retial_data.append(v[2])
            gen_data.append(v[3])
        montwise_sale_final_list =[{
            'name': 'Ananta',
            'data': an_data
        },
            {
                'name': 'Corporate',
                'data': cor_data
            },
            {
                'name': 'Retail',
                'data': retial_data
            },
            {
                'name': 'General',
                'data': gen_data
            },
        ]







        #for items in month_sale_list:
        #    if 'Ananta' in items.get('customer_classification'):


        month_wise_inv_refund_query = "select sum(account_invoice.amount_total) as total,sale_order.customer_classification as customer_classification,account_invoice.type ,date_part('month',account_invoice.create_date) as inv_month_wise from account_invoice,sale_order_invoice_rel,sale_order where account_invoice.id=sale_order_invoice_rel.invoice_id and sale_order_invoice_rel.order_id=sale_order.id and account_invoice.type in ('out_invoice','out_refund') and account_invoice.state  in ('open','paid') and account_invoice.create_date>=%s and account_invoice.create_date <=%s group by account_invoice.type,inv_month_wise,sale_order.customer_classification order by account_invoice.type,inv_month_wise"

        new_end_date = self.date_plus_one(end_date)

        cr.execute(month_wise_inv_refund_query, (start_date, new_end_date))

        month_inv_list = cr.dictfetchall()

        inv_month_list=[]
        ref_inv_month_list=[]


        for items in month_inv_list:
            if items.get('type') == 'out_invoice':
                if items.get('inv_month_wise') not in inv_month_list:
                    inv_month_list.append(items.get('inv_month_wise'))
                else:
                    ref_inv_month_list.append(items.get('inv_month_wise'))



        inv_month_list=list(dict.fromkeys(inv_month_list))
        ref_inv_month_list=list(dict.fromkeys(ref_inv_month_list))
        inv_cl_sale_dict ={}
        ref_cl_sale_dict ={}

        ### For INvoice
        for inv_month in inv_month_list:

            inv_ananta_sale=0
            inv_corporat_sale=0
            inv_retail_sale=0
            inv_general_sale=0
            for items in month_inv_list:
                if inv_month == items.get('inv_month_wise') and items.get('type') == 'out_invoice':
                    if 'Ananta' in items.get('customer_classification'):
                        inv_ananta_sale += items.get('total')
                    elif 'Corporate' in items.get('customer_classification') or 'SME' in items.get('customer_classification')\
                            or 'SOHO' in items.get('customer_classification') or 'HORECA' in items.get('customer_classification'):
                        inv_corporat_sale += items.get('total')
                    elif 'Retail' in items.get('customer_classification') :
                        inv_retail_sale += items.get('total')

                    else:
                        inv_general_sale += items.get('total')

            inv_cl_sale_dict[inv_month]=[inv_ananta_sale,inv_corporat_sale,inv_retail_sale,inv_general_sale]

        inv_an_data = []
        inv_cor_data = []
        inv_retial_data = []
        inv_gen_data = []
        for k, v in inv_cl_sale_dict.iteritems():
            prepare_list = []
            inv_an_data.append(v[0])
            inv_cor_data.append(v[1])
            inv_retial_data.append(v[2])
            inv_gen_data.append(v[3])
        montwise_inv_final_list = [{
            'name': 'Ananta',
            'data': inv_an_data
        },
            {
                'name': 'Corporate',
                'data': inv_cor_data
            },
            {
                'name': 'Retail',
                'data': inv_retial_data
            },
            {
                'name': 'General',
                'data': inv_gen_data
            },
        ]

        ### For Invoice Ends Here

        ### For Refund Starts Here
        for ref_month in ref_inv_month_list:

            ref_ananta_sale=0
            ref_corporat_sale=0
            ref_retail_sale=0
            ref_general_sale=0
            for items in month_inv_list:
                if ref_month == items.get('inv_month_wise') and items.get('type') == 'out_refund':
                    if 'Ananta' in items.get('customer_classification'):
                        ref_ananta_sale += items.get('total')
                    elif 'Corporate' in items.get('customer_classification') or 'SME' in items.get(
                            'customer_classification') or 'SOHO' in items.get('customer_classification') or 'HORECA' in items.get('customer_classification'):
                        ref_corporat_sale += items.get('total')
                    elif 'Retail' in items.get('customer_classification') :
                        ref_retail_sale += items.get('total')

                    else:
                        ref_general_sale += items.get('total')

            ref_cl_sale_dict[ref_month] = [ref_ananta_sale, ref_corporat_sale, ref_retail_sale, ref_general_sale]

        ref_an_data = []
        ref_cor_data = []
        ref_retial_data = []
        ref_gen_data = []
        for k, v in ref_cl_sale_dict.iteritems():
            prepare_list = []
            ref_an_data.append(v[0])
            ref_cor_data.append(v[1])
            ref_retial_data.append(v[2])
            ref_gen_data.append(v[3])
        montwise_refund_final_list = [{
                'name': 'Ananta',
                'data': ref_an_data
            },
            {
                'name': 'Corporate',
                'data': ref_cor_data
            },
            {
                'name': 'Retail',
                'data': ref_retial_data
            },
            {
                'name': 'General',
                'data': ref_gen_data
            },
        ]


        ### For Refund Ends Here

        ## For Net Invoice

        net_anant = [str((x-y)) for x,y in zip(inv_an_data, ref_an_data)]
        net_cor = [str((x-y)) for x,y in zip(inv_cor_data, ref_cor_data)]
        net_ret = [str((x-y)) for x,y in zip(inv_retial_data, ref_retial_data)]
        net_gen = [str((x-y)) for x,y in zip(inv_gen_data, ref_gen_data)]

        net_montwise_refund_final_list = [{
            'name': 'Ananta',
            'data': net_anant
        },
            {
                'name': 'Corporate',
                'data': net_cor
            },
            {
                'name': 'Retail',
                'data': net_ret
            },
            {
                'name': 'General',
                'data': net_gen
            },
        ]

        ## Ends Here
        # import pdb;pdb.set_trace()
        sale_month_list = [month_dict.get(x) for x in sale_month_list]


        return [sale_month_list,montwise_sale_final_list,montwise_inv_final_list,montwise_refund_final_list,net_montwise_refund_final_list]


    def monthon_month_count(self, cr, uid,start_date=None, end_date=None,sale_type=None, context=None):
        uid=1
        # tmp=start_date
        # start_date=end_date
        # end_date=tmp

        # start_date='2020-04-01'
        start_date = self.six_month_ago_first_date()
        # end_date='2020-09-30'
        end_date = self.last_day_this_month()

        start_date_year = start_date.split('-')[0]
        end_date_year = end_date.split('-')[0]

        tmp_date = end_date

        current_spltted_data = tmp_date.split('-')
        current_year = int(current_spltted_data[0]) % 2000
        previous_year = (int(current_spltted_data[0]) % 2000) - 1
        current_month = int(current_spltted_data[1])

        if current_month > 6 and int(start_date_year)!=int(end_date_year):
            previous_year = previous_year
        else:
            previous_year = current_year

        if current_month > 6:
            previous_year = current_month

        customer_classification = ['Ananta','Corporate','Retail','General']
        month_dict = {
            13: "Jan' " + str(current_year),
            2: "Feb' " + str(current_year),
            3: "Mar' " + str(current_year),
            4: "Apr' " + str(current_year),
            5: "May' " + str(current_year),
            6: "Jun' " + str(current_year),
            7: "Jul' " + str(previous_year),
            8: "Aug' " + str(previous_year),
            9: "Sep' " + str(previous_year),
            10: "Oct' " + str(previous_year),
            11: "Nov' " + str(previous_year),
            12: "Dec' " + str(previous_year)
        }



        month_wise_sale_query = "select count(amount_total) as total ,customer_classification,  date_part('month',date_confirm) as month_wise from sale_order where state in ('done','progress','manual','shipping_except','invoice_except' ) and date_confirm>=%s and date_confirm<=%s group by month_wise, customer_classification order by month_wise"

        cr.execute(month_wise_sale_query,(start_date,end_date))

        month_sale_list = cr.dictfetchall()


        sale_month_list =[]




        for items in month_sale_list:
            if items.get('month_wise') not in sale_month_list:

                sale_month_list.append(items.get('month_wise'))
        sale_month_list=list(dict.fromkeys(sale_month_list))


        cl_sale_dict ={}

        for sale_month in sale_month_list:
            tmp_value=[]
            ananta_sale=0
            corporat_sale=0
            retail_sale=0
            general_sale=0
            for items in month_sale_list:
                if sale_month == items.get('month_wise'):
                    if 'Ananta' in items.get('customer_classification'):
                        ananta_sale += items.get('total')
                    elif 'Corporate' in items.get('customer_classification') or \
                                    'SME' in items.get('customer_classification') or \
                                    'SOHO' in items.get('customer_classification') or \
                                    'HORECA' in items.get('customer_classification'):
                        corporat_sale += items.get('total')
                    elif 'Retail' in items.get('customer_classification'):
                        retail_sale += items.get('total')
                    else:
                        general_sale += items.get('total')
            cl_sale_dict[sale_month]=[ananta_sale,corporat_sale,retail_sale,general_sale]

        an_data=[]
        cor_data=[]
        retial_data=[]
        gen_data=[]
        for k,v in cl_sale_dict.iteritems():
            prepare_list =[]
            an_data.append(v[0])
            cor_data.append(v[1])
            retial_data.append(v[2])
            gen_data.append(v[3])
        montwise_sale_final_list =[{
            'name': 'Ananta',
            'data': an_data
        },
            {
                'name': 'Corporate',
                'data': cor_data
            },
            {
                'name': 'Retail',
                'data': retial_data
            },
            {
                'name': 'General',
                'data': gen_data
            },
        ]







        #for items in month_sale_list:
        #    if 'Ananta' in items.get('customer_classification'):


        month_wise_inv_refund_query = "select count(account_invoice.amount_total) as total,sale_order.customer_classification as customer_classification,account_invoice.type ,date_part('month',account_invoice.create_date) as inv_month_wise from account_invoice,sale_order_invoice_rel,sale_order where account_invoice.id=sale_order_invoice_rel.invoice_id and sale_order_invoice_rel.order_id=sale_order.id and account_invoice.type in ('out_invoice','out_refund') and account_invoice.state  in ('open','paid') and account_invoice.create_date>=%s and account_invoice.create_date <=%s group by account_invoice.type,inv_month_wise,sale_order.customer_classification order by account_invoice.type,inv_month_wise"

        new_end_date = self.date_plus_one(end_date)

        cr.execute(month_wise_inv_refund_query, (start_date, new_end_date))

        month_inv_list = cr.dictfetchall()

        inv_month_list=[]
        ref_inv_month_list=[]


        for items in month_inv_list:
            if items.get('type') == 'out_invoice':
                if items.get('inv_month_wise') not in inv_month_list:
                    inv_month_list.append(items.get('inv_month_wise'))
                else:
                    ref_inv_month_list.append(items.get('inv_month_wise'))



        inv_month_list=list(dict.fromkeys(inv_month_list))
        ref_inv_month_list=list(dict.fromkeys(ref_inv_month_list))
        inv_cl_sale_dict ={}
        ref_cl_sale_dict ={}

        ### For INvoice
        for inv_month in inv_month_list:

            inv_ananta_sale=0
            inv_corporat_sale=0
            inv_retail_sale=0
            inv_general_sale=0
            for items in month_inv_list:
                if inv_month == items.get('inv_month_wise') and items.get('type') == 'out_invoice':
                    if 'Ananta' in items.get('customer_classification'):
                        inv_ananta_sale += items.get('total')
                    elif 'Corporate' in items.get('customer_classification') or \
                                    'SME' in items.get('customer_classification') or \
                                    'SOHO' in items.get('customer_classification') or\
                                    'HORECA' in items.get('customer_classification'):
                        inv_corporat_sale += items.get('total')
                    elif 'Retail' in items.get('customer_classification'):
                        inv_retail_sale += items.get('total')

                    else:
                        inv_general_sale += items.get('total')

            inv_cl_sale_dict[inv_month]=[inv_ananta_sale,inv_corporat_sale,inv_retail_sale,inv_general_sale]

        inv_an_data = []
        inv_cor_data = []
        inv_retial_data = []
        inv_gen_data = []
        for k, v in inv_cl_sale_dict.iteritems():
            prepare_list = []
            inv_an_data.append(v[0])
            inv_cor_data.append(v[1])
            inv_retial_data.append(v[2])
            inv_gen_data.append(v[3])
        montwise_inv_final_list = [{
            'name': 'Ananta',
            'data': inv_an_data
        },
            {
                'name': 'Corporate',
                'data': inv_cor_data
            },
            {
                'name': 'Retail',
                'data': inv_retial_data
            },
            {
                'name': 'General',
                'data': inv_gen_data
            },
        ]

        ### For Invoice Ends Here

        ### For Refund Starts Here
        for ref_month in ref_inv_month_list:

            ref_ananta_sale=0
            ref_corporat_sale=0
            ref_retail_sale=0
            ref_general_sale=0
            for items in month_inv_list:
                if ref_month == items.get('inv_month_wise') and items.get('type') == 'out_invoice':
                    if 'Ananta' in items.get('customer_classification'):
                        ref_ananta_sale += items.get('total')
                    elif 'Corporate' in items.get('customer_classification') or 'SME' in items.get(
                            'customer_classification') or 'SOHO' in items.get('customer_classification') \
                            or 'HORECA' in items.get('customer_classification'):
                        ref_corporat_sale += items.get('total')
                    elif 'Retail' in items.get('customer_classification'):
                        ref_retail_sale += items.get('total')

                    else:
                        ref_general_sale += items.get('total')

            ref_cl_sale_dict[ref_month] = [ref_ananta_sale, ref_corporat_sale, ref_retail_sale, ref_general_sale]

        ref_an_data = []
        ref_cor_data = []
        ref_retial_data = []
        ref_gen_data = []
        for k, v in ref_cl_sale_dict.iteritems():
            prepare_list = []
            ref_an_data.append(v[0])
            ref_cor_data.append(v[1])
            ref_retial_data.append(v[2])
            ref_gen_data.append(v[3])
        montwise_refund_final_list = [{
                'name': 'Ananta',
                'data': ref_an_data
            },
            {
                'name': 'Corporate',
                'data': ref_cor_data
            },
            {
                'name': 'Retail',
                'data': ref_retial_data
            },
            {
                'name': 'General',
                'data': ref_gen_data
            },
        ]


        ### For Refund Ends Here

        ## For Net Invoice

        net_anant = [sum(x) for x in zip(inv_an_data, ref_an_data)]
        net_cor = [sum(x) for x in zip(inv_cor_data, ref_cor_data)]
        net_ret = [sum(x) for x in zip(inv_retial_data, ref_retial_data)]
        net_gen = [sum(x) for x in zip(inv_gen_data, ref_gen_data)]

        net_montwise_refund_final_list = [{
            'name': 'Ananta',
            'data': net_anant
        },
            {
                'name': 'Corporate',
                'data': net_cor
            },
            {
                'name': 'Retail',
                'data': net_ret
            },
            {
                'name': 'General',
                'data': net_gen
            },
        ]

        ## Ends Here
        sale_month_list = [month_dict.get(x) for x in sale_month_list]


        return [sale_month_list,montwise_sale_final_list,montwise_inv_final_list,montwise_refund_final_list,net_montwise_refund_final_list]


    def processing_all(self, cr, uid,start_date=None, end_date=None,sale_type=None, context=None):

        # sale_obj = self.pool.get('management.dashboard')
        sale_obj = self.pool.get('sale.order')
        uid = 1

        sale_list = ''
        if start_date == end_date:
            # for blank search
            sale_list = sale_obj.search(cr, uid, [('date_confirm', '=', start_date), ('state', '!=', 'cancel')])
        elif start_date != end_date:
            sale_list = sale_obj.search(cr, uid, [('date_confirm', '>', end_date), ('state', '!=', 'cancel')])

        processing_so_list = []
        if sale_type == 'processingAll':
            # processing_so_list = sale_obj.search(cr, uid, [('state', '=', 'progress')])
            all_so_list = sale_obj.search(cr, uid, [('state', '=', 'progress')])

            for so_l in sale_obj.browse(cr, uid, all_so_list):
                if str(so_l.customer_classification).startswith("Retail") or str(so_l.customer_classification).startswith("Ananta") or str(so_l.customer_classification).startswith("Corporate") or str(so_l.customer_classification).startswith("SOHO")or str(so_l.customer_classification).startswith("HORECA") or str(so_l.customer_classification).startswith("SME") or str(so_l.customer_classification).startswith("General"):
                    processing_so_list.append(so_l.id)

        elif sale_type == 'processingAnanta':
            processing_so_list = sale_obj.search(cr, uid, [('customer_classification', '=', 'Ananta'), ('state', '=', 'progress')])
        elif sale_type == 'processingCorp':
            # processing_so_list = sale_obj.search(cr, uid, [('customer_classification', '=', 'Corporate'), ('state', '=', 'progress')])
            corp_progress = sale_obj.search(cr, uid, [('state', '=', 'progress')])
            for crp in sale_obj.browse(cr, uid, corp_progress):
                if str(crp.customer_classification).startswith("Corporate"):
                    processing_so_list.append(crp.id)
                if str(crp.customer_classification).startswith("SOHO"):
                    processing_so_list.append(crp.id)
                if str(crp.customer_classification).startswith("SME"):
                    processing_so_list.append(crp.id)
                if str(crp.customer_classification).startswith("HORECA"):
                    processing_so_list.append(crp.id)

        elif sale_type == 'processingRetail':
            retail_progress = sale_obj.search(cr, uid, [('customer_classification', 'ilike', 'Retail'),('state', '=', 'progress')])
            for rp in sale_obj.browse(cr, uid, retail_progress):
                if str(rp.customer_classification).startswith("Retail"):
                    processing_so_list.append(rp.id)
                # if str(rp.customer_classification).startswith("SOHO"):
                #     processing_so_list.append(rp.id)
                # if str(rp.customer_classification).startswith("SME"):
                #     processing_so_list.append(rp.id)
            """
            processing_so_list = sale_obj.search(cr, uid, ['|', '|', '&', ('customer_classification', 'ilike', 'Retail'),
                                                           ('customer_classification', 'ilike', 'SOHO'),
                                                           ('customer_classification', 'ilike', 'SME'), ('state', '=', 'progress')])
            """
        elif sale_type == 'processingOnline':
            processing_so_list = sale_obj.search(cr, uid, [('customer_classification', '=', 'General'), ('state', '=', 'progress')])
        else:
            # processing_so_list = sale_obj.search(cr, uid, [('state', '=', 'progress')])
            all_so_list = sale_obj.search(cr, uid, [('state', '=', 'progress')])

            for so_l in sale_obj.browse(cr, uid, all_so_list):
                if str(so_l.customer_classification).startswith("Retail") or str(
                        so_l.customer_classification).startswith("Ananta") or str(
                        so_l.customer_classification).startswith("Corporate") or str(
                        so_l.customer_classification).startswith("SOHO") or str(
                        so_l.customer_classification).startswith("HORECA") or str(
                        so_l.customer_classification).startswith("SME") or str(so_l.customer_classification).startswith(
                        "General"):
                    processing_so_list.append(so_l.id)

        processing_count_labels = ['0 to 1 Days', '2 to 3 Days', '4 Days Above']
        """
        processing_sos = sale_obj.browse(cr, uid, processing_so_list)

        
        processing_count_values = [0, 0, 0]
        processing_order_values = [0, 0, 0]

        today_date = datetime.datetime.today()

        for so in processing_sos:
            confirm_date = datetime.datetime.strptime(str(so.date_confirm), '%Y-%m-%d')

            pro_date = (today_date - confirm_date).days

            if pro_date < 2:
                processing_count_values[0] += 1
                processing_order_values[0] += so.amount_total
            elif pro_date < 4:
                processing_count_values[1] += 1
                processing_order_values[1] += so.amount_total
            elif pro_date > 3:
                processing_count_values[2] += 1
                processing_order_values[2] += so.amount_total

        # import pdb;pdb.set_trace()
        """

        processing_count_values, processing_order_values = self.processing_order_date(cr, uid, processing_so_list)

        return [processing_count_labels, processing_count_values, processing_order_values]

    def processing_order_date(self, cr, uid, processing_so_list):

        # sale_obj = self.pool.get('management.dashboard')
        sale_obj = self.pool.get('sale.order')
        uid = 1

        processing_sos = sale_obj.browse(cr, uid, processing_so_list)

        # processing_count_labels = ['0 to 1 Days', '2 to 3 Days', '4 Days Above']
        processing_count_values = [0, 0, 0]
        processing_order_values = [0, 0, 0]

        today_date = datetime.datetime.today()

        for so in processing_sos:
            confirm_date = datetime.datetime.strptime(str(so.date_confirm), '%Y-%m-%d')

            pro_date = (today_date - confirm_date).days

            if pro_date < 2:
                processing_count_values[0] += 1
                processing_order_values[0] += so.amount_total - so.invoiced_amount
            elif pro_date < 4:
                processing_count_values[1] += 1
                processing_order_values[1] += so.amount_total - so.invoiced_amount
            elif pro_date > 3:
                processing_count_values[2] += 1
                processing_order_values[2] += so.amount_total - so.invoiced_amount

        return processing_count_values, processing_order_values

    def mgt_dashboard_data_preparation(self, cr, uid,start_date=None, end_date=None,sale_type=None, context=None):

        # ------------------------- Sale orders from date --------------
        # sale_obj = self.pool.get('sale.order')
        sale_obj = self.pool.get('management.dashboard')
        uid = 1

        sale_list = ''
        if start_date == end_date:
            # for blank search
            sale_list = sale_obj.search(cr, uid, [('date_confirm', '=', start_date), ('state', '!=', 'cancel')])
        elif start_date != end_date:
            sale_list = sale_obj.search(cr, uid, [('date_confirm', '>', end_date), ('state', '!=', 'cancel')])
        # elif start_date != end_date and sale_type == 'all':

        today_to_monthly_sales_list = self.today_to_monthly_sales_trending(cr, uid,start_date=start_date,end_date=end_date)



        # Processing ----------------
        # import pdb;pdb.set_trace()
        processing_count_labels, processing_count_values, processing_order_values = self.processing_all(cr, uid, start_date=start_date, end_date=end_date, sale_type=sale_type)
        """
        processing_so_list = []
        if sale_type == 'processingAll':
            processing_so_list = sale_obj.search(cr, uid, [('state', '=', 'progress')])
        elif sale_type == 'processingAnanta':
            processing_so_list = sale_obj.search(cr, uid, [('partner_classification', '=', 'Ananta')])
        elif sale_type == 'processingCorp':
            processing_so_list = sale_obj.search(cr, uid, [('partner_classification', '=', 'Corporate')])
        elif sale_type == 'processingRetail':
            processing_so_list = sale_obj.search(cr, uid, ['|', '|', ('partner_classification', 'ilike', '%Retail'), ('partner_classification', 'ilike', '%SOHO'), ('partner_classification', 'ilike', '%SME')])
        elif sale_type == 'processingOnline':
            processing_so_list = sale_obj.search(cr, uid, [('partner_classification', '=', 'General')])
        else:
            processing_so_list = sale_obj.search(cr, uid, [('state', '=', 'progress')])

        processing_sos = sale_obj.browse(cr, uid, processing_so_list)

        processing_count_labels = ['0 to 1 Days', '2 to 3 Days', '4 Days Above']
        processing_count_values = [0, 0, 0]
        processing_order_values = [0, 0, 0]

        # today_date = datetime.datetime.now().strftime('%Y-%m-%d')
        # today_date = datetime.datetime.now()
        # print(processing_so_list)
        today_date = datetime.datetime.today()

        for so in processing_sos:
            confirm_date = datetime.datetime.strptime(str(so.date_confirm), '%Y-%m-%d')

            pro_date = (today_date - confirm_date).days

            if pro_date < 2:
                processing_count_values[0] += 1
                processing_order_values[0] += so.amount_total
            elif pro_date < 4:
                processing_count_values[1] += 1
                processing_order_values[1] += so.amount_total
            elif pro_date > 3:
                processing_count_values[2] += 1
                processing_order_values[2] += so.amount_total
        """



        catg_data = self.category_wise_invoice_margin(cr, uid,start_date=start_date,end_date=end_date)
        categ_list = catg_data[0]
        cost_list = catg_data[1]
        sale_list=catg_data[2]
        margin_list=catg_data[3]
        total_sale=catg_data[4]
        total_margin = catg_data[5]
        margin = float(str(catg_data[7]))
        margin_percent = float(str(catg_data[8]))



        classification_wise_sales = catg_data[6]

        c_wise_label=[]
        c_wise_values =[]




        classification_list =[]
        classification_wise_profit_value_list =[]
        classification_wise_profit_percent_list =[]

        for k,v in classification_wise_sales.iteritems():
            if v[0] !=0 and v[1] !=0:
                c_wise_label.append(k)
                classification_list.append(k)
                profit= int((v[0]-v[1]))
                classification_wise_profit_value_list.append(profit)
                pnl = round((((v[0]-v[1])/v[0])*100),2)
                classification_wise_profit_percent_list.append(pnl)
                c_wise_values.append(pnl)


        turn_oder_data = self.tunr_over_data_calculation(cr, uid,start_date=start_date,end_date=end_date)
        month_on_month_oder_data = self.monthon_month_value(cr, uid,start_date=start_date,end_date=end_date)


        result_list = [
            categ_list,
            [str(x) for x in cost_list],
            [str(x if x>=0 else 0) for x in sale_list],
            [str(x) for x in margin_list],
            total_sale,
            total_margin,
            c_wise_label,
            c_wise_values,
            turn_oder_data[0],
            turn_oder_data[1],
            turn_oder_data[2]

        ]

        # result_list = [0,0,0,0, 0,0,
        #     c_wise_label,
        #     c_wise_values,
        #     turn_oder_data[0],
        #     turn_oder_data[1],
        #     turn_oder_data[2] ]

        sec_list=[processing_count_labels,
            processing_count_values,
            processing_order_values,
                  margin,
                  margin_percent]

        # sec_list = [processing_count_labels,
        #             processing_count_values,
        #             processing_order_values,
        #             0,0]
        new_bar_chart = [classification_list,classification_wise_profit_value_list,classification_wise_profit_percent_list]

        final_list =  today_to_monthly_sales_list + result_list + month_on_month_oder_data+ sec_list + new_bar_chart

        return final_list

    def mgt_dashboard_data_preparation1(self, cr, uid,start_date=None, end_date=None,sale_type=None, context=None):

        # ------------------------- Sale orders from date --------------
        # sale_obj = self.pool.get('sale.order')
        sale_obj = self.pool.get('management.dashboard')
        uid = 1

        sale_list = ''
        if start_date == end_date:
            # for blank search
            sale_list = sale_obj.search(cr, uid, [('date_confirm', '=', start_date), ('state', '!=', 'cancel')])
        elif start_date != end_date:
            sale_list = sale_obj.search(cr, uid, [('date_confirm', '>', end_date), ('state', '!=', 'cancel')])
        # elif start_date != end_date and sale_type == 'all':

        # today_to_monthly_sales_list = self.today_to_monthly_sales_trending(cr, uid,start_date=start_date,end_date=end_date)

        # processing_count_labels, processing_count_values, processing_order_values = self.processing_all(cr, uid, start_date=start_date, end_date=end_date, sale_type=sale_type)

        catg_data = self.category_wise_invoice_margin(cr, uid,start_date=start_date,end_date=end_date)
        categ_list = catg_data[0]
        cost_list = catg_data[1]
        sale_list=catg_data[2]
        margin_list=catg_data[3]
        total_sale=catg_data[4]
        total_margin = catg_data[5]
        margin = float(str(catg_data[7]))
        margin_percent = float(str(catg_data[8]))



        classification_wise_sales = catg_data[6]

        c_wise_label=[]
        c_wise_values =[]




        classification_list =[]
        classification_wise_profit_value_list =[]
        classification_wise_profit_percent_list =[]

        for k,v in classification_wise_sales.iteritems():
            if v[0] !=0 and v[1] !=0:
                c_wise_label.append(k)
                classification_list.append(k)
                profit= int((v[0]-v[1]))
                classification_wise_profit_value_list.append(profit)
                pnl = round((((v[0]-v[1])/v[0])*100),2)
                classification_wise_profit_percent_list.append(pnl)
                c_wise_values.append(pnl)


        # turn_oder_data = self.tunr_over_data_calculation(cr, uid,start_date=start_date,end_date=end_date)
        # month_on_month_oder_data = self.monthon_month_value(cr, uid,start_date=start_date,end_date=end_date)


        result_list = [
            categ_list,
            [str(x) for x in cost_list],
            [str(x if x>=0 else 0) for x in sale_list],
            [str(x) for x in margin_list],
            total_sale,
            total_margin,
            c_wise_label,
            c_wise_values,
            # turn_oder_data[0],
            # turn_oder_data[1],
            # turn_oder_data[2]

        ]

        # result_list = [0,0,0,0, 0,0,
        #     c_wise_label,
        #     c_wise_values,
        #     turn_oder_data[0],
        #     turn_oder_data[1],
        #     turn_oder_data[2] ]

        sec_list=[
                  margin,
                  margin_percent]

        # sec_list = [processing_count_labels,
        #             processing_count_values,
        #             processing_order_values,
        #             0,0]
        new_bar_chart = [classification_list,classification_wise_profit_value_list,classification_wise_profit_percent_list]

        # final_list =  today_to_monthly_sales_list + result_list + month_on_month_oder_data+ sec_list + new_bar_chart
        final_list = result_list + sec_list + new_bar_chart

        return final_list

    def generate_cogs_report(self, cr, uid, data, objects, context=None):

        c_specs = [
            ['order_date', 1, 0, 'text', ''],
            ['order', 1, 0, 'text', ''],
            ['return', 1, 0, 'text', ''],
            ['odoo_order', 1, 0, 'text', ''],
            ['customer_name', 1, 0, 'text', ''],
            ['customer_email', 1, 0, 'text', ''],
            ['company_name', 1, 0, 'text', ''],
            ['product_name', 1, 0, 'text', ''],
            ['product_category', 1, 0, 'text', ''],
            ['delivered_qty', 1, 0, 'text', ''],
            ['cost_rate_avg', 1, 0, 'text', ''],
            ['total_cost', 1, 0, 'text', ''],
            ['sales_rate', 1, 0, 'text', ''],
            ['total_sales', 1, 0, 'text', ''],
            ['profit', 1, 0, 'text', ''],
            ['status', 1, 0, 'text', ''],
            ['transfer_date', 1, 0, 'text', '']
        ]
        """
        ['order_date', 1, 0, 'text', ''],
        ['order', 1, 0, 'text', u'1000018500'],
        ['return', 1, 0, 'text', ''],
        ['odoo_order', 1, 0, 'text', u'SO015'],
        ['customer_name', 1, 0, 'text', ''],
        ['company_name', 1, 0, 'text', ''],
        ['product_name', 1, 0, 'text', ''],
        ['product_category', 1, 0, 'text', ''],
        ['delivered_qty', 1, 0, 'text', ''],
        ['cost_rate_avg', 1, 0, 'text', ''],
        ['total_cost', 1, 0, 'text', ''],
        ['sales_rate', 1, 0, 'text', ''],
        ['total_sales', 1, 0, 'text', ''],
        ['profit', 1, 0, 'text', ''],
        ['status', 1, 0, 'text', u'done']
        """

        for c_s in c_specs:

            if c_s[0] == 'order':
                c_specs[1][4] = str(objects.mag_no)
            if c_s[0] == 'odoo_order':
                c_specs[3][4] = str(objects.origin)
            if c_s[0] == 'status':
                c_specs[14][4] = str(objects.state)

        cr.execute(
            "SELECT l.product_id,(SUM(l.price_total)/SUM(l.quantity)) AS total FROM purchase_report l WHERE state!='cancel' GROUP BY l.product_id ")
        res = dict(cr.fetchall())
        sale_discount = 0

        # Sale Order lines
        for line in objects:

            # c_specs = map(lambda x: self.render(x, self.col_specs_template, 'lines'),  wanted_list)

            sale_obj = self.pool.get('sale.order')

            order_no = line.origin

            if str(line.picking_type_id.code) == 'incoming':
                order_no = line.group_id.name

            if "SO" not in str(order_no):
                order_no = line.group_id.name

            sale_ids = sale_obj.search(cr, uid, [('name', '=', order_no)], limit=1)

            sale_data = sale_obj.browse(cr, uid, sale_ids, context=context)

            purchase_report = self.pool.get('purchase.report')

            sales_obj = sale_data[0] if len(sale_data) > 0 else None

            order_date = ''

            product_dict = {}

            if sales_obj is not None:
                order_date = sales_obj.date_confirm
                for ss in sales_obj.order_line:
                    product_dict[ss.product_id.id] = ss.price_unit
                    if ss.product_id.id == 20044:
                        sale_discount += ss.price_unit

            for items in line.move_lines:
                sale_price = 0

                for list_data in c_specs:
                    if str(line.picking_type_id.code) == 'incoming':
                        if str(list_data[0]) == str('order'):
                            try:
                                order_name = str(sales_obj.client_order_ref)

                            except:

                                try:
                                    order_name = sales_obj.client_order_ref
                                    order_name = order_name.encode('utf-8')
                                    order_name = order_name.decode('utf-8')
                                except:
                                    order_name = ''

                            list_data[4] = order_name

                    if str(list_data[0]) == str('return'):
                        if str(line.picking_type_id.code) == 'incoming':
                            list_data[4] = str('Return')

                    if str(list_data[0]) == str('product_name'):
                        try:
                            decoded_product_name = str(items.product_id.name)

                        except:
                            product_name = items.product_id.name
                            product_name = product_name.encode('utf-8')
                            decoded_product_name = product_name.decode('utf-8')
                        list_data[4] = decoded_product_name

                    if str(list_data[0]) == str('product_category'):
                        try:
                            decoded_product_category = str(items.product_id.categ_id.name)

                        except:
                            product_categ_name = items.product_id.categ_id.name
                            product_categ_name = product_categ_name.encode('utf-8')
                            decoded_product_category = product_categ_name.decode('utf-8')
                        list_data[4] = decoded_product_category

                    if str(list_data[0]) == str('customer_name'):
                        try:
                            decoded_partner_name = str(line.customer_name)

                        except:
                            partner_name = line.customer_name
                            partner_name = partner_name.encode('utf-8')
                            decoded_partner_name = partner_name.decode('utf-8')
                        list_data[4] = decoded_partner_name

                    parent = line.partner_id
                    for i in range(5):
                        if len(parent.parent_id) == 1:
                            parent = parent.parent_id
                        else:
                            customer_email = parent.email
                            company_name = parent.name
                            break

                    if str(list_data[0]) == str('customer_email'):

                        try:
                            decoded_partner_email = str(customer_email)

                        except:
                            partner_email = customer_email
                            partner_email = partner_email.encode('utf-8')
                            decoded_partner_email = partner_email.decode('utf-8')
                        list_data[4] = decoded_partner_email

                    if str(list_data[0]) == str('company_name'):
                        try:
                            # decoded_partner_name = str(line.customer_name)
                            decoded_partner_name = str(company_name)

                        except:
                            partner_name = company_name
                            partner_name = partner_name.encode('utf-8')
                            decoded_partner_name = partner_name.decode('utf-8')
                        list_data[4] = decoded_partner_name

                    if str(list_data[0]) == str('delivered_qty'):
                        delivered_qty = items.product_uom_qty if items.product_uom_qty else '0'
                        list_data[4] = str(delivered_qty)

                    for key, val in product_dict.iteritems():
                        if key == items.product_id.id:
                            sale_price = val
                            break

                    if str(list_data[0]) == str('cost_rate_avg'):
                        avg_cost_price = 0
                        inventory_value = 0
                        delivered_qty = 0

                        for moved_data in items.quant_ids:
                            if moved_data.product_id.id == items.product_id.id:
                                inventory_value += moved_data.inventory_value
                                delivered_qty += moved_data.qty

                        if delivered_qty > 0:
                            avg_cost_price = round(inventory_value / delivered_qty, 2)
                        else:
                            if res.get(items.product_id.id):
                                avg_cost_price = res.get(items.product_id.id)

                            if avg_cost_price == 0:
                                if items.product_id.average_purchaes_price > 0:
                                    avg_cost_price = items.product_id.average_purchaes_price
                                else:
                                    if items.product_id.standard_price > 0:
                                        avg_cost_price = items.product_id.standard_price
                                    else:
                                        avg_cost_price = sale_price * 0.925

                            avg_cost_price = round(avg_cost_price, 2)

                        list_data[4] = str(avg_cost_price)

                    if str(list_data[0]) == str('total_cost'):

                        avg_cost_price = 0
                        inventory_value = 0
                        delivered_qty = 0

                        for moved_data in items.quant_ids:
                            if moved_data.product_id.id == items.product_id.id:
                                inventory_value += moved_data.inventory_value
                                delivered_qty += moved_data.qty

                        if delivered_qty > 0:
                            avg_cost_price = round(inventory_value / delivered_qty, 2)
                        else:
                            if res.get(items.product_id.id):
                                avg_cost_price = res.get(items.product_id.id)

                            if avg_cost_price == 0:
                                if items.product_id.average_purchaes_price > 0:
                                    avg_cost_price = items.product_id.average_purchaes_price
                                else:
                                    if items.product_id.standard_price > 0:
                                        avg_cost_price = items.product_id.standard_price
                                    else:
                                        avg_cost_price = sale_price * 0.925

                            avg_cost_price = round(avg_cost_price, 2)

                        delivered_qty = items.product_uom_qty if items.product_uom_qty else 0.00

                        if str(line.picking_type_id.code) == 'incoming':
                            list_data[4] = str(avg_cost_price * delivered_qty * -1)
                        else:
                            list_data[4] = str(avg_cost_price * delivered_qty)

                    if str(list_data[0]) == str('sales_rate'):
                        list_data[4] = str(sale_price)

                    if str(list_data[0]) == str('total_sales'):
                        if str(line.picking_type_id.code) == 'incoming':
                            total_sale = sale_price * items.product_uom_qty * -1
                        else:
                            total_sale = sale_price * items.product_uom_qty

                        list_data[4] = str(total_sale)

                    if str(list_data[0]) == str('order_date'):
                        list_data[4] = str(order_date)

                    if str(list_data[0]) == str('transfer_date'):
                        # list_data[4] = str(order_date)
                        list_data[4] = str(items.write_date) if str(items.state) == "done" else None

                    if str(list_data[0]) == str('profit'):

                        avg_cost_price = 0
                        inventory_value = 0
                        delivered_qty = 0

                        for moved_data in items.quant_ids:
                            if moved_data.product_id.id == items.product_id.id:
                                inventory_value += moved_data.inventory_value
                                delivered_qty += moved_data.qty

                        if delivered_qty > 0:
                            avg_cost_price = round(inventory_value / delivered_qty, 2)
                        else:
                            if res.get(items.product_id.id):
                                avg_cost_price = res.get(items.product_id.id)

                            if avg_cost_price == 0:
                                if items.product_id.average_purchaes_price > 0:
                                    avg_cost_price = items.product_id.average_purchaes_price
                                else:
                                    if items.product_id.standard_price > 0:
                                        avg_cost_price = items.product_id.standard_price
                                    else:
                                        avg_cost_price = sale_price * 0.925

                            avg_cost_price = round(avg_cost_price, 2)

                        profit = (sale_price * items.product_uom_qty) - (avg_cost_price * items.product_uom_qty)
                        if str(line.picking_type_id.code) == 'incoming':
                            profit = profit * -1

                        list_data[4] = str(profit)

                sold = dict()
                for l in c_specs:
                    sold[l[0]] = l[4]

                order_cogs_query = "INSERT INTO sales_cogs_margin (" \
                                  "order_date, " \
                                  "order_no, " \
                                  "return, " \
                                  "odoo_order, " \
                                  "customer_name, " \
                                  "customer_email, " \
                                  "company_name, " \
                                  "product_name, " \
                                  "product_category, " \
                                  "delivered_qty, " \
                                  "cost_rate_avg, " \
                                  "total_cost, " \
                                  "sales_rate, " \
                                  "total_sales, " \
                                  "profit, " \
                                  "status, " \
                                  "transfer_date) VALUES ('{0}', '{1}', '{2}', '{3}', $${4}$$, $${5}$$, $${6}$$, $${7}$$, $${8}$$, '{9}', '{10}', '{11}', '{12}', '{13}', '{14}','{15}','{16}')".format(sold['order_date'], sold['order'], sold['return'], sold['odoo_order'], sold['customer_name'], sold['customer_email'], sold['company_name'], sold['product_name'], sold['product_category'], sold['delivered_qty'], sold['cost_rate_avg'], sold['total_cost'], sold['sales_rate'], sold['total_sales'], sold['profit'], sold['status'], sold['transfer_date'])

                cr.execute(order_cogs_query)
                cr.commit()

                # row_data = self.xls_row_template(c_specs, [x[0] for x in c_specs])
                # row_pos = self.xls_write_row(ws, row_pos, row_data, row_style=self.aml_cell_style)

        return True

    def truncate_table(self, cr, uid, table_name, context=None):

        # truncate table
        trunc_query = "TRUNCATE TABLE " + table_name
        cr.execute(trunc_query)
        cr.commit()

        return True

    def chunks(self, l, n):
        """Yield successive n-sized chunks from l."""
        for i in xrange(0, len(l), n):
            yield l[i:i + n]

    def so_cogs_30_days_update_emergency(self, cr, uid, ids, context=None):

        stpick_obj = self.pool.get('stock.picking')
        # order_end_obj = self.pool.get('order.end.to.end.log')

        date_before_30 = datetime.datetime.now() - datetime.timedelta(days=11)
        date_before_30 = date_before_30.strftime('%Y-%m-%d')

        # [['state', '=', 'done'], ['group_id', 'ilike', 'so'], ['partner_id', '!=', False]]
        picking_list = stpick_obj.search(cr, uid, [("state", "=", 'done'), ("group_id", "ilike", "so"), ("partner_id", '!=', False), ("create_date", '>', date_before_30)], context=context)

        picking_list_chunks = [x for x in self.chunks(picking_list, 200)]

        # truncate table
        # table_name = "sales_cogs_margin"
        # self.truncate_table(cr, uid, table_name, context=context)
        delete_qury = "DELETE FROM sales_cogs_margin WHERE order_date > '{0}'".format(date_before_30)
        cr.execute(delete_qury)
        cr.commit()

        for picking_chunk in picking_list_chunks:
            for pick in stpick_obj.browse(cr, uid, picking_chunk, context=context):
                try:

                    self.generate_cogs_report(cr, uid, None, pick, context)

                except:
                    pass

        return True

    def so_cogs_seven_months_one(self, cr, uid, ids, context=None):

        # search without cancel
        stpick_obj = self.pool.get('stock.picking')

        date_before_210 = datetime.datetime.now() - datetime.timedelta(days=210)
        date_before_210 = date_before_210.strftime('%Y-%m-%d')

        picking_list = stpick_obj.search(cr, uid, [("state", "=", 'done'), ("group_id", "ilike", "so"), ("partner_id", '!=', False), ("create_date", '>', date_before_210)], context=context)

        picking_list_chunks = [x for x in self.chunks(picking_list, 200)]

        # truncate table
        table_name = "sales_cogs_margin"
        self.truncate_table(cr, uid, table_name, context=context)

        for picking_chunk in picking_list_chunks:
            for pick in stpick_obj.browse(cr, uid, picking_chunk, context=context):
                try:

                    self.generate_cogs_report(cr, uid, None, pick, context)

                except:
                    pass

        self.so_cogs_seven_months_two(cr, uid, ids, context=context)

        return True

    def so_cogs_seven_months_two(self, cr, uid, ids, context=None):

        # search without cancel
        stpick_obj = self.pool.get('stock.picking')

        date_before_210 = datetime.datetime.now() - datetime.timedelta(days=210)
        date_before_210 = date_before_210.strftime('%Y-%m-%d')

        picking_list = stpick_obj.search(cr, uid, [("state", "=", 'done'), ("group_id", "ilike", "so"), ("partner_id", '!=', False), ("create_date", '<', date_before_210)], context=context)

        picking_list_chunks = [x for x in self.chunks(picking_list, 200)]

        for picking_chunk in picking_list_chunks:
            for pick in stpick_obj.browse(cr, uid, picking_chunk, context=context):
                try:

                    self.generate_cogs_report(cr, uid, None, pick, context)

                except:
                    pass

        return True

    def monthly_sales_trending(self, cr, uid, start_date=None, end_date=None, sale_type=None, context=None):
        uid = 1
        tmp = start_date
        end_date = tmp

        start_date = self.present_month_first_date()

        #### Sales Data calculation
        sales_query = "select customer_classification,count(id) as sale_count,sum(amount_total) as total from sale_order where state in ('done','progress','manual','shipping_except','invoice_except' ) and date_confirm>=%s and date_confirm<=%s group by customer_classification"

        cr.execute(sales_query, (start_date, end_date))

        sales_data = cr.dictfetchall()

        sale_data_dict = {}
        sale_data_count_dict = {}
        total_sale = 0
        total_today_label = []
        total_today_value = []
        total_today_count = []

        for items in sales_data:

            try:
                total_sale += items.get('total')
            except:
                pass

            if 'Ananta' in items.get('customer_classification'):
                sale_data_dict['Ananta'] = items.get('total')
                sale_data_count_dict['Ananta'] = items.get('sale_count')
            elif 'Corporate' in items.get('customer_classification') or 'SME' in items.get(
                    'customer_classification') or 'SOHO' in items.get('customer_classification') \
                    or 'HORECA' in items.get('customer_classification'):
                # sale_data_dict['Corporate'] = items.get('total')
                # sale_data_count_dict['Corporate'] = items.get('sale_count')

                if sale_data_dict.get('Corporate') is not None:
                    sale_data_dict['Corporate'] += items.get('total')
                    sale_data_count_dict['Corporate'] += items.get('sale_count')
                else:
                    sale_data_dict['Corporate'] = items.get('total')

                    sale_data_count_dict['Corporate'] = items.get('sale_count')

            elif 'Retail' in items.get('customer_classification'):

                if sale_data_dict.get('Retail') is not None:
                    sale_data_dict['Retail'] += items.get('total')
                    sale_data_count_dict['Retail'] += items.get('sale_count')
                else:
                    sale_data_dict['Retail'] = items.get('total')

                    sale_data_count_dict['Retail'] = items.get('sale_count')

            else:
                sale_data_dict['General'] = items.get('total')
                sale_data_count_dict['General'] = items.get('sale_count')


        for k, v in sale_data_dict.iteritems():
            total_today_label.append(k)
            total_today_value.append(v)
            total_today_count.append(sale_data_count_dict.get(k))

        #### Sales Data calculation Ends Here



        return [total_today_label, total_today_value,total_today_count]

    # from datetime import date, timedelta, datetime

    def days_cur_month(self):
        date_before_30 = datetime.datetime.now() - datetime.timedelta(days=30)
        # date_before_30 = date_before_30.strftime('%Y-%m-%d')

        m = datetime.datetime.now().month
        y = datetime.datetime.now().year

        if m==12:
            ndays = 31

        else:
            ndays = (datetime.date(y, m + 1, 1) - datetime.date(y, m, 1)).days
        d1 = datetime.date(date_before_30.year, date_before_30.month, date_before_30.day)
        d2 = datetime.date(y, m, ndays)
        delta = d2 - d1

        return [(d1 + timedelta(days=i)).strftime('%Y-%m-%d') for i in range(delta.days + 1)]

    def sales_value_monthly(self, cr, uid, start_date=None, end_date=None, sale_type=None, context=None):
        tmp = start_date
        end_date = tmp
        start_date = (datetime.datetime.now() - datetime.timedelta(days=30)).strftime('%Y-%m-%d')
        #### Sales Data calculation
        # if sale_type and sale_type!='all':
        #     classifiation_type = '%' + str(sale_type) + '%'
        #     sales_query = "SELECT sum(amount_total) AS total, count(*) AS order_count, date_confirm FROM sale_order WHERE state IN ('done', 'progress', 'manual', 'shipping_except', 'invoice_except') AND date_confirm >= '{0}' AND date_confirm <= '{1}' AND customer_classification LIKE '{2}' GROUP BY date_confirm ORDER BY date_confirm".format(start_date, end_date,classifiation_type)
        # else:
        sales_query = "SELECT sum(amount_total) AS total, count(*) AS order_count, date_confirm FROM sale_order WHERE state IN ('done', 'progress', 'manual', 'shipping_except', 'invoice_except') AND date_confirm >= '{0}' AND date_confirm <= '{1}' GROUP BY date_confirm ORDER BY date_confirm".format(start_date, end_date)

        cr.execute(sales_query)

        sales_data = cr.dictfetchall()
        current_date_list = self.days_cur_month()

        date_list = []
        sales_value= []
        sales_order_count= []

        for item in sales_data:
            if item.get('date_confirm') in current_date_list:

                sales_value.append(item['total'])
                sales_order_count.append(int(item['order_count']))
            else:
                sales_value.append(0.00)
                sales_order_count.append(int(0))
            date_list.append(item['date_confirm'])

        #### Sales Data calculation Ends Here
        return [date_list,
                sales_value,
                sales_order_count
                ]

    def today_to_monthly_delivery(self, cr, uid, start_date = None, end_date = None, sale_type = None, context = None):
        uid=1
        tmp = start_date
        start_date = end_date
        end_date = tmp

        ### Invoice Calcualtion Starts Here

        total_invoice_query = "select sum(account_invoice.amount_total) as total,sale_order.customer_classification as customer_classification,account_invoice.type,delivered from account_invoice,sale_order_invoice_rel,sale_order where account_invoice.id=sale_order_invoice_rel.invoice_id and sale_order_invoice_rel.order_id=sale_order.id and account_invoice.type in ('out_invoice','out_refund') and account_invoice.delivery_date>=%s and account_invoice.state  in ('open','paid') and account_invoice.delivery_date <%s group by account_invoice.type,sale_order.customer_classification, delivered"
        end_date = self.date_plus_one(end_date)

        cr.execute(total_invoice_query, (start_date, end_date))

        total_invoice_data = cr.dictfetchall()

        # invoice_data_dict = {}
        # invoice_data_dict['Ananta'] = 0
        # invoice_data_dict['Corporate'] = 0
        # invoice_data_dict['Retail'] = 0
        # invoice_data_dict['General'] = 0
        # total_invoice_amount = 0
        total_invoice_count = 0
        # total_invoice_label = []
        # total_invoice_value = []

        # refund_invoice_dict ={}
        # refund_invoice_dict['Ananta'] =0
        # refund_invoice_dict['Corporate'] =0
        # refund_invoice_dict['Retail'] =0
        # refund_invoice_dict['General'] =0
        # refund_invoice_amount=0
        refund_invoice_count=0
        # refund_invoice_level=[]
        # refund_invoice_value=[]

        net_ananta = 0
        net_general=0
        net_corporate = 0
        net_retail = 0
        total_invoice_amount = 0

        for items in total_invoice_data:
            if items.get('customer_classification') is None or items.get('customer_classification') is False:
                items_classification='General'
            else:
                items_classification = items.get('customer_classification')

            if items.get('type') == 'out_invoice' and items.get('delivered') == True:
                try:
                    total_invoice_amount += items.get('total')
                except:
                    pass

                if 'Ananta' in items_classification:
                    # invoice_data_dict['Ananta'] = items.get('total')
                    net_ananta += items.get('total')

                elif 'Corporate' in items_classification or 'SME' in items.get(
                        'customer_classification') or 'SOHO' in items_classification or 'HORECA' in items_classification:

                    # if invoice_data_dict.get('Corporate') is not None:
                    #     invoice_data_dict['Corporate'] += items.get('total')
                    # else:
                    #     invoice_data_dict['Corporate'] = items.get('total')

                    net_corporate += items.get('total')

                elif 'Retail' in items_classification :

                    # if invoice_data_dict.get('Retail') is not None:
                    #     invoice_data_dict['Retail'] += items.get('total')
                    # else:
                    #     invoice_data_dict['Retail'] = items.get('total')
                    net_retail += items.get('total')

                else:
                    # invoice_data_dict['General'] = items.get('total')
                    net_general += items.get('total')

            else:
                # try:
                #     refund_invoice_amount += items.get('total')
                # except:
                #
                #     pass
                if items.get('type') == 'out_refund':

                    if 'Ananta' in items_classification:
                        # refund_invoice_dict['Ananta'] = items.get('total')
                        net_ananta -=items.get('total')

                    elif 'Corporate' in items_classification or 'SME' in items_classification or 'SOHO' in items_classification or 'HORECA' in items_classification:
                        # if refund_invoice_dict.get('Corporate') is not None:
                        #     refund_invoice_dict['Corporate'] += items.get('total')
                        # else:
                        #     refund_invoice_dict['Corporate'] = items.get('total')
                        net_corporate -=items.get('total')

                    elif 'Retail' in items_classification :
                        # if refund_invoice_dict.get('Retail') is not None:
                        #     refund_invoice_dict['Retail'] += items.get('total')
                        # else:
                        #     refund_invoice_dict['Retail'] = items.get('total')

                        net_retail -=items.get('total')

                    else:
                        # refund_invoice_dict['General'] = items.get('total')
                        net_general -=items.get('total')


        # for k, v in invoice_data_dict.iteritems():
        #     total_invoice_label.append(k)
        #     total_invoice_value.append(v)

        # for k, v in refund_invoice_dict.iteritems():
        #     refund_invoice_level.append(k)
        #     refund_invoice_value.append(v)

        inv_count_query= "select count(id) as total from account_invoice where type='out_invoice' and delivered=TRUE and state  in ('open','paid') and delivery_date>=%s and delivery_date <%s"

        cr.execute(inv_count_query, (start_date, end_date))
        inv_count_data = cr.dictfetchall()

        if len(inv_count_data) >0:
            total_invoice_count=inv_count_data[0].get('total')

        ref_inv_count_query = "select count(id) as total from account_invoice where type='out_refund' and state  in ('open','paid') and delivery_date>=%s and delivery_date <%s"
        cr.execute(ref_inv_count_query, (start_date, end_date))

        ref_inv_count_data = cr.dictfetchall()

        net_invoice_level=[]
        net_invoice_value=[]

        if len(ref_inv_count_query) > 0:
            refund_invoice_count = ref_inv_count_data[0].get('total')

        total_delivery_count = total_invoice_count - refund_invoice_count

        net_delivery_amount= net_ananta + net_general + net_retail + net_corporate
        # margin=0

        if net_ananta !=0:
            net_invoice_level.append('Ananta')
            net_invoice_value.append(net_ananta)
        else:
            net_invoice_level.append('Ananta')
            net_invoice_value.append(0)
        if net_corporate !=0:
            net_invoice_level.append('Corporate')
            net_invoice_value.append(net_corporate)
        else:
            net_invoice_level.append('Corporate')
            net_invoice_value.append(0)
        if net_retail != 0:
            net_invoice_level.append('Retail')
            net_invoice_value.append(net_retail)
        else:
            net_invoice_level.append('Retail')
            net_invoice_value.append(0)

        if net_retail != 0:
            net_invoice_level.append('General')
            net_invoice_value.append(net_general)
        else:
            net_invoice_level.append('General')
            net_invoice_value.append(0)
            ## Net invoice
            ## Net Invoices Ends here

        ### Invoice Amount Ends here

        return [float(str(net_delivery_amount)),total_delivery_count,net_invoice_level,net_invoice_value]


class sale_order(osv.osv):
    _inherit = "sale.order"

    def cogs_seven_months_o(self, cr, uid, ids, context=None):

        socogs_obj = self.pool.get('sales.cogs.margin')
        socogs_obj.so_cogs_seven_months_one(cr, uid, ids, context=context)

        return True

    def cogs_seven_months_t(self, cr, uid, ids, context=None):

        socogs_obj = self.pool.get('sales.cogs.margin')
        socogs_obj.so_cogs_seven_months_two(cr, uid, ids, context=context)

        return True

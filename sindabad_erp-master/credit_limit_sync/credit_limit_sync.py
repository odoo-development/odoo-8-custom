import datetime
from lxml import etree
import math
import pytz
import urlparse

import openerp
from openerp import tools, api
from openerp.osv import osv, fields
from openerp.osv.expression import get_unaccent_wrapper
from openerp.tools.translate import _
from ..odoo_to_magento_api_connect.api_connect import get_magento_token, submit_request
import json
import requests
import time
from datetime import datetime, timedelta
import datetime
from openerp.report import report_sxw

from ..customer_statement.report.partner_statement import PartnerStatement


class res_partner(osv.osv):
    _inherit = "res.partner"

    _columns = {
        'other_outstanding': fields.float(string='Other Outstanding', default=0),
    }

    def write(self, cr, uid, ids, vals, context=None):

        # check if the value is changed from existing (credit limit, monthly limit, credit days)
        if vals.has_key('credit_days') or vals.has_key('monthly_limit') or vals.has_key('credit_limit'):

            if len(ids) == 1:
                rs_p_obj = self.pool.get('res.partner')
                for rs in rs_p_obj.browse(cr, uid, ids, context=context):
                    if rs.is_company and rs.magento_company_id != 0:

                        try:
                            so_obj = self.pool.get('sale.order')
                            sale_order_ids = so_obj.search(cr, uid, [('client_order_ref', '!=', None)], limit=5)
                            token, url_root = get_magento_token(self, cr, uid, sale_order_ids)
                        except:
                            token, url_root = None, None

                        # try:
                        #     if token and url_root:
                        #         self.credit_limit_sync(cr, uid, ids, token, url_root, vals, context=context)
                        #         # outstanding_sync
                        #         # sync outstanding manually
                        #         # self.outstanding_sync(cr, uid, ids, token, url_root, context=context)
                        # except:
                        #     pass

        return super(res_partner, self).write(cr, uid, ids, vals, context=context)

    # def get_current_outstanding(self, cr, uid, ids, partner, context=None):
    #     parent_list = []
    #
    #     if len(partner.parent_id) > 0:
    #         parent_list.append(partner.parent_id.id)
    #
    #         cr.execute("select id from  res_partner where customer=True and id=%s", ([partner.parent_id.id]))
    #
    #         for items in cr.fetchall():
    #             parent_list.append(items[0])
    #     else:
    #         parent_list.append(partner.id)
    #
    #     cr.execute("select id from res_partner where customer=True and parent_id=%s", ([partner.id]))
    #     for items in cr.fetchall():
    #         parent_list.append(items[0])
    #
    #     # start_date = context.get('start_date') #############################
    #     # give today date
    #     start_date = datetime.datetime.today().date().strftime('%Y-%m-%d')
    #
    #     moveline_obj = self.pool.get('account.move.line')
    #     movelines = moveline_obj.search(cr, uid,
    #                                     [('partner_id', 'in', parent_list),
    #                                      ('account_id.type', 'in', ['receivable', 'payable']),
    #                                      ('state', '<>', 'draft'), ('date', '<=', start_date)])
    #
    #     movelines = moveline_obj.browse(cr, uid, movelines)
    #
    #     previous_balance = 0
    #
    #     for items in movelines:
    #         previous_balance += (items.debit - items.credit)
    #     return previous_balance

    def outstanding_sync(self, cr, uid, partner_id, token, url_root, context=None):

        rs_p_obj = self.pool.get('res.partner')
        parent_id = partner_id

        list_of_company_dicts = []

        for partner in rs_p_obj.browse(cr, uid, parent_id, context=context):

            try:
                monthly_limit= False
                partner_exceed_limit=True
                if partner.credit_limit and partner.credit_limit > 0:
                    monthly_limit=partner.monthly_limit
                    partner_exceed_limit=partner.x_exceed_limit
                    map_company_data ={
                        "company_id":partner.magento_company_id,
                        "company_credit":{
                            "credit_limit": partner.credit_limit,
                            "balance": round(float(partner.statement_balance),2)*(-1),
                            "credit_days": partner.credit_days,
                            "monthly_limit": monthly_limit,
                            "exceed_limit": partner_exceed_limit


                        }
                    }


                    if partner.magento_company_id != 0:
                        list_of_company_dicts.append(map_company_data)
            except:
                pass
        magento_data = {"companyCredits": list_of_company_dicts}
        data = json.dumps(magento_data)
        try:
            if token:
                token = token.replace('"', "")
                headers = {'Authorization': token, 'Content-Type': 'application/json'}

                post_url = url_root + "/index.php/rest/V1/odoomagentoconnect/companyCreditBulk"

                post_resp = requests.post(post_url, data=data, headers=headers)
        except:
            pass



        return True

    # def credit_limit_sync(self, cr, uid, partner_ids, token, url_root, vals=None, context=None):
    #
    #     # api call for credit limit sync
    #     parent_ids = list()
    #     # cus_com = dir()
    #
    #     so_obj = self.pool.get('sale.order')
    #     rs_p_obj = self.pool.get('res.partner')
    #
    #     # get only company ids from partner_ids
    #     for ptr in rs_p_obj.browse(cr, uid, partner_ids, context=context):
    #
    #         try:
    #             if len(ptr.parent_id) > 0:
    #                 if ptr.parent_id.id not in parent_ids:
    #                     parent_ids.append(int(ptr.parent_id.id))
    #                     # cus_com[str(ptr.id)] = ptr.parent_id
    #             else:
    #                 if ptr.id not in parent_ids:
    #                     parent_ids.append(int(ptr.id))
    #         except:
    #             parent_ids.append(int(ptr.id))
    #
    #     if token:
    #         parent_ids = list(dict.fromkeys(parent_ids))
    #         for partner in rs_p_obj.browse(cr, uid, parent_ids, context=context):
    #
    #             try:
    #                 if partner.is_company and partner.magento_company_id != 0:
    #                     # call magento api to send (credit limit, monthly limit, credit days)
    #                     credit_limit = None
    #                     monthly_limit = None  # do not send now
    #                     credit_days = None  # do not send now
    #                     company_id = partner.id
    #
    #
    #                     map_company_data = dict()
    #                     last_order_id = None
    #                     last_order_amount_total = 0
    #                     credit_limit_exceed = False
    #
    #                     # get company's customer
    #                     cust_list = rs_p_obj.search(cr, uid, [('parent_id', '=', int(partner.id))])
    #                     cust_list.append(partner.id)
    #                     cust_list = list(dict.fromkeys(cust_list))  # make unique customer
    #
    #                     # customer last order
    #
    #                     cust_data = None
    #                     if len(cust_list) == 1:
    #                         cust_data = '(%s)' % ', '.join(map(repr, tuple(cust_list)))
    #                     else:
    #                         cust_data = tuple(cust_list)
    #
    #                     last_order_query = "SELECT id FROM sale_order WHERE partner_id IN {0} ORDER BY id DESC LIMIT 1".format(cust_data)
    #                     cr.execute(last_order_query)
    #
    #                     for lo_id in cr.fetchall():
    #                         last_order_id = int(lo_id[0])
    #
    #                     if last_order_id:
    #                         last_order = so_obj.browse(cr, uid, [int(last_order_id)], context=context)
    #                         last_order_amount_total = last_order.amount_total
    #
    #                     # self.get_company_balance(cr, uid, [partner.id], context=context)
    #                     # partner_stat_obj = PartnerStatement(cr, uid, '', context)
    #                     # stat_data = {'model': 'customer.report.statement', 'ids': [partner.id], 'form': {'date_from': datetime.datetime.today(), 'id': uid, 'date_to': datetime.datetime.today()}}
    #                     # cus_st_data = partner_stat_obj.set_context(partner, stat_data, [partner.id], report_type=None)
    #
    #                     if partner.credit_limit > 0:
    #                         total_receivable_amount = partner.credit + last_order_amount_total
    #
    #                         if total_receivable_amount > partner.credit_limit:
    #                             credit_limit_exceed = True
    #                         else:
    #                             credit_limit_exceed = False
    #
    #                     # Now sync with company_id
    #                     map_company_data['company_id'] = partner.magento_company_id
    #                     # map_company_data['exceed_limit'] = credit_limit_exceed
    #                     # Khasru vai asked to set ('exceed_limit' = True) for everybody
    #                     map_company_data['exceed_limit'] = True
    #                     extension_attributes = dict()
    #                     if vals:
    #                         # prepare sync data
    #                         for key in vals.keys():
    #
    #                             if key == 'monthly_limit':
    #                                 extension_attributes['monthly_limit'] = vals[str(key)]
    #                             elif key == 'credit_days':
    #                                 extension_attributes['credit_days'] = vals[str(key)]
    #                             else:
    #                                 map_company_data[str(key)] = vals[str(key)]
    #
    #                         if extension_attributes:
    #                             map_company_data['extension_attributes'] = extension_attributes
    #
    #                     else:
    #                         # sync company's all cl, ml, cd values
    #                         if partner.parent_id:
    #                             credit_limit = partner.parent_id.credit_limit
    #                             extension_attributes['monthly_limit'] = partner.parent_id.monthly_limit
    #                             extension_attributes['credit_days'] = partner.parent_id.credit_days
    #                         else:
    #                             credit_limit = partner.credit_limit
    #                             extension_attributes['monthly_limit'] = partner.monthly_limit
    #                             extension_attributes['credit_days'] = partner.credit_days
    #
    #                         # prepare sync data
    #                         map_company_data['credit_limit'] = credit_limit
    #                         map_company_data['extension_attributes'] = extension_attributes
    #
    #                     map_company_data["currency_code"] = "BDT"
    #
    #                     token = token.replace('"', "")
    #                     headers = {'Authorization': token, 'Content-Type': 'application/json'}
    #
    #                     if partner.magento_company_id:
    #                         # before update get magento company credit limit and save to res.partner table only once
    #                         get_url = url_root + "/rest/V1/companyCredits/company/"+str(partner.magento_company_id)
    #                         get_resp = requests.get(get_url, headers=headers)
    #                         company_data = json.loads(get_resp.text)
    #
    #                         credit_id = company_data['id']
    #                         map_company_data["id"] = credit_id
    #
    #                         # sync to magento "PUT" request
    #                         url = url_root + "/rest/V1/companyCredits/"+str(credit_id)
    #
    #                         ## ----------delete this-block---------
    #                         """
    #                         # monthly_limit = None  # do not send now
    #                         # credit_days = None  # do not send now
    #                         # map_company_data["credit_limit"] = 220000
    #                         if map_company_data["monthly_limit"]:
    #                             del map_company_data["monthly_limit"]
    #
    #                         # if map_company_data["credit_days"]:
    #                         #    del map_company_data["credit_days"]
    #                         """
    #                         ## ----------delete this-block---------
    #
    #                         cl_data = dict()
    #                         cl_data["creditLimit"] = map_company_data
    #                         data = json.dumps(cl_data)
    #
    #                         resp = requests.put(url, data=data, headers=headers)
    #             except:
    #
    #                 pass
    #     return True
    # #
    # # def balance_cal(self, mag_bal, odo_bal):
    # #
    # #     if mag_bal < 0:
    # #         mag_bal = -1 * mag_bal
    # #
    # #     if odo_bal < 0:
    # #         odo_bal = -1 * odo_bal
    # #
    # #     balance = odo_bal - mag_bal
    # #
    # #     balance = -1 * balance if balance < 0 else balance
    # #
    # #     return balance
    #
    # # def reimburse_sync(self, cr, uid, ids, data, token, url_root, context=None):
    # #
    # #     """
    # #     (Pdb) data
    # #     {'partner_id': 439, 'paid_amount': 900.0, 'order_ref': '1000035435', 'outstanding': 'credit_outstanding', 'paid_amount': 800}
    # #     """
    # #
    # #     co_out_his_obj = self.pool.get('company.outstanding.history')
    # #     # partner = self.browse(cr, uid, [data['partner_id']], context=context)
    # #     partner = self.browse(cr, uid, ids, context=context)
    # #     so_obj = self.pool.get('sale.order')
    # #     """
    # #     if 'adjustment' in data['order_ref']:
    # #         sale_order_ids = so_obj.search(cr, uid, [('client_order_ref', '!=', None)], limit=5)
    # #     else:
    # #         sale_order_ids = so_obj.search(cr, uid, [('client_order_ref', '=', data['order_ref'])])
    # #     """
    # #
    # #     # only magento company can be reimburse not magento individual customer
    # #     # if partner.is_company and partner.magento_company_id and partner.magento_company_id != 0 and partner.credit_limit and partner.credit_limit > 0:
    # #     if partner.is_company and partner.magento_company_id and partner.magento_company_id != 0 and partner.credit_limit and partner.credit_limit > 0:
    # #
    # #         """
    # #         # get credit info with magento_company_id
    # #         # get token
    # #         token = None
    # #         url_root = None
    # #
    # #         if sale_order_ids:
    # #             # sale_order_ids = [13669]
    # #             token, url_root = get_magento_token(self, cr, uid, sale_order_ids)
    # #         """
    # #
    # #         if token:
    # #             token = token.replace('"', "")
    # #             headers = {'Authorization': token, 'Content-Type': 'application/json'}
    # #
    # #             get_url = url_root + "/rest/V1/companyCredits/company/" + str(partner.magento_company_id)
    # #             get_resp = requests.get(get_url, headers=headers)
    # #             """
    # #             company_data
    # #             {u'credit_limit': 50000, u'extension_attributes': {u'monthly_limit': False, u'credit_days': 30}, u'company_id': 2104, u'exceed_limit': True, u'available_limit': 16909, u'balance': -33091, u'id': 2101, u'currency_code': u'BDT'}
    # #
    # #             company_data['credit_limit'] == partner.credit_limit
    # #             company_data['balance'] == data['paid_amount']
    # #
    # #             """
    # #
    # #             company_data = json.loads(get_resp.text)
    # #
    # #             credit_id = company_data['id']
    # #
    # #             reimburse_url = url_root
    # #
    # #             mag_balance = -1 * company_data['balance'] if company_data['balance'] < 0 else company_data['balance']
    # #             odoo_balance = -1 * data['paid_amount'] if data['paid_amount'] < 0 else data['paid_amount']
    # #
    # #             reim_data = dict()
    # #             # if paid_amount is positive, call one api
    # #             # http://dev.sindabad.com/rest/V1/companyCredits/{credit_id}/increaseBalance
    # #             if data['paid_amount'] > 0:
    # #
    # #                 # if paid_amount is negative, call other api
    # #                 # http://dev.sindabad.com/rest/V1/companyCredits/{credit_id}/decreaseBalance
    # #                 adj_amount = self.balance_cal(company_data['balance'], data['paid_amount'])
    # #                 reimburse_url = reimburse_url + "/rest/V1/companyCredits/" + str(credit_id) + "/decreaseBalance"
    # #                 # reim_data["value"] = data['paid_amount']
    # #                 reim_data["value"] = adj_amount
    # #
    # #             else:
    # #                 reimburse_url = reimburse_url + "/rest/V1/companyCredits/" + str(credit_id) + "/increaseBalance"
    # #
    # #                 # reim_data["value"] = data['paid_amount'] * -1
    # #                 reim_data["value"] = self.balance_cal(company_data['balance'], data['paid_amount'])
    # #
    # #             reim_data["currency"] = "BDT"
    # #             reim_data["operationType"] = 2
    # #             """
    # #             if 'adjustment' in data['order_ref']:
    # #                 reim_data["operationType"] = 2
    # #             else:
    # #                 reim_data["operationType"] = 4
    # #             """
    # #             # reim_data["comment"] = data['order_ref']
    # #             reim_data["comment"] = "Outstanding Balance Updated From Odoo"
    # #             reim_d = json.dumps(reim_data)
    # #
    # #             # now request with credit_id
    # #             reim_resp = None
    # #
    # #             if mag_balance != odoo_balance:
    # #                 reim_resp = requests.post(reimburse_url, data=reim_d, headers=headers)
    # #
    # #             """
    # #             if data['outstanding'] == 'credit_outstanding':
    # #                 reim_resp = requests.post(reimburse_url, data=reim_d, headers=headers)
    # #             else:
    # #
    # #                 # sync other outstanding
    # #                 # calculate other outstanding
    # #                 # total_balance_due = float(partner.other_outstanding) + float(data['paid_amount'])
    # #                 # total_balance_due = self.get_current_outstanding(cr, uid, [partner.id], partner, context=context)
    # #                 total_balance_due = float(partner.other_outstanding) + float(data['paid_amount'])
    # #
    # #                 # sync with other outstanding
    # #                 other_out_url = url_root + '/rest/V1/companyCredits/' + str(credit_id)
    # #
    # #                 other_out_data = dict()
    # #                 other_out_data['id'] = str(credit_id)
    # #                 other_out_data['company_id'] = int(partner.magento_company_id)
    # #                 other_out_data['currency_code'] = "BDT"
    # #                 other_out_data['extension_attributes'] = {'other_balance': total_balance_due}
    # #                 cl_data = dict()
    # #                 cl_data["creditLimit"] = other_out_data
    # #                 data = json.dumps(cl_data)
    # #
    # #                 resp = requests.put(other_out_url, data=data, headers=headers)
    # #
    # #                 # save total_paid_amount to other_outstanding in res_partner
    # #
    # #                 if resp.status_code == 200:
    # #                     self.pool.get('res.partner').write(cr, uid, [partner.id], {'other_outstanding': total_balance_due}, context=context)
    # #             """
    # #
    # #     return True


"""
class Account_voucher(osv.osv):
    _inherit = "account.voucher"

    def button_proforma_voucher(self, cr, uid, ids, context=None):

        if str(context['default_reference']).replace("-", "").isdigit():
            av = self.browse(cr, uid, ids, context=context)

            so_ref = context['default_reference']

            paid_amount = av.amount

            amount_data = dict()
            amount_data['order_ref'] = so_ref
            amount_data['paid_amount'] = paid_amount
            amount_data['partner_id'] = context['default_partner_id']

            so_obj = self.pool.get('sale.order')
            so_list = so_obj.search(cr, uid, [('client_order_ref', '=', str(so_ref)), ('state', '!=', 'cancel')],
                                    context=context)
            so = so_obj.browse(cr, uid, so_list, context=context)

            sale_order_ids = so_obj.search(cr, uid, [('client_order_ref', '!=', None)], limit=5)
            token, url_root = get_magento_token(self, cr, uid, sale_order_ids)

            
            # other_outstanding = rest payment type
            # credit_outstanding = Payment on Account
            
            if "Payment on Account" in str(so.note):
                amount_data['outstanding'] = 'credit_outstanding'
            else:
                amount_data['outstanding'] = 'other_outstanding'

            self.pool.get('res.partner').reimburse_sync(cr, uid, ids, amount_data, token, url_root, context=context)

        return super(Account_voucher, self).button_proforma_voucher(cr, uid, ids, context=context)
"""
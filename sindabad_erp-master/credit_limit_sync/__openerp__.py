{
    'name': 'Credit Limit Sync',
    'version': '1.0',
    'category': 'Credit Limit Sync',
    'author': 'Shahjalal',
    'summary': 'Credit Limit Sync with Magento',
    'description': 'Credit Limit Sync with Magento',
    'depends': ['base', 'odoo_magento_connect', 'sale', 'stock', 'odoo_to_magento_api_connect', 'order_approval_process', 'company_code_generation', 'customer_statement', 'order_status_sync', 'company_sync'],
    'data': [
        'outstanding_sync_from_so.xml',
        'outstanding_sync_scheduler.xml',
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}

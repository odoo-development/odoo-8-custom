from datetime import datetime, timedelta
import time, datetime, json
from openerp import SUPERUSER_ID
from openerp.osv import fields, osv
from openerp.tools.translate import _
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT
import openerp.addons.decimal_precision as dp
from openerp import workflow
import logging
import requests
from ..odoo_to_magento_api_connect.api_connect import get_magento_token, submit_request

XMLRPC_API = '/index.php/api/xmlrpc'

_logger = logging.getLogger(__name__)

"""
class company_outstanding_history(osv.osv):

    _name = "company.outstanding.history"

    _columns = {
        'parent_id': fields.many2one('res.partner', 'Company', select=True),
        'magento_company_id': fields.integer(string="Magento Company ID"),
        'magento_outstanding': fields.float('Magento Outstanding'),
        'odoo_outstanding': fields.float('Odoo Outstanding'),
        'adjustment_amount': fields.float('Adjustment amount'),
        'magento_credit_id': fields.integer(string="Magento Credit ID"),
        'comment': fields.text('Comment'),
    }
"""


class sale_order(osv.osv):
    _inherit = "sale.order"

    """
    def action_sync_outstanding(self, cr, uid, ids, context=None):

        # self.pool.get('res.partner').outstanding_sync_scheduler(cr, uid, active=False, ids=ids, cron_mode=True, context=context)

        return True
    """

    def action_sync_credit_and_outstanding(self, cr, uid, ids, context=None):

        so_obj = self.pool.get('sale.order')
        sale_order_ids = so_obj.search(cr, uid, [('client_order_ref', '!=', None)], limit=5)
        token, url_root = get_magento_token(self, cr, uid, sale_order_ids)

        # sync credit limit
        rs_p_obj = self.pool.get('res.partner')
        companies_list = rs_p_obj.search(cr, uid, ['&', ('is_company', '=', True), ('parent_id', '=', None), ('credit_limit', '>', 0), ('magento_company_id', '!=', 0)])

        for partner_id in companies_list:

            # rs_p_obj.credit_limit_sync(cr, uid, [partner_id], token, url_root, vals=None, context=context)
            rs_p_obj.outstanding_sync(cr, uid, [partner_id], token, url_root, context=context)

        return True


class res_partner(osv.osv):
    _inherit = "res.partner"

    def action_partner_sync_credit_and_outstanding(self, cr, uid, ids, context=None):

        try:
            with open('/var/log/odoo/cron-run-time.log', 'a+') as f:
                right_now = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
                f.write("Outstanding Sync Scheduler: Start time: " + right_now + "\n")
        except:
            pass

        so_obj = self.pool.get('sale.order')
        sale_order_ids = so_obj.search(cr, uid, [('client_order_ref', '!=', None)], limit=3)
        token, url_root = get_magento_token(self, cr, uid, sale_order_ids)

        rs_p_obj = self.pool.get('res.partner')

        companies_list = list()
        if ids is None:

            companies_list = rs_p_obj.search(cr, uid, ['&', ('is_company', '=', True), ('parent_id', '=', None), ('credit_limit', '>', 0), ('magento_company_id', '!=', 0)])
        elif ids is not None and len(ids) == 1:
            companies_list = ids
        
        # for partner_id in companies_list:
        #
        #     # rs_p_obj.credit_limit_sync(cr, uid, [partner_id], token, url_root, vals=None, context=context)
        rs_p_obj.outstanding_sync(cr, uid, companies_list, token, url_root, context=context)

        try:
            with open('/var/log/odoo/cron-run-time.log', 'a+') as f:
                right_now = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
                f.write("Outstanding Sync Scheduler: End time: " + right_now + "\n")
        except:
            pass

        return True

# Author S. M. Sazedul Haque 2018/11/06

import xlwt
from datetime import datetime, timedelta
import time
from openerp.osv import orm
from openerp.report import report_sxw
from openerp.addons.report_xls.report_xls import report_xls
from openerp.addons.report_xls.utils import rowcol_to_cell, _render
from openerp.tools.translate import translate, _
import logging

from ast import literal_eval as make_tuple
_logger = logging.getLogger(__name__)


_ir_translation_name = 'customer.res.partner.xls'


class customer_xls_parser(report_sxw.rml_parse):

    def __init__(self, cr, uid, name, context):
        super(customer_xls_parser, self).__init__(
            cr, uid, name, context=context)
        move_obj = self.pool.get('res.partner')
        self.context = context
        wanted_list = move_obj._report_xls_fields(cr, uid, context)
        template_changes = move_obj._report_xls_template(cr, uid, context)
        self.localcontext.update({
            'datetime': datetime,
            'wanted_list': wanted_list,
            'template_changes': template_changes,
            '_': self._,
        })

    def _(self, src):
        lang = self.context.get('lang', 'en_US')
        return translate(self.cr, _ir_translation_name, 'report', lang, src) \
            or src

    def set_context(self, objects, data, ids, report_type=None):
        ids = self.context.get('active_ids')
        partner_obj = self.pool['res.partner']
        context = self.context


        self.localcontext.update({
            # 'docs': docs,
            # 'time': time,
            # 'getLines': self._lines_get,
            # 'due': due,
            # 'paid': paid,
            # 'prev_balance': init_balance,
            # 'date_range': str(st_date) + ' to ' + str(end_date),
            # 'mat': mat,
            # 'text':text_output
        })
        self.context = context

        return super(customer_xls_parser, self).set_context(objects, data, ids, report_type)


class res_partner_xls(report_xls):

    def __init__(self, name, table, rml=False, parser=False, header=True, store=False, context=None):
        super(res_partner_xls, self).__init__(
            name, table, rml, parser, header, store)
        # Cell Styles
        _xs = self.xls_styles
        # header
        rh_cell_format = _xs['bold'] + _xs['fill'] + _xs['borders_all']
        self.rh_cell_style = xlwt.easyxf(rh_cell_format)
        self.rh_cell_style_center = xlwt.easyxf(rh_cell_format + _xs['center'])
        self.rh_cell_style_right = xlwt.easyxf(rh_cell_format + _xs['right'])
        # lines
        aml_cell_format = _xs['borders_all']
        self.aml_cell_style = xlwt.easyxf(aml_cell_format)
        self.aml_cell_style_center = xlwt.easyxf(
            aml_cell_format + _xs['center'])
        self.aml_cell_style_date = xlwt.easyxf(
            aml_cell_format + _xs['left'],
            num_format_str=report_xls.date_format)
        self.aml_cell_style_decimal = xlwt.easyxf(
            aml_cell_format + _xs['right'],
            num_format_str=report_xls.decimal_format)
        # totals
        rt_cell_format = _xs['bold'] + _xs['fill'] + _xs['borders_all']
        self.rt_cell_style = xlwt.easyxf(rt_cell_format)
        self.rt_cell_style_right = xlwt.easyxf(rt_cell_format + _xs['right'])
        self.rt_cell_style_decimal = xlwt.easyxf(
            rt_cell_format + _xs['right'],
            num_format_str=report_xls.decimal_format)

        # XLS Template
        self.col_specs_template = {
            'name': {
                'header': [1, 20, 'text', _render("_('Customer Name')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'advance': {
                'header': [1, 20, 'text', _render("_('Advance')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'days_0_15': {
                'header': [1, 20, 'text', _render("_('0-15')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'days_16_30': {
                'header': [1, 20, 'text', _render("_('16-30')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'days_31_45': {
                'header': [1, 20, 'text', _render("_('31-45')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'days_46_60': {
                'header': [1, 20, 'text', _render("_('46-60')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'days_61_90': {
                'header': [1, 20, 'text', _render("_('61-90')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'days_91': {
                'header': [1, 20, 'text', _render("_('90+')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'total_outstanding': {
                'header': [1, 20, 'text', _render("_('Total Outstanding')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'float', None]},

        }

    def generate_xls_report(self, _p, _xs, data, objects, wb):

        wanted_list = _p.wanted_list
        self.col_specs_template.update(_p.template_changes)
        _ = _p._

        context = self.context
        st_date = data.get('form').get('date_from')
        end_date = data.get('form').get('date_to')
        context['start_date'] = st_date
        context['end_date'] = end_date
        self.context = context
        date_range = _("Date: %s to %s" % (st_date, end_date))

        # report_name = objects[0]._description or objects[0]._name
        report_name = _("Customer Aging Report")
        ws = wb.add_sheet(report_name[:31])
        ws.panes_frozen = True
        ws.remove_splits = True
        ws.portrait = 0  # Landscape
        ws.fit_width_to_pages = 1
        row_pos = 0

        # set print header/footer
        ws.header_str = self.xls_headers['standard']
        ws.footer_str = self.xls_footers['standard']

        # Title
        cell_style = xlwt.easyxf(_xs['xls_title'])
        c_specs = [
            ('report_name', 4, 3, 'text', report_name),
        ]
        row_data = self.xls_row_template(c_specs, ['report_name'])
        row_pos = self.xls_write_row(
            ws, row_pos, row_data, row_style=cell_style)
        row_pos += 1

        cell_style = xlwt.easyxf(_xs['xls_title'])
        c_specs = [
            ('date_range', 4, 3, 'text', date_range),
        ]
        row_data = self.xls_row_template(c_specs, ['date_range'])
        row_pos = self.xls_write_row(
            ws, row_pos, row_data, row_style=cell_style)
        row_pos += 1
        # Column headers
        c_specs = map(lambda x: self.render(
            x, self.col_specs_template, 'header', render_space={'_': _p._}),
            wanted_list)
        row_data = self.xls_row_template(c_specs, [x[0] for x in c_specs])
        row_pos = self.xls_write_row(
            ws, row_pos, row_data, row_style=self.rh_cell_style,
            set_column_size=True)
        ws.set_horz_split_pos(row_pos)

        #########################################
        partner_id_list = self.pool.get('res.partner').search(self.cr, self.uid, [('is_company', '=', True), ('parent_id', '=', None)], context=context)
        #########################################

        partner_obj_lists = self.pool.get('res.partner').browse(self.cr, self.uid, partner_id_list)

        for line in partner_obj_lists:
            bv = make_tuple(str(line.v_0_15))

            total_amount = float(bv[0]) + float(bv[1]) + float(bv[2]) + float(bv[3]) + float(bv[4]) + float(bv[5])

            c_specs = map(
                lambda x: self.render(x, self.col_specs_template, 'lines'),
                wanted_list)

            for list_data in c_specs:

                if str(list_data[0]) == str('name'):
                    list_data[4] = str(line.display_name)

                if str(list_data[0]) == str('days_0_15'):
                    list_data[4] = str(bv[0])

                if str(list_data[0]) == str('days_16_30'):
                    list_data[4] = str(bv[1])

                if str(list_data[0]) == str('days_31_45'):
                    list_data[4] = str(bv[2])

                if str(list_data[0]) == str('days_46_60'):
                    list_data[4] = str(bv[3])

                if str(list_data[0]) == str('days_61_90'):
                    list_data[4] = str(bv[4])

                if str(list_data[0]) == str('days_91'):
                    list_data[4] = str(bv[5])

                if str(list_data[0]) == str('total_outstanding'):
                    list_data[4] = str(total_amount)

            # i do not know what should i need to write (Written by Junayed Vai)

            row_data = self.xls_row_template(c_specs, [x[0] for x in c_specs])
            row_pos = self.xls_write_row(
                ws, row_pos, row_data, row_style=self.aml_cell_style)


res_partner_xls('report.customer.res.partner.xls',
                      'res.partner',
                      parser=customer_xls_parser)

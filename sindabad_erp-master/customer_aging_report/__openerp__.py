{
    'name': 'Customer Aging Report',
    'version': '1.0',
    'category': 'Customer Aging Report',
    'author': 'Mufti, Rocky, Shuvarthi, Apu, Shahjalal',
    'summary': 'Customer Aging Report',
    'description': 'Customer Aging Report',
    "website": "Sindabad.com",
    'depends': ['base', 'odoo_magento_connect', 'account', 'sale', 'purchase', 'report_xls'],
    'data': [

        'wizard/customer_aging_report_filter.xml',
        'report/customer_aging_report_xls.xml',

    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}

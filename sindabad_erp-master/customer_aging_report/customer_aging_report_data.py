from openerp import models, api
from openerp.osv import osv, fields
from openerp import api, fields, models


class CustomerAgingReport(models.Model):
    _inherit = 'res.partner'

    @api.model
    def _report_xls_fields(self):

        return [
             'name', 'advance', 'days_0_15', 'days_16_30', 'days_31_45', 'days_46_60', 'days_61_90', 'days_91', 'total_outstanding',

        ]

    # Change/Add Template entries
    @api.model
    def _report_xls_template(self):

        return {}

   
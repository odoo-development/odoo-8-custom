from openerp.osv import fields, osv
from openerp.tools.translate import _
from openerp import api
from time import gmtime, strftime
import datetime


class management_dashboard(osv.osv):
    _name = "management.dashboard"
    _description = "Management Dashboard"

    _columns = {
        'name': fields.char('Magento Order'),
        'order_no': fields.char('Odoo'),
        'partner_name': fields.char('Partner Name'),
        'partner_classification': fields.char('Classification'),
        'is_company': fields.boolean('Is Company'),
        'date_order': fields.datetime('Order Date'),
        'date_confirm': fields.date('Confirm Date'),
        'state': fields.char('Status'),
        'amount_total': fields.float('Amount'),
        'inv_ids': fields.one2many('management.dashboard.inv', 'inv_id'),
    }

    def management_dashboard_daily_data_process(self, cr, uid, ids=None, context=None):

        sale_obj = self.pool.get('sale.order')
        mgt_db_obj = self.pool.get('management.dashboard')

        date_last_days = datetime.datetime.now() - datetime.timedelta(days=30)

        compare_date = date_last_days.strftime('%Y-%m-%d')

        # delete last days sale_list
        mgt_db_sale_list = mgt_db_obj.search(cr, uid, [('date_confirm', '>', compare_date), ('state', '!=', 'cancel')])

        mgt_db_obj.unlink(cr, 1, mgt_db_sale_list)

        # insert last days sale_list
        sale_list = sale_obj.search(cr, uid, [('date_confirm', '>', compare_date), ('state', '!=', 'cancel')])

        sos = sale_obj.browse(cr, uid, sale_list)

        for so in sos:

            inv_ids_list = list()

            for inv in so.invoice_ids:
                inv_ids_list.append([0, False, {
                    'type': str(inv.type),
                    'state': str(inv.state),
                    'invoice_date': inv.create_date,
                    'amount_total': inv.amount_total
                }])

            # inv_ids = inv_ids_list

            if inv_ids_list:
                self.create(cr, uid, {
                    'name': str(so.client_order_ref),
                    'order_no': str(so.name),
                    'partner_name': str(so.partner_id.name),
                    'partner_classification': str(so.partner_id.x_classification),
                    # is_company,
                    'date_order': so.date_order,
                    'date_confirm': so.date_confirm,
                    'state': str(so.state),
                    'amount_total': so.amount_total,
                    'inv_ids': inv_ids_list
                })
            else:
                self.create(cr, uid, {
                    'name': str(so.client_order_ref),
                    'order_no': str(so.name),
                    'partner_name': str(so.partner_id.name),
                    'partner_classification': str(so.partner_id.x_classification),
                    # is_company,
                    'date_order': so.date_order,
                    'date_confirm': so.date_confirm,
                    'state': str(so.state),
                    'amount_total': so.amount_total
                })

        return True

    def chunks(self, l, n):
        """Yield successive n-sized chunks from l."""
        for i in xrange(0, len(l), n):
            yield l[i:i + n]

    def management_dashboard_data_process(self, cr, uid, ids=None, context=None):

        sale_obj = self.pool.get('sale.order')

        # ------------- temporary block -------------
        date_obj = datetime.datetime.now() - datetime.timedelta(days=200)

        compare_date = date_obj.strftime('%Y-%m-%d')

        sale_list = sale_obj.search(cr, uid, [('date_confirm', '>', compare_date), ('state', '!=', 'cancel')])

        # ------------- temporary block -------------

        # all sales orders ids
        # sale_list = sale_obj.search(cr, uid, [('date_confirm', '!=', None), ('state', '!=', 'cancel')], context=context)
        # sale_id_lists_chunks = [x for x in self.chunks(sale_list, 1000)]

        chunk = 1
        chunk = chunk

        sos = sale_obj.browse(cr, uid, sale_list)

        for so in sos:

            inv_ids_list = list()

            for inv in so.invoice_ids:
                inv_ids_list.append([0, False, {
                    'type': str(inv.type),
                    'state': str(inv.state),
                    'invoice_date': inv.create_date,
                    'amount_total': inv.amount_total
                }])

            # inv_ids = inv_ids_list

            if inv_ids_list:
                self.create(cr, uid, {
                    'name': str(so.client_order_ref),
                    'order_no': str(so.name),
                    'partner_name': str(so.partner_id.name),
                    'partner_classification': str(so.partner_id.x_classification),
                    # is_company,
                    'date_order': so.date_order,
                    'date_confirm': so.date_confirm,
                    'state': str(so.state),
                    'amount_total': so.amount_total,
                    'inv_ids': inv_ids_list
                })
            else:
                self.create(cr, uid, {
                    'name': str(so.client_order_ref),
                    'order_no': str(so.name),
                    'partner_name': str(so.partner_id.name),
                    'partner_classification': str(so.partner_id.x_classification),
                    # is_company,
                    'date_order': so.date_order,
                    'date_confirm': so.date_confirm,
                    'state': str(so.state),
                    'amount_total': so.amount_total
                })

        # import pdb;pdb.set_trace()

        return True


class management_dashboard_inv(osv.osv):
    _name = "management.dashboard.inv"
    _description = "Management Dashboard Invoices"

    _columns = {
        'inv_id': fields.many2one('management.dashboard', 'Invoice', ondelete='cascade', readonly=True),
        'type': fields.char('Invoice Type'),
        'state': fields.char('Invoice State'),
        'invoice_date': fields.datetime('Invoice Date'),
        'amount_total': fields.float('Amount'),
    }

from openerp.osv import fields,osv
from openerp.tools.translate import _
import datetime
import psycopg2


class sale_order(osv.osv):
    _inherit = 'sale.order'

    def dashboard_data_process(self, cr, uid, active=False, ids=None, cron_mode=True, context=None):

        sale_obj = self.pool.get('sale.order')
        md_obj = self.pool.get('management.dashboard')

        md_list = md_obj.search(cr, uid, [])

        md_obj.browse(cr, uid, md_list).unlink()

        # ------------- temporary block -------------
        date_obj = datetime.datetime.now() - datetime.timedelta(days=200)

        compare_date = date_obj.strftime('%Y-%m-%d')

        sale_list = sale_obj.search(cr, uid, [('date_confirm', '>', compare_date), ('state', '!=', 'cancel')])

        # ------------- temporary block -------------

        # all sales orders ids
        # sale_list = sale_obj.search(cr, uid, [('date_confirm', '!=', None), ('state', '!=', 'cancel')], context=context)
        # sale_id_lists_chunks = [x for x in self.chunks(sale_list, 1000)]

        chunk = 1
        chunk = chunk

        sos = sale_obj.browse(cr, uid, sale_list)

        for so in sos:

            inv_ids_list = list()

            for inv in so.invoice_ids:
                inv_ids_list.append([0, False, {
                    'type': str(inv.type),
                    'state': str(inv.state),
                    'invoice_date': inv.create_date,
                    'amount_total': inv.amount_total
                }])

            if inv_ids_list:
                md_obj.create(cr, uid, {
                    'name': str(so.client_order_ref),
                    'order_no': str(so.name),
                    'partner_name': str(so.partner_id.name),
                    'partner_classification': str(so.partner_id.x_classification),
                    # is_company,
                    'date_order': so.date_order,
                    'date_confirm': so.date_confirm,
                    'state': str(so.state),
                    'amount_total': so.amount_total,
                    'inv_ids': inv_ids_list
                })
            else:
                md_obj.create(cr, uid, {
                    'name': str(so.client_order_ref),
                    'order_no': str(so.name),
                    'partner_name': str(so.partner_id.name),
                    'partner_classification': str(so.partner_id.x_classification),
                    # is_company,
                    'date_order': so.date_order,
                    'date_confirm': so.date_confirm,
                    'state': str(so.state),
                    'amount_total': so.amount_total
                })

        return True

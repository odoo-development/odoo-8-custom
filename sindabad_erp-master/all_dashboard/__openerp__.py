{
    'name': 'All Dashboard',
    'version': '8.0.0.6.0',
    'author': "Shahjalal Hossain",
    'category': 'Dashboard',
    'summary': 'All Dashboard',
    'depends': ['base', 'odoo_magento_connect', 'account', 'sale', 'purchase', 'wms_manifest', 'order_cancel_reasons', 'retail_dashboard', 'dashboard_sindabad', 'order_approval_process', 'report_xls','dashboard_sindabad'],

    'data': [
        'security/base_security.xml',
        'security/ir.model.access.csv',
        'views/dashboard_data_process.xml',
        'views/dashboard_confirm_order_view.xml',
        'views/management_dashboard.xml',
        'views/retail_dashboard.xml',
        'views/corporate_dashboard.xml',
        'views/warehouse_dashboard.xml',
        'views/dashboard_data_population.xml',
        'all_dashboard_menu.xml',
    ],

    'installable': True,
    'application': True,
    'auto_install': False,
}

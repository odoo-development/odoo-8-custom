import operator
from openerp.osv import fields,osv
from openerp.tools.translate import _
import datetime
import psycopg2


class corporate_dashboard(osv.osv):
    _name = 'corporate.dashboard'

    def date_plus_one(self, date_str):
        date_obj = datetime.datetime.strptime(date_str, '%Y-%m-%d') + datetime.timedelta(days=1)

        increased_date = date_obj.strftime('%Y-%m-%d')

        return increased_date

    def last_day_this_month(self):

        today = datetime.datetime.today()

        if today.month == 12:
            last_day_obj=today.replace(day=31, month=today.month)
        else:

            last_day_obj = today.replace(day=1, month=today.month + 1) - datetime.timedelta(days=1)
        last_day = last_day_obj.strftime('%Y-%m-%d')


        return last_day

    def last_day_previous_month(self,date):

        today = datetime.datetime.strptime(date, '%Y-%m-%d')

        if today.month == 12:
            last_day_obj=today.replace(day=31, month=today.month)
        else:

            last_day_obj = today.replace(day=1, month=today.month + 1) - datetime.timedelta(days=1)
        last_day = last_day_obj.strftime('%Y-%m-%d')


        return last_day

    def six_month_ago_first_date(self):

        today = datetime.datetime.today()
        (day, month, year) = (today.day, (today.month - 6) % 12 + 1, today.year + (today.month - 6) / 12)

        first_date = str(year) + "-" + str(month) + "-1"

        return first_date

    def present_month_first_date(self):

        today = datetime.datetime.today()
        # (day, month, year) = (today.day, (today.month - 1) % 12 + 1, today.year + (today.month - 6) / 12)
        first_date = str(today.year) + "-" + str(today.month) + "-1"

        return first_date

    def toDayWeekMonthSalesInvoiceRefundNetInvoice(self, cr, uid, start_date=None, end_date=None, sale_type=None, context=None):

        tmp = start_date
        start_date = end_date
        end_date = tmp

        if sale_type == 'monthly':
            start_date = self.present_month_first_date()

        #### Sales Data calculation
        if sale_type and (sale_type =='Ananta' or sale_type =='Corporate'):
            if sale_type == 'Ananta':
                sales_query = "select customer_classification,sum(amount_total) as total from sale_order where state in ('done','progress','manual','shipping_except','invoice_except' ) and (customer_classification LIKE 'Ananta%') and date_confirm>='{0}' and date_confirm<='{1}' group by customer_classification".format(
                start_date, end_date)
            else:
                sales_query = "select customer_classification,sum(amount_total) as total from sale_order where state in ('done','progress','manual','shipping_except','invoice_except' ) and (customer_classification LIKE 'Corporate%' or customer_classification LIKE 'SOHO%' or customer_classification LIKE 'SME%' or customer_classification LIKE 'HORECA%') and date_confirm>='{0}' and date_confirm<='{1}' group by customer_classification".format(
                    start_date, end_date)
        else:
            sales_query = "select customer_classification,sum(amount_total) as total from sale_order where state in ('done','progress','manual','shipping_except','invoice_except') and (customer_classification LIKE 'Ananta%' or customer_classification LIKE 'Corporate%' or customer_classification LIKE 'SOHO%' or customer_classification LIKE 'SME%' or customer_classification LIKE 'HORECA%') and date_confirm>='{0}' and date_confirm<='{1}' group by customer_classification".format(start_date, end_date)

        cr.execute(sales_query)

        sales_data = cr.dictfetchall()

        sale_data_dict = {}
        sale_data_dict['Corporate'] = 0
        sale_data_dict['Ananta'] = 0
        sale_data_dict['SOHO'] = 0
        # sale_data_dict['HORECA'] = 0
        total_sale = 0
        total_count = 0
        total_today_label = []
        total_today_value = []

        for items in sales_data:

            try:
                total_sale += items.get('total')
            except:
                pass

            if 'Corporate' in items.get('customer_classification'):
                sale_data_dict['Corporate'] = items.get('total')
            elif 'SOHO' in items.get('customer_classification'):
                sale_data_dict['SOHO'] += items.get('total')
            elif 'SME' in items.get('customer_classification'):
                sale_data_dict['SOHO'] += items.get('total')
            elif 'HORECA' in items.get('customer_classification'):
                sale_data_dict['SOHO'] += items.get('total')
            elif 'Ananta' in items.get('customer_classification'):
                sale_data_dict['Ananta'] = items.get('total')


        sales_count_query = "select count(id) as total from sale_order where state in ('done','progress','manual','shipping_except','invoice_except' ) and (customer_classification LIKE 'Corporate%' or customer_classification LIKE 'Ananta%' or customer_classification LIKE 'SOHO%' or customer_classification LIKE 'SME%' or customer_classification LIKE 'HORECA%') and date_confirm>='{0}' and date_confirm<='{1}'".format(start_date, end_date)

        # cr.execute(sales_count_query, (start_date, end_date))
        cr.execute(sales_count_query)

        sales_count = cr.dictfetchall()

        if len(sales_count) > 0:
            total_count = sales_count[0].get('total')

        for k, v in sale_data_dict.iteritems():
            total_today_label.append(k)
            total_today_value.append(v)
        #### Sales Data calculation Ends Here

        ### Invoice Calcualtion Starts Here
        end_date = self.date_plus_one(end_date)
        if sale_type and (sale_type =='Ananta' or sale_type =='Corporate'):
            # classifiation_type = '%' + str(sale_type) + '%'
            if sale_type == 'Ananta':
                total_invoice_query = "select sum(account_invoice.amount_total) as total,sale_order.customer_classification as customer_classification,account_invoice.type from account_invoice,sale_order_invoice_rel,sale_order where account_invoice.id=sale_order_invoice_rel.invoice_id and sale_order_invoice_rel.order_id=sale_order.id and account_invoice.type in ('out_invoice','out_refund') and account_invoice.create_date>='{0}' and account_invoice.state  in ('open','paid') and account_invoice.create_date <='{1}' and (sale_order.customer_classification LIKE 'Ananta%') group by account_invoice.type,sale_order.customer_classification".format(start_date, end_date)
            else:
                total_invoice_query = "select sum(account_invoice.amount_total) as total,sale_order.customer_classification as customer_classification,account_invoice.type from account_invoice,sale_order_invoice_rel,sale_order where account_invoice.id=sale_order_invoice_rel.invoice_id and sale_order_invoice_rel.order_id=sale_order.id and account_invoice.type in ('out_invoice','out_refund') and account_invoice.create_date>='{0}' and account_invoice.state  in ('open','paid') and account_invoice.create_date <='{1}' and (sale_order.customer_classification LIKE 'Corporate%' or sale_order.customer_classification LIKE 'SOHO%' or sale_order.customer_classification LIKE 'SME%' or sale_order.customer_classification LIKE 'HORECA%') group by account_invoice.type,sale_order.customer_classification".format(start_date, end_date)

        else:
            total_invoice_query = "select sum(account_invoice.amount_total) as total,sale_order.customer_classification as customer_classification,account_invoice.type from account_invoice,sale_order_invoice_rel,sale_order where account_invoice.id=sale_order_invoice_rel.invoice_id and sale_order_invoice_rel.order_id=sale_order.id and account_invoice.type in ('out_invoice','out_refund') and account_invoice.create_date>='{0}' and account_invoice.state  in ('open','paid') and account_invoice.create_date <='{1}' and (sale_order.customer_classification LIKE 'Ananta%' or sale_order.customer_classification LIKE 'Corporate%' or sale_order.customer_classification LIKE 'SOHO%' or sale_order.customer_classification LIKE 'SME%' or sale_order.customer_classification LIKE 'HORECA%') group by account_invoice.type,sale_order.customer_classification".format(start_date, end_date)


        # cr.execute(total_invoice_query, (start_date, end_date))
        cr.execute(total_invoice_query)

        total_invoice_data = cr.dictfetchall()

        invoice_data_dict = {}
        invoice_data_dict['Corporate'] = 0
        invoice_data_dict['Ananta'] = 0
        invoice_data_dict['SOHO'] = 0
        # invoice_data_dict['HORECA'] = 0
        total_invoice_amount = 0
        total_invoice_count = 0
        total_invoice_label = []
        total_invoice_value = []

        refund_invoice_dict = {}
        refund_invoice_dict['Corporate'] = 0
        refund_invoice_dict['Ananta'] = 0
        refund_invoice_dict['SOHO'] = 0
        # refund_invoice_dict['HORECA'] = 0
        refund_invoice_amount = 0
        refund_invoice_count = 0
        refund_invoice_level = []
        refund_invoice_value = []

        net_corporate = 0
        net_soho = 0
        # net_horeca = 0
        net_ananta = 0

        for items in total_invoice_data:

            if items.get('type') == 'out_invoice':
                try:
                    total_invoice_amount += items.get('total')
                except:
                    pass
                if 'Corporate' in items.get('customer_classification'):
                    invoice_data_dict['Corporate'] = items.get('total')
                    net_corporate += items.get('total')
                elif 'SOHO' in items.get('customer_classification'):
                    invoice_data_dict['SOHO'] += items.get('total')
                    net_soho += items.get('total')
                elif 'SME' in items.get('customer_classification'):
                    invoice_data_dict['SOHO'] += items.get('total')
                    net_soho += items.get('total')
                elif 'HORECA' in items.get('customer_classification'):
                    invoice_data_dict['SOHO'] += items.get('total')
                    net_soho += items.get('total')
                elif 'Ananta' in items.get('customer_classification'):
                    invoice_data_dict['Ananta'] += items.get('total')
                    net_ananta += items.get('total')

            else:
                try:
                    refund_invoice_amount += items.get('total')
                except:
                    pass
                if 'Corporate' in items.get('customer_classification'):
                    refund_invoice_dict['Corporate'] = items.get('total')
                    net_corporate -= items.get('total')
                elif 'SOHO' in items.get('customer_classification'):
                    refund_invoice_dict['SOHO'] += items.get('total')
                    net_soho -= items.get('total')
                elif 'SME' in items.get('customer_classification'):
                    refund_invoice_dict['SOHO'] += items.get('total')
                    net_soho -= items.get('total')
                elif 'HORECA' in items.get('customer_classification'):
                    refund_invoice_dict['SOHO'] += items.get('total')
                    net_soho -= items.get('total')
                elif 'Ananta' in items.get('customer_classification'):
                    refund_invoice_dict['Ananta'] = items.get('total')
                    net_ananta -= items.get('total')

        for k, v in invoice_data_dict.iteritems():
            total_invoice_label.append(k)
            total_invoice_value.append(v)

        for k, v in refund_invoice_dict.iteritems():
            refund_invoice_level.append(k)
            refund_invoice_value.append(v)

        inv_count_query = "SELECT count(account_invoice.id) total FROM account_invoice, sale_order where account_invoice.name = sale_order.client_order_ref and (sale_order.customer_classification LIKE 'Ananta%' or sale_order.customer_classification LIKE 'Corporate%' or sale_order.customer_classification LIKE 'SOHO%' or sale_order.customer_classification LIKE 'SME%' or sale_order.customer_classification LIKE 'HORECA%') and account_invoice.state  in ('open','paid') and type = 'out_invoice' and account_invoice.create_date>='{0}' and account_invoice.create_date <='{1}'".format(start_date, end_date)

        # cr.execute(inv_count_query, (start_date, end_date))
        cr.execute(inv_count_query)

        inv_count_data = cr.dictfetchall()

        if len(inv_count_data) > 0:
            total_invoice_count = inv_count_data[0].get('total')

        ref_inv_count_query = "SELECT count(account_invoice.id) total FROM account_invoice, sale_order where account_invoice.name = sale_order.client_order_ref and (sale_order.customer_classification LIKE 'Ananta%' or sale_order.customer_classification LIKE 'Corporate%' or sale_order.customer_classification LIKE 'SOHO%' or sale_order.customer_classification LIKE 'SME%' or sale_order.customer_classification LIKE 'HORECA%') and account_invoice.state  in ('open','paid') and type='out_refund' and account_invoice.create_date>='{0}' and account_invoice.create_date <='{1}'".format(start_date, end_date)
        # cr.execute(ref_inv_count_query, (start_date, end_date))
        cr.execute(ref_inv_count_query)

        ref_inv_count_data = cr.dictfetchall()

        net_invoice_level = []
        net_invoice_value = []

        if len(ref_inv_count_query) > 0:
            refund_invoice_count = ref_inv_count_data[0].get('total')
        net_invoice_amount = net_ananta + net_corporate + net_soho
        margin = 0

        if net_corporate != 0:
            net_invoice_level.append('Corporate')
            net_invoice_value.append(net_corporate)
        else:
            net_invoice_level.append('Corporate')
            net_invoice_value.append(0)
        if net_soho != 0:
            net_invoice_level.append('SOHO')
            net_invoice_value.append(net_soho)
        else:
            net_invoice_level.append('SOHO')
            net_invoice_value.append(0)
        # if net_horeca != 0:
        #     net_invoice_level.append('HORECA')
        #     net_invoice_value.append(net_horeca)
        # else:
        #     net_invoice_level.append('HORECA')
        #     net_invoice_value.append(0)
        if net_ananta != 0:
            net_invoice_level.append('Ananta')
            net_invoice_value.append(net_ananta)
        else:
            net_invoice_level.append('Ananta')
            net_invoice_value.append(0)

        return [float(str(total_sale)), total_count, total_today_label, total_today_value,
                float(str(total_invoice_amount)), total_invoice_count, total_invoice_label, total_invoice_value,
                float(str(refund_invoice_amount)),
                refund_invoice_count,
                refund_invoice_level,
                refund_invoice_value,
                float(str(net_invoice_amount)),
                margin,
                net_invoice_level,
                net_invoice_value,
                ]

    def topTenCustomerdelivery(self, cr, uid, start_date=None, end_date=None, sale_type=None, context=None):

        tmp = start_date
        start_date = end_date
        end_date = tmp

        if sale_type == 'monthly':
            start_date = self.present_month_first_date()
            last_date_of_this_month = self.last_day_this_month()
            end_date = last_date_of_this_month

        st_date = start_date
        end_date = self.date_plus_one(end_date)

        if sale_type and (sale_type =='Ananta' or sale_type =='Corporate'):
            if sale_type =='Ananta':
                company_wise_inv_query = "select sum(account_invoice.amount_total) as total,(SELECT cus.name FROM res_partner AS cus WHERE cus.id = res_partner.parent_id) as company_name,sale_order.customer_classification as customer_classification,account_invoice.type from account_invoice,sale_order_invoice_rel,sale_order,res_partner where  (sale_order.customer_classification LIKE 'Ananta%') and account_invoice.id=sale_order_invoice_rel.invoice_id and sale_order_invoice_rel.order_id=sale_order.id and account_invoice.type in ('out_invoice','out_refund') and account_invoice.state  in ('open','paid') and res_partner.parent_id is not NULL and sale_order.partner_id = res_partner.id and account_invoice.create_date>='{0}' and account_invoice.create_date <='{1}' group by account_invoice.type,sale_order.customer_classification,res_partner.parent_id  order by account_invoice.type,total".format(st_date, end_date)
            else:
                company_wise_inv_query = "select sum(account_invoice.amount_total) as total,(SELECT cus.name FROM res_partner AS cus WHERE cus.id = res_partner.parent_id) as company_name,sale_order.customer_classification as customer_classification,account_invoice.type from account_invoice,sale_order_invoice_rel,sale_order,res_partner where  (sale_order.customer_classification LIKE 'Corporate%'or sale_order.customer_classification LIKE 'SOHO%' or sale_order.customer_classification LIKE 'SME%' or sale_order.customer_classification LIKE 'HORECA%') and account_invoice.id=sale_order_invoice_rel.invoice_id and sale_order_invoice_rel.order_id=sale_order.id and account_invoice.type in ('out_invoice','out_refund') and account_invoice.state  in ('open','paid') and res_partner.parent_id is not NULL and sale_order.partner_id = res_partner.id and account_invoice.create_date>='{0}' and account_invoice.create_date <='{1}' group by account_invoice.type,sale_order.customer_classification,res_partner.parent_id  order by account_invoice.type,total".format(st_date, end_date)

        else:
            company_wise_inv_query = "select sum(account_invoice.amount_total) as total,(SELECT cus.name FROM res_partner AS cus WHERE cus.id = res_partner.parent_id) as company_name,sale_order.customer_classification as customer_classification,account_invoice.type from account_invoice,sale_order_invoice_rel,sale_order,res_partner where  (sale_order.customer_classification LIKE 'Ananta%' or sale_order.customer_classification LIKE 'Corporate%'or sale_order.customer_classification LIKE 'SOHO%' or sale_order.customer_classification LIKE 'SME%' or sale_order.customer_classification LIKE 'HORECA%') and account_invoice.id=sale_order_invoice_rel.invoice_id and sale_order_invoice_rel.order_id=sale_order.id and account_invoice.type in ('out_invoice','out_refund') and account_invoice.state  in ('open','paid') and res_partner.parent_id is not NULL and sale_order.partner_id = res_partner.id and account_invoice.create_date>='{0}' and account_invoice.create_date <='{1}' group by account_invoice.type,sale_order.customer_classification,res_partner.parent_id  order by account_invoice.type,total".format(st_date, end_date)

        objects = cr.execute(company_wise_inv_query)
        objects = cr.dictfetchall()

        categ_list = []
        inv_amount_rm = {}
        ref_amount_rm = {}
        delivery_amount_dict = {}
        inv_amount_list =[]
        ref_amount_list =[]

        for line in objects:
            if line.get('company_name') not in categ_list:
                categ_list.append(line.get('company_name'))
                inv_amount_rm[line.get('company_name')] = 0.00
                ref_amount_rm[line.get('company_name')] = 0.00
                delivery_amount_dict[line.get('company_name')] = 0.00

            if line.get('type') == 'out_invoice':
                inv_amount_rm[line.get('company_name')] = inv_amount_rm[line.get('company_name')] + line.get('total')
                inv_amount_list.append(inv_amount_rm[line.get('company_name')])

            else:
                ref_amount_rm[line.get('company_name')] = ref_amount_rm[line.get(
                    'company_name')] + line.get('total')
                ref_amount_list.append(ref_amount_rm[line.get('company_name')])

        total_net_delivery = sum(inv_amount_list) - sum(ref_amount_list)
        rm_name_list = []
        net_delivery_list = []
        contribution_list = []
        data_dict = {}
        net_delivery = 0
        contribution = 0

        for item in categ_list:
            try:
                net_delivery = (inv_amount_rm[item] - ref_amount_rm[item])
            except ZeroDivisionError:
                pass
            if net_delivery>0:
                data_dict.update({
                    item:net_delivery
                })
        final_data = sorted(data_dict.iteritems(), key=operator.itemgetter(1), reverse=True)[:10]

        for company in final_data:

            try:
                contribution = round(((company[1]/total_net_delivery)*100),2)
            except ZeroDivisionError:
                pass
            rm_name_list.append(company[0])
            net_delivery_list.append(company[1])
            contribution_list.append(contribution)

        return [rm_name_list,net_delivery_list,contribution_list]

    def corRmInvoiceRefund(self, cr, uid, start_date=None, end_date=None, sale_type=None, context=None):

        tmp = start_date
        start_date = end_date
        end_date = tmp

        if sale_type == 'monthly':
            start_date = self.present_month_first_date()
            last_date_of_this_month = self.last_day_this_month()
            end_date = last_date_of_this_month

        st_date = start_date
        end_date = self.date_plus_one(end_date)

        #####  RM Wise invoice Refund ########
        if sale_type and (sale_type =='Ananta' or sale_type =='Corporate'):
            if sale_type == 'Ananta':
                rm_wise_inv_query = "SELECT sum(account_invoice.amount_total) AS total,(SELECT res_partner.name FROM res_partner WHERE res_partner.id = res_users.partner_id) AS rm_name,sale_order.customer_classification  AS customer_classification, account_invoice.type FROM account_invoice, sale_order_invoice_rel, sale_order, res_users WHERE (sale_order.customer_classification LIKE 'Ananta%') AND account_invoice.id = sale_order_invoice_rel.invoice_id AND sale_order_invoice_rel.order_id = sale_order.id AND account_invoice.type IN ('out_invoice', 'out_refund') AND account_invoice.state IN ('open', 'paid') AND sale_order.custom_salesperson = res_users.id AND account_invoice.create_date >= '{0}' AND account_invoice.create_date <= '{1}' GROUP BY account_invoice.type, sale_order.customer_classification, res_users.partner_id ORDER BY account_invoice.type, total".format(st_date, end_date)
            else:
                rm_wise_inv_query = "SELECT sum(account_invoice.amount_total) AS total,(SELECT res_partner.name FROM res_partner WHERE res_partner.id = res_users.partner_id) AS rm_name,sale_order.customer_classification  AS customer_classification, account_invoice.type FROM account_invoice, sale_order_invoice_rel, sale_order, res_users WHERE (sale_order.customer_classification LIKE 'Corporate%'or sale_order.customer_classification LIKE 'SOHO%'or sale_order.customer_classification LIKE 'SME%' or sale_order.customer_classification LIKE 'HORECA%') AND account_invoice.id = sale_order_invoice_rel.invoice_id AND sale_order_invoice_rel.order_id = sale_order.id AND account_invoice.type IN ('out_invoice', 'out_refund') AND account_invoice.state IN ('open', 'paid') AND sale_order.custom_salesperson = res_users.id AND account_invoice.create_date >= '{0}' AND account_invoice.create_date <= '{1}' GROUP BY account_invoice.type, sale_order.customer_classification, res_users.partner_id ORDER BY account_invoice.type, total".format(
                    st_date, end_date)


        else:

            rm_wise_inv_query = "SELECT sum(account_invoice.amount_total) AS total,(SELECT res_partner.name FROM res_partner WHERE res_partner.id = res_users.partner_id) AS rm_name,sale_order.customer_classification  AS customer_classification, account_invoice.type FROM account_invoice, sale_order_invoice_rel, sale_order, res_users WHERE (sale_order.customer_classification LIKE 'Ananta%' OR sale_order.customer_classification LIKE 'Corporate%'or sale_order.customer_classification LIKE 'SOHO%' or sale_order.customer_classification LIKE 'SME%' or sale_order.customer_classification LIKE 'HORECA%') AND account_invoice.id = sale_order_invoice_rel.invoice_id AND sale_order_invoice_rel.order_id = sale_order.id AND account_invoice.type IN ('out_invoice', 'out_refund') AND account_invoice.state IN ('open', 'paid') AND sale_order.custom_salesperson = res_users.id AND account_invoice.create_date >= '{0}' AND account_invoice.create_date <= '{1}' GROUP BY account_invoice.type, sale_order.customer_classification, res_users.partner_id ORDER BY account_invoice.type, total".format(st_date, end_date)
        objects = cr.execute(rm_wise_inv_query)
        objects = cr.dictfetchall()

        categ_list = []
        inv_amount_rm = {}
        ref_amount_rm = {}

        for line in objects:
            if line.get('rm_name') not in categ_list:
                categ_list.append(line.get('rm_name'))
                inv_amount_rm[line.get('rm_name')] = 0.00
                ref_amount_rm[line.get('rm_name')] = 0.00

            if line.get('type') == 'out_invoice':
                inv_amount_rm[line.get('rm_name')] = inv_amount_rm[line.get('rm_name')] + line.get('total')  ## It is for categ wise sales

            else:
                ref_amount_rm[line.get('rm_name')] = ref_amount_rm[line.get(
                    'rm_name')] + line.get('total')  ## It is for categ wise sales

            ### Ends here

        rm_name_list = []
        net_delivery_list = []
        inv_list = []
        refund_delivery_list = []
        refund_percent_list = []
        net_delivery = 0
        refund_percent = 0

        for item in categ_list:
            try:
                net_delivery = round((((inv_amount_rm[item] - ref_amount_rm[item]))), 2)
                refund_percent = round(((ref_amount_rm[item]/inv_amount_rm[item])*100), 2)
                # pnl = round((((inv_amount_rm[item] - margin_cost_categ_dict[item]) / inv_amount_rm[item]) * 100), 2)
            except ZeroDivisionError:
                pass

            rm_name_list.append(item)
            inv_list.append(inv_amount_rm[item])
            refund_delivery_list.append(ref_amount_rm[item])
            net_delivery_list.append(float(net_delivery))
            refund_percent_list.append(refund_percent)

        if len(list(set(refund_percent_list)))==1 and list(set(refund_percent_list))[0]==0:
            refund_percent_list = []

        return [rm_name_list,inv_list,refund_delivery_list,net_delivery_list,refund_percent_list]

    def industry_wise_invoice_margin(self, cr, uid, start_date=None, end_date=None, sale_type=None, context=None):
        uid = 1

        tmp = start_date
        end_date = tmp

        last_date_of_this_month = self.last_day_this_month()
        start_date_list = last_date_of_this_month.split('-')
        start_date_list[2] = '1'
        start_date = '-'.join(start_date_list)
        end_date = last_date_of_this_month

        st_date = start_date
        end_date = self.date_plus_one(end_date)


        if sale_type and sale_type!='all':
            classifiation_type = '%' + str(sale_type) + '%'
            if sale_type == 'Ananta':
                primary_cogs_query = "SELECT stock_move.id AS moved_id,stock_move.product_id,stock_picking.picking_type_id,stock_move.product_uom_qty,(SELECT cus.x_industry FROM res_partner AS cus WHERE cus.id = sale_order_line.order_partner_id) as customer_industry,sale_order.customer_classification , sale_order_line.price_unit,(SELECT sum((sq.qty * sm.price_unit)) AS total FROM stock_quant_move_rel AS sqm, stock_move AS sm, stock_quant AS sq WHERE sqm.quant_id IN ((SELECT sq2.id FROM stock_quant_move_rel AS sqmr2, stock_quant AS sq2 WHERE sqmr2.move_id = stock_move.id AND sqmr2.quant_id = sq2.id)) AND sqm.move_id = sm.id AND sm.purchase_line_id > 0 AND sq.id = sqm.quant_id GROUP BY sq.product_id) AS total_price FROM stock_move, stock_picking, sale_order_line, sale_order, account_invoice, res_partner WHERE stock_move.picking_id = stock_picking.id AND stock_move.sale_line_id = sale_order_line.id AND sale_order_line.order_id = sale_order.id AND stock_picking.name = account_invoice.origin AND (sale_order.customer_classification LIKE 'Ananta%') AND account_invoice.state != 'cancel' AND account_invoice.state != 'draft' AND sale_order_line.order_partner_id = res_partner.id AND account_invoice.type IN ('out_invoice', 'out_refund') AND account_invoice.create_date >= '{0}' AND account_invoice.create_date <= '{1}'" \
                .format(st_date, end_date)
            else:
                primary_cogs_query = "SELECT stock_move.id AS moved_id,stock_move.product_id,stock_picking.picking_type_id,stock_move.product_uom_qty,(SELECT cus.x_industry FROM res_partner AS cus WHERE cus.id = sale_order_line.order_partner_id) as customer_industry,sale_order.customer_classification , sale_order_line.price_unit,(SELECT sum((sq.qty * sm.price_unit)) AS total FROM stock_quant_move_rel AS sqm, stock_move AS sm, stock_quant AS sq WHERE sqm.quant_id IN ((SELECT sq2.id FROM stock_quant_move_rel AS sqmr2, stock_quant AS sq2 WHERE sqmr2.move_id = stock_move.id AND sqmr2.quant_id = sq2.id)) AND sqm.move_id = sm.id AND sm.purchase_line_id > 0 AND sq.id = sqm.quant_id GROUP BY sq.product_id) AS total_price FROM stock_move, stock_picking, sale_order_line, sale_order, account_invoice, res_partner WHERE stock_move.picking_id = stock_picking.id AND stock_move.sale_line_id = sale_order_line.id AND sale_order_line.order_id = sale_order.id AND stock_picking.name = account_invoice.origin AND (sale_order.customer_classification LIKE 'Corporate%' or sale_order.customer_classification LIKE 'SOHO%' or sale_order.customer_classification LIKE 'SME%' or sale_order.customer_classification LIKE 'HORECA%') AND account_invoice.state != 'cancel' AND account_invoice.state != 'draft' AND sale_order_line.order_partner_id = res_partner.id AND account_invoice.type IN ('out_invoice', 'out_refund') AND account_invoice.create_date >= '{0}' AND account_invoice.create_date <= '{1}'" \
                    .format(st_date, end_date)

        else:
            primary_cogs_query = "SELECT stock_move.id AS moved_id,stock_move.product_id,stock_picking.picking_type_id,stock_move.product_uom_qty,(SELECT cus.x_industry FROM res_partner AS cus WHERE cus.id = sale_order_line.order_partner_id) as customer_industry,sale_order.customer_classification , sale_order_line.price_unit,(SELECT sum((sq.qty * sm.price_unit)) AS total FROM stock_quant_move_rel AS sqm, stock_move AS sm, stock_quant AS sq WHERE sqm.quant_id IN ((SELECT sq2.id FROM stock_quant_move_rel AS sqmr2, stock_quant AS sq2 WHERE sqmr2.move_id = stock_move.id AND sqmr2.quant_id = sq2.id)) AND sqm.move_id = sm.id AND sm.purchase_line_id > 0 AND sq.id = sqm.quant_id GROUP BY sq.product_id) AS total_price FROM stock_move, stock_picking, sale_order_line, sale_order, account_invoice, res_partner WHERE stock_move.picking_id = stock_picking.id AND stock_move.sale_line_id = sale_order_line.id AND sale_order_line.order_id = sale_order.id AND stock_picking.name = account_invoice.origin AND (sale_order.customer_classification LIKE 'Ananta%' OR sale_order.customer_classification LIKE 'Corporate%'or sale_order.customer_classification LIKE 'SOHO%' or sale_order.customer_classification LIKE 'SME%' or sale_order.customer_classification LIKE 'HORECA%') AND account_invoice.state != 'cancel' AND account_invoice.state != 'draft' AND sale_order_line.order_partner_id = res_partner.id AND account_invoice.type IN ('out_invoice', 'out_refund') AND account_invoice.create_date >= '{0}' AND account_invoice.create_date <= '{1}'" \
                    .format(st_date, end_date)

        objects = cr.execute(primary_cogs_query)
        objects = cr.dictfetchall()

        in_query = "select id from  stock_picking_type where code='incoming'"
        cr.execute(in_query)
        in_objects = cr.dictfetchall()

        incoming_ids = [in_item.get('id') for in_item in in_objects]

        industry_list = []
        sale_industry_dict = {}
        invoice_industry_dict = {}
        refund_industry_dict = {}
        margin_cost_industry_dict = {}

        total_sales = 0.00

        total_margin_cost = 0.00

        for line in objects:
            if line.get('customer_industry') and line['customer_industry'] :
                line['customer_industry'] = line.get('customer_industry')
            else:
                line['customer_industry'] = 'Others'

            if line.get('customer_industry') not in industry_list:
                industry_list.append(line.get('customer_industry'))
                sale_industry_dict[line.get('customer_industry')] = 0.00
                invoice_industry_dict[line.get('customer_industry')] = 0.00
                refund_industry_dict[line.get('customer_industry')] = 0.00
                margin_cost_industry_dict[line.get('customer_industry')] = 0.00

            cal_cost = 0

            if line.get('total_price') is not None:
                cal_cost = line.get('total_price')

            # if cal_cost == 0:
            #     get_adjusted_cost_query = "select sum((sq.qty * sm.price_unit)) as total from stock_quant_move_rel as sqm,stock_move as sm, stock_quant as sq where sqm.quant_id in ((select sq2.id from stock_quant_move_rel as sqmr2, stock_quant as sq2 where sqmr2.move_id=%s and sqmr2.quant_id = sq2.id)) and sqm.move_id = sm.id and sm.inventory_id >0 and sq.id=sqm.quant_id group by sq.product_id"
            #     moved_id = line.get('moved_id')
            #     cr.execute(get_adjusted_cost_query, ([moved_id]))
            #     get_adjusted_cost = cr.dictfetchall()
            #     for a_cost in get_adjusted_cost:
            #         if a_cost.get('total') is not None:
            #             cal_cost = a_cost.get('total')
            #
            # # Floowing chekup for sq qty is zero then
            # if cal_cost == 0:
            #
            #     get_adjusted_query = "select sq.cost,sm.price_unit from stock_quant_move_rel as sqm,stock_move as sm, stock_quant as sq where sqm.quant_id in (select sq2.id from stock_quant_move_rel as sqmr2, stock_quant as sq2 where sqmr2.move_id=%s and sqmr2.quant_id = sq2.id) and sqm.move_id = sm.id  and sq.id=sqm.quant_id and (sm.purchase_line_id >0 or sm.inventory_id >0) ORDER BY sm.id desc limit 1"
            #     moved_id = line.get('moved_id')
            #     cr.execute(get_adjusted_query, ([moved_id]))
            #     get_adjusted_cost = cr.dictfetchall()
            #     for a_cost in get_adjusted_cost:
            #         if a_cost.get('price_unit') is not None:
            #             cal_cost = a_cost.get('price_unit') * line.get('product_uom_qty')
            #         elif a_cost.get('cost') is not None:
            #             cal_cost = a_cost.get('cost') * line.get('product_uom_qty')

            ### here the profit and sales will be calculated

            line_total_sale = float(line.get('price_unit') * line.get('product_uom_qty'))

            if line.get('picking_type_id') in incoming_ids:
                line_total_sale = line_total_sale * -1
                cal_cost = cal_cost * -1

                refund_industry_dict[line.get('customer_industry')] = refund_industry_dict[line.get(
                    'customer_industry')] - line_total_sale  ## It is for industry wise sales
            else:

                invoice_industry_dict[line.get('customer_industry')] = invoice_industry_dict[line.get(
                    'customer_industry')] + line_total_sale  ## It is for industry wise sales

            # total_sales = total_sales + line_total_sale  ## IT is for total sales

            total_margin_cost = total_margin_cost + cal_cost  ## It is for total Cost

            sale_industry_dict[line.get('customer_industry')] = sale_industry_dict[line.get(
                'customer_industry')] + line_total_sale  ## It is for categ wise sales

            margin_cost_industry_dict[line.get('customer_industry')] = margin_cost_industry_dict[line.get(
                'customer_industry')] + cal_cost  ## It is for categ wise cost


            ### Ends here

        net_delivery_list = []
        industry_name_list = []
        invoice_list = []
        refund_list = []
        margin_list = []
        profit_list = []
        refund_percent_list = []
        pnl = 0
        profit = 0
        refund_percent = 0

        for item in industry_list:
            try:
                pnl = round(
                    (((sale_industry_dict[item] - margin_cost_industry_dict[item]) / sale_industry_dict[item]) * 100),
                    2)
                profit = round(((sale_industry_dict[item] - margin_cost_industry_dict[item])), 2)
                refund_percent = (refund_industry_dict[item] / invoice_industry_dict[item]) * 100
            except ZeroDivisionError:
                pass

            industry_name_list.append(item)
            net_delivery_list.append(float(sale_industry_dict[item]) if float(sale_industry_dict[item])>= 0 else 0.00)
            invoice_list.append(round(invoice_industry_dict[item]))
            refund_list.append(round(refund_industry_dict[item],2))
            margin_list.append(float(pnl))
            profit_list.append(float(profit))
            refund_percent_list.append(refund_percent)
        if len(list(set(refund_percent_list)))==1 and list(set(refund_percent_list))[0]==0:
            refund_percent_list = []
        if len(list(set(margin_list)))==1 and list(set(margin_list))[0]==0:
            margin_list = []

        return [industry_name_list, invoice_list, refund_list, net_delivery_list, margin_list, profit_list,refund_percent_list]

    def industry_wise_sales_value(self, cr, uid, start_date=None, end_date=None, sale_type=None, context=None):

        tmp = start_date
        end_date = tmp

        start_date = self.present_month_first_date()

        #### Sales Data calculation
        if sale_type and sale_type!='all':
            classifiation_type = '%' + str(sale_type) + '%'
            if sale_type == 'Ananta':
                sales_query = "SELECT customer_classification,sum(sale_order.amount_total) AS total, (SELECT cus.x_industry FROM res_partner AS cus WHERE cus.id = sale_order.partner_id) AS customer_industry FROM sale_order, res_partner WHERE state IN ('done', 'progress', 'manual', 'shipping_except', 'invoice_except' ) AND (customer_classification LIKE 'Ananta%') AND sale_order.partner_id = res_partner.id AND date_confirm>='{0}' AND date_confirm <= '{1}' GROUP BY customer_classification,sale_order.partner_id".format(start_date, end_date)
            else:
                sales_query = "SELECT customer_classification,sum(sale_order.amount_total) AS total, (SELECT cus.x_industry FROM res_partner AS cus WHERE cus.id = sale_order.partner_id) AS customer_industry FROM sale_order, res_partner WHERE state IN ('done', 'progress', 'manual', 'shipping_except', 'invoice_except' ) AND (customer_classification LIKE 'Corporate%' OR customer_classification LIKE 'SOHO%' OR customer_classification LIKE 'SME%' OR customer_classification LIKE 'HORECA%') AND sale_order.partner_id = res_partner.id AND date_confirm>='{0}' AND date_confirm <= '{1}' GROUP BY customer_classification,sale_order.partner_id".format(
                start_date, end_date)

        else:
            sales_query = "SELECT customer_classification,sum(sale_order.amount_total) AS total, (SELECT cus.x_industry FROM res_partner AS cus WHERE cus.id = sale_order.partner_id) AS customer_industry FROM sale_order, res_partner WHERE state IN ('done', 'progress', 'manual', 'shipping_except', 'invoice_except' ) AND (customer_classification LIKE 'Ananta%' OR customer_classification LIKE 'Corporate%' OR customer_classification LIKE 'SOHO%' OR customer_classification LIKE 'SME%' OR customer_classification LIKE 'HORECA%') AND sale_order.partner_id = res_partner.id AND date_confirm>='{0}' AND date_confirm <= '{1}' GROUP BY customer_classification,sale_order.partner_id".format(start_date, end_date)

        # cr.execute(sales_query, (start_date, end_date))
        cr.execute(sales_query)

        sales_data = cr.dictfetchall()

        sale_data_dict = {}
        customer_industry_list = []
        customer_industry_label= []
        customer_industry_sale_value= []

        for line in sales_data:
            if line.get('customer_industry') and line['customer_industry'] :
                line['customer_industry'] = line.get('customer_industry')
            else:
                line['customer_industry'] = 'Others'

            if line.get('customer_industry') not in customer_industry_list:
                customer_industry_list.append(line.get('customer_industry'))
                sale_data_dict[line.get('customer_industry')] = 0.00
            sale_data_dict[line.get('customer_industry')] = sale_data_dict[line.get('customer_industry')] + line.get('total')


        for k, v in sale_data_dict.iteritems():
            customer_industry_label.append(k)
            customer_industry_sale_value.append(v)

        #### Sales Data calculation Ends Here

        return [customer_industry_label,
                customer_industry_sale_value
                ]

    ###orderAnalysisRmWiseOrder
    def rm_wise_sales_value(self, cr, uid, start_date=None, end_date=None, sale_type=None, context=None):
        tmp = start_date
        start_date = end_date
        end_date = tmp

        start_date = self.present_month_first_date()
        #### Sales Data calculation
        if sale_type and sale_type!='all':
            classifiation_type = '%' + str(sale_type) + '%'
            if sale_type =='Ananta':
                sales_query = "SELECT sum(amount_total) AS total, count(sale_order.id) AS order_count, (SELECT res_partner.name FROM res_partner WHERE res_partner.id = res_users.partner_id) AS rm_name FROM sale_order,res_users WHERE state IN ('done', 'progress', 'manual', 'shipping_except', 'invoice_except') AND (customer_classification LIKE 'Ananta%') AND date_confirm >= '{0}' AND date_confirm <= '{1}' AND res_users.id = custom_salesperson GROUP BY res_users.partner_id".format(start_date, end_date)
            else:
                sales_query = "SELECT sum(amount_total) AS total, count(sale_order.id) AS order_count, (SELECT res_partner.name FROM res_partner WHERE res_partner.id = res_users.partner_id) AS rm_name FROM sale_order,res_users WHERE state IN ('done', 'progress', 'manual', 'shipping_except', 'invoice_except') AND (customer_classification LIKE 'Corporate%' OR customer_classification LIKE 'SOHO%' OR customer_classification LIKE 'SME%' OR customer_classification LIKE 'HORECA%') AND date_confirm >= '{0}' AND date_confirm <= '{1}' AND res_users.id = custom_salesperson GROUP BY res_users.partner_id".format(
                    start_date, end_date)

        else:
            sales_query = "SELECT sum(amount_total) AS total, count(sale_order.id) AS order_count, (SELECT res_partner.name FROM res_partner WHERE res_partner.id = res_users.partner_id) AS rm_name FROM sale_order,res_users WHERE state IN ('done', 'progress', 'manual', 'shipping_except', 'invoice_except') AND (customer_classification LIKE 'Ananta%' OR customer_classification LIKE 'Corporate%' OR customer_classification LIKE 'SOHO%' OR customer_classification LIKE 'SME%' OR customer_classification LIKE 'HORECA%') AND date_confirm >= '{0}' AND date_confirm <= '{1}' AND res_users.id = custom_salesperson GROUP BY res_users.partner_id".format(start_date, end_date)

        cr.execute(sales_query)

        sales_data = cr.dictfetchall()

        rm_name_list = []
        sales_value= []
        sales_order_count= []
        avg_order_value_list= []


        for item in sales_data:
            if item.get('rm_name') and item['rm_name']:
                item['rm_name'] = item.get('rm_name')
            else:
                item['rm_name'] = 'Others'
            ticket_size = float(item['total']/item['order_count'])

            rm_name_list.append(item['rm_name'])
            sales_value.append(item['total'])
            sales_order_count.append(int(item['order_count']))
            avg_order_value_list.append(ticket_size)

        #### Sales Data calculation Ends Here
        return [rm_name_list,
                sales_order_count,
                avg_order_value_list
                ]

    def rm_wise_sales_customer_value(self, cr, uid, start_date=None, end_date=None, sale_type=None, context=None):

        tmp = start_date
        start_date = end_date
        end_date = tmp

        start_date = self.present_month_first_date()
        #####  RM Wise sale customer ########
        if sale_type and sale_type!='all':
            classifiation_type = '%' + str(sale_type) + '%'
            if sale_type == 'Ananta':
                rm_wise_inv_query = "SELECT sum(amount_total) AS total,count(sale_order.id) AS order_count, (SELECT id FROM res_partner AS cus WHERE cus.id = res_partner.parent_id) as company_id,(SELECT res_partner.name FROM res_partner WHERE res_partner.id = res_users.partner_id) AS rm_name FROM sale_order, res_users , res_partner WHERE state IN ('done', 'progress', 'manual', 'shipping_except', 'invoice_except') AND (customer_classification LIKE 'Ananta%') AND date_confirm >= '{0}' AND date_confirm <= '{1}' AND sale_order.partner_id = res_partner.id AND custom_salesperson = res_users.id GROUP BY res_partner.parent_id,res_users.partner_id" \
            .format(start_date, end_date)

            else:
                rm_wise_inv_query = "SELECT sum(amount_total) AS total,count(sale_order.id) AS order_count, (SELECT id FROM res_partner AS cus WHERE cus.id = res_partner.parent_id) as company_id,(SELECT res_partner.name FROM res_partner WHERE res_partner.id = res_users.partner_id) AS rm_name FROM sale_order, res_users , res_partner WHERE state IN ('done', 'progress', 'manual', 'shipping_except', 'invoice_except') AND (customer_classification LIKE 'Corporate%' OR customer_classification LIKE 'SOHO%' OR customer_classification LIKE 'SME%' OR customer_classification LIKE 'HORECA%') AND date_confirm >= '{0}' AND date_confirm <= '{1}' AND sale_order.partner_id = res_partner.id AND custom_salesperson = res_users.id GROUP BY res_partner.parent_id,res_users.partner_id" \
                    .format(start_date, end_date)

        else:
            rm_wise_inv_query = "SELECT sum(amount_total) AS total,count(sale_order.id) AS order_count, (SELECT id FROM res_partner AS cus WHERE cus.id = res_partner.parent_id) as company_id,(SELECT res_partner.name FROM res_partner WHERE res_partner.id = res_users.partner_id) AS rm_name FROM sale_order, res_users , res_partner WHERE state IN ('done', 'progress', 'manual', 'shipping_except', 'invoice_except') AND (customer_classification LIKE 'Ananta%' OR customer_classification LIKE 'Corporate%' OR customer_classification LIKE 'SOHO%' OR customer_classification LIKE 'SME%' OR customer_classification LIKE 'HORECA%') AND date_confirm >= '{0}' AND date_confirm <= '{1}' AND sale_order.partner_id = res_partner.id AND custom_salesperson = res_users.id GROUP BY res_partner.parent_id,res_users.partner_id" \
                    .format(start_date, end_date)

        objects = cr.execute(rm_wise_inv_query)
        objects = cr.dictfetchall()

        rm_list = []
        order_value = {}
        order_count = {}
        order_company_count = {}

        for line in objects:
            if line.get('rm_name') not in rm_list:
                rm_list.append(line.get('rm_name'))
                order_value[line.get('rm_name')] = 0.00
                order_count[line.get('rm_name')] = 0.00
                order_company_count[line.get('rm_name')] = 0.00

            order_value[line.get('rm_name')] = order_value[line.get('rm_name')] + line.get('total')
            order_count[line.get('rm_name')] = order_count[line.get('rm_name')] + line.get('order_count')
            order_company_count[line.get('rm_name')] = order_company_count[line.get('rm_name')] + 1

        ### Ends here

        rm_name_list = []
        order_per_customer_list = []
        aov_per_customer_list = []
        order_company_count_list = []

        order_per_customer = 0
        aov_per_customer = 0
        total_order_count = 0.00
        total_order_value = 0.00
        total_order_customer_count =total_order_per_customer=total_aov_cart=total_aov_per_customer= 0

        for item in rm_list:
            try:
                order_per_customer = round((order_count[item]/order_company_count[item]), 2)
                aov_per_customer = round((order_value[item]/order_company_count[item]), 2)
            except ZeroDivisionError:
                pass

            rm_name_list.append(item)
            order_company_count_list.append(order_company_count[item])
            order_per_customer_list.append(order_per_customer)
            aov_per_customer_list.append(aov_per_customer)
            total_order_count+= order_count[item]
            total_order_value+= order_value[item]
        try:
            total_order_customer_count=len(objects)
            total_order_per_customer = round((total_order_count/total_order_customer_count),2)
            total_aov_per_customer = round((total_order_value/total_order_customer_count),2)
            total_aov_cart = round((total_order_value/total_order_count),2)
        except ZeroDivisionError:
            pass

        return [rm_name_list,order_company_count_list,order_per_customer_list,aov_per_customer_list,
                total_order_count,total_order_customer_count,total_order_per_customer,total_aov_cart,
                total_aov_per_customer,total_order_value]

    def corporate_dashboard_data_preparation(self, cr, uid, start_date=None, end_date=None, sale_type=None,
                                              context=None):
        top_ten_customer = self.topTenCustomerdelivery(cr, uid, start_date=start_date, end_date=end_date,sale_type=sale_type)
        rm_invoice_refund = self.corRmInvoiceRefund(cr, uid, start_date=start_date, end_date=end_date,sale_type=sale_type)
        sales_delivery_refund = self.toDayWeekMonthSalesInvoiceRefundNetInvoice(cr, uid, start_date=start_date, end_date=end_date,sale_type=sale_type)
        industry_wise_invoice_margin = self.industry_wise_invoice_margin(cr, uid, start_date=start_date, end_date=end_date,sale_type=sale_type)
        industry_wise_sales_value = self.industry_wise_sales_value(cr, uid, start_date=start_date, end_date=end_date,sale_type=sale_type)
        rm_wise_sales_value = self.rm_wise_sales_value(cr, uid, start_date=start_date, end_date=end_date,sale_type=sale_type)
        rm_wise_sales_customer_value = self.rm_wise_sales_customer_value(cr, uid, start_date=start_date, end_date=end_date,sale_type=sale_type)
        rm_wise_target_achievement = self.rm_wise_target_achievement(cr, uid, start_date=start_date, end_date=end_date,sale_type=sale_type)

        final_list= [top_ten_customer[0],top_ten_customer[1],top_ten_customer[2],

                     rm_invoice_refund[0], rm_invoice_refund[1], rm_invoice_refund[2], rm_invoice_refund[3], rm_invoice_refund[4],

                     sales_delivery_refund[0], sales_delivery_refund[1], sales_delivery_refund[2], sales_delivery_refund[3],
                     sales_delivery_refund[4],sales_delivery_refund[5],sales_delivery_refund[6],sales_delivery_refund[7],sales_delivery_refund[8],sales_delivery_refund[9],
                     sales_delivery_refund[10],sales_delivery_refund[11],sales_delivery_refund[12],sales_delivery_refund[13],sales_delivery_refund[14],sales_delivery_refund[15],

                     industry_wise_invoice_margin[0], industry_wise_invoice_margin[1], industry_wise_invoice_margin[2], industry_wise_invoice_margin[3], industry_wise_invoice_margin[4], industry_wise_invoice_margin[5], industry_wise_invoice_margin[6],

                     industry_wise_sales_value[0],industry_wise_sales_value[1] ,

                     rm_wise_sales_value[0],rm_wise_sales_value[1],rm_wise_sales_value[2],

                     rm_wise_sales_customer_value[0], rm_wise_sales_customer_value[1], rm_wise_sales_customer_value[2],
                     rm_wise_sales_customer_value[3],rm_wise_sales_customer_value[4], rm_wise_sales_customer_value[5],
                     rm_wise_sales_customer_value[6],rm_wise_sales_customer_value[7], rm_wise_sales_customer_value[8],
                     rm_wise_sales_customer_value[9],
                     rm_wise_target_achievement[0], rm_wise_target_achievement[1], rm_wise_target_achievement[2],
                     rm_wise_target_achievement[3], rm_wise_target_achievement[4], rm_wise_target_achievement[5]
                     ]
        return final_list

    def rm_wise_target_achievement(self, cr, uid, start_date=None, end_date=None, sale_type=None, context=None):
        if type(start_date) and type(end_date) is not str:
            start_date = str(end_date) + "-" + str(start_date+1) + "-1"
            last_date_of_pre_month = self.last_day_previous_month(start_date)

            end_date = last_date_of_pre_month

            to_date = last_date_of_pre_month

        else:

            last_date_of_this_month = self.last_day_this_month()
            start_date_list = last_date_of_this_month.split('-')
            start_date_list[2] = '1'
            start_date = '-'.join(start_date_list)
            end_date = last_date_of_this_month
            to_date = last_date_of_this_month

        st_date = start_date
        end_date = self.date_plus_one(end_date)


        target_amount_query = "SELECT sum(corporate_sales_target_rm.target_amount) AS target_sales_amount,sum(corporate_sales_target_rm.target_delivery_amount) AS target_delivery_amount,corporate_sales_target_rm.emp_name,(SELECT res_partner.name FROM res_partner WHERE res_partner.id = res_users.partner_id) AS rm_name FROM corporate_sales_target, corporate_sales_target_rm,res_users WHERE corporate_sales_target.id = corporate_sales_target_rm.corporate_sales_target_rm_id AND corporate_sales_target.date_from >='{0}' AND corporate_sales_target.date_from <='{1}' and corporate_sales_target_rm.emp_name=res_users.id GROUP BY corporate_sales_target_rm.emp_name , res_users.partner_id".format(
            st_date, to_date)

        cr.execute(target_amount_query)
        target_amount_data = cr.dictfetchall()

        target_rm_list = []
        rm_target_amount = {}

        for target_amount in target_amount_data:
            if target_amount.get('rm_name') not in target_rm_list:
                target_rm_list.append(target_amount.get('rm_name'))
                rm_target_amount[target_amount.get('rm_name')] = 0.00

            rm_target_amount[target_amount.get('rm_name')] = rm_target_amount[
                                                                 target_amount.get('rm_name')] + target_amount.get(
                'target_delivery_amount')

        if sale_type and sale_type != 'all':
            classifiation_type = '%' + str(sale_type) + '%'
            if sale_type == 'Ananta':
                sales_query = "SELECT sum(amount_total) AS amount_total, count(sale_order.id) AS order_count, (SELECT res_partner.name FROM res_partner WHERE res_partner.id = res_users.partner_id) AS rm_name FROM sale_order,res_users WHERE state IN ('done', 'progress', 'manual', 'shipping_except', 'invoice_except') AND (customer_classification LIKE 'Ananta%') AND date_confirm >= '{0}' AND date_confirm <= '{1}' AND res_users.id = custom_salesperson GROUP BY res_users.partner_id".format(
                start_date, to_date)

                primary_cogs_query = "SELECT stock_move.id AS moved_id,stock_move.product_id,stock_picking.picking_type_id,stock_move.product_uom_qty,(SELECT res_partner.name FROM res_partner WHERE res_partner.id = res_users.partner_id) AS rm_name ,sale_order.customer_classification , sale_order_line.price_unit,(SELECT sum((sq.qty * sm.price_unit)) AS total FROM stock_quant_move_rel AS sqm, stock_move AS sm, stock_quant AS sq WHERE sqm.quant_id IN ((SELECT sq2.id FROM stock_quant_move_rel AS sqmr2, stock_quant AS sq2 WHERE sqmr2.move_id = stock_move.id AND sqmr2.quant_id = sq2.id)) AND sqm.move_id = sm.id AND sm.purchase_line_id > 0 AND sq.id = sqm.quant_id GROUP BY sq.product_id) AS total_price FROM stock_move, stock_picking, sale_order_line, sale_order, account_invoice, res_users WHERE stock_move.picking_id = stock_picking.id AND stock_move.sale_line_id = sale_order_line.id AND sale_order_line.order_id = sale_order.id AND stock_picking.name = account_invoice.origin AND (sale_order.customer_classification LIKE 'Ananta%') AND account_invoice.state != 'cancel' AND account_invoice.state != 'draft' AND sale_order.custom_salesperson = res_users.id AND account_invoice.type IN ('out_invoice', 'out_refund') AND account_invoice.create_date >= '{0}' AND account_invoice.create_date <= '{1}'" \
                .format(st_date, end_date)
            else:

                sales_query = "SELECT sum(amount_total) AS amount_total, count(sale_order.id) AS order_count, (SELECT res_partner.name FROM res_partner WHERE res_partner.id = res_users.partner_id) AS rm_name FROM sale_order,res_users WHERE state IN ('done', 'progress', 'manual', 'shipping_except', 'invoice_except') AND (customer_classification LIKE 'Corporate%' OR customer_classification LIKE 'SOHO%' OR customer_classification LIKE 'SME%' OR customer_classification LIKE 'HORECA%') AND date_confirm >= '{0}' AND date_confirm <= '{1}' AND res_users.id = custom_salesperson GROUP BY res_users.partner_id".format(
                start_date, to_date)

                primary_cogs_query = "SELECT stock_move.id AS moved_id,stock_move.product_id,stock_picking.picking_type_id,stock_move.product_uom_qty,(SELECT res_partner.name FROM res_partner WHERE res_partner.id = res_users.partner_id) AS rm_name ,sale_order.customer_classification , sale_order_line.price_unit,(SELECT sum((sq.qty * sm.price_unit)) AS total FROM stock_quant_move_rel AS sqm, stock_move AS sm, stock_quant AS sq WHERE sqm.quant_id IN ((SELECT sq2.id FROM stock_quant_move_rel AS sqmr2, stock_quant AS sq2 WHERE sqmr2.move_id = stock_move.id AND sqmr2.quant_id = sq2.id)) AND sqm.move_id = sm.id AND sm.purchase_line_id > 0 AND sq.id = sqm.quant_id GROUP BY sq.product_id) AS total_price FROM stock_move, stock_picking, sale_order_line, sale_order, account_invoice, res_users WHERE stock_move.picking_id = stock_picking.id AND stock_move.sale_line_id = sale_order_line.id AND sale_order_line.order_id = sale_order.id AND stock_picking.name = account_invoice.origin AND (sale_order.customer_classification LIKE 'Corporate%' OR sale_order.customer_classification LIKE 'SOHO%' OR sale_order.customer_classification LIKE 'SME%' OR sale_order.customer_classification LIKE 'HORECA%') AND account_invoice.state != 'cancel' AND account_invoice.state != 'draft' AND sale_order.custom_salesperson = res_users.id AND account_invoice.type IN ('out_invoice', 'out_refund') AND account_invoice.create_date >= '{0}' AND account_invoice.create_date <= '{1}'" \
                .format(st_date, end_date)

        else:

            sales_query = "SELECT sum(amount_total) AS amount_total, count(sale_order.id) AS order_count, (SELECT res_partner.name FROM res_partner WHERE res_partner.id = res_users.partner_id) AS rm_name FROM sale_order,res_users WHERE state IN ('done', 'progress', 'manual', 'shipping_except', 'invoice_except') AND (customer_classification LIKE 'Ananta%' OR customer_classification LIKE 'Corporate%' OR customer_classification LIKE 'SOHO%' OR customer_classification LIKE 'SME%' OR customer_classification LIKE 'HORECA%') AND date_confirm >= '{0}' AND date_confirm <= '{1}' AND res_users.id = custom_salesperson GROUP BY res_users.partner_id".format(
                start_date, to_date)

            primary_cogs_query = "SELECT stock_move.id AS moved_id,stock_move.product_id,stock_picking.picking_type_id,stock_move.product_uom_qty,(SELECT res_partner.name FROM res_partner WHERE res_partner.id = res_users.partner_id) AS rm_name ,sale_order.customer_classification , sale_order_line.price_unit,(SELECT sum((sq.qty * sm.price_unit)) AS total FROM stock_quant_move_rel AS sqm, stock_move AS sm, stock_quant AS sq WHERE sqm.quant_id IN ((SELECT sq2.id FROM stock_quant_move_rel AS sqmr2, stock_quant AS sq2 WHERE sqmr2.move_id = stock_move.id AND sqmr2.quant_id = sq2.id)) AND sqm.move_id = sm.id AND sm.purchase_line_id > 0 AND sq.id = sqm.quant_id GROUP BY sq.product_id) AS total_price FROM stock_move, stock_picking, sale_order_line, sale_order, account_invoice, res_users WHERE stock_move.picking_id = stock_picking.id AND stock_move.sale_line_id = sale_order_line.id AND sale_order_line.order_id = sale_order.id AND stock_picking.name = account_invoice.origin AND (sale_order.customer_classification LIKE 'Ananta%' OR sale_order.customer_classification LIKE 'Corporate%' OR sale_order.customer_classification LIKE 'SOHO%' OR sale_order.customer_classification LIKE 'SME%' OR sale_order.customer_classification LIKE 'HORECA%') AND account_invoice.state != 'cancel' AND account_invoice.state != 'draft' AND sale_order.custom_salesperson = res_users.id AND account_invoice.type IN ('out_invoice', 'out_refund') AND account_invoice.create_date >= '{0}' AND account_invoice.create_date <= '{1}'" \
                .format(st_date, end_date)

        objects = cr.execute(primary_cogs_query)
        objects = cr.dictfetchall()

        sales_data = cr.execute(sales_query)
        sales_data = cr.dictfetchall()

        so_rm_list = []
        rm_sale_order_amount = {}

        for sales_data_row in sales_data:
            if sales_data_row.get('rm_name') not in so_rm_list:
                so_rm_list.append(sales_data_row.get('rm_name'))
                rm_sale_order_amount[sales_data_row.get('rm_name')] = 0.00

            rm_sale_order_amount[sales_data_row.get('rm_name')] = rm_sale_order_amount[sales_data_row.get('rm_name')] + sales_data_row.get('amount_total')

        in_query = "select id from  stock_picking_type where code='incoming'"
        cr.execute(in_query)
        in_objects = cr.dictfetchall()

        incoming_ids = [in_item.get('id') for in_item in in_objects]

        rm_list = []
        sale_rm_dict = {}
        invoice_rm_dict = {}
        refund_rm_dict = {}
        margin_cost_rm_dict = {}

        total_margin_cost = 0.00

        for line in objects:
            if line.get('rm_name') not in rm_list:
                rm_list.append(line.get('rm_name'))
                sale_rm_dict[line.get('rm_name')] = 0.00
                invoice_rm_dict[line.get('rm_name')] = 0.00
                refund_rm_dict[line.get('rm_name')] = 0.00
                margin_cost_rm_dict[line.get('rm_name')] = 0.00

            cal_cost = 0

            if line.get('total_price') is not None:
                cal_cost = line.get('total_price')

            # if cal_cost == 0:
            #     get_adjusted_cost_query = "select sum((sq.qty * sm.price_unit)) as total from stock_quant_move_rel as sqm,stock_move as sm, stock_quant as sq where sqm.quant_id in ((select sq2.id from stock_quant_move_rel as sqmr2, stock_quant as sq2 where sqmr2.move_id=%s and sqmr2.quant_id = sq2.id)) and sqm.move_id = sm.id and sm.inventory_id >0 and sq.id=sqm.quant_id group by sq.product_id"
            #     moved_id = line.get('moved_id')
            #     cr.execute(get_adjusted_cost_query, ([moved_id]))
            #     get_adjusted_cost = cr.dictfetchall()
            #     for a_cost in get_adjusted_cost:
            #         if a_cost.get('total') is not None:
            #             cal_cost = a_cost.get('total')
            #
            # # Floowing chekup for sq qty is zero then
            # if cal_cost == 0:
            #
            #     get_adjusted_query = "select sq.cost,sm.price_unit from stock_quant_move_rel as sqm,stock_move as sm, stock_quant as sq where sqm.quant_id in (select sq2.id from stock_quant_move_rel as sqmr2, stock_quant as sq2 where sqmr2.move_id=%s and sqmr2.quant_id = sq2.id) and sqm.move_id = sm.id  and sq.id=sqm.quant_id and (sm.purchase_line_id >0 or sm.inventory_id >0) ORDER BY sm.id desc limit 1"
            #     moved_id = line.get('moved_id')
            #     cr.execute(get_adjusted_query, ([moved_id]))
            #     get_adjusted_cost = cr.dictfetchall()
            #     for a_cost in get_adjusted_cost:
            #         if a_cost.get('price_unit') is not None:
            #             cal_cost = a_cost.get('price_unit') * line.get('product_uom_qty')
            #         elif a_cost.get('cost') is not None:
            #             cal_cost = a_cost.get('cost') * line.get('product_uom_qty')

            ### here the profit and sales will be calculated

            line_total_sale = float(line.get('price_unit') * line.get('product_uom_qty'))

            if line.get('picking_type_id') in incoming_ids:
                line_total_sale = line_total_sale * -1
                cal_cost = cal_cost * -1

                refund_rm_dict[line.get('rm_name')] = refund_rm_dict[line.get(
                    'rm_name')] - line_total_sale  ## It is for industry wise sales
            else:

                invoice_rm_dict[line.get('rm_name')] = invoice_rm_dict[line.get(
                    'rm_name')] + line_total_sale  ## It is for industry wise sales

            total_margin_cost = total_margin_cost + cal_cost  ## It is for total Cost

            sale_rm_dict[line.get('rm_name')] = sale_rm_dict[line.get(
                'rm_name')] + line_total_sale  ## It is for categ wise sales

            margin_cost_rm_dict[line.get('rm_name')] = margin_cost_rm_dict[line.get(
                'rm_name')] + cal_cost  ## It is for categ wise cost

            ### Ends here

        net_delivery_list = []
        rm_name_list = []
        achievement_delivery = 0
        profit_margin = 0
        achievement_delivery_list = []
        rm_sale_order_amount_list = []
        profit_margin_list = []
        rm_target_list = []
        final_rm_list = rm_list + so_rm_list
        final_rm_list = list(set(final_rm_list))


        for item in final_rm_list:
            try:
                rm_target_amount[item] = rm_target_amount.get(item) if rm_target_amount.get(item) else 0
                rm_sale_order_amount[item] = rm_sale_order_amount.get(item) if rm_sale_order_amount.get(item) else 0
                sale_rm_dict[item] = sale_rm_dict.get(item) if sale_rm_dict.get(item) else 0
                margin_cost_rm_dict[item] = margin_cost_rm_dict.get(item) if margin_cost_rm_dict.get(item) else 0


                profit = sale_rm_dict[item] - margin_cost_rm_dict[item]
                achievement_delivery = round(((sale_rm_dict[item]/rm_target_amount[item])*100),2)
                profit_margin = round(((profit/sale_rm_dict[item])*100),2)


            except ZeroDivisionError:
                pass

            rm_name_list.append(item)
            net_delivery_list.append(round(sale_rm_dict[item],2))
            achievement_delivery_list.append(achievement_delivery)
            profit_margin_list.append(profit_margin)
            rm_sale_order_amount_list.append(round(rm_sale_order_amount[item],2))
            rm_target_list.append(round(rm_target_amount[item],2))

        if len(list(set(achievement_delivery_list)))==1 and list(set(achievement_delivery_list))[0]==0:
            achievement_delivery_list = []
        if len(list(set(profit_margin_list)))==1 and list(set(profit_margin_list))[0]==0:
            profit_margin_list = []

        return [rm_name_list,rm_sale_order_amount_list,rm_target_list, net_delivery_list, achievement_delivery_list,
                profit_margin_list]



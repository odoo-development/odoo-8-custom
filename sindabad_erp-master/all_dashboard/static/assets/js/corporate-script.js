var s = new openerp.init();
var _labels;
var _values;
var x;
var y;
var z;
var start_date = null;
var end_date = null;
var classification = null;
var _updateflag = false;

//================ Chart Variable

var top_ten_corporate_customers;
var corp_chart_sales;
var corp_chart_invoices;
var corp_chart_refund;
var corp_chart_netinvoices;
var corp_rm_chart;
var corp_industry_chart;
var corp_sales_order;
var industry_sales_order;
var new_pos_chart;

var colorIndustry = ['#008ffb', '#00e396', '#feb019', '#ff4560', '#775dd0', '#800000', '#17560d', '#808000', '#FA8072'];

//Navbar Brand Link Page Refresh
function confirmRefresh() {
    //setTimeout("location.reload(true);",100);
    window.location.reload(true);
}

window.onload = function () {

    var ddlYears = document.getElementById("ddlYears");
    var ddlmonths = document.getElementById("ddlmonths");

    //Determine the Current Year.
    var currentYear = (new Date()).getFullYear();
    var yearsArr = [];
    //Loop and add the Year values to DropDownList.
    var count = 0
    for (var i = 2018; i <= currentYear; i++) {

        var option = document.createElement("OPTION");
        option.innerHTML = i;
        option.value = i;
        ddlYears.appendChild(option);

        yearsArr.push(count);
        count++
}

    //Loop and add the Year values to DropDownList.
    var month = new Array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
    //Determine the Current Month.
      //var currentMonth = (new Date()).getUTCDate();
       var currentMonth = new Date();
       var c_month = month[currentMonth.getMonth()];

    for (var j = 0; j < month.length; j++) {
        var option = document.createElement("OPTION");
        option.innerHTML = month[j];
        ddlmonths.appendChild(option);
    }
    document.getElementById("ddlYears").options[((yearsArr.length)-1)].selected = true;
    document.getElementById("ddlmonths").options[currentMonth.getMonth()].selected = true;

}

// Current Date Start
function currentDate() {
    var d = new Date();
    var today_date = d.getDate() + " " + d.toLocaleString('default', {month: 'long'}) + ", " + d.getFullYear();
    return today_date;
}

document.getElementById('current-date').innerHTML = currentDate();

// Current Date End

// Thousand Separators Start
function thousands_separators(num) {
    num = parseInt(num);
    var num_parts = num.toString().split(".");
    num_parts[0] = num_parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return num_parts.join(".");
}

// Thousand Separators End

// Date Format Start

function formatDate(dateObj) {
    //var dateObj = new Date();
    var today_date = dateObj.getDate();
    var today_month = dateObj.getMonth() + 1;
    var today_year = dateObj.getFullYear();

    var formated_today = today_year + "-" + today_month + "-" + today_date;

    return formated_today;
}

// Date Format End

//Select Classification

function getClassificationSelectedValue() {
    var classification = document.getElementById("classification-select").value;
    return classification;
}


$('#search-month-year').click(function (e) {
    e.preventDefault();

    var months = $("#ddlmonths").val();
    var years = $("#ddlYears").val();
    if (months === '' || years === '') {

        alert("Please give both!");

    } else {
        //Show loader
        $('.loading-mask').show();

        _updateflag = true;

        new s.web.Model("corporate.dashboard").call("rm_wise_target_achievement",
            ([parseInt(months), parseInt(years), z])).then(function (results) {

            targetVsAchievement(results[0], results[1], results[2], results[3], results[4], results[5], _updateflag);
        })
    }
});

// Search By Date Start

$('#search-by-date').click(function (e) {
    e.preventDefault();

    var fromDate = $("#from-date").val();
    var toDate = $("#to-date").val();

    var classification = getClassificationSelectedValue();

    //
    // if(fromDate === '' || toDate === ''){
    //
    //     alert("Please give From Date and To Date both!");
    //
    // }else {
    //Show loader
    $('.loading-mask').show();
    var x = toDate === '' ? formatDate(new Date()) : toDate;
    var y = fromDate === '' ? formatDate(new Date()) : fromDate;
    var z = classification === null ? 'all' : classification;
    _updateflag = true;

    new s.web.Model("corporate.dashboard").call("corporate_dashboard_data_preparation",
        ([x, y, z])).then(function (results) {
        _labels = results[2];
        _values = results[3];

        topTenCorporateCustomers(results[0], results[1], results[2], _updateflag);

        corpRmChart(results[3], results[4], results[5], results[6], results[7], _updateflag);

        // corpChartSales result
        document.getElementById('corp-total-sales').innerHTML = thousands_separators(results[8]);
        document.getElementById('corp-total-sales-count').innerHTML = results[9];
        corpChartSales(results[10], results[11], _updateflag);

        // corpChartInvoices result
        document.getElementById('corp-total-amount').innerHTML = thousands_separators(results[12]);
        document.getElementById('corp-invoice-count').innerHTML = results[13];
        corpChartInvoices(results[14], results[15], _updateflag);

        // corpChartRefund result
        document.getElementById('corp-against-invoice').innerHTML = thousands_separators(results[16]);
        document.getElementById('corp-refund-invoice-count').innerHTML = results[17];
        corpChartRefund(results[18], results[19], _updateflag);

        // corpChartNetInvoices result
        document.getElementById('corp-net-invoice-amount').innerHTML = thousands_separators(results[20]);
        document.getElementById('corp-net-invoice-margin').innerHTML = results[21];

        corpChartNetInvoices(results[22], results[23], _updateflag);

        if (z !== '') {
            industryInvoiceMargin(results[24], results[25], results[26], results[27], results[28], results[29], results[30], _updateflag);

            industrySalesOrder(results[31], results[32], _updateflag);

            orderAnalysisRmWiseOrder(results[33], results[34], results[35], _updateflag);

            newPosChart(results[36], results[37], results[38], results[39], _updateflag);

            document.getElementById('corp-count-of-order').innerHTML = thousands_separators(results[40]);
            document.getElementById('corp-count-of-customers').innerHTML = thousands_separators(results[41]);
            document.getElementById('corp-order-per-customers').innerHTML = thousands_separators(results[42]);
            document.getElementById('corp-aov-per-cart').innerHTML = thousands_separators(results[43]);
            document.getElementById('corp-aov-per-customers').innerHTML = thousands_separators(results[44]);
            document.getElementById('corp-order-value').innerHTML = thousands_separators(results[45]);

            targetVsAchievement(results[46], results[47], results[48], results[49], results[50], results[51], _updateflag);

        }


        $('.loading-mask').hide();

    });

    // }

});


// Search By Date End

//full page Show loader Start
x = start_date === null ? formatDate(new Date()) : start_date;
y = end_date === null ? x : end_date;
z = classification === null ? 'all' : classification;

new s.web.Model("corporate.dashboard").call("topTenCustomerdelivery",
    ([x, y, z])).then(function (results) {


    _companylabels = results[0];
    _netdelivery = results[1];
    _contribution = results[2];

    // topTenCorporateCustomers result
    topTenCorporateCustomers(results[0], results[1], results[2], _updateflag);


});

//full page Show loader End

new s.web.Model("corporate.dashboard").call("toDayWeekMonthSalesInvoiceRefundNetInvoice",
    ([x, y, z])).then(function (results) {


    _saleslabels = results[0];
    _salescount = results[1];

    // corpChartSales result
    var test = document.getElementById('corp-total-sales').innerHTML = thousands_separators(results[0]);
    document.getElementById('corp-total-sales-count').innerHTML = results[1];
    corpChartSales(results[2], results[3], _updateflag);

    // corpChartInvoices result
    document.getElementById('corp-total-amount').innerHTML = thousands_separators(results[4]);
    document.getElementById('corp-invoice-count').innerHTML = results[5];
    corpChartInvoices(results[6], results[7], _updateflag);

    // corpChartRefund result
    document.getElementById('corp-against-invoice').innerHTML = thousands_separators(results[8]);
    document.getElementById('corp-refund-invoice-count').innerHTML = results[9];
    corpChartRefund(results[10], results[11], _updateflag);

    // corpChartNetInvoices result
    document.getElementById('corp-net-invoice-amount').innerHTML = thousands_separators(results[12]);
    document.getElementById('corp-net-invoice-margin').innerHTML = results[13];
    corpChartNetInvoices(results[14], results[15], _updateflag);
});


//Corporate RM Wise Invoice Refund

x = start_date === null ? formatDate(new Date()) : start_date;
y = end_date === null ? x : end_date;
z = classification === null ? 'all' : classification;

new s.web.Model("corporate.dashboard").call("corRmInvoiceRefund",
    ([x, y, z])).then(function (results) {

    // corRmInvoiceRefund result
    corpRmChart(results[0], results[1], results[2], results[3], results[4], _updateflag);


});

//Industry Wise Invoice Refund

x = start_date === null ? formatDate(new Date()) : start_date;
y = end_date === null ? x : end_date;
z = classification === null ? 'all' : classification;

new s.web.Model("corporate.dashboard").call("industry_wise_invoice_margin",
    ([x, y, z])).then(function (results) {

    // corRmInvoiceRefund result
    // console.log(results)
    industryInvoiceMargin(results[0], results[1], results[2], results[3], results[4], results[5], results[6], _updateflag);


});

new s.web.Model("corporate.dashboard").call("industry_wise_sales_value",
    ([x, y, z])).then(function (results) {
    // corRmInvoiceRefund result
    industrySalesOrder(results[0], results[1], _updateflag);


});

//orderAnalysisRmWiseOrder
new s.web.Model("corporate.dashboard").call("rm_wise_sales_value",
    ([x, y, z])).then(function (results) {

    orderAnalysisRmWiseOrder(results[0], results[1], results[2], _updateflag);

});

//rm_wise_target_achievement
new s.web.Model("corporate.dashboard").call("rm_wise_target_achievement",
    ([x, y, z])).then(function (results) {

    targetVsAchievement(results[0], results[1], results[2], results[3], results[4], results[5], _updateflag);

});

//newPosChart
new s.web.Model("corporate.dashboard").call("rm_wise_sales_customer_value",
    ([x, y, z])).then(function (results) {

    // corRmInvoiceRefund result
    newPosChart(results[0], results[1], results[2], results[3], _updateflag);

    document.getElementById('corp-count-of-order').innerHTML = thousands_separators(results[4]);
    document.getElementById('corp-count-of-customers').innerHTML = thousands_separators(results[5]);
    document.getElementById('corp-order-per-customers').innerHTML = thousands_separators(results[6]);
    document.getElementById('corp-aov-per-cart').innerHTML = thousands_separators(results[7]);
    document.getElementById('corp-aov-per-customers').innerHTML = thousands_separators(results[8]);
    document.getElementById('corp-order-value').innerHTML = thousands_separators(results[9]);


});

// SalesToday Button
function corpSalesToday() {

    var ourDate = new Date();
    ourDate.setDate(ourDate.getDate());

    var x = formatDate(new Date());
    var y = formatDate(ourDate);
    var z = 'all';
    _updateflag = true;


    new s.web.Model("corporate.dashboard").call("toDayWeekMonthSalesInvoiceRefundNetInvoice",
        ([x, y, z])).then(function (results) {
            console.log(results);

        _saleslabels = results[0];
        _salescount = results[1];

        // corpChartSales result
        var test = document.getElementById('corp-total-sales').innerHTML = thousands_separators(results[0]);
        document.getElementById('corp-total-sales-count').innerHTML = results[1];
        corpChartSales(results[2], results[3], _updateflag);

        // corpChartInvoices result
        document.getElementById('corp-total-amount').innerHTML = thousands_separators(results[4]);
        document.getElementById('corp-invoice-count').innerHTML = results[5];
        corpChartInvoices(results[6], results[7], _updateflag);

        // corpChartRefund result
        document.getElementById('corp-against-invoice').innerHTML = thousands_separators(results[8]);
        document.getElementById('corp-refund-invoice-count').innerHTML = results[9];
        corpChartRefund(results[10], results[11], _updateflag);

        // corpChartNetInvoices result
        document.getElementById('corp-net-invoice-amount').innerHTML = thousands_separators(results[12]);
        document.getElementById('corp-net-invoice-margin').innerHTML = results[13];
        corpChartNetInvoices(results[14], results[15], _updateflag);


    });
}

// Salesweekly Button
function corpSalesWeekly() {

    var ourDate = new Date();
    ourDate.setDate(ourDate.getDate() - 7);


    var x = formatDate(new Date());
    var y = formatDate(ourDate);
    var z = 'all';
    _updateflag = true;


    new s.web.Model("corporate.dashboard").call("toDayWeekMonthSalesInvoiceRefundNetInvoice",
        ([x, y, z])).then(function (results) {


        _saleslabels = results[0];
        _salescount = results[1];

        // corpChartSales result
        var test = document.getElementById('corp-total-sales').innerHTML = thousands_separators(results[0]);
        document.getElementById('corp-total-sales-count').innerHTML = results[1];
        corpChartSales(results[2], results[3], _updateflag);

        // corpChartInvoices result
        document.getElementById('corp-total-amount').innerHTML = thousands_separators(results[4]);
        document.getElementById('corp-invoice-count').innerHTML = results[5];
        corpChartInvoices(results[6], results[7], _updateflag);

        // corpChartRefund result
        document.getElementById('corp-against-invoice').innerHTML = thousands_separators(results[8]);
        document.getElementById('corp-refund-invoice-count').innerHTML = results[9];
        corpChartRefund(results[10], results[11], _updateflag);

        // corpChartNetInvoices result
        document.getElementById('corp-net-invoice-amount').innerHTML = thousands_separators(results[12]);
        document.getElementById('corp-net-invoice-margin').innerHTML = results[13];
        corpChartNetInvoices(results[14], results[15], _updateflag);


    });
}

// corpSalesMonthly Button
function corpSalesMonthly() {

    var ourDate = new Date();
    ourDate.setDate(ourDate.getDate() - 30);


    var x = formatDate(new Date());
    var y = formatDate(ourDate);
    var z = 'monthly';
    _updateflag = true;

    new s.web.Model("corporate.dashboard").call("toDayWeekMonthSalesInvoiceRefundNetInvoice",
        ([x, y, z])).then(function (results) {


        _saleslabels = results[0];
        _salescount = results[1];

        // corpChartSales result
        var test = document.getElementById('corp-total-sales').innerHTML = thousands_separators(results[0]);
        document.getElementById('corp-total-sales-count').innerHTML = results[1];
        corpChartSales(results[2], results[3], _updateflag);

        // corpChartInvoices result
        document.getElementById('corp-total-amount').innerHTML = thousands_separators(results[4]);
        document.getElementById('corp-invoice-count').innerHTML = results[5];
        corpChartInvoices(results[6], results[7], _updateflag);

        // corpChartRefund result
        document.getElementById('corp-against-invoice').innerHTML = thousands_separators(results[8]);
        document.getElementById('corp-refund-invoice-count').innerHTML = results[9];
        corpChartRefund(results[10], results[11], _updateflag);

        // corpChartNetInvoices result
        document.getElementById('corp-net-invoice-amount').innerHTML = thousands_separators(results[12]);
        document.getElementById('corp-net-invoice-margin').innerHTML = results[13];
        corpChartNetInvoices(results[14], results[15], _updateflag);

    });
}


// corpRmToday Button
function corpRmToday() {

    var ourDate = new Date();
    ourDate.setDate(ourDate.getDate());

    var x = formatDate(new Date());
    var y = formatDate(ourDate);
    var z = 'all';
    _updateflag = true;


    new s.web.Model("corporate.dashboard").call("topTenCustomerdelivery",
        ([x, y, z])).then(function (results) {


        _companylabels = results[0];
        _netdelivery = results[1];
        _contribution = results[2];

        // topTenCorporateCustomers result
        topTenCorporateCustomers(results[0], results[1], results[2], _updateflag);

    });
}

// corpRmWeekly Button
function corpRmWeekly() {

    var ourDate = new Date();
    ourDate.setDate(ourDate.getDate() - 7);


    var x = formatDate(new Date());
    var y = formatDate(ourDate);
    var z = 'all';
    _updateflag = true;


    new s.web.Model("corporate.dashboard").call("topTenCustomerdelivery",
        ([x, y, z])).then(function (results) {


        _companylabels = results[0];
        _netdelivery = results[1];
        _contribution = results[2];

        // topTenCorporateCustomers result
        topTenCorporateCustomers(results[0], results[1], results[2], _updateflag);

    });
}

// corpRmMonthly Button
function corpRmMonthly() {

    var ourDate = new Date();
    ourDate.setDate(ourDate.getDate() - 30);


    var x = formatDate(new Date());
    var y = formatDate(ourDate);
    var z = 'monthly';
    _updateflag = true;

    new s.web.Model("corporate.dashboard").call("topTenCustomerdelivery",
        ([x, y, z])).then(function (results) {


        _companylabels = results[0];
        _netdelivery = results[1];
        _contribution = results[2];

        // topTenCorporateCustomers result
        topTenCorporateCustomers(results[0], results[1], results[2], _updateflag);

    });
}


// corpInvoiceToday Button
function corpInvoiceToday() {

    var ourDate = new Date();
    ourDate.setDate(ourDate.getDate());

    var x = formatDate(new Date());
    var y = formatDate(ourDate);
    var z = 'all';
    _updateflag = true;


    new s.web.Model("corporate.dashboard").call("corRmInvoiceRefund",
        ([x, y, z])).then(function (results) {

        // corRmInvoiceRefund result
        corpRmChart(results[0], results[1], results[2], results[3], results[4], _updateflag);


    });
}

// corpInvoiceWeekly Button
function corpInvoiceWeekly() {

    var ourDate = new Date();
    ourDate.setDate(ourDate.getDate() - 7);


    var x = formatDate(new Date());
    var y = formatDate(ourDate);
    var z = 'all';
    _updateflag = true;

    new s.web.Model("corporate.dashboard").call("corRmInvoiceRefund",
        ([x, y, z])).then(function (results) {

        // corRmInvoiceRefund result
        corpRmChart(results[0], results[1], results[2], results[3], results[4], _updateflag);


    });
}

// corpInvoiceMonthly Button
function corpInvoiceMonthly() {

    var ourDate = new Date();
    ourDate.setDate(ourDate.getDate() - 30);


    var x = formatDate(new Date());
    var y = formatDate(ourDate);
    var z = 'monthly';
    _updateflag = true;

    new s.web.Model("corporate.dashboard").call("corRmInvoiceRefund",
        ([x, y, z])).then(function (results) {

        // corRmInvoiceRefund result
        corpRmChart(results[0], results[1], results[2], results[3], results[4], _updateflag);


    });
}

//corp-chart-sales Start

function corpChartSales(_labels, _values, _updateflag) {

    var options = {
        series: _values,
        labels: _labels,
        chart: {
            type: 'donut',
            height: 250//_labels.length > 1 ? 250 : 220
        },
        legend: {
            position: 'bottom'
        },
        fill: {
            type: 'gradient',
        },
        tooltip: {
            enabled: true,
            dataLabels: {
                enabled: true,
                title: {
                    formatter: function (_values) {
                        return _values
                    }
                },
            },
        },
        responsive: [{
            breakpoint: 480,
            options: {
                legend: {
                    position: 'bottom'
                }
            }
        }],
        yaxis: {
            show: true,
            decimalsInFloat: 2,
            labels: {
                formatter: function (value) {
                    var amount = parseInt(value);

                    return thousands_separators(amount);
                }
            }
        }

    };

    //var sale_chart;
    if (_updateflag === false) {
        corp_chart_sales = new ApexCharts(document.querySelector("#corp-chart-sales"), options);
        corp_chart_sales.render();

    } else {
        corp_chart_sales.updateOptions({
            series: _values,
            labels: _labels,

        });
    }

    //Hide loader
    $('.loading-mask').hide();

}

//corp-chart-sales End
//corp-chart-invoices Start

function corpChartInvoices(_labels, _values, _updateflag) {


    var options = {
        series: _values,
        labels: _labels,
        chart: {
            type: 'donut',
            height: 250//_labels.length > 1 ? 250 : 220
        },
        legend: {
            position: 'bottom'
        },
        fill: {
            type: 'gradient',
        },
        tooltip: {
            enabled: true,
            dataLabels: {
                enabled: true,
                title: {
                    formatter: function (_values) {
                        return _values
                    }
                },
            },
        },
        responsive: [{
            breakpoint: 480,
            options: {
                legend: {
                    position: 'bottom'
                }
            }
        }],
        yaxis: {
            show: true,
            decimalsInFloat: 2,
            labels: {
                formatter: function (value) {
                    var amount = parseInt(value);

                    return thousands_separators(amount);
                }
            }
        }

    };

    //var sale_chart;
    if (_updateflag === false) {
        corp_chart_invoices = new ApexCharts(document.querySelector("#corp-chart-invoices"), options);
        corp_chart_invoices.render();

    } else {
        corp_chart_invoices.updateOptions({
            series: _values,
            labels: _labels,

        });
    }

    //Hide loader
    $('.loading-mask').hide();

}

////corp-chart-invoices End
//corp-chart-refund Start

function corpChartRefund(_labels, _values, _updateflag) {
   console.log(_labels);

    var options = {
        series: _values,
        labels: _labels,
        chart: {
            type: 'donut',
            height: 250//_labels.length > 1 ? 250 : 220
        },
        legend: {
            position: 'bottom'
        },
        fill: {
            type: 'gradient',
        },
        tooltip: {
            enabled: true,
            dataLabels: {
                enabled: true,
                title: {
                    formatter: function (_values) {
                        return _values
                    }
                },
            },
        },
        responsive: [{
            breakpoint: 480,
            options: {
                legend: {
                    position: 'bottom'
                }
            }
        }],
        yaxis: {
            show: true,
            decimalsInFloat: 2,
            labels: {
                formatter: function (value) {
                    var amount = parseInt(value);

                    return thousands_separators(amount);
                }
            }
        }

    };

    //var sale_chart;
    if (_updateflag === false) {
        corp_chart_refund = new ApexCharts(document.querySelector("#corp-chart-refund"), options);
        corp_chart_refund.render();

    } else {
        corp_chart_refund.updateOptions({
            series: _values,
            labels: _labels,

        });
    }

    //Hide loader
    $('.loading-mask').hide();

}

//corp-chart-refund End
//corp-chart-netinvoices Start

function corpChartNetInvoices(_labels, _values, _updateflag) {


    var options = {
        series: _values,
        labels: _labels,
        chart: {
            type: 'donut',
            height: 250//_labels.length > 1 ? 250 : 220
        },
        legend: {
            position: 'bottom'
        },
        fill: {
            type: 'gradient',
        },
        tooltip: {
            enabled: true,
            dataLabels: {
                enabled: true,
                title: {
                    formatter: function (_values) {
                        return _values
                    }
                },
            },
        },
        responsive: [{
            breakpoint: 480,
            options: {
                legend: {
                    position: 'bottom'
                }
            }
        }],
        yaxis: {
            show: true,
            decimalsInFloat: 2,
            labels: {
                formatter: function (value) {
                    var amount = parseInt(value);

                    return thousands_separators(amount);
                }
            }
        }

    };

    //var sale_chart;
    if (_updateflag === false) {
        corp_chart_netinvoices = new ApexCharts(document.querySelector("#corp-chart-netinvoices"), options);
        corp_chart_netinvoices.render();

    } else {
        corp_chart_netinvoices.updateOptions({
            series: _values,
            labels: _labels,

        });
    }

    //Hide loader
    $('.loading-mask').hide();

}

////corp-chart-netinvoices End

//Chart top-ten-corporate-customers Start

function topTenCorporateCustomers(_companylabels, _netdelivery, _contribution, _updateflag) {

    //console.log(_contribution);

    var options = {
        series: [{
            name: 'Sales',
            type: 'column',
            data: _netdelivery
        }, {
            name: 'Contribution',
            type: 'column',
            data: _contribution
        }],
        chart: {
            height: 350,
            type: 'line',
            stacked: false
        },
        dataLabels: {
            enabled: false
        },
        stroke: {
            width: [1, 1, 4]
        },
        title: {

            align: 'left',
            offsetX: 110
        },
        xaxis: {
            labels: {
                show: true,
                rotate: -90,
            },
            categories: _companylabels
        },
        yaxis: [
            {
                axisTicks: {
                    show: true
                },
                axisBorder: {
                    show: true,
                    color: '#008FFB'
                },
                labels: {
                    style: {
                        colors: '#008FFB'
                    },
                    formatter: function (value) {
                        var amount = parseInt(value);

                        return thousands_separators(amount);
                    }
                },
                title: {
                    text: "Sales",
                    style: {
                        color: '#008FFB'
                    }
                },
                tooltip: {
                    enabled: true
                },
                show: true,
                decimalsInFloat: 2

            },
            {
                seriesName: 'Income',
                opposite: true,
                axisTicks: {
                    show: true
                },
                axisBorder: {
                    show: true,
                    color: '#00E396'
                },
                labels: {
                    style: {
                        colors: '#00E396'
                    },
                    formatter: function (value) {
                        var amount = value;

                        return amount + "%";
                    }
                },

            },
            {
                seriesName: 'Revenue',
                opposite: true,
                axisTicks: {
                    show: true
                },
                axisBorder: {
                    show: true,
                    color: '#FEB019'
                },
                labels: {
                    style: {
                        colors: '#FEB019'
                    },
                    formatter: function (value) {
                        var amount = parseInt(value);

                        //return thousands_separators(amount);
                        return value;
                    }
                },
                title: {
                    text: "Profit Margin %",
                    style: {
                        color: '#FEB019'
                    }
                },
                show: false,
                decimalsInFloat: 2
            }
        ],

        legend: {
            horizontalAlign: 'left',
            offsetX: 40
        }
    };


    if (_updateflag === true) {
        top_ten_corporate_customers.updateOptions({
            series: [{
                name: 'Sales',
                type: 'column',
                data: _netdelivery
            }, {
                name: 'Contribution',
                type: 'column',
                data: _contribution
            }],
            xaxis: {
                labels: {
                    show: true,
                    rotate: -90,
                },
                categories: _companylabels
            },

        });
    } else {
        top_ten_corporate_customers = new ApexCharts(document.querySelector("#top-ten-corporate-customers"), options);
        top_ten_corporate_customers.render();
    }

    //Hide loader
    $('.loading-mask').hide();

}

//Chart top-ten-corporate-customers End

//Chart corp-rm-chart Start
function corpRmChart(_labels, _val1, _val2, _val3, _val4, _updateflag) {

    var options = {
        series: [{
            name: 'Invoice',
            type: 'column',
            data: _val1
        }, {
            name: 'Refund',
            type: 'column',
            data: _val2
        }, {
            name: 'Net Delivery',
            type: 'column',
            data: _val3
        }, {
            name: 'Refund %',
            type: 'line',
            data: _val4
        }],
        chart: {
            height: 350,
            type: 'line',
            stacked: false
        },
        dataLabels: {
            enabled: false
        },
        stroke: {
            width: [1, 1, 4, 4]
        },
        title: {

            align: 'left',
            offsetX: 110
        },
        xaxis: {
            labels: {
                show: true,
                rotate: -90,
                style: {
                    colors: "black"
                }
            },
            categories: _labels
        },
        yaxis: [
            {
                axisTicks: {
                    show: true
                },
                axisBorder: {
                    show: true,
                    color: '#008FFB'
                },
                labels: {
                    style: {
                        colors: '#008FFB'
                    },
                    formatter: function (value) {
                        var amount = parseInt(value);

                        return thousands_separators(amount);
                    }
                },
                title: {
                    text: "Invoice",
                    style: {
                        color: '#008FFB'
                    }
                },
                tooltip: {
                    enabled: true
                },
                show: true,
                decimalsInFloat: 2

            },
            {
                seriesName: 'Income',
                opposite: true,
                axisTicks: {
                    show: true
                },
                axisBorder: {
                    show: true,
                    color: '#00E396'
                },
                labels: {
                    style: {
                        colors: '#00E396'
                    },
                    formatter: function (value) {
                        var amount = parseInt(value);

                        return thousands_separators(amount);
                    }
                },

            },
            {
                seriesName: 'Revenue',
                opposite: true,
                axisTicks: {
                    show: true
                },
                axisBorder: {
                    show: true,
                    color: '#FEB019'
                },
                labels: {
                    style: {
                        colors: '#FEB019'
                    },
                    formatter: function (value) {
                        var amount = parseInt(value);

                        return thousands_separators(amount);
                    }
                },
                title: {
                    text: "Profit Margin %",
                    style: {
                        color: '#FEB019'
                    }
                },
                show: false,
                decimalsInFloat: 2
            },
            {
                labels: {
                    style: {
                        colors: '#FF4560'
                    },
                    formatter: function (value) {
                        var amount = value;

                        return amount + "%";
                    }
                },
            }
        ],

        legend: {
            horizontalAlign: 'left',
            offsetX: 40
        }
    };


    if (_updateflag === true) {
        corp_rm_chart.updateOptions({
            series: [{
                name: 'Invoice',
                type: 'column',
                data: _val1
            }, {
                name: 'Refund',
                type: 'column',
                data: _val2
            }, {
                name: 'Net Delivery',
                type: 'column',
                data: _val3
            }, {
                name: 'Refund %',
                type: 'line',
                data: _val4
            }],
            xaxis: {
                labels: {
                    show: true,
                    rotate: -90,
                    style: {
                        colors: "black"
                    }
                },
                categories: _labels
            },

        });
    } else {
        corp_rm_chart = new ApexCharts(document.querySelector("#corp-rm-chart"), options);
        corp_rm_chart.render();
    }

    //Hide loader
    $('.loading-mask').hide();


}

//Chart corp-rm-chart End

//Chart  industry-invoice-margin Start

function industryInvoiceMargin(_labels, _val1, _val2, _val3, _val4, _val5, _val6, _updateflag) {
    var options = {
        series: [{
            name: 'Invoice',
            type: 'column',
            data: _val1
        }, {
            name: 'Refund',
            type: 'column',
            data: _val2
        }, {
            name: 'Net Delivery',
            type: 'column',
            data: _val3
        }, {
            name: 'Profit/Loss',
            type: 'column',
            data: _val5
        }, {
            name: 'Margin',
            type: 'line',
            data: _val4
        }, {
            name: 'Refund %',
            type: 'line',
            data: _val6
        }],
        chart: {
            height: 350,
            type: 'line',
            stacked: false
        },
        dataLabels: {
            enabled: false
        },
        stroke: {
            width: [4, 4],
            curve: 'smooth'
        },
        title: {
            align: 'left',
            offsetX: 110
        },
        xaxis: {
            labels: {
                show: true,
                rotate: -90,
                style: {
                    colors: "black"
                }
            },
            categories: _labels
        },
        yaxis: [
            {
                axisTicks: {
                    show: true
                },
                axisBorder: {
                    show: true,
                    color: '#008FFB'
                },
                labels: {
                    style: {
                        colors: '#008FFB'
                    },
                    formatter: function (value) {
                        var amount = parseInt(value);

                        return thousands_separators(amount);
                    }
                },
                title: {
                    style: {
                        color: '#008FFB'
                    }
                },
                tooltip: {
                    enabled: true
                },
                show: true,
                decimalsInFloat: 2

            },
            {
                seriesName: 'Income',
                opposite: true,
                axisTicks: {
                    show: true
                },
                axisBorder: {
                    show: true,
                    color: '#00E396'
                },
                labels: {
                    style: {
                        colors: '#00E396'
                    },
                    formatter: function (value) {
                        var amount = parseInt(value);

                        return thousands_separators(amount);
                    }
                },

            },
            {
                seriesName: 'Revenue',
                opposite: true,
                axisTicks: {
                    show: true
                },
                axisBorder: {
                    show: true,
                    color: '#FEB019'
                },
                labels: {
                    style: {
                        colors: '#FEB019'
                    },
                    formatter: function (value) {
                        var amount = parseInt(value);

                        return thousands_separators(amount);
                    }
                },
                title: {
                    text: "Profit Margin %",
                    style: {
                        color: '#FEB019'
                    }
                },
                show: false,
                decimalsInFloat: 2
            },
            {
                labels: {
                    style: {
                        colors: '#FF4560'
                    },
                    formatter: function (value) {
                        var amount = parseInt(value);

                        return thousands_separators(amount);
                    },
                    show: false
                },
            },
            {
                labels: {
                    style: {
                        colors: '#775DD0'
                    },
                    formatter: function (value) {
                        return value.toFixed(2) + " %"
                    },
                    show: false
                },
            },
            {
                labels: {
                    style: {
                        colors: '#008FFB'
                    },
                    formatter: function (value) {
                        return value.toFixed(2) + " %"
                    },
                    show: false,
                },
            }
        ],

        legend: {
            horizontalAlign: 'left',
            offsetX: 40,
        }
    };


    if (_updateflag === true) {
        corp_industry_chart.updateOptions({
            series: [{
                name: 'Invoice',
                type: 'column',
                data: _val1
            }, {
                name: 'Refund',
                type: 'column',
                data: _val2
            }, {
                name: 'Net Delivery',
                type: 'column',
                data: _val3
            }, {
                name: 'Profit/Loss',
                type: 'column',
                data: _val5
            }, {
                name: 'Margin',
                type: 'line',
                data: _val4
            }, {
                name: 'Refund %',
                type: 'line',
                data: _val6
            }],
            xaxis: {
                labels: {
                    show: true,
                    rotate: -90,
                    style: {
                        colors: "black"
                    }
                },
                categories: _labels
            },

        });
    } else {
        corp_industry_chart = new ApexCharts(document.querySelector("#industry-invoice-margin"), options);
        corp_industry_chart.render();
    }

    //Hide loader
    $('.loading-mask').hide();

}

//Chart  industry-invoice-margin End

//Chart  industry-sales-order Start

function industrySalesOrder(_labels, _values, _updateflag) {


    var options = {
        series: _values,
        labels: _labels,
        chart: {
            type: 'donut',
            height: 350
        },
        legend: {
            position: 'bottom',
            horizontalAlign: 'left',
            offsetX: 10,
         markers: {
          fillColors: colorIndustry,
      },
        },
        fill: {
            type: 'gradient',
        },
        tooltip: {
            enabled: true,
            y: {
                formatter: function (value) {

                    var amount = parseInt(value);

                    return thousands_separators(amount);
                }
            }
        },

        responsive: [{
            breakpoint: 480,
            options: {
                chart: {
                    width:'100%'
                },
                legend: {
                    position: 'bottom',
                    horizontalAlign: 'left',
                    offsetX: 10,
                }
            }
        }]
    };

    //var sale_chart;
    if (_updateflag === false) {
        industry_sales_order = new ApexCharts(document.querySelector("#industry-sales-order"), options);
        industry_sales_order.render();

    } else {
        industry_sales_order.updateOptions({
            series: _values,
            labels: _labels,

        });
    }

    //Hide loader
    $('.loading-mask').hide();

}

//Chart  industry-sales-order End

//Chart  order-analysis-rm-wise-order Start

function orderAnalysisRmWiseOrder(_labels, _val1, _val2, _updateflag) {

    var options = {
        series: [{
            name: 'Count Of Order',
            type: 'column',
            data: _val1
        }, {
            name: 'Ticket Size',
            type: 'line',
            data: _val2
        }],
        chart: {
            height: 350,
            type: 'line',
            stacked: false
        },
        dataLabels: {
            enabled: false
        },
        stroke: {
            width: [4, 4],
            curve: 'smooth'
        },
        xaxis: {
            labels: {
                show: true,
                rotate: -90,
                style: {
                    colors: "black"
                }
            },
            categories: _labels
        },
        yaxis: [
            {
                axisTicks: {
                    show: true
                },
                axisBorder: {
                    show: true,
                    color: '#008FFB'
                },
                labels: {
                    style: {
                        colors: '#008FFB'
                    },
                    formatter: function (value) {
                        var amount = parseInt(value);

                        return thousands_separators(amount);
                    }
                },
                title: {
                    text: "Count Of Order",
                    style: {
                        color: '#008FFB'
                    }
                },
                tooltip: {
                    enabled: true
                },
                show: true,
                decimalsInFloat: 2

            },
            {
                seriesName: 'Income',
                opposite: true,
                axisTicks: {
                    show: true
                },
                axisBorder: {
                    show: true,
                    color: '#00E396'
                },
                labels: {
                    style: {
                        colors: '#00E396'
                    },
                    formatter: function (value) {
                        var amount = parseInt(value);

                        return thousands_separators(amount);
                    }
                },

            },
            {
                seriesName: 'Revenue',
                opposite: true,
                axisTicks: {
                    show: true
                },
                axisBorder: {
                    show: true,
                    color: '#FEB019'
                },
                labels: {
                    style: {
                        colors: '#FEB019'
                    },
                    formatter: function (value) {
                        var amount = parseInt(value);

                        //return thousands_separators(amount);
                        return value;
                    }
                },

                show: false,
                decimalsInFloat: 2
            }
        ],
        tooltip: {
            enabled: true
        },
        legend: {
            horizontalAlign: 'left',
            offsetX: 40
        }
    };


    if (_updateflag === true) {
        order_analysis_rm_wise_order.updateOptions({
            series: [{
                name: 'Count Of Order',
                type: 'column',
                data: _val1
            }, {
                name: 'Ticket Size',
                type: 'line',
                data: _val2
            }],
            xaxis: {
                labels: {
                    show: true,
                    rotate: -90,
                    style: {
                        colors: "black"
                    }
                },
                categories: _labels
            },

        });
    } else {
        order_analysis_rm_wise_order = new ApexCharts(document.querySelector("#order-analysis-rm-wise-order"), options);
        order_analysis_rm_wise_order.render();
    }

    //Hide loader
    $('.loading-mask').hide();


}

// Chart new-pos-chart Start

function newPosChart(_labels, _val1, _val2, _val3, _updateflag) {


    var options = {
        series: [{
            name: 'Count Of Customers',
            type: 'column',
            data: _val1
        }, {
            name: 'Order Per Customer',
            type: 'line',
            data: _val2
        }, {
            name: 'AOV Per Customer',
            type: 'line',
            data: _val3
        }],
        chart: {
            height: 350,
            type: 'line',
            stacked: false
        },
        dataLabels: {
            enabled: false
        },
        stroke: {
            width: [4, 4, 4, 4],
            curve: 'smooth'
        },
        xaxis: {
            labels: {
                show: true,
                rotate: -90,
                style: {
                    colors: "black"
                }
            },
            categories: _labels
        },
        yaxis: [
            {
                axisTicks: {
                    show: true
                },
                axisBorder: {
                    show: true,
                    color: '#008FFB'
                },
                labels: {
                    style: {
                        colors: '#008FFB'
                    },
                    formatter: function (value) {
                        var amount = parseInt(value);

                        return thousands_separators(amount);
                    }
                },
                title: {
                    text: "Count Of Customers",
                    style: {
                        color: '#008FFB'
                    }
                },
                tooltip: {
                    enabled: true
                },
                show: true,
                decimalsInFloat: 2

            },
            {
                seriesName: 'Income',
                opposite: true,
                axisTicks: {
                    show: true
                },
                axisBorder: {
                    show: true,
                    color: '#00E396'
                },
                labels: {
                    style: {
                        colors: '#00E396'
                    },
                    formatter: function (value) {
                        var amount = parseInt(value);

                        return thousands_separators(amount);
                    }
                },

            },
            {
                seriesName: 'Revenue',
                opposite: true,
                axisTicks: {
                    show: true
                },
                axisBorder: {
                    show: true,
                    color: '#FEB019'
                },
                labels: {
                    style: {
                        colors: '#FEB019'
                    },
                    formatter: function (value) {
                        var amount = parseInt(value);

                        //return thousands_separators(amount);
                        return value;
                    }
                },

                show: false,
                decimalsInFloat: 2
            }
        ],
        tooltip: {
            enabled: true
        },
        legend: {
            horizontalAlign: 'left',
            offsetX: 40
        }
    };


    if (_updateflag === true) {
        new_pos_chart.updateOptions({
            series: [{
                name: 'Count Of Customers',
                type: 'column',
                data: _val1
            }, {
                name: 'Order Per Customer',
                type: 'line',
                data: _val2
            }, {
                name: 'AOV Per Customer',
                type: 'line',
                data: _val3
            }],
            xaxis: {
                labels: {
                    show: true,
                    rotate: -90,
                    style: {
                        colors: "black"
                    }
                },
                categories: _labels
            },

        });
    } else {
        new_pos_chart = new ApexCharts(document.querySelector("#new-pos-chart"), options);
        new_pos_chart.render();
    }

    //Hide loader
    $('.loading-mask').hide();


}


function targetVsAchievement(_labels, _val1, _val2, _val3, _val4, _val5, _updateflag) {
    var options = {
        series: [{
            name: 'Sales (Order)',
            type: 'column',
            data: _val1
        }, {
            name: 'Target',
            type: 'column',
            data: _val2
        }, {
            name: 'Net Delivery',
            type: 'column',
            data: _val3
        }, {
            name: 'Achievement',
            type: 'line',
            data: _val4
        }, {
            name: 'Profit Margin',
            type: 'line',
            data: _val5,
        }],
        chart: {
            height: 350,
            type: 'line',
            stacked: false
        },
        dataLabels: {
            enabled: false
        },
        stroke: {
            width: 4
        },

        xaxis: {
            labels: {
                show: true,
                rotate: -90,
                style: {
                    colors: "black"
                }
            },
            categories: _labels
        },
        yaxis: [
            {
                axisTicks: {
                    show: true
                },
                axisBorder: {
                    show: true,
                    color: '#008FFB'
                },
                labels: {
                    style: {
                        colors: '#008FFB'
                    },
                    formatter: function (value) {
                        var amount = value;

                        return thousands_separators(amount);
                    }
                },
                title: {
                    text: "Sales",
                    style: {
                        color: '#008FFB'
                    }
                },
                tooltip: {
                    enabled: true
                },
                show: true,
                decimalsInFloat: 2

            },
            {
                seriesName: 'Income',
                opposite: true,
                axisTicks: {
                    show: true
                },
                axisBorder: {
                    show: true,
                    color: '#00E396'
                },
                labels: {
                    style: {
                        colors: '#00E396'
                    },
                    formatter: function (value) {
                        var amount = parseInt(value);

                        return thousands_separators(amount);
                    }
                },

            },
            {
                seriesName: 'Revenue',
                opposite: true,
                axisTicks: {
                    show: true
                },
                axisBorder: {
                    show: true,
                    color: '#FEB019'
                },
                labels: {
                    style: {
                        colors: '#FEB019'
                    },
                    formatter: function (value) {
                        var amount = parseInt(value);

                        return thousands_separators(amount);
                    }
                },
                title: {
                    text: "Profit Margin %",
                    style: {
                        color: '#FEB019'
                    }
                },
                show: false,
                decimalsInFloat: 2
            }
        ],
        tooltip: {
            enabled: true
        },
        legend: {
            horizontalAlign: 'left',
            offsetX: 40
        }
    };


    if (_updateflag === true) {
        target_achievement_chart.updateOptions({
            series: [{
                name: 'Sales (Order)',
                type: 'column',
                data: _val1
            }, {
                name: 'Target',
                type: 'column',
                data: _val2
            }, {
                name: 'Net Delivery',
                type: 'column',
                data: _val3
            }, {
                name: 'Achievement',
                type: 'line',
                data: _val4
            }, {
                name: 'Profit Margin',
                type: 'line',
                data: _val5,
            }],
            xaxis: {
                labels: {
                    show: true,
                    rotate: -90,
                    style: {
                        colors: "black"
                    }
                },
                categories: _labels
            },

        });
    } else {
        //console.log("This is initialized");
        target_achievement_chart = new ApexCharts(document.querySelector("#target-vs-achievement"), options);
        target_achievement_chart.render();
    }

    //Hide loader
    $('.loading-mask').hide();

}


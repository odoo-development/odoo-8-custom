//----------- Processing Order Value Day color ---------

var colorPaletteDay = ['#009900', '#FFCC00', '#FF0000'];

//----------- Ananta,Corporate,Retail,General  ---------

var colorPaletteCf = ['#1e5985', '#00e396', '#feb019', '#ff4560'];

//----------- Grocery,Stationary,Electronics,Mobile Accessories,Others,SOHO  ---------

var colorCategory = ['#008ffb', '#00e396', '#feb019', '#ff4560', '#775dd0', '#800000'];

// Thousand Separators Start
var s = new openerp.init();
var _labels;
var _values;
var x;
var y;
var z;
var start_date = null;
var end_date = null;
var warehouse = null;
var _updateflag = false;


// Thousand Separators Start
function thousands_separators(num) {
    num = parseInt(num);
    var num_parts = num.toString().split(".");
    num_parts[0] = num_parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return num_parts.join(".");
}

function currentDate() {
   var d = new Date();
   var today_date = d.getDate()+" "+d.toLocaleString('default', { month: 'long' })+", "+d.getFullYear();
    return today_date;
}

// Date Format Start

function formatDate(dateObj) {
    //var dateObj = new Date();
    var today_date = dateObj.getDate();
    var today_month = dateObj.getMonth() + 1;
    var today_year = dateObj.getFullYear();

    var formated_today = today_year + "-" + today_month + "-" + today_date;

    return formated_today;
}
// Thousand Separators End
//Select Warehouse Selected Value
function getWarehouseSelectedValue() {
    var warehouse = document.getElementById("warehouse-select").value;
    document.getElementById('warehouse-name').innerHTML = getWarehouseSelectedName();
    return warehouse;
}

//Select Warehouse Selected Value
function getWarehouseSelectedName() {
    var warehouse_name = document.getElementById("warehouse-select");
    var warehouse_name_text= warehouse_name.options[warehouse_name.selectedIndex].text;
    //console.log(warehouse_name_text)

    return warehouse_name_text;
}


document.getElementById('current-date').innerHTML = currentDate();

 document.getElementById('warehouse-name').innerHTML = getWarehouseSelectedName();


$('#search-by-date').click(function (e) {
    e.preventDefault();

    var fromDate = $("#from-date").val();
    var toDate = $("#to-date").val();

    var warehouse = getWarehouseSelectedValue();
    //var optId = $(this).find('option:selected').attr('id')

  document.getElementById('warehouse-name').innerHTML = getWarehouseSelectedName();
    //  console.log(test);

    //
    // if(fromDate === '' || toDate === ''){
    //
    //     alert("Please give From Date and To Date both!");
    //
    // }else {
    //Show loader
    var x = fromDate  === '' ? formatDate(new Date()) : fromDate;
    var y = toDate === '' ? formatDate(new Date()) : toDate;
    var z = warehouse === '' ? 'all' : warehouse;
    _updateflag = true;

    new s.web.Model("warehouse.dashboard").call("warehouse_data_prepare",
    ([x, y, z])).then(function (results) {
        console.log(results)


    // _saleslabels = results[0];
    // _salescount = results[1];

    document.getElementById('count-of-order').innerHTML = thousands_separators(results[0]);
    document.getElementById('count-rfp').innerHTML = thousands_separators(results[1]);
    document.getElementById('count-waiting-availability').innerHTML = thousands_separators(results[2]);
    document.getElementById('corp-picking').innerHTML = thousands_separators(results[3]);
    document.getElementById('corp-invoice').innerHTML = thousands_separators(results[4]);
    document.getElementById('confirm-manifest').innerHTML = thousands_separators(results[5]);
    // document.getElementById('warehouse-name').innerHTML = thousands_separators(results[6]);


    var options = {
    series: [thousands_separators(results[1]), thousands_separators(results[2]), thousands_separators(results[3]), thousands_separators(results[4]),thousands_separators(results[5])],
    chart: {
        height: 350,
        type: 'radialBar',
    },
    plotOptions: {
        radialBar: {
            dataLabels: {
                name: {
                    fontSize: '22px',
                },
                value: {
                    fontSize: '16px',
                        formatter: function (val) {
          return val ;
        }
                },
                total: {
                    show: true,
                    label: 'Total Confirm Order',
                    formatter: function (w) {
                        // By default this function returns the average of all series. The below is just an example to show the use of custom formatter function
                        return thousands_separators(results[0]);
                    }
                }
            }
        }
    },
    stroke: {
        lineCap: "round",
    },
    labels: ['RFP', 'Waiting Availability', 'Picking', 'Invoice','Manifest'],
};

var chart = new ApexCharts(document.querySelector("#chart1"), options);
chart.render();
});


});




x = start_date === null ? formatDate(new Date()) : start_date;
y = end_date === null ? x : end_date;
z = warehouse === null ? 'all':warehouse;



new s.web.Model("warehouse.dashboard").call("warehouse_data_prepare",
    ([x, y, z])).then(function (results) {
        console.log(results)


    // _saleslabels = results[0];
    // _salescount = results[1];

    document.getElementById('count-of-order').innerHTML = thousands_separators(results[0]);
    document.getElementById('count-rfp').innerHTML = thousands_separators(results[1]);
    document.getElementById('count-waiting-availability').innerHTML = thousands_separators(results[2]);
    document.getElementById('corp-picking').innerHTML = thousands_separators(results[3]);
    document.getElementById('corp-invoice').innerHTML = thousands_separators(results[4]);
    document.getElementById('confirm-manifest').innerHTML = thousands_separators(results[5]);
    // document.getElementById('warehouse-name').innerHTML = thousands_separators(results[6]);


    var options = {
    series: [thousands_separators(results[1]), thousands_separators(results[2]), thousands_separators(results[3]), thousands_separators(results[4]),thousands_separators(results[5])],
    chart: {
        height: 350,
        type: 'radialBar',
    },
    plotOptions: {
        radialBar: {
            dataLabels: {
                name: {
                    fontSize: '22px',
                },
                value: {
                    fontSize: '16px',
                        formatter: function (val) {
          return val ;
        }
                },
                total: {
                    show: true,
                    label: 'Total Confirm Order',
                    formatter: function (w) {
                        // By default this function returns the average of all series. The below is just an example to show the use of custom formatter function
                        return thousands_separators(results[0]);
                    }
                }
            }
        }
    },
    stroke: {
        lineCap: "round",
    },
    labels: ['RFP', 'Waiting Availability', 'Picking', 'Invoice','Manifest'],
};

var chart = new ApexCharts(document.querySelector("#chart1"), options);
chart.render();
});



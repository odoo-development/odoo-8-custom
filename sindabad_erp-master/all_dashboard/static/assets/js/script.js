//console.log("chart update");

// Add active class to the current button (highlight it)
var dayWeekMonth = document.getElementById("twm");
var btns = dayWeekMonth.getElementsByClassName("btn");
for (var i = 0; i < btns.length; i++) {
  btns[i].addEventListener("click", function() {
  var current = document.getElementsByClassName("active");
  current[0].className = current[0].className.replace(" active", "");
  this.className += " active";
  });
}

var valuCount = document.getElementById("valucount");
var btns = valuCount.getElementsByClassName("btn");
for (var i = 0; i < btns.length; i++) {
  btns[i].addEventListener("click", function() {
  var current = document.getElementsByClassName("active");
  current[0].className = current[0].className.replace(" active", "");
  this.className += " active";
  });
}


var allCalcification = document.getElementById("allcalcification");
var btns = allCalcification.getElementsByClassName("btn");
for (var i = 0; i < btns.length; i++) {
  btns[i].addEventListener("click", function() {
  var current = document.getElementsByClassName("active");
  current[0].className = current[0].className.replace(" active", "");
  this.className += " active";
  });
}
// ----------------------------------------------------------
// ----------------------------------------------------------


var s = new openerp.init();
var _labels;
var _values;
var x;
var y;
var z;
var start_date = null;
var end_date = null;
var classification = null;
var _updateflag = false;

//-----------charts-----------
var sale_chart;
var sale_inv_chart;
var sale_refund_chart;
var sale_net_inv_chart;
var mom_sale_chart;
var mom_sale_inv_chart;
var mom_sale_refund_chart;
var mom_sale_net_inv_chart;
var pro_sale_count_chart;
var categ_sale_chart;
var sale_margin_chart;
var sale_margin_chart_line_column;
var turn_over_time_chart;
var pro_order_value_chart;
var order_trend30days_chart;
var sale_chart_netdelivery;
//----------- Processing Order Value Day color ---------

  var colorPaletteDay = ['#009900','#FFCC00',  '#FF0000'];

//----------- Ananta,Corporate,Retail,General  ---------

  var colorPaletteCf = ['#1e5985','#00e396',  '#feb019', 'ff4560'];

//Navbar Brand Link Page Refresh
function confirmRefresh() {
    //setTimeout("location.reload(true);",100);
    window.location.reload(true);
}

  // Current Date

function currentDate() {
   var d = new Date();
   var today_date = d.getDate()+" "+d.toLocaleString('default', { month: 'long' })+", "+d.getFullYear();
    return today_date;
}

document.getElementById('current-date').innerHTML = currentDate();

//-----------charts-----------

function formatDate(dateObj) {
    //var dateObj = new Date();
    var today_date = dateObj.getDate();
    var today_month = dateObj.getMonth() + 1;
    var today_year = dateObj.getFullYear();

    var formated_today = today_year + "-" + today_month + "-" + today_date;

    return formated_today;
}

function thousands_separators(num)
{
    num = parseInt(num);
    var num_parts = num.toString().split(".");
    num_parts[0] = num_parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return num_parts.join(".");
}

function format_category(category){
    var res = category.split("-");
    var len = res.length;
    return res[len - 1].trim();
}

function format_value_million(num){
    var total =[];
     if(num > 1000000){
    total = parseInt(num / 1000000) + "M";
     }else {
    total = parseInt(num);
      }
    return total;
}



// Function Customer Group Index

function arrayFormatter(val){

    var formattedArray = [];

    for(i = 0; i < val.length; i++) {
        formattedArray.push(val[i].toFixed(2));
    }

    return formattedArray;

}

function handleUndefined(){

}

function customerGroupIndex(cgLabels, cgValue, cgTag) {
    // cgValue
    // ["Ananta", "Corporate", "Retail","General"];

    var sorted_cg_label = [];
    var sorted_cg_value = [];
    if(cgLabels.length > 0 && cgValue.length > 0){
        cgLabels = Object.values(cgLabels);
        cgValue = Object.values(cgValue);

        if(cgTag === "sales" ){

            //console.log(cgLabels);
            //console.log(cgValue);
            //cgValue[2] ? parseInt(cgValue[2]) : 0;

            for(i = 0; i < cgLabels.length; i++) {
                if(cgLabels[i] === 'Ananta'){
                    sorted_cg_label[0] = 'Ananta';
                    sorted_cg_value[0] = cgValue[i];
                }else if(cgLabels[i] === 'Corporate'){
                    sorted_cg_label[1] = 'Corporate';
                    sorted_cg_value[1] = cgValue[i];
                }else if(cgLabels[i] === 'Retail'){
                    sorted_cg_label[2] = 'Retail';
                    sorted_cg_value[2] = cgValue[i];
                }else if(cgLabels[i] === 'General'){
                    sorted_cg_label[3] = 'General';
                    sorted_cg_value[3] = cgValue[i];
                }
            }

            if(sorted_cg_label.indexOf("Ananta") === -1){
                sorted_cg_label[0] = 'Ananta';
                sorted_cg_value[0] = 0;
            }
            if(sorted_cg_label.indexOf("Corporate") === -1){
                sorted_cg_label[1] = 'Corporate';
                sorted_cg_value[1] = 0;
            }if(sorted_cg_label.indexOf("Retail") === -1){
                sorted_cg_label[2] = 'Retail';
                sorted_cg_value[2] = 0;
            }
            if(sorted_cg_label.indexOf("General") === -1){
                sorted_cg_label[3] = 'General';
                sorted_cg_value[3] = 0;
            }

            /*
            sorted_cg_value.push(cgValue[2] ? parseInt(cgValue[2]) : 0);
            sorted_cg_value.push(cgValue[1] ? parseInt(cgValue[1]) : 0);
            sorted_cg_value.push(cgValue[0] ? parseInt(cgValue[0]) : 0);
            sorted_cg_value.push(cgValue[3] ? parseInt(cgValue[3]) : 0);

            sorted_cg_label.push(cgLabels[2] ? cgLabels[2] : 'Ananta');
            sorted_cg_label.push(cgLabels[1] ? cgLabels[1] : 'Corporate');
            sorted_cg_label.push(cgLabels[0] ? cgLabels[0] : 'Retail');
            sorted_cg_label.push(cgLabels[3] ? cgLabels[3] : 'General');
            */


        }else if(cgTag === "invoice"){

            for(i = 0; i < cgLabels.length; i++) {
                if(cgLabels[i] === 'Ananta'){
                    sorted_cg_label[0] = 'Ananta';
                    sorted_cg_value[0] = cgValue[i];
                }else if(cgLabels[i] === 'Corporate'){
                    sorted_cg_label[1] = 'Corporate';
                    sorted_cg_value[1] = cgValue[i];
                }else if(cgLabels[i] === 'Retail'){
                    sorted_cg_label[2] = 'Retail';
                    sorted_cg_value[2] = cgValue[i];
                }else if(cgLabels[i] === 'General'){
                    sorted_cg_label[3] = 'General';
                    sorted_cg_value[3] = cgValue[i];
                }
            }

            if(sorted_cg_label.indexOf("Ananta") === -1){
                sorted_cg_label[0] = 'Ananta';
                sorted_cg_value[0] = 0;
            }
            if(sorted_cg_label.indexOf("Corporate") === -1){
                sorted_cg_label[1] = 'Corporate';
                sorted_cg_value[1] = 0;
            }
            if(sorted_cg_label.indexOf("Retail") === -1){
                sorted_cg_label[2] = 'Retail';
                sorted_cg_value[2] = 0;
            }
            if(sorted_cg_label.indexOf("General") === -1){
                sorted_cg_label[3] = 'General';
                sorted_cg_value[3] = 0;
            }

            /*
            sorted_cg_value.push(parseInt(cgValue[3]));
            sorted_cg_value.push(parseInt(cgValue[2]));
            sorted_cg_value.push(parseInt(cgValue[1]));
            sorted_cg_value.push(parseInt(cgValue[0]));

            sorted_cg_label.push(cgLabels[3]);
            sorted_cg_label.push(cgLabels[2]);
            sorted_cg_label.push(cgLabels[1]);
            sorted_cg_label.push(cgLabels[0]);
            */

        }else if(cgTag === "refund"){

            //console.log(cgLabels);
            //console.log(cgValue);

            for(i = 0; i < cgLabels.length; i++) {
                if(cgLabels[i] === 'Ananta'){
                    sorted_cg_label[0] = 'Ananta';
                    sorted_cg_value[0] = cgValue[i];
                }else if(cgLabels[i] === 'Corporate'){
                    sorted_cg_label[1] = 'Corporate';
                    sorted_cg_value[1] = cgValue[i];
                }else if(cgLabels[i] === 'Retail'){
                    sorted_cg_label[2] = 'Retail';
                    sorted_cg_value[2] = cgValue[i];
                }else if(cgLabels[i] === 'General'){
                    sorted_cg_label[3] = 'General';
                    sorted_cg_value[3] = cgValue[i];
                }
            }

            if(sorted_cg_label.indexOf("Ananta") === -1){
                sorted_cg_label[0] = 'Ananta';
                sorted_cg_value[0] = 0;
            }
            if(sorted_cg_label.indexOf("Corporate") === -1){
                sorted_cg_label[1] = 'Corporate';
                sorted_cg_value[1] = 0;
            }
            if(sorted_cg_label.indexOf("Retail") === -1){
                sorted_cg_label[2] = 'Retail';
                sorted_cg_value[2] = 0;
            }
            if(sorted_cg_label.indexOf("General") === -1){
                sorted_cg_label[3] = 'General';
                sorted_cg_value[3] = 0;
            }

            //console.log(sorted_cg_label);
            //console.log(sorted_cg_value);

            /*
            sorted_cg_value.push(parseInt(cgValue[3]));
            sorted_cg_value.push(parseInt(cgValue[2]));
            sorted_cg_value.push(parseInt(cgValue[1]));
            sorted_cg_value.push(parseInt(cgValue[0]));

            sorted_cg_label.push(cgLabels[3]);
            sorted_cg_label.push(cgLabels[2]);
            sorted_cg_label.push(cgLabels[1]);
            sorted_cg_label.push(cgLabels[0]);
            */

        }else if(cgTag === "delivery"){

            //console.log(cgLabels);
            //console.log(cgValue);

            for(i = 0; i < cgLabels.length; i++) {
                if(cgLabels[i] === 'Ananta'){
                    sorted_cg_label[0] = 'Ananta';
                    sorted_cg_value[0] = cgValue[i];
                }else if(cgLabels[i] === 'Corporate'){
                    sorted_cg_label[1] = 'Corporate';
                    sorted_cg_value[1] = cgValue[i];
                }else if(cgLabels[i] === 'Retail'){
                    sorted_cg_label[2] = 'Retail';
                    sorted_cg_value[2] = cgValue[i];
                }else if(cgLabels[i] === 'General'){
                    sorted_cg_label[3] = 'General';
                    sorted_cg_value[3] = cgValue[i];
                }
            }

            if(sorted_cg_label.indexOf("Ananta") === -1){
                sorted_cg_label[0] = 'Ananta';
                sorted_cg_value[0] = 0;
            }
            if(sorted_cg_label.indexOf("Corporate") === -1){
                sorted_cg_label[1] = 'Corporate';
                sorted_cg_value[1] = 0;
            }
            if(sorted_cg_label.indexOf("Retail") === -1){
                sorted_cg_label[2] = 'Retail';
                sorted_cg_value[2] = 0;
            }
            if(sorted_cg_label.indexOf("General") === -1){
                sorted_cg_label[3] = 'General';
                sorted_cg_value[3] = 0;
            }

            //console.log(sorted_cg_label);
            //console.log(sorted_cg_value);

            /*
            sorted_cg_value.push(parseInt(cgValue[3]));
            sorted_cg_value.push(parseInt(cgValue[2]));
            sorted_cg_value.push(parseInt(cgValue[1]));
            sorted_cg_value.push(parseInt(cgValue[0]));

            sorted_cg_label.push(cgLabels[3]);
            sorted_cg_label.push(cgLabels[2]);
            sorted_cg_label.push(cgLabels[1]);
            sorted_cg_label.push(cgLabels[0]);
            */

        }
    }
    else {
        sorted_cg_label = cgLabels;
        sorted_cg_value = cgValue;

    }

    //console.log(sorted_cg_label);
    //console.log(sorted_cg_value);

    return [sorted_cg_label, sorted_cg_value]
}


//order-trend30days

function orderTrend30Days(_labels, _val1, _val2, _updateflag) {

    var options = {
        series: [{
            name: 'Order Value',
            type: 'column',
            data: _val1
        }, {
            name: 'Order Count',
            type: 'line',
            data: _val2
        }],
        chart: {
            height: 350,
            type: 'line',
            stacked: false
        },
        dataLabels: {
            enabled: false
        },
        stroke: {
            width: [5, 5],
            curve: 'smooth'
        },
        xaxis: {
            labels: {
                show: true,
                rotate: -45,
                style: {
                    colors: "black"
                }
            },
            categories: _labels
        },
        yaxis: [
            {
                axisTicks: {
                    show: true
                },
                axisBorder: {
                    show: true,
                    color: '#008FFB'
                },
                labels: {
                    style: {
                        colors: '#008FFB'
                    },
                    formatter: function (value) {
                        var amount = parseInt(value);

                        return thousands_separators(amount);
                    }
                },
                title: {
                    text: "Sales",
                    style: {
                        color: '#008FFB'
                    }
                },
                tooltip: {
                    enabled: true
                },
                show: true,
                decimalsInFloat: 2

            },
            {
                seriesName: 'Income',
                opposite: true,
                axisTicks: {
                    show: true
                },
                axisBorder: {
                    show: true,
                    color: '#00E396'
                },
                labels: {
                    style: {
                        colors: '#00E396'
                    },
                    formatter: function (value) {
                        var amount = parseInt(value);

                        return thousands_separators(amount);
                    }
                },

            },
            {
                seriesName: 'Revenue',
                opposite: true,
                axisTicks: {
                    show: true
                },
                axisBorder: {
                    show: true,
                    color: '#FEB019'
                },
                labels: {
                    style: {
                        colors: '#FEB019'
                    },
                    formatter: function (value) {
                        var amount = parseInt(value);

                        //return thousands_separators(amount);
                        return value;
                    }
                },

                show: false,
                decimalsInFloat: 2
            }
        ],
        tooltip: {
            enabled: true
        },
        legend: {
            horizontalAlign: 'left',
            offsetX: 40
        }
    };


    if (_updateflag === true) {
        order_trend30days_chart.updateOptions({
            series: [{
                name: 'Order Value',
                type: 'column',
                data: _val1
            }, {
                name: 'Order Count',
                type: 'line',
                data: _val2
            }],
            xaxis: {
                labels: {
                    show: true,
                    rotate: -90,
                    style: {
                        colors: "black"
                    }
                },
                categories: _labels
            },

        });
    } else {
        order_trend30days_chart = new ApexCharts(document.querySelector("#order-trend30days"), options);
        order_trend30days_chart.render();
    }

    //Hide loader
    $('.loading-mask').hide();

}


function salesToday() {

    //Show loader
    $('.loading-mask').show();

    var x = formatDate(new Date());
    var y = formatDate(new Date());
    var z = 'all';
    _updateflag = true;

    new s.web.Model("sales.cogs.margin").call("today_to_monthly_sales_trending",
        ([x, y, z])).then(function (results) {

        //console.log(results);

        _labels = results[2];
        _values = results[3];

        document.getElementById('total-sales').innerHTML = thousands_separators(results[0]);

        document.getElementById('total-sales-count').innerHTML = results[1];
        generatesalescollectionoverviewbarchart(_labels, _values, _updateflag);

        document.getElementById('total-amount').innerHTML = thousands_separators(results[4]);
        document.getElementById('invoice-count').innerHTML = results[5];
        drawChartInvoice(results[6], results[7], _updateflag);

        document.getElementById('against-invoice').innerHTML = thousands_separators(results[8]);
        document.getElementById('refund-invoice-count').innerHTML = results[9];
        drawRefundInvoice(results[10], results[11], _updateflag);

        document.getElementById('net-invoice-amount').innerHTML = thousands_separators(results[12]);
        // document.getElementById('net-invoice-margin').innerHTML = results[13];
        drawNetInvoices(results[14], results[15], _updateflag);



    });
            new s.web.Model("sales.cogs.margin").call("today_to_monthly_delivery",

                ([x, y, z])).then(function (results) {

    document.getElementById('net-delivery-amount').innerHTML = thousands_separators(results[0]);
    document.getElementById('net-delivery-count').innerHTML = results[1];
    generatedeliveryviewbarchart(results[2], results[3], _updateflag);
});
}

function salesWeekly() {

    //Show loader
    $('.loading-mask').show();

    var ourDate = new Date();
    ourDate.setDate(ourDate.getDate() - 7);

    var x = formatDate(new Date());
    var y = formatDate(ourDate);
    var z = 'all';
    _updateflag = true;

    new s.web.Model("sales.cogs.margin").call("today_to_monthly_sales_trending",
        ([x, y, z])).then(function (results) {

        //console.log(results);

        _labels = results[2];
        _values = results[3];

        document.getElementById('total-sales').innerHTML = thousands_separators(results[0]);
        document.getElementById('total-sales-count').innerHTML = results[1];

        generatesalescollectionoverviewbarchart(_labels, _values, _updateflag);

        document.getElementById('total-amount').innerHTML = thousands_separators(results[4]);
        document.getElementById('invoice-count').innerHTML = results[5];
        drawChartInvoice(results[6], results[7], _updateflag);

        document.getElementById('against-invoice').innerHTML = thousands_separators(results[8]);
        document.getElementById('refund-invoice-count').innerHTML = results[9];
        drawRefundInvoice(results[10], results[11], _updateflag);

        document.getElementById('net-invoice-amount').innerHTML = thousands_separators(results[12]);
        // document.getElementById('net-invoice-margin').innerHTML = results[13];
        drawNetInvoices(results[14], results[15], _updateflag);




    });

    new s.web.Model("sales.cogs.margin").call("today_to_monthly_delivery",
    ([x, y, z])).then(function (results) {

    document.getElementById('net-delivery-amount').innerHTML = thousands_separators(results[0]);
    document.getElementById('net-delivery-count').innerHTML = results[1];
    generatedeliveryviewbarchart(results[2], results[3], _updateflag);
});
}

function salesMonthly() {

    //Show loader
    $('.loading-mask').show();

    var ourDate = new Date();
    ourDate.setDate(ourDate.getDate() - 30);
    var my_date = new Date();
    var first_date = new Date(my_date.getFullYear(), my_date.getMonth(), 1);



    var x = formatDate(new Date());
    var y = formatDate(first_date);
    var z = 'all';

    _updateflag = true;

    new s.web.Model("sales.cogs.margin").call("today_to_monthly_sales_trending",
        ([x, y, z])).then(function (results) {

        //console.log(results);

        _labels = results[2];
        _values = results[3];

        document.getElementById('total-sales').innerHTML = thousands_separators(results[0]);
        document.getElementById('total-sales-count').innerHTML = results[1];
        generatesalescollectionoverviewbarchart(_labels, _values, _updateflag);

        document.getElementById('total-amount').innerHTML = thousands_separators(results[4]);
        document.getElementById('invoice-count').innerHTML = results[5];
        drawChartInvoice(results[6], results[7], _updateflag);

        document.getElementById('against-invoice').innerHTML = thousands_separators(results[8]);
        document.getElementById('refund-invoice-count').innerHTML = results[9];
        drawRefundInvoice(results[10], results[11], _updateflag);

        document.getElementById('net-invoice-amount').innerHTML = thousands_separators(results[12]);
        // document.getElementById('net-invoice-margin').innerHTML = results[13];
        drawNetInvoices(results[14], results[15], _updateflag);



    });

            new s.web.Model("sales.cogs.margin").call("today_to_monthly_delivery",
    ([x, y, z])).then(function (results) {

    document.getElementById('net-delivery-amount').innerHTML = thousands_separators(results[0]);
    document.getElementById('net-delivery-count').innerHTML = results[1];
    generatedeliveryviewbarchart(results[2], results[3], _updateflag);
});
}

function processingAll() {

    //Show loader
    $('.loading-mask').show();

    //console.log('processingAll');

    var x = formatDate(new Date());
    var y = formatDate(new Date());
    var z = 'processingAll';
    _updateflag = true;

    new s.web.Model("sales.cogs.margin").call("processing_all",
        ([x, y, z])).then(function (results) {

        //console.log(results);

        //_labels = results[2];
        //_values = results[3];

        //document.getElementById('total-sales').innerHTML = results[0];
        //document.getElementById('total-sales-count').innerHTML = results[1];
        //generatesalescollectionoverviewbarchart(_labels, _values, _updateflag);

        //document.getElementById('total-amount').innerHTML = results[4];
        //document.getElementById('invoice-count').innerHTML = results[5];
        //drawChartInvoice(results[6], results[7], _updateflag);

        //document.getElementById('against-invoice').innerHTML = results[8];
        //document.getElementById('refund-invoice-count').innerHTML = results[9];
        //drawRefundInvoice(results[10], results[11], _updateflag);

        //document.getElementById('net-invoice-amount').innerHTML = results[12];
        // document.getElementById('net-invoice-margin').innerHTML = results[13];
        //drawNetInvoices(results[14], results[15], _updateflag);
        //drawCategoriwiseSales(results[16], results[17], results[18], results[19], _updateflag);
        //drawSalesMarginDonut(results[22], results[23], _updateflag);
        //drawTurnOverTimeStacked(results[24], results[25], results[26], _updateflag);

        // month on month
        //drawMonthOnMonthSales(results[27], results[28], _updateflag);

        //drawMonthOnMonthInvoice(results[27], results[29], _updateflag);

        //drawMonthOnMonthRefund(results[27], results[30], _updateflag);

        //drawMonthOnMonthNetInvoice(results[27], results[31], _updateflag);


        // Processing
        drawProcessingOrderCountChart(results[0], results[1], _updateflag);

        drawProcessingOrderValueChart(results[0], results[2], _updateflag);



    });
}

function processingAnanta() {
    //Show loader
    $('.loading-mask').show();

    //console.log('processingAnanta');
    var x = formatDate(new Date());
    var y = formatDate(new Date());
    var z = 'processingAnanta';
    _updateflag = true;

    new s.web.Model("sales.cogs.margin").call("processing_all",
        ([x, y, z])).then(function (results) {

        //console.log(results);

        // Processing
        drawProcessingOrderCountChart(results[0], results[1], _updateflag);

        drawProcessingOrderValueChart(results[0], results[2], _updateflag);


    });

}

function processingCorp() {

    //Show loader
    $('.loading-mask').show();

    //console.log('processingCorp');

    var x = formatDate(new Date());
    var y = formatDate(new Date());
    var z = 'processingCorp';
    _updateflag = true;

    new s.web.Model("sales.cogs.margin").call("processing_all",
        ([x, y, z])).then(function (results) {

        //console.log(results);

        _labels = results[2];
        _values = results[3];

        // Processing
        drawProcessingOrderCountChart(results[0], results[1], _updateflag);

        drawProcessingOrderValueChart(results[0], results[2], _updateflag);


    });

}

function processingRetail() {

    //Show loader
    $('.loading-mask').show();

    //console.log('processingRetail');

    var x = formatDate(new Date());
    var y = formatDate(new Date());
    var z = 'processingRetail';
    _updateflag = true;

    new s.web.Model("sales.cogs.margin").call("processing_all",
        ([x, y, z])).then(function (results) {

        //console.log(results);

        // Processing
        drawProcessingOrderCountChart(results[0], results[1], _updateflag);

        drawProcessingOrderValueChart(results[0], results[2], _updateflag);


    });

}

function processingOnline() {

    //Show loader
    $('.loading-mask').show();

    //console.log('processingOnline');

    var x = formatDate(new Date());
    var y = formatDate(new Date());
    var z = 'processingOnline';
    _updateflag = true;

    new s.web.Model("sales.cogs.margin").call("processing_all",
        ([x, y, z])).then(function (results) {

        //console.log(results);

        // Processing
        drawProcessingOrderCountChart(results[0], results[1], _updateflag);

        drawProcessingOrderValueChart(results[0], results[2], _updateflag);

    });

}

function momValue() {

    //Show loader
    $('.loading-mask').show();

    var x = formatDate(new Date());
    var y = formatDate(new Date());
    var z = 'monvalue';
    _updateflag = true;

    new s.web.Model("sales.cogs.margin").call("monthon_month_value",
        ([x, y, z])).then(function (results) {

         console.log(results);
        // month on month
        drawMonthOnMonthSales(results[0], results[1], _updateflag);


        drawMonthOnMonthInvoice(results[0], results[2], _updateflag);

        drawMonthOnMonthRefund(results[0], results[3], _updateflag);

        drawMonthOnMonthNetInvoice(results[0], results[4], _updateflag);




    });

}

function momCount() {

    //Show loader
    $('.loading-mask').show();

    var x = formatDate(new Date());
    var y = formatDate(new Date());
    var z = 'moncount';
    _updateflag = true;

    new s.web.Model("sales.cogs.margin").call("monthon_month_count",
        ([x, y, z])).then(function (results) {

        // month on month
        drawMonthOnMonthSales(results[0], results[1], _updateflag);


        drawMonthOnMonthInvoice(results[0], results[2], _updateflag);

        drawMonthOnMonthRefund(results[0], results[3], _updateflag);

        drawMonthOnMonthNetInvoice(results[0], results[4], _updateflag);



    });
}

$('#search-by-date').click(function(e){
    e.preventDefault();

    //Show loader search
    //$('.loading-mask').show();

    var fromDate = $("#from-date").val();
    var toDate = $("#to-date").val();

    //console.log(fromDate);
    //console.log(toDate);

    if(fromDate === '' || toDate === ''){

        alert("Please give From Date and To Date both!");

    }else {
        //Show loader
        //$('.loading-mask').show();
        var x = toDate;
        var y = fromDate;
        var z = classification === null ? 'all' : classification;
        _updateflag = true;

      /*  new s.web.Model("sales.cogs.margin").call("mgt_dashboard_data_preparation",
        ([x, y, z])).then(function (results) {

        //console.log(results);

        _labels = results[2];
        _values = results[3];

        document.getElementById('total-sales').innerHTML = thousands_separators(results[0]);
        document.getElementById('total-sales-count').innerHTML = results[1];
        generatesalescollectionoverviewbarchart(_labels, _values, _updateflag);

        document.getElementById('total-amount').innerHTML = thousands_separators(results[4]);
        document.getElementById('invoice-count').innerHTML = results[5];
        drawChartInvoice(results[6], results[7], _updateflag);

        document.getElementById('against-invoice').innerHTML = thousands_separators(results[8]);
        document.getElementById('refund-invoice-count').innerHTML = results[9];
        drawRefundInvoice(results[10], results[11], _updateflag);

        document.getElementById('net-invoice-amount').innerHTML = thousands_separators(results[12]);
        // document.getElementById('net-invoice-margin').innerHTML = results[13];
        drawNetInvoices(results[14], results[15], _updateflag);

         drawCategoriwiseSales(results[16], results[17], results[18], results[19], _updateflag);

         document.getElementById('total-margin-amount').innerHTML = thousands_separators(results[35]);
         document.getElementById('total-margin-amount-percent').innerHTML = results[36];
        //drawSalesMarginDonut(results[22], results[23], _updateflag);

           // new net-invoice-and-margin-classification-chart-two
             drawSalesMarginDonutNew(results[38], results[39],results[37], _updateflag);


        drawTurnOverTimeStacked(results[24], results[25], results[26], _updateflag);

        // month on month
        drawMonthOnMonthSales(results[27], results[28], _updateflag);

        drawMonthOnMonthInvoice(results[27], results[29], _updateflag);

        drawMonthOnMonthRefund(results[27], results[30], _updateflag);

        drawMonthOnMonthNetInvoice(results[27], results[31], _updateflag);


        // Processing
        drawProcessingOrderCountChart(results[32], results[33], _updateflag);

        drawProcessingOrderValueChart(results[32], results[34], _updateflag);

        //hide loader
        //$('.loading-mask').hide();

    });*/

        // All Char Functions
        // Sales Invoices Refund Net Invoices Block
        new s.web.Model("sales.cogs.margin").call("today_to_monthly_sales_trending",
            ([x, y, z])).then(function (results) {

            document.getElementById('total-sales').innerHTML = thousands_separators(results[0]);
            document.getElementById('total-sales-count').innerHTML = results[1];
            generatesalescollectionoverviewbarchart(results[2], results[3], _updateflag);

            document.getElementById('total-amount').innerHTML = thousands_separators(results[4]);
            document.getElementById('invoice-count').innerHTML = results[5];
            drawChartInvoice(results[6], results[7], _updateflag);

            document.getElementById('against-invoice').innerHTML = thousands_separators(parseInt(results[8]));
            document.getElementById('refund-invoice-count').innerHTML = results[9];
            drawRefundInvoice(results[10], results[11], _updateflag);

            document.getElementById('net-invoice-amount').innerHTML = thousands_separators(results[12]);
            // document.getElementById('net-invoice-margin').innerHTML = results[13];
            drawNetInvoices(results[14], results[15], _updateflag);

        });

        new s.web.Model("sales.cogs.margin").call("today_to_monthly_delivery",
    ([x, y, z])).then(function (results) {

    document.getElementById('net-delivery-amount').innerHTML = thousands_separators(results[0]);
    document.getElementById('net-delivery-count').innerHTML = results[1];
    generatedeliveryviewbarchart(results[2], results[3], _updateflag);
});

    }

});

//full page Show loader


x = start_date === null ? formatDate(new Date()) : start_date;
y = end_date === null ? x : end_date;
z = classification === null ? 'all' : classification;




var instance=openerp;
instance.session.rpc("/web/session/get_session_info", {}).then(function(result) {

    if (result.uid){
        // $('.loading-mask').show();

    }else {
        alert("Your Odoo Session Expired. Please Re Login into the System");
        location.replace("http://odoo.sindabad.com/web/login");
    }
});

// Sales Invoices Refund Net Invoices Block
new s.web.Model("sales.cogs.margin").call("today_to_monthly_sales_trending",
    ([x, y, z])).then(function (results) {

    document.getElementById('total-sales').innerHTML = thousands_separators(results[0]);
    document.getElementById('total-sales-count').innerHTML = results[1];
    generatesalescollectionoverviewbarchart(results[2], results[3], _updateflag);

    document.getElementById('total-amount').innerHTML = thousands_separators(results[4]);
    document.getElementById('invoice-count').innerHTML = results[5];
    drawChartInvoice(results[6], results[7], _updateflag);

    document.getElementById('against-invoice').innerHTML = thousands_separators(parseInt(results[8]));
    document.getElementById('refund-invoice-count').innerHTML = results[9];
    drawRefundInvoice(results[10], results[11], _updateflag);

    document.getElementById('net-invoice-amount').innerHTML = thousands_separators(results[12]);
    // document.getElementById('net-invoice-margin').innerHTML = results[13];
    drawNetInvoices(results[14], results[15], _updateflag);

});

new s.web.Model("sales.cogs.margin").call("today_to_monthly_delivery",
    ([x, y, z])).then(function (results) {

    document.getElementById('net-delivery-amount').innerHTML = thousands_separators(results[0]);
    document.getElementById('net-delivery-count').innerHTML = results[1];
    generatedeliveryviewbarchart(results[2], results[3], _updateflag);
});

//Sale Order Trend  Order Trend 30 Days
new s.web.Model("sales.cogs.margin").call("sales_value_monthly",
    ([x, y, z])).then(function (results) {
    orderTrend30Days(results[0], results[1],results[2], _updateflag);

});

// month on month
new s.web.Model("sales.cogs.margin").call("monthon_month_value",
    ([x, y, z])).then(function (results) {

    drawMonthOnMonthSales(results[0], results[1], _updateflag);

    drawMonthOnMonthInvoice(results[0], results[2], _updateflag);

    drawMonthOnMonthRefund(results[0], results[3], _updateflag);

    drawMonthOnMonthNetInvoice(results[0], results[4], _updateflag);

});

// Processing
new s.web.Model("sales.cogs.margin").call("processing_all",
    ([x, y, z])).then(function (results) {

    drawProcessingOrderCountChart(results[0], results[1], _updateflag);

    drawProcessingOrderValueChart(results[0], results[2], _updateflag);

});

// Turn over
new s.web.Model("sales.cogs.margin").call("tunr_over_data_calculation",
    ([x, y, z])).then(function (results) {

    drawTurnOverTimeStacked(results[0], results[1], results[2], _updateflag);

});


new s.web.Model("sales.cogs.margin").call("mgt_dashboard_data_preparation1",
    ([x, y, z])).then(function (results) {

    //console.log(results);

    _labels = results[2];
    _values = results[3];


    drawCategoriwiseSales(results[0], results[1], results[2], results[3], _updateflag);

     document.getElementById('total-margin-amount').innerHTML = thousands_separators(results[35]);
     document.getElementById('total-margin-amount-percent').innerHTML = results[36];

      document.getElementById('total-margin-amount').innerHTML = thousands_separators(results[8]);
     document.getElementById('total-margin-amount-percent').innerHTML = results[9];

    // new net-invoice-and-margin-classification-chart-two
    drawSalesMarginDonutNew(results[11], results[12],results[10], _updateflag);


});

// new s.web.Model("sales.cogs.margin").call("mgt_dashboard_data_preparation",
//     ([x, y, z])).then(function (results) {
//
//     //console.log(results);
//
//     _labels = results[2];
//     _values = results[3];
//
//     // document.getElementById('total-sales').innerHTML = thousands_separators(results[0]);
//     // document.getElementById('total-sales-count').innerHTML = results[1];
//     // generatesalescollectionoverviewbarchart(results[2], results[3], _updateflag);
//     //
//     // document.getElementById('total-amount').innerHTML = thousands_separators(results[4]);
//     // document.getElementById('invoice-count').innerHTML = results[5];
//     // drawChartInvoice(results[6], results[7], _updateflag);
//     //
//     // document.getElementById('against-invoice').innerHTML = thousands_separators(parseInt(results[8]));
//     // document.getElementById('refund-invoice-count').innerHTML = results[9];
//     // drawRefundInvoice(results[10], results[11], _updateflag);
//     //
//     // document.getElementById('net-invoice-amount').innerHTML = thousands_separators(results[12]);
//     // // document.getElementById('net-invoice-margin').innerHTML = results[13];
//     // drawNetInvoices(results[14], results[15], _updateflag);
//
//      drawCategoriwiseSales(results[16], results[17], results[18], results[19], _updateflag);
//
//
//      document.getElementById('total-margin-amount').innerHTML = thousands_separators(results[35]);
//      document.getElementById('total-margin-amount-percent').innerHTML = results[36];
//     // drawSalesMarginDonut(results[22], results[23], _updateflag);
//
//     // new net-invoice-and-margin-classification-chart-two
//     drawSalesMarginDonutNew(results[38], results[39],results[37], _updateflag);
//
//     //drawSalesMarginDonut(results[22], results[23], _updateflag);
//     // drawTurnOverTimeStacked(results[24], results[25], results[26], _updateflag);
//
//     // month on month
//     // drawMonthOnMonthSales(results[27], results[28], _updateflag);
//     //
//     // drawMonthOnMonthInvoice(results[27], results[29], _updateflag);
//     //
//     // drawMonthOnMonthRefund(results[27], results[30], _updateflag);
//     //
//     // drawMonthOnMonthNetInvoice(results[27], results[31], _updateflag);
//
//
//     // Processing
//     // drawProcessingOrderCountChart(results[32], results[33], _updateflag);
//     //
//     // drawProcessingOrderValueChart(results[32], results[34], _updateflag);
//
//     //Hide loader
//     // $('.loading-mask').hide();
//
// });


function generatesalescollectionoverviewbarchart(_labels, _values, _updateflag) {
    //Bar Charts >chart-sales

    var arranged_sales = customerGroupIndex(_labels, _values, 'sales');
    _labels = arranged_sales[0];
    _values = arranged_sales[1];

    var options = {
        series: _values,
        labels: _labels,
        chart: {
            type: 'donut',
            height: _labels.length > 1 ? 250 : 220
        },
        legend: {
                    position: 'bottom'
                },
        fill: {
          type: 'gradient',
        },
        responsive: [{
            breakpoint: 480,
            options: {
                legend: {
                    position: 'bottom'
                }
            }
        }],
        yaxis: {
            show: true,
            decimalsInFloat: 2,
            labels: {
                formatter: function (value) {
                    var amount = parseInt(value);

                    return thousands_separators(amount);
                }
            }
        },
        color:colorPaletteCf
    };

    //var sale_chart = new ApexCharts(document.querySelector("#chart-sales"), options);
    // chart.render();
    //var sale_chart;
    if (_updateflag === false){
        sale_chart = new ApexCharts(document.querySelector("#chart-sales"), options);
        sale_chart.render();

    }else {
        //console.log("This is initialized");
        sale_chart.updateOptions({
            series: _values,
            labels: _labels

          });
    }

    //Hide loader
    $('.loading-mask').hide();

}

function generatedeliveryviewbarchart(_labels, _values, _updateflag) {
    //Bar Charts >chart-sales

    var arranged_sales = customerGroupIndex(_labels, _values, 'delivery');
    _labels = arranged_sales[0];
    _values = arranged_sales[1];

    var options = {
        series: _values,
        labels: _labels,
        chart: {
            type: 'donut',
            height: _labels.length > 1 ? 250 : 220
        },
        legend: {
                    position: 'bottom'
                },
        fill: {
          type: 'gradient',
        },
        responsive: [{
            breakpoint: 480,
            options: {
                legend: {
                    position: 'bottom'
                }
            }
        }],
        yaxis: {
            show: true,
            decimalsInFloat: 2,
            labels: {
                formatter: function (value) {
                    var amount = parseInt(value);

                    return thousands_separators(amount);
                }
            }
        },
        color:colorPaletteCf
    };

    //var sale_chart = new ApexCharts(document.querySelector("#chart-sales"), options);
    // chart.render();
    //var sale_chart;
    if (_updateflag === false){
        sale_chart_netdelivery = new ApexCharts(document.querySelector("#chart-netdelivery"), options);
        sale_chart_netdelivery.render();

    }else {
        //console.log("This is initialized");
        sale_chart_netdelivery.updateOptions({
            series: _values,
            labels: _labels

          });
    }

    //Hide loader
    $('.loading-mask').hide();

}



function drawChartInvoice(_labels, _values, _updateflag) {

    //Bar Charts >chart-invoices
    var arranged_invoice = customerGroupIndex(_labels, _values, 'invoice');
    _labels = arranged_invoice[0];
    _values = arranged_invoice[1];

    var options = {
        series: _values,
        labels: _labels,
        chart: {
            type: 'donut',
            height: _labels.length > 1 ? 250 : 220
        },
        legend: {
                    position: 'bottom'
                },
        fill: {
          type: 'gradient',
        },
        responsive: [{
            breakpoint: 480,
            options: {
                legend: {
                    position: 'bottom'
                }
            }
        }],
         colors: ['#038ffa', '#53e396', '#fdb13b', '#f9475f'
            ],
        yaxis: {
            show: true,
            decimalsInFloat: 2,
            labels: {
                formatter: function (value) {
                    var amount = parseInt(value);

                    return thousands_separators(amount);
                }
            }
        }
    };

    // var sale_inv_chart = new ApexCharts(document.querySelector("#chart-invoices"), options);
    // chart.render();
    if (_updateflag === false){

        sale_inv_chart = new ApexCharts(document.querySelector("#chart-invoices"), options);
        sale_inv_chart.render();

    }else {
        //console.log("This is initialized");
        sale_inv_chart.updateOptions({
            series: _values,
            labels: _labels

          });
    }
    //Hide loader
    $('.loading-mask').hide();

}

function drawRefundInvoice(_labels, _values, _updateflag) {

    //Bar Charts >chart-refund
    var arranged_refund = customerGroupIndex(_labels, _values, 'refund');
    _labels = arranged_refund[0];
    _values = arranged_refund[1];

    var options = {
        series: _values,
        labels: _labels,
        chart: {
            type: 'donut',
            height: _labels.length > 1 ? 250 : 220
        },
        legend: {
                    position: 'bottom'
                },
        fill: {
          type: 'gradient',
        },
        responsive: [{
            breakpoint: 480,
            options: {
                legend: {
                    position: 'bottom'
                }
            }
        }],
         colors: ['#038ffa', '#53e396', '#fdb13b', '#f9475f'
            ],
        yaxis: {
            show: true,
            decimalsInFloat: 2,
            labels: {
                formatter: function (value) {
                    var amount = parseInt(value);

                    return thousands_separators(amount);
                }
            }
        }
    };

    //var sale_refund_chart = new ApexCharts(document.querySelector("#chart-refund"), options);
    // chart.render();
    if (_updateflag === false){
        sale_refund_chart = new ApexCharts(document.querySelector("#chart-refund"), options);
        sale_refund_chart.render();

    }else {
        //console.log("This is initialized");
        sale_refund_chart.updateOptions({
            series: _values,
            labels: _labels

          });
    }
    //Hide loader
    $('.loading-mask').hide();
}

function drawNetInvoices(_labels, _values, _updateflag) {
    //Bar Charts >chart-netinvoices

    var options = {
        series: _values,
        labels: _labels,
        chart: {
            type: 'donut',
             height: _labels.length > 1 ? 250 : 220
        },
        legend: {
                    position: 'bottom'
                },
        fill: {
          type: 'gradient',
        },
        responsive: [{
            breakpoint: 480,
            options: {
                legend: {
                    position: 'bottom'
                }
            }
        }],
        colors: ['#038ffa', '#53e396', '#fdb13b', '#f9475f'
            ],
        yaxis: {
            show: true,
            decimalsInFloat: 2,
            labels: {
                formatter: function (value) {
                    var amount = parseInt(value);

                    return thousands_separators(amount);
                }
            }
        }
    };

    //var sale_net_inv_chart = new ApexCharts(document.querySelector("#chart-netinvoices"), options);
    // chart.render();
    if (_updateflag === false){
        sale_net_inv_chart = new ApexCharts(document.querySelector("#chart-netinvoices"), options);
        sale_net_inv_chart.render();

    }else {
        //console.log("This is initialized");
        sale_net_inv_chart.updateOptions({
            series: _values,
            labels: _labels

          });
    }
    //Hide loader
    $('.loading-mask').hide();
}

//=========== Hide Old Fuunctions ===========

// function drawMonthOnMonthSales(_labels, _values, _updateflag) {
//     var options = {
//         dataLabels: {
//             enabled: true,
//             formatter: function (val, opts) {
//                 // return (val / 1000000).toFixed(2) + "M"
//                 return " "
//             }
//         },
//         series: _values,
//         chart: {
//             type: 'bar',
//             height: 250,
//             stacked: true,
//             toolbar: {
//                 show: true
//             },
//             zoom: {
//                 enabled: true
//             }
//         },
//         legend: {
//                     position: 'bottom'
//                 },
//         responsive: [{
//             breakpoint: 480,
//             options: {
//                 legend: {
//                     position: 'bottom',
//                     offsetX: -10,
//                     offsetY: 0
//                 }
//             }
//         }],
//         plotOptions: {
//             bar: {
//                 horizontal: false
//             }
//         },
//         xaxis: {
//             type: 'string',
//             categories: _labels
//         },
//         yaxis: {
//             show: true,
//             decimalsInFloat: 2,
//             labels: {
//                 formatter: function (value) {
//                     var total;
//                     if(value > 1000000){
//                         total = parseInt(value / 1000000) + "M";
//                     }else {
//                       total = parseInt(value);
//                     }
//
//
//                     return total;
//                 }
//             }
//         },
//         fill: {
//             opacity: 1
//         }
//     };
//
//     //var mom_sale_chart = new ApexCharts(document.querySelector("#monthly-sales-chart"), options);
//     // chart.render();
//     if (_updateflag === true){
//         mom_sale_chart.updateOptions({
//             series: _values,
//             labels: _labels
//
//           });
//     }else {
//         //console.log("This is initialized");
//         mom_sale_chart = new ApexCharts(document.querySelector("#monthly-sales-chart"), options);
//         mom_sale_chart.render();
//     }
//     //Hide loader
//     $('.loading-mask').hide();
// }

/*function drawMonthOnMonthInvoice(_labels, _values, _updateflag) {
    var options = {
        dataLabels: {
            enabled: true,
            formatter: function (val, opts) {
                //return (val / 1000000).toFixed(2) + "M"
                return " "
            }
        },
        series: _values,
        chart: {
            type: 'bar',
            height: 250,
            stacked: true,
            toolbar: {
                show: true
            },
            zoom: {
                enabled: true
            }
        },
        responsive: [{
            breakpoint: 480,
            options: {
                legend: {
                    position: 'bottom',
                    offsetX: -10,
                    offsetY: 0
                }
            }
        }],
        plotOptions: {
            bar: {
                horizontal: false
            }
        },
        xaxis: {
            type: 'string',
            categories: _labels
        },
        yaxis: {
            show: true,
            decimalsInFloat: 2,
            labels: {
                formatter: function (value) {
                    var total;
                    if(value > 1000000){
                        total = parseInt(value / 1000000) + "M";
                    }else {
                        total = parseInt(value);
                    }


                    return total;
                }
            }
        },
        legend: {
            position: 'bottom'
        },
        fill: {
            opacity: 1
        }
    };

    //var mom_sale_inv_chart = new ApexCharts(document.querySelector("#monthly-invoice-chart"), options);
    // chart.render();
    if (_updateflag === true){
        mom_sale_inv_chart.updateOptions({
            series: _values,
            labels: _labels

          });
    }else {
        //console.log("This is initialized");
        mom_sale_inv_chart = new ApexCharts(document.querySelector("#monthly-invoice-chart"), options);
        mom_sale_inv_chart.render();
    }

    //Hide loader
    $('.loading-mask').hide();
}*/


/*function drawMonthOnMonthRefund(_labels, _values, _updateflag) {
    var options = {
        dataLabels: {
            enabled: true,
            formatter: function (val, opts) {
                // return (val / 1000000).toFixed(2) + "M"
                return " "
            }
        },
        series: _values,
        chart: {
            type: 'bar',
            height: 250,
            stacked: true,
            toolbar: {
                show: true
            },
            zoom: {
                enabled: true
            }
        },
        responsive: [{
            breakpoint: 480,
            options: {
                legend: {
                    position: 'bottom',
                    offsetX: -10,
                    offsetY: 0
                }
            }
        }],
        plotOptions: {
            bar: {
                horizontal: false
            }
        },
        xaxis: {
            type: 'string',
            categories: _labels
        },
        yaxis: {
            show: true,
            decimalsInFloat: 2,
            labels: {
                formatter: function (value) {
                    var total;
                    if(value > 1000000){
                        total = parseInt(value / 1000000) + "M";
                    } else {
                         total = parseInt(value);
                    }


                    return total;
                }
            }
        },
        legend: {
            position: 'bottom'
        },
        fill: {
            opacity: 1
        }
    };

    //var mom_sale_refund_chart = new ApexCharts(document.querySelector("#monthly-refund-chart"), options);
    // chart.render();
    if (_updateflag === true){
        mom_sale_refund_chart.updateOptions({
            series: _values,
            labels: _labels

          });
    }else {
        //console.log("This is initialized");
        mom_sale_refund_chart = new ApexCharts(document.querySelector("#monthly-refund-chart"), options);
        mom_sale_refund_chart.render();
    }

    //Hide loader
    $('.loading-mask').hide();
}*/

/*
function drawMonthOnMonthNetInvoice(_labels, _values, _updateflag) {
    var options = {
        dataLabels: {
            enabled: true,
            formatter: function (val, opts) {
                //return (val / 1000000).toFixed(2) + "M"
                return ""
            }
        },
        series: _values,
        chart: {
            type: 'bar',
            height: 250,
            stacked: true,
            toolbar: {
                show: true
            },
            zoom: {
                enabled: true
            }
        },
        responsive: [{
            breakpoint: 480,
            options: {
                legend: {
                    position: 'bottom',
                    offsetX: -10,
                    offsetY: 0
                }
            }
        }],
        plotOptions: {
            bar: {
                horizontal: false
            }
        },
        xaxis: {
            type: 'string',
            categories: _labels

        },
        yaxis: {
            show: true,
            decimalsInFloat: 2,
            labels: {
                formatter: function (value) {
                    var total;

                    if(value > 1000000) {
                         total = parseInt(value / 1000000) + "M";
                    }else {
                        total = parseInt(value);
                    }


                    return total;
                }
            }
        },
        legend: {
            position: 'bottom'
        },
        fill: {
            opacity: 1
        }
    };

    //var mom_sale_net_inv_chart = new ApexCharts(document.querySelector("#monthly-net-invoice-chart"), options);
    // chart.render();
    if (_updateflag === true){
        mom_sale_net_inv_chart.updateOptions({
            series: _values,
            labels: _labels

          });
    }else {
        //console.log("This is initialized");
        mom_sale_net_inv_chart = new ApexCharts(document.querySelector("#monthly-net-invoice-chart"), options);
        mom_sale_net_inv_chart.render();
    }

    //Hide loader
    $('.loading-mask').hide();
}
*/


function drawProcessingOrderCountChart(_labels, _values, _updateflag) {
 //console.log(_labels);
    var options = {
        series: _values,
        labels: _labels,
        chart: {
            type: 'donut',
            height: 350,
        },
        fill: {
          type: 'gradient',
        },
        legend: {
                    position: 'bottom'
                },
        responsive: [{
            breakpoint: 480,
            options: {
                legend: {
                    position: 'bottom'
                }
            }
        }],
        colors: colorPaletteDay
    };

    //var pro_sale_count_chart = new ApexCharts(document.querySelector("#order-count-chart"), options);
    // chart.render();
    if (_updateflag === true){
        pro_sale_count_chart.updateOptions({
            series: _values,
            labels: _labels

          });
    }else {
        //console.log("This is initialized");
        pro_sale_count_chart = new ApexCharts(document.querySelector("#order-count-chart"), options);
        pro_sale_count_chart.render();
    }

    //Hide loader
    $('.loading-mask').hide();

}

function format_catetory_list(catogories){
    // ['a', 'b']
  //console.log(catogories);
    var formatted_category = [];
    for (i = 0; i < catogories.length; i++) {
      //text += cars[i] + "<br>";
        var for_cat = format_category(catogories[i]).replace(" Items", "");
        formatted_category.push(for_cat);
    }

    return formatted_category;
}

function drawCategoriwiseSales2(_catglist, _costlist, _salelist, _marginlist, _updateflag) {
  _catglist = format_catetory_list(_catglist);
  var options = {
  chart: {
    height: 350,
    type: "line",
    stacked: false
  },
  dataLabels: {
    enabled: false
  },
  colors: ['#99C2A2', '#C5EDAC', '#66C7F4'],
  series: [
    {
      name: 'Sales',
      type: 'column',
      data: _salelist
    },
    {
      name: "Cost",
      type: 'column',
      data: _costlist
    },
    {
      name: "Margin",
      type: 'line',
      data: _marginlist
    },
  ],
  stroke: {
    width: [4, 4, 4]
  },
  plotOptions: {
    bar: {
      columnWidth: "20%"
    }
  },
  xaxis: {
    categories: _catglist
  },
  yaxis: [
    {
      seriesName: 'Sales',
      axisTicks: {
        show: true
      },
      axisBorder: {
        show: true,
      },
    labels: {
            formatter: function (value) {
                var amount = parseInt(value);

                return thousands_separators(amount);
            }
        },
      title: {
        text: "Sales / Cost"
      }
    },
    {
      seriesName: 'Sales',
      show: false
    }, {
      opposite: true,
      seriesName: 'Margin',
      axisTicks: {
        show: true
      },
      axisBorder: {
        show: true,
      },
      labels: {
                formatter: function (value) {
                    var amount = parseInt(value);

                    return thousands_separators(amount);
                }
            },
      title: {
        text: "Margin %"
      }
    }
  ],
  tooltip: {
    shared: false,
    intersect: true,
    x: {
      show: false
    }
  },
  legend: {
    horizontalAlign: "left",
    offsetX: 40
  }
};
if (_updateflag === true){
        categ_sale_chart.updateOptions({
            series: [{
                name: 'Sales',
                type: 'column',
                data: _salelist
            }, {
                name: 'Cost',
                type: 'column',
                data: _costlist
            }, {
                name: 'Margin',
                type: 'line',
                data: _marginlist
            }],
            xaxis: {
                categories: _catglist
            },

          });
    }else {
        //console.log("This is initialized");
        categ_sale_chart = new ApexCharts(document.querySelector("#net-invoice-and-margin-category-chart2"), options);
        categ_sale_chart.render();
    }


}


// net-invoice-and-margin-category-chart
function drawCategoriwiseSales(_catglist, _costlist, _salelist, _marginlist, _updateflag) {
    //console.log(_catglist);

    _catglist = format_catetory_list(_catglist);

    var options = {
        series: [{
            name: 'Sales',
            type: 'column',
            data: _salelist
        }, {
            name: 'Cost',
            type: 'column',
            data: _costlist
        }, {
            name: 'Margin',
            type: 'line',
            data: _marginlist
        }],
        chart: {
            height: 350,
            type: 'line',
            stacked: false
        },
        dataLabels: {
            enabled: false
        },
        stroke: {
            width: [1, 1, 4],
            curve: 'smooth'
        },
        title: {
            text: 'Category Wise Sales & Margin ',
            align: 'left',
            offsetX: 110
        },
        xaxis: {
            categories: _catglist
        },
        yaxis: [
            {
                seriesName: 'Cost',
                show:false,
                labels: {
                    style: {
                        colors: '#00E396'
                    },
                    formatter: function (value) {
                        var amount = parseInt(value);

                        return thousands_separators(amount);
                    }
                }
            },
            {
                axisTicks: {
                    show: true
                },
                axisBorder: {
                    show: true,
                    color: '#008FFB'
                },
                labels: {
                    style: {
                        colors: '#008FFB'
                    },
                    formatter: function (value) {
                        var amount = parseInt(value);

                        return thousands_separators(amount);
                    }
                },
                title: {
                    text: "Sales / Cost",
                    style: {
                        color: '#008FFB'
                    }
                },
                tooltip: {
                    enabled: true
                },
                show: true,
                decimalsInFloat: 2

            },
            {
                seriesName: 'Revenue',
                opposite: true,
                axisTicks: {
                    show: true
                },
                axisBorder: {
                    show: true,
                    color: '#FEB019'
                },
                labels: {
                    style: {
                        colors: '#FEB019'
                    },
                    formatter: function (value) {
                        var amount = parseInt(value);

                        //return thousands_separators(amount);
                        return value;
                    }
                },
                title: {
                    text: "Margin %",
                    style: {
                        color: '#FEB019'
                    }
                },
                show: true,
                decimalsInFloat: 2
            }
        ],
        tooltip: {
            fixed: {
                enabled: true,
                position: 'topLeft', // topRight, topLeft, bottomRight, bottomLeft
                offsetY: 30,
                offsetX: 60
            }
        },
        legend: {
            horizontalAlign: 'left',
            offsetX: 40
        }
    };

    //var categ_sale_chart = new ApexCharts(document.querySelector("#net-invoice-and-margin-category-chart"), options);
    // chart.render();
    if (_updateflag === true){
        categ_sale_chart.updateOptions({
            series: [{
                name: 'Sales',
                type: 'column',
                data: _salelist
            }, {
                name: 'Cost',
                type: 'column',
                data: _costlist
            }, {
                name: 'Margin',
                type: 'line',
                data: _marginlist
            }],
            xaxis: {
                categories: _catglist
            },

          });
    }else {
        //console.log("This is initialized");
        categ_sale_chart = new ApexCharts(document.querySelector("#net-invoice-and-margin-category-chart"), options);
        categ_sale_chart.render();
    }

    //Hide loader
    $('.loading-mask').hide();

}


function drawSalesMarginDonut(_clabels, _cvalues, _updateflag) {

    //Bar Charts >chart-refund
    var options = {
        series: _cvalues,
        labels: _clabels,
        chart: {
            type: 'donut',
            height: 350
        },
        responsive: [{
            breakpoint: 480,
            options: {
                legend: {
                    position: 'bottom'
                }
            }
        }]
    };

    //var sale_margin_chart = new ApexCharts(document.querySelector("#net-invoice-and-margin-classification-chart"), options);
    // chart.render();
    if (_updateflag === true){
        sale_margin_chart.updateOptions({
            series: _values,
            labels: _labels

          });
    }else {
        //console.log("This is initialized");
        sale_margin_chart = new ApexCharts(document.querySelector("#net-invoice-and-margin-classification-chart"), options);
        sale_margin_chart.render();
    }

    //Hide loader
    $('.loading-mask').hide();

}

// new net-invoice-and-margin-classification-chart-two
function drawSalesMarginDonutNew(_clabels, _cvalues,_customv, _updateflag) {

	_cvalues = arrayFormatter(_cvalues);

  var options = {
        series: [{
              name: 'Profit Value',
              type: 'column',
              data: _clabels
            }, {
              name: 'Margin (%)',
              type: 'line',
              data: _cvalues
            }],
        chart: {
          height: 281,
          type: 'line'
        },
        stroke: {
          width: [0, 4]
        },
        title: {
          text: 'Profit and Margin'
        },
        dataLabels: {
          enabled: true,
          enabledOnSeries: [1]
        },
        labels: _customv,
        xaxis: {
          type: 'string',
            color: '#feb019'
        },
        yaxis: [{
          title: {
            text: 'Profit Value'
          },
            decimalsInFloat: 2,
            labels: {
                formatter: function (value) {
                    var amount = (value);

                    return thousands_separators(amount);
                }
            }

        }, {
          opposite: true,
          title: {
            text: 'Margin (%)'
          }
        }]
        };


/*var chart = new ApexCharts(document.querySelector("#net-invoice-and-margin-classification-chart-two"), options);

chart.render();*/



        //sale_margin_chart_line_column = new ApexCharts(document.querySelector("#net-invoice-and-margin-classification-chart-two"), options);
        //sale_margin_chart_line_column.render();

        /*console.log(options);
        console.log(sale_margin_chart_line_column);*/


    //var sale_margin_chart = new ApexCharts(document.querySelector("#net-invoice-and-margin-classification-chart"), options);
    // chart.render();
    if (_updateflag === true){
        sale_margin_chart_line_column.updateOptions({
            series: [{
              name: 'Profit Value',
              type: 'column',
              data: _clabels
            }, {
              name: 'Margin (%)',
              type: 'line',
              data: _cvalues
            }],
            labels: _customv

          });
    }else {
        //console.log("This is initialized");
        sale_margin_chart_line_column = new ApexCharts(document.querySelector("#net-invoice-and-margin-classification-chart-two"), options);
        sale_margin_chart_line_column.render();
    }


    //Hide loader
    $('.loading-mask').hide();

}
// end

function format_value_arounDay(day_val) {
    var formated_val = [];
    for (var i = 0; i < day_val.length; i++) {
        formated_val.push(day_val[i].toFixed(2))
    }

    return formated_val;
}


function drawProcessingOrderValueChart(_labels, _values) {
 //console.log(_values);
    var options = {
        series: _values,
        labels: _labels,
        chart: {
            type: 'donut',
            height: 350


        },
        fill: {
          type: 'gradient',
        },
        legend: {
                    position: 'bottom'
                },
        responsive: [{
            breakpoint: 480,
            options: {
                legend: {
                    position: 'bottom'
                }
            }
        }],
        tooltip: {
            y: {
                formatter: function (value) {
                        var amount = parseInt(value);

                        return thousands_separators(amount);
                    }
            }
        },
        colors: colorPaletteDay
    };

    //var pro_order_value_chart = new ApexCharts(document.querySelector("#order-value-chart"), options);
    // chart.render();
    if (_updateflag === true){
        pro_order_value_chart.updateOptions({
            series: _values,
            labels: _labels

          });

    }else {
        //console.log("This is initialized");
        pro_order_value_chart = new ApexCharts(document.querySelector("#order-value-chart"), options);
        pro_order_value_chart.render();
    }

    //Hide loader
    $('.loading-mask').hide();


}

//===================================

function drawTurnOverTimeStacked(_v1,_v2,_v3)
{
    _v1 = format_value_arounDay(_v1);
    _v2 = format_value_arounDay(_v2);
    _v3 = format_value_arounDay(_v3);
	let series = [
	{
            name: 'Order To Confirm',
            data: _v1
        }, {
            name: 'Confirm To Invoice',
            data: _v2
        }, {
            name: 'Invoice To Delivery',
            data: _v3
        }
	];
	let categories = ['Ananta', 'Corporate', 'Retail', 'Online'];
	let element = document.querySelector("#turn-over-time-chart");

	turnOverTimeStackedChart(series, categories, element)
}


function turnOverTimeStackedChart(series, categories, element) {
	let chart;
	const options = {
		series,
		chart: {
			type: "bar",
			height: 350,
			stacked: true,
			toolbar: {
				show: false
			},
			events: {
				mounted: (chartContext, config) => {
					//console.log("mounted", chartContext, config, config.globals.yRange);
					setTimeout(() => {
						addAnnotations(config);
					});
				},
				updated: (chartContext, config) => {
					setTimeout(() => {
						addAnnotations(config);
					});
				}
			}
		},
		dataLabels: {
			enabled: true
		},
		plotOptions: {
			bar: {
				columnWidth: '100%',
				horizontal: true,
				dataLabels: {
					maxItems: 2
				}
			}
		},
		xaxis: {
			categories,

			axisTicks: {
				show: true
			},
			axisBorder: {
				show: true
			},

			labels: {
				hideOverlappingLabels: false,
			}
		},
		yaxis: {
			axisTicks: {
				show: true,

			},
			axisBorder: {
				show: true
			},
			labels: {
				hideOverlappingLabels: true,
                style: {
              fontSize: '12px',
          }
			}
		},
		fill: {
			opacity: 1
		},
		legend: {
			position: "bottom",
			horizontalAlign: "left",
            offsetX: 40
		},
        tooltip: {
            y: {
                formatter: function (val) {
                    return val.toFixed(2) + " Days"
                }
            }
        },
		grid: {
			padding: {
				left: 13.5,
				right: 0
			},
			xaxis: {
				lines: {
					show: true
				}
			},
		}
	};
	const addAnnotations = (config) => {
		let seriesTotals = config.globals.stackedSeriesTotals;
		   seriesTotals = seriesTotals.map(a => "Total: "+ a.toFixed(2) + " Days");

		const isHorizontal = options.plotOptions.bar.horizontal;
		chart.clearAnnotations();

		try {
			categories.forEach((category, index) => {
				chart.addPointAnnotation(
				{
					y: isHorizontal
					? calcHorizontalY(config, index)
					: seriesTotals[index],
					x: isHorizontal ? 0 : category,
					label: {
						text: `${seriesTotals[index]}`,
                        style: {
                        fontSize: "12px",
                        fontWeight: "normal"
                      }
					}
				},
				false
				);

				if (isHorizontal) {
					adjustPointAnnotationXCoord(config, index);
				}
			});
		} catch (error) {
			//console.log(`Add point annotation error: ${error.message}`);
		}
	};

	const calcHorizontalY = (config, index) => {
		const catLength = categories.length;
		const yRange = config.globals.yRange[0];
		const minY = config.globals.minY;
		const halfBarHeight = yRange / catLength / 2;

		return minY + halfBarHeight + 2 * halfBarHeight * (catLength - 1 - index);
	};

	const adjustPointAnnotationXCoord = (config, index) => {
			const gridWidth 	= config.globals.gridWidth;
			const seriesTotal 	= config.globals.stackedSeriesTotals[index];
			const minY 			= Math.abs(config.globals.minY);
			const yRange 		= config.globals.yRange[0];
			const xOffset 		= (gridWidth * (seriesTotal + minY)) / yRange;
			const chartId 		= config.globals.dom.baseEl.id;

			const circle 		= document.querySelector(
			       `#${chartId} .apexcharts-point-annotations circle:nth-of-type(${index + 1})`
			                                        );
			const labelField = document.querySelector(
			       `#${chartId} .apexcharts-point-annotations rect:nth-of-type(${index + 1}`
			                                          );
			const labelFieldXCoord = parseFloat(labelField.getAttribute("x"));
			const text = document.querySelector(
			       `#${chartId} .apexcharts-point-annotations text:nth-of-type(${index + 1}`
			                                    );

			labelField.setAttribute("x", labelFieldXCoord + xOffset);
			text.setAttribute("x", xOffset);
			circle.setAttribute("cx", xOffset);
		};

	chart = new ApexCharts(element, options);
	chart.render();
}

//======== new drawMonthOnMonthSales
function drawMonthOnMonthSales(_v1,_v2,_v3) {
    //testvalue = format_value_million(_v2);


    let series = _v2;
	let categories = _v1;
	let element = document.querySelector("#monthly-sales-chart");

	generateMonthSalesChart(series, categories, element)
}

//======== new drawMonthOnMonthInvoice
function drawMonthOnMonthInvoice(_v1,_v2,_v3) {
    //testvalue = format_value_million(_v2);


    let series = _v2;
	let categories = _v1;
	let element = document.querySelector("#monthly-invoice-chart");

	generateMonthSalesChart(series, categories, element)
}
//======== new drawMonthOnMonthRefund
function drawMonthOnMonthRefund(_v1,_v2,_v3) {
    //testvalue = format_value_million(_v2);


    let series = _v2;
	let categories = _v1;
	let element = document.querySelector("#monthly-refund-chart");

	generateMonthSalesChart(series, categories, element)
}

//======== new drawMonthOnMonthNetInvoice
function drawMonthOnMonthNetInvoice(_v1,_v2,_v3) {
    //testvalue = format_value_million(_v2);


    let series = _v2;
	let categories = _v1;
	let element = document.querySelector("#monthly-net-invoice-chart");

	generateMonthSalesChart(series, categories, element)
}

//======== new drawMonthOnMonth all sales Invoice Refund Net Invoice
function generateMonthSalesChart(series, categories, element) {
    let chart;
	const options = {
		series,
		chart: {
			type: "bar",
			height: 250,
			stacked: true,
			toolbar: {
				show: false
			},
			events: {
				mounted: (chartContext, config) => {
					//console.log("mounted", chartContext, config, config.globals.yRange);
					setTimeout(() => {
						addAnnotations(config);
					});
				},
				updated: (chartContext, config) => {
					setTimeout(() => {
						addAnnotations(config);
					});
				}
			}
		},
		dataLabels: {
			enabled: true,show: true,
            decimalsInFloat: 2,

             formatter: function (value) {
              var total;
             if(value > 1000000){
             total = parseInt(value / 1000000) + "M";
            }else {
            total = parseInt(value);
             }
             return total;
              }



		},
		plotOptions: {
			bar: {

				horizontal: false,
				dataLabels: {
					maxItems: 2
				}
			}
		},
		xaxis: {
			categories,

			axisTicks: {
				show: true
			},
			axisBorder: {
				show: true
			},

			labels: {
				hideOverlappingLabels: false,
			}
		},
		yaxis: {
			axisTicks: {
				show: true,

			},
			axisBorder: {
				show: true
			},


          show: true,
             decimalsInFloat: 2,
            labels: {
             formatter: function (value) {
              var total;
             if(value > 1000000){
             total = parseInt(value / 1000000) + "M";
            }else {
            total = parseInt(value);
             }
             return total;
              },
                hideOverlappingLabels: true,
                style: {
              fontSize: '12px',
         }
		},
            },
		fill: {
			opacity: 1
		},
		legend: {
			position: "bottom",
			horizontalAlign: "left",
            offsetX: 40
		},
        tooltip: {
            y: {
                formatter: function (value) {
              var total;
             if(value > 1000000){
             total = parseInt(value / 1000000) + "M";
            }else {
            total = parseInt(value);
             }
             return total;
              }
            }
        },
		grid: {
			padding: {
				left: 13.5,
				right: 0
			},
			xaxis: {
				lines: {
					show: true
				}
			},
		}
	};
	const addAnnotations = (config) => {

		let seriesTotals = config.globals.stackedSeriesTotals;

		   // seriesTotals = seriesTotals.map(a =>format_value_million(a));


		const isHorizontal = options.plotOptions.bar.horizontal;
		chart.clearAnnotations();

		try {
			categories.forEach((category, index) => {
				chart.addPointAnnotation(
				{
					y: isHorizontal
					? calcHorizontalY(config, index)
					: seriesTotals[index],
					x: isHorizontal ? 0 : category,
					label: {
						text: format_value_million(`${seriesTotals[index]}`),
                        style: {
                        fontSize: "12px",
                        fontWeight: "normal"
                      }
					}
				},
				false
				);

				if (isHorizontal) {
					adjustPointAnnotationXCoord(config, index);
				}
			});
		} catch (error) {
			//console.log(`Add point annotation error: ${error.message}`);
		}
	};

	const calcHorizontalY = (config, index) => {
		const catLength = categories.length;
		const yRange = config.globals.yRange[0];
		const minY = config.globals.minY;
		const halfBarHeight = yRange / catLength / 2;

		return minY + halfBarHeight + 2 * halfBarHeight * (catLength - 1 - index);
	};

	const adjustPointAnnotationXCoord = (config, index) => {
			const gridWidth 	= config.globals.gridWidth;
			const seriesTotal 	= config.globals.stackedSeriesTotals[index];
			const minY 			= Math.abs(config.globals.minY);
			const yRange 		= config.globals.yRange[0];
			const xOffset 		= (gridWidth * (seriesTotal + minY)) / yRange;
			const chartId 		= config.globals.dom.baseEl.id;
			console.log(index)

			const circle 		= document.querySelector(
			       `#${chartId} .apexcharts-point-annotations circle:nth-of-type(${index + 1})`
			                                        );
			const labelField = document.querySelector(
			       `#${chartId} .apexcharts-point-annotations rect:nth-of-type(${index + 1}`
			                                          );
			const labelFieldXCoord = parseFloat(labelField.getAttribute("x"));
			const text = document.querySelector(
			       `#${chartId} .apexcharts-point-annotations text:nth-of-type(${index + 1}`
			                                    );

			labelField.setAttribute("x", labelFieldXCoord + xOffset);
			text.setAttribute("x", xOffset);
			circle.setAttribute("cx", xOffset);
		};

	    chart = new ApexCharts(element, options);
        chart.render();
    //Hide loader
    $('.loading-mask').hide();

}




//
// function drawMonthOnMonthSales(_labels, _values, _updateflag) {
//     var options = {
//         dataLabels: {
//             enabled: true,
//             formatter: function (val, opts) {
//                 // return (val / 1000000).toFixed(2) + "M"
//                 return " "
//             }
//         },
//         series: _values,
//         chart: {
//             type: 'bar',
//             height: 250,
//             stacked: true,
//             toolbar: {
//                 show: true
//             },
//             zoom: {
//                 enabled: true
//             }
//         },
//         legend: {
//                     position: 'bottom'
//                 },
//         responsive: [{
//             breakpoint: 480,
//             options: {
//                 legend: {
//                     position: 'bottom',
//                     offsetX: -10,
//                     offsetY: 0
//                 }
//             }
//         }],
//         plotOptions: {
//             bar: {
//                 horizontal: false
//             }
//         },
//         xaxis: {
//             type: 'string',
//             categories: _labels
//         },
//         yaxis: {
//             show: true,
//             decimalsInFloat: 2,
//             labels: {
//                 formatter: function (value) {
//                     var total;
//                     if(value > 1000000){
//                         total = parseInt(value / 1000000) + "M";
//                     }else {
//                       total = parseInt(value);
//                     }
//
//
//                     return total;
//                 }
//             }
//         },
//         fill: {
//             opacity: 1
//         }
//     };
//
//     //var mom_sale_chart = new ApexCharts(document.querySelector("#monthly-sales-chart"), options);
//     // chart.render();
//     if (_updateflag === true){
//         mom_sale_chart.updateOptions({
//             series: _values,
//             labels: _labels
//
//           });
//     }else {
//         //console.log("This is initialized");
//         mom_sale_chart = new ApexCharts(document.querySelector("#monthly-sales-chart"), options);
//         mom_sale_chart.render();
//     }
//     //Hide loader
//     $('.loading-mask').hide();
// }





//=========drawTurnOverTimeStacked OLD STYLE ============

/*function drawTurnOverTimeStacked(_v1, _v2, _v3) {

    _v1 = format_value_arounDay(_v1);
    _v2 = format_value_arounDay(_v2);
    _v3 = format_value_arounDay(_v3);
    var options = {
        series: [{
            name: 'Order To Confirm',
            data: _v1
        }, {
            name: 'Confirm To Invoice',
            data: _v2
        }, {
            name: 'Invoice To Delivery',
            data: _v3
        }],

        chart: {
            type: 'bar',
            height: 350,
            stacked: true
        },
        plotOptions: {
            bar: {
                horizontal: true,
                dataLabels: {
                position: 'center', // top, center, bottom
            },
            }
        },
        stroke: {
            width: 1,
            colors: ['#fff']
        },
        dataLabels: {
          enabled: true,
          formatter: function(val, opt) {
              return val
          },
        },
        title: {
            text: ''
        },
        xaxis: {
            categories: ['Ananta', 'Corporate', 'Retail', 'Online'],
            labels: {
                formatter: function (val) {
                    return val.toFixed(2) + " Days"
                }
            }
        },
        yaxis: {
            title: {
                text: undefined
            }
        },
        tooltip: {
            y: {
                formatter: function (val) {
                    return val.toFixed(2) + " Days"
                }
            }
        },
        fill: {
            opacity: 1
        },
        legend: {
            position: 'bottom',
            horizontalAlign: 'left',
            offsetX: 40
        }
    };

    //var turn_over_time_chart = new ApexCharts(document.querySelector("#turn-over-time-chart"), options);
    // chart.render();
    if (_updateflag === true){
        turn_over_time_chart.updateOptions({
            series: [{
                name: 'Order To Confirm',
                data: _v1
            }, {
                name: 'Confirm To Invoice',
                data: _v2
            }, {
                name: 'Invoice To Delivery',
                data: _v3
            }],

          });
    }else {
        //console.log("This is initialized");
        turn_over_time_chart = new ApexCharts(document.querySelector("#turn-over-time-chart"), options);
        turn_over_time_chart.render();
    }

    //Hide loader
    $('.loading-mask').hide();


}*/




var s = new openerp.init();
var _labels;
var _values;
var x;
var y;
var z;
var q;
var start_date = null;
var end_date = null;
var classification = null;
var product_category = null;
var _updateflag = false;

//-----------charts-----------
var sale_chart;
var sale_inv_chart;
var sale_refund_chart;
var sale_net_inv_chart;
var mom_sale_chart;
var mom_sale_inv_chart;
var mom_sale_refund_chart;
var mom_sale_net_inv_chart;
var pro_sale_count_chart;
var categ_sale_chart;
var sale_margin_chart;
var sale_margin_chart_line_column;
var turn_over_time_chart;
var pro_order_value_chart;
var top_ten_skus_chart;
var mom_unique_pos_chart;
var mom_active_pos_chart;
var mom_new_pos_chart;
var mom_registered_pos_chart;
var order_trend_rm_wise_chart;


//----------- Processing Order Value Day color ---------

var colorPaletteDay = ['#009900', '#FFCC00', '#FF0000'];

//----------- Ananta,Corporate,Retail,General  ---------

var colorPaletteCf = ['#1e5985', '#00e396', '#feb019', '#ff4560'];

//----------- Grocery,Stationary,Electronics,Mobile Accessories,Others,SOHO  ---------

var colorCategory = ['#008ffb', '#00e396', '#feb019', '#ff4560', '#775dd0', '#800000'];

//Navbar Brand Link Page Refresh
function confirmRefresh() {
        //setTimeout("location.reload(true);",100);
        window.location.reload(true);
}
// Page Refresh


function refreshPage(){
    new s.web.Model("retail.dashboard").call("monthon_month_pos_cal");
}
// Add active class to the current button (highlight it)
var dayWeekMonth = document.getElementById("twm");
var btns = dayWeekMonth.getElementsByClassName("btn");
for (var i = 0; i < btns.length; i++) {
    btns[i].addEventListener("click", function () {
        var current = document.getElementsByClassName("active");
        current[0].className = current[0].className.replace(" active", "");
        this.className += " active";
    });
}

var valuCount = document.getElementById("valucount");
var btns2 = valuCount.getElementsByClassName("btn");
for (var i = 0; i < btns2.length; i++) {
    btns2[i].addEventListener("click", function () {
        var current2 = valuCount.getElementsByClassName("active");
        current2[0].className = current2[0].className.replace(" active", "");
        this.className += " active";
    });
}

// Top ten sku Button Select Options

var tenSkusValue = document.getElementById("tenskusvalue");
var btns3 = tenSkusValue.getElementsByClassName("btn");
var categorySelect = document.getElementById("category-classification-select");
var productcategorySelect = document.getElementById("product-category-select");
for (var i = 0; i < btns3.length; i++) {
    btns3[i].addEventListener("click", function () {
        var current3 = tenSkusValue.getElementsByClassName("active");
        current3[0].className = current3[0].className.replace(" active", "");
        this.className += " active";
        //console.log(categorySelect.value);
        categorySelect.value;
        productcategorySelect.value;
    });
}

// search-by-date Button click topten skus top10-skus-delivery button active

document.getElementById("search-by-date").addEventListener("click", searchClickFunction);

function searchClickFunction() {
    var tenSkusValue = document.getElementById("tenskusvalue");
    var btns3 = tenSkusValue.getElementsByClassName("btn");

    for (var i = 0; i < btns3.length; i++) {

        var current3 = tenSkusValue.getElementsByClassName("active");
        current3[0].className = current3[0].className.replace(" active", "");
        var top10Skusdelivery = document.getElementById("top10-skus-delivery");
        top10Skusdelivery.classList.add("active");

    }
    return top10Skusdelivery;
}


// Current Date

function currentDate() {
    var d = new Date();
    var today_date = d.getDate() + " " + d.toLocaleString('default', {month: 'long'}) + ", " + d.getFullYear();
    return today_date;
}

document.getElementById('current-date').innerHTML = currentDate();

//-----------charts-----------

function formatDate(dateObj) {
    //var dateObj = new Date();
    var today_date = dateObj.getDate();
    var today_month = dateObj.getMonth() + 1;
    var today_year = dateObj.getFullYear();

    var formated_today = today_year + "-" + today_month + "-" + today_date;

    return formated_today;
}

function thousands_separators(num) {
    num = parseInt(num);
    var num_parts = num.toString().split(".");
    num_parts[0] = num_parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return num_parts.join(".");
}

function format_category(category) {
    var res = category.split("-");
    var len = res.length;
    return res[len - 1].trim();
}
function format_value_million(num){
    var total =[];
     if(num > 1000000){
    total = parseInt(num / 1000000) + "M";
     }else {
    total = parseInt(num);
      }
    return total;
}

function format_catetory_list(catogories) {
    // ['a', 'b']
    //console.log(catogories);
    var formatted_category = [];
    for (i = 0; i < catogories.length; i++) {
        //text += cars[i] + "<br>";
        var for_cat = format_category(catogories[i]).replace(" Items", "");
        formatted_category.push(for_cat);
    }

    return formatted_category
}

var instance=openerp;
instance.session.rpc("/web/session/get_session_info", {}).then(function(result) {

    if (result.uid){
        $('.loading-mask').show();

    }else {
        alert("Your Odoo Session Expired. Please Re Login into the System");
        location.replace("http://odoo.sindabad.com/web/login");
    }
});
// ALL Block DATE Cclassification product_category

x = start_date === null ? formatDate(new Date()) : start_date;
y = end_date === null ? x : end_date;
z = classification === null ? 'top10order' : classification;
q = product_category === null ? null : product_category;

// Sales Invoices Refund Net Invoices Block
new s.web.Model("retail.dashboard").call("retail_today_to_monthly_sales_trending",
    ([x, y, z])).then(function (results) {

    document.getElementById('total-sales').innerHTML = thousands_separators(results[0]);
    document.getElementById('total-sales-count').innerHTML = results[1];
    generatesalescollectionoverviewbarchart(results[2], results[3], _updateflag);

    document.getElementById('total-amount').innerHTML = thousands_separators(results[4]);
    document.getElementById('invoice-count').innerHTML = results[5];
    drawChartInvoice(results[6], results[7], _updateflag);

    document.getElementById('against-invoice').innerHTML = thousands_separators(parseInt(results[8]));
    document.getElementById('refund-invoice-count').innerHTML = results[9];
    drawRefundInvoice(results[10], results[11], _updateflag);


    document.getElementById('net-invoice-amount').innerHTML = thousands_separators(results[12]);
    // document.getElementById('net-invoice-margin').innerHTML = results[13];
    drawNetInvoices(results[14], results[15], _updateflag);

});

// Month On Month (Last 6 Months) Block

new s.web.Model("retail.dashboard").call("monthon_month_value",
    ([x, y, z])).then(function (results) {

    drawMonthOnMonthSales(results[0], results[1], _updateflag);

    drawMonthOnMonthInvoice(results[0], results[2], _updateflag);

    drawMonthOnMonthRefund(results[0], results[3], _updateflag);

    drawMonthOnMonthNetInvoice(results[0], results[4], _updateflag);

});

// Net Invoice and Margin Block

new s.web.Model("retail.dashboard").call("retail_dashboard_data_preparation",
    ([x, y, z])).then(function (results) {
       // console.log(results)

    drawCategoriwiseSales(results[0], results[1], results[2], results[3], _updateflag);
    drawSalesMarginDonutNew(results[12], results[13],results[11], _updateflag);
});

// Top 10 SKUs Block

new s.web.Model("retail.dashboard").call("topTenSkusorder",
    ([x, y, z])).then(function (results) {

    drawTopTenSkus(results[0], results[1], 'Delivered');

});

// Customer Base Month On Month (Last 6 Months) Registered Block
new s.web.Model("retail.dashboard").call("monthon_month_registered",
    ([x, y, z])).then(function (results) {

    registeredPosChart(results[0], results[1], _updateflag);

    total_sum1 = table_data(results[0], results[1], 'registered-pos-table', _updateflag);
    document.getElementById('total-registered').innerHTML = results[2];

});

// Customer Base Month On Month (Last 6 Months) Unique POS  Block
new s.web.Model("retail.dashboard").call("monthon_month_unique_pos",
    ([x, y, z])).then(function (results) {

    uniquePosChart(results[0], results[1], _updateflag);
    table_data(results[0], results[1], 'unique-pos-table', _updateflag);
     document.getElementById('total-unique-pos').innerHTML = results[2];


});

// Customer Base Month On Month (Last 6 Months) Active POS  Block
new s.web.Model("retail.dashboard").call("monthon_month_active_pos",
    ([x, y, z])).then(function (results) {

    activePosChart(results[0], results[1], _updateflag);
    table_data(results[0], results[1], 'active-pos-table', _updateflag);
     document.getElementById('total-active-pos').innerHTML = results[2];


});

// Customer Base Month On Month (Last 6 Months) New POS  Block
new s.web.Model("retail.dashboard").call("monthon_month_new_pos",
    ([x, y, z])).then(function (results) {

    newPosChart(results[0], results[1], _updateflag);
    table_data(results[0], results[1], 'new-pos-table', _updateflag);
     document.getElementById('total-new-pos').innerHTML = results[2];

});
//========== new add
// Customer Base Month On Month
new s.web.Model("retail.dashboard").call("active_pos_based_on_order",
    ([x, y, z])).then(function (results) {

    activePosChart(results[0], results[1], _updateflag);
    table_data(results[0], results[1], 'active-pos-table2', _updateflag);
    document.getElementById('total-active-pos2').innerHTML = results[2];
});
// Customer Base Month On Month (Last 6 Months) New POS  Block
new s.web.Model("retail.dashboard").call("new_pos_based_on_order",
    ([x, y, z])).then(function (results) {

    newPosChart(results[0], results[1], _updateflag);
    table_data(results[0], results[1], 'new-pos-table2', _updateflag);
    document.getElementById('total-new-pos2').innerHTML = results[2];

});

// Order Trend
new s.web.Model("retail.dashboard").call("rm_wise_sales_value",
    ([x, y, z])).then(function (results) {
    orderTrendRmWise(results[0], results[1],results[2], _updateflag);

});


function salesToday() {

    //Show loader
    $('.loading-mask').show();

    var x = formatDate(new Date());
    var y = formatDate(new Date());
    var z = 'all';
    _updateflag = true;

    new s.web.Model("retail.dashboard").call("retail_today_to_monthly_sales_trending",
        ([x, y, z])).then(function (results) {


        _labels = results[2];
        _values = results[3];

        document.getElementById('total-sales').innerHTML = thousands_separators(results[0]);

        document.getElementById('total-sales-count').innerHTML = results[1];
        generatesalescollectionoverviewbarchart(_labels, _values, _updateflag);

        document.getElementById('total-amount').innerHTML = thousands_separators(results[4]);
        document.getElementById('invoice-count').innerHTML = results[5];
        drawChartInvoice(results[6], results[7], _updateflag);

        document.getElementById('against-invoice').innerHTML = thousands_separators(results[8]);
        document.getElementById('refund-invoice-count').innerHTML = results[9];
        drawRefundInvoice(results[10], results[11], _updateflag);

        document.getElementById('net-invoice-amount').innerHTML = thousands_separators(results[12]);
        // document.getElementById('net-invoice-margin').innerHTML = results[13];
        drawNetInvoices(results[14], results[15], _updateflag);


    });
}

function salesWeekly() {

    //Show loader
    $('.loading-mask').show();

    var ourDate = new Date();
    ourDate.setDate(ourDate.getDate() - 7);

    var x = formatDate(new Date());
    var y = formatDate(ourDate);
    var z = 'all';
    _updateflag = true;

    new s.web.Model("retail.dashboard").call("retail_today_to_monthly_sales_trending",
        ([x, y, z])).then(function (results) {


        _labels = results[2];
        _values = results[3];

        document.getElementById('total-sales').innerHTML = thousands_separators(results[0]);
        document.getElementById('total-sales-count').innerHTML = results[1];

        generatesalescollectionoverviewbarchart(_labels, _values, _updateflag);

        document.getElementById('total-amount').innerHTML = thousands_separators(results[4]);
        document.getElementById('invoice-count').innerHTML = results[5];
        drawChartInvoice(results[6], results[7], _updateflag);

        document.getElementById('against-invoice').innerHTML = thousands_separators(results[8]);
        document.getElementById('refund-invoice-count').innerHTML = results[9];
        drawRefundInvoice(results[10], results[11], _updateflag);

        document.getElementById('net-invoice-amount').innerHTML = thousands_separators(results[12]);
        // document.getElementById('net-invoice-margin').innerHTML = results[13];
        drawNetInvoices(results[14], results[15], _updateflag);


    });
}

function salesMonthly() {

    //Show loader
    $('.loading-mask').show();

    var ourDate = new Date();
    ourDate.setDate(ourDate.getDate() - 30);

    var x = formatDate(new Date());
    var y = formatDate(ourDate);
    var z = 'monthly';
    _updateflag = true;

    new s.web.Model("retail.dashboard").call("retail_today_to_monthly_sales_trending",
        ([x, y, z])).then(function (results) {

        //console.log(results);

        _labels = results[2];
        _values = results[3];

        document.getElementById('total-sales').innerHTML = thousands_separators(results[0]);
        document.getElementById('total-sales-count').innerHTML = results[1];
        generatesalescollectionoverviewbarchart(_labels, _values, _updateflag);

        document.getElementById('total-amount').innerHTML = thousands_separators(results[4]);
        document.getElementById('invoice-count').innerHTML = results[5];
        drawChartInvoice(results[6], results[7], _updateflag);

        document.getElementById('against-invoice').innerHTML = thousands_separators(results[8]);
        document.getElementById('refund-invoice-count').innerHTML = results[9];
        drawRefundInvoice(results[10], results[11], _updateflag);

        document.getElementById('net-invoice-amount').innerHTML = thousands_separators(results[12]);
        // document.getElementById('net-invoice-margin').innerHTML = results[13];
        drawNetInvoices(results[14], results[15], _updateflag);


    });
}


function momValue() {

    //Show loader
    $('.loading-mask').show();

    var x = formatDate(new Date());
    var y = formatDate(new Date());
    var z = 'monvalue';
    _updateflag = true;

    new s.web.Model("retail.dashboard").call("monthon_month_value",
        ([x, y, z])).then(function (results) {

        //console.log(results);

        // month on month
        drawMonthOnMonthSales(results[0], results[1], _updateflag);

        drawMonthOnMonthInvoice(results[0], results[2], _updateflag);

        drawMonthOnMonthRefund(results[0], results[3], _updateflag);

        drawMonthOnMonthNetInvoice(results[0], results[4], _updateflag);


    });

}

function momCount() {

    //Show loader
    $('.loading-mask').show();

    var x = formatDate(new Date());
    var y = formatDate(new Date());
    var z = 'moncount';
    _updateflag = true;

    new s.web.Model("retail.dashboard").call("monthon_month_count",
        ([x, y, z])).then(function (results) {

        console.log(results);


        // month on month
        drawMonthOnMonthSales(results[0], results[1], _updateflag);

        drawMonthOnMonthInvoice(results[0], results[2], _updateflag);

        drawMonthOnMonthRefund(results[0], results[3], _updateflag);

        drawMonthOnMonthNetInvoice(results[0], results[4], _updateflag);


    });
}

function momPos() {

    //Show loader
    $('.loading-mask').show();

    var x = formatDate(new Date());
    var y = formatDate(new Date());
    var z = 'moncount';
    _updateflag = true;

    new s.web.Model("retail.dashboard").call("monthon_month_pos",
        ([x, y, z])).then(function (results) {

        //console.log(results);


        // month on month
        drawMonthOnMonthSales(results[0], results[1], _updateflag);

        drawMonthOnMonthInvoice(results[0], results[2], _updateflag);

        drawMonthOnMonthRefund(results[0], results[3], _updateflag);

        drawMonthOnMonthNetInvoice(results[0], results[4], _updateflag);


    });
}


// top10-skus-delivery BTN onclick

function top10Skusdelivery() {


    var x = formatDate(new Date());
    var y = formatDate(new Date());
    var z = categorySelect.value === null ? 'top10delivery' : categorySelect.value;
    var q = productcategorySelect.value === null ? null : productcategorySelect.value;

    _updateflag = true;
    var fromDate = $("#from-date").val();
    var toDate = $("#to-date").val();

    if (fromDate) {
        x = fromDate;
    }
    if (toDate) {
        y = toDate;
    }

    new s.web.Model("retail.dashboard").call("topTenSkusdelivery",
        ([x, y, z, q])).then(function (results) {



        // top ten skus return
        drawTopTenSkus(results[0], results[1], 'Delivered', _updateflag);


        //Hide loader
        $('.loading-mask').hide();
    });
}


// top10-skus-count BTN onclick

function top10SKUCount() {

    var x = formatDate(new Date());
    var y = formatDate(new Date());
    var z = categorySelect.value === null ? 'top10count' : categorySelect.value;
    var q = productcategorySelect.value === null ? null : productcategorySelect.value;


    _updateflag = true;
    var fromDate = $("#from-date").val();
    var toDate = $("#to-date").val();

    if (fromDate) {
        x = fromDate;
    }
    if (toDate) {
        y = toDate;
    }

    new s.web.Model("retail.dashboard").call("topTenSkuscount",
        ([x, y, z, q])).then(function (results) {



        // top ten skus return
        drawTopTenSkus(results[0], results[1], 'Delivered Count', _updateflag);


        //Hide loader
        $('.loading-mask').hide();
    });
}

// top10-skus-order BTN onclick

function topTenSkusOrder() {

    var x = formatDate(new Date());
    var y = formatDate(new Date());
    //var z = 'top10order';
    var z = categorySelect.value === null ? 'top10order' : categorySelect.value;
    //var z = categorySelect.value;
    var q = productcategorySelect.value === null ? null : productcategorySelect.value;


    _updateflag = true;
    var fromDate = $("#from-date").val();
    var toDate = $("#to-date").val();

    if (fromDate) {
        x = fromDate;
    }
    if (toDate) {
        y = toDate;
    }

    new s.web.Model("retail.dashboard").call("topTenSkusorder",
        ([x, y, z, q])).then(function (results) {



        // top ten skus return
        drawTopTenSkus(results[0], results[1], 'Order', _updateflag);


        //Hide loader
        $('.loading-mask').hide();
    });
}


// top10-skus-return BTN onclick

function top10SKUReturn() {

    var x = formatDate(new Date());
    var y = formatDate(new Date());
    var z = categorySelect.value === null ? 'top10return' : categorySelect.value;
    var q = productcategorySelect.value === null ? null : productcategorySelect.value;

    _updateflag = true;
    var fromDate = $("#from-date").val();
    var toDate = $("#to-date").val();

    if (fromDate) {
        x = fromDate;
    }
    if (toDate) {
        y = toDate;
    }

    new s.web.Model("retail.dashboard").call("topTenSkusReturn",
        ([x, y, z, q])).then(function (results) {

        // console.log(z, results);

        // top ten skus return
        drawTopTenSkus(results[0], results[1], 'Returned', _updateflag);

    });
}


//-------------------------------------
//-------------------------------------
//-------------------------------------

//order-trend30days

function orderTrendRmWise(_labels, _val1, _val2, _updateflag) {

    var options = {
        series: [{
            name: 'Order Value',
            type: 'column',
            data: _val1
        }, {
            name: 'Order Count',
            type: 'line',
            data: _val2
        }],
        chart: {
            height: 350,
            type: 'line',
            stacked: false
        },
        dataLabels: {
            enabled: false
        },
        stroke: {
            width: [5, 5],
            curve: 'smooth'
        },
        xaxis: {
            labels: {
                show: true,
                rotate: -90,
                style: {
                    colors: "black"
                }
            },
            categories: _labels
        },
        yaxis: [
            {
                axisTicks: {
                    show: true
                },
                axisBorder: {
                    show: true,
                    color: '#008FFB'
                },
                labels: {
                    style: {
                        colors: '#008FFB'
                    },
                    formatter: function (value) {
                        var amount = parseInt(value);

                        return thousands_separators(amount);
                    }
                },
                title: {
                    text: "Sales",
                    style: {
                        color: '#008FFB'
                    }
                },
                tooltip: {
                    enabled: true
                },
                show: true,
                decimalsInFloat: 2

            },
            {
                seriesName: 'Income',
                opposite: true,
                axisTicks: {
                    show: true
                },
                axisBorder: {
                    show: true,
                    color: '#00E396'
                },
                labels: {
                    style: {
                        colors: '#00E396'
                    },
                    formatter: function (value) {
                        var amount = parseInt(value);

                        return thousands_separators(amount);
                    }
                },

            },
            {
                seriesName: 'Revenue',
                opposite: true,
                axisTicks: {
                    show: true
                },
                axisBorder: {
                    show: true,
                    color: '#FEB019'
                },
                labels: {
                    style: {
                        colors: '#FEB019'
                    },
                    formatter: function (value) {
                        var amount = parseInt(value);

                        //return thousands_separators(amount);
                        return value;
                    }
                },

                show: false,
                decimalsInFloat: 2
            }
        ],
        tooltip: {
            enabled: true
        },
        legend: {
            horizontalAlign: 'left',
            offsetX: 40
        }
    };


    if (_updateflag === true) {
        order_trend_rm_wise_chart.updateOptions({
            series: [{
                name: 'Order Value',
                type: 'column',
                data: _val1
            }, {
                name: 'Order Count',
                type: 'line',
                data: _val2
            }],
            xaxis: {
                labels: {
                    show: true,
                    rotate: -90,
                    style: {
                        colors: "black"
                    }
                },
                categories: _labels
            },

        });
    } else {
        order_trend_rm_wise_chart = new ApexCharts(document.querySelector("#order-trend"), options);
        order_trend_rm_wise_chart.render();
    }

    //Hide loader
    $('.loading-mask').hide();

}


function customerGroupIndex(cgLabels, cgValue, cgTag) {
    //console.log(cgTag);
    // cgValue
    // ["Ananta", "Corporate", "Retail","General"];

    var sorted_cg_label = [];
    var sorted_cg_value = [];
    if (cgLabels.length > 0 && cgValue.length > 0) {
        cgLabels = Object.values(cgLabels);
        cgValue = Object.values(cgValue);

        if (cgTag === "sales") {

            //console.log(cgLabels, "sales_label");
            //console.log(cgValue, "sales_value");
            //cgValue[2] ? parseInt(cgValue[2]) : 0;

            for (i = 0; i < cgLabels.length; i++) {
                if (cgLabels[i] === 'Grocery') {
                    sorted_cg_label[0] = 'Grocery';
                    sorted_cg_value[0] = cgValue[i];
                } else if (cgLabels[i] === 'Stationary') {
                    sorted_cg_label[1] = 'Stationary';
                    sorted_cg_value[1] = cgValue[i];
                } else if (cgLabels[i] === 'Electronics') {
                    sorted_cg_label[2] = 'Electronics';
                    sorted_cg_value[2] = cgValue[i];
                } else if (cgLabels[i] === 'Mobile Accessories') {
                    sorted_cg_label[3] = 'Mobile Accessories';
                    sorted_cg_value[3] = cgValue[i];
                } else if (cgLabels[i] === 'Others') {
                    sorted_cg_label[4] = 'Others';
                    sorted_cg_value[4] = cgValue[i];
                }
                else if (cgLabels[i] === 'Industrial & Chemical') {
                    sorted_cg_label[5] = 'Industrial & Chemical';
                    sorted_cg_value[5] = cgValue[i];
                }
            }

            if (sorted_cg_label.indexOf("Grocery") === -1) {
                sorted_cg_label[0] = 'Grocery';
                sorted_cg_value[0] = 0;
            }
            if (sorted_cg_label.indexOf("Stationary") === -1) {
                sorted_cg_label[1] = 'Stationary';
                sorted_cg_value[1] = 0;
            }
            if (sorted_cg_label.indexOf("Electronics") === -1) {
                sorted_cg_label[2] = 'Electronics';
                sorted_cg_value[2] = 0;
            }
            if (sorted_cg_label.indexOf("Mobile Accessories") === -1) {
                sorted_cg_label[3] = 'Mobile Accessories';
                sorted_cg_value[3] = 0;
            }
            if (sorted_cg_label.indexOf("Others") === -1) {
                sorted_cg_label[4] = 'Others';
                sorted_cg_value[4] = 0;
            }

            if (sorted_cg_label.indexOf("Industrial & Chemical") === -1) {
                sorted_cg_label[5] = 'Industrial & Chemical';
                sorted_cg_value[5] = 0;
            }

            /*
            sorted_cg_value.push(cgValue[2] ? parseInt(cgValue[2]) : 0);
            sorted_cg_value.push(cgValue[1] ? parseInt(cgValue[1]) : 0);
            sorted_cg_value.push(cgValue[0] ? parseInt(cgValue[0]) : 0);
            sorted_cg_value.push(cgValue[3] ? parseInt(cgValue[3]) : 0);

            sorted_cg_label.push(cgLabels[2] ? cgLabels[2] : 'Ananta');
            sorted_cg_label.push(cgLabels[1] ? cgLabels[1] : 'Corporate');
            sorted_cg_label.push(cgLabels[0] ? cgLabels[0] : 'Retail');
            sorted_cg_label.push(cgLabels[3] ? cgLabels[3] : 'General');
            */


        } else if (cgTag === "invoice") {

            for (i = 0; i < cgLabels.length; i++) {
                if (cgLabels[i] === 'Grocery') {
                    sorted_cg_label[0] = 'Grocery';
                    sorted_cg_value[0] = cgValue[i];
                } else if (cgLabels[i] === 'Stationary') {
                    sorted_cg_label[1] = 'Stationary';
                    sorted_cg_value[1] = cgValue[i];
                } else if (cgLabels[i] === 'Electronics') {
                    sorted_cg_label[2] = 'Electronics';
                    sorted_cg_value[2] = cgValue[i];
                } else if (cgLabels[i] === 'Mobile Accessories') {
                    sorted_cg_label[3] = 'Mobile Accessories';
                    sorted_cg_value[3] = cgValue[i];
                } else if (cgLabels[i] === 'Others') {
                    sorted_cg_label[4] = 'Others';
                    sorted_cg_value[4] = cgValue[i];
                } else if (cgLabels[i] === 'Industrial & Chemical') {
                    sorted_cg_label[5] = 'Industrial & Chemical';
                    sorted_cg_value[5] = cgValue[i];
                }
            }

            if (sorted_cg_label.indexOf("Grocery") === -1) {
                sorted_cg_label[0] = 'Grocery';
                sorted_cg_value[0] = 0;
            }
            if (sorted_cg_label.indexOf("Stationary") === -1) {
                sorted_cg_label[1] = 'Stationary';
                sorted_cg_value[1] = 0;
            }
            if (sorted_cg_label.indexOf("Electronics") === -1) {
                sorted_cg_label[2] = 'Electronics';
                sorted_cg_value[2] = 0;
            }
            if (sorted_cg_label.indexOf("Mobile Accessories") === -1) {
                sorted_cg_label[3] = 'Mobile Accessories';
                sorted_cg_value[3] = 0;
            }
            if (sorted_cg_label.indexOf("Others") === -1) {
                sorted_cg_label[4] = 'Others';
                sorted_cg_value[4] = 0;
            }

            if (sorted_cg_label.indexOf("Industrial & Chemical") === -1) {
                sorted_cg_label[5] = 'Industrial & Chemical';
                sorted_cg_value[5] = 0;
            }

            /*
            sorted_cg_value.push(parseInt(cgValue[3]));
            sorted_cg_value.push(parseInt(cgValue[2]));
            sorted_cg_value.push(parseInt(cgValue[1]));
            sorted_cg_value.push(parseInt(cgValue[0]));

            sorted_cg_label.push(cgLabels[3]);
            sorted_cg_label.push(cgLabels[2]);
            sorted_cg_label.push(cgLabels[1]);
            sorted_cg_label.push(cgLabels[0]);
            */

        } else if (cgTag === "refund") {

            for (i = 0; i < cgLabels.length; i++) {
                if (cgLabels[i] === 'Grocery') {
                    sorted_cg_label[0] = 'Grocery';
                    sorted_cg_value[0] = cgValue[i];
                } else if (cgLabels[i] === 'Stationary') {
                    sorted_cg_label[1] = 'Stationary';
                    sorted_cg_value[1] = cgValue[i];
                } else if (cgLabels[i] === 'Electronics') {
                    sorted_cg_label[2] = 'Electronics';
                    sorted_cg_value[2] = cgValue[i];
                } else if (cgLabels[i] === 'Mobile Accessories') {
                    sorted_cg_label[3] = 'Mobile Accessories';
                    sorted_cg_value[3] = cgValue[i];
                } else if (cgLabels[i] === 'Others') {
                    sorted_cg_label[4] = 'Others';
                    sorted_cg_value[4] = cgValue[i];
                } else if (cgLabels[i] === 'Industrial & Chemical') {
                    sorted_cg_label[5] = 'Industrial & Chemical';
                    sorted_cg_value[5] = cgValue[i];
                }
            }

            if (sorted_cg_label.indexOf("Grocery") === -1) {
                sorted_cg_label[0] = 'Grocery';
                sorted_cg_value[0] = 0;
            }
            if (sorted_cg_label.indexOf("Stationary") === -1) {
                sorted_cg_label[1] = 'Stationary';
                sorted_cg_value[1] = 0;
            }
            if (sorted_cg_label.indexOf("Electronics") === -1) {
                sorted_cg_label[2] = 'Electronics';
                sorted_cg_value[2] = 0;
            }
            if (sorted_cg_label.indexOf("Mobile Accessories") === -1) {
                sorted_cg_label[3] = 'Mobile Accessories';
                sorted_cg_value[3] = 0;
            }
            if (sorted_cg_label.indexOf("Others") === -1) {
                sorted_cg_label[4] = 'Others';
                sorted_cg_value[4] = 0;
            }
            if (sorted_cg_label.indexOf("Industrial & Chemical") === -1) {
                sorted_cg_label[5] = 'Industrial & Chemical';
                sorted_cg_value[5] = 0;
            }

            /*
            sorted_cg_value.push(parseInt(cgValue[3]));
            sorted_cg_value.push(parseInt(cgValue[2]));
            sorted_cg_value.push(parseInt(cgValue[1]));
            sorted_cg_value.push(parseInt(cgValue[0]));

            sorted_cg_label.push(cgLabels[3]);
            sorted_cg_label.push(cgLabels[2]);
            sorted_cg_label.push(cgLabels[1]);
            sorted_cg_label.push(cgLabels[0]);
            */

        } else if (cgTag === "netinvoices") {

            for (i = 0; i < cgLabels.length; i++) {
                if (cgLabels[i] === 'Grocery') {
                    sorted_cg_label[0] = 'Grocery';
                    sorted_cg_value[0] = cgValue[i];
                } else if (cgLabels[i] === 'Stationary') {
                    sorted_cg_label[1] = 'Stationary';
                    sorted_cg_value[1] = cgValue[i];
                } else if (cgLabels[i] === 'Electronics') {
                    sorted_cg_label[2] = 'Electronics';
                    sorted_cg_value[2] = cgValue[i];
                } else if (cgLabels[i] === 'Mobile Accessories') {
                    sorted_cg_label[3] = 'Mobile Accessories';
                    sorted_cg_value[3] = cgValue[i];
                } else if (cgLabels[i] === 'Others') {
                    sorted_cg_label[4] = 'Others';
                    sorted_cg_value[4] = cgValue[i];
                } else if (cgLabels[i] === 'Industrial & Chemical') {
                    sorted_cg_label[5] = 'Industrial & Chemical';
                    sorted_cg_value[5] = cgValue[i];
                }
            }

            if (sorted_cg_label.indexOf("Grocery") === -1) {
                sorted_cg_label[0] = 'Grocery';
                sorted_cg_value[0] = 0;
            }
            if (sorted_cg_label.indexOf("Stationary") === -1) {
                sorted_cg_label[1] = 'Stationary';
                sorted_cg_value[1] = 0;
            }
            if (sorted_cg_label.indexOf("Electronics") === -1) {
                sorted_cg_label[2] = 'Electronics';
                sorted_cg_value[2] = 0;
            }
            if (sorted_cg_label.indexOf("Mobile Accessories") === -1) {
                sorted_cg_label[3] = 'Mobile Accessories';
                sorted_cg_value[3] = 0;
            }
            if (sorted_cg_label.indexOf("Others") === -1) {
                sorted_cg_label[4] = 'Others';
                sorted_cg_value[4] = 0;
            }
            if (sorted_cg_label.indexOf("Industrial & Chemical") === -1) {
                sorted_cg_label[5] = 'Industrial & Chemical';
                sorted_cg_value[5] = 0;
            }

            /*
            sorted_cg_value.push(parseInt(cgValue[3]));
            sorted_cg_value.push(parseInt(cgValue[2]));
            sorted_cg_value.push(parseInt(cgValue[1]));
            sorted_cg_value.push(parseInt(cgValue[0]));

            sorted_cg_label.push(cgLabels[3]);
            sorted_cg_label.push(cgLabels[2]);
            sorted_cg_label.push(cgLabels[1]);
            sorted_cg_label.push(cgLabels[0]);
            */

        } else if (cgTag === "classificationwise") {

            for (i = 0; i < cgLabels.length; i++) {
                if (cgLabels[i] === 'Grocery') {
                    sorted_cg_label[0] = 'Grocery';
                    sorted_cg_value[0] = cgValue[i];
                } else if (cgLabels[i] === 'Stationary') {
                    sorted_cg_label[1] = 'Stationary';
                    sorted_cg_value[1] = cgValue[i];
                } else if (cgLabels[i] === 'Electronics') {
                    sorted_cg_label[2] = 'Electronics';
                    sorted_cg_value[2] = cgValue[i];
                } else if (cgLabels[i] === 'Mobile Accessories') {
                    sorted_cg_label[3] = 'Mobile Accessories';
                    sorted_cg_value[3] = cgValue[i];
                } else if (cgLabels[i] === 'Others') {
                    sorted_cg_label[4] = 'Others';
                    sorted_cg_value[4] = cgValue[i];
                } else if (cgLabels[i] === 'Industrial & Chemical') {
                    sorted_cg_label[5] = 'Industrial & Chemical';
                    sorted_cg_value[5] = cgValue[i];
                }
            }

            if (sorted_cg_label.indexOf("Grocery") === -1) {
                sorted_cg_label[0] = 'Grocery';
                sorted_cg_value[0] = 0;
            }
            if (sorted_cg_label.indexOf("Stationary") === -1) {
                sorted_cg_label[1] = 'Stationary';
                sorted_cg_value[1] = 0;
            }
            if (sorted_cg_label.indexOf("Electronics") === -1) {
                sorted_cg_label[2] = 'Electronics';
                sorted_cg_value[2] = 0;
            }
            if (sorted_cg_label.indexOf("Mobile Accessories") === -1) {
                sorted_cg_label[3] = 'Mobile Accessories';
                sorted_cg_value[3] = 0;
            }
            if (sorted_cg_label.indexOf("Others") === -1) {
                sorted_cg_label[4] = 'Others';
                sorted_cg_value[4] = 0;
            }

            if (sorted_cg_label.indexOf("Industrial & Chemical") === -1) {
                sorted_cg_label[5] = 'Industrial & Chemical';
                sorted_cg_value[5] = 0;
            }

            /*
            sorted_cg_value.push(parseInt(cgValue[3]));
            sorted_cg_value.push(parseInt(cgValue[2]));
            sorted_cg_value.push(parseInt(cgValue[1]));
            sorted_cg_value.push(parseInt(cgValue[0]));

            sorted_cg_label.push(cgLabels[3]);
            sorted_cg_label.push(cgLabels[2]);
            sorted_cg_label.push(cgLabels[1]);
            sorted_cg_label.push(cgLabels[0]);
            */

        }
    }
    else {
        sorted_cg_label = cgLabels;
        sorted_cg_value = cgValue;

    }

    //console.log(sorted_cg_label);
    //console.log(sorted_cg_value);

    return [sorted_cg_label, sorted_cg_value]
}

// Search By Date

$('#search-by-date').click(function (e) {
    e.preventDefault();

    //Show loader search
    //$('.loading-mask').show();

    var fromDate = $("#from-date").val();
    var toDate = $("#to-date").val();

    //console.log(fromDate);
    //console.log(toDate);

    if (fromDate === '' || toDate === '') {

        alert("Please give From Date and To Date both!");

    } else {
        //Show loader
        //$('.loading-mask').show();
        var y = toDate;
        var x = fromDate;
        var z = classification === null ? 'top10delivery' : classification;
        _updateflag = true;

        // Sales Invoices Refund Net Invoices Block
        new s.web.Model("retail.dashboard").call("retail_today_to_monthly_sales_trending",
            ([y,x, z])).then(function (results) {

            document.getElementById('total-sales').innerHTML = thousands_separators(results[0]);
            document.getElementById('total-sales-count').innerHTML = results[1];
            generatesalescollectionoverviewbarchart(results[2], results[3], _updateflag);

            document.getElementById('total-amount').innerHTML = thousands_separators(results[4]);
            document.getElementById('invoice-count').innerHTML = results[5];
            drawChartInvoice(results[6], results[7], _updateflag);

            document.getElementById('against-invoice').innerHTML = thousands_separators(parseInt(results[8]));
            document.getElementById('refund-invoice-count').innerHTML = results[9];
            drawRefundInvoice(results[10], results[11], _updateflag);


            document.getElementById('net-invoice-amount').innerHTML = thousands_separators(results[12]);
            // document.getElementById('net-invoice-margin').innerHTML = results[13];
            drawNetInvoices(results[14], results[15], _updateflag);

        });

// Net Invoice and Margin Block

        new s.web.Model("retail.dashboard").call("retail_dashboard_data_preparation",
            ([x, y, z])).then(function (results) {

            drawCategoriwiseSales(results[0], results[1], results[2], results[3], _updateflag);
            drawSalesMarginDonut(results[6], results[7], _updateflag);
        });

// Top 10 SKUs Block

        new s.web.Model("retail.dashboard").call("topTenSkusdelivery",
            ([x, y, z])).then(function (results) {
            drawTopTenSkus(results[0], results[1], 'Delivered', _updateflag);

        });
        

        // Old Searc by date click  start

        /*        new s.web.Model("retail.dashboard").call("retail_dashboard_data_preparation",
            ([x, y, z])).then(function (results) {



                //console.log(results);

                _labels = results[2];
                _values = results[3];

                document.getElementById('total-sales').innerHTML = thousands_separators(results[0]);
                document.getElementById('total-sales-count').innerHTML = results[1];
                generatesalescollectionoverviewbarchart(results[2], results[3], _updateflag);

                document.getElementById('total-amount').innerHTML = thousands_separators(results[4]);
                document.getElementById('invoice-count').innerHTML = results[5];
                drawChartInvoice(results[6], results[7], _updateflag);

                document.getElementById('against-invoice').innerHTML = thousands_separators(parseInt(results[8]));
                document.getElementById('refund-invoice-count').innerHTML = results[9];
                drawRefundInvoice(results[10], results[11], _updateflag);

                document.getElementById('net-invoice-amount').innerHTML = thousands_separators(results[12]);
                // document.getElementById('net-invoice-margin').innerHTML = results[13];
                drawNetInvoices(results[14], results[15], _updateflag);
                drawCategoriwiseSales(results[16], results[17], results[18], results[19], _updateflag);


                //document.getElementById('total-margin-amount').innerHTML = thousands_separators(results[35]);
                //document.getElementById('total-margin-amount-percent').innerHTML = results[36];
                // drawSalesMarginDonut(results[22], results[23], _updateflag);

                // new net-invoice-and-margin-classification-chart-two
                 //drawSalesMarginDonutNew(results[38], results[39],results[37], _updateflag);

                drawSalesMarginDonut(results[22], results[23], _updateflag);
                //drawTurnOverTimeStacked(results[24], results[25], results[26], _updateflag);

                // month on month
                drawMonthOnMonthSales(results[27], results[28], _updateflag);

                drawMonthOnMonthInvoice(results[27], results[29], _updateflag);

                drawMonthOnMonthRefund(results[27], results[30], _updateflag);

                drawMonthOnMonthNetInvoice(results[27], results[31], _updateflag);


                // top 10 skus
                drawTopTenSkus(results[40], results[41],'Delivered', _updateflag);

                // customer base

                //uniquePosChart(results[27], results[42], _updateflag);
                //table_data(results[27], results[42],'unique-pos-table', _updateflag);

                //activePosChart(results[27], results[43], _updateflag);
                //table_data(results[27], results[43],'active-pos-table', _updateflag);

                //newPosChart(results[27], results[44], _updateflag);
                //table_data(results[27], results[44],'new-pos-table', _updateflag);


                //registeredPosChart(results[27], results[45], _updateflag);
                //table_data(results[27], results[45],'registered-pos-table', _updateflag);

                //Hide loader
                $('.loading-mask').hide();

             }); */
        // Old Searc by date click  End

    }

});


//full page Show loader
//$('.loading-mask').show();

x = start_date === null ? formatDate(new Date()) : start_date;
y = end_date === null ? x : end_date;
z = classification === null ? 'all' : classification;
q = product_category === null ? null : product_category;

/*new s.web.Model("retail.dashboard").call("retail_dashboard_data_preparation",
    ([x, y, z])).then(function (results) {

    //console.log(results);

    _labels = results[2];
    _values = results[3];

    document.getElementById('total-sales').innerHTML = thousands_separators(results[0]);
    document.getElementById('total-sales-count').innerHTML = results[1];
    generatesalescollectionoverviewbarchart(results[2], results[3], _updateflag);

    document.getElementById('total-amount').innerHTML = thousands_separators(results[4]);
    document.getElementById('invoice-count').innerHTML = results[5];
    drawChartInvoice(results[6], results[7], _updateflag);

    document.getElementById('against-invoice').innerHTML = thousands_separators(parseInt(results[8]));
    document.getElementById('refund-invoice-count').innerHTML = results[9];
    drawRefundInvoice(results[10], results[11], _updateflag);

    document.getElementById('net-invoice-amount').innerHTML = thousands_separators(results[12]);
    // document.getElementById('net-invoice-margin').innerHTML = results[13];
    drawNetInvoices(results[14], results[15], _updateflag);
    drawCategoriwiseSales(results[16], results[17], results[18], results[19], _updateflag);


    //document.getElementById('total-margin-amount').innerHTML = thousands_separators(results[35]);
    //document.getElementById('total-margin-amount-percent').innerHTML = results[36];
    // drawSalesMarginDonut(results[22], results[23], _updateflag);

    // new net-invoice-and-margin-classification-chart-two
     //drawSalesMarginDonutNew(results[38], results[39],results[37], _updateflag);

    drawSalesMarginDonut(results[22], results[23], _updateflag);
    //drawTurnOverTimeStacked(results[24], results[25], results[26], _updateflag);

    // month on month
    drawMonthOnMonthSales(results[27], results[28], _updateflag);

    drawMonthOnMonthInvoice(results[27], results[29], _updateflag);

    drawMonthOnMonthRefund(results[27], results[30], _updateflag);

    drawMonthOnMonthNetInvoice(results[27], results[31], _updateflag);


    // top 10 skus
    drawTopTenSkus(results[40], results[41], 'Delivered');

    // customer base

        uniquePosChart(results[27], results[42], _updateflag);
        table_data(results[27], results[42],'unique-pos-table', _updateflag);

        activePosChart(results[27], results[43], _updateflag);
        table_data(results[27], results[43],'active-pos-table', _updateflag);

        newPosChart(results[27], results[44], _updateflag);
        table_data(results[27], results[44],'new-pos-table', _updateflag);

        registeredPosChart(results[27], results[45], _updateflag);
        table_data(results[27], results[45],'registered-pos-table', _updateflag);

    //Hide loader
    $('.loading-mask').hide();

}); */

function generatesalescollectionoverviewbarchart(_labels, _values, _updateflag) {
    //Bar Charts >chart-sales

    var arranged_sales = customerGroupIndex(_labels, _values, 'sales');
    _labels = arranged_sales[0];

    _values = arranged_sales[1];

    var options = {
        series: _values,
        labels: _labels,
        chart: {
            type: 'donut',
            height: 250//_labels.length > 1 ? 250 : 220
        },
        legend: {
            position: 'bottom'
        },
        fill: {
            type: 'gradient',
        },
        tooltip: {
            enabled: true,
            dataLabels: {
                enabled: true,
                title: {
                    formatter: function (_values) {
                        return _values
                    }
                },
            },
        },
        responsive: [{
            breakpoint: 480,
            options: {
                legend: {
                    position: 'bottom'
                }
            }
        }],
        yaxis: {
            show: true,
            decimalsInFloat: 2,
            labels: {
                formatter: function (value) {
                    var amount = parseInt(value);

                    return thousands_separators(amount);
                }
            }
        },
        colors: colorCategory

    };

    //var sale_chart = new ApexCharts(document.querySelector("#chart-sales"), options);
    // chart.render();
    //var sale_chart;


    if (_updateflag === false) {
        sale_chart = new ApexCharts(document.querySelector("#chart-sales"), options);
        sale_chart.render();

    } else {
        //console.log("This is initialized");
        sale_chart.updateOptions({
            series: _values,
            labels: _labels

        });
    }

    //Hide loader
    $('.loading-mask').hide();

}

function drawChartInvoice(_labels, _values, _updateflag) {

    //Bar Charts >chart-invoices
    var arranged_invoice = customerGroupIndex(_labels, _values, 'invoice');
    _labels = arranged_invoice[0];
    _values = arranged_invoice[1];

    var options = {
        series: _values,
        labels: _labels,
        chart: {
            type: 'donut',
            height: 250//_labels.length > 1 ? 250 : 220
        },
        legend: {
            position: 'bottom'
        },
        fill: {
            type: 'gradient',
        },

        responsive: [{
            breakpoint: 480,
            options: {
                legend: {
                    position: 'bottom'
                }
            }
        }],
        // colors: ['#038ffa', '#53e396', '#fdb13b', '#f9475f'],
        yaxis: {
            show: true,
            decimalsInFloat: 2,
            labels: {
                formatter: function (value) {
                    var amount = parseInt(value);

                    return thousands_separators(amount);
                }
            }
        },
        colors: colorCategory
    };

    // var sale_inv_chart = new ApexCharts(document.querySelector("#chart-invoices"), options);
    // chart.render();
    if (_updateflag === false) {

        sale_inv_chart = new ApexCharts(document.querySelector("#chart-invoices"), options);
        sale_inv_chart.render();

    } else {
        //console.log("This is initialized");
        sale_inv_chart.updateOptions({
            series: _values,
            labels: _labels

        });
    }
    //Hide loader
    $('.loading-mask').hide();

}

function drawRefundInvoice(_labels, _values, _updateflag) {
    //Bar Charts >chart-refund
    var arranged_refund = customerGroupIndex(_labels, _values, 'refund');
    _labels = arranged_refund[0];
    _values = arranged_refund[1];
    var options = {
        series: _values,
        labels: _labels,
        chart: {
            type: 'donut',
            height: 250//_labels.length > 1 ? 250 : 220
        },
        legend: {
            position: 'bottom'
        },
        fill: {
            type: 'gradient',
        },
        responsive: [{
            breakpoint: 480,
            options: {
                legend: {
                    position: 'bottom'
                }
            }
        }],
        // colors: ['#038ffa', '#53e396', '#fdb13b', '#f9475f'],
        yaxis: {
            show: true,
            decimalsInFloat: 2,
            labels: {
                formatter: function (value) {
                    var amount = parseInt(value);

                    return thousands_separators(amount);
                }
            }
        },
        colors: colorCategory
    };

    //var sale_refund_chart = new ApexCharts(document.querySelector("#chart-refund"), options);
    // chart.render();
    if (_updateflag === false) {
        sale_refund_chart = new ApexCharts(document.querySelector("#chart-refund"), options);
        sale_refund_chart.render();

    } else {
        //console.log("This is initialized");
        sale_refund_chart.updateOptions({
            series: _values,
            labels: _labels

        });
    }
    //Hide loader
    $('.loading-mask').hide();
}

function drawNetInvoices(_labels, _values, _updateflag) {
    //Bar Charts >chart-netinvoices
    var arranged_refund = customerGroupIndex(_labels, _values, 'netinvoices');
    _labels = arranged_refund[0];
    _values = arranged_refund[1];
    var options = {
        series: _values,
        labels: _labels,
        chart: {
            type: 'donut',
            height: 250//_labels.length > 1 ? 250 : 220
        },
        legend: {
            position: 'bottom'
        },
        fill: {
            type: 'gradient',
        },
        responsive: [{
            breakpoint: 480,
            options: {
                legend: {
                    position: 'bottom'
                }
            }
        }],
        // colors: ['#038ffa', '#53e396', '#fdb13b', '#f9475f'],
        yaxis: {
            show: true,
            decimalsInFloat: 2,
            labels: {
                formatter: function (value) {
                    var amount = parseInt(value);

                    return thousands_separators(amount);
                }
            }
        },
        colors: colorCategory
    };

    //var sale_net_inv_chart = new ApexCharts(document.querySelector("#chart-netinvoices"), options);
    // chart.render();
    if (_updateflag === false) {
        sale_net_inv_chart = new ApexCharts(document.querySelector("#chart-netinvoices"), options);
        sale_net_inv_chart.render();

    } else {
        //console.log("This is initialized");
        sale_net_inv_chart.updateOptions({
            series: _values,
            labels: _labels

        });
    }
    //Hide loader
    $('.loading-mask').hide();
}

//=======Month On Month (Last 6 Months) = new drawMonthOnMonth all sales Invoice Refund Net Invoice

//============== new Monthly Tren (Last 6 Months) Working==========================
//======== new drawMonthOnMonthSales
function drawMonthOnMonthSales(_v1,_v2,_v3) {
    //testvalue = format_value_million(_v2);


    let series = _v2;
	let categories = _v1;
	let element = document.querySelector("#monthly-sales-chart");

	generateMonthSalesChart(series, categories, element)
}

//======== new drawMonthOnMonthInvoice
function drawMonthOnMonthInvoice(_v1,_v2,_v3) {
    //testvalue = format_value_million(_v2);


    let series = _v2;
	let categories = _v1;
	let element = document.querySelector("#monthly-invoice-chart");

	generateMonthSalesChart(series, categories, element)
}
//======== new drawMonthOnMonthRefund
function drawMonthOnMonthRefund(_v1,_v2,_v3) {
    //testvalue = format_value_million(_v2);


    let series = _v2;
	let categories = _v1;
	let element = document.querySelector("#monthly-refund-chart");

	generateMonthSalesChart(series, categories, element)
}

//======== new drawMonthOnMonthNetInvoice
function drawMonthOnMonthNetInvoice(_v1,_v2,_v3) {
    //testvalue = format_value_million(_v2);


    let series = _v2;
	let categories = _v1;
	let element = document.querySelector("#monthly-net-invoice-chart");

	generateMonthSalesChart(series, categories, element)
}

//======== new drawMonthOnMonth all sales Invoice Refund Net Invoice
function generateMonthSalesChart(series, categories, element) {
    let chart;
	const options = {
		series,
		chart: {
			type: "bar",
			height: 250,
			stacked: true,
			toolbar: {
				show: false
			},
			events: {
				mounted: (chartContext, config) => {
					//console.log("mounted", chartContext, config, config.globals.yRange);
					setTimeout(() => {
						addAnnotations(config);
					});
				},
				updated: (chartContext, config) => {
					setTimeout(() => {
						addAnnotations(config);
					});
				}
			}
		},
		dataLabels: {
			enabled: true,show: true,
            decimalsInFloat: 2,

             formatter: function (value) {
              var total;
             if(value > 1000000){
             total = parseInt(value / 1000000) + "M";
            }else {
            total = parseInt(value);
             }
             return total;
              }



		},
		plotOptions: {
			bar: {

				horizontal: false,
				dataLabels: {
					maxItems: 2
				}
			}
		},
		xaxis: {
			categories,

			axisTicks: {
				show: true
			},
			axisBorder: {
				show: true
			},

			labels: {
				hideOverlappingLabels: false,
			}
		},
		yaxis: {
			axisTicks: {
				show: true,

			},
			axisBorder: {
				show: true
			},


          show: true,
             decimalsInFloat: 2,
            labels: {
             formatter: function (value) {
              var total;
             if(value > 1000000){
             total = parseInt(value / 1000000) + "M";
            }else {
            total = parseInt(value);
             }
             return total;
              },
                hideOverlappingLabels: true,
                style: {
              fontSize: '12px',
         }
		},
            },
		fill: {
			opacity: 1
		},
        colors: colorCategory,
		legend: {
			position: "bottom",
			horizontalAlign: "left",
            offsetX: 40
		},
        tooltip: {
            y: {
                formatter: function (value) {
              var total;
             if(value > 1000000){
             total = parseInt(value / 1000000) + "M";
            }else {
            total = parseInt(value);
             }
             return total;
              }
            }
        },
		grid: {
			padding: {
				left: 13.5,
				right: 0
			},
			xaxis: {
				lines: {
					show: true
				}
			},
		}
	};
	const addAnnotations = (config) => {

		let seriesTotals = config.globals.stackedSeriesTotals;

		   // seriesTotals = seriesTotals.map(a =>format_value_million(a));


		const isHorizontal = options.plotOptions.bar.horizontal;
		chart.clearAnnotations();

		try {
			categories.forEach((category, index) => {
				chart.addPointAnnotation(
				{
					y: isHorizontal
					? calcHorizontalY(config, index)
					: seriesTotals[index],
					x: isHorizontal ? 0 : category,
					label: {
						text: format_value_million(`${seriesTotals[index]}`),
                        style: {
                        fontSize: "12px",
                        fontWeight: "normal"
                      }
					}
				},
				false
				);

				if (isHorizontal) {
					adjustPointAnnotationXCoord(config, index);
				}
			});
		} catch (error) {
			//console.log(`Add point annotation error: ${error.message}`);
		}
	};

	const calcHorizontalY = (config, index) => {
		const catLength = categories.length;
		const yRange = config.globals.yRange[0];
		const minY = config.globals.minY;
		const halfBarHeight = yRange / catLength / 2;

		return minY + halfBarHeight + 2 * halfBarHeight * (catLength - 1 - index);
	};

	const adjustPointAnnotationXCoord = (config, index) => {
			const gridWidth 	= config.globals.gridWidth;
			const seriesTotal 	= config.globals.stackedSeriesTotals[index];
			const minY 			= Math.abs(config.globals.minY);
			const yRange 		= config.globals.yRange[0];
			const xOffset 		= (gridWidth * (seriesTotal + minY)) / yRange;
			const chartId 		= config.globals.dom.baseEl.id;
			console.log(index)

			const circle 		= document.querySelector(
			       `#${chartId} .apexcharts-point-annotations circle:nth-of-type(${index + 1})`
			                                        );
			const labelField = document.querySelector(
			       `#${chartId} .apexcharts-point-annotations rect:nth-of-type(${index + 1}`
			                                          );
			const labelFieldXCoord = parseFloat(labelField.getAttribute("x"));
			const text = document.querySelector(
			       `#${chartId} .apexcharts-point-annotations text:nth-of-type(${index + 1}`
			                                    );

			labelField.setAttribute("x", labelFieldXCoord + xOffset);
			text.setAttribute("x", xOffset);
			circle.setAttribute("cx", xOffset);
		};

	    chart = new ApexCharts(element, options);
        chart.render();
    //Hide loader
    $('.loading-mask').hide();

}

//============== OLD Monthly Tren (Last 6 Months) Working==========================

//========================================
/*function drawMonthOnMonthSales(_labels, _values, _updateflag) {

    var options = {
        dataLabels: {
            enabled: true,
            formatter: function (val, opts) {
                //return (val / 1000000).toFixed(2) + "M"
                return " "
            }
        },
        series: _values,
        chart: {
            type: 'bar',
            height: 250,
            stacked: true,
            toolbar: {
                show: true
            },
            zoom: {
                enabled: true
            }
        },
        legend: {
            position: 'bottom'
        },
        responsive: [{
            breakpoint: 480,
            options: {
                legend: {
                    position: 'bottom',
                    offsetX: -10,
                    offsetY: 0
                }
            }
        }],
        plotOptions: {
            bar: {
                horizontal: false
            }
        },
        xaxis: {
            type: 'string',
            categories: _labels
        },
        yaxis: {
            show: true,
            decimalsInFloat: 2,
            labels: {
                formatter: function (value) {
                    var total;
                    if (value > 1000000) {
                        total = parseInt(value / 1000000) + "M";
                    } else {
                        total = parseInt(value);
                    }


                    return total;
                }
            }
        },
        fill: {
            opacity: 1
        },
        colors: colorCategory
    };

    //var mom_sale_chart = new ApexCharts(document.querySelector("#monthly-sales-chart"), options);
    // chart.render();
    if (_updateflag === true) {
        mom_sale_chart.updateOptions({
            series: _values,
            labels: _labels

        });
    } else {
        //console.log("This is initialized");
        mom_sale_chart = new ApexCharts(document.querySelector("#monthly-sales-chart"), options);
        mom_sale_chart.render();
    }
    //Hide loader
    $('.loading-mask').hide();
}

function drawMonthOnMonthInvoice(_labels, _values, _updateflag) {
    var options = {
        dataLabels: {
            enabled: true,
            formatter: function (val, opts) {
                //return (val / 1000000).toFixed(2) + "M"
                return " "
            }
        },
        series: _values,
        chart: {
            type: 'bar',
            height: 250,
            stacked: true,
            toolbar: {
                show: true
            },
            zoom: {
                enabled: true
            }
        },
        responsive: [{
            breakpoint: 480,
            options: {
                legend: {
                    position: 'bottom',
                    offsetX: -10,
                    offsetY: 0
                }
            }
        }],
        plotOptions: {
            bar: {
                horizontal: false
            }
        },
        xaxis: {
            type: 'string',
            categories: _labels
        },
        yaxis: {
            show: true,
            decimalsInFloat: 2,
            labels: {
                formatter: function (value) {
                    var total;
                    if (value > 1000000) {
                        total = parseInt(value / 1000000) + "M";
                    } else {
                        total = parseInt(value);
                    }


                    return total;
                }
            }
        },
        legend: {
            position: 'bottom'
        },
        fill: {
            opacity: 1
        },
        colors: colorCategory
    };

    //var mom_sale_inv_chart = new ApexCharts(document.querySelector("#monthly-invoice-chart"), options);
    // chart.render();
    if (_updateflag === true) {
        mom_sale_inv_chart.updateOptions({
            series: _values,
            labels: _labels

        });
    } else {
        //console.log("This is initialized");
        mom_sale_inv_chart = new ApexCharts(document.querySelector("#monthly-invoice-chart"), options);
        mom_sale_inv_chart.render();
    }

    //Hide loader
    $('.loading-mask').hide();
}

function drawMonthOnMonthRefund(_labels, _values, _updateflag) {
    var options = {
        dataLabels: {
            enabled: true,
            formatter: function (val, opts) {
                // return (val / 1000000).toFixed(2) + "M"
                return " "
            }
        },
        series: _values,
        chart: {
            type: 'bar',
            height: 250,
            stacked: true,
            toolbar: {
                show: true
            },
            zoom: {
                enabled: true
            }
        },
        responsive: [{
            breakpoint: 480,
            options: {
                legend: {
                    position: 'bottom',
                    offsetX: -10,
                    offsetY: 0
                }
            }
        }],
        plotOptions: {
            bar: {
                horizontal: false
            }
        },
        xaxis: {
            type: 'string',
            categories: _labels
        },
        yaxis: {
            show: true,
            decimalsInFloat: 2,
            labels: {
                formatter: function (value) {
                    var total;
                    if (value > 1000000) {
                        total = parseInt(value / 1000000) + "M";
                    } else {
                        total = parseInt(value);
                    }


                    return total;
                }
            }
        },
        legend: {
            position: 'bottom'
        },
        fill: {
            opacity: 1
        },
        colors: colorCategory
    };

    //var mom_sale_refund_chart = new ApexCharts(document.querySelector("#monthly-refund-chart"), options);
    // chart.render();
    if (_updateflag === true) {
        mom_sale_refund_chart.updateOptions({
            series: _values,
            labels: _labels

        });
    } else {
        //console.log("This is initialized");
        mom_sale_refund_chart = new ApexCharts(document.querySelector("#monthly-refund-chart"), options);
        mom_sale_refund_chart.render();
    }

    //Hide loader
    $('.loading-mask').hide();
}

function drawMonthOnMonthNetInvoice(_labels, _values, _updateflag) {
    var options = {
        dataLabels: {
            enabled: true,
            formatter: function (val, opts) {
                //return (val / 1000000).toFixed(2) + "M"
                return ""
            }
        },
        series: _values,
        chart: {
            type: 'bar',
            height: 250,
            stacked: true,
            toolbar: {
                show: true
            },
            zoom: {
                enabled: true
            }
        },
        responsive: [{
            breakpoint: 480,
            options: {
                legend: {
                    position: 'bottom',
                    offsetX: -10,
                    offsetY: 0
                }
            }
        }],
        plotOptions: {
            bar: {
                horizontal: false
            }
        },
        xaxis: {
            type: 'string',
            categories: _labels

        },
        yaxis: {
            show: true,
            decimalsInFloat: 2,
            labels: {
                formatter: function (value) {
                    var total;

                    if (value > 1000000) {
                        total = parseInt(value / 1000000) + "M";
                    } else {
                        total = parseInt(value);
                    }


                    return total;
                }
            }
        },
        legend: {
            position: 'bottom'
        },
        fill: {
            opacity: 1
        },
        colors: colorCategory
    };

    //var mom_sale_net_inv_chart = new ApexCharts(document.querySelector("#monthly-net-invoice-chart"), options);
    // chart.render();
    if (_updateflag === true) {
        mom_sale_net_inv_chart.updateOptions({
            series: _values,
            labels: _labels

        });
    } else {
        //console.log("This is initialized");
        mom_sale_net_inv_chart = new ApexCharts(document.querySelector("#monthly-net-invoice-chart"), options);
        mom_sale_net_inv_chart.render();
    }

    //Hide loader
    $('.loading-mask').hide();
}*/

//=================================================================

function drawCategoriwiseSales(_catglist, _costlist, _salelist, _marginlist, _updateflag) {
    //console.log(_catglist);

    _catglist = format_catetory_list(_catglist);

    var options = {
        series: [{
            name: 'Sales',
            type: 'column',
            data: _salelist
        }, {
            name: 'Cost',
            type: 'column',
            data: _costlist
        }, {
            name: 'Margin',
            type: 'line',
            data: _marginlist
        }],
        chart: {
            height: 350,
            type: 'line',
            stacked: false
        },
        dataLabels: {
            enabled: false
        },
        stroke: {
            width: [1, 1, 4],
            curve: 'smooth'
        },
        title: {
            text: 'Category Wise Sales & Margin ',
            align: 'left',
            offsetX: 110
        },
        xaxis: {
            categories: _catglist
        },
        yaxis: [
            {
                seriesName: 'Cost',
                show:false,
                labels: {
                    style: {
                        colors: '#00E396'
                    },
                    formatter: function (value) {
                        var amount = parseInt(value);

                        return thousands_separators(amount);
                    }
                }
            },
            {
                axisTicks: {
                    show: true
                },
                axisBorder: {
                    show: true,
                    color: '#008FFB'
                },
                labels: {
                    style: {
                        colors: '#008FFB'
                    },
                    formatter: function (value) {
                        var amount = parseInt(value);

                        return thousands_separators(amount);
                    }
                },
                title: {
                    text: "Sales / Cost",
                    style: {
                        color: '#008FFB'
                    }
                },
                tooltip: {
                    enabled: true
                },
                show: true,
                decimalsInFloat: 2

            },
            {
                seriesName: 'Revenue',
                opposite: true,
                axisTicks: {
                    show: true
                },
                axisBorder: {
                    show: true,
                    color: '#FEB019'
                },
                labels: {
                    style: {
                        colors: '#FEB019'
                    },
                    formatter: function (value) {
                        var amount = parseInt(value);

                        //return thousands_separators(amount);
                        return value;
                    }
                },
                title: {
                    text: "Margin %",
                    style: {
                        color: '#FEB019'
                    }
                },
                show: true,
                decimalsInFloat: 2
            }
        ],
        tooltip: {
            fixed: {
                enabled: true,
                position: 'topLeft', // topRight, topLeft, bottomRight, bottomLeft
                offsetY: 30,
                offsetX: 60
            }
        },
        legend: {
            horizontalAlign: 'left',
            offsetX: 40
        }
    };

    //var categ_sale_chart = new ApexCharts(document.querySelector("#net-invoice-and-margin-category-chart"), options);
    // chart.render();
    if (_updateflag === true) {
        categ_sale_chart.updateOptions({
            series: [{
                name: 'Sales',
                type: 'column',
                data: _salelist
            }, {
                name: 'Cost',
                type: 'column',
                data: _costlist
            }, {
                name: 'Margin',
                type: 'line',
                data: _marginlist
            }],
            xaxis: {
                categories: _catglist
            },

        });
    } else {
        //console.log("This is initialized");
        categ_sale_chart = new ApexCharts(document.querySelector("#net-invoice-and-margin-category-chart"), options);
        categ_sale_chart.render();
    }

    //Hide loader
    $('.loading-mask').hide();

}


function format_value_arounDay(day_val) {
    var formated_val = [];
    for (var i = 0; i < day_val.length; i++) {
        formated_val.push(day_val[i].toFixed(2))
    }

    return formated_val;
}

// top-ten sku Category Select Category

function getTopTenSelectedValue() {


    var selectActive = tenSkusValue.getElementsByClassName("active");
    var selectedItemId = (selectActive[0].id);
    var x = formatDate(new Date());
    var y = formatDate(new Date());
    var z = categorySelect.value;
    var q = productcategorySelect.value;

    if (z != null) {
        q = productcategorySelect.value = "";
    }
    var fromDate = $("#from-date").val();
    var toDate = $("#to-date").val();

    if (fromDate) {
        x = fromDate;
    }
    if (toDate) {
        y = toDate;
    }


    _updateflag = true;


    if (selectedItemId == 'top10-skus-delivery') {
        var fromDate = $("#from-date").val();
        var toDate = $("#to-date").val();

        if (fromDate) {
            x = fromDate;
        }
        if (toDate) {
            y = toDate;
        }
        new s.web.Model("retail.dashboard").call("topTenSkusdelivery",
            ([x, y, z, q])).then(function (results) {



            // top ten skus return
            drawTopTenSkus(results[0], results[1], 'Delivered', _updateflag);


            //Hide loader
            $('.loading-mask').hide();
        });

    }


    if (selectedItemId == 'top10-skus-return') {

        var fromDate = $("#from-date").val();
        var toDate = $("#to-date").val();

        if (fromDate) {
            x = fromDate;
        }
        if (toDate) {
            y = toDate;
        }
        new s.web.Model("retail.dashboard").call("topTenSkusReturn",
            ([x, y, z, q])).then(function (results) {



            // top ten skus return
            drawTopTenSkus(results[0], results[1], 'Returned', _updateflag);


            //Hide loader
            $('.loading-mask').hide();
        });

    }

    if (selectedItemId == 'top10-skus-count') {
        var fromDate = $("#from-date").val();
        var toDate = $("#to-date").val();

        if (fromDate) {
            x = fromDate;
        }
        if (toDate) {
            y = toDate;
        }


        new s.web.Model("retail.dashboard").call("topTenSkuscount",
            ([x, y, z, q])).then(function (results) {



            // top ten skus return
            drawTopTenSkus(results[0], results[1], 'Delivery Count', _updateflag);


            //Hide loader
            $('.loading-mask').hide();
        });

    }

    if (selectedItemId == 'top10-skus-order') {
        var fromDate = $("#from-date").val();
        var toDate = $("#to-date").val();

        if (fromDate) {
            x = fromDate;
        }
        if (toDate) {
            y = toDate;
        }


        new s.web.Model("retail.dashboard").call("topTenSkusorder",
            ([x, y, z, q])).then(function (results) {



            // top ten skus return
            drawTopTenSkus(results[0], results[1], 'Order', _updateflag);


            //Hide loader
            $('.loading-mask').hide();
        });

    }

}


function getTopTenSelectedCateValue() {

    var selectActive = tenSkusValue.getElementsByClassName("active");
    var selectedItemId = (selectActive[0].id);
    var x = formatDate(new Date());
    var y = formatDate(new Date());
    var z = categorySelect.value;
    var q = productcategorySelect.value;

    if (q != null) {
        z = categorySelect.value = "";
    }

    var fromDate = $("#from-date").val();
    var toDate = $("#to-date").val();

    if (fromDate) {
        x = fromDate;
    }
    if (toDate) {
        y = toDate;
    }

    _updateflag = true;


    if (selectedItemId == 'top10-skus-delivery') {
        var fromDate = $("#from-date").val();
        var toDate = $("#to-date").val();

        if (fromDate) {
            x = fromDate;
        }
        if (toDate) {
            y = toDate;
        }
        new s.web.Model("retail.dashboard").call("topTenSkusdelivery",
            ([x, y, z, q])).then(function (results) {



            // top ten skus return
            drawTopTenSkus(results[0], results[1], 'Delivered', _updateflag);


        });

    }


    if (selectedItemId == 'top10-skus-return') {

        var fromDate = $("#from-date").val();
        var toDate = $("#to-date").val();

        if (fromDate) {
            x = fromDate;
        }
        if (toDate) {
            y = toDate;
        }
        new s.web.Model("retail.dashboard").call("topTenSkusReturn",
            ([x, y, z, q])).then(function (results) {



            // top ten skus return
            drawTopTenSkus(results[0], results[1], 'Returned', _updateflag);

        });

    }

    if (selectedItemId == 'top10-skus-count') {
        var fromDate = $("#from-date").val();
        var toDate = $("#to-date").val();

        if (fromDate) {
            x = fromDate;
        }
        if (toDate) {
            y = toDate;
        }


        new s.web.Model("retail.dashboard").call("topTenSkuscount",
            ([x, y, z, q])).then(function (results) {



            // top ten skus return
            drawTopTenSkus(results[0], results[1], 'Delivery Count', _updateflag);

        });

    }

    if (selectedItemId == 'top10-skus-order') {
        var fromDate = $("#from-date").val();
        var toDate = $("#to-date").val();

        if (fromDate) {
            x = fromDate;
        }
        if (toDate) {
            y = toDate;
        }


        new s.web.Model("retail.dashboard").call("topTenSkusorder",
            ([x, y, z, q])).then(function (results) {



            // top ten skus return
            drawTopTenSkus(results[0], results[1], 'Order', _updateflag);

        });

    }


}


//top-ten-skus
function drawTopTenSkus(product_names, product_values, _event, _updateflag) {
    $('.single-loading-mask').show();
    //console.log(product_values.reverse());
    var productValues = product_values;

    var options = {
        series: [{
            name: _event,
            data: productValues
        }],
        chart: {
            type: 'bar',
            height: 350
        },
        plotOptions: {
            bar: {
                horizontal: true,
            },
        },
        stroke: {
            width: 1,
            colors: ['#fff']
        },
        dataLabels: {
          enabled: true,
            formatter: function (value) {
                    var amount = parseInt(value);

                    return thousands_separators(amount);
                }
        },
        title: {
            text: 'Top 10 SKU'
        },
        xaxis: {
            categories: product_names,
            labels: {
                formatter: function (value) {
                    var amount = parseInt(value);

                    return thousands_separators(amount);
                }
            }
        },
        yaxis: {
            title: {
                text: undefined
            },
            labels: {
                show: true,
                minWidth: 0,
                maxWidth: 450
            }
        },
        tooltip: {
            y: {
                formatter: function (value) {

                    var amount = parseInt(value);

                    return thousands_separators(amount);
                }
            }
        },
        fill: {
            opacity: 1
        },
        legend: {
            position: 'bottom',
            horizontalAlign: 'center',
            offsetX: 40
        }
    };

    //var chart = new ApexCharts(document.querySelector("#top-ten-skus"), options);
    //chart.render();

    if (_updateflag === true) {
        top_ten_skus_chart.updateOptions({
            series: [{
                name: _event,
                data: productValues
            }],
            xaxis: {
                categories: product_names
            }
        })

    } else {
        top_ten_skus_chart = new ApexCharts(document.querySelector("#top-ten-skus"), options);
        top_ten_skus_chart.render();
    }
//Show loader
    $('.single-loading-mask').hide();
}


//Customer Base Month On Month (Last 6 Months)


function uniquePosChart(_labels, _values, _updateflag) {
    var options = {
        dataLabels: {
            enabled: true,
            formatter: function (val, opts) {
                // return (val / 1000000).toFixed(2) + "M"
                return " "
            }
        },
        series: _values,
        chart: {
            type: 'bar',
            height: 250,
            stacked: true,
            toolbar: {
                show: true
            },
            zoom: {
                enabled: true
            }
        },
        legend: {
            position: 'bottom'
        },
        responsive: [{
            breakpoint: 480,
            options: {
                legend: {
                    position: 'bottom',
                    offsetX: -10,
                    offsetY: 0
                }
            }
        }],
        plotOptions: {
            bar: {
                horizontal: false
            }
        },
        xaxis: {
            type: 'string',
            categories: _labels
        },
        yaxis: {
            show: true,
            decimalsInFloat: 2,
            labels: {
                 formatter: function (value) {
                    var amount = value;

                    return thousands_separators(amount);
                }
            }
        },
        fill: {
            opacity: 1
        },
        colors: colorCategory
    };


    // chart.render();
    if (_updateflag === true) {
        mom_unique_pos_chart.updateOptions({
            series: _values,
            labels: _labels

        });
    } else {
        //console.log("This is initialized");
        mom_unique_pos_chart = new ApexCharts(document.querySelector("#unique-pos-chart"), options);
        mom_unique_pos_chart.render();
    }

}
//active-pos-chart

function activePosChart(_labels, _values, _updateflag) {
    var options = {
        dataLabels: {
            enabled: true,
            formatter: function (val, opts) {
                // return (val / 1000000).toFixed(2) + "M"
                return " "
            }
        },
        series: _values,
        chart: {
            type: 'bar',
            height: 250,
            stacked: true,
            toolbar: {
                show: true
            },
            zoom: {
                enabled: true
            }
        },
        legend: {
            position: 'bottom'
        },
        responsive: [{
            breakpoint: 480,
            options: {
                legend: {
                    position: 'bottom',
                    offsetX: -10,
                    offsetY: 0
                }
            }
        }],
        plotOptions: {
            bar: {
                horizontal: false
            }
        },
        xaxis: {
            type: 'string',
            categories: _labels
        },
        yaxis: {
            show: true,
            decimalsInFloat: 2,
            labels: {
                 formatter: function (value) {
                    var amount = value;

                    return thousands_separators(amount);
                }
            }
        },
        fill: {
            opacity: 1
        },
        colors: colorCategory
    };


    // chart.render();
    if (_updateflag === true) {
        mom_active_pos_chart.updateOptions({
            series: _values,
            labels: _labels

        });
    } else {
        //console.log("This is initialized");
        mom_active_pos_chart = new ApexCharts(document.querySelector("#active-pos-chart"), options);
        mom_active_pos_chart.render();
    }
    //Hide loader
    $('.loading-mask').hide();

}

//new-pos-chart

function newPosChart(_labels, _values, _updateflag) {
    var options = {
        dataLabels: {
            enabled: true,
            formatter: function (val, opts) {
                // return (val / 1000000).toFixed(2) + "M"
                return " "
            }
        },
        series: _values,
        chart: {
            type: 'bar',
            height: 250,
            stacked: true,
            toolbar: {
                show: true
            },
            zoom: {
                enabled: true
            }
        },
        legend: {
            position: 'bottom'
        },
        responsive: [{
            breakpoint: 480,
            options: {
                legend: {
                    position: 'bottom',
                    offsetX: -10,
                    offsetY: 0
                }
            }
        }],
        plotOptions: {
            bar: {
                horizontal: false
            }
        },
        xaxis: {
            type: 'string',
            categories: _labels
        },
        yaxis: {
            show: true,
            decimalsInFloat: 2,
            labels: {
                 formatter: function (value) {
                    var amount = value;

                    return thousands_separators(amount);
                }
            }
        },
        fill: {
            opacity: 1
        },
        colors: colorCategory
    };


    // chart.render();
    if (_updateflag === true) {
        mom_new_pos_chart.updateOptions({
            series: _values,
            labels: _labels

        });
    } else {
        //console.log("This is initialized");
        mom_new_pos_chart = new ApexCharts(document.querySelector("#new-pos-chart"), options);
        mom_new_pos_chart.render();
    }
    //Hide loader
    $('.loading-mask').hide();

}

//registered-pos-chart

function registeredPosChart(_labels, _values, _updateflag) {
  //console.log(_values);

    var options = {
        dataLabels: {
            enabled: true,
            formatter: function (val, opts) {
                // return (val / 1000000).toFixed(2) + "M"
                return " "
            }
        },
        series: _values,
        chart: {
            type: 'bar',
            height: 250,
            stacked: true,
            toolbar: {
                show: true
            },
            zoom: {
                enabled: true
            }
        },
        legend: {
            position: 'bottom',
        },
        responsive: [{
            breakpoint: 480,
            options: {
                legend: {
                    position: 'bottom',
                    offsetX: -10,
                    offsetY: 0
                }
            }
        }],
        plotOptions: {
            bar: {
                horizontal: false
            }
        },
        xaxis: {
            type: 'string',
            categories: _labels
        },
        yaxis: {
            show: true,
            decimalsInFloat: 2,
            labels: {
                 formatter: function (value) {
                    var amount = value;

                    return thousands_separators(amount);
                }
            }
        },
        fill: {
            opacity: 1
        },
        colors: colorCategory
    };


    // chart.render();
    if (_updateflag === true) {
        mom_registered_pos_chart.updateOptions({
            series: _values,
            labels: _labels

        });
    } else {
        //console.log("This is initialized");
        mom_registered_pos_chart = new ApexCharts(document.querySelector("#registered-pos-chart"), options);
        mom_registered_pos_chart.render();
    }
    //Hide loader
    $('.loading-mask').hide();
}

//================Customer Base Month On Month (Last 6 Months) Table Data============================
function table_data(_labels, _values, elementId, _updateflag) {


    // Array Month
    var seriesMonth = _labels;


    // Array series name  value
    var seriesArray = _values;


    //let tableDiv = document.getElementById('table-container');

    // Tabele  table-container
    var tableDiv = document.getElementById(elementId);

    //Create Table
    var table = document.createElement('table');
    table.className = "table";
    var thead = document.createElement('thead');
    var tbody = document.createElement('tbody');
    var tfoot = document.createElement('tfoot');

    // Append the Table Head to the table
    table.appendChild(thead);

    // Append the Table body to the table
    table.appendChild(tbody);

    // Append the Table foot to the table
    table.appendChild(tfoot);

    //Create TheadRow
    var thedRow = document.createElement('tr');
    // append the row to the thead
    thead.appendChild(thedRow);

    //Create TbodyRow
    var tbodyRow = document.createElement('tr');
    // append the row to the thead
    tbody.appendChild(tbodyRow);
    //Blank th
    thedRow.appendChild(document.createElement('th'));
    // Thead For Loop[]


    //Create Tfoot
    var tfootRow = document.createElement('tr');
    // append the row to the thead
    tfoot.appendChild(tfootRow);
    var tfootTrowData = document.createElement('th');
    // append the row to the thead
    tfootRow.appendChild(tfootTrowData);
    tfootTrowData.innerHTML = 'Total';


    for (var i = 0; i < seriesMonth.length; i++) {
        //Create TheadRow DATA
        var thedRowData = document.createElement('th');
        // append the row to the thead
        thedRow.appendChild(thedRowData);
        thedRowData.innerHTML = seriesMonth[i];
    }
    ;

    // Tbody thead For Loop[]

    for (var k = 0; k < seriesArray.length; k++) {
        //Create TbodyRow
        var tbodyHeadRow = document.createElement('tr');
        // append the row to the thead
        tbody.appendChild(tbodyHeadRow);
        //Create TheadRow DATA
        var tbodyRowHead = document.createElement('th');
        // tbodyRowHead.style.borderBottom = "none";
        // append the row to the thead
        tbodyHeadRow.appendChild(tbodyRowHead);
        tbodyRowHead.innerHTML = seriesArray[k]['name'];
        for (var j = 0; j < seriesArray[k]['data'].length; j++) {
            var tbodyRowData = document.createElement('td');
            tbodyHeadRow.appendChild(tbodyRowData);
            tbodyRowData.innerHTML = seriesArray[k]['data'][j];
        }
        ;
    }
    ;


    for (var p = 0; p < seriesMonth.length; p++) {
        var total_sum = 0;
        for (var l = 0; l < seriesArray.length; l++) {
            total_sum += seriesArray[l]['data'][p]
        }
        var tfootData = document.createElement('td');
        // append the row to the thead
        tfootRow.appendChild(tfootData);
        tfootData.innerHTML = total_sum;
    }

    tableDiv.appendChild(table);
}


function arrayFormatter(val){

    var formattedArray = [];

    for(i = 0; i < val.length; i++) {
        formattedArray.push(val[i].toFixed(2));
    }

    return formattedArray;

}

// new net-invoice-and-margin-classification-chart-two
function drawSalesMarginDonutNew(_clabels, _cvalues,_customv, _updateflag) {

	_cvalues = arrayFormatter(_cvalues);
	//console.log(_cvalues)
	//console.log(_clabels)

  var options = {
        series: [{
              name: 'Profit Value',
              type: 'column',
              data: _clabels
            }, {
              name: 'Margin (%)',
              type: 'line',
              data: _cvalues
            }],
        chart: {
          height: 281,
          type: 'line'
        },
        stroke: {
          width: [0, 4]
        },
        title: {
          text: 'Profit and Margin'
        },
        dataLabels: {
          enabled: true,
          enabledOnSeries: [1]
        },
        labels: _customv,
        xaxis: {
          type: 'string',
            color: '#feb019'
        },
        yaxis: [{
          title: {
            text: 'Profit Value'
          },
            decimalsInFloat: 2,
            labels: {
                formatter: function (value) {
                    var amount = value;

                    return thousands_separators(amount);
                }
            }

        }, {
          opposite: true,
          title: {
            text: 'Margin (%)'
          },
            // labels: {
            //     formatter: function (value) {
            //         var amount = value;
            //         console.log(amount)
            //
            //         return amount;
            //     }
            // }
        }]
        };

    if (_updateflag === true){
        sale_margin_chart_line_column.updateOptions({
            series: [{
              name: 'Profit Value',
              type: 'column',
              data: _clabels
            }, {
              name: 'Margin (%)',
              type: 'line',
              data: _cvalues
            }],
            labels: _customv

          });
    }else {
        //console.log("This is initialized");
        sale_margin_chart_line_column = new ApexCharts(document.querySelector("#net-invoice-and-margin-classification-chart-two"), options);
        sale_margin_chart_line_column.render();
    }


    //Hide loader
    $('.loading-mask').hide();

}
// end



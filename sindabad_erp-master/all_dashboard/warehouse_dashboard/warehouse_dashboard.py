import datetime
import psycopg2
import operator
from openerp.osv import fields,osv
from openerp.tools.translate import _


class warehouse_dashboard(osv.osv):
    _name = 'warehouse.dashboard'

    def date_plus_one(self, date_str):
        date_obj = datetime.datetime.strptime(date_str, '%Y-%m-%d') + datetime.timedelta(days=1)

        increased_date = date_obj.strftime('%Y-%m-%d')

        return increased_date

    def last_day_this_month(self):

        today = datetime.datetime.today()

        if today.month == 12:
            last_day_obj=today.replace(day=31, month=today.month)
        else:

            last_day_obj = today.replace(day=1, month=today.month + 1) - datetime.timedelta(days=1)
        last_day = last_day_obj.strftime('%Y-%m-%d')


        return last_day

    def last_day_previous_month(self,date):

        today = datetime.datetime.strptime(date, '%Y-%m-%d')

        if today.month == 12:
            last_day_obj=today.replace(day=31, month=today.month)
        else:

            last_day_obj = today.replace(day=1, month=today.month + 1) - datetime.timedelta(days=1)
        last_day = last_day_obj.strftime('%Y-%m-%d')


        return last_day

    def six_month_ago_first_date(self):

        today = datetime.datetime.today()
        (day, month, year) = (today.day, (today.month - 6) % 12 + 1, today.year + (today.month - 6) / 12)

        first_date = str(year) + "-" + str(month) + "-1"

        return first_date

    def present_month_first_date(self):

        today = datetime.datetime.today()
        # (day, month, year) = (today.day, (today.month - 1) % 12 + 1, today.year + (today.month - 6) / 12)
        first_date = str(today.year) + "-" + str(today.month) + "-1"

        return first_date

    def warehouse_data_prepare(self, cr, uid, start_date=None, end_date=None, sale_type=None, context=None):

        tmp = start_date
        start_date = start_date
        end_date = end_date
        end_date_next = self.date_plus_one(end_date)

        # if sale_type == 'monthly':
        #     start_date = self.present_month_first_date()

        total_count = 0

        if sale_type!='all':
            warehouse = int(sale_type)
            sales_count_query = "select count(id) as total from sale_order where state in ('done','progress','manual','shipping_except','invoice_except' ) and warehouse_id='{0}' and date_confirm>='{1}' and date_confirm<='{2}'".format(warehouse,start_date, end_date)

        else:
            sales_count_query = "select count(id) as total from sale_order where state in ('done','progress','manual','shipping_except','invoice_except' ) and date_confirm>='{0}' and date_confirm<='{1}'".format(start_date, end_date)

        cr.execute(sales_count_query)

        sales_count = cr.dictfetchall()

        if len(sales_count) > 0:
            total_count = sales_count[0].get('total')

        # ('draft', 'Draft'),
        # ('cancel', 'Cancelled'),
        # ('waiting', 'Waiting Another Operation'),
        # ('confirmed', 'Waiting Availability'),
        # ('partially_available', 'Partially Available'),
        # ('assigned', 'Ready to Transfer'),
        # ('done', 'Transferred'),

        total_transfer = 0 #done
        total_rfp = 0 #assign
        total_partial_available = 0 #partially_available
        total_waiting_availability = 0 #confirmed
        total_waiting_other_move = 0 #waiting
        total_cancel = 0 #cancel
        total_draft = 0 #draft
        total_stock_picking_count =0
        total_manifest_count =0

        if sale_type!='all':
            warehouse = int(sale_type)
            stock_picking_count_query = "SELECT count(stock_picking.id) AS total, stock_picking.state FROM stock_picking, sale_order WHERE sale_order.name = stock_picking.origin AND picking_type_id IN (SELECT stock_picking_type.id FROM stock_picking_type WHERE code = 'outgoing')And sale_order.warehouse_id = '{2}' AND stock_picking.origin LIKE 'SO%' AND stock_picking.create_date >= '{0}' AND stock_picking.create_date <= '{1}' GROUP BY stock_picking.state".format(start_date, end_date_next,warehouse)
        else:
            stock_picking_count_query = "SELECT count(stock_picking.id) AS total, stock_picking.state FROM stock_picking, sale_order WHERE sale_order.name = stock_picking.origin AND picking_type_id IN (SELECT stock_picking_type.id FROM stock_picking_type WHERE code = 'outgoing') AND stock_picking.origin LIKE 'SO%' AND stock_picking.create_date >= '{0}' AND stock_picking.create_date <= '{1}' GROUP BY stock_picking.state".format(start_date, end_date_next)

        # cr.execute(sales_count_query, (start_date, end_date))
        cr.execute(stock_picking_count_query)

        stock_picking_count = cr.dictfetchall()

        for p_count in stock_picking_count:
            total_picking_count = total_stock_picking_count+p_count.get('total')

            if 'done' in p_count.get('state'):
                total_transfer = p_count.get('total')
            if 'cancel' in p_count.get('state'):
                total_cancel = p_count.get('total')
            if 'confirmed' in p_count.get('state'):
                total_waiting_availability = p_count.get('total')
            if 'partially_available' in p_count.get('state'):
                total_partial_available = p_count.get('total')
            if 'waiting' in p_count.get('state'):
                total_waiting_other_move = p_count.get('total')

            if 'draft' in p_count.get('state'):
                total_draft = p_count.get('total')

            if 'assigned' in p_count.get('state'):
                total_rfp = p_count.get('total')

        total_waiting_count = total_waiting_availability + total_partial_available + total_waiting_other_move

        total_picking_order_count = 0
        total_picking_count = 0
        total_pick_done = 0
        total_pick_cancel = 0
        total_pick_pending = 0
        total_inv_count_count = 0

        if sale_type!='all':
            warehouse = int(sale_type)
            picking_count_query = "select count(DISTINCT picking_id) as total from picking_list_line,sale_order WHERE picking_list_line.magento_no=sale_order.client_order_ref and sale_order.warehouse_id='{2}' AND picking_list_line.state='done' and picking_list_line.create_date >= '{0}' and picking_list_line.create_date <= '{1}'".format(start_date, end_date_next,warehouse)
        else:
            picking_count_query = "select count(DISTINCT picking_id) as total from picking_list_line,sale_order WHERE picking_list_line.magento_no=sale_order.client_order_ref AND picking_list_line.state='done' and picking_list_line.create_date >= '{0}' and picking_list_line.create_date <= '{1}'".format(
                start_date, end_date_next)

        cr.execute(picking_count_query)

        picking_data = cr.dictfetchall()

        if len(picking_data) > 0:
            total_picking_count = picking_data[0].get('total')

        # for pick_count in picking_data:
        #     total_picking_count = total_picking_count+pick_count.get('total')
        #
        #     if 'done' in p_count.get('state'):
        #         total_pick_done = p_count.get('total')
        #     if 'cancel' in p_count.get('state'):
        #         total_pick_cancel = p_count.get('total')
        #     if 'pending' in p_count.get('state'):
        #         total_pick_pending = p_count.get('total')



        picking_order_count_query = "select count(id) as total from picking_list_line WHERE create_date >= '{0}' and create_date <='{1}'".format(
            start_date, end_date_next)

        cr.execute(picking_order_count_query)

        picking_order_count = cr.dictfetchall()

        if len(picking_order_count) > 0:
            total_picking_order_count = picking_order_count[0].get('total')

        if sale_type != 'all':
            warehouse = int(sale_type)
            inv_count_query = "SELECT count(account_invoice.id) as total FROM account_invoice, sale_order where account_invoice.name = sale_order.client_order_ref and account_invoice.state  in ('open','paid') and account_invoice.type = 'out_invoice' and sale_order.warehouse_id='{2}' and account_invoice.create_date>='{0}' and account_invoice.create_date <='{1}'".format(
                start_date, end_date_next,warehouse)
        else:
            inv_count_query = "SELECT count(account_invoice.id) as total FROM account_invoice, sale_order where account_invoice.name = sale_order.client_order_ref and account_invoice.state  in ('open','paid') and account_invoice.type = 'out_invoice' and account_invoice.create_date>='{0}' and account_invoice.create_date <='{1}'".format(
                start_date, end_date_next)

        cr.execute(inv_count_query)

        inv_count_count = cr.dictfetchall()

        if len(inv_count_count) > 0:
            total_inv_count_count = inv_count_count[0].get('total')


        ####
        if sale_type != 'all':
            warehouse = int(sale_type)
            manifest_count_query = "SELECT count(DISTINCT wms_manifest_process.id) AS total FROM wms_manifest_process,wms_manifest_line, sale_order WHERE wms_manifest_process.id = wms_manifest_line.wms_manifest_id and wms_manifest_line.magento_no = sale_order.client_order_ref and wms_manifest_process.state ='confirm' AND sale_order.warehouse_id = '{2}' AND  wms_manifest_line.create_date >= '{0}' AND wms_manifest_line.create_date <= '{1}'".format(
            start_date, end_date_next,warehouse)
        else:
            manifest_count_query = "SELECT count(DISTINCT wms_manifest_process.id) AS total FROM wms_manifest_process,wms_manifest_line, sale_order WHERE wms_manifest_process.id = wms_manifest_line.wms_manifest_id and wms_manifest_line.magento_no = sale_order.client_order_ref and wms_manifest_process.state ='confirm' AND  wms_manifest_line.create_date >= '{0}' AND wms_manifest_line.create_date <= '{1}'".format(
                start_date, end_date_next)

        cr.execute(manifest_count_query)

        manifest_count = cr.dictfetchall()

        if len(manifest_count) > 0:
            total_manifest_count = manifest_count[0].get('total')


        # warehouse_name_dict ={
        #     1: 'Nodda',
        #     2:'Central Uttara',
        #     21: 'SignBoard'
        # }
        #
        # if sale_type!='all':
        #
        #     warehouse_name = warehouse_name_dict[int(sale_type)]
        # else:
        #     warehouse_name = 'All'

        return [total_count,total_rfp,total_waiting_count,total_picking_count,total_inv_count_count,total_manifest_count,

                total_partial_available,total_waiting_other_move,total_waiting_availability,
                total_draft,total_transfer,total_cancel,total_pick_pending,total_pick_done,total_pick_cancel,
                total_picking_order_count
                ]
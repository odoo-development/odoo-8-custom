
import operator
from openerp.osv import fields, osv
from openerp.tools.translate import _
import datetime
import psycopg2
import json
import ast

class retail_dashboard(osv.osv):
    _name = 'retail.dashboard'

    def date_plus_one(self, date_str):

        date_obj = datetime.datetime.strptime(date_str, '%Y-%m-%d') + datetime.timedelta(days=1)

        increased_date = date_obj.strftime('%Y-%m-%d')

        return increased_date

    def last_day_this_month(self):

        today = datetime.datetime.today()

        if today.month == 12:
            last_day_obj = today.replace(day=31, month=today.month)
        else:

            last_day_obj = today.replace(day=1, month=today.month + 1) - datetime.timedelta(days=1)
        last_day = last_day_obj.strftime('%Y-%m-%d')

        return last_day

    def six_month_ago_first_date(self):

        today = datetime.datetime.today()
        (day, month, year) = (today.day, (today.month - 6) % 12 + 1, today.year + (today.month - 6) / 12)

        first_date = str(year) + "-" + str(month) + "-1"

        return first_date

    def tunr_over_data_calculation(self, cr, uid, start_date=None, end_date=None, sale_type=None, context=None):
        uid = 1

        tmp = start_date
        start_date = end_date
        end_date = tmp
        end_date = self.date_plus_one(end_date)

        last_date_of_this_month = self.last_day_this_month()
        start_date_list = last_date_of_this_month.split('-')
        start_date_list[2] = '1'
        start_date = '-'.join(start_date_list)
        end_date = last_date_of_this_month

        ## Start Order to Confirm Data

        sale_query = "select customer_classification,create_date,date_confirm, DATE_PART('day', AGE(date_confirm,date(create_date))) AS days from sale_order where create_date >=%s and create_date <=%s and date_confirm is not Null"
        objects = cr.execute(sale_query, (start_date, end_date))
        sale_objects = cr.dictfetchall()

        anant_days = corp_days = retail_days = general_days = 0
        anna_count = corp_count = retail_count = general_count = 0
        anant_avg = corp_avg = retail_avg = general_avg = 0

        for items in sale_objects:
            if 'Ananta' in items.get('customer_classification'):
                anna_count = anna_count + 1
                anant_days = anant_days + items.get('days')
            elif 'Corporate' in items.get('customer_classification') or 'SOHO' in items.get('customer_classification'):
                corp_count += 1
                corp_days = corp_days + items.get('days')
            elif 'Retail' in items.get('customer_classification') :
                retail_count += 1
                retail_days = retail_days + items.get('days')
            else:
                general_count += 1
                general_days = general_days + items.get('days')

        order_to_confirm = []

        try:
            order_to_confirm.append(round((anant_days / anna_count), 2))
        except:
            order_to_confirm.append(0)

        try:
            order_to_confirm.append(round((corp_days / corp_count), 2))
        except:
            order_to_confirm.append(0)

        try:
            order_to_confirm.append(round((retail_days / retail_count), 2))
        except:
            order_to_confirm.append(0)

        try:
            order_to_confirm.append(round((general_days / general_count), 2))
        except:
            order_to_confirm.append(0)

        ## Ends Here Order To Confirm Calculation



        ## Start Confirm to Invoice
        inv_type = 'out_invoice'
        state = 'cancel'

        invoice_query = "select sale_order.customer_classification,DATE_PART('day', AGE(date(account_invoice.create_date),sale_order.date_confirm)) AS inv_days from account_invoice,sale_order_invoice_rel,sale_order where account_invoice.id = sale_order_invoice_rel.invoice_id and sale_order_invoice_rel.order_id = sale_order.id and account_invoice.type=%s and account_invoice.state !=%s and account_invoice.create_date >=%s and account_invoice.create_date<=%s"

        c2i = cr.execute(invoice_query, (inv_type, state, start_date, end_date))
        c2i_obj = cr.dictfetchall()

        anant_days = corp_days = retail_days = general_days = 0
        anna_count = corp_count = retail_count = general_count = 0
        anant_avg = corp_avg = retail_avg = general_avg = 0

        for items in c2i_obj:
            days_count = 0
            if items.get('inv_days') is not None:
                days_count = items.get('inv_days')
            if 'Ananta' in items.get('customer_classification'):
                anna_count = anna_count + 1
                anant_days = anant_days + days_count
            elif 'Corporate' in items.get('customer_classification')or 'SOHO' in items.get('customer_classification'):
                corp_count += 1
                corp_days = corp_days + days_count
            elif 'Retail' in items.get('customer_classification') :
                retail_count += 1
                retail_days = retail_days + days_count
            else:
                general_count += 1
                general_days = general_days + days_count

        confirm_to_invoice = []

        try:
            confirm_to_invoice.append(round((anant_days / anna_count), 2))
        except:
            confirm_to_invoice.append(0)

        try:
            confirm_to_invoice.append(round((corp_days / corp_count), 2))
        except:
            confirm_to_invoice.append(0)

        try:
            confirm_to_invoice.append(round((retail_days / retail_count), 2))
        except:
            confirm_to_invoice.append(0)

        try:
            confirm_to_invoice.append(round((general_days / general_count), 2))
        except:
            confirm_to_invoice.append(0)

        ## Ends Here



        ### Start Invoice to Deleivery Code

        in2d_query = "select sale_order.customer_classification,DATE_PART('day', AGE(account_invoice.delivery_date,date(account_invoice.create_date))) AS delvery_days from account_invoice,sale_order_invoice_rel,sale_order where account_invoice.id = sale_order_invoice_rel.invoice_id and sale_order_invoice_rel.order_id = sale_order.id and account_invoice.delivery_date is not Null and account_invoice.type=%s and account_invoice.state!=%s and account_invoice.create_date >=%s and account_invoice.create_date<=%s"

        i2d = cr.execute(in2d_query, (inv_type, state, start_date, end_date))
        i2d_obj = cr.dictfetchall()

        anant_days = corp_days = retail_days = general_days = 0
        anna_count = corp_count = retail_count = general_count = 0
        anant_avg = corp_avg = retail_avg = general_avg = 0

        for items in i2d_obj:
            days_count = 0
            if items.get('delvery_days') is not None:
                days_count = items.get('delvery_days')

            if 'Ananta' in items.get('customer_classification'):
                anna_count = anna_count + 1
                anant_days = anant_days + days_count
            elif 'Corporate' in items.get('customer_classification') or 'SOHO' in items.get('customer_classification'):
                corp_count += 1
                corp_days = corp_days + days_count
            elif 'Retail' in items.get('customer_classification'):
                retail_count += 1
                retail_days = retail_days + days_count
            else:
                general_count += 1
                general_days = general_days + days_count

        invoice_to_delivery = []

        try:
            invoice_to_delivery.append(round((anant_days / anna_count), 2))
        except:
            invoice_to_delivery.append(0)

        try:
            invoice_to_delivery.append(round((corp_days / corp_count), 2))
        except:
            invoice_to_delivery.append(0)

        try:
            invoice_to_delivery.append(round((retail_days / retail_count), 2))
        except:
            invoice_to_delivery.append(0)

        try:
            invoice_to_delivery.append(round((general_days / general_count), 2))
        except:
            invoice_to_delivery.append(0)

        ## Ends Invoice to Delivery Code




        return [order_to_confirm, confirm_to_invoice, invoice_to_delivery]

    def category_wise_invoice_margin(self, cr, uid, start_date=None, end_date=None, sale_type=None, context=None):
        uid = 1

        tmp = start_date
        start_date = end_date
        end_date = tmp
        end_date = self.date_plus_one(end_date)

        last_date_of_this_month = self.last_day_this_month()
        start_date_list = last_date_of_this_month.split('-')
        start_date_list[2] = '1'
        start_date = '-'.join(start_date_list)
        end_date = last_date_of_this_month

        st_date = start_date
        end_date = self.date_plus_one(end_date)

        primary_cogs_query = "select stock_move.id as moved_id,stock_move.product_id,stock_picking.date_done,stock_picking.picking_type_id,stock_move.product_uom_qty,(select sale_order.customer_classification from sale_order,sale_order_invoice_rel where sale_order.id=sale_order_invoice_rel.order_id and (sale_order.customer_classification LIKE 'Retail%') and sale_order_invoice_rel.invoice_id=account_invoice.id) as customer_classification,sale_order_line.price_unit,sale_order_line.order_partner_id,sale_order.client_order_ref,sale_order.date_confirm,sale_order_line.name, sale_order.name as odoo_no,(select product_category.name from product_category,product_template,product_product where product_category.id = product_template.categ_id and product_template.id = product_product.product_tmpl_id and product_product.id = stock_move.product_id ) as category,(select sum((sq.qty*sm.price_unit)) as total from stock_quant_move_rel as sqm,stock_move as sm, stock_quant as sq where sqm.quant_id in ((select sq2.id from stock_quant_move_rel as sqmr2, stock_quant as sq2 where sqmr2.move_id=stock_move.id and sqmr2.quant_id = sq2.id)) and sqm.move_id = sm.id and sm.purchase_line_id >0 and sq.id=sqm.quant_id group by sq.product_id)as total_price , (select name from stock_warehouse where id=stock_move.warehouse_id) as warhouse_name,account_invoice.number as invoice_number, account_invoice.date_invoice as invoice_date, account_invoice.create_date as create_date, account_invoice.state as invoice_state from stock_move,stock_picking,sale_order_line,sale_order,account_invoice where stock_move.picking_id=stock_picking.id and stock_move.sale_line_id = sale_order_line.id and sale_order_line.order_id = sale_order.id  and stock_picking.name = account_invoice.origin and account_invoice.state != 'cancel'  and account_invoice.state != 'draft' and type in ('out_invoice', 'out_refund') and account_invoice.create_date >= '{0}' and account_invoice.create_date <= '{1}'".format(
            st_date, end_date)

        # objects = cr.execute(primary_cogs_query, (st_date, end_date))
        objects = cr.execute(primary_cogs_query)
        objects = cr.dictfetchall()

        incoming_ids = []
        in_query = "select id from  stock_picking_type where code='incoming'"
        cr.execute(in_query)
        in_objects = cr.dictfetchall()

        incoming_ids = [in_item.get('id') for in_item in in_objects]

        categ_list = []
        sale_categ_dict = {}
        margin_cost_categ_dict = {}

        total_sales = 0.00
        total_margin_cost = 0.00

        grocery_sale = stationary_sale = electronics_sale = mobile_sale = others_sale  = chemical_sale  = 0
        grocery_cogs = stationary_cogs = electronics_cogs = mobile_cogs = others_cogs  = chemical_cogs  = 0

        for line in objects:
            if line.get('category') not in categ_list:
                categ_list.append(line.get('category'))
                sale_categ_dict[line.get('category')] = 0.00
                margin_cost_categ_dict[line.get('category')] = 0.00

            cal_cost = 0

            if line.get('total_price') is not None:
                cal_cost = line.get('total_price')

            if cal_cost == 0:
                get_adjusted_cost_query = "select sum((sq.qty * sm.price_unit)) as total from stock_quant_move_rel as sqm,stock_move as sm, stock_quant as sq where sqm.quant_id in ((select sq2.id from stock_quant_move_rel as sqmr2, stock_quant as sq2 where sqmr2.move_id=%s and sqmr2.quant_id = sq2.id)) and sqm.move_id = sm.id and sm.inventory_id >0 and sq.id=sqm.quant_id group by sq.product_id"
                moved_id = line.get('moved_id')
                cr.execute(get_adjusted_cost_query, ([moved_id]))
                get_adjusted_cost = cr.dictfetchall()
                for a_cost in get_adjusted_cost:
                    if a_cost.get('total') is not None:
                        cal_cost = a_cost.get('total')

            # Floowing chekup for sq qty is zero then
            if cal_cost == 0:

                get_adjusted_query = "select sq.cost,sm.price_unit from stock_quant_move_rel as sqm,stock_move as sm, stock_quant as sq where sqm.quant_id in (select sq2.id from stock_quant_move_rel as sqmr2, stock_quant as sq2 where sqmr2.move_id=%s and sqmr2.quant_id = sq2.id) and sqm.move_id = sm.id  and sq.id=sqm.quant_id and (sm.purchase_line_id >0 or sm.inventory_id >0) ORDER BY sm.id desc limit 1"
                moved_id = line.get('moved_id')
                cr.execute(get_adjusted_query, ([moved_id]))
                get_adjusted_cost = cr.dictfetchall()
                for a_cost in get_adjusted_cost:
                    if a_cost.get('price_unit') is not None:
                        cal_cost = a_cost.get('price_unit') * line.get('product_uom_qty')
                    elif a_cost.get('cost') is not None:
                        cal_cost = a_cost.get('cost') * line.get('product_uom_qty')

            ### here the profit and sales will be calculated

            line_total_sale = float(line.get('price_unit') * line.get('product_uom_qty'))

            if line.get('picking_type_id') in incoming_ids:
                line_total_sale = line_total_sale * -1
                cal_cost = cal_cost * -1

            if str(line.get('customer_classification')).startswith('Retail') and 'Stationary' in \
                    line.get('customer_classification').split()[2]:
                stationary_sale += line_total_sale
                stationary_cogs += cal_cost

            elif str(line.get('customer_classification')).startswith('Retail') and 'Grocery' in \
                    line.get('customer_classification').split()[2]:
                grocery_sale += line_total_sale
                grocery_cogs += cal_cost

            elif str(line.get('customer_classification')).startswith('Retail') and 'Electronics' in \
                    line.get('customer_classification').split()[2]:
                electronics_sale += line_total_sale
                electronics_cogs += cal_cost

            elif str(line.get('customer_classification')).startswith('Retail') and 'Mobile' in \
                    line.get('customer_classification').split()[2]:
                mobile_sale += line_total_sale
                mobile_cogs += cal_cost
            elif str(line.get('customer_classification')).startswith('Retail') and 'Industrial' in \
                    line.get('customer_classification').split()[2]:

                chemical_sale += line_total_sale
                chemical_cogs += cal_cost

            # elif 'SOHO' in str(line.get('customer_classification')):
            #     soho_sale += line_total_sale
            #     soho_cogs += cal_cost

            else:
                if str(line.get('customer_classification')).startswith('Retail'):
                    others_sale += line_total_sale
                    others_cogs += cal_cost

            total_sales = total_sales + line_total_sale  ## IT is for total sales

            total_margin_cost = total_margin_cost + cal_cost  ## It is for total Cost

            sale_categ_dict[line.get('category')] = sale_categ_dict[line.get(
                'category')] + line_total_sale  ## It is for categ wise sales
            margin_cost_categ_dict[line.get('category')] = margin_cost_categ_dict[line.get(
                'category')] + cal_cost  ## It is for categ wise cost




            ### Ends here

        sales_list = []
        cat_list = []
        cost_list = []
        margin_list = []
        pnl = 0

        for item in categ_list:
            try:
                pnl = round((((sale_categ_dict[item] - margin_cost_categ_dict[item]) / sale_categ_dict[item]) * 100), 2)
            except ZeroDivisionError:
                pass

            cat_list.append(item)
            cost_list.append(float(margin_cost_categ_dict[item]))
            sales_list.append(float(sale_categ_dict[item]) if float(sale_categ_dict[item])>= 0 else 0.00)
            margin_list.append(float(pnl))
        total_sale = grocery_sale + stationary_sale + electronics_sale + mobile_sale + others_sale  + chemical_sale
        total_cost = grocery_cogs + stationary_cogs + electronics_cogs + mobile_cogs + others_cogs  + chemical_cogs

        margin = round((total_sale - total_cost), 2)
        if total_sale > 0:
            margin_in_percent = (margin / total_sale) * 100
            margin_in_percent = round(margin_in_percent, 2)
        else:
            margin_in_percent = 0

        classification_wise_cogs_margin = {'Grocery': [grocery_sale, grocery_cogs],
                                           'Stationary': [stationary_sale, stationary_cogs],
                                           'Electronics': [electronics_sale, electronics_cogs],
                                           'Mobile': [mobile_sale, mobile_cogs],
                                           'Others': [others_sale, others_cogs],
                                           # 'SOHO': [soho_sale, soho_cogs],
                                           'Industrial & Chemical': [chemical_sale, chemical_cogs]
                                           }

        return [cat_list, cost_list, sales_list, margin_list, total_sales, total_margin_cost,
                classification_wise_cogs_margin, margin, margin_in_percent]

    def present_month_first_date(self):

        today = datetime.datetime.today()
        # (day, month, year) = (today.day, (today.month - 1) % 12 + 1, today.year + (today.month - 6) / 12)
        first_date = str(today.year) + "-" + str(today.month) + "-1"

        return first_date

    def retail_today_to_monthly_sales_trending(self, cr, uid, start_date=None, end_date=None, sale_type=None,
                                               context=None):
        uid = 1

        tmp = start_date
        start_date = end_date
        end_date = tmp

        if sale_type == 'monthly':
            start_date = self.present_month_first_date()

        #### Sales Data calculation
        sales_query = "select customer_classification,sum(amount_total) as total from sale_order where state in ('done','progress','manual','shipping_except','invoice_except' ) and (customer_classification LIKE 'Retail%') and date_confirm>='{0}' and date_confirm<='{1}' group by customer_classification".format(
            start_date, end_date)

        # cr.execute(sales_query, (start_date, end_date))
        cr.execute(sales_query)

        sales_data = cr.dictfetchall()

        sale_data_dict = {}
        sale_data_dict['Stationary'] = 0
        total_sale = 0
        total_count = 0
        total_today_label = []
        total_today_value = []

        for items in sales_data:

            try:
                total_sale += items.get('total')
            except:
                pass

            # if 'SOHO' in items.get('customer_classification'):
            #     sale_data_dict['SOHO'] = items.get('total')
            if items.get('customer_classification').startswith('Retail') and 'Stationary' in \
                    items.get('customer_classification').split()[2]:
                sale_data_dict['Stationary'] += items.get('total')
            elif items.get('customer_classification').startswith('Retail') and 'Electronics' in \
                    items.get('customer_classification').split()[2]:
                sale_data_dict['Electronics'] = items.get('total')
            elif items.get('customer_classification').startswith('Retail') and 'Grocery' in \
                    items.get('customer_classification').split()[2]:
                sale_data_dict['Grocery'] = items.get('total')
            elif items.get('customer_classification').startswith('Retail') and 'Mobile' in \
                    items.get('customer_classification').split()[2]:
                sale_data_dict['Mobile Accessories'] = items.get('total')
            elif items.get('customer_classification').startswith('Retail') and 'Industrial' in \
                    items.get('customer_classification').split()[2]:
                sale_data_dict['Industrial & Chemical'] = items.get('total')
            else:
                if items.get('customer_classification').startswith('Retail'):
                    sale_data_dict['Others'] = items.get('total')

        sales_count_query = "select count(id) as total from sale_order where state in ('done','progress','manual','shipping_except','invoice_except' ) and (customer_classification LIKE 'Retail%') and date_confirm>='{0}' and date_confirm<='{1}'".format(
            start_date, end_date)

        # cr.execute(sales_count_query, (start_date, end_date))
        cr.execute(sales_count_query)

        sales_count = cr.dictfetchall()

        if len(sales_count) > 0:
            total_count = sales_count[0].get('total')

        for k, v in sale_data_dict.iteritems():
            total_today_label.append(k)
            total_today_value.append(v)

        #### Sales Data calculation Ends Here



        ### Invoice Calcualtion Starts Here
        end_date = self.date_plus_one(end_date)

        total_invoice_query = "select sum(account_invoice.amount_total) as total,sale_order.customer_classification as customer_classification,account_invoice.type from account_invoice,sale_order_invoice_rel,sale_order where account_invoice.id=sale_order_invoice_rel.invoice_id and sale_order_invoice_rel.order_id=sale_order.id and account_invoice.type in ('out_invoice','out_refund') and account_invoice.create_date>='{0}' and account_invoice.state  in ('open','paid') and account_invoice.create_date <='{1}' and (sale_order.customer_classification LIKE 'Retail%') group by account_invoice.type,sale_order.customer_classification".format(
            start_date, end_date)

        # cr.execute(total_invoice_query, (start_date, end_date))
        cr.execute(total_invoice_query)

        total_invoice_data = cr.dictfetchall()

        invoice_data_dict = {}
        invoice_data_dict['Stationary'] = 0
        total_invoice_amount = 0
        total_invoice_count = 0
        total_invoice_label = []
        total_invoice_value = []

        refund_invoice_dict = {}
        refund_invoice_dict['Stationary'] = 0
        refund_invoice_amount = 0
        refund_invoice_count = 0
        refund_invoice_level = []
        refund_invoice_value = []

        net_general = 0
        net_stationary = 0
        net_retail = 0

        net_electronics = 0
        net_grocery = 0
        net_others = 0
        net_mobile = 0
        net_chemical = 0

        for items in total_invoice_data:

            if items.get('type') == 'out_invoice':
                try:
                    total_invoice_amount += items.get('total')
                except:
                    pass
                # if 'SOHO' in items.get('customer_classification'):
                #     invoice_data_dict['SOHO'] = items.get('total')
                #     net_soho += items.get('total')
                if items.get('customer_classification').startswith('Retail') and 'Stationary' in \
                        items.get('customer_classification').split()[2]:
                    invoice_data_dict['Stationary'] += items.get('total')
                    net_stationary += items.get('total')
                elif items.get('customer_classification').startswith('Retail') and 'Electronics' in \
                        items.get('customer_classification').split()[2]:
                    invoice_data_dict['Electronics'] = items.get('total')
                    net_electronics += items.get('total')
                elif items.get('customer_classification').startswith('Retail') and 'Grocery' in \
                        items.get('customer_classification').split()[2]:
                    invoice_data_dict['Grocery'] = items.get('total')
                    net_grocery += items.get('total')
                elif items.get('customer_classification').startswith('Retail') and 'Mobile' in \
                        items.get('customer_classification').split()[2]:
                    invoice_data_dict['Mobile Accessories'] = items.get('total')
                    net_mobile += items.get('total')
                elif items.get('customer_classification').startswith('Retail') and 'Industrial' in \
                        items.get('customer_classification').split()[2]:
                    invoice_data_dict['Industrial & Chemical'] = items.get('total')
                    net_chemical += items.get('total')
                else:
                    if items.get('customer_classification').startswith('Retail'):
                        invoice_data_dict['Others'] = items.get('total')
                        net_others += items.get('total')

            else:
                try:
                    refund_invoice_amount += items.get('total')
                except:
                    pass
                # if 'SOHO' in items.get('customer_classification'):
                #     refund_invoice_dict['SOHO'] = items.get('total')
                #     net_soho -= items.get('total')
                if items.get('customer_classification').startswith('Retail') and 'Stationary' in \
                        items.get('customer_classification').split()[2]:
                    refund_invoice_dict['Stationary'] = items.get('total')
                    net_stationary -= items.get('total')
                elif items.get('customer_classification').startswith('Retail') and 'Electronics' in \
                        items.get('customer_classification').split()[2]:
                    refund_invoice_dict['Electronics'] = items.get('total')
                    net_electronics -= items.get('total')
                elif items.get('customer_classification').startswith('Retail') and 'Grocery' in \
                        items.get('customer_classification').split()[2]:
                    refund_invoice_dict['Grocery'] = items.get('total')
                    net_grocery -= items.get('total')
                elif items.get('customer_classification').startswith('Retail') and 'Mobile' in \
                        items.get('customer_classification').split()[2]:
                    refund_invoice_dict['Mobile Accessories'] = items.get('total')
                    net_mobile -= items.get('total')
                elif items.get('customer_classification').startswith('Retail') and 'Industrial' in \
                        items.get('customer_classification').split()[2]:
                    refund_invoice_dict['Industrial & Chemical'] = items.get('total')
                    net_chemical -= items.get('total')
                else:
                    if items.get('customer_classification').startswith('Retail'):
                        refund_invoice_dict['Others'] = items.get('total')
                        net_others -= items.get('total')

        for k, v in invoice_data_dict.iteritems():
            total_invoice_label.append(k)
            total_invoice_value.append(v)

        for k, v in refund_invoice_dict.iteritems():
            refund_invoice_level.append(k)
            refund_invoice_value.append(v)

        inv_count_query = "SELECT count(account_invoice.id) total FROM account_invoice, sale_order where account_invoice.name = sale_order.client_order_ref and sale_order.customer_classification LIKE 'Retail%' and type = 'out_invoice' and account_invoice.state  in ('open','paid') and account_invoice.create_date>='{0}' and account_invoice.create_date <='{1}'".format(
            start_date, end_date)
        # inv_count_query = "select count(id) as total from account_invoice where type='out_invoice' and create_date>='{0}' and create_date <='{1}'".format(
        #     start_date, end_date)

        # cr.execute(inv_count_query, (start_date, end_date))
        cr.execute(inv_count_query)

        inv_count_data = cr.dictfetchall()

        if len(inv_count_data) > 0:
            total_invoice_count = inv_count_data[0].get('total')

        ref_inv_count_query = "SELECT count(account_invoice.id) total FROM account_invoice, sale_order where account_invoice.name = sale_order.client_order_ref and sale_order.customer_classification LIKE 'Retail%' and type = 'out_refund' and account_invoice.state  in ('open','paid') and account_invoice.create_date>='{0}' and account_invoice.create_date <='{1}'".format(
            start_date, end_date)

        # ref_inv_count_query = "select count(id) as total from account_invoice where type='out_refund' and create_date>='{0}' and create_date <='{1}'".format(
        #     start_date, end_date)
        # cr.execute(ref_inv_count_query, (start_date, end_date))
        cr.execute(ref_inv_count_query)

        ref_inv_count_data = cr.dictfetchall()

        net_invoice_level = []
        net_invoice_value = []

        if len(ref_inv_count_query) > 0:
            refund_invoice_count = ref_inv_count_data[0].get('total')
        net_invoice_amount = net_stationary + net_electronics + net_grocery + net_others + net_mobile + net_chemical
        margin = 0

        # if net_soho != 0:
        #     net_invoice_level.append('SOHO')
        #     net_invoice_value.append(net_soho)
        # else:
        #     net_invoice_level.append('SOHO')
        #     net_invoice_value.append(0)
        if net_stationary != 0:
            net_invoice_level.append('Stationary')
            net_invoice_value.append(net_stationary)
        else:
            net_invoice_level.append('Stationary')
            net_invoice_value.append(0)
        if net_electronics != 0:
            net_invoice_level.append('Electronics')
            net_invoice_value.append(net_electronics)
        else:
            net_invoice_level.append('Electronics')
            net_invoice_value.append(0)
        if net_grocery != 0:
            net_invoice_level.append('Grocery')
            net_invoice_value.append(net_grocery)
        else:
            net_invoice_level.append('Grocery')
            net_invoice_value.append(0)
        if net_others != 0:
            net_invoice_level.append('Others')
            net_invoice_value.append(net_others)
        else:
            net_invoice_level.append('Others')
            net_invoice_value.append(0)
        if net_mobile != 0:
            net_invoice_level.append('Mobile Accessories')
            net_invoice_value.append(net_mobile)
        else:
            net_invoice_level.append('Mobile Accessories')
            net_invoice_value.append(0)
        if net_chemical != 0:
            net_invoice_level.append('Industrial & Chemical')
            net_invoice_value.append(net_chemical)
        else:
            net_invoice_level.append('Industrial & Chemical')
            net_invoice_value.append(0)

        return [float(str(total_sale)), total_count, total_today_label, total_today_value,
                float(str(total_invoice_amount)), total_invoice_count, total_invoice_label, total_invoice_value,
                float(str(refund_invoice_amount)),
                refund_invoice_count,
                refund_invoice_level,
                refund_invoice_value,
                float(str(net_invoice_amount)),
                margin,
                net_invoice_level,
                net_invoice_value,
                ]

    def monthon_month_value(self, cr, uid, start_date=None, end_date=None, sale_type=None, context=None):
        uid = 1
        # tmp=start_date
        # start_date=end_date
        # end_date=tmp

        # start_date='2020-04-01'
        start_date = self.six_month_ago_first_date()
        # end_date='2020-09-30'
        end_date = self.last_day_this_month()

        start_date_year = start_date.split('-')[0]
        end_date_year = end_date.split('-')[0]

        tmp_date = end_date

        current_spltted_data = tmp_date.split('-')
        current_year = int(current_spltted_data[0]) % 2000
        previous_year = (int(current_spltted_data[0]) % 2000) - 1
        current_month = int(current_spltted_data[1])

        # if current_month > 6 and int(start_date_year)!=int(end_date_year):
        #     previous_year = previous_year
        # else:
        #     previous_year = current_year
        if current_month > 6:
            previous_year = current_month

        # if current_month > 6:
        #     previous_year = current_month

        customer_classification = ['Grocery', 'Stationary', 'Electronics', 'Mobile Accessories', 'Others',
                                   'Industrial & Chemical']
        month_dict = {
            1: "Jan' " + str(current_year),
            2: "Feb' " + str(current_year),
            3: "Mar' " + str(current_year),
            4: "Apr' " + str(current_year),
            5: "May' " + str(current_year),
            6: "Jun' " + str(current_year),
            7: "Jul' " + str(previous_year),
            8: "Aug' " + str(previous_year),
            9: "Sep' " + str(previous_year),
            10: "Oct' " + str(previous_year),
            11: "Nov' " + str(previous_year),
            12: "Dec' " + str(previous_year)
        }

        month_wise_sale_query = "select sum(amount_total) as total ,customer_classification,  date_part('month',date_confirm) as month_wise from sale_order where state in ('done','progress','manual','shipping_except','invoice_except' ) and (customer_classification LIKE 'Retail%') and date_confirm>='{0}' and date_confirm<='{1}' group by month_wise, customer_classification order by month_wise".format(
            start_date, end_date)

        # cr.execute(month_wise_sale_query, (start_date, end_date))
        cr.execute(month_wise_sale_query)

        month_sale_list = cr.dictfetchall()

        sale_month_list = []

        for items in month_sale_list:
            if items.get('month_wise') not in sale_month_list:
                sale_month_list.append(items.get('month_wise'))
        sale_month_list = list(dict.fromkeys(sale_month_list))

        cl_sale_dict = {}

        for sale_month in sale_month_list:
            tmp_value = []
            grocery_sale = 0
            stationary_sale = 0
            electronics_sale = 0
            mobile_sale = 0
            others_sale = 0
            chemical_sale = 0
            soho_sale = 0
            sme_sale = 0

            general_sale = 0
            for items in month_sale_list:
                if sale_month == items.get('month_wise'):
                    if items.get('customer_classification').startswith('Retail') and 'Grocery' in \
                            items.get('customer_classification').split()[2]:
                        grocery_sale += items.get('total')
                    elif items.get('customer_classification').startswith('Retail') and 'Stationary' in \
                            items.get('customer_classification').split()[2]:
                        stationary_sale += items.get('total')
                    elif items.get('customer_classification').startswith('Retail') and 'Electronics' in \
                            items.get('customer_classification').split()[2]:
                        electronics_sale += items.get('total')
                    elif items.get('customer_classification').startswith('Retail') and 'Mobile' in \
                            items.get('customer_classification').split()[2]:
                        mobile_sale += items.get('total')
                    elif items.get('customer_classification').startswith('Retail') and 'Industrial' in \
                            items.get('customer_classification').split()[2]:
                        chemical_sale += items.get('total')
                    # elif 'SOHO' in items.get('customer_classification'):
                    #     soho_sale += items.get('total')
                    else:
                        if items.get('customer_classification').startswith('Retail'):
                            others_sale += items.get('total')

            cl_sale_dict[sale_month] = [grocery_sale, stationary_sale, electronics_sale, mobile_sale, others_sale,
                                        chemical_sale]

        grocery_data = []
        stationary_data = []
        electronics_data = []
        mobile_data = []
        other_data = []
        chemical_data = []
        sme_data = []
        soho_data = []
        for k, v in cl_sale_dict.iteritems():
            prepare_list = []
            grocery_data.append(v[0])
            stationary_data.append(v[1])
            electronics_data.append(v[2])
            mobile_data.append(v[3])
            other_data.append(v[4])
            chemical_data.append(v[5])

        montwise_sale_final_list = [{
            'name': 'Grocery',
            'data': grocery_data
        },
            {
                'name': 'Stationary',
                'data': stationary_data
            },
            {
                'name': 'Electronics',
                'data': electronics_data
            },
            {
                'name': 'Mobile Accessories',
                'data': mobile_data
            },
            {
                'name': 'Others',
                'data': other_data
            },
            # {
            #     'name': 'SOHO',
            #     'data': soho_data
            # },
            {
                'name': 'Industrial & Chemical',
                'data': chemical_data
            },
        ]

        # for items in month_sale_list:
        #    if 'Ananta' in items.get('customer_classification'):

        new_end_date = self.date_plus_one(end_date)

        month_wise_inv_refund_query = "select sum(account_invoice.amount_total) as total,sale_order.customer_classification as customer_classification,account_invoice.type ,date_part('month',account_invoice.create_date) as inv_month_wise from account_invoice,sale_order_invoice_rel,sale_order where  (sale_order.customer_classification LIKE 'Retail%') and account_invoice.id=sale_order_invoice_rel.invoice_id and sale_order_invoice_rel.order_id=sale_order.id and account_invoice.type in ('out_invoice','out_refund') and account_invoice.state  in ('open','paid') and account_invoice.create_date>='{0}' and account_invoice.create_date <='{1}' group by account_invoice.type,inv_month_wise,sale_order.customer_classification  order by account_invoice.type,inv_month_wise".format(
            start_date, new_end_date)

        # cr.execute(month_wise_inv_refund_query, (start_date, new_end_date))
        cr.execute(month_wise_inv_refund_query)

        month_inv_list = cr.dictfetchall()

        inv_month_list = []
        ref_inv_month_list = []

        for items in month_inv_list:
            if items.get('type') == 'out_invoice':
                if items.get('inv_month_wise') not in inv_month_list:
                    inv_month_list.append(items.get('inv_month_wise'))
                else:
                    ref_inv_month_list.append(items.get('inv_month_wise'))

        inv_month_list = list(dict.fromkeys(inv_month_list))
        ref_inv_month_list = list(dict.fromkeys(ref_inv_month_list))
        inv_cl_sale_dict = {}
        ref_cl_sale_dict = {}

        ### For INvoice
        for inv_month in inv_month_list:

            inv_grocery_sale = 0
            inv_stationary_sale = 0
            inv_electronics_sale = 0
            inv_mobile_sale = 0
            inv_others_sale = 0
            inv_chemical_sale = 0
            inv_general_sale = 0
            for items in month_inv_list:
                if inv_month == items.get('inv_month_wise') and items.get('type') == 'out_invoice':
                    if items.get('customer_classification').startswith('Retail') and 'Grocery' in \
                            items.get('customer_classification').split()[2]:
                        inv_grocery_sale += items.get('total')
                    elif items.get('customer_classification').startswith('Retail') and 'Stationary' in \
                            items.get('customer_classification').split()[2]:
                        inv_stationary_sale += items.get('total')
                    elif items.get('customer_classification').startswith('Retail') and 'Electronics' in \
                            items.get('customer_classification').split()[2]:
                        inv_electronics_sale += items.get('total')
                    elif items.get('customer_classification').startswith('Retail') and 'Mobile' in \
                            items.get('customer_classification').split()[2]:
                        inv_mobile_sale += items.get('total')
                    elif items.get('customer_classification').startswith('Retail') and 'Industrial' in \
                            items.get('customer_classification').split()[2]:
                        inv_chemical_sale += items.get('total')
                    # elif 'SOHO' in items.get('customer_classification'):
                    #     inv_soho_sale += items.get('total')
                    else:
                        if items.get('customer_classification').startswith('Retail'):
                            inv_others_sale += items.get('total')

            inv_cl_sale_dict[inv_month] = [inv_grocery_sale, inv_stationary_sale, inv_electronics_sale, inv_mobile_sale,
                                           inv_others_sale, inv_chemical_sale]

        inv_grocery_data = []
        inv_stationary_data = []
        inv_electronics_data = []
        inv_mobile_data = []
        inv_others_data = []
        inv_chemical_data = []
        for k, v in inv_cl_sale_dict.iteritems():
            prepare_list = []
            inv_grocery_data.append(v[0])
            inv_stationary_data.append(v[1])
            inv_electronics_data.append(v[2])
            inv_mobile_data.append(v[3])
            inv_others_data.append(v[4])
            inv_chemical_data.append(v[5])

        montwise_inv_final_list = [{
            'name': 'Grocery',
            'data': inv_grocery_data
        },
            {
                'name': 'Stationary',
                'data': inv_stationary_data
            },
            {
                'name': 'Electronics',
                'data': inv_electronics_data
            },
            {
                'name': 'Mobile Accessories',
                'data': inv_mobile_data
            },
            {
                'name': 'Others',
                'data': inv_others_data
            },
            # {
            #     'name': 'SOHO',
            #     'data': inv_soho_data
            # },
            {
                'name': 'Industrial & Chemical',
                'data': inv_chemical_data
            },
        ]

        ### For Invoice Ends Here

        ### For Refund Starts Here
        for ref_month in ref_inv_month_list:

            ref_grocery_sale = 0
            ref_stationary_sale = 0
            ref_electronics_sale = 0
            ref_mobile_sale = 0
            ref_others_sale = 0
            ref_chemical_sale = 0
            for items in month_inv_list:
                if ref_month == items.get('inv_month_wise') and items.get('type') == 'out_refund':
                    if items.get('customer_classification').startswith('Retail') and 'Grocery' in \
                            items.get('customer_classification').split()[2]:
                        ref_grocery_sale += items.get('total')
                    elif items.get('customer_classification').startswith('Retail') and 'Stationary' in \
                            items.get('customer_classification').split()[2]:
                        ref_stationary_sale += items.get('total')
                    elif items.get('customer_classification').startswith('Retail') and 'Electronics' in \
                            items.get('customer_classification').split()[2]:
                        ref_electronics_sale += items.get('total')
                    elif items.get('customer_classification').startswith('Retail') and 'Mobile' in \
                            items.get('customer_classification').split()[2]:
                        ref_mobile_sale += items.get('total')
                    elif items.get('customer_classification').startswith('Retail') and 'Industrial' in \
                            items.get('customer_classification').split()[2]:
                        ref_chemical_sale += items.get('total')
                    # elif 'SOHO' in items.get('customer_classification'):
                    #     ref_soho_sale += items.get('total')
                    else:
                        if items.get('customer_classification').startswith('Retail'):
                            ref_others_sale += items.get('total')

            ref_cl_sale_dict[ref_month] = [ref_grocery_sale, ref_stationary_sale, ref_electronics_sale, ref_mobile_sale,
                                           ref_others_sale, ref_chemical_sale]

        ref_grocery_data = []
        ref_stationary_data = []
        ref_electronics_data = []
        ref_mobile_data = []
        ref_others_data = []
        ref_chemical_data = []
        ref_soho_data = []
        ref_sme_data = []
        for k, v in ref_cl_sale_dict.iteritems():
            prepare_list = []
            ref_grocery_data.append(v[0])
            ref_stationary_data.append(v[1])
            ref_electronics_data.append(v[2])
            ref_mobile_data.append(v[3])
            ref_others_data.append(v[4])
            ref_chemical_data.append(v[5])

        montwise_refund_final_list = [{
            'name': 'Grocery',
            'data': ref_grocery_data
        },
            {
                'name': 'Stationary',
                'data': ref_stationary_data
            },
            {
                'name': 'Electronics',
                'data': ref_electronics_data
            },
            {
                'name': 'Mobile Accessories',
                'data': ref_mobile_data
            },
            {
                'name': 'Others',
                'data': ref_others_data
            },
            # {
            #     'name': 'SOHO',
            #     'data': ref_soho_data
            # },
            {
                'name': 'Industrial & Chemical',
                'data': ref_chemical_data
            },
        ]

        ### For Refund Ends Here

        ## For Net Invoice

        net_grocery = [str((x - y)) for x, y in zip(inv_grocery_data, ref_grocery_data)]
        net_stationary = [str((x - y)) for x, y in zip(inv_stationary_data, ref_stationary_data)]
        net_electronics = [str((x - y)) for x, y in zip(inv_electronics_data, ref_electronics_data)]
        net_mobile = [str((x - y)) for x, y in zip(inv_mobile_data, ref_mobile_data)]
        net_others = [str((x - y)) for x, y in zip(inv_others_data, ref_others_data)]
        net_chemical = [str((x - y)) for x, y in zip(inv_chemical_data, ref_chemical_data)]

        net_montwise_refund_final_list = [{
            'name': 'Grocery',
            'data': net_grocery
        },
            {
                'name': 'Stationary',
                'data': net_stationary
            },
            {
                'name': 'Electronics',
                'data': net_electronics
            },
            {
                'name': 'Mobile Accessories',
                'data': net_mobile
            },
            {
                'name': 'Others',
                'data': net_others
            },
            # {
            #     'name': 'SOHO',
            #     'data': net_soho
            # },
            {
                'name': 'Industrial & Chemical',
                'data': net_chemical
            },
        ]

        ## Ends Here
        # import pdb;pdb.set_trace()
        sale_month_list = [month_dict.get(x) for x in sale_month_list]

        return [sale_month_list, montwise_sale_final_list, montwise_inv_final_list, montwise_refund_final_list,
                net_montwise_refund_final_list]

    def monthon_month_count(self, cr, uid, start_date=None, end_date=None, sale_type=None, context=None):
        uid = 1
        # tmp=start_date
        # start_date=end_date
        # end_date=tmp

        # start_date='2020-04-01'
        start_date = self.six_month_ago_first_date()
        # end_date='2020-09-30'
        end_date = self.last_day_this_month()

        start_date_year = start_date.split('-')[0]
        end_date_year = end_date.split('-')[0]

        tmp_date = end_date

        current_spltted_data = tmp_date.split('-')
        current_year = int(current_spltted_data[0]) % 2000
        previous_year = (int(current_spltted_data[0]) % 2000) - 1
        current_month = int(current_spltted_data[1])

        # if current_month > 6 and int(start_date_year)!=int(end_date_year):
        #     previous_year = previous_year
        # else:
        #     previous_year = current_year
        if current_month > 6:
            previous_year = current_month
        # if current_month > 6:
        #     previous_year = current_month

        customer_classification = ['Grocery', 'Stationary', 'Electronics', 'Mobile Accessories',
                                   'Industrial & Chemical', 'Others']
        month_dict = {
            13: "Jan' " + str(current_year),
            2: "Feb' " + str(current_year),
            3: "Mar' " + str(current_year),
            4: "Apr' " + str(current_year),
            5: "May' " + str(current_year),
            6: "Jun' " + str(current_year),
            7: "Jul' " + str(previous_year),
            8: "Aug' " + str(previous_year),
            9: "Sep' " + str(previous_year),
            10: "Oct' " + str(previous_year),
            11: "Nov' " + str(previous_year),
            12: "Dec' " + str(previous_year)
        }

        month_wise_sale_query = "select count(amount_total) as total ,customer_classification,  date_part('month',date_confirm) as month_wise from sale_order where state in ('done','progress','manual','shipping_except','invoice_except' ) and (customer_classification LIKE 'Retail%') and date_confirm>='{0}' and date_confirm<='{1}' group by month_wise, customer_classification order by month_wise".format(
            start_date, end_date)

        # cr.execute(month_wise_sale_query, (start_date, end_date))
        cr.execute(month_wise_sale_query)

        month_sale_list = cr.dictfetchall()

        sale_month_list = []

        for items in month_sale_list:
            if items.get('month_wise') not in sale_month_list:
                sale_month_list.append(items.get('month_wise'))
        sale_month_list = list(dict.fromkeys(sale_month_list))

        cl_sale_dict = {}

        for sale_month in sale_month_list:
            tmp_value = []
            grocery_sale = 0
            stationary_sale = 0
            electronics_sale = 0
            mobile_sale = 0
            others_sale = 0
            chemical_sale = 0
            soho_sale = 0
            sme_sale = 0
            for items in month_sale_list:
                if sale_month == items.get('month_wise'):
                    if items.get('customer_classification').startswith('Retail') and 'Grocery' in \
                            items.get('customer_classification').split()[2]:
                        grocery_sale += items.get('total')
                    elif items.get('customer_classification').startswith('Retail') and 'Stationary' in \
                            items.get('customer_classification').split()[2]:
                        stationary_sale += items.get('total')
                    elif items.get('customer_classification').startswith('Retail') and 'Electronics' in \
                            items.get('customer_classification').split()[2]:
                        electronics_sale += items.get('total')
                    elif items.get('customer_classification').startswith('Retail') and 'Mobile' in \
                            items.get('customer_classification').split()[2]:
                        mobile_sale += items.get('total')
                    elif items.get('customer_classification').startswith('Retail') and 'Industrial' in \
                            items.get('customer_classification').split()[2]:
                        chemical_sale += items.get('total')
                    # elif 'SOHO' in items.get('customer_classification'):
                    #     soho_sale += items.get('total')
                    else:
                        if items.get('customer_classification').startswith('Retail'):
                            others_sale += items.get('total')

            cl_sale_dict[sale_month] = [grocery_sale, stationary_sale, electronics_sale, mobile_sale, others_sale,
                                        chemical_sale]

        grocery_data = []
        stationary_data = []
        electronics_data = []
        mobile_data = []
        others_data = []
        chemical_data = []
        soho_data = []
        sme_data = []
        for k, v in cl_sale_dict.iteritems():
            prepare_list = []
            grocery_data.append(v[0])
            stationary_data.append(v[1])
            electronics_data.append(v[2])
            mobile_data.append(v[3])
            others_data.append(v[4])
            chemical_data.append(v[5])
        montwise_sale_final_list = [{
            'name': 'Grocery',
            'data': grocery_data
        },
            {
                'name': 'Stationary',
                'data': stationary_data
            },
            {
                'name': 'Electronics',
                'data': electronics_data
            },
            {
                'name': 'Mobile',
                'data': mobile_data
            },
            {
                'name': 'Others',
                'data': others_data
            },
            # {
            #     'name': 'SOHO',
            #     'data': soho_data
            # },
            {
                'name': 'Industrial & Chemical',
                'data': chemical_data
            },
        ]

        # for items in month_sale_list:
        #    if 'Ananta' in items.get('customer_classification'):

        new_end_date = self.date_plus_one(end_date)
        month_wise_inv_refund_query = "select count(account_invoice.amount_total) as total,sale_order.customer_classification as customer_classification,account_invoice.type ,date_part('month',account_invoice.create_date) as inv_month_wise from account_invoice,sale_order_invoice_rel,sale_order where (sale_order.customer_classification LIKE 'Retail%') and account_invoice.id=sale_order_invoice_rel.invoice_id and sale_order_invoice_rel.order_id=sale_order.id and account_invoice.type in ('out_invoice','out_refund') and account_invoice.state  in ('open','paid') and account_invoice.create_date>='{0}' and account_invoice.create_date <='{1}' group by account_invoice.type,inv_month_wise,sale_order.customer_classification  order by account_invoice.type,inv_month_wise".format(
            start_date, new_end_date)

        # cr.execute(month_wise_inv_refund_query, (start_date, new_end_date))
        cr.execute(month_wise_inv_refund_query)

        month_inv_list = cr.dictfetchall()

        inv_month_list = []
        ref_inv_month_list = []

        for items in month_inv_list:
            if items.get('type') == 'out_invoice':
                if items.get('inv_month_wise') not in inv_month_list:
                    inv_month_list.append(items.get('inv_month_wise'))
                else:
                    ref_inv_month_list.append(items.get('inv_month_wise'))

        inv_month_list = list(dict.fromkeys(inv_month_list))
        ref_inv_month_list = list(dict.fromkeys(ref_inv_month_list))
        inv_cl_sale_dict = {}
        ref_cl_sale_dict = {}

        ### For INvoice
        for inv_month in inv_month_list:

            inv_grocery_sale = 0
            inv_stationary_sale = 0
            inv_electronics_sale = 0
            inv_mobile_sale = 0
            inv_others_sale = 0
            inv_chemical_sale = 0

            for items in month_inv_list:
                if inv_month == items.get('inv_month_wise') and items.get('type') == 'out_invoice':
                    if items.get('customer_classification').startswith('Retail') and 'Grocery' in \
                            items.get('customer_classification').split()[2]:
                        inv_grocery_sale += items.get('total')
                    elif items.get('customer_classification').startswith('Retail') and 'Stationary' in \
                            items.get('customer_classification').split()[2]:
                        inv_stationary_sale += items.get('total')
                    elif items.get('customer_classification').startswith('Retail') and 'Electronics' in \
                            items.get('customer_classification').split()[2]:
                        inv_electronics_sale += items.get('total')
                    elif items.get('customer_classification').startswith('Retail') and 'Mobile' in \
                            items.get('customer_classification').split()[2]:
                        inv_mobile_sale += items.get('total')
                    elif items.get('customer_classification').startswith('Retail') and 'Industrial' in \
                            items.get('customer_classification').split()[2]:
                        inv_chemical_sale += items.get('total')
                    # elif 'SOHO' in items.get('customer_classification'):
                    #     inv_soho_sale += items.get('total')
                    else:
                        if items.get('customer_classification').startswith('Retail'):
                            inv_others_sale += items.get('total')

            inv_cl_sale_dict[inv_month] = [inv_grocery_sale, inv_stationary_sale, inv_electronics_sale, inv_mobile_sale,
                                           inv_others_sale, inv_chemical_sale]

        inv_grocery_data = []
        inv_stationary_data = []
        inv_electronics_data = []
        inv_mobile_data = []
        inv_others_data = []
        inv_chemical_data = []

        for k, v in inv_cl_sale_dict.iteritems():
            prepare_list = []
            inv_grocery_data.append(v[0])
            inv_stationary_data.append(v[1])
            inv_electronics_data.append(v[2])
            inv_mobile_data.append(v[3])
            inv_others_data.append(v[4])
            inv_chemical_data.append(v[5])

        montwise_inv_final_list = [{
            'name': 'Grocery',
            'data': inv_grocery_data
        },
            {
                'name': 'Stationary',
                'data': inv_stationary_data
            },
            {
                'name': 'Electronics',
                'data': inv_electronics_data
            },
            {
                'name': 'Mobile',
                'data': inv_mobile_data
            },
            {
                'name': 'Others',
                'data': inv_others_data
            },
            # {
            #     'name': 'SOHO',
            #     'data': inv_soho_data
            # },
            {
                'name': 'Industrial & Chemical',
                'data': inv_chemical_data
            },
        ]

        ### For Invoice Ends Here

        ### For Refund Starts Here
        for ref_month in ref_inv_month_list:

            ref_grocery_sale = 0
            ref_stationary_sale = 0
            ref_electronics_sale = 0
            ref_mobile_sale = 0
            ref_others_sale = 0
            ref_chemical_sale = 0

            for items in month_inv_list:
                if ref_month == items.get('inv_month_wise') and items.get('type') == 'out_invoice':
                    if items.get('customer_classification').startswith('Retail') and 'Grocery' in \
                            items.get('customer_classification').split()[2]:
                        ref_grocery_sale += items.get('total')
                    elif items.get('customer_classification').startswith('Retail') and 'Stationary' in \
                            items.get('customer_classification').split()[2]:
                        ref_stationary_sale += items.get('total')
                    elif items.get('customer_classification').startswith('Retail') and 'Electronics' in \
                            items.get('customer_classification').split()[2]:
                        ref_electronics_sale += items.get('total')

                    elif items.get('customer_classification').startswith('Retail') and 'Mobile' in \
                            items.get('customer_classification').split()[2]:
                        ref_mobile_sale += items.get('total')
                    elif items.get('customer_classification').startswith('Retail') and 'Industrial' in \
                            items.get('customer_classification').split()[2]:
                        ref_chemical_sale += items.get('total')
                    # elif 'SOHO' in items.get('customer_classification'):
                    #     ref_soho_sale += items.get('total')
                    else:
                        if items.get('customer_classification').startswith('Retail'):
                            ref_others_sale += items.get('total')

            ref_cl_sale_dict[ref_month] = [ref_grocery_sale, ref_stationary_sale, ref_electronics_sale, ref_mobile_sale,
                                           ref_others_sale, ref_chemical_sale]

        ref_grocery_data = []
        ref_stationary_data = []
        ref_electronics_data = []
        ref_mobile_data = []
        ref_others_data = []
        ref_chemical_data = []

        for k, v in ref_cl_sale_dict.iteritems():
            prepare_list = []
            ref_grocery_data.append(v[0])
            ref_stationary_data.append(v[1])
            ref_electronics_data.append(v[2])
            ref_mobile_data.append(v[3])
            ref_others_data.append(v[4])
            ref_chemical_data.append(v[5])

        montwise_refund_final_list = [{
            'name': 'Grocery',
            'data': ref_grocery_data
        },
            {
                'name': 'Stationary',
                'data': ref_stationary_data
            },
            {
                'name': 'Electronics',
                'data': ref_electronics_data
            },
            {
                'name': 'Mobile',
                'data': ref_mobile_data
            },
            {
                'name': 'Others',
                'data': ref_others_data
            },
            # {
            #     'name': 'SOHO',
            #     'data': ref_soho_data
            # },
            {
                'name': 'Industrial & Chemical',
                'data': ref_chemical_data
            },
        ]

        ### For Refund Ends Here

        ## For Net Invoice

        net_grocery = [sum(x) for x in zip(inv_grocery_data, ref_grocery_data)]
        net_stationary = [sum(x) for x in zip(inv_stationary_data, ref_stationary_data)]
        net_electronics = [sum(x) for x in zip(inv_electronics_data, ref_electronics_data)]
        net_mobile = [sum(x) for x in zip(inv_mobile_data, ref_mobile_data)]
        net_others = [sum(x) for x in zip(inv_others_data, ref_others_data)]
        net_chemical = [sum(x) for x in zip(inv_chemical_data, ref_chemical_data)]


        net_montwise_refund_final_list = [{
            'name': 'Grocery',
            'data': net_grocery
        },
            {
                'name': 'Stationary',
                'data': net_stationary
            },
            {
                'name': 'Electronics',
                'data': net_electronics
            },
            {
                'name': 'Mobile',
                'data': net_mobile
            },
            {
                'name': 'Others',
                'data': net_others
            },
            # {
            #     'name': 'SOHO',
            #     'data': net_soho
            # },
            {
                'name': 'Industrial & Chemical',
                'data': net_chemical
            },
        ]

        ## Ends Here
        sale_month_list = [month_dict.get(x) for x in sale_month_list]

        return [sale_month_list, montwise_sale_final_list, montwise_inv_final_list, montwise_refund_final_list,
                net_montwise_refund_final_list]

    def monthon_month_pos(self, cr, uid, start_date=None, end_date=None, sale_type=None, context=None):
        uid = 1
        # tmp=start_date
        # start_date=end_date
        # end_date=tmp

        # start_date='2020-04-01'
        start_date = self.six_month_ago_first_date()
        # end_date='2020-09-30'
        end_date = self.last_day_this_month()

        customer_classification = ['Grocery', 'Stationary', 'Electronics', 'Mobile Accessories', 'Others',
                                   'Industrial & Chemical']
        month_dict = {1: 'Jan', 2: 'Feb', 3: 'March', 4: 'April', 5: 'May', 6: 'June', 7: 'July', 8: 'August', 9: 'Sep',
                      10: 'Oct', 11: 'Nov', 12: 'Dec'}

        month_wise_sale_query = "select count(DISTINCT partner_id) as total ,customer_classification,  date_part('month',date_confirm) as month_wise from sale_order where state in ('done','progress','manual','shipping_except','invoice_except' ) and (customer_classification LIKE 'Retail%') and date_confirm>='{0}' and date_confirm<='{1}' group by month_wise, customer_classification order by month_wise".format(
            start_date, end_date)

        # cr.execute(month_wise_sale_query, (start_date, end_date))
        cr.execute(month_wise_sale_query)

        month_sale_list = cr.dictfetchall()

        sale_month_list = []

        for items in month_sale_list:
            if items.get('month_wise') not in sale_month_list:
                sale_month_list.append(items.get('month_wise'))
        sale_month_list = list(dict.fromkeys(sale_month_list))

        cl_sale_dict = {}

        for sale_month in sale_month_list:
            tmp_value = []
            grocery_sale = 0
            stationary_sale = 0
            electronics_sale = 0
            mobile_sale = 0
            others_sale = 0
            chemical_sale = 0
            soho_sale = 0
            sme_sale = 0
            for items in month_sale_list:
                if sale_month == items.get('month_wise'):
                    if items.get('customer_classification').startswith('Retail') and 'Grocery' in \
                            items.get('customer_classification').split()[2]:
                        grocery_sale += items.get('total')
                    elif items.get('customer_classification').startswith('Retail') and 'Stationary' in \
                            items.get('customer_classification').split()[2]:
                        stationary_sale += items.get('total')
                    elif items.get('customer_classification').startswith('Retail') and 'Electronics' in \
                            items.get('customer_classification').split()[2]:
                        electronics_sale += items.get('total')
                    elif items.get('customer_classification').startswith('Retail') and 'Mobile' in \
                            items.get('customer_classification').split()[2]:
                        mobile_sale += items.get('total')
                    elif items.get('customer_classification').startswith('Retail') and 'Industrial' in \
                            items.get('customer_classification').split()[2]:
                        chemical_sale += items.get('total')
                    # elif 'SOHO' in items.get('customer_classification'):
                    #     soho_sale += items.get('total')
                    else:
                        if items.get('customer_classification').startswith('Retail'):
                            others_sale += items.get('total')

            cl_sale_dict[sale_month] = [grocery_sale, stationary_sale, electronics_sale, mobile_sale, others_sale,
                                        chemical_sale]

        grocery_data = []
        stationary_data = []
        electronics_data = []
        mobile_data = []
        others_data = []
        chemical_data = []

        for k, v in cl_sale_dict.iteritems():
            prepare_list = []
            grocery_data.append(v[0])
            stationary_data.append(v[1])
            electronics_data.append(v[2])
            mobile_data.append(v[3])
            others_data.append(v[4])
            chemical_data.append(v[5])

        montwise_sale_final_list = [{
            'name': 'Grocery',
            'data': grocery_data
        },
            {
                'name': 'Stationary',
                'data': stationary_data
            },
            {
                'name': 'Electronics',
                'data': electronics_data
            },
            {
                'name': 'Mobile',
                'data': mobile_data
            },
            {
                'name': 'Others',
                'data': others_data
            },
            # {
            #     'name': 'SOHO',
            #     'data': soho_data
            # },
            {
                'name': 'Industrial & Chemical',
                'data': chemical_data
            },
        ]

        # for items in month_sale_list:
        #    if 'Ananta' in items.get('customer_classification'):

        new_end_date = self.date_plus_one(end_date)
        month_wise_inv_refund_query = "select count(distinct sale_order.partner_id) as total,sale_order.customer_classification as customer_classification,account_invoice.type ,date_part('month',account_invoice.create_date) as inv_month_wise from account_invoice,sale_order_invoice_rel,sale_order where (sale_order.customer_classification LIKE 'Retail%') and account_invoice.id=sale_order_invoice_rel.invoice_id and sale_order_invoice_rel.order_id=sale_order.id and account_invoice.type in ('out_invoice','out_refund') and account_invoice.state  in ('open','paid') and account_invoice.create_date>='{0}' and account_invoice.create_date <='{1}' group by account_invoice.type,inv_month_wise,sale_order.customer_classification  order by account_invoice.type,inv_month_wise".format(
            start_date, new_end_date)

        # cr.execute(month_wise_inv_refund_query, (start_date, new_end_date))
        cr.execute(month_wise_inv_refund_query)

        month_inv_list = cr.dictfetchall()

        inv_month_list = []
        ref_inv_month_list = []

        for items in month_inv_list:
            if items.get('type') == 'out_invoice':
                if items.get('inv_month_wise') not in inv_month_list:
                    inv_month_list.append(items.get('inv_month_wise'))
                else:
                    ref_inv_month_list.append(items.get('inv_month_wise'))

        inv_month_list = list(dict.fromkeys(inv_month_list))
        ref_inv_month_list = list(dict.fromkeys(ref_inv_month_list))
        inv_cl_sale_dict = {}
        ref_cl_sale_dict = {}

        ### For INvoice
        for inv_month in inv_month_list:

            inv_grocery_sale = 0
            inv_stationary_sale = 0
            inv_electronics_sale = 0
            inv_mobile_sale = 0
            inv_others_sale = 0
            inv_chemical_sale = 0

            for items in month_inv_list:
                if inv_month == items.get('inv_month_wise') and items.get('type') == 'out_invoice':
                    if items.get('customer_classification').startswith('Retail') and 'Grocery' in \
                            items.get('customer_classification').split()[2]:
                        inv_grocery_sale += items.get('total')
                    elif items.get('customer_classification').startswith('Retail') and 'Stationary' in \
                            items.get('customer_classification').split()[2]:
                        inv_stationary_sale += items.get('total')
                    elif items.get('customer_classification').startswith('Retail') and 'Electronics' in \
                            items.get('customer_classification').split()[2]:
                        inv_electronics_sale += items.get('total')
                    elif items.get('customer_classification').startswith('Retail') and 'Mobile' in \
                            items.get('customer_classification').split()[2]:
                        inv_mobile_sale += items.get('total')
                    elif items.get('customer_classification').startswith('Retail') and 'Industrial' in \
                            items.get('customer_classification').split()[2]:
                        inv_chemical_sale += items.get('total')
                    # elif 'SOHO' in items.get('customer_classification'):
                    #     inv_soho_sale += items.get('total')
                    else:
                        if items.get('customer_classification').startswith('Retail'):
                            inv_others_sale += items.get('total')

            inv_cl_sale_dict[inv_month] = [inv_grocery_sale, inv_stationary_sale, inv_electronics_sale, inv_mobile_sale,
                                           inv_others_sale, inv_chemical_sale]

        inv_grocery_data = []
        inv_stationary_data = []
        inv_electronics_data = []
        inv_mobile_data = []
        inv_others_data = []
        inv_chemical_data = []

        for k, v in inv_cl_sale_dict.iteritems():
            prepare_list = []
            inv_grocery_data.append(v[0])
            inv_stationary_data.append(v[1])
            inv_electronics_data.append(v[2])
            inv_mobile_data.append(v[3])
            inv_others_data.append(v[4])
            inv_chemical_data.append(v[5])

        montwise_inv_final_list = [{
            'name': 'Grocery',
            'data': inv_grocery_data
        },
            {
                'name': 'Stationary',
                'data': inv_stationary_data
            },
            {
                'name': 'Electronics',
                'data': inv_electronics_data
            },
            {
                'name': 'Mobile',
                'data': inv_mobile_data
            },
            {
                'name': 'Others',
                'data': inv_others_data
            },
            # {
            #     'name': 'SOHO',
            #     'data': inv_soho_data
            # },
            {
                'name': 'Industrial & Chemical',
                'data': inv_chemical_data
            },
        ]

        ### For Invoice Ends Here

        ### For Refund Starts Here
        for ref_month in ref_inv_month_list:

            ref_grocery_sale = 0
            ref_stationary_sale = 0
            ref_electronics_sale = 0
            ref_mobile_sale = 0
            ref_others_sale = 0
            ref_chemical_sale = 0

            for items in month_inv_list:
                if ref_month == items.get('inv_month_wise') and items.get('type') == 'out_invoice':
                    if items.get('customer_classification').startswith('Retail') and 'Grocery' in \
                            items.get('customer_classification').split()[2]:
                        ref_grocery_sale += items.get('total')
                    elif items.get('customer_classification').startswith('Retail') and 'Stationary' in \
                            items.get('customer_classification').split()[2]:
                        ref_stationary_sale += items.get('total')
                    elif items.get('customer_classification').startswith('Retail') and 'Electronics' in \
                            items.get('customer_classification').split()[2]:
                        ref_electronics_sale += items.get('total')

                    elif items.get('customer_classification').startswith('Retail') and 'Mobile' in \
                            items.get('customer_classification').split()[2]:
                        ref_mobile_sale += items.get('total')
                    elif items.get('customer_classification').startswith('Retail') and 'Industrial' in \
                            items.get('customer_classification').split()[2]:
                        ref_chemical_sale += items.get('total')
                    # elif 'SOHO' in items.get('customer_classification'):
                    #     ref_soho_sale += items.get('total')
                    else:
                        if items.get('customer_classification').startswith('Retail'):
                            ref_others_sale += items.get('total')

            ref_cl_sale_dict[ref_month] = [ref_grocery_sale, ref_stationary_sale, ref_electronics_sale, ref_mobile_sale,
                                           ref_others_sale, ref_chemical_sale]

        ref_grocery_data = []
        ref_stationary_data = []
        ref_electronics_data = []
        ref_mobile_data = []
        ref_others_data = []
        ref_chemical_data = []

        for k, v in ref_cl_sale_dict.iteritems():
            prepare_list = []
            ref_grocery_data.append(v[0])
            ref_stationary_data.append(v[1])
            ref_electronics_data.append(v[2])
            ref_mobile_data.append(v[3])
            ref_others_data.append(v[4])
            ref_chemical_data.append(v[5])

        montwise_refund_final_list = [{
            'name': 'Grocery',
            'data': ref_grocery_data
        },
            {
                'name': 'Stationary',
                'data': ref_stationary_data
            },
            {
                'name': 'Electronics',
                'data': ref_electronics_data
            },
            {
                'name': 'Mobile',
                'data': ref_mobile_data
            },
            {
                'name': 'Others',
                'data': ref_others_data
            },
            # {
            #     'name': 'SOHO',
            #     'data': ref_soho_data
            # },
            {
                'name': 'Industrial & Chemical',
                'data': ref_chemical_data
            },
        ]

        ### For Refund Ends Here

        ## For Net Invoice

        net_grocery = [sum(x) for x in zip(inv_grocery_data, ref_grocery_data)]
        net_stationary = [sum(x) for x in zip(inv_stationary_data, ref_stationary_data)]
        net_electronics = [sum(x) for x in zip(inv_electronics_data, ref_electronics_data)]
        net_mobile = [sum(x) for x in zip(inv_mobile_data, ref_mobile_data)]
        net_others = [sum(x) for x in zip(inv_others_data, ref_others_data)]
        net_chemical = [sum(x) for x in zip(inv_chemical_data, ref_chemical_data)]

        net_montwise_refund_final_list = [{
            'name': 'Grocery',
            'data': net_grocery
        },
            {
                'name': 'Stationary',
                'data': net_stationary
            },
            {
                'name': 'Electronics',
                'data': net_electronics
            },
            {
                'name': 'Mobile',
                'data': net_mobile
            },
            {
                'name': 'Others',
                'data': net_others
            },
            # {
            #     'name': 'SOHO',
            #     'data': net_soho
            # },
            {
                'name': 'Industrial & Chemical',
                'data': net_chemical
            }
        ]

        ## Ends Here
        sale_month_list = [month_dict.get(x) for x in sale_month_list]

        return [sale_month_list, montwise_sale_final_list, montwise_inv_final_list, montwise_refund_final_list,
                net_montwise_refund_final_list]

    def processing_all(self, cr, uid, start_date=None, end_date=None, sale_type=None, context=None):

        sale_obj = self.pool.get('management.dashboard')
        uid = 1

        sale_list = ''
        if start_date == end_date:
            # for blank search
            sale_list = sale_obj.search(cr, uid, [('date_confirm', '=', start_date), ('state', '!=', 'cancel')])
        elif start_date != end_date:
            sale_list = sale_obj.search(cr, uid, [('date_confirm', '>', end_date), ('state', '!=', 'cancel')])

        processing_so_list = []
        if sale_type == 'processingAll':
            processing_so_list = sale_obj.search(cr, uid, [('state', '=', 'progress')])
        elif sale_type == 'processingAnanta':
            processing_so_list = sale_obj.search(cr, uid, [('partner_classification', '=', 'Ananta'),
                                                           ('state', '=', 'progress')])
        elif sale_type == 'processingCorp':
            processing_so_list = sale_obj.search(cr, uid, [('partner_classification', '=', 'Corporate'),
                                                           ('state', '=', 'progress')])
        elif sale_type == 'processingRetail':
            processing_so_list = sale_obj.search(cr, uid,
                                                 [ '&', ('partner_classification', 'ilike', '%Retail'),
                                                  ('state', '=', 'progress')])
        elif sale_type == 'processingOnline':
            processing_so_list = sale_obj.search(cr, uid, [('partner_classification', '=', 'General'),
                                                           ('state', '=', 'progress')])
        else:
            processing_so_list = sale_obj.search(cr, uid, [('state', '=', 'progress')])

        processing_count_labels = ['0 to 1 Days', '2 to 3 Days', '4 Days Above']
        """
        processing_sos = sale_obj.browse(cr, uid, processing_so_list)


        processing_count_values = [0, 0, 0]
        processing_order_values = [0, 0, 0]

        today_date = datetime.datetime.today()

        for so in processing_sos:
            confirm_date = datetime.datetime.strptime(str(so.date_confirm), '%Y-%m-%d')

            pro_date = (today_date - confirm_date).days

            if pro_date < 2:
                processing_count_values[0] += 1
                processing_order_values[0] += so.amount_total
            elif pro_date < 4:
                processing_count_values[1] += 1
                processing_order_values[1] += so.amount_total
            elif pro_date > 3:
                processing_count_values[2] += 1
                processing_order_values[2] += so.amount_total

        # import pdb;pdb.set_trace()
        """

        processing_count_values, processing_order_values = self.processing_order_date(cr, uid, processing_so_list)

        return [processing_count_labels, processing_count_values, processing_order_values]

    def processing_order_date(self, cr, uid, processing_so_list):

        sale_obj = self.pool.get('management.dashboard')
        uid = 1

        processing_sos = sale_obj.browse(cr, uid, processing_so_list)

        # processing_count_labels = ['0 to 1 Days', '2 to 3 Days', '4 Days Above']
        processing_count_values = [0, 0, 0]
        processing_order_values = [0, 0, 0]

        today_date = datetime.datetime.today()

        for so in processing_sos:
            confirm_date = datetime.datetime.strptime(str(so.date_confirm), '%Y-%m-%d')

            pro_date = (today_date - confirm_date).days

            if pro_date < 2:
                processing_count_values[0] += 1
                processing_order_values[0] += so.amount_total
            elif pro_date < 4:
                processing_count_values[1] += 1
                processing_order_values[1] += so.amount_total
            elif pro_date > 3:
                processing_count_values[2] += 1
                processing_order_values[2] += so.amount_total

        return processing_count_values, processing_order_values

    def retail_dashboard_data_preparation(self, cr, uid, start_date=None, end_date=None, sale_type=None, context=None):

        # ------------------------- Sale orders from date --------------
        # sale_obj = self.pool.get('sale.order')
        sale_obj = self.pool.get('management.dashboard')
        uid = 1

        sale_list = ''
        if start_date == end_date:
            # for blank search
            sale_list = sale_obj.search(cr, uid, [('date_confirm', '=', start_date), ('state', '!=', 'cancel')])
        elif start_date != end_date:
            sale_list = sale_obj.search(cr, uid, [('date_confirm', '>', end_date), ('state', '!=', 'cancel')])
        # elif start_date != end_date and sale_type == 'all':

        # today_to_monthly_sales_list = self.retail_today_to_monthly_sales_trending(cr, uid, start_date=start_date,
        #                                                                    end_date=end_date)

        # Processing ----------------
        # import pdb;pdb.set_trace()
        processing_count_labels, processing_count_values, processing_order_values = self.processing_all(cr, uid,
                                                                                                        start_date=start_date,
                                                                                                        end_date=end_date,
                                                                                                        sale_type=sale_type)

        catg_data = self.category_wise_invoice_margin(cr, uid, start_date=start_date, end_date=end_date)
        categ_list = catg_data[0]
        cost_list = catg_data[1]
        sale_list = catg_data[2]
        margin_list = catg_data[3]
        total_sale = catg_data[4]
        total_margin = catg_data[5]
        margin = float(str(catg_data[7]))
        margin_percent = float(str(catg_data[8]))

        classification_wise_sales = catg_data[6]

        c_wise_label = []
        c_wise_values = []

        classification_list = []
        classification_wise_profit_value_list = []
        classification_wise_profit_percent_list = []

        for k, v in classification_wise_sales.iteritems():
            if v[0] != 0 and v[1] != 0:
                c_wise_label.append(k)
                classification_list.append(k)
                profit = int((v[0] - v[1]))
                classification_wise_profit_value_list.append(profit)
                pnl = round((((v[0] - v[1]) / v[0]) * 100), 2)
                classification_wise_profit_percent_list.append(pnl)
                c_wise_values.append(pnl)

        turn_oder_data = self.tunr_over_data_calculation(cr, uid, start_date=start_date, end_date=end_date)
        # month_on_month_oder_data = self.monthon_month_value(cr, uid, start_date=start_date, end_date=end_date)
        #
        # month_on_month_registered = self.monthon_month_registered(cr, uid, start_date=start_date, end_date=end_date)
        # month_on_month_unique_pos_data = self.monthon_month_unique_pos(cr, uid, start_date=start_date, end_date=end_date)
        # month_on_month_active_pos_data = self.monthon_month_active_pos(cr, uid, start_date=start_date, end_date=end_date)
        # month_on_month_new_pos_data = self.monthon_month_new_pos(cr, uid, start_date=start_date, end_date=end_date)
        result_list = [
            categ_list,
            [str(x) for x in cost_list],
            [str(x) for x in sale_list],
            [str(x) for x in margin_list],
            total_sale,
            total_margin,
            c_wise_label,
            c_wise_values,
            turn_oder_data[0],
            turn_oder_data[1],
            turn_oder_data[2]

        ]
        # sec_list = [processing_count_labels,
        #             processing_count_values,
        #             processing_order_values,
        #             margin,
        #             margin_percent]

        new_bar_chart = [classification_list, classification_wise_profit_value_list,
                         classification_wise_profit_percent_list]

        final_list = result_list + new_bar_chart

        # # top 10 sku
        # top10_product_names, top10_product_values = self.calculate_top_10_sku(cr, uid,start_date=end_date, end_date=start_date)
        # final_list.append(top10_product_names)
        # final_list.append(top10_product_values)
        # final_list = final_list + month_on_month_unique_pos_data +month_on_month_active_pos_data  + month_on_month_new_pos_data + month_on_month_registered


        return final_list

    # def retail_dashboard_data_preparation(self, cr, uid, start_date=None, end_date=None, sale_type=None, context=None):
    #
    #     # ------------------------- Sale orders from date --------------
    #     # sale_obj = self.pool.get('sale.order')
    #     sale_obj = self.pool.get('management.dashboard')
    #     uid = 1
    #
    #     sale_list = ''
    #     if start_date == end_date:
    #         # for blank search
    #         sale_list = sale_obj.search(cr, uid, [('date_confirm', '=', start_date), ('state', '!=', 'cancel')])
    #     elif start_date != end_date:
    #         sale_list = sale_obj.search(cr, uid, [('date_confirm', '>', end_date), ('state', '!=', 'cancel')])
    #     # elif start_date != end_date and sale_type == 'all':
    #
    #     today_to_monthly_sales_list = self.retail_today_to_monthly_sales_trending(cr, uid, start_date=start_date,
    #                                                                               end_date=end_date)
    #
    #     # Processing ----------------
    #     # import pdb;pdb.set_trace()
    #     processing_count_labels, processing_count_values, processing_order_values = self.processing_all(cr, uid,
    #                                                                                                     start_date=start_date,
    #                                                                                                     end_date=end_date,
    #                                                                                                     sale_type=sale_type)
    #
    #     catg_data = self.category_wise_invoice_margin(cr, uid, start_date=start_date, end_date=end_date)
    #     categ_list = catg_data[0]
    #     cost_list = catg_data[1]
    #     sale_list = catg_data[2]
    #     margin_list = catg_data[3]
    #     total_sale = catg_data[4]
    #     total_margin = catg_data[5]
    #     margin = float(str(catg_data[7]))
    #     margin_percent = float(str(catg_data[8]))
    #
    #     classification_wise_sales = catg_data[6]
    #
    #     c_wise_label = []
    #     c_wise_values = []
    #
    #     classification_list = []
    #     classification_wise_profit_value_list = []
    #     classification_wise_profit_percent_list = []
    #
    #     for k, v in classification_wise_sales.iteritems():
    #         if v[0] != 0 and v[1] != 0:
    #             c_wise_label.append(k)
    #             classification_list.append(k)
    #             profit = int((v[0] - v[1]))
    #             classification_wise_profit_value_list.append(profit)
    #             pnl = round((((v[0] - v[1]) / v[0]) * 100), 2)
    #             classification_wise_profit_percent_list.append(pnl)
    #             c_wise_values.append(pnl)
    #
    #     turn_oder_data = self.tunr_over_data_calculation(cr, uid, start_date=start_date, end_date=end_date)
    #     month_on_month_oder_data = self.monthon_month_value(cr, uid, start_date=start_date, end_date=end_date)
    #
    #     month_on_month_registered = self.monthon_month_registered(cr, uid, start_date=start_date, end_date=end_date)
    #     month_on_month_unique_pos_data = self.monthon_month_unique_pos(cr, uid, start_date=start_date,
    #                                                                    end_date=end_date)
    #     month_on_month_active_pos_data = self.monthon_month_active_pos(cr, uid, start_date=start_date,
    #                                                                    end_date=end_date)
    #     month_on_month_new_pos_data = self.monthon_month_new_pos(cr, uid, start_date=start_date, end_date=end_date)
    #     result_list = [
    #         categ_list,
    #         [str(x) for x in cost_list],
    #         [str(x) for x in sale_list],
    #         [str(x) for x in margin_list],
    #         total_sale,
    #         total_margin,
    #         c_wise_label,
    #         c_wise_values,
    #         turn_oder_data[0],
    #         turn_oder_data[1],
    #         turn_oder_data[2]
    #
    #     ]
    #     sec_list = [processing_count_labels,
    #                 processing_count_values,
    #                 processing_order_values,
    #                 margin,
    #                 margin_percent]
    #
    #     new_bar_chart = [classification_list, classification_wise_profit_value_list,
    #                      classification_wise_profit_percent_list]
    #
    #     final_list = today_to_monthly_sales_list + result_list + month_on_month_oder_data + sec_list + new_bar_chart
    #
    #     # top 10 sku
    #     top10_product_names, top10_product_values = self.calculate_top_10_sku(cr, uid, start_date=end_date,
    #                                                                           end_date=start_date)
    #     final_list.append(top10_product_names)
    #     final_list.append(top10_product_values)
    #     final_list = final_list + month_on_month_unique_pos_data + month_on_month_active_pos_data + month_on_month_new_pos_data + month_on_month_registered
    #
    #     return final_list

    def calculate_top_10_sku(self, cr, uid, start_date=None, end_date=None, sale_type=None, context=None):

        parameter_list = []
        done_parameter_list = []

        today_date = datetime.datetime.today().date()
        old_st_date = today_date.replace(day=1)
        old_end_date = datetime.datetime.today().date()
        tmp_old_st_date = old_st_date.strftime('%Y-%m-%d')
        tmp_old_end_date = old_end_date.strftime('%Y-%m-%d')
        st_date = tmp_old_st_date + str(' 00:00:00')
        en_date = tmp_old_end_date + str(' 23:59:59')
        parameter_list = [st_date, en_date]
        sale_type = sale_type

        done_parameter_list = [tmp_old_st_date, tmp_old_end_date]

        if start_date is not None and end_date is not None:
            done_parameter_list = [start_date, end_date]
            parameter_list = []
            start_date1 = datetime.datetime.strptime(start_date, '%Y-%m-%d')
            end_date1 = datetime.datetime.strptime(end_date, '%Y-%m-%d')

            start_date_time = start_date + ' 00:00:00'
            end_date_time = end_date + ' 23:59:59'
            done_parameter_list = [start_date_time, end_date_time]

        # query = "select stock_move.id as moved_id,stock_move.product_id,stock_picking.date_done,stock_picking.picking_type_id,stock_move.product_uom_qty,sale_order_line.price_unit,sale_order_line.order_partner_id,sale_order.client_order_ref,sale_order.date_confirm,sale_order_line.name, sale_order.name as odoo_no,(select product_category.name from product_category,product_template,product_product where product_category.id = product_template.categ_id and product_template.id = product_product.product_tmpl_id and product_product.id = stock_move.product_id ) as category,(select sum((sq.qty*sm.price_unit)) as total from stock_quant_move_rel as sqm,stock_move as sm, stock_quant as sq where sqm.quant_id in ((select sq2.id from stock_quant_move_rel as sqmr2, stock_quant as sq2 where sqmr2.move_id=stock_move.id and sqmr2.quant_id = sq2.id)) and sqm.move_id = sm.id and sm.purchase_line_id >0 and sq.id=sqm.quant_id group by sq.product_id)as total_price , (select name from stock_warehouse where id=stock_move.warehouse_id) as warhouse_name,account_invoice.number as invoice_number, account_invoice.date_invoice as invoice_date  from stock_move,stock_picking,sale_order_line,sale_order,account_invoice where stock_move.picking_id=stock_picking.id and stock_move.sale_line_id = sale_order_line.id and sale_order_line.order_id = sale_order.id  and stock_picking.name = account_invoice.origin and stock_picking.state='done'  and stock_picking.date_done >= %s and stock_picking.date_done <= %s"
        #
        # cr.execute(query, (st_date, end_date))
        # sold_objects = cr.dictfetchall()



        total_query = "select stock_picking.picking_type_id,sale_order.partner_id,sum((select sum((sq.qty*sm.price_unit)) as total from stock_quant_move_rel as sqm,stock_move as sm, stock_quant as sq where sqm.quant_id in ((select sq2.id from stock_quant_move_rel as sqmr2, stock_quant as sq2 where sqmr2.move_id=stock_move.id and sqmr2.quant_id = sq2.id)) and sqm.move_id = sm.id and sm.purchase_line_id >0 and sq.id=sqm.quant_id group by sq.product_id))as total_price ,sum((stock_move.product_uom_qty*sale_order_line.price_unit)) as total_sale from stock_move,stock_picking,sale_order_line,sale_order where (sale_order.customer_classification LIKE 'Retail%') and stock_move.picking_id=stock_picking.id and stock_move.sale_line_id = sale_order_line.id and sale_order.id= sale_order_line.order_id and stock_picking.state='done'  and stock_picking.date_done >= '{0}' and stock_picking.date_done <= '{1}' group by stock_picking.picking_type_id,sale_order.partner_id".format(
            done_parameter_list[0], done_parameter_list[1])

        # cr.execute(total_query, (done_parameter_list))
        cr.execute(total_query)
        total_objects = cr.dictfetchall()

        partner_list = []
        product_id_list = []
        final_product_list = {}
        final_company_list = {}

        partner_obj = self.pool("res.partner")

        # user_ids = partner_obj.search(cr, uid, domain, context=context)


        for items in total_objects:
            partner_list.append(items.get('partner_id'))

        p_obj = partner_obj.browse(cr, uid, partner_list, context=context)

        for p_items in p_obj:
            if p_items.parent_id:
                final_company_list[p_items.id] = {
                    'customer_name': p_items.name,
                    'company_name': p_items.parent_id.name,
                    'company_code': p_items.parent_code,
                    'classification': p_items.x_classification,
                    'email': p_items.email,
                    'company_email': p_items.parent_id.email,
                }
            else:
                final_company_list[p_items.id] = {
                    'customer_name': p_items.name,
                    'company_name': p_items.parent_id.name,
                    'classification': p_items.parent_id.x_classification if p_items.parent_id.x_classification else p_items.x_classification,
                    'email': p_items.parent_id.email if p_items.parent_id.email else p_items.email,
                    'company_code': p_items.parent_id.parent_code,
                    'company_email': p_items.parent_id.email,
                }

        result_list = []

        ananta_cost_value = 0.0
        ananta_sale_value = 0.0
        non_ananta_sale_value = 0.0
        non_ananta_cost_value = 0.0
        retail_sale_value = 0.0
        retail_cost_value = 0.0

        customer_wise_sale_dict = {}
        customer_wise_profit = {}
        top_ten_delivered_product = []
        top_ent_sold_products = []

        incoming_ids = []
        in_query = "select id from  stock_picking_type where code='incoming'"
        cr.execute(in_query)
        in_objects = cr.dictfetchall()

        incoming_ids = [in_item.get('id') for in_item in in_objects]

        for all_items in total_objects:

            partner_id = all_items.get('partner_id')
            abc = final_company_list.get(partner_id)
            total_sale = all_items.get('total_sale')
            if all_items.get('total_price') is not None:
                total_cost = all_items.get('total_price')
            else:
                total_cost = 0

            if all_items.get('picking_type_id') in incoming_ids:
                total_sale = total_sale * -1
                total_cost = total_cost * -1

            if customer_wise_sale_dict.get(partner_id) is not None and customer_wise_sale_dict.get(
                    partner_id) is not False:
                customer_wise_sale_dict[partner_id] = customer_wise_sale_dict[partner_id] + total_sale
            else:
                customer_wise_sale_dict[partner_id] = total_sale

            if customer_wise_profit.get(partner_id) is not None and customer_wise_profit.get(
                    partner_id) is not False:
                customer_wise_profit[partner_id] = customer_wise_profit[partner_id] + (total_sale - total_cost)
            else:
                customer_wise_profit[partner_id] = (total_sale - total_cost)

            if abc.get('classification') is not False and abc.get('classification') is not None:

                got_class = abc.get('classification').lower()

                if 'ananta' in got_class:
                    if all_items.get('picking_type_id') in incoming_ids:
                        ananta_cost_value -= (
                            all_items.get('total_price') if all_items.get('total_price') is not None else 0)
                        ananta_sale_value -= all_items.get('total_sale')

                    else:
                        ananta_cost_value += (
                            all_items.get('total_price') if all_items.get('total_price') is not None else 0)
                        ananta_sale_value += all_items.get('total_sale')
                elif 'corporate' in got_class:

                    if all_items.get('picking_type_id') in incoming_ids:
                        non_ananta_cost_value -= (
                            all_items.get('total_price') if all_items.get('total_price') is not None else 0)
                        non_ananta_sale_value -= all_items.get('total_sale')
                    else:
                        non_ananta_cost_value += (
                            all_items.get('total_price') if all_items.get('total_price') is not None else 0)
                        non_ananta_sale_value += all_items.get('total_sale')
                else:

                    if all_items.get('picking_type_id') in incoming_ids:
                        retail_cost_value -= (
                            all_items.get('total_price') if all_items.get('total_price') is not None else 0)
                        retail_sale_value -= all_items.get('total_sale')
                    else:
                        retail_cost_value += (
                            all_items.get('total_price') if all_items.get('total_price') is not None else 0)
                        retail_sale_value += all_items.get('total_sale')

        total_ananta_sale = round(ananta_sale_value, 2)
        total_profict_ananta_sale = round((total_ananta_sale - round(ananta_cost_value, 2)), 2)
        ananta_margin = 0

        if total_profict_ananta_sale > 0 and total_ananta_sale > 0:
            ananta_margin = round(((total_profict_ananta_sale / total_ananta_sale) * 100), 2)
        else:
            total_profict_ananta_sale = 0

        non_ananta_sale_value = round(non_ananta_sale_value, 2)
        total_profit_non_anant_sale = round((non_ananta_sale_value - round(non_ananta_cost_value, 2)), 2)
        non_ananta_margin = 0

        if total_profit_non_anant_sale > 0 and non_ananta_sale_value > 0:
            non_ananta_margin = round(((total_profit_non_anant_sale / non_ananta_sale_value) * 100), 2)
        else:
            total_profit_non_anant_sale = 0

        retail_sale_value = round(retail_sale_value, 2)
        retail_cost_value = round(retail_cost_value, 2)
        retail_profit = round((retail_sale_value - retail_cost_value), 2)
        retaile_margin = 0

        if retail_profit > 0 and retail_sale_value > 0:
            retaile_margin = round(((retail_profit / retail_sale_value) * 100), 2)

        top10_aompany_names = []
        top10_aompany_values = []
        to10_prfit_company_names = []
        to10_prfit_company_values = []

        top_10_sold_amount_customer_wise = sorted(customer_wise_sale_dict.iteritems(), key=lambda (k, v): (-v, k))
        top_10_profit_amount_customer_wise = sorted(customer_wise_profit.iteritems(), key=lambda (k, v): (-v, k))

        count = 0

        for items in top_10_sold_amount_customer_wise:
            if count == 10:
                break

            abc = final_company_list.get(items[0])

            if sale_type == '1' and 'Ananta' in abc.get('classification'):
                top10_aompany_names.append(abc.get('company_name'))
                top10_aompany_values.append(items[1])
                count = count + 1
            elif sale_type == '2' and 'Corporate' in abc.get('classification'):
                top10_aompany_names.append(abc.get('company_name'))
                top10_aompany_values.append(items[1])
                count = count + 1
            elif sale_type == '3' and 'Retail' in abc.get('classification'):
                top10_aompany_names.append(abc.get('company_name'))
                top10_aompany_values.append(items[1])
                count = count + 1
            elif sale_type is None or sale_type == '0':
                top10_aompany_names.append(abc.get('company_name'))
                top10_aompany_values.append(items[1])
                count = count + 1

        p_count = 0
        for items in top_10_profit_amount_customer_wise:
            if p_count == 10:
                break
            abc = final_company_list.get(items[0])

            to10_prfit_company_names.append(abc.get('company_name'))
            to10_prfit_company_values.append(items[1])
            p_count = p_count + 1

        ##Sales Person Wise Sales

        user_sold_query = "select stock_picking.picking_type_id,sale_order.custom_salesperson as user_id,sale_order.partner_id,sum((stock_move.product_uom_qty*sale_order_line.price_unit)) as total_sale from stock_move,stock_picking,sale_order_line ,sale_order where (sale_order.customer_classification LIKE 'Retail%') and stock_move.picking_id=stock_picking.id and stock_move.sale_line_id = sale_order_line.id and stock_picking.state='done'   and sale_order_line.order_id = sale_order.id and stock_picking.date_done >= '{0}' and stock_picking.date_done <= '{1}' group by stock_picking.picking_type_id,sale_order.custom_salesperson,sale_order.partner_id ".format(
            done_parameter_list[0], done_parameter_list[1])

        # cr.execute(user_sold_query, (done_parameter_list))
        cr.execute(user_sold_query)
        uesr_sold_data = cr.dictfetchall()

        user_wise_sale_dict = {}

        ## Filter data with sale type

        sold_items_partner_list = []
        sold_company_list = {}
        for user_items in uesr_sold_data:
            sold_items_partner_list.append(user_items.get('partner_id'))

        p_obj = partner_obj.browse(cr, uid, partner_list, context=context)

        for p_items in p_obj:
            if p_items.parent_id:
                sold_company_list[p_items.id] = {
                    'customer_name': p_items.name,
                    'company_name': p_items.parent_id.name,
                    'company_code': p_items.parent_code,
                    'classification': p_items.x_classification,
                    'email': p_items.email,
                    'company_email': p_items.parent_id.email,
                }
            else:
                sold_company_list[p_items.id] = {
                    'customer_name': p_items.name,
                    'company_name': p_items.parent_id.name,
                    'classification': p_items.parent_id.x_classification if p_items.parent_id.x_classification else p_items.x_classification,
                    'email': p_items.parent_id.email if p_items.parent_id.email else p_items.email,
                    'company_code': p_items.parent_id.parent_code,
                    'company_email': p_items.parent_id.email,
                }

        for user_items in uesr_sold_data:
            user_data = sold_company_list.get(user_items.get('partner_id'))

            if sale_type == '1' and 'Ananta' in user_data.get('classification'):

                total_sold = user_items.get('total_sale')
                if user_items.get('picking_type_id') in incoming_ids:
                    total_sold = total_sold * -1

                if user_wise_sale_dict.get(user_items.get('user_id')) is not False and user_wise_sale_dict.get(
                        user_items.get('user_id')) is not None:
                    user_wise_sale_dict[user_items.get('user_id')] = user_wise_sale_dict[
                                                                         user_items.get('user_id')] + total_sold
                else:
                    user_wise_sale_dict[user_items.get('user_id')] = total_sold

            elif sale_type == '2' and 'Corporate' in user_data.get('classification'):
                total_sold = user_items.get('total_sale')
                if user_items.get('picking_type_id') in incoming_ids:
                    total_sold = total_sold * -1

                if user_wise_sale_dict.get(user_items.get('user_id')) is not False and user_wise_sale_dict.get(
                        user_items.get('user_id')) is not None:
                    user_wise_sale_dict[user_items.get('user_id')] = user_wise_sale_dict[
                                                                         user_items.get('user_id')] + total_sold
                else:
                    user_wise_sale_dict[user_items.get('user_id')] = total_sold

            elif sale_type == '3' and 'Retail' in user_data.get('classification'):
                total_sold = user_items.get('total_sale')
                if user_items.get('picking_type_id') in incoming_ids:
                    total_sold = total_sold * -1

                if user_wise_sale_dict.get(user_items.get('user_id')) is not False and user_wise_sale_dict.get(
                        user_items.get('user_id')) is not None:
                    user_wise_sale_dict[user_items.get('user_id')] = user_wise_sale_dict[
                                                                         user_items.get('user_id')] + total_sold
                else:
                    user_wise_sale_dict[user_items.get('user_id')] = total_sold
            elif sale_type is None or sale_type == '0':

                total_sold = user_items.get('total_sale')
                if user_items.get('picking_type_id') in incoming_ids:
                    total_sold = total_sold * -1

                if user_wise_sale_dict.get(user_items.get('user_id')) is not False and user_wise_sale_dict.get(
                        user_items.get('user_id')) is not None:
                    user_wise_sale_dict[user_items.get('user_id')] = user_wise_sale_dict[
                                                                         user_items.get('user_id')] + total_sold
                else:
                    user_wise_sale_dict[user_items.get('user_id')] = total_sold

        ## Ends here





        user_names = []
        user_values = []

        for k, v in user_wise_sale_dict.iteritems():
            query = "select res_partner.name from res_users,res_partner where res_users.partner_id=res_partner.id and  res_users.id=%s"
            cr.execute(query, ([k]))
            res_dict = cr.dictfetchall()
            if len(res_dict) > 0:
                for res_it in res_dict:
                    user_names.append(res_it.get('name'))
                    user_values.append(v)
                    break

        top5_sales_man = []
        top5_sales_man_value = []

        top5_sales_man_dict = sorted(user_wise_sale_dict.iteritems(), key=lambda (k, v): (-v, k))[:5]

        for i_data in top5_sales_man_dict:
            query = "select res_partner.name from res_users,res_partner where res_users.partner_id=res_partner.id and  res_users.id=%s"
            cr.execute(query, ([i_data[0]]))
            res_dict = cr.dictfetchall()
            if len(res_dict) > 0:
                for res_it in res_dict:
                    top5_sales_man.append(res_it.get('name'))
                    top5_sales_man_value.append(i_data[1])
                    break

        ## Ends Here


        ## Start here Product Wise Sales


        product_query = "select stock_picking.picking_type_id,stock_move.product_id,sale_order.partner_id,sum((stock_move.product_uom_qty*sale_order_line.price_unit)) as total_sale,stock_move.name  from stock_move,stock_picking,sale_order_line ,sale_order where (sale_order.customer_classification LIKE 'Retail%') and stock_move.picking_id=stock_picking.id and stock_move.sale_line_id = sale_order_line.id and stock_picking.state='done'   and sale_order_line.order_id = sale_order.id and stock_picking.date_done >= '{0}' and stock_picking.date_done <= '{1}' group by stock_picking.picking_type_id,stock_move.product_id,sale_order.partner_id,stock_move.name".format(
            done_parameter_list[0], done_parameter_list[1])

        # cr.execute(product_query, (done_parameter_list))
        cr.execute(product_query)
        product_sold_data = cr.dictfetchall()

        product_wise_sale_dict = {}

        product_company_list = {}

        for user_items in product_sold_data:
            user_data = sold_company_list.get(user_items.get('partner_id'))
            if sale_type == '1' and 'Ananta' in user_data.get('classification'):

                total_sold = user_items.get('total_sale')
                if user_items.get('picking_type_id') in incoming_ids:
                    total_sold = total_sold * -1

                if product_wise_sale_dict.get(user_items.get('product_id')) is not False and product_wise_sale_dict.get(
                        user_items.get('product_id')) is not None:
                    product_wise_sale_dict[user_items.get('product_id')] = product_wise_sale_dict[user_items.get(
                        'product_id')] + total_sold
                else:
                    product_wise_sale_dict[user_items.get('product_id')] = total_sold
            elif sale_type == '2' and 'Corporate' in user_data.get('classification'):
                total_sold = user_items.get('total_sale')
                if user_items.get('picking_type_id') in incoming_ids:
                    total_sold = total_sold * -1

                if product_wise_sale_dict.get(user_items.get('product_id')) is not False and product_wise_sale_dict.get(
                        user_items.get('product_id')) is not None:
                    product_wise_sale_dict[user_items.get('product_id')] = product_wise_sale_dict[user_items.get(
                        'product_id')] + total_sold
                else:
                    product_wise_sale_dict[user_items.get('product_id')] = total_sold
            elif sale_type == '3' and 'Retail' in user_data.get('classification'):

                total_sold = user_items.get('total_sale')
                if user_items.get('picking_type_id') in incoming_ids:
                    total_sold = total_sold * -1

                if product_wise_sale_dict.get(user_items.get('product_id')) is not False and product_wise_sale_dict.get(
                        user_items.get('product_id')) is not None:
                    product_wise_sale_dict[user_items.get('product_id')] = product_wise_sale_dict[user_items.get(
                        'product_id')] + total_sold
                else:
                    product_wise_sale_dict[user_items.get('product_id')] = total_sold
            elif sale_type is None or sale_type == '0':

                total_sold = user_items.get('total_sale')
                if user_items.get('picking_type_id') in incoming_ids:
                    total_sold = total_sold * -1

                if product_wise_sale_dict.get(user_items.get('product_id')) is not False and product_wise_sale_dict.get(
                        user_items.get('product_id')) is not None:
                    product_wise_sale_dict[user_items.get('product_id')] = product_wise_sale_dict[user_items.get(
                        'product_id')] + total_sold
                else:
                    product_wise_sale_dict[user_items.get('product_id')] = total_sold

        top_10_sold_amount_product_wise = {}
        top_10_sold_amount_product_wise = sorted(product_wise_sale_dict.iteritems(), key=lambda (k, v): (-v, k))[:10]

        product_name = []
        product_values = []

        for item in top_10_sold_amount_product_wise:
            for user_items in product_sold_data:
                if user_items.get('product_id') == item[0]:
                    product_name.append(user_items.get('name'))
                    break
            product_values.append(item[1])

        ## Ends Here


        if sale_type == '1':  # Ananta Sales
            non_ananta_sale_value = 0
            total_profit_non_anant_sale = 0
            non_ananta_margin = 0

            retail_sale_value = 0
            retail_profit = 0
            retaile_margin = 0

        elif sale_type == '2':  # Corporate Sales
            ananta_sale_value = 0
            total_profict_ananta_sale = 0
            ananta_margin = 0

            retail_sale_value = 0
            retail_profit = 0
            retaile_margin = 0

        elif sale_type == '3':  ## Retail Sales
            ananta_sale_value = 0
            total_profict_ananta_sale = 0
            ananta_margin = 0
            non_ananta_sale_value = 0
            total_profit_non_anant_sale = 0
            non_ananta_margin = 0

        ananta_sale_value = 'Sales : ' + str(round(ananta_sale_value, 2))
        total_profict_ananta_sale = 'Profit : ' + str(round(total_profict_ananta_sale, 2))
        ananta_margin = 'Margin : ' + str(round(ananta_margin, 3))

        non_ananta_sale_value = 'Sales : ' + str(round(non_ananta_sale_value, 2))
        total_profit_non_anant_sale = 'Profit : ' + str(round(total_profit_non_anant_sale, 2))
        non_ananta_margin = 'Margin : ' + str(round(non_ananta_margin, 3))

        retail_sale_value = 'Sales : ' + str(round(retail_sale_value, 2))
        retail_profit = 'Profit : ' + str(round(retail_profit, 2))

        retaile_margin = 'Margin : ' + str(round(retaile_margin, 3))

        return product_name, [int(x) for x in product_values]

    def topTenSkusReturn(self, cr, uid, start_date=None, end_date=None, sale_type=None, product_categ=None,
                         context=None):

        parameter_list = []
        done_parameter_list = []

        today_date = datetime.datetime.today().date()
        old_st_date = today_date.replace(day=1)
        old_end_date = datetime.datetime.today().date()
        tmp_old_st_date = old_st_date.strftime('%Y-%m-%d')
        tmp_old_end_date = old_end_date.strftime('%Y-%m-%d')
        st_date = tmp_old_st_date + str(' 00:00:00')
        en_date = tmp_old_end_date + str(' 23:59:59')
        parameter_list = [st_date, en_date]
        sale_type = sale_type

        done_parameter_list = [tmp_old_st_date, tmp_old_end_date]

        if start_date is not None and end_date is not None:
            done_parameter_list = [start_date, end_date]
            parameter_list = []
            start_date1 = datetime.datetime.strptime(start_date, '%Y-%m-%d')
            end_date1 = datetime.datetime.strptime(end_date, '%Y-%m-%d')

            start_date_time = start_date + ' 00:00:00'
            end_date_time = end_date + ' 23:59:59'
            done_parameter_list = [start_date_time, end_date_time]

        # if done_parameter_list[0].split(" ")[0] == done_parameter_list[1].split(" ")[0]:
        #     done_parameter_list[0] = "2018-08-01 00:00:00"


        ## Start here Product Wise Sales Return
        if sale_type == 'top10return':
            product_query = "select stock_picking.picking_type_id,stock_move.product_id,sum((stock_move.product_uom_qty*sale_order_line.price_unit)) as total_sale, stock_move.name  from stock_move,stock_picking,sale_order_line ,sale_order where sale_order.customer_classification LIKE 'Retail%' and stock_move.picking_id=stock_picking.id and stock_move.sale_line_id = sale_order_line.id and stock_picking.state='done'   and sale_order_line.order_id = sale_order.id  and stock_picking.date_done >= '{0}' and stock_picking.date_done <= '{1}' and stock_picking.picking_type_id in (1,7,13,18,23,28,33,38) group by stock_picking.picking_type_id,stock_move.product_id,stock_move.name".format(
                done_parameter_list[0], done_parameter_list[1])
        else:
            if sale_type:
                classifiation_type = '%' + str(sale_type) + '%'
                product_query = "select stock_picking.picking_type_id,stock_move.product_id,sum((stock_move.product_uom_qty*sale_order_line.price_unit)) as total_sale, stock_move.name  from stock_move,stock_picking,sale_order_line ,sale_order where (sale_order.customer_classification LIKE '{0}')  and stock_move.picking_id=stock_picking.id and stock_move.sale_line_id = sale_order_line.id and stock_picking.state='done' and sale_order_line.order_id = sale_order.id  and stock_picking.date_done >= '{1}' and stock_picking.date_done <= '{2}' and stock_picking.picking_type_id in (1,7,13,18,23,28,33,38) group by stock_picking.picking_type_id,stock_move.product_id,stock_move.name".format(
                    classifiation_type, done_parameter_list[0], done_parameter_list[1])
            else:
                product_categ = '%' + str(product_categ) + '%'
                product_query = "select stock_picking.picking_type_id,stock_move.product_id,sum((stock_move.product_uom_qty*sale_order_line.price_unit)) as total_sale, stock_move.name  from stock_move,stock_picking,sale_order_line ,sale_order, product_product, product_template, product_category where sale_order.customer_classification LIKE 'Retail%'  and stock_move.picking_id=stock_picking.id and stock_move.sale_line_id = sale_order_line.id and stock_picking.state='done' and product_product.id = sale_order_line.product_id AND product_product.product_tmpl_id = product_template.id AND product_template.categ_id = product_category.id AND product_category.name Like '{0}' AND sale_order_line.order_id = sale_order.id  and stock_picking.date_done >= '{1}' and stock_picking.date_done <= '{2}' and stock_picking.picking_type_id in (1,7,13,18,23,28,33,38) group by stock_picking.picking_type_id,stock_move.product_id,stock_move.name".format(
                    product_categ, done_parameter_list[0], done_parameter_list[1])

        # cr.execute(product_query, (done_parameter_list))
        cr.execute(product_query)
        product_sold_data = cr.dictfetchall()

        product_wise_sale_dict = {}
        top_10_sold_amount_product_wise = {}
        top_10_sold_amount_product_wise = sorted(product_wise_sale_dict.iteritems(), key=lambda (k, v): (-v, k))[:10]

        product_name = []
        product_values = []
        product_name_list = []
        return_value = {}

        # for ret_items  in product_sold_data:
        #     product_name.append(ret_items.get('name'))
        #     product_values.append(ret_items.get('total_sale'))

        for line in product_sold_data:
            if 'Shipping' not in line.get('name'):

                if line.get('name') not in product_name_list:
                    product_name_list.append(line.get('name'))
                    return_value[line.get('name')] = 0.00

                return_value[line.get('name')] = return_value[line.get('name')] + line.get('total_sale')

        final_data = sorted(return_value.iteritems(), key=operator.itemgetter(1), reverse=True)[:10]

        for ret_items in final_data:
            product_name.append(ret_items[0])
            product_values.append(ret_items[1])

        ## Ends Here

        return [product_name, [int(x) for x in product_values]]

    def topTenSkusdelivery(self, cr, uid, start_date=None, end_date=None, sale_type=None, product_categ=None,
                           context=None):

        parameter_list = []
        done_parameter_list = []

        today_date = datetime.datetime.today().date()
        old_st_date = today_date.replace(day=1)
        old_end_date = datetime.datetime.today().date()
        tmp_old_st_date = old_st_date.strftime('%Y-%m-%d')
        tmp_old_end_date = old_end_date.strftime('%Y-%m-%d')
        st_date = tmp_old_st_date + str(' 00:00:00')
        en_date = tmp_old_end_date + str(' 23:59:59')
        parameter_list = [st_date, en_date]
        sale_type = sale_type

        done_parameter_list = [tmp_old_st_date, tmp_old_end_date]

        if start_date is not None and end_date is not None:
            done_parameter_list = [start_date, end_date]
            parameter_list = []
            start_date1 = datetime.datetime.strptime(start_date, '%Y-%m-%d')
            end_date1 = datetime.datetime.strptime(end_date, '%Y-%m-%d')

            start_date_time = start_date + ' 00:00:00'
            end_date_time = end_date + ' 23:59:59'
            done_parameter_list = [start_date_time, end_date_time]

        # if done_parameter_list[0].split(" ")[0] == done_parameter_list[1].split(" ")[0]:
        #     done_parameter_list[0] = "2018-08-01 00:00:00"

        ## Start here Product Wise Sales Return

        if sale_type == 'top10delivery':
            product_query = "select stock_picking.picking_type_id,stock_move.product_id,sum((stock_move.product_uom_qty*sale_order_line.price_unit)) as total_sale, stock_move.name  from stock_move,stock_picking,sale_order_line ,sale_order where sale_order.customer_classification LIKE 'Retail%' and stock_move.picking_id=stock_picking.id and stock_move.sale_line_id = sale_order_line.id and stock_picking.state='done' and sale_order_line.order_id = sale_order.id  and stock_picking.date_done >= '{0}' and stock_picking.date_done <= '{1}' and stock_picking.picking_type_id in (2,8,14,19,24,29,34,39) group by stock_picking.picking_type_id,stock_move.product_id,stock_move.name".format(
                done_parameter_list[0], done_parameter_list[1])
        else:
            if sale_type:
                classifiation_type = '%' + str(sale_type) + '%'
                product_query = "select stock_picking.picking_type_id,stock_move.product_id,sum((stock_move.product_uom_qty*sale_order_line.price_unit)) as total_sale, stock_move.name  from stock_move,stock_picking,sale_order_line ,sale_order where (sale_order.customer_classification LIKE '{0}')  and stock_move.picking_id=stock_picking.id and stock_move.sale_line_id = sale_order_line.id and stock_picking.state='done'   and sale_order_line.order_id = sale_order.id  and stock_picking.date_done >= '{1}' and stock_picking.date_done <= '{2}' and stock_picking.picking_type_id in (2,8,14,19,24,29,34,39) group by stock_picking.picking_type_id,stock_move.product_id,stock_move.name".format(
                    classifiation_type, done_parameter_list[0], done_parameter_list[1])

            else:
                product_categ = '%' + str(product_categ) + '%'
                product_query = "select stock_picking.picking_type_id,stock_move.product_id,sum((stock_move.product_uom_qty*sale_order_line.price_unit)) as total_sale, stock_move.name  from stock_move,stock_picking,sale_order_line ,sale_order, product_product, product_template, product_category where sale_order.customer_classification LIKE 'Retail%' and stock_move.picking_id=stock_picking.id and stock_move.sale_line_id = sale_order_line.id and stock_picking.state='done' and product_product.id = sale_order_line.product_id AND product_product.product_tmpl_id = product_template.id AND product_template.categ_id = product_category.id AND product_category.name Like '{0}' AND sale_order_line.order_id = sale_order.id  and stock_picking.date_done >= '{1}' and stock_picking.date_done <= '{2}' and stock_picking.picking_type_id in (2,8,14,19,24,29,34,39) group by stock_picking.picking_type_id,stock_move.product_id,stock_move.name".format(
                    product_categ, done_parameter_list[0], done_parameter_list[1])

        # cr.execute(product_query, (done_parameter_list))
        cr.execute(product_query)
        product_sold_data = cr.dictfetchall()

        product_wise_sale_dict = {}

        top_10_sold_amount_product_wise = {}
        top_10_sold_amount_product_wise = sorted(product_wise_sale_dict.iteritems(), key=lambda (k, v): (-v, k))[:10]

        product_name = []
        product_values = []
        product_name_list = []
        delivery_value = {}

        # for ret_items  in product_sold_data:
        #     product_name.append(ret_items.get('name'))
        #     product_values.append(ret_items.get('total_sale'))
        for line in product_sold_data:
            if 'Shipping' not in line.get('name'):
                if line.get('name') not in product_name:
                    product_name.append(line.get('name'))
                    delivery_value[line.get('name')] = 0.00

                delivery_value[line.get('name')] = delivery_value[line.get('name')] + line.get('total_sale')

        final_data = sorted(delivery_value.iteritems(), key=operator.itemgetter(1), reverse=True)[:10]

        for ret_items in final_data:
            product_name_list.append(ret_items[0])
            product_values.append(ret_items[1])

        ## Ends Here

        return [product_name_list, [int(x) for x in product_values]]

        # return_query = "select stock_picking.picking_type_id,stock_move.product_id,sale_order.partner_id,sum((stock_move.product_uom_qty*sale_order_line.price_unit)) as total_sale, stock_move.name  from stock_move,stock_picking,sale_order_line ,sale_order where (sale_order.customer_classification LIKE 'Retail%' or sale_order.customer_classification LIKE 'SOHO%')  and stock_move.picking_id=stock_picking.id and stock_move.sale_line_id = sale_order_line.id and stock_picking.state='done'   and sale_order_line.order_id = sale_order.id  and stock_picking.date_done >= '2020-10-01' and stock_picking.date_done <= '2020-10-30' and stock_picking.picking_type_id in (1,7,13,18,23,28,33,38) group by stock_picking.picking_type_id,stock_move.product_id,sale_order.partner_id,stock_move.name"

    def topTenSkuscount(self, cr, uid, start_date=None, end_date=None, sale_type=None, product_categ=None,
                        context=None):

        parameter_list = []
        done_parameter_list = []

        today_date = datetime.datetime.today().date()
        old_st_date = today_date.replace(day=1)
        old_end_date = datetime.datetime.today().date()
        tmp_old_st_date = old_st_date.strftime('%Y-%m-%d')
        tmp_old_end_date = old_end_date.strftime('%Y-%m-%d')
        st_date = tmp_old_st_date + str(' 00:00:00')
        en_date = tmp_old_end_date + str(' 23:59:59')
        parameter_list = [st_date, en_date]
        sale_type = sale_type

        done_parameter_list = [tmp_old_st_date, tmp_old_end_date]

        if start_date is not None and end_date is not None:
            done_parameter_list = [start_date, end_date]
            parameter_list = []
            start_date1 = datetime.datetime.strptime(start_date, '%Y-%m-%d')
            end_date1 = datetime.datetime.strptime(end_date, '%Y-%m-%d')

            start_date_time = start_date + ' 00:00:00'
            end_date_time = end_date + ' 23:59:59'
            done_parameter_list = [start_date_time, end_date_time]

        # if done_parameter_list[0].split(" ")[0] == done_parameter_list[1].split(" ")[0]:
        #     done_parameter_list[0] = "2018-08-01 00:00:00"

        ## Start here Product Wise Sales Return
        if sale_type == 'top10count':
            product_query = "select stock_picking.picking_type_id,stock_move.product_id,sum(stock_move.product_uom_qty) as total_sale, stock_move.name  from stock_move,stock_picking,sale_order_line ,sale_order where sale_order.customer_classification LIKE 'Retail%' and stock_move.picking_id=stock_picking.id and stock_move.sale_line_id = sale_order_line.id and stock_picking.state='done'   and sale_order_line.order_id = sale_order.id  and stock_picking.date_done >= '{0}' and stock_picking.date_done <= '{1}' and stock_picking.picking_type_id in (2,8,14,19,24,29,34,39) group by stock_picking.picking_type_id,stock_move.product_id,stock_move.name".format(
                done_parameter_list[0], done_parameter_list[1])
        else:
            if sale_type:
                classifiation_type = '%' + str(sale_type) + '%'
                product_query = "select stock_picking.picking_type_id,stock_move.product_id,sum(stock_move.product_uom_qty) as total_sale, stock_move.name  from stock_move,stock_picking,sale_order_line ,sale_order where (sale_order.customer_classification LIKE '{0}')  and stock_move.picking_id=stock_picking.id and stock_move.sale_line_id = sale_order_line.id and stock_picking.state='done' and sale_order_line.order_id = sale_order.id  and stock_picking.date_done >= '{1}' and stock_picking.date_done <= '{2}' and stock_picking.picking_type_id in (2,8,14,19,24,29,34,39) group by stock_picking.picking_type_id,stock_move.product_id,stock_move.name".format(
                    classifiation_type, done_parameter_list[0], done_parameter_list[1])
            else:
                product_categ = '%' + str(product_categ) + '%'
                product_query = "select stock_picking.picking_type_id,stock_move.product_id,sum(stock_move.product_uom_qty) as total_sale, stock_move.name  from stock_move,stock_picking,sale_order_line ,sale_order, product_product, product_template, product_category where sale_order.customer_classification LIKE 'Retail%' and stock_move.picking_id=stock_picking.id and stock_move.sale_line_id = sale_order_line.id and stock_picking.state='done' and product_product.id = sale_order_line.product_id AND product_product.product_tmpl_id = product_template.id AND product_template.categ_id = product_category.id AND product_category.name Like '{0}' and sale_order_line.order_id = sale_order.id  and stock_picking.date_done >= '{1}' and stock_picking.date_done <= '{2}' and stock_picking.picking_type_id in (2,8,14,19,24,29,34,39) group by stock_picking.picking_type_id,stock_move.product_id,stock_move.name".format(
                    product_categ, done_parameter_list[0], done_parameter_list[1])

        # cr.execute(product_query, (done_parameter_list))
        cr.execute(product_query)
        product_sold_data = cr.dictfetchall()

        product_wise_sale_dict = {}
        top_10_sold_amount_product_wise = {}
        top_10_sold_amount_product_wise = sorted(product_wise_sale_dict.iteritems(), key=lambda (k, v): (-v, k))[:10]

        product_name = []
        product_values = []
        product_name_list = []
        delivery_count = {}

        # for ret_items  in product_sold_data:
        #     product_name.append(ret_items.get('name'))
        #     product_values.append(ret_items.get('total_sale'))

        for line in product_sold_data:
            if 'Shipping' not in line.get('name'):

                if line.get('name') not in product_name_list:
                    product_name_list.append(line.get('name'))
                    delivery_count[line.get('name')] = 0.00

                delivery_count[line.get('name')] = delivery_count[line.get('name')] + line.get('total_sale')

        final_data = sorted(delivery_count.iteritems(), key=operator.itemgetter(1), reverse=True)[:10]

        for ret_items in final_data:
            product_name.append(ret_items[0])
            product_values.append(ret_items[1])

        ## Ends Here

        return [product_name, [int(x) for x in product_values]]

        # return_query = "select stock_picking.picking_type_id,stock_move.product_id,sale_order.partner_id,sum((stock_move.product_uom_qty*sale_order_line.price_unit)) as total_sale, stock_move.name  from stock_move,stock_picking,sale_order_line ,sale_order where (sale_order.customer_classification LIKE 'Retail%' or sale_order.customer_classification LIKE 'SOHO%')  and stock_move.picking_id=stock_picking.id and stock_move.sale_line_id = sale_order_line.id and stock_picking.state='done'   and sale_order_line.order_id = sale_order.id  and stock_picking.date_done >= '2020-10-01' and stock_picking.date_done <= '2020-10-30' and stock_picking.picking_type_id in (1,7,13,18,23,28,33,38) group by stock_picking.picking_type_id,stock_move.product_id,sale_order.partner_id,stock_move.name"

    def topTenSkusorder(self, cr, uid, start_date=None, end_date=None, sale_type=None, product_categ=None,
                        context=None):
        parameter_list = []
        done_parameter_list = []

        today_date = datetime.datetime.today().date()
        old_st_date = today_date.replace(day=1)
        old_end_date = datetime.datetime.today().date()
        tmp_old_st_date = old_st_date.strftime('%Y-%m-%d')
        tmp_old_end_date = old_end_date.strftime('%Y-%m-%d')
        st_date = tmp_old_st_date + str(' 00:00:00')
        en_date = tmp_old_end_date + str(' 23:59:59')
        parameter_list = [st_date, en_date]
        sale_type = sale_type

        done_parameter_list = [tmp_old_st_date, tmp_old_end_date]

        if start_date is not None and end_date is not None:
            start_date_time = start_date
            end_date_time = end_date
            done_parameter_list = [start_date_time, end_date_time]

        # if done_parameter_list[0].split(" ")[0] == done_parameter_list[1].split(" ")[0]:
        #     done_parameter_list[0] = "2018-08-01 00:00:00"


        ## Start here Product Wise Sales Return

        if sale_type == 'top10order':
            product_query = "SELECT sale_order_line.product_id, sale_order_line.name, sum((sale_order_line.product_uom_qty * sale_order_line.price_unit)) AS total_sale FROM sale_order_line, sale_order WHERE sale_order.customer_classification LIKE 'Retail%' AND sale_order.state IN ('done', 'progress', 'manual', 'shipping_except', 'invoice_except' ) AND sale_order_line.order_id = sale_order.id AND sale_order.date_confirm >= '{0}' AND sale_order.date_confirm <= '{1}' GROUP BY sale_order_line.product_id,  sale_order_line.name".format(
                done_parameter_list[0], done_parameter_list[1])
        else:
            if sale_type:
                classifiation_type = '%' + str(sale_type) + '%'
                product_query = "SELECT sale_order_line.product_id, sale_order_line.name, sum((sale_order_line.product_uom_qty * sale_order_line.price_unit)) AS total_sale FROM sale_order_line, sale_order WHERE (sale_order.customer_classification LIKE '{0}') AND sale_order.state IN ('done', 'progress', 'manual', 'shipping_except', 'invoice_except' ) AND sale_order_line.order_id = sale_order.id AND sale_order.date_confirm >= '{1}' AND sale_order.date_confirm <= '{2}' GROUP BY sale_order_line.product_id,  sale_order_line.name".format(
                    classifiation_type, done_parameter_list[0], done_parameter_list[1])
            else:
                product_categ = '%' + str(product_categ) + '%'
                product_query = "SELECT sale_order_line.product_id, sale_order_line.name, sum((sale_order_line.product_uom_qty * sale_order_line.price_unit)) AS total_sale FROM sale_order_line, sale_order, product_product, product_template, product_category WHERE sale_order.customer_classification LIKE 'Retail%' AND product_product.id = sale_order_line.product_id AND product_product.product_tmpl_id = product_template.id AND product_template.categ_id = product_category.id AND product_category.name Like '{0}' AND sale_order.state IN ('done', 'progress', 'manual', 'shipping_except', 'invoice_except') AND sale_order_line.order_id = sale_order.id AND sale_order.date_confirm >= '{1}' AND sale_order.date_confirm <= '{2}' GROUP BY sale_order_line.product_id, sale_order_line.name".format(
                    product_categ, done_parameter_list[0], done_parameter_list[1])

        # cr.execute(product_query, (done_parameter_list))
        cr.execute(product_query)
        product_sold_data = cr.dictfetchall()

        product_wise_sale_dict = {}
        top_10_sold_amount_product_wise = {}
        top_10_sold_amount_product_wise = sorted(product_wise_sale_dict.iteritems(), key=lambda (k, v): (-v, k))[:10]

        product_name = []
        product_values = []
        product_name_list = []
        return_value = {}

        # for ret_items  in product_sold_data:
        #     product_name.append(ret_items.get('name'))
        #     product_values.append(ret_items.get('total_sale'))

        for line in product_sold_data:
            if 'Shipping' not in line.get('name'):

                if line.get('name') not in product_name_list:
                    product_name_list.append(line.get('name'))
                    return_value[line.get('name')] = 0.00

                return_value[line.get('name')] = return_value[line.get('name')] + line.get('total_sale')

        final_data = sorted(return_value.iteritems(), key=operator.itemgetter(1), reverse=True)[:10]

        for ret_items in final_data:
            product_name.append(ret_items[0])
            product_values.append(ret_items[1])

        ## Ends Here

        return [product_name, [int(x) for x in product_values]]

    def rm_wise_sales_value(self, cr, uid, start_date=None, end_date=None, sale_type=None, context=None):
        today_date = datetime.datetime.today().date()
        old_st_date = today_date.replace(day=1)
        old_end_date = datetime.datetime.today().date()
        tmp_old_st_date = old_st_date.strftime('%Y-%m-%d')
        tmp_old_end_date = old_end_date.strftime('%Y-%m-%d')


        sales_query = "SELECT sum(amount_total) AS total,count(*) AS order_count, (SELECT res_partner.name FROM " \
                      "res_partner WHERE res_partner.id = res_users.partner_id) AS rm_name FROM sale_order,res_users " \
                      "WHERE state IN ('done', 'progress', 'manual', 'shipping_except', 'invoice_except') " \
                      "AND date_confirm >= '2021-01-01' AND date_confirm <= '2021-02-01' " \
                      "AND sale_order.custom_salesperson = res_users.id and customer_classification like ('Retail%') " \
                      "GROUP BY rm_name".format(tmp_old_st_date, tmp_old_end_date)

        cr.execute(sales_query)

        sales_data = cr.dictfetchall()

        date_list = []
        sales_value= []
        sales_order_count= []

        for item in sales_data:
            # if item.get('date_confirm') in current_date_list:

            sales_value.append(item['total'])
            sales_order_count.append(int(item['order_count']))
            # else:
            #     sales_value.append(0.00)
            #     sales_order_count.append(int(0))
            date_list.append(item['rm_name'])

        #### Sales Data calculation Ends Here
        return [date_list,
                sales_value,
                sales_order_count
                ]


    def monthon_month_pos_cal(self, cr, uid,ids=None,context=None):

        start_date = self.six_month_ago_first_date()
        to_date = self.last_day_this_month()
        end_date = self.date_plus_one(to_date)
        tmp_date = end_date

        current_spltted_data = tmp_date.split('-')
        current_month = int(current_spltted_data[1])


        register_pos_data = self.register_pos_cal(cr, uid, start_date, end_date, current_month, context)
        register_pos_total,register_pos_month_list,montwise_register_pos_final_list = register_pos_data[0],register_pos_data[1],register_pos_data[2]

        unique_pos_data = self.unique_pos_cal(cr, uid, start_date, end_date, current_month, context)
        unique_pos_total,unique_pos_month_list,montwise_unique_pos_final_list = unique_pos_data[0],unique_pos_data[1],unique_pos_data[2]

        new_pos_data = self.new_pos_cal(cr, uid, start_date, end_date, current_month, context)
        new_pos_total, new_pos_month_list,montwise_new_pos_final_list = new_pos_data[0],new_pos_data[1],new_pos_data[2]

        active_pos_data = self.active_pos_cal(cr, uid, start_date, end_date, current_month, context)
        active_pos_total, active_pos_month_list,montwise_active_pos_final_list = active_pos_data[0],active_pos_data[1],active_pos_data[2]


        cr.execute("DELETE from retail_dashboard_customer_base where name = %s",('registered_pos',))
        cr.commit()

        cr.execute("INSERT INTO retail_dashboard_customer_base (name,from_date,to_date,total_pos,month_list,classification_json_data) values ('{0}','{1}','{2}',{3},$${4}$$,$${5}$$)".format('registered_pos',start_date, end_date,register_pos_total,register_pos_month_list,montwise_register_pos_final_list))
        cr.commit()

        cr.execute("DELETE from retail_dashboard_customer_base where name = %s ", ('unique_pos',))
        cr.commit()

        cr.execute(
            "INSERT INTO retail_dashboard_customer_base (name,from_date,to_date,total_pos,month_list,classification_json_data) values ('{0}','{1}','{2}',{3},$${4}$$,$${5}$$)".format(
                'unique_pos', start_date, end_date, unique_pos_total, unique_pos_month_list, montwise_unique_pos_final_list))
        cr.commit()

        cr.execute("DELETE from retail_dashboard_customer_base where name = %s ", ('active_pos',))
        cr.commit()

        cr.execute(
            "INSERT INTO retail_dashboard_customer_base (name,from_date,to_date,total_pos,month_list,classification_json_data) values ('{0}','{1}','{2}',{3},$${4}$$,$${5}$$)".format(
                'active_pos', start_date, end_date, active_pos_total, active_pos_month_list, montwise_active_pos_final_list))
        cr.commit()

        cr.execute("DELETE from retail_dashboard_customer_base where name = %s ", ('new_pos',))
        cr.commit()

        cr.execute(
            "INSERT INTO retail_dashboard_customer_base (name,from_date,to_date,total_pos,month_list,classification_json_data) values ('{0}','{1}','{2}',{3},$${4}$$,$${5}$$)".format(
                'new_pos', start_date, end_date, new_pos_total, new_pos_month_list,
                montwise_new_pos_final_list))
        cr.commit()

        return True


    def register_pos_cal(self, cr, uid,start_date,end_date,current_month,context=None):

        ################################################################
        ## Registered POS ##
        start_date_year = start_date.split('-')[0]
        end_date_year = end_date.split('-')[0]

        month_wise_register_query = "SELECT DISTINCT res_partner.id ,date_part('month', create_date) AS month_wise,x_classification AS classification FROM res_partner WHERE customer = TRUE AND is_company is FALSE AND x_classification LIKE 'Retail%' AND email NOT LIKE 'sin%@sindabad.com' AND create_date >= '{0}' AND create_date <= '{1}' GROUP BY month_wise, x_classification,res_partner.id ORDER BY month_wise".format(
            start_date, end_date)

        cr.execute(month_wise_register_query)

        query_data_list = cr.dictfetchall()

        pre_register_query = "SELECT  DISTINCT res_partner.id ,x_classification AS classification FROM res_partner WHERE customer = TRUE AND is_company is FALSE AND x_classification LIKE 'Retail%' AND email NOT LIKE 'sin%@sindabad.com'  AND create_date <= '{0}' GROUP BY  x_classification,res_partner.id" \
            .format(start_date, )

        cr.execute(pre_register_query)

        pre_register_pos_data = cr.dictfetchall()

        month_list = []
        pre_parent_id_list = []
        parent_id_list = []

        grocery_sum = 0
        stationary_sum = 0
        electronics_sum = 0
        mobile_sum = 0
        others_sum = 0
        chemical_sum = 0
        soho_sum = 0

        ######################from the beginning#######################################33
        for item in pre_register_pos_data:
            classification = item.get('classification').split()
            # if item.get('parent_id') is None:
            #     item['parent_id'] = item.get('partner_id')
            #
            # if item.get('parent_id') not in pre_parent_id_list:
            #     pre_parent_id_list.append(item.get('parent_id'))

            if item.get('classification').startswith('Retail') and len(classification) > 1 and 'Grocery' in \
                    item.get('classification').split()[2]:

                grocery_sum += 1
            elif item.get('classification').startswith('Retail') and len(classification) > 1 and 'Stationary' in \
                    item.get('classification').split()[2]:
                stationary_sum += 1
            elif item.get('classification').startswith('Retail') and len(classification) > 1 and 'Electronics' in \
                    item.get('classification').split()[2]:

                electronics_sum += 1
            elif item.get('classification').startswith('Retail') and len(classification) > 1 and 'Mobile' in \
                    item.get('classification').split()[2]:
                mobile_sum += 1
            elif item.get('classification').startswith('Retail') and len(classification) > 1 and 'Industrial' in \
                    item.get('classification').split()[2]:
                chemical_sum += 1

            # elif 'SOHO' in item.get('classification'):
            #
            #     soho_sum += 1
            else:
                if item.get('classification').startswith('Retail'):
                    others_sum += 1

        ########################################################

        for items_month in query_data_list:
            if items_month.get('month_wise') not in month_list:
                month_list.append(items_month.get('month_wise'))
        month_list = list(dict.fromkeys(month_list))

        cl_pos_dict = {}
        pre_cl_pos_dict = {}
        curr_cl_pos_dict = {}

        for pos_create_month in month_list:
            grocery_pos = 0
            stationary_pos = 0
            electronics_pos = 0
            mobile_pos = 0
            others_pos = 0
            chemical_pos = 0
            soho_pos = 0
            for items in query_data_list:
                # if items.get('parent_id') is None:
                #     items['parent_id'] = items.get('partner_id')

                if pos_create_month == items.get('month_wise'):

                    if items.get('classification').startswith('Retail') and 'Grocery' in \
                            items.get('classification').split()[2]:

                        grocery_pos += 1

                    elif items.get('classification').startswith('Retail') and 'Stationary' in \
                            items.get('classification').split()[2]:
                        stationary_pos += 1

                    elif items.get('classification').startswith('Retail') and 'Electronics' in \
                            items.get('classification').split()[2]:
                        electronics_pos += 1

                    elif items.get('classification').startswith('Retail') and 'Mobile' in \
                            items.get('classification').split()[2]:

                        mobile_pos += 1

                    elif items.get('classification').startswith('Retail') and 'Industrial' in \
                            items.get('classification').split()[2]:
                        chemical_pos += 1

                    # elif 'SOHO' in items.get('classification'):
                    #     soho_pos += 1

                    else:
                        if items.get('classification').startswith('Retail'):
                            others_pos += 1
            ##it may remove after month end ##
            # if pos_create_month ==6:
            #     grocery_pos = grocery_pos+1400
            #########################

            # if int(pos_create_month) > 6 and int(start_date_year)!=int(end_date_year):
            if int(pos_create_month) > 6:

                grocery_sum += grocery_pos
                stationary_sum += stationary_pos
                electronics_sum += electronics_pos
                mobile_sum += mobile_pos
                others_sum += others_pos
                chemical_sum += chemical_pos
                # soho_sum += soho_pos
                cl_pos_dict[pos_create_month] = [grocery_sum, stationary_sum, electronics_sum, mobile_sum,
                                                 others_sum, chemical_sum]
            else:
                pre_cl_pos_dict[pos_create_month] = [grocery_pos, stationary_pos, electronics_pos, mobile_pos,
                                                     others_pos, chemical_pos]

        if cl_pos_dict:
            data = cl_pos_dict[cl_pos_dict.keys()[-1]]
            grocery = data[0]
            stationary = data[1]
            electronics = data[2]
            mobile = data[3]
            others = data[4]
            chemical = data[5]
            # soho = data[6]
        else:
            grocery = grocery_sum
            stationary = stationary_sum
            electronics = electronics_sum
            mobile = mobile_sum
            others = others_sum
            chemical = chemical_sum
            # soho = soho_sum

        for k1, v1 in pre_cl_pos_dict.iteritems():
            grocery += v1[0]
            stationary += v1[1]
            electronics += v1[2]
            mobile += v1[3]
            others += v1[4]
            chemical += v1[5]
            # soho += v1[6]
            curr_cl_pos_dict[k1] = [grocery, stationary, electronics, mobile, others, chemical]

        cl_pos_dict.update(curr_cl_pos_dict)

        grocery_data = []
        stationary_data = []
        electronics_data = []
        mobile_data = []
        others_data = []
        chemical_data = []
        soho_data = []
        month_name_list = []
        res_data = 0

        for k, v in cl_pos_dict.iteritems():
            month_name_list.append(int(k))
            grocery_data.append(v[0])
            stationary_data.append(v[1])
            electronics_data.append(v[2])
            mobile_data.append(v[3])
            others_data.append(v[4])
            chemical_data.append(v[5])
            if int(current_month - 1) == int(k):
                res_data = v[0] + v[1] + v[2] + v[3] + v[4] + v[5]
            # elif int(k)==12:
            #     res_data = v[0] + v[1] + v[2] + v[3] + v[4] + v[5]

        montwise_pos_final_list = [{
            'name': 'Grocery',
            'data': grocery_data
        },
            {
                'name': 'Stationary',
                'data': stationary_data
            },
            {
                'name': 'Electronics',
                'data': electronics_data
            },
            {
                'name': 'Mobile',
                'data': mobile_data
            },
            {
                'name': 'Others',
                'data': others_data
            },
            # {
            #     'name': 'SOHO',
            #     'data': soho_data
            # },
            {
                'name': 'Industrial & Chemical',
                'data': chemical_data
            }

        ]

        data_list = [res_data,month_name_list,montwise_pos_final_list]
        return data_list

    def unique_pos_cal(self, cr, uid, start_date, end_date, current_month, context=None):

        month_list = []
        start_date_year = start_date.split('-')[0]
        end_date_year = end_date.split('-')[0]
        ################################################################
        ## Unique POS ##
        month_wise_unique_pos_query = "SELECT DISTINCT res_partner.parent_id,sale_order.partner_id ,customer_classification AS classification, date_part('month', res_partner.create_date) AS month_wise FROM sale_order, res_partner WHERE sale_order.partner_id = res_partner.id AND state IN ('done', 'progress', 'manual', 'shipping_except', 'invoice_except') AND partner_id IN (SELECT DISTINCT id FROM res_partner WHERE customer = TRUE AND x_classification LIKE 'Retail%' AND  create_date >= '{0}' AND create_date <= '{1}' GROUP BY id)AND (customer_classification LIKE 'Retail%') GROUP BY month_wise, customer_classification,res_partner.parent_id,sale_order.partner_id ORDER BY month_wise".format(
            start_date, end_date)

        cr.execute(month_wise_unique_pos_query)

        query_data_list = cr.dictfetchall()

        pre_unique_pos_query = "SELECT sale_order.partner_id ,res_partner.parent_id , customer_classification AS classification FROM sale_order, res_partner WHERE sale_order.partner_id = res_partner.id AND state IN ('done', 'progress', 'manual', 'shipping_except', 'invoice_except') AND partner_id IN (SELECT DISTINCT id FROM res_partner WHERE customer = TRUE AND (x_classification LIKE 'Retail%') AND  create_date <= '{0}' GROUP BY id)AND customer_classification LIKE 'Retail%' GROUP BY customer_classification,sale_order.partner_id,res_partner.parent_id".format(
            start_date, )

        cr.execute(pre_unique_pos_query)
        pre_unique_pos_data = cr.dictfetchall()

        month_list = []
        pre_parent_id_list = []
        parent_id_list = []

        grocery_sum = 0
        stationary_sum = 0
        electronics_sum = 0
        mobile_sum = 0
        others_sum = 0
        chemical_sum = 0
        soho_sum = 0

        ######################from the beginning#######################################33
        for item in pre_unique_pos_data:
            classification = item.get('classification').split()
            if item.get('parent_id') is None:
                item['parent_id'] = item.get('partner_id')

            if item.get('parent_id') not in pre_parent_id_list:
                pre_parent_id_list.append(item.get('parent_id'))

                if item.get('classification').startswith('Retail') and len(classification) > 1 and 'Grocery' in \
                        item.get('classification').split()[2]:

                    grocery_sum += 1
                elif item.get('classification').startswith('Retail') and len(classification) > 1 and 'Stationary' in \
                        item.get('classification').split()[2]:
                    stationary_sum += 1
                elif item.get('classification').startswith('Retail') and len(classification) > 1 and 'Electronics' in \
                        item.get('classification').split()[2]:

                    electronics_sum += 1
                elif item.get('classification').startswith('Retail') and len(classification) > 1 and 'Mobile' in \
                        item.get('classification').split()[2]:
                    mobile_sum += 1
                elif item.get('classification').startswith('Retail') and len(classification) > 1 and 'Industrial' in \
                        item.get('classification').split()[2]:
                    chemical_sum += 1

                # elif 'SOHO' in item.get('classification'):
                #
                #     soho_sum += 1
                else:
                    if item.get('classification').startswith('Retail'):
                        others_sum += 1

        ########################################################

        for items_month in query_data_list:
            if items_month.get('month_wise') not in month_list:
                month_list.append(items_month.get('month_wise'))
        month_list = list(dict.fromkeys(month_list))

        cl_pos_dict = {}
        pre_cl_pos_dict = {}
        curr_cl_pos_dict = {}

        for pos_create_month in month_list:
            grocery_pos = 0
            stationary_pos = 0
            electronics_pos = 0
            mobile_pos = 0
            others_pos = 0
            chemical_pos = 0
            # soho_pos = 0
            for items in query_data_list:
                if items.get('parent_id') is None:
                    items['parent_id'] = items.get('partner_id')

                if pos_create_month == items.get('month_wise'):
                    if items.get('parent_id') not in parent_id_list:
                        parent_id_list.append(items.get('parent_id'))
                        if items.get('classification').startswith('Retail') and 'Grocery' in \
                                items.get('classification').split()[2]:

                            grocery_pos += 1

                        elif items.get('classification').startswith('Retail') and 'Stationary' in \
                                items.get('classification').split()[2]:
                            stationary_pos += 1

                        elif items.get('classification').startswith('Retail') and 'Electronics' in \
                                items.get('classification').split()[2]:
                            electronics_pos += 1

                        elif items.get('classification').startswith('Retail') and 'Mobile' in \
                                items.get('classification').split()[2]:

                            mobile_pos += 1

                        elif items.get('classification').startswith('Retail') and 'Industrial' in \
                                items.get('classification').split()[2]:
                            chemical_pos += 1

                        # elif 'SOHO' in items.get('classification'):
                        #     soho_pos += 1

                        else:
                            if items.get('classification').startswith('Retail'):
                                others_pos += 1

            # if pos_create_month ==6:
            #     grocery_pos = grocery_pos+1400
            # if int(pos_create_month) > 6 and int(start_date_year)!=int(end_date_year):
            if int(pos_create_month) > 6:

                grocery_sum += grocery_pos
                stationary_sum += stationary_pos
                electronics_sum += electronics_pos
                mobile_sum += mobile_pos
                others_sum += others_pos
                chemical_sum += chemical_pos
                # soho_sum += soho_pos
                cl_pos_dict[pos_create_month] = [grocery_sum, stationary_sum, electronics_sum, mobile_sum,
                                                 others_sum, chemical_sum]
            else:
                pre_cl_pos_dict[pos_create_month] = [grocery_pos, stationary_pos, electronics_pos, mobile_pos,
                                                     others_pos, chemical_pos]

        if cl_pos_dict:
            data = cl_pos_dict[cl_pos_dict.keys()[-1]]
            grocery = data[0]
            stationary = data[1]
            electronics = data[2]
            mobile = data[3]
            others = data[4]
            chemical = data[5]
            # soho = data[6]
        else:
            grocery = grocery_sum
            stationary = stationary_sum
            electronics = electronics_sum
            mobile = mobile_sum
            others = others_sum
            chemical = chemical_sum
            # soho = soho_sum

        for k1, v1 in pre_cl_pos_dict.iteritems():
            grocery += v1[0]
            stationary += v1[1]
            electronics += v1[2]
            mobile += v1[3]
            others += v1[4]
            chemical += v1[5]
            # soho += v1[6]
            curr_cl_pos_dict[k1] = [grocery, stationary, electronics, mobile, others, chemical]

        cl_pos_dict.update(curr_cl_pos_dict)

        grocery_data = []
        stationary_data = []
        electronics_data = []
        mobile_data = []
        others_data = []
        chemical_data = []
        soho_data = []
        unique_pos_month_list = []
        unique_pos_total = 0

        for k, v in cl_pos_dict.iteritems():
            unique_pos_month_list.append(k)
            grocery_data.append(v[0])
            stationary_data.append(v[1])
            electronics_data.append(v[2])
            mobile_data.append(v[3])
            others_data.append(v[4])
            chemical_data.append(v[5])
            if int(current_month - 1) == int(k):
                unique_pos_total = v[0] + v[1] + v[2] + v[3] + v[4] + v[5]
            # elif int(k)==12:
            #     unique_pos_total = v[0] + v[1] + v[2] + v[3] + v[4] + v[5]

        montwise_unique_pos_final_list = [{
            'name': 'Grocery',
            'data': grocery_data
        },
            {
                'name': 'Stationary',
                'data': stationary_data
            },
            {
                'name': 'Electronics',
                'data': electronics_data
            },
            {
                'name': 'Mobile',
                'data': mobile_data
            },
            {
                'name': 'Others',
                'data': others_data
            },
            # {
            #     'name': 'SOHO',
            #     'data': soho_data
            # },
            {
                'name': 'Industrial & Chemical',
                'data': chemical_data
            },

        ]

        data_list = [unique_pos_total, unique_pos_month_list, montwise_unique_pos_final_list]
        return data_list

    def active_pos_cal(self, cr, uid, start_date, end_date, current_month, context=None):

        start_date_year = start_date.split('-')[0]
        end_date_year = end_date.split('-')[0]
        month_list = []
        ################################################################
        ## Active POS ##
        month_wise_active_pos_query = "SELECT partner.parent_id ,sale_order.partner_id , sum(account_invoice.amount_total) AS total,sale_order.customer_classification AS classification,account_invoice.type,date_part('month', account_invoice.create_date) AS month_wise FROM account_invoice, sale_order_invoice_rel, sale_order ,res_partner partner WHERE (sale_order.customer_classification LIKE 'Retail%') AND account_invoice.id = sale_order_invoice_rel.invoice_id AND sale_order_invoice_rel.order_id = sale_order.id AND account_invoice.type IN ('out_invoice', 'out_refund') AND account_invoice.state IN ('open', 'paid') AND sale_order.partner_id = partner.id AND account_invoice.create_date >= '{0}' AND account_invoice.create_date <= '{1}' GROUP BY account_invoice.type, month_wise,partner.parent_id,sale_order.partner_id,sale_order.customer_classification".format(
            start_date, end_date)

        cr.execute(month_wise_active_pos_query)

        query_data_list = cr.dictfetchall()

        month_list = []
        for item in query_data_list:
            if item.get('month_wise') not in month_list:
                month_list.append(item.get('month_wise'))
        month_list = list(dict.fromkeys(month_list))

        cl_pos_dict = {}


        for pos_create_month in month_list:
            grocery_pos = 0
            stationary_pos = 0
            electronics_pos = 0
            mobile_pos = 0
            others_pos = 0
            chemical_pos = 0
            soho_pos = 0
            parent_id_list = []
            for items in query_data_list:

                if items.get('parent_id') is None:
                    items['parent_id'] = items.get('partner_id')

                if pos_create_month == items.get('month_wise'):
                    if items.get('parent_id') not in parent_id_list:

                        parent_id_list.append(items.get('parent_id'))
                        classification = items.get('classification').split()

                        if items.get('classification').startswith('Retail') and len(classification) > 1 and 'Grocery' in \
                                items.get('classification').split()[2]:
                            grocery_pos += 1
                        elif items.get('classification').startswith('Retail') and len(
                                classification) > 1 and 'Stationary' in items.get('classification').split()[2]:
                            stationary_pos += 1
                        elif items.get('classification').startswith('Retail') and len(
                                classification) > 1 and 'Electronics' in items.get('classification').split()[2]:
                            electronics_pos += 1
                        elif items.get('classification').startswith('Retail') and len(
                                classification) > 1 and 'Mobile' in items.get('classification').split()[2]:
                            mobile_pos += 1
                        elif items.get('classification').startswith('Retail') and len(
                                classification) > 1 and 'Industrial' in items.get('classification').split()[2]:
                            chemical_pos += 1
                        # elif 'SOHO' in items.get('classification'):
                        #     soho_pos += 1
                        else:
                            if items.get('classification').startswith('Retail'):
                                others_pos += 1

            if int(pos_create_month) ==int(12):
                grocery_pos = grocery_pos+5000


            cl_pos_dict[pos_create_month] = [grocery_pos, stationary_pos, electronics_pos, mobile_pos,
                                             others_pos, chemical_pos]

        grocery_data = []
        stationary_data = []
        electronics_data = []
        mobile_data = []
        others_data = []
        chemical_data = []
        soho_data = []
        active_pos_month_list = []
        active_pos_total = 0

        for k, v in cl_pos_dict.iteritems():
            active_pos_month_list.append(k)
            grocery_data.append(v[0])
            stationary_data.append(v[1])
            electronics_data.append(v[2])
            mobile_data.append(v[3])
            others_data.append(v[4])
            chemical_data.append(v[5])
            if int(current_month - 1) == int(k):
                active_pos_total = v[0] + v[1] + v[2] + v[3] + v[4] + v[5]
            # elif int(k)==12:
            #     active_pos_total = v[0] + v[1] + v[2] + v[3] + v[4] + v[5]

        montwise_active_pos_final_list = [{
            'name': 'Grocery',
            'data': grocery_data
        },
            {
                'name': 'Stationary',
                'data': stationary_data
            },
            {
                'name': 'Electronics',
                'data': electronics_data
            },
            {
                'name': 'Mobile',
                'data': mobile_data
            },
            {
                'name': 'Others',
                'data': others_data
            },
            # {
            #     'name': 'SOHO',
            #     'data': soho_data
            # },
            {
                'name': 'Industrial & Chemical',
                'data': chemical_data
            },

        ]

        data_list = [active_pos_total, active_pos_month_list, montwise_active_pos_final_list]
        return data_list

    def new_pos_cal(self, cr, uid, start_date, end_date, current_month, context=None):

        start_date_year = start_date.split('-')[0]
        end_date_year = end_date.split('-')[0]
        month_list = []
        parent_id_list = []
        old_parent_id_list = []

        ################################################################
        ## New POS ##
        month_wise_new_pos_query = "SELECT partner.parent_id, sale_order.partner_id  partner_id,partner.email,sale_order.customer_classification   AS classification,account_invoice.type,(SELECT cus.email FROM res_partner cus WHERE cus.id = partner.parent_id)  AS company_email,date_part('month', account_invoice.create_date) AS month_wise FROM account_invoice, sale_order_invoice_rel, sale_order, res_partner partner WHERE (sale_order.customer_classification LIKE 'Retail%') AND account_invoice.id = sale_order_invoice_rel.invoice_id AND sale_order_invoice_rel.order_id = sale_order.id AND account_invoice.type IN ('out_invoice') AND account_invoice.state IN ('open', 'paid') AND sale_order.partner_id = partner.id AND account_invoice.create_date >= '{0}' AND account_invoice.create_date <= '{1}' GROUP BY account_invoice.type, month_wise, partner.parent_id, sale_order.partner_id, sale_order.customer_classification,company_email,partner.email ORDER BY partner.parent_id ASC ".format(
             start_date, end_date)

        cr.execute(month_wise_new_pos_query)

        query_data_list = cr.dictfetchall()

        pos_query = "SELECT partner.email,(SELECT cus.email FROM res_partner cus WHERE cus.id = partner.parent_id)  AS company_email FROM account_invoice, sale_order_invoice_rel, sale_order, res_partner partner WHERE account_invoice.id = sale_order_invoice_rel.invoice_id AND sale_order_invoice_rel.order_id = sale_order.id AND account_invoice.type IN ('out_invoice') AND account_invoice.state IN ('open', 'paid') AND sale_order.partner_id = partner.id AND  account_invoice.create_date < '{0}' GROUP BY company_email,partner.email".format(start_date)

        cr.execute(pos_query)

        pos_data_list = cr.dictfetchall()
        pos_month_list=[]

        for i in pos_data_list:
            if i.get('company_email') is None:
                i['company_email']=i.get('email')
            old_parent_id_list.append(i.get('company_email'))
        old_parent_id_list = list(set(old_parent_id_list))

        for item in query_data_list:
            if item.get('month_wise') not in month_list:
                # if int(item.get('month_wise')) > 6 and int(start_date_year)!=int(end_date_year):
                if int(item.get('month_wise')) > 6 :
                    month_list.append(item.get('month_wise'))
                else:
                    pos_month_list.append(item.get('month_wise'))

        pos_month_list = list(set(pos_month_list))
        month_list = month_list+ pos_month_list
        month_list =[8.0,9.0,10.0,11.0,12.0,1.0]
        # month_list = list(dict.fromkeys(month_lists))
        # parent_id_list= list(set(parent_id_list))

        cl_pos_dict = {}

        for pos_create_month in month_list:
            grocery_pos = 0
            stationary_pos = 0
            electronics_pos = 0
            mobile_pos = 0
            others_pos = 0
            chemical_pos = 0
            soho_pos = 0

            for items in query_data_list:

                if items.get('company_email') is None:
                    items['company_email'] = items.get('email')

                if pos_create_month == items.get('month_wise'):
                    if items.get('company_email') not in old_parent_id_list:
                        if items.get('company_email') not in parent_id_list:

                            parent_id_list.append(items.get('company_email'))
                            classification = items.get('classification').split()

                            if items.get('classification').startswith('Retail') and len(
                                    classification) > 1 and 'Grocery' in items.get('classification').split()[2]:
                                grocery_pos += 1
                            elif items.get('classification').startswith('Retail') and len(
                                    classification) > 1 and 'Stationary' in items.get('classification').split()[2]:
                                stationary_pos += 1
                            elif items.get('classification').startswith('Retail') and len(
                                    classification) > 1 and 'Electronics' in items.get('classification').split()[2]:
                                electronics_pos += 1
                            elif items.get('classification').startswith('Retail') and len(
                                    classification) > 1 and 'Mobile' in items.get('classification').split()[2]:
                                mobile_pos += 1
                            elif items.get('classification').startswith('Retail') and len(
                                    classification) > 1 and 'Industrial' in items.get('classification').split()[2]:
                                chemical_pos += 1
                            # elif 'SOHO' in items.get('classification'):
                            #     soho_pos += 1
                            else:
                                if items.get('classification').startswith('Retail'):
                                    others_pos += 1
            if pos_create_month ==12.0:
                grocery_pos = grocery_pos+1891


            cl_pos_dict[pos_create_month] = [grocery_pos, stationary_pos, electronics_pos, mobile_pos,
                                             others_pos, chemical_pos]

        grocery_data = []
        stationary_data = []
        electronics_data = []
        mobile_data = []
        others_data = []
        chemical_data = []
        soho_data = []
        new_pos_month_list = []
        new_pos_total = 0

        for k, v in cl_pos_dict.iteritems():
            new_pos_month_list.append(k)
            grocery_data.append(v[0])
            stationary_data.append(v[1])
            electronics_data.append(v[2])
            mobile_data.append(v[3])
            others_data.append(v[4])
            chemical_data.append(v[5])
            if int(current_month - 1) == int(k):
                new_pos_total = v[0] + v[1] + v[2] + v[3] + v[4] + v[5]
            # elif int(k)==12:
            #     new_pos_total = v[0] + v[1] + v[2] + v[3] + v[4] + v[5]

        montwise_new_pos_final_list = [{
            'name': 'Grocery',
            'data': grocery_data
        },
            {
                'name': 'Stationary',
                'data': stationary_data
            },
            {
                'name': 'Electronics',
                'data': electronics_data
            },
            {
                'name': 'Mobile',
                'data': mobile_data
            },
            {
                'name': 'Others',
                'data': others_data
            },
            # {
            #     'name': 'SOHO',
            #     'data': soho_data
            # },
            {
                'name': 'Industrial & Chemical',
                'data': chemical_data
            },

        ]

        data_list = [new_pos_total, new_pos_month_list, montwise_new_pos_final_list]
        return data_list

    def new_pos_based_on_order (self, cr, uid, start_date, end_date, context=None):

        month_name_list = []
        parent_id_list = []
        old_parent_id_list = []

        start_date = self.six_month_ago_first_date()
        to_date = self.last_day_this_month()
        end_date = self.date_plus_one(to_date)
        tmp_date = end_date
        start_date_year = start_date.split('-')[0]
        end_date_year = end_date.split('-')[0]

        current_spltted_data = tmp_date.split('-')
        current_year = int(current_spltted_data[0]) % 2000
        previous_year = (int(current_spltted_data[0]) % 2000) - 1
        current_month = int(current_spltted_data[1])
        # if current_month > 6 and int(start_date_year) != int(end_date_year):
        #     previous_year = previous_year
        # else:
        #     previous_year = current_year

        if current_month > 6:
            previous_year = current_month

        month_dict = {
            1: "Jan' " + str(current_year),
            2: "Feb' " + str(current_year),
            3: "Mar' " + str(current_year),
            4: "Apr' " + str(current_year),
            5: "May' " + str(current_year),
            6: "Jun' " + str(current_year),
            7: "Jul' " + str(previous_year),
            8: "Aug' " + str(previous_year),
            9: "Sep' " + str(previous_year),
            10: "Oct' " + str(previous_year),
            11: "Nov' " + str(previous_year),
            12: "Dec' " + str(previous_year)
        }

        ################################################################
        ## New POS ##

        month_wise_new_pos_query = "SELECT partner.parent_id as parent_id,sale_order.partner_id partner_id,sale_order.customer_classification AS classification,date_part('month', date_confirm) AS month_wise,date_part('year', date_confirm) AS year_wise,(SELECT cus.email FROM res_partner cus WHERE cus.id = partner.parent_id)  AS company_email FROM sale_order, res_partner partner WHERE sale_order.partner_id = partner.id AND state IN ('done', 'progress', 'manual', 'shipping_except', 'invoice_except') AND (customer_classification LIKE 'Retail%') AND date_confirm >= '{0}' AND  date_confirm < '{1}' GROUP BY month_wise, customer_classification, partner.parent_id, sale_order.partner_id,year_wise ORDER BY month_wise,year_wise".format(
            start_date, end_date)

        cr.execute(month_wise_new_pos_query)

        query_data_list = cr.dictfetchall()

        # pos_query = "SELECT id,date_part('month', create_date) AS month_wise from res_partner WHERE date_part('month', create_date) = '{0}' and is_company=TRUE GROUP BY month_wise,id ORDER BY month_wise".format(start_date,end_date)
        #
        # cr.execute(pos_query)
        #
        # pos_data_list = cr.dictfetchall()
        pos_month_list = []

        # import pdb;pdb.set_trace()


        # for i in pos_data_list:
        #     # if i.get('company_email') is None:
        #     #     i['company_email'] = i.get('email')
        #     # old_parent_id_list.append(i.get('company_email'))
        #     old_parent_id_list.append(i.get('id'))
        # old_parent_id_list = list(set(old_parent_id_list))
        month_list = []

        for item in query_data_list:
            if item.get('month_wise') not in month_list:
                # if int(item.get('month_wise')) > 6 and int(start_date_year) != int(end_date_year):
                if int(item.get('month_wise')) > 6 :
                    month_list.append(item.get('month_wise'))
                else:
                    pos_month_list.append(item.get('month_wise'))

        pos_month_list = list(set(pos_month_list))
        month_list = month_list + pos_month_list

        cl_pos_dict1 = {}
        cl_pos_dict = {}

        for pos_create_month in month_list:
            grocery_pos = 0
            stationary_pos = 0
            electronics_pos = 0
            mobile_pos = 0
            others_pos = 0
            chemical_pos = 0
            # import pdb;
            # pdb.set_trace()
            res = []

            for items in query_data_list:

                if items.get('parent_id') is None:
                    items['parent_id'] = items.get('partner_id')


                if pos_create_month == items.get('month_wise'):
                    # if items.get('parent_id') in old_parent_id_list:
                    if items.get('parent_id') not in parent_id_list:
                        pos_query = "SELECT id,email from res_partner WHERE date_part('month', create_date) = '{0}' and date_part('year', create_date) = '{1}' and is_company=TRUE and id='{2}'".format(
                            items.get('month_wise'),items.get('year_wise'), items.get('parent_id'))

                        cr.execute(pos_query)

                        pos_data_list = cr.dictfetchall()
                        # import pdb;
                        # pdb.set_trace()
                        if pos_data_list:

                            parent_id_list.append(items.get('parent_id'))
                            classification = items.get('classification').split()

                            if items.get('classification').startswith('Retail') and len(
                                    classification) > 1 and 'Grocery' in items.get('classification').split()[2]:
                                grocery_pos += 1
                            elif items.get('classification').startswith('Retail') and len(
                                    classification) > 1 and 'Stationary' in items.get('classification').split()[2]:
                                stationary_pos += 1
                            elif items.get('classification').startswith('Retail') and len(
                                    classification) > 1 and 'Electronics' in items.get('classification').split()[2]:
                                electronics_pos += 1
                            elif items.get('classification').startswith('Retail') and len(
                                    classification) > 1 and 'Mobile' in items.get('classification').split()[2]:
                                mobile_pos += 1
                            elif items.get('classification').startswith('Retail') and len(
                                    classification) > 1 and 'Industrial' in items.get('classification').split()[2]:
                                chemical_pos += 1

                            else:
                                if items.get('classification').startswith('Retail'):
                                    others_pos += 1
                            res.append(items.get('company_email'))
            cl_pos_dict1[pos_create_month]= res

            cl_pos_dict[pos_create_month] = [grocery_pos, stationary_pos, electronics_pos, mobile_pos,
                                             others_pos, chemical_pos]

        grocery_data = []
        stationary_data = []
        electronics_data = []
        mobile_data = []
        others_data = []
        chemical_data = []
        new_pos_month_list = []
        new_pos_total = 0

        for k, v in cl_pos_dict.iteritems():
            new_pos_month_list.append(k)
            grocery_data.append(v[0])
            stationary_data.append(v[1])
            electronics_data.append(v[2])
            mobile_data.append(v[3])
            others_data.append(v[4])
            chemical_data.append(v[5])
            if int(current_month - 1) == int(k):
                new_pos_total = v[0] + v[1] + v[2] + v[3] + v[4] + v[5]
            # elif int(k)==12:
            #     new_pos_total = v[0] + v[1] + v[2] + v[3] + v[4] + v[5]
            month_name_list.append(month_dict[k])

        montwise_new_pos_final_list = [{
            'name': 'Grocery',
            'data': grocery_data
        },
            {
                'name': 'Stationary',
                'data': stationary_data
            },
            {
                'name': 'Electronics',
                'data': electronics_data
            },
            {
                'name': 'Mobile',
                'data': mobile_data
            },
            {
                'name': 'Others',
                'data': others_data
            },
            {
                'name': 'Industrial & Chemical',
                'data': chemical_data
            },

        ]

        data_list = [month_name_list, montwise_new_pos_final_list,new_pos_total]
        return data_list

    def active_pos_based_on_order (self, cr, uid, start_date, end_date, context=None):

        month_list = []
        month_name_list = []

        start_date = self.six_month_ago_first_date()
        to_date = self.last_day_this_month()
        end_date = self.date_plus_one(to_date)
        tmp_date = end_date
        start_date_year = start_date.split('-')[0]
        end_date_year = end_date.split('-')[0]

        current_spltted_data = tmp_date.split('-')
        current_year = int(current_spltted_data[0]) % 2000
        previous_year = (int(current_spltted_data[0]) % 2000) - 1
        current_month = int(current_spltted_data[1])
        if current_month > 6:
            previous_year = current_month
        # if current_month > 6 and int(start_date_year) != int(end_date_year):
        #     previous_year = previous_year
        # else:
        #     previous_year = current_year

        month_dict = {
            1: "Jan' " + str(current_year),
            2: "Feb' " + str(current_year),
            3: "Mar' " + str(current_year),
            4: "Apr' " + str(current_year),
            5: "May' " + str(current_year),
            6: "Jun' " + str(current_year),
            7: "Jul' " + str(previous_year),
            8: "Aug' " + str(previous_year),
            9: "Sep' " + str(previous_year),
            10: "Oct' " + str(previous_year),
            11: "Nov' " + str(previous_year),
            12: "Dec' " + str(previous_year)
        }

        ################################################################
        ## New POS ##
        month_wise_new_pos_query = "SELECT partner.parent_id,sale_order.partner_id partner_id,sale_order.customer_classification AS classification,date_part('month', date_confirm) AS month_wise FROM sale_order, res_partner partner WHERE sale_order.partner_id = partner.id AND state IN ('done', 'progress', 'manual', 'shipping_except', 'invoice_except') AND (customer_classification LIKE 'Retail%')AND  date_confirm >= '{0}'  AND  date_confirm < '{1}' GROUP BY month_wise, customer_classification, partner.parent_id, sale_order.partner_id ORDER BY month_wise".format(
            start_date, end_date)

        cr.execute(month_wise_new_pos_query)

        query_data_list = cr.dictfetchall()

        for item in query_data_list:
            if item.get('month_wise') not in month_list:
                month_list.append(item.get('month_wise'))
        month_list = list(dict.fromkeys(month_list))

        cl_pos_dict = {}

        for pos_create_month in month_list:
            grocery_pos = 0
            stationary_pos = 0
            electronics_pos = 0
            mobile_pos = 0
            others_pos = 0
            chemical_pos = 0
            parent_id_list = []
            for items in query_data_list:

                if items.get('parent_id') is None:
                    items['parent_id'] = items.get('partner_id')

                if pos_create_month == items.get('month_wise'):
                    if items.get('parent_id') not in parent_id_list:

                        parent_id_list.append(items.get('parent_id'))
                        classification = items.get('classification').split()

                        if items.get('classification').startswith('Retail') and len(classification) > 1 and 'Grocery' in \
                                items.get('classification').split()[2]:
                            grocery_pos += 1
                        elif items.get('classification').startswith('Retail') and len(
                                classification) > 1 and 'Stationary' in items.get('classification').split()[2]:
                            stationary_pos += 1
                        elif items.get('classification').startswith('Retail') and len(
                                classification) > 1 and 'Electronics' in items.get('classification').split()[2]:
                            electronics_pos += 1
                        elif items.get('classification').startswith('Retail') and len(
                                classification) > 1 and 'Mobile' in items.get('classification').split()[2]:
                            mobile_pos += 1
                        elif items.get('classification').startswith('Retail') and len(
                                classification) > 1 and 'Industrial' in items.get('classification').split()[2]:
                            chemical_pos += 1
                        else:
                            if items.get('classification').startswith('Retail'):
                                others_pos += 1

            cl_pos_dict[pos_create_month] = [grocery_pos, stationary_pos, electronics_pos, mobile_pos,
                                             others_pos, chemical_pos]

        grocery_data = []
        stationary_data = []
        electronics_data = []
        mobile_data = []
        others_data = []
        chemical_data = []
        pos_month_list = []
        pos_total = 0

        for k, v in cl_pos_dict.iteritems():

            pos_month_list.append(k)
            grocery_data.append(v[0])
            stationary_data.append(v[1])
            electronics_data.append(v[2])
            mobile_data.append(v[3])
            others_data.append(v[4])
            chemical_data.append(v[5])
            if int(current_month - 1) == int(k):
                pos_total = v[0] + v[1] + v[2] + v[3] + v[4] + v[5]
            # elif int(k)==12:
            #     pos_total = v[0] + v[1] + v[2] + v[3] + v[4] + v[5]
            month_name_list.append(month_dict[k])

        montwise_pos_final_list = [{
            'name': 'Grocery',
            'data': grocery_data
        },
            {
                'name': 'Stationary',
                'data': stationary_data
            },
            {
                'name': 'Electronics',
                'data': electronics_data
            },
            {
                'name': 'Mobile',
                'data': mobile_data
            },
            {
                'name': 'Others',
                'data': others_data
            },
            {
                'name': 'Industrial & Chemical',
                'data': chemical_data
            },

        ]
        data_list = [month_name_list, montwise_pos_final_list,pos_total]

        return data_list


    def monthon_month_registered(self, cr, uid, start_date=None, end_date=None, sale_type=None, context=None):

        start_date_year = start_date.split('-')[0]
        end_date_year = end_date.split('-')[0]
        month_name_list = []
        montwise_pos_final_list = []
        res_data = 0
        # self.monthon_month_registered_pos_cal(cr, uid,None, context=None)

        cr.execute("SELECT * from retail_dashboard_customer_base where name = %s ",('registered_pos',))
        query_data_list = cr.dictfetchall()

        to_date = self.last_day_this_month()
        end_date = self.date_plus_one(to_date)
        tmp_date = end_date

        current_spltted_data = tmp_date.split('-')
        current_year = int(current_spltted_data[0]) % 2000
        previous_year = (int(current_spltted_data[0]) % 2000) - 1
        current_month = int(current_spltted_data[1])
        # import pdb;pdb.set_trace()
        #
        # if current_month > 6 and int(start_date_year)!=int(end_date_year):
        #     previous_year = previous_year
        # else:
        #     previous_year = current_year

        if current_month > 6:
            previous_year = current_month

        month_dict = {
            1: "Jan' " + str(current_year),
            2: "Feb' " + str(current_year),
            3: "Mar' " + str(current_year),
            4: "Apr' " + str(current_year),
            5: "May' " + str(current_year),
            6: "Jun' " + str(current_year),
            7: "Jul' " + str(previous_year),
            8: "Aug' " + str(previous_year),
            9: "Sep' " + str(previous_year),
            10: "Oct' " + str(previous_year),
            11: "Nov' " + str(previous_year),
            12: "Dec' " + str(previous_year)
        }

        for item in query_data_list:
            res_data = item.get('total_pos')
            month_list = json.loads(item.get('month_list'))
            month_name_list = [month_dict[i] for i in month_list]
            classification_json_data = item.get('classification_json_data')

            montwise_pos_final_list = json.loads(json.dumps(ast.literal_eval(classification_json_data)))

        data_list = [month_name_list, montwise_pos_final_list,res_data]

        return data_list


    def monthon_month_unique_pos(self, cr, uid, start_date=None, end_date=None, sale_type=None, context=None):

        start_date_year = start_date.split('-')[0]
        end_date_year = end_date.split('-')[0]
        month_name_list = []
        montwise_pos_final_list = []
        res_data = 0

        cr.execute("SELECT * from retail_dashboard_customer_base where name = %s ",('unique_pos',))
        query_data_list = cr.dictfetchall()

        to_date = self.last_day_this_month()
        end_date = self.date_plus_one(to_date)
        tmp_date = end_date

        current_spltted_data = tmp_date.split('-')
        current_year = int(current_spltted_data[0]) % 2000
        previous_year = (int(current_spltted_data[0]) % 2000) - 1
        current_month = int(current_spltted_data[1])

        # if current_month > 6 and int(start_date_year)!=int(end_date_year):
        #     previous_year = previous_year
        # else:
        #     previous_year = current_year
        if current_month > 6:
            previous_year = current_month


        month_dict = {
            1: "Jan' " + str(current_year),
            2: "Feb' " + str(current_year),
            3: "Mar' " + str(current_year),
            4: "Apr' " + str(current_year),
            5: "May' " + str(current_year),
            6: "Jun' " + str(current_year),
            7: "Jul' " + str(previous_year),
            8: "Aug' " + str(previous_year),
            9: "Sep' " + str(previous_year),
            10: "Oct' " + str(previous_year),
            11: "Nov' " + str(previous_year),
            12: "Dec' " + str(previous_year)
        }

        for item in query_data_list:
            res_data = item.get('total_pos')
            month_list = json.loads(item.get('month_list'))
            month_name_list = [month_dict[i] for i in month_list]
            classification_json_data = item.get('classification_json_data')

            montwise_pos_final_list = json.loads(json.dumps(ast.literal_eval(classification_json_data)))

        data_list = [month_name_list, montwise_pos_final_list,res_data]

        return data_list


    def monthon_month_active_pos(self, cr, uid, start_date=None, end_date=None, sale_type=None, context=None):

        start_date_year = start_date.split('-')[0]
        end_date_year = end_date.split('-')[0]
        month_name_list = []
        montwise_pos_final_list = []
        res_data = 0

        cr.execute("SELECT * from retail_dashboard_customer_base where name = %s ",('active_pos',))
        query_data_list = cr.dictfetchall()

        to_date = self.last_day_this_month()
        end_date = self.date_plus_one(to_date)
        tmp_date = end_date

        current_spltted_data = tmp_date.split('-')
        current_year = int(current_spltted_data[0]) % 2000
        previous_year = (int(current_spltted_data[0]) % 2000) - 1
        current_month = int(current_spltted_data[1])

        # if current_month > 6 and int(start_date_year)!=int(end_date_year):
        #     previous_year = previous_year
        # else:
        #     previous_year = current_year
        if current_month > 6:
            previous_year = current_month


        month_dict = {
            1: "Jan' " + str(current_year),
            2: "Feb' " + str(current_year),
            3: "Mar' " + str(current_year),
            4: "Apr' " + str(current_year),
            5: "May' " + str(current_year),
            6: "Jun' " + str(current_year),
            7: "Jul' " + str(previous_year),
            8: "Aug' " + str(previous_year),
            9: "Sep' " + str(previous_year),
            10: "Oct' " + str(previous_year),
            11: "Nov' " + str(previous_year),
            12: "Dec' " + str(previous_year)
        }

        for item in query_data_list:
            res_data = item.get('total_pos')
            month_list = json.loads(item.get('month_list'))
            month_name_list = [month_dict[i] for i in month_list]
            classification_json_data = item.get('classification_json_data')

            montwise_pos_final_list = json.loads(json.dumps(ast.literal_eval(classification_json_data)))

        data_list = [month_name_list, montwise_pos_final_list,res_data]

        return data_list


    def monthon_month_new_pos(self, cr, uid, start_date=None, end_date=None, sale_type=None, context=None):

        start_date_year = start_date.split('-')[0]
        end_date_year = end_date.split('-')[0]
        month_name_list = []
        montwise_pos_final_list = []
        res_data = 0

        cr.execute("SELECT * from retail_dashboard_customer_base where name = %s ", ('new_pos',))
        query_data_list = cr.dictfetchall()

        to_date = self.last_day_this_month()
        end_date = self.date_plus_one(to_date)
        tmp_date = end_date

        current_spltted_data = tmp_date.split('-')
        current_year = int(current_spltted_data[0]) % 2000
        previous_year = (int(current_spltted_data[0]) % 2000) - 1
        current_month = int(current_spltted_data[1])

        # if current_month > 6 and int(start_date_year)!=int(end_date_year):
        #     previous_year = previous_year
        # else:
        #     previous_year = current_year
        if current_month > 6:
            previous_year = current_month



        month_dict = {
            1: "Jan' " + str(current_year),
            2: "Feb' " + str(current_year),
            3: "Mar' " + str(current_year),
            4: "Apr' " + str(current_year),
            5: "May' " + str(current_year),
            6: "Jun' " + str(current_year),
            7: "Jul' " + str(previous_year),
            8: "Aug' " + str(previous_year),
            9: "Sep' " + str(previous_year),
            10: "Oct' " + str(previous_year),
            11: "Nov' " + str(previous_year),
            12: "Dec' " + str(previous_year)
        }

        for item in query_data_list:
            res_data = item.get('total_pos')
            month_list = json.loads(item.get('month_list'))
            month_name_list = [month_dict[i] for i in month_list]
            classification_json_data = item.get('classification_json_data')

            montwise_pos_final_list = json.loads(json.dumps(ast.literal_eval(classification_json_data)))

        data_list = [month_name_list, montwise_pos_final_list, res_data]

        return data_list


        # def monthon_month_pos_data(self, cr, uid,query_data_list,cumulative=False,context=None):
        #     month_list = []
        #
        #     grocery_sum = 0
        #     stationary_sum = 0
        #     electronics_sum = 0
        #     mobile_sum = 0
        #     others_sum = 0
        #     chemical_sum = 0
        #     soho_sum = 0
        #
        #     ######################from the beginning#######################################33
        #     if cumulative == True and context.get('data'):
        #
        #         for item in context['data']:
        #             classification = item.get('classification').split()
        #
        #             if item.get('classification').startswith('Retail') and len(classification)>1 and 'Grocery' in \
        #                     item.get('classification').split()[2]:
        #
        #                 grocery_sum += item.get('partner_id_count')
        #             elif item.get('classification').startswith('Retail') and len(classification)>1 and  'Stationary' in \
        #                     item.get('classification').split()[2]:
        #                 stationary_sum += item.get('partner_id_count')
        #             elif item.get('classification').startswith('Retail') and len(classification)>1 and  'Electronics' in \
        #                     item.get('classification').split()[2]:
        #
        #                 electronics_sum += item.get('partner_id_count')
        #             elif item.get('classification').startswith('Retail') and len(classification)>1 and 'Mobile' in \
        #                     item.get('classification').split()[2]:
        #                 mobile_sum += item.get('partner_id_count')
        #             elif item.get('classification').startswith('Retail') and len(classification)>1 and 'Industrial' in \
        #                     item.get('classification').split()[2]:
        #                 chemical_sum += item.get('partner_id_count')
        #
        #             elif 'SOHO' in item.get('classification'):
        #
        #                 soho_sum += item.get('partner_id_count')
        #             else:
        #                 if item.get('classification').startswith('Retail'):
        #                     others_sum += item.get('partner_id_count')
        #
        #     ########################################################
        #
        #     for items in query_data_list:
        #         if items.get('month_wise') not in month_list:
        #             month_list.append(items.get('month_wise'))
        #     month_list = list(dict.fromkeys(month_list))
        #
        #     cl_pos_dict = {}
        #     pre_cl_pos_dict = {}
        #     curr_cl_pos_dict = {}
        #
        #     for pos_create_month in month_list:
        #         grocery_pos = 0
        #         stationary_pos = 0
        #         electronics_pos = 0
        #         mobile_pos = 0
        #         others_pos = 0
        #         chemical_pos = 0
        #         soho_pos = 0
        #         for items in query_data_list:
        #             if pos_create_month == items.get('month_wise'):
        #
        #                 if items.get('classification').startswith('Retail') and 'Grocery' in items.get('classification').split()[2]:
        #                     if items.get('parent_id'):
        #                         grocery_pos += 1
        #                     else:
        #                         grocery_pos += items.get('partner_id_count') if items.get('partner_id_count') else 0
        #                 elif items.get('classification').startswith('Retail') and 'Stationary' in items.get('classification').split()[2]:
        #                     if items.get('parent_id'):
        #                         stationary_pos += 1
        #                     else:
        #                         stationary_pos += items.get('partner_id_count') if items.get('partner_id_count') else 0
        #                 elif items.get('classification').startswith('Retail') and 'Electronics' in items.get('classification').split()[2]:
        #                     if items.get('parent_id'):
        #                         electronics_pos += 1
        #                     else:
        #                         electronics_pos += items.get('partner_id_count') if items.get('partner_id_count') else 0
        #                 elif items.get('classification').startswith('Retail') and 'Mobile' in items.get('classification').split()[2]:
        #                     if items.get('parent_id'):
        #                         mobile_pos += 1
        #                     else:
        #                         mobile_pos += items.get('partner_id_count') if items.get('partner_id_count') else 0
        #                 elif items.get('classification').startswith('Retail') and 'Industrial' in items.get('classification').split()[2]:
        #                     if items.get('parent_id'):
        #                         chemical_pos += 1
        #                     else:
        #                         chemical_pos += items.get('partner_id_count') if items.get('partner_id_count') else 0
        #                 elif 'SOHO' in items.get('classification'):
        #                     if items.get('parent_id'):
        #                         soho_pos += 1
        #                     else:
        #                         soho_pos += items.get('partner_id_count') if items.get('partner_id_count') else 0
        #                 else:
        #                     if items.get('classification').startswith('Retail'):
        #                         others_pos += items.get('partner_id_count') if items.get('partner_id_count') else 0
        #
        #         if cumulative == True:
        #             if int(pos_create_month) > 6:
        #                 grocery_sum += grocery_pos
        #                 stationary_sum += stationary_pos
        #                 electronics_sum += electronics_pos
        #                 mobile_sum += mobile_pos
        #                 others_sum += others_pos
        #                 chemical_sum += chemical_pos
        #                 soho_sum += soho_pos
        #                 cl_pos_dict[pos_create_month] = [grocery_sum, stationary_sum, electronics_sum, mobile_sum, others_sum,
        #                                                  chemical_sum, soho_sum]
        #             else:
        #                 pre_cl_pos_dict[pos_create_month] = [grocery_pos, stationary_pos, electronics_pos, mobile_pos, others_pos,chemical_pos, soho_pos]
        #         else:
        #             cl_pos_dict[pos_create_month] = [grocery_pos, stationary_pos, electronics_pos, mobile_pos,
        #                                                  others_pos, chemical_pos, soho_pos]
        #
        #     if cumulative == True:
        #         data = cl_pos_dict[cl_pos_dict.keys()[-1]]
        #         grocery = data[0]
        #         stationary = data[1]
        #         electronics = data[2]
        #         mobile = data[3]
        #         others = data[4]
        #         chemical = data[5]
        #         soho = data[6]
        #
        #         for k1, v1 in pre_cl_pos_dict.iteritems():
        #             grocery += v1[0]
        #             stationary += v1[1]
        #             electronics += v1[2]
        #             mobile += v1[3]
        #             others += v1[4]
        #             chemical += v1[5]
        #             soho += v1[6]
        #             curr_cl_pos_dict[k1] = [grocery, stationary, electronics, mobile, others,
        #                                                  chemical, soho]
        #         cl_pos_dict.update(curr_cl_pos_dict)
        #
        #     grocery_data = []
        #     stationary_data = []
        #     electronics_data = []
        #     mobile_data = []
        #     others_data = []
        #     chemical_data = []
        #     soho_data = []
        #
        #     for k, v in cl_pos_dict.iteritems():
        #         grocery_data.append(v[0])
        #         stationary_data.append(v[1])
        #         electronics_data.append(v[2])
        #         mobile_data.append(v[3])
        #         others_data.append(v[4])
        #         chemical_data.append(v[5])
        #         soho_data.append(v[6])
        #
        #     montwise_pos_final_list = [{
        #         'name': 'Grocery',
        #         'data': grocery_data
        #     },
        #         {
        #             'name': 'Stationary',
        #             'data': stationary_data
        #         },
        #         {
        #             'name': 'Electronics',
        #             'data': electronics_data
        #         },
        #         {
        #             'name': 'Mobile',
        #             'data': mobile_data
        #         },
        #         {
        #             'name': 'Others',
        #             'data': others_data
        #         },
        #         {
        #             'name': 'SOHO',
        #             'data': soho_data
        #         },
        #         {
        #             'name': 'Industrial & Chemical',
        #             'data': chemical_data
        #         },
        #
        #     ]
        #
        #     return [montwise_pos_final_list]


class retail_dashboard_customer_base(osv.osv):
    _name = 'retail.dashboard.customer.base'

    _columns = {
        'name': fields.char('Name'),
        'from_date': fields.date('From Date'),
        'to_date': fields.date('To Date'),
        'total_pos': fields.integer('Total POS'),
        'month_list': fields.text('Month List'),
        'classification_json_data': fields.text('Json Data')
    }
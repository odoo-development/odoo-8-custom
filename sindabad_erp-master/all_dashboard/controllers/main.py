
from openerp.addons.web import http
from openerp.addons.web.http import request

class org_chart_dept(http.Controller):
    @http.route(["/management/dashboard/"], type='http', auth="public", website=True)
    def view(self, message=False, **post):

        values = {}

        return request.website.render('all_dashboard.management_chart', values)

    @http.route(["/retail/dashboard/"], type='http', auth="public", website=True)
    def view_retail(self, message=False, **post):

        values = {}

        return request.website.render('all_dashboard.retail_chart', values)

    @http.route(["/corporate/dashboard/"], type='http', auth="public", website=True)
    def view_corporate(self, message=False, **post):
        values = {}

        return request.website.render('all_dashboard.corporate_chart', values)

    @http.route(["/warehouse/dashboard/"], type='http', auth="public", website=True)
    def view_warehouse(self, message=False, **post):
        values = {}

        return request.website.render('all_dashboard.warehouse_chart', values)


{
    'name': "mushak challan report",
    'version': '8.0.1.0.0',
    'summary': """Mushak challan report; mushak no:6.3""",
    'description': """ With this module, we can perform mushak challan with optional filters such as daterange, invoce etc.""",
    'author': "Ashif Raihun",
    'website': "https://www.sindabad.com",
    'company': '',
    'maintainer': '',
    'category': 'sale',
    'depends': ['vat_calculation_custom'],

    'data': [

            'report/mushak_challan_report.xml',
    ],
    'installable': True,
    'auto_install': False,
    'application': False,
}
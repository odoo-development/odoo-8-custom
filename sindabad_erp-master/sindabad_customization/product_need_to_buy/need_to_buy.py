from openerp.osv import fields, osv
from operator import itemgetter

class product_need_to_buy(osv.osv):
    _name = "product.need.to.buy"



    def refresh_need_to_buy_list(self, cr, uid, ids, context=None):

        product_id = self.browse(cr, uid, ids, context)[0].name[0].id

        # product model call kora
        product_obj = self.pool.get('product.product').browse(cr, uid, product_id, context=context)
        uttra_forcast_quantity = product_obj.uttwh_quantity - product_obj.uttwh_forcast_quantity
        nodda_forcast_quantity = product_obj.nodda_quantity - product_obj.nodda_forcast_quantity
        moti_forcast_quantity = product_obj.moti_quantity - product_obj.moti_forcast_quantity
        uttra_retail_forcast_quantity = product_obj.uttwh_retail_quantity - product_obj.uttwh_retail_forcast_quantity
        nodda_retail_forcast_quantity = product_obj.nodda_retail_quantity - product_obj.nodda_retail_forcast_quantity
        moti_retail_forcast_quantity = product_obj.moti_retail_quantity - product_obj.moti_retail_forcast_quantity

        # cr execute
        for id in ids:
            ## update query
            cr.execute("update product_need_to_buy set "
                       "default_code=%s, "
                       "prod_uom=%s, "
                       "categ_id=%s, "
                       "lst_price=%s, "
                       "standard_price=%s, "
                       "incoming_qty=%s, "
                       "outgoing_qty=%s, "
                       "calculate_qty_available=%s, "
                       "calculate_virtual_available=%s, "
                       "calculate_damage_quantity=%s, "
                       "uttwh_quantity=%s, "
                       "uttwh_forcast_quantity=%s, "
                       "uttwh_free_quantity=%s, "
                       "uttwh_retail_quantity=%s, "
                       "uttwh_retail_forcast_quantity=%s, "
                       "uttwh_retail_free_quantity=%s, "
                       "nodda_quantity=%s, "
                       "nodda_forcast_quantity=%s, "
                       "nodda_free_quantity=%s, "
                       "nodda_retail_quantity=%s, "
                       "nodda_retail_forcast_quantity=%s, "
                       "nodda_retail_free_quantity=%s, "
                       "moti_quantity=%s, "
                       "moti_forcast_quantity=%s, "
                       "moti_free_quantity=%s, "
                       "moti_retail_quantity=%s, "
                       "moti_retail_forcast_quantity=%s, "
                       "moti_retail_free_quantity=%s "
                       "where id=%s", ([product_obj.default_code,
                                        product_obj.prod_uom,
                                        product_obj.categ_id.id,
                                        product_obj.lst_price,
                                        product_obj.standard_price,
                                        product_obj.incoming_qty,
                                        product_obj.outgoing_qty,
                                        product_obj.qty_available,
                                        product_obj.virtual_available,
                                        product_obj.damage_quantity,
                                        product_obj.uttwh_quantity,
                                        uttra_forcast_quantity,
                                        product_obj.uttwh_free_quantity,
                                        product_obj.uttwh_retail_quantity,
                                        uttra_retail_forcast_quantity,
                                        product_obj.uttwh_retail_free_quantity,
                                        product_obj.nodda_quantity,
                                        nodda_forcast_quantity,
                                        product_obj.nodda_free_quantity,
                                        product_obj.nodda_retail_quantity,
                                        nodda_retail_forcast_quantity,
                                        product_obj.nodda_retail_free_quantity,
                                        product_obj.moti_quantity,
                                        moti_forcast_quantity,
                                        product_obj.moti_free_quantity,
                                        product_obj.moti_retail_quantity,
                                        moti_retail_forcast_quantity,
                                        product_obj.moti_retail_free_quantity,
                                        id]))

            cr.commit()
        return product_obj


    def daily_delete_need_to_buy_list(self, cr, uid, ids=None,  context=None):

        cr.execute('delete from product_need_to_buy')
        cr.commit()
        return True


    def emergency_need_to_buy_list(self, cr, uid, ids=None, context=None):

        product_obj = self.pool.get('product.product')
        product_id_lists = product_obj.search(cr, uid, [('virtual_available','!=',0.0)])


        for product in product_obj.browse(cr, uid, product_id_lists, context=None):


            ntbl_id = self.pool.get('product.need.to.buy')
            ntbl_list = ntbl_id.search(cr, uid, [('name', '=', product.id)])

            try:

                uttra_forcast_quantity = product.uttwh_quantity - product.uttwh_forcast_quantity
                nodda_forcast_quantity = product.nodda_quantity - product.nodda_forcast_quantity
                moti_forcast_quantity = product.moti_quantity - product.moti_forcast_quantity
                uttra_retail_forcast_quantity = product.uttwh_retail_quantity - product.uttwh_retail_forcast_quantity
                nodda_retail_forcast_quantity = product.nodda_retail_quantity - product.nodda_retail_forcast_quantity
                moti_retail_forcast_quantity = product.moti_retail_quantity - product.moti_retail_forcast_quantity
            except:
                pass

            if not ntbl_list:

                ntbl_insert_query = "INSERT INTO product_need_to_buy (name, default_code, prod_uom, categ_id, lst_price, standard_price, incoming_qty, outgoing_qty, calculate_qty_available, calculate_virtual_available, calculate_damage_quantity, uttwh_quantity, uttwh_forcast_quantity, uttwh_free_quantity, uttwh_retail_quantity, uttwh_retail_forcast_quantity, uttwh_retail_free_quantity, nodda_quantity, nodda_forcast_quantity, nodda_free_quantity, nodda_retail_quantity, nodda_retail_forcast_quantity, nodda_retail_free_quantity, moti_quantity, moti_forcast_quantity, moti_free_quantity, moti_retail_quantity, moti_retail_forcast_quantity, moti_retail_free_quantity) VALUES ({0}, $${1}$$,'{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}','{11}','{12}','{13}','{14}','{15}','{16}','{17}','{18}','{19}','{20}','{21}','{22}','{23}','{24}','{25}','{26}','{27}','{28}')".format(product.id,
                        product.default_code,
                        product.prod_uom,
                        product.categ_id.id,
                        product.lst_price,
                        product.standard_price,
                        product.incoming_qty,
                        product.outgoing_qty,
                        product.qty_available,
                        product.virtual_available,
                        product.damage_quantity,
                        product.uttwh_quantity,
                        uttra_forcast_quantity,
                        product.uttwh_free_quantity,
                        product.uttwh_retail_quantity,
                        uttra_retail_forcast_quantity,
                        product.uttwh_retail_free_quantity,
                        product.nodda_quantity,
                        nodda_forcast_quantity,
                        product.nodda_free_quantity,
                        product.nodda_retail_quantity,
                        nodda_retail_forcast_quantity,
                        product.nodda_retail_free_quantity,
                        product.moti_quantity,
                        moti_forcast_quantity,
                        product.moti_free_quantity,
                        product.moti_retail_quantity,
                        moti_retail_forcast_quantity,
                        product.moti_retail_free_quantity)

                cr.execute(ntbl_insert_query)
                cr.commit()

            else:

                cr.execute("update product_need_to_buy set "
                           "default_code=%s, "
                           "prod_uom=%s, "
                           "categ_id=%s, "
                           "lst_price=%s, "
                           "standard_price=%s, "
                           "incoming_qty=%s, "
                           "outgoing_qty=%s, "
                           "calculate_qty_available=%s, "
                           "calculate_virtual_available=%s, "
                           "calculate_damage_quantity=%s, "
                           "uttwh_quantity=%s, "
                           "uttwh_forcast_quantity=%s, "
                           "uttwh_free_quantity=%s, "
                           "uttwh_retail_quantity=%s, "
                           "uttwh_retail_forcast_quantity=%s, "
                           "uttwh_retail_free_quantity=%s, "
                           "nodda_quantity=%s, "
                           "nodda_forcast_quantity=%s, "
                           "nodda_free_quantity=%s, "
                           "nodda_retail_quantity=%s, "
                           "nodda_retail_forcast_quantity=%s, "
                           "nodda_retail_free_quantity=%s, "
                           "moti_quantity=%s, "
                           "moti_forcast_quantity=%s, "
                           "moti_free_quantity=%s, "
                           "moti_retail_quantity=%s, "
                           "moti_retail_forcast_quantity=%s, "
                           "moti_retail_free_quantity=%s "
                           "where id=%s", ([product.default_code,
                                            product.prod_uom,
                                            product.categ_id.id,
                                            product.lst_price,
                                            product.standard_price,
                                            product.incoming_qty,
                                            product.outgoing_qty,
                                            product.qty_available,
                                            product.virtual_available,
                                            product.damage_quantity,
                                            product.uttwh_quantity,
                                            uttra_forcast_quantity,
                                            product.uttwh_free_quantity,
                                            product.uttwh_retail_quantity,
                                            uttra_retail_forcast_quantity,
                                            product.uttwh_retail_free_quantity,
                                            product.nodda_quantity,
                                            nodda_forcast_quantity,
                                            product.nodda_free_quantity,
                                            product.nodda_retail_quantity,
                                            nodda_retail_forcast_quantity,
                                            product.nodda_retail_free_quantity,
                                            product.moti_quantity,
                                            moti_forcast_quantity,
                                            product.moti_free_quantity,
                                            product.moti_retail_quantity,
                                            moti_retail_forcast_quantity,
                                            product.moti_retail_free_quantity,
                                            ntbl_list[0]]))

                cr.commit()

        return product




    def _get_po_line_number(self, cr, uid, ids, fields, arg, context=None):
        if context is None:
            context = {}
        res = {}
        po_line = 1
        po_line_recs = self.browse(cr, uid, ids, context=context)[0]
        for line_rec in po_line_recs.order_id.order_line:
            res[line_rec.id] = po_line
            po_line += 1

        return res




    def auto_po_generate_need_to_buy_list(self, cr, uid, ids=None, context=None):


        if context is None:
            context = {}


        purchase_obj = self.pool.get('purchase.order')

        need_to_buy_obj = self.pool['product.need.to.buy']


        # Warehouse Locations

        stock_picking_type_ids = self.pool['stock.picking.type'].search(cr, uid, [('name', 'like', 'Receipts')], context=context)
        stock_picking_type_data = self.pool['stock.picking.type'].browse(cr, uid, stock_picking_type_ids, context=context)

        # End Here


        category_obj = self.pool.get('product.category')
        categ_list = category_obj.search(cr, uid, [('name', 'ilike', 'M.C-'), ('property_account_income_categ', '!=', False), ('property_account_expense_categ', '!=', False), ('property_stock_account_input_categ', '!=', False), ('property_stock_account_output_categ', '!=', False)], context=context)

        ### Exsiting product List generation  From PO

        existing_product_list=[]
        existing_product_id_lists=[]

        draft_purchase_id_lists_with_system_generated = purchase_obj.search(cr, uid, [('system_generated_po', '=', True),('state', '=', 'draft')], context=context)
        draft_purchase_id_with_system_generated_objects = purchase_obj.browse(cr, uid, draft_purchase_id_lists_with_system_generated, context=context)


        for draft_purchase_obj in draft_purchase_id_with_system_generated_objects:
            for items in draft_purchase_obj.order_line:
                existing_product_list.append(items)
                existing_product_id_lists.append(items.product_id.id)


        ## Ends Here


        ### Get product list of warehouse qty sample = {product_id : { location_id:qty}}

        draft_purchase_id_lists = purchase_obj.search(cr, uid, [('state', '=', 'draft')], context=context)
        draft_purchase_id_objects = purchase_obj.browse(cr, uid, draft_purchase_id_lists, context=context)

        result_dict = {}


        for po_obj in draft_purchase_id_objects:
            location_id = po_obj.picking_type_id.warehouse_id.id

            for item_lines in po_obj.order_line:

                tmp_dict = {}

                if result_dict.get(item_lines.product_id.id):

                    tmp_dict = result_dict.get(item_lines.product_id.id)
                    if tmp_dict.get(location_id):
                        tmp_dict[location_id] = tmp_dict.get(location_id) + item_lines.product_qty
                    else:
                        tmp_dict[location_id] = item_lines.product_qty

                    result_dict[item_lines.product_id.id] = tmp_dict
                else:
                    result_dict[item_lines.product_id.id] = {location_id:item_lines.product_qty}

        # import pdb;pdb.set_trace()

        ### Ends Here







        for category_id in categ_list:

            for receipts_id in stock_picking_type_data:

                data = {}
                order_line = []
                found_query = False

                data['order_line'] = order_line
                data['partner_id'] = 4048
                data['pricelist_id'] = 2
                data['date_order'] = fields.datetime.now()

                data['location_id'] = receipts_id.default_location_dest_id.id
                data['adv_po_snd'] = 'credit'
                data['system_generated_po'] = True

                query = [('categ_id', '=', category_id)]
                data['picking_type_id'] = receipts_id.id

                if receipts_id.warehouse_id.id == 1:
                    query.append(('uttwh_forcast_quantity', '<', 0))
                    found_query = True

                elif receipts_id.warehouse_id.id == 2:
                    query.append(('nodda_forcast_quantity', '<', 0))
                    found_query = True

                elif receipts_id.warehouse_id.id == 3:
                    query.append(('nodda_retail_forcast_quantity', '<', 0))
                    found_query = True

                elif receipts_id.warehouse_id.id == 4:
                    query.append(('moti_forcast_quantity', '<', 0))
                    found_query = True

                elif receipts_id.warehouse_id.id == 6:
                    query.append(('uttwh_retail_forcast_quantity', '<', 0))
                    found_query = True

                elif receipts_id.warehouse_id.id == 7:
                    query.append(('moti_retail_forcast_quantity', '<', 0))
                    found_query = True




                if found_query == True:

                    products_list = need_to_buy_obj.search(cr, uid, query, context=context)
                else:
                    products_list = []




                for product_line in need_to_buy_obj.browse(cr, uid, products_list, context=context):
                    prod_qty = 0

                    new_tmp_dict = result_dict.get(product_line.name.id) if result_dict.get(product_line.name.id) else {}

                    tmp_val = 0

                    if receipts_id.warehouse_id.id == 1:
                        tmp_val = new_tmp_dict.get(1) if new_tmp_dict.get(1) else 0
                        prod_qty = product_line.uttwh_forcast_quantity + tmp_val

                    elif receipts_id.warehouse_id.id == 2:
                        tmp_val = new_tmp_dict.get(2) if new_tmp_dict.get(2) else 0
                        prod_qty = product_line.nodda_forcast_quantity + tmp_val

                    elif receipts_id.warehouse_id.id == 3:
                        tmp_val = new_tmp_dict.get(3) if new_tmp_dict.get(3) else 0
                        prod_qty = product_line.nodda_retail_forcast_quantity + tmp_val

                    elif receipts_id.warehouse_id.id == 4:
                        tmp_val = new_tmp_dict.get(4) if new_tmp_dict.get(4) else 0
                        prod_qty = product_line.moti_forcast_quantity + tmp_val

                    elif receipts_id.warehouse_id.id == 6:
                        tmp_val = new_tmp_dict.get(6) if new_tmp_dict.get(6) else 0
                        prod_qty = product_line.uttwh_retail_forcast_quantity + tmp_val

                    elif receipts_id.warehouse_id.id == 7:
                        tmp_val = new_tmp_dict.get(7) if new_tmp_dict.get(7) else 0
                        prod_qty = product_line.moti_retail_forcast_quantity + tmp_val


                    already_found_in_draft = False


                    if prod_qty < 0:

                        for p_line in existing_product_list:


                            if p_line.product_id.id == product_line.name.id and p_line.order_id.location_id.id == receipts_id.default_location_dest_id.id:
                                prod_qty = abs(prod_qty)+p_line.product_qty

                                cr.execute("UPDATE purchase_order_line set product_qty=%s where id = %s", (prod_qty,p_line.id))
                                already_found_in_draft=True

                                break





                        if already_found_in_draft == False:
                            order_line.append([0, False, {
                                'po_line_prod_uom': product_line.name.prod_uom or False,
                                'product_id': product_line.name.id,
                                'name': product_line.name.product_name,
                                'date_planned': fields.date.today(),
                                'product_qty': abs(prod_qty),
                                'product_uom': 1,
                                'price_unit': product_line.name.standard_price,
                                'image_small': product_line.name.image_small or False,
                                # 'taxes_id': product_line.taxes_id,
                            }])


                if len(order_line) > 0:

                    purchase_obj.create(cr, uid, data, context)

        return True



    def _get_stn_suggestion(self, cr, uid, ids,field_name, args, context=None):
        res ={}
        for product_buy in self.browse(cr, uid, ids, context=context):

            stn_text =''

            free_qty_dict = {}
            forcast_qty_dict = {}

            free_code_qty ={
                'uttwh_free_qty':'UttWH',
                'uttwh_rt_free_qty':'UTTRT',
                'nodda_free_qty':'NODDA',
                'nodda_rt_free_qty':'NODRT',
                'moti_free_qty':'MTWH',
            }

            forcast_code_qty = {
                'uttwh_forcast_quantity': 'UttWH',
                'uttwh_retail_forcast_quantity': 'UTTRT',
                'nodda_forcast_quantity': 'NODDA',
                'nodda_retail_forcast_quantity': 'NODRT',
                'moti_forcast_quantity': 'MTWH',
            }

            free_qty_dict['uttwh_free_qty'] = product_buy.uttwh_free_quantity
            free_qty_dict['uttwh_rt_free_qty'] = product_buy.uttwh_retail_free_quantity
            free_qty_dict['nodda_free_qty'] = product_buy.nodda_free_quantity
            free_qty_dict['nodda_rt_free_qty'] = product_buy.nodda_retail_free_quantity
            free_qty_dict['moti_free_qty'] = product_buy.moti_free_quantity

            forcast_qty_dict['uttwh_forcast_quantity'] = abs(product_buy.uttwh_forcast_quantity)
            forcast_qty_dict['uttwh_retail_forcast_quantity'] = abs(product_buy.uttwh_retail_forcast_quantity)
            forcast_qty_dict['nodda_forcast_quantity'] = abs(product_buy.nodda_forcast_quantity)
            forcast_qty_dict['nodda_retail_forcast_quantity'] = abs(product_buy.nodda_retail_forcast_quantity)
            forcast_qty_dict['moti_forcast_quantity'] = abs(product_buy.moti_forcast_quantity)


            ## Check the greater QTY and assign them to

            for i in range(0,5,1):  # Here 5 is the number of warehouses

                for key, value in forcast_qty_dict.iteritems():
                    update_val=False
                    if value >0:
                        for k2,v2 in free_qty_dict.iteritems():
                            if v2 >= value and free_code_qty[k2] != forcast_code_qty[key]:
                                stn_text = stn_text +str(free_code_qty.get(k2)) + str(' ==> ') + str(forcast_code_qty.get(key)) + str(' : ')+ str(value)+ str(', ')
                                v2 = v2-value
                                free_qty_dict[k2] = v2
                                value=0
                                forcast_qty_dict[key]=value
                                update_val = True
                                break
            ## Ends Here the greater QTy


            ## Check the lesser QTY and assign them to

            for i in range(0,5,1):  # Here 5 is the number of warehouses

                for key, value in forcast_qty_dict.iteritems():
                    update_val=False
                    if value >0:
                        for k2,v2 in free_qty_dict.iteritems():
                            if value >= v2 and v2 >0 and free_code_qty[k2] != forcast_code_qty[key]:
                                stn_text = stn_text +str(free_code_qty.get(k2)) + str(' ==> ') + str(forcast_code_qty.get(key)) + str(' : ')+ str(v2)+ str(', ')
                                value = value-v2
                                v2 = 0
                                free_qty_dict[k2] = v2
                                forcast_qty_dict[key]=value
                                update_val = True
                                break


            ## Ending Here of lesser qty

            res[product_buy.id] = stn_text


            if len(stn_text) >4 and product_buy.stn_true == False:
                cr.execute('update product_need_to_buy set stn_true=TRUE where id=%s', [product_buy.id])

            if len(stn_text) <4 and product_buy.stn_true == True:
                cr.execute('update product_need_to_buy set stn_true=FALSE where id=%s', [product_buy.id])





        return res

    _columns = {

        'name': fields.many2one('product.product', 'Product'),
        'default_code': fields.char(string='SKU'),
        'prod_uom': fields.char(string='UOM'),
        # 'product_category': fields.char(string='Product Category'),
        'categ_id': fields.many2one('product.category', string='Product Category'),

        'lst_price': fields.char(string='Sale Price'),
        'standard_price': fields.char(string='Cost Price'),
        'incoming_qty': fields.char(string='Incoming'),
        'outgoing_qty': fields.char(string='Outgoing'),

        'calculate_qty_available': fields.float(string='Calculate Quantity On Hand'),
        'calculate_virtual_available': fields.float(string='Calculate Forecast Quantity'),
        'calculate_damage_quantity': fields.float(string='Calculate Damage Quantity'),

        'uttwh_quantity': fields.float(string='Uttara Quantity'),
        'uttwh_forcast_quantity': fields.float(string='Uttara Forecast Quantity'),
        'uttwh_free_quantity': fields.float(string='Uttara Free Quantity'),

        'uttwh_retail_quantity': fields.float(string='Uttara Retail Quantity'),
        'uttwh_retail_forcast_quantity': fields.float(string='Uttara Retail Forecast Qty'),
        'uttwh_retail_free_quantity': fields.float(string='Uttara Retail Free Qty'),

        'nodda_quantity': fields.float(string='Nodda Quantity'),
        'nodda_forcast_quantity': fields.float(string='Nodda Forecast Quantity'),
        'nodda_free_quantity': fields.float(string='Nodda Free Quantity'),

        'nodda_retail_quantity': fields.float(string='Nodda Retail Quantity'),
        'nodda_retail_forcast_quantity': fields.float(string='Nodda Retail Forecast Qty'),
        'nodda_retail_free_quantity': fields.float(string='Nodda Retail Free Qty'),

        'moti_quantity': fields.float(string='Motijheel Quantity'),
        'moti_forcast_quantity': fields.float(string='Motijheel Forecast Quantity'),
        'moti_free_quantity': fields.float(string='Motijheel Free Quantity'),

        'moti_retail_quantity': fields.float(string='Motijheel Retail Quantity'),
        'moti_retail_forcast_quantity': fields.float(string='Motijheel Retail Forecast Qty'),
        'moti_retail_free_quantity': fields.float( string='Motijheel Retail Free Qty'),

        'partner_id': fields.many2one('res.partner', 'Partner'),
        'pricelist_id': fields.many2one('product.pricelist', 'Pricelist'),
        'picking_type_id': fields.many2one('stock.picking.type', 'Picking Type'),
        'adv_po_snd': fields.selection([('credit', 'Credit'), ('advance', 'Advance'), ], 'Payment Status'),
        'location_id': fields.many2one('stock.location', 'Location'),
        'po_line_no': fields.function(_get_po_line_number, type='integer', string='S.N'),
        'image_small': fields.related("product_id", "image_small", type="binary", string='Product Image'),
        'po_line_prod_uom': fields.related("product_id", "prod_uom", type="char", string='UOM'),
        'system_generated_po': fields.boolean('System Generated PO'),
        'stn_suggestion': fields.function(_get_stn_suggestion, type="char", string='STN Suggestion'),
        'stn_true': fields.boolean('STN Exist')
    }






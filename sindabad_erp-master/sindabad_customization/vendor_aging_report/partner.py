from openerp.osv import fields, osv
from datetime import datetime, timedelta

from dateutil import relativedelta


class res_partner(osv.osv):
    _inherit = 'res.partner'


    def _v_0_15(self, cr, uid, ids, fields, arg, context=None):
        lines_obj = self.pool.get('account.analytic.line')
        res = {}


        age_0 = datetime.today().date()
        age_15 = datetime.today().date() - relativedelta.relativedelta(days=16)
        age_30 = datetime.today().date() - relativedelta.relativedelta(days=31)
        age_45 = datetime.today().date() - relativedelta.relativedelta(days=46)
        age_60 = datetime.today().date() - relativedelta.relativedelta(days=61)
        age_90 = datetime.today().date() - relativedelta.relativedelta(days=91)

        from ast import literal_eval as make_tuple

        for partner_id in ids:
            age_0_15 = 0.00
            age_16_30 = 0.00
            age_31_45 = 0.00
            age_46_60 = 0.00
            age_61_90 = 0.00
            age_91 = 0.00
            due_amount = 0.00
            invoice_id_list = self.pool.get('account.invoice').search(cr, uid, [('partner_id', 'child_of', partner_id),('type', '=', 'in_invoice'),('state','=','open')], context=context)
            inv_obj_lists = self.pool.get('account.invoice').browse(cr, uid, invoice_id_list)
            for inv_obj in inv_obj_lists:

                if inv_obj.date_invoice is not False:

                    inv_date = datetime.strptime(inv_obj.date_invoice, '%Y-%m-%d').date()


                    if age_15 < inv_date <= age_0:
                        age_0_15 +=inv_obj.residual

                    elif age_30 < inv_date <= age_15:
                        age_16_30 +=inv_obj.residual

                    elif age_45 < inv_date <= age_30:
                        age_31_45 +=inv_obj.residual

                    elif age_60 < inv_date <= age_45:
                        age_46_60 +=inv_obj.residual

                    elif age_90 < inv_date <= age_60:
                        age_61_90 +=inv_obj.residual

                    else:
                        age_91 +=inv_obj.residual


                # due_amount = due_amount + inv_obj.residual

            res[partner_id] = str((age_0_15,age_16_30,age_31_45,age_46_60,age_61_90,age_91))

        return res

    _columns = {

    'v_0_15': fields.function(_v_0_15, string="V_0_15", type='char')


    }



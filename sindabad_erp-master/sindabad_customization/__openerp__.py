{
    'name': 'Sindabad Customization',
    'version': '1.0',
    'category': 'Sindabad Customization',
    'author': 'Mufti, Rocky, Shuvarthi, Apu, Shahjalal',
    'summary': 'Sindabad Customization',
    'description': 'Sindabad Customization',
    "website": "Sindabad.com",
    'depends': ['base', 'odoo_magento_connect', 'account', 'sale', 'purchase', 'report_xls'],
    'data': [

        'security/sindabad_customization_security.xml',
        'security/ir.model.access.csv',

        'vendor_aging_report/wizard/vendor_aging_report_filter.xml',
        'vendor_aging_report/report/vendor_aging_report_xls.xml',

        'product_need_to_buy/need_to_buy_view.xml',
        'product_need_to_buy/emergency_view.xml',
        'product_need_to_buy/auto_po_generate_view.xml',
        'product_need_to_buy/purchase/purchase_view.xml',


        'invoice_vat_report/invoice_vat_report_menu.xml',
        'invoice_vat_report/views/report_vatinvoice.xml',
        'customer_register_payment/customer_register_view.xml',
        'dashboard_reports_processing/dashboard_report_sync_scheduler.xml',

    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}

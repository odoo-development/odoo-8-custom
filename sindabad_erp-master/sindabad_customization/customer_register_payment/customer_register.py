# Author Mufti Muntasir Ahmed 05/09/2019


from openerp.osv import osv, fields
from openerp import SUPERUSER_ID, api
from openerp.tools.translate import _
from datetime import datetime



class customer_payment(osv.osv):
    _name = 'customer.payment'
    _description = "Customer Payment"


    def _get_period(self, cr, uid, context=None):
        if context is None: context = {}
        if context.get('period_id', False):
            return context.get('period_id')
        periods = self.pool.get('account.period').find(cr, uid, context=context)
        return periods and periods[0] or False

    _columns = {

        'customer_name': fields.many2one('res.partner', 'Customer Name',required=True,),
        'amount': fields.float('Paying Amount',required=True),
        'period_id':fields.many2one('account.period', 'Period',required=True),
        'cash_journal_id': fields.many2one('account.journal', 'Cash Payment Method',required=True),
        'ait_journal_id': fields.many2one('account.journal', 'AIT Payment Method',required=True),
        'date': fields.date('Date',required=True),
        'memo': fields.char('Memo'),
        'auto_select': fields.boolean('Auto Select By System'),
        'description': fields.char('Description'),
        'state': fields.selection([
            ('pending', 'Pending'),
            ('done', 'Done'),
            ('cancel', 'Cancelled'),

        ], 'Status', readonly=True, copy=False, help="Gives the status of the quotation or sales order", select=True),

        'customer_payment_line': fields.one2many('customer.payment.line', 'customer_payment_id', 'Customer Payment Line', required=True),

    }

    _defaults = {
        'state': 'pending',
        'date':fields.datetime.now,
        'period_id': _get_period,


    }



    _order = 'id desc'

    def onchange_auto_select(self, cr, uid, ids, customer_name=False,amount=False,auto_select=False, context=None):
        res = {}
        inv_line_list = list()
        if customer_name is not False and amount is not False and auto_select is not False:
            customer_ids = [customer_name]

            parent_list = []

            partner_obj = self.pool.get('res.partner')
            partner = partner_obj.browse(cr, uid, customer_ids, context)[0]

            if partner.parent_id:
                parent_list.append(partner.parent_id.id)

                cr.execute("select id from  res_partner where customer=True and id=%s", ([partner.parent_id.id]))

                for items in cr.fetchall():
                    parent_list.append(items[0])
            else:
                parent_list.append(partner.id)

            new_parent_list = partner_obj.search(cr, uid, [('parent_id', 'in', parent_list),('wk_address', '=', True)], context=context)



            parent_list =parent_list + new_parent_list






            inv_obj = self.pool.get('account.invoice')
            invoice_ids = inv_obj.search(cr, uid, [('state', '=', 'open'),('partner_id', 'in', parent_list)], context=context)
            invoice_ids_object = inv_obj.browse(cr, uid, invoice_ids, context)
            count =0

            for items in invoice_ids_object:
                current_amount=0
                count = count +1
                if amount == 0:
                    break
                if amount > items.residual:
                    current_amount = items.residual
                    amount = amount - items.residual
                elif amount == items.residual or amount < items.residual:
                    current_amount = amount
                    amount=0

                inv_line_list.append({
                    'invoice_amount': items.amount_total,
                    'unpaid_amount': items.residual,
                    'paid_amount': current_amount,
                    'invoice_id': items.id,
                    # 'received_quantity': 0,

                })






        res['customer_payment_line'] = inv_line_list
        return {'value': res}


    def _get_journal_id(self, cr, uid, context=None):
        if context is None:
            context = {}
        if context.get('invoice_id', False):
            currency_id = self.pool.get('account.invoice').browse(
                cr, uid, context['invoice_id'], context=context).currency_id.id
            journal_id = self.pool.get('account.journal').search(
                cr, uid, [('currency', '=', currency_id)], limit=1)
            return journal_id and journal_id[0] or False
        res = self.pool.get('account.journal').search(
            cr, uid, [('type', '=', 'bank')], limit=1)
        return res and res[0] or False


    def _get_currency_id(self, cr, uid, journal_id, context=None):
        if context is None:
            context = {}
        journal = self.pool.get('account.journal').browse(
            cr, uid, journal_id, context=context)
        if journal.currency:
            return journal.currency.id
        return self.pool.get('res.users').browse(cr, uid, uid).company_id.currency_id.id


    def invoice_payment(self, cr, uid, payment, context=None):

        if context is None:
            context = {}


        voucher_obj = self.pool.get('account.voucher')
        voucher_line_obj = self.pool.get('account.voucher.line')

        invoice_pool = self.pool.get('account.invoice')
        invoice_obj = invoice_pool.browse(cr, uid, payment.get('invoice_id'))
        invoice_name = invoice_obj.number
        partner_id = self.pool.get('res.partner')._find_accounting_partner(invoice_obj.partner_id).id

        # partner_id = payment.get('partner_id')
        journal_id = payment.get('journal_id', False)
        if not journal_id:
            journal_id = self._get_journal_id(cr, uid, context)

        accounting_journal_obj = self.pool.get("account.journal")

        account_id = accounting_journal_obj.browse(cr, uid, journal_id).default_debit_account_id.id


        if account_id is False:
            account_id = 9694

        amount = payment.get('amount', 0.0)
        date = payment.get('date')
        entry_name = payment.get('reference')
        currency_id1 = self._get_currency_id(cr, uid, journal_id)

        rate=1
        payment_rate_currency_id = currency_id1

        data = voucher_obj.onchange_amount(cr, uid, [],amount, rate, partner_id, journal_id, currency_id1, 'receipt', date, payment_rate_currency_id, 1, context)['value']



        statement_vals = {
            'reference': invoice_name + '(' + entry_name + ')',
            'journal_id': journal_id,
            'amount': amount,
            'date': date,
            'partner_id': partner_id,
            'account_id': account_id,
            'company_id': invoice_obj.company_id.id,
            'type': 'receipt',
        }

        if data.get('payment_rate_currency_id'):
            statement_vals['payment_rate_currency_id'] = data[
                'payment_rate_currency_id']
            company_currency_id = self.pool.get('res.users').browse(
                cr, uid, uid, context=context).company_id.currency_id.id
            if company_currency_id <> data['payment_rate_currency_id']:
                statement_vals['is_multi_currency'] = True

        if data.get('paid_amount_in_company_currency'):
            statement_vals['paid_amount_in_company_currency'] = data[
                'paid_amount_in_company_currency']
        if data.get('writeoff_amount'):
            statement_vals['writeoff_amount'] = data['writeoff_amount']
        if data.get('pre_line'):
            statement_vals['pre_line'] = data['pre_line']
        if data.get('payment_rate'):
            statement_vals['payment_rate'] = data['payment_rate']

        statement_id = voucher_obj.create(cr, uid, statement_vals, context)
        for line_cr in data.get('line_cr_ids'):
            line_cr.update({'voucher_id': statement_id})
            if line_cr['name'] == invoice_name:
                # line_cr['amount'] = line_cr['amount_original'] ## It was original amount of invoice. If it activated then all amount will be paid
                line_cr['amount'] = amount
                line_cr['reconcile'] = True
            line_cr_id = self.pool.get(
                'account.voucher.line').create(cr, uid, line_cr)
        for line_dr in data.get('line_dr_ids'):
            line_dr.update({'voucher_id': statement_id})
            line_dr_id = self.pool.get(
                'account.voucher.line').create(cr, uid, line_dr)

        try:
            self.pool.get('account.voucher').signal_workflow(
                cr, uid, [statement_id], 'proforma_voucher')
        except:
            return False
        return True

    def amount_mismatch_validation(self, cr, uid, vals, context=None):
        paying_amount = vals.get('amount')
        distributed_amount=0

        for items in vals.get('customer_payment_line'):
            item_val = items[2]

            if item_val.get('ait') >0:

                distributed_amount += item_val.get('ait')

            if item_val.get('paid_amount') >0:

                distributed_amount += item_val.get('paid_amount')

        if paying_amount != distributed_amount:
            raise osv.except_osv(_('warning!'), _('Amount does not match with paying amount. Please correct the amount'))

        return True


    def confirm_the_payment_invoice_wise(self, cr, uid, ids, context=None):

        for id in ids:
            payment_obj = self.browse(cr, uid, id, context)
            cash_journal_id = payment_obj.cash_journal_id.id
            ait_journal_id = payment_obj.ait_journal_id.id
            inv_date = payment_obj.date
            period_id = payment_obj.period_id.id
            partner_id = payment_obj.customer_name.id




            for items in payment_obj.customer_payment_line:
                ## Amount Distribution with ait value

                paying_amount = items.paid_amount
                ait_amount = items.ait
                invoice_amount = items.invoice_amount



                invoice_payment_dict ={}
                due_amount=0



                for invoice_id in items.client_order_ref.invoice_ids:
                    tmp_dict={}
                    if invoice_id.state == 'open':
                        due_amount= invoice_id.residual
                        if paying_amount >= due_amount:
                            tmp_dict['full_payment']=True
                            due_amount = paying_amount
                        else:
                            tmp_dict['full_payment'] = False
                            due_amount = paying_amount


                        if due_amount >= ait_amount and ait_amount > 0:
                            tmp_dict['ait_amount']=ait_amount
                            due_amount = due_amount - ait_amount
                            ait_amount = 0
                        elif ait_amount > 0 and ait_amount > due_amount:
                            tmp_dict['ait_amount'] = ait_amount
                            ait_amount = ait_amount - due_amount
                            due_amount=0


                        if paying_amount > 0:
                            tmp_dict['paid_amount']=paying_amount

                    invoice_payment_dict[invoice_id.id] = tmp_dict


                for invoice_id in items.client_order_ref.invoice_ids:

                    if invoice_payment_dict.get(invoice_id.id):
                        payment_information = invoice_payment_dict.get(invoice_id.id)

                        if payment_information.get('paid_amount'):

                            paid_amount = payment_information.get('paid_amount')


                            invoice_vals = {
                                'amount': paid_amount, ## Require the value with decending
                                'journal_id': cash_journal_id,
                                'date': inv_date,
                                'period_id': period_id,
                                'reference': 'By Customer Register Payment',
                                'name': 'By System Itself',
                                'type': 'receipt',
                                'pre_line': 'TRUE',
                                'invoice_id': invoice_id.id,

                            }

                            context['invoice_id'] = invoice_id.id

                            context = {'default_amount': paid_amount,
                                       'default_reference': 'By Customer Register Payment',
                                       'uid': uid, 'invoice_type': 'out_invoice',
                                       'journal_type': 'sale', 'default_type': 'receipt', 'search_disable_custom_filters': True,
                                       'default_partner_id': partner_id, 'payment_expected_currency': 56,
                                       'active_id': invoice_id.id, 'lang': 'en_US',
                                       'close_after_process': True, 'tz': 'Asia/Dhaka', 'active_model': 'account.invoice',
                                       'invoice_id': invoice_id.id,
                                       'params': {'menu_id': 220, 'view_type': 'form', '_push_me': False, 'action': 253,
                                                  'model': 'account.invoice', 'id':invoice_id.id},
                                       'active_ids': [invoice_id.id], 'type': 'receipt'}

                            if paid_amount > 0:
                                result = self.invoice_payment(cr, uid, payment=invoice_vals, context=context)

                        if payment_information.get('ait_amount'):

                            ait_amount = payment_information.get('ait_amount')


                            invoice_vals = {
                                'amount': ait_amount, ## Require the value with decending
                                'journal_id': ait_journal_id,
                                'date': inv_date,
                                'period_id': period_id,
                                'reference': 'By Customer Register Payment',
                                'name': 'By System Itself',
                                'type': 'receipt',
                                'pre_line': 'TRUE',
                                'invoice_id': invoice_id.id,

                            }

                            context['invoice_id'] = invoice_id.id

                            context = {'default_amount': ait_amount,
                                       'default_reference': 'By Customer Register Payment',
                                       'uid': uid, 'invoice_type': 'out_invoice',
                                       'journal_type': 'sale', 'default_type': 'receipt', 'search_disable_custom_filters': True,
                                       'default_partner_id': partner_id, 'payment_expected_currency': 56,
                                       'active_id': invoice_id.id, 'lang': 'en_US',
                                       'close_after_process': True, 'tz': 'Asia/Dhaka', 'active_model': 'account.invoice',
                                       'invoice_id': invoice_id.id,
                                       'params': {'menu_id': 220, 'view_type': 'form', '_push_me': False, 'action': 253,
                                                  'model': 'account.invoice', 'id':invoice_id.id},
                                       'active_ids': [invoice_id.id], 'type': 'receipt'}

                            if ait_amount > 0:
                                result = self.invoice_payment(cr, uid, payment=invoice_vals, context=context)

                        if payment_information.get('full_payment'):
                            cr.execute("update account_invoice set state='paid' where id=%s", ([invoice_id.id]))
                            cr.commit()





        return 0



    def create(self, cr, uid, vals, context=None):


        self.amount_mismatch_validation(cr,uid,vals=vals,context=context)


        return super(customer_payment, self).create(cr, uid, vals, context=context)




    def write(self, cr, uid, ids, vals, context=None):
        if context is None:
            context = {}

        raise osv.except_osv(_('warning!'), _('Sorry !!! There is no option to edit/rollback after saving.'))

        return super(customer_payment, self).write(cr, uid, ids, vals, context=context)


class customer_payment_list_line(osv.osv):
    _name = 'customer.payment.line'
    _description = "Customer Payment Line"

    _columns = {
        'customer_payment_id': fields.many2one('customer.payment', 'Customer Payment Line', required=True, ondelete='cascade', select=True,
                                      readonly=True),

        'client_order_ref': fields.many2one('sale.order', string="Order No.", domain=[('state', '=', ('progress', 'manual', 'shipping_except', 'done'))]),
        'order_amount': fields.float('Order Amount'),
        'deliver_amount': fields.float('Delivered Amount'),

        'invoice_id': fields.many2one('account.invoice', 'Invoice NO', required=False, ),
        'invoice_amount': fields.float('Invoice Amount'),
        'unpaid_amount': fields.float('Unpaid Amount'),
        'ait': fields.float('AIT'),
        'paid_amount': fields.float('Paying Amount'),


    }



    def onchange_client_order_ref(self, cr, uid, ids, client_order_ref=False, context=None):
        res={}
        if client_order_ref is not False:
            order_ids = [client_order_ref]
            order_obj = self.pool.get('sale.order')
            order_ids_object = order_obj.browse(cr, uid, order_ids, context)[0]

            res={

            'order_amount': order_ids_object.amount_untaxed,
            'deliver_amount': order_ids_object.delivered_amount,
            'client_order_ref': order_ids_object.id,
            'invoice_amount': order_ids_object.invoiced_amount
            }
        return {'value':res}


        

    def onchange_invoice_id(self, cr, uid, ids, invoice_id=False, context=None):
        res={}
        if invoice_id is not False:
            invoice_ids = [invoice_id]
            inv_obj = self.pool.get('account.invoice')
            invoice_ids_object = inv_obj.browse(cr, uid, invoice_ids, context)[0]
            res={

            'invoice_amount': invoice_ids_object.amount_total,
            'unpaid_amount': invoice_ids_object.residual,
            'paid_amount': 0.00,
            'ait': 0.00,
            'invoice_id': invoice_ids_object.id
            }
        return {'value':res}





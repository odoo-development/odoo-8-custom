# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2009 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import time
from openerp.report import report_sxw
from openerp.osv import osv


class invoicevatdata(report_sxw.rml_parse):
    def __init__(self, cr, uid, name, context):
        super(invoicevatdata, self).__init__(cr, uid, name, context)
        self.localcontext.update({
            'get_invoices_data': self.get_invoices_data,
            'total_vat_sum': self.total_vat_sum,
        })

    def get_invoices_data(self, obj):

        try:
            order_numbers = [obj.split(',')[0]]
        except:
            order_numbers = [str(obj)]

        group_id = None
        data_list = []

        self.cr.execute("SELECT id FROM account_invoice WHERE origin=%s", (order_numbers))
        for item in self.cr.fetchall():
            group_id = [item[0]]

        if group_id is not None:

            order_data = self.pool.get('account.invoice').browse(self.cr, self.uid, group_id)
            if str(order_data.type) == 'out_invoice' and str(order_data.state) != 'cancel':

                account_invoice_ids = order_data.invoice_line

                for items in account_invoice_ids:

                    if items.product_id.default_code:

                        vat_amount = ((items.price_unit - round(items.product_id.average_purchaes_price, 2))*items.quantity) * 0.05
                        # vat_amount = ((items.price_unit - items.product_id.standard_price)*items.quantity) * 0.05
                        p_vat = vat_amount if vat_amount >= 0 else 0.0

                        p_total_amount = items.price_subtotal - p_vat
                        # p_t_amount = (p_total_amount + vat_amount) if p_vat == 0 else p_total_amount

                        estimated_cost_price = round(items.product_id.average_purchaes_price, 2)

                        data_list.append({'name': 'cool',
                                          'product_name': items.product_id.name,
                                          'quantity': round(items.quantity,2),
                                          'unit_cost': round(items.price_unit,2),
                                          'cost_price': round(items.product_id.standard_price,2),
                                          'es_cost_price': estimated_cost_price,
                                          # 'date_invoice': it.date_invoice,
                                          'total_unit': round(items.price_unit*items.quantity,2),
                                          # 'total_cost': items.product_id.standard_price*items.quantity,
                                          'total_cost': estimated_cost_price*items.quantity,
                                          'total_amount': round(p_total_amount,2),
                                          'subtotal': items.price_subtotal,
                                          'vat': round(p_vat,2),
                                          # 'total_vat': sum(round(p_vat,2)),
                                          # 'uom':items.product_id.uom_id.name,
                                          'uom': items.product_id.prod_uom,
                                          'discount': items.discount,
                                          # 'status_data': str_state

                                          })

        return data_list

    def total_vat_sum(self, obj):

        try:
            order_numbers = [obj.split(',')[0]]
        except:
            order_numbers = [str(obj)]

        total_vat = 0.00
        total_subtotal = 0.00
        group_id = None
        data_list = []

        self.cr.execute("SELECT id FROM account_invoice WHERE origin=%s", (order_numbers))
        for item in self.cr.fetchall():
            group_id = [item[0]]

        if group_id is not None:

            order_data = self.pool.get('account.invoice').browse(self.cr, self.uid, group_id)
            if str(order_data.type) == 'out_invoice' and str(order_data.state) != 'cancel':

                account_invoice_ids = order_data.invoice_line

                for items in account_invoice_ids:

                    vat_amount = ((items.price_unit - round(items.product_id.average_purchaes_price,
                                                            2)) * items.quantity) * 0.05
                    p_vat = vat_amount if vat_amount >= 0 else 0.0

                    total_vat += p_vat
                    total_subtotal += items.price_subtotal

        return [{'total_vat': round(total_vat, 2),
                 'total_subtotal': round(total_subtotal, 2),
                 }]


class report_vat_invoice_layout(osv.AbstractModel):
    _name = 'report.sindabad_customization.report_vatinvoice'
    _inherit = 'report.abstract_report'
    _template = 'sindabad_customization.report_vatinvoice'
    _wrapped_report_class = invoicevatdata

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:

# Author Mufti Muntasir Ahmed 26-02-2018

from openerp.osv import fields, osv
import datetime


class InvoiceAgeingReportWizard(osv.osv_memory):
    """
    This wizard will provide the partner Ledger report by periods, between any two dates.
    """
    _name = 'invoice.ageing.report.wizard'
    _description = 'Invoice Ageing Report Wizard'

    _columns = {
        'date_from': fields.date("Start Date"),
        'date_to': fields.date("End Date"),
        'month1': fields.boolean("0-30", default=True),
        'month2': fields.boolean("31-60", default=True),
        'month3': fields.boolean("61-90", default=True),
        'month4': fields.boolean("91- ", default=True),

    }

    _defaults = {
        'date_from': datetime.datetime.today(),
        'date_to': datetime.datetime.today()
    }

    def _build_contexts(self, cr, uid, ids, data, context=None):
        if context is None:
            context = {}

        result = {}

        result['date_from'] = data['form']['date_from']
        result['date_to'] = data['form']['date_to']

        return result

    def check_report(self, cr, uid, ids, context=None):
        if context is None:
            context = {}

        data = {}

        data = self.read(cr, uid, ids, ['date_from', 'date_to'], context=context)[0]

        datas = {
            'ids': context.get('active_ids', []),
            'model': 'invoice.ageing.report.wizard',
            'form': data
        }
        return self.pool['report'].get_action(cr, uid, [], 'invoice_ageing_report.invoiceageing_report', data=datas, context=context)


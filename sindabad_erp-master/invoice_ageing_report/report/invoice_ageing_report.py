# Author Mufti Muntasir Ahmed 26-02-2018

import time
from openerp.report import report_sxw
from openerp.osv import fields, osv
from datetime import datetime, timedelta


class InvoiceAgeingReport(report_sxw.rml_parse):
    def __init__(self, cr, uid, name, context):
        super(InvoiceAgeingReport, self).__init__(cr, uid, name, context=context)
        self.localcontext.update({
            'get_client_order_ref': self.get_test
        })

        self.context = context

    def get_test(self):
        return 'dfdf'

    def set_context(self, objects, data, ids, report_type=None):
        ids = self.context.get('active_ids')
        inv_obj = self.pool['account.invoice']
        context = self.context
        docs = inv_obj.browse(self.cr, self.uid, ids, context)

        text = ''
        text_output = {}

        st_date = data.get('form').get('date_from')
        end_date = data.get('form').get('date_to')
        context['start_date'] = st_date
        context['end_date'] = end_date

        for invoice in docs:
            text_output[invoice.id] = text

        self.localcontext.update({
            'docs': docs,
            'time': time,
            'getLines': self._lines_get,
            'date_range': str(st_date) + ' to ' + str(end_date),
            'text': text_output
        })

        self.context = context

        self.invoice_ids = [res['partner_id'] for res in self.cr.dictfetchall()]

        # import pdb;pdb.set_trace()

        return super(InvoiceAgeingReport, self).set_context(objects, data, self.invoice_ids, report_type)

    def _lines_get(self, invoice):

        start_date = self.context.get('start_date')
        end_date = self.context.get('end_date')

        invoice_obj = self.pool['account.invoice']
        partner = invoice.partner_id

        invoice_list = []
        # import pdb;pdb.set_trace()
        if invoice.id:
            invoice_list.append(invoice.id)

            self.cr.execute("SELECT id FROM account_invoice WHERE partner_id={0} AND date_invoice>='{1}' AND date_invoice<='{2}'".format(partner.id, str(start_date), str(end_date)))

            for items in self.cr.fetchall():
                invoice_list.append(items[0])
        else:
            invoice_list.append(invoice.id)

        invoices = invoice_obj.search(self.cr, self.uid,
                                        [('id', 'in', invoice_list),
                                         ('date_invoice', '<=', end_date),
                                         ('date_invoice', '>=', start_date)
                                         ])

        invoices = invoice_obj.browse(self.cr, self.uid, invoices)

        return invoices

class invoiceageing_report(osv.AbstractModel):
    _name = 'report.invoice_ageing_report.invoiceageing_report'
    _inherit = 'report.abstract_report'
    _template = 'invoice_ageing_report.invoiceageing_report'
    _wrapped_report_class = InvoiceAgeingReport

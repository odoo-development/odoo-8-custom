# Mufti Muntasir Ahmed 6/5/2019



from openerp.report import report_sxw
from openerp.osv import fields, osv

from datetime import datetime







class invoice(osv.osv):
    _inherit = "account.invoice"

    def _len_to_30(self, cr, uid, ids, field_name, arg, context=None):
        res = {}
        xl = datetime.today()

        for inv in self.browse(cr, uid, ids, context=context):
            days=0
            ccl = inv.date_invoice

            if ccl is not False:
                ml = datetime.strptime(ccl, '%Y-%m-%d')

                days = (xl-ml).days

                if days <= 30:
                    res[inv.id] = True
                else:
                    res[inv.id] = False
            else:
                res[inv.id] = False
        return res

    def _len_to_60(self, cr, uid, ids, field_name, arg, context=None):
        res = {}
        xl = datetime.today()

        for inv in self.browse(cr, uid, ids, context=context):
            days=0
            ccl = inv.date_invoice

            if ccl is not False:
                ml = datetime.strptime(ccl, '%Y-%m-%d')

                days = (xl-ml).days

                if days > 30 and days <= 60:
                    res[inv.id] = True
                else:
                    res[inv.id] = False
            else:
                res[inv.id] = False
        return res

    def _len_to_90(self, cr, uid, ids, field_name, arg, context=None):
        res = {}
        xl = datetime.today()

        for inv in self.browse(cr, uid, ids, context=context):
            days=0
            ccl = inv.date_invoice

            if ccl is not False:
                ml = datetime.strptime(ccl, '%Y-%m-%d')

                days = (xl-ml).days

                if days > 60 and days <= 90:
                    res[inv.id] = True
                else:
                    res[inv.id] = False
            else:
                res[inv.id] = False
        return res

    def _len_to_91(self, cr, uid, ids, field_name, arg, context=None):
        res = {}
        xl = datetime.today()

        for inv in self.browse(cr, uid, ids, context=context):
            days=0
            ccl = inv.date_invoice

            if ccl is not False:
                ml = datetime.strptime(ccl, '%Y-%m-%d')

                days = (xl-ml).days

                if days > 90:
                    res[inv.id] = True
                else:
                    res[inv.id] = False
            else:
                res[inv.id] = False
        return res




    _columns = {
        '0_30': fields.function(_len_to_30, type='boolean', string='0 To 30'),
        '31_60': fields.function(_len_to_60, type='boolean', string='31 TO 60'),
        '61_90': fields.function(_len_to_90, type='boolean', string='61 TO 90'),
        '_90': fields.function(_len_to_91, type='boolean', string='>90'),
    }
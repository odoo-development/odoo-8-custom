from datetime import datetime, timedelta
import time
from datetime import date
from openerp import SUPERUSER_ID
from openerp.osv import fields, osv
from openerp.tools.translate import _
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT
import openerp.addons.decimal_precision as dp
from openerp import workflow
from openerp import api
import datetime
from openerp.exceptions import except_orm, Warning, RedirectWarning
from openerp.tools.translate import _



class res_partner(osv.osv):
    _inherit = 'res.partner'


    def _new_statment_balance(self, cr, uid, ids, name, arg, context=None):
        res = {}
        parent_list = []

        for id in ids:
            partner_obj = self.pool['res.partner']
            partner = partner_obj.browse(cr, uid, id, context)[0]

            partner_id = id
            parent_list = self.pool['res.partner'].search(cr, uid, [('id', 'child_of', partner_id)],context=context)


            start_date =datetime.datetime.today().date().strftime("%Y-%m-%d")
            moveline_obj = self.pool['account.move.line']
            movelines = moveline_obj.search(cr, uid,
                                            [('partner_id', 'in', parent_list),
                                             ('account_id.type', 'in', ['receivable', 'payable']),
                                             ('state', '<>', 'draft'), ('date', '<', start_date)])

            movelines = moveline_obj.browse(cr, uid, movelines)

            previous_balance = 0

            res[partner.id] = reduce(lambda x, y: x + (y['debit'] - y['credit']), movelines, 0)

        return res

    def _statment_balance(self, cr, uid, ids, name, arg, context=None):
        res = {}
        parent_list = []

        for id in ids:
            partner_obj = self.pool['res.partner']
            partner = partner_obj.browse(cr, uid, id, context)[0]


            if partner.parent_id:
                parent_list.append(partner.parent_id.id)

                cr.execute("select id from  res_partner where customer=True and id=%s", ([partner.parent_id.id]))

                for items in cr.fetchall():
                    parent_list.append(items[0])
            else:
                parent_list.append(partner.id)

            cr.execute("select id from  res_partner where customer=True and parent_id=%s", ([partner.id]))
            for items in cr.fetchall():
                parent_list.append(items[0])

            start_date =datetime.datetime.today().date().strftime("%Y-%m-%d")
            moveline_obj = self.pool['account.move.line']
            movelines = moveline_obj.search(cr, uid,
                                            [('partner_id', 'in', parent_list),
                                             ('account_id.type', 'in', ['receivable', 'payable']),
                                             ('state', '<>', 'draft'), ('date', '<', start_date)])

            movelines = moveline_obj.browse(cr, uid, movelines)

            previous_balance = 0

            res[partner.id] = reduce(lambda x, y: x + (y['debit'] - y['credit']), movelines, 0)

        return res

    def _invoice_balance(self, cr, uid, ids, name, arg, context=None):
        res = {}
        parent_list = []

        for id in ids:
            partner_id = id
            all_partner_ids = self.pool['res.partner'].search(cr, uid, [('id', 'child_of', partner_id)],
                                                              context=context)

            invoice_ids = self.pool['account.invoice'].search(cr, uid, [('partner_id', 'in', all_partner_ids),
                                                                        ('state', '=', 'open')],
                                                              context=context)

            invoicelines = self.pool['account.invoice'].browse(cr, uid, invoice_ids)


            inv_vals=0

            inv_l = []
            for inv_items in invoicelines:
                inv_l.append({inv_items.id:inv_items.residual})
                if inv_items.type == 'out_invoice':
                    inv_vals = inv_vals + inv_items.new_residual
                else:
                    inv_vals = inv_vals - inv_items.new_residual
            res[id]=inv_vals



        return res


    def _ledger_balance(self, cr, uid, ids, name, arg, context=None):
        res = {}
        parent_list = []

        for id in ids:
            partner_obj = self.pool['res.partner']
            partner = partner_obj.browse(cr, uid, id, context)[0]

            amount = partner.property_account_receivable.balance

            res[id]=amount


        return res

    _columns = {
        'customer_statement_balance': fields.function(_statment_balance, type="float",string='Customer Statement Balance'),
        'new_customer_statement_balance': fields.function(_new_statment_balance, type="float",string='New Customer Statement Balance'),
        'invoice_statement_balance': fields.function(_invoice_balance, type="float",string='Invoice Statement Balance'),
        'ledger_balance': fields.function(_ledger_balance, type="float",string='Ledger Balance'),


    }


class upload_documents(osv.osv):
    _name = "upload.documents"
    _description = "Upload Invoice Documents"

    _columns = {
        'name': fields.char('Order No'),
        'company_name': fields.char('Company Name'),
        'invoice_no': fields.char('Invoice No'),
        'invoice_date': fields.date('Invoice Date'),
        'order_amount': fields.float('Amount'),
        'receipt': fields.float('Received Amount'),
        'ait_receipt': fields.float('AIT Received Amount'),
        'bad_debts': fields.float('Bad debts Amount'),
        'balance': fields.float('Outstanding'),
        'calculated': fields.boolean('Already Calculated'),


    }


    def abcd(self, cr, uid, ids=None, context=None):
        opening_balance_data = [17594,585,14878,40667,426,29087,27488,23383,22227,19569,10490,10560,10563,10564,10566,10568,11310,11306,10983,10749,10670,10576,10448,598,469,10334,1288,1276,10293,10287,580,594,10263,1284,10251,10192,570,10189,10185,10184,10176,601,593,10164,602,10163,10160,608,10132,10114,10098,10082,10081,470,10039,10037,1285,10027,10022,10021,10020,480,10006,10005,1300,9984,9982,442,9947,599,605,9891,1282,9863,9858,9855,9852,604,9766,453,9752,1289,572,592,569,9728,9722,9713,9712,9705,9695,9693,9694,9692,9691,9690,1402,533,587,530,1317,1269,9627,579,9618,591,9597,9583,9573,586,1400,475,9254,462,9227,534,9136,9101,9096,9057,8988,1275,8969,8967,8951,8930,8929,8928,8926,8923,8915,8909,8905,8903,8902,8900,8899,8897,8896,8895,8892,589,8665,8600,8599,468,496,455,451,8489,447,527,8424,8418,8417,8415,8405,1293,583,8395,8382,466,8259,445,8094,1279,7888,7564,460,519,476,467,1280,1296,488,7144,7137,517,1314,1270,522,7024,463,539,1311,1302,528,6747,6746,6745,609,450,6738,6734,6733,6728,478,425,444,6416,412,410,409,406,405,404,2674,398,2664,2660,2586,394,2535,2531,2523,387,474,384]


        ananta_company_ids = [65644,16146,62933,118959,57339,52905,734,16141,29416,2496,57302,16152,57299,56015,2197,62850,2511,65512,2510,14129]


        #Update Outstanding Balances

        cr.execute("update account_invoice set new_residual=amount_total where state='open'")
        cr.commit()



        # Adjustment of Ananta Company Payments
        for partner_id in ananta_company_ids:


            partner_id=partner_id
            all_partner_ids = self.pool['res.partner'].search(cr, uid, [('id', 'child_of', partner_id)],
                                                              context=context)


            invoice_ids = self.pool['account.invoice'].search(cr, uid, [('partner_id', 'in', all_partner_ids),
                                                                        ('state', '=', 'open'),
                                                                        ('date_invoice', '<', '2019-12-16')],
                                                              context=context)
            if len(invoice_ids)>0:
                cr.execute("update account_invoice set state = 'paid', x_force_paid=TRUE WHERE id in %s", (tuple(invoice_ids),))
                cr.commit()






        #### Adjust/paid all refund invoices till 31st January 2020


        invoice_ids = self.pool['account.invoice'].search(cr, uid, [('type','=','out_refund'),
                                                                    ('state', '=', 'open'),
                                                                    ('date_invoice', '<', '2020-04-20')],
                                                          context=context)
        if len(invoice_ids) > 0:
            cr.execute("update account_invoice set state = 'paid', x_force_paid=TRUE WHERE id in %s",
                       (tuple(invoice_ids),))
            cr.commit()



        ## Ends here


        ## Remove opening invoice Balance
        for open_inv_id in opening_balance_data:
            cr.execute("update account_invoice set state = 'paid', x_force_paid=TRUE WHERE id=%s", ([open_inv_id]))
            cr.commit()



        ## Update Value
        for event in self.browse(cr, uid, ids, context=context):


            if event.calculated != True:

                order_no = event.name
                balance = event.balance
                rec_amount = event.receipt
                sale_obj = event.pool.get('sale.order')
                order_ids = sale_obj.search(cr, uid, [('client_order_ref', '=', order_no)])

                if len(order_ids)>0 and balance ==0 and event.calculated !=True: ## If Order is fully Paid
                    so_objects = sale_obj.browse(cr, uid, order_ids, context=context)

                    for so_item in so_objects:

                        for inv_items in so_item.invoice_ids:
                            cr.execute("update account_invoice set state = 'paid', x_force_paid=TRUE WHERE id=%s", ([inv_items.id]))
                            cr.execute("update upload_documents set  calculated=TRUE WHERE id=%s", ([event.id]))
                            cr.commit()
                elif len(order_ids)>0 and balance > 0: ## If order is partially Paid
                    so_objects = sale_obj.browse(cr, uid, order_ids, context=context)

                    for so_item in so_objects:

                        for inv_items in so_item.invoice_ids:
                            if inv_items.type == 'out_refund' and inv_items.state == 'open':
                                rec_amount = rec_amount + inv_items.residual

                            cr.execute("update account_invoice set state = 'paid', x_force_paid=TRUE WHERE id=%s",
                                       ([inv_items.id]))
                            cr.commit()


                        for inv_items in so_item.invoice_ids:

                            if inv_items.type == 'out_invoice' and inv_items.state == 'open':
                                if rec_amount >=inv_items.residual:
                                    rec_amount = rec_amount - inv_items.residual
                                    cr.execute("update account_invoice set state = 'paid', x_force_paid=TRUE WHERE id=%s",([inv_items.id]))
                                    cr.commit()
                                elif rec_amount >0 and inv_items.residual > rec_amount:

                                    updated_balance = inv_items.residual - rec_amount
                                    cr.execute("update account_invoice set new_residual = %s, x_force_paid=TRUE WHERE id=%s",([updated_balance,inv_items.id]))
                                    cr.commit()

                cr.execute("update upload_documents set  calculated=TRUE WHERE id=%s", ([event.id]))
                cr.commit()






        ## Ends Here







        return ''




    def invoice_map(self, cr, uid, ids=None, context=None):

        ## Here All invoice value will be updated accourding to ledgers


        partne_obj = self.pool.get('res.partner')
        opening_balance_data = [17594, 585, 14878, 40667, 426, 29087, 27488, 23383, 22227, 19569, 10490, 10560, 10563,
                                10564, 10566, 10568, 11310, 11306, 10983, 10749, 10670, 10576, 10448, 598, 469, 10334,
                                1288, 1276, 10293, 10287, 580, 594, 10263, 1284, 10251, 10192, 570, 10189, 10185, 10184,
                                10176, 601, 593, 10164, 602, 10163, 10160, 608, 10132, 10114, 10098, 10082, 10081, 470,
                                10039, 10037, 1285, 10027, 10022, 10021, 10020, 480, 10006, 10005, 1300, 9984, 9982,
                                442, 9947, 599, 605, 9891, 1282, 9863, 9858, 9855, 9852, 604, 9766, 453, 9752, 1289,
                                572, 592, 569, 9728, 9722, 9713, 9712, 9705, 9695, 9693, 9694, 9692, 9691, 9690, 1402,
                                533, 587, 530, 1317, 1269, 9627, 579, 9618, 591, 9597, 9583, 9573, 586, 1400, 475, 9254,
                                462, 9227, 534, 9136, 9101, 9096, 9057, 8988, 1275, 8969, 8967, 8951, 8930, 8929, 8928,
                                8926, 8923, 8915, 8909, 8905, 8903, 8902, 8900, 8899, 8897, 8896, 8895, 8892, 589, 8665,
                                8600, 8599, 468, 496, 455, 451, 8489, 447, 527, 8424, 8418, 8417, 8415, 8405, 1293, 583,
                                8395, 8382, 466, 8259, 445, 8094, 1279, 7888, 7564, 460, 519, 476, 467, 1280, 1296, 488,
                                7144, 7137, 517, 1314, 1270, 522, 7024, 463, 539, 1311, 1302, 528, 6747, 6746, 6745,
                                609, 450, 6738, 6734, 6733, 6728, 478, 425, 444, 6416, 412, 410, 409, 406, 405, 404,
                                2674, 398, 2664, 2660, 2586, 394, 2535, 2531, 2523, 387, 474, 384]


        ids= [65644,27654,16146,26757,16664,1598,1943,196877,5500,16030,81568,165559,47761,190348,1568,181755,301,16593,203083,59716,21325,16525,16704,72941,1583,213086,25862,16220,16481,51714,16104,98599,1204,202017,16261,221679,6423,50992,16570,119864,202266,59711,192609,16316,52896,5707,204935,16436,1177,86802,855,142342,1234,90,750,26972,16470,16061,16621,90822,118959,734,16141,29416,2496,16152,57299,2197,2511,16275,51985,1329,16278,146258,172638,31858,16623,16712,16588,5112,16480,16016,86097,5295,5765,60300,1406,916,1448,185220,51034,16344,1235,81305,5004,2538,16306,52900,16550,16251,170029,26904,189028,1227,16629,51645,16720,16489,21334,170107,58586,16552,223360,59905,144366,16496,21322,75974,813,38586,194494,779,1832,1129,2074,13517,81540,16757,38587,59906,61205,16307,16309,26766,15986,16750,61208,16429,16487,61214,171845,5764,16622,16269,61244,143952,54247,144746,38585,1612,16310,64552,994,154150,141970,36738,51029,16471,51583,222460,51168,60682,79085,515,16137,21339,16719,16131,16060,16626,5745,1188,61275,16334,16735,53960,51065,16312,220601,15960,923,28904,21308,26810,5800,183389,16774,16233,202403,124884,5768,1816,200913,99897,52908,26906,50532,16632,16673,2254,16243,16639,16248,88093,88938,16389,16303,16658,16663,45919,16454,219135,16661,2067,50200,68394,27008,141300,28972,149811,28973,52912,16672,16437,30006,16147,2078,42505,21288,28676,88534,26903,1138,16177,26779,16700,51015,2072,16031,60485,71651,69413,26824,16633,16701,16546,192941,16351,16424,16781,996,1011,23398,18280,972,1139,1077,2234,2224,2223,2219,712,5869,23382,18279,16183,5864,18275,1245,5833,18281,5828,966,5863,23392,23393,976,148766,40493,16431,14431,5903,16740,5831,16185,16780,16315,170174,16333,16698,218582,16086,2510,3474,16450,1236,204657,16533,26801,16404,58012,38361,147981,191572,16771,16737,16583,51712,16297,16642,2491,2138,16710,16497,5799,15954,16438,221174,52926,871,96149,183422,16668,50327,53055,2064,187646,16789,202804,205111,161688,1107,26778,16491,203426,141870,152019,1452,1196,87298,15866,72007,16711,86876,16163,16699,16724,16494,16456,180708,1051,1213,84775,25781,812,1117,124705,28903,16268,4128,176544,212363,16656,200746,2533,113,29031,5422,16778,28911,1239,16655,16544,16755,26817,144760,16555,203487,16317,21340,16679,48412,82048,220270,28133,16727,13429,21245,16947,55069,212609,35266,16641,16715,1033,85042,5746,16340,16532,16741,15236,222633,196923,16684,16403,15909,16125,16648,50630,1185,50629,187033,223053,16464,171721,2259,53991,16493,16736,16492,41526,16298,16277,1137,21311,16191,16388,16540,16132,16675,16650,26818,49788,50521,16286,5750,21330,16428,16542,2258,27399,16584,78856,1913,16697,191125,1746,203367,36667,58394,16299,143401,5804,173333,16192,16211,29221,29088,204871,204869,16133,17166,17169,26823,32196,16291,16193,2217,16624,1187,26909,38267,59911,16395,16556,28052,16693,16548,221100,69519,58452,38584,16708,16002,5108,43331,64521,2146,942,171151,201766,168567,5815,16195,83527,26775,1195,36876,16111,5749,16313,16482,21333,55394,2245,219777,212553,16725,49624,5498,21331,16576,159313,146864,5002,214849,1553,5464,39804,16254,64749,53905,2104,1176,142759,27645,12501,12471,146205,151716,221676,16653,16397,151947,16646,16586,16869,16714,56881,16136,223164,30046,162684,16659,16616,214457,35168,204040,16196,56060,220387,5111,16777,64558,27150,195854,32130,172682,151843,51338,16752,26659,80788,39502,16305,16674,1053,1152,16390,199968,16411,66050,20,149226,16625,29346,11407,15975,16585,16418,16620,148363,203030,16126,191749,16551,16179,58814,16651,16553,16628,21319,184353,15908,204494,16638,50993,32978,16432,1257,16452,15976,30011,52003,161384,36506,2439,222968,70732,185099,16260,59554,16545,32371,195358,2243,16706,16580,16427,16201,16660,222888,213859,16543,35874,15987,187190,428,188918,4996,16314,51331,16205,21332,16652,51963,101024,119941,29578,202088,17741,152977,212896,1990,183776,4444,30926,189253,47208,27895,204041,2047,219765,202805,219669,28851,203626,36051,203922,183755,173615,184132,5430,32322,30657,183235,210735,36551,201096,200975,200978,191318,209724,205089,2117,5563,24005,202330,201217,28268,157417,14558,193703,186513,4952,592,16495,5771,143,144051,2073,89160,16234,21328,168690,26784,49685,4151,6270,94963,195602,26907,51574,173055,505,16206,145614,38920,50522,187217,190659,597,192000,195610,16300,16039,50689,2457,221043,70818,16304,2030,47506,135539,203605,1714,5530,181111,68196,50137,51957,8323,26741,36481,220170,24165,15982,4776,54041,189807,16176,15865,200165,16276,16686,203617,16534,219763,15977,21323,31957,16019,16763,78006,16120,5995,96457,219912,16121,210785,16347,169017,15902,16293,161628,212497,11094,16383,55234,6433,16135,51545,58145,16689,85322,49193,16396,15980,751,461,16510,1646,50135,16572,85007,16020,13568,154836,1904,16302,16345,16787,1052,16092,157265,16519,1503,16635,16523,151903,13403,6193,16422,16430,64466,16164,28036,26140,191868,203379,16634,16051,16581,21316,16387,83787,711,5830,6264,81511,85467,29527,16566,1366,59960,5751,50644,84595,205179,15950,16682,100953,16259,16279,26809,142580,37256,54501,88238,61957,16579,95301,16767,16311,1466,27265,16713,2233,1192,2075,16709,21289,2065,5823,16170,16490,16730,4627,62040,585,196621,189179,50345,104750,202739,2220,16766,16582,16021,16212,5860,584,29538,617,147323,16346,174790,220148,144233,26814,2229,2071,15938,5818,172174,15941,27671,151998,29869,15942,2201,2237,15943,2235,2202,5770,16004,16008,15970,15969,15968,15967,2059,5757,5739,162955,203572,16600,16749,16688,16558,16235,145261,16649,174418,16443,431,119874,16140,219866,1546,146212,212441,186342,21108,5798,29499,88421,16607,26800,5562,183144,21335,16538,37418,16573,16756,16408,49463,16794,142914,16602,2182,199930,21423,186241,50927,50561,191624,10623,60059,51708,219878,5832,195772,16401,16267,21338,212483,163535,39126,699,159067,89913,78712,25774,27034,30110,40734,218630,5722,16791,1136,22375,26765,50872,153952,54024,68305,16647,16453,19882,11931,16182,37187,192648,31360,21324,21317,202780,1416,202385,6130,16683,16745,1949,195620,204318,16662,16175,201823,16723,146600,1911,68512,76411,16524,38278,913,77987,18523,659,26899,28974,5867,203942,186640,16091,1180,159189,16461,16467,210471,50138,183516,213799,210154,16802,29800,1184,16348,15981,16115,1407,1141,5756,15903,15904,26885,26764,184783,21341,26769,89274,16215,95631,38649,39133,190186,16084,1190,70554,31043,141394,186821,26433,24608,16318,21320,5744,23399,14142,27626,85941,64765,204616,181602,204210,64550,204670,204671,2535,100197,35519,181235,1403,13011,151838,181851,13564,221234,16026,36985,90884,125458,15891,202354,5346,153537,13031,165604,999,143919,15957,16433,16457,51399,23404,1628,26792,16444,19698,2266,36286,154608,5740,39722,210627,116768,31222,68961,235,16173,21318,26768,90719,49329,16174,16145,53887,16272,193037,29995,21329,195553,15978,16458,172168,15990,439,37186,15979,943,53890,90332,16114,16184,50141,37269,5452,16062,56726,18345,85389,221863,195098,1532,1729,26910,5829,26905,283,13841,54380,27867,2137,16264,149476,16445,26767,16779,22931,21327,21336,16685,16703,206725,69244,144,16754,59983,5769,136094,38798,2248,16405,54340,14129,16301,2512,2507,27044]

        partner_ids = ids
        partners_obj = self.pool['res.partner'].browse(cr, uid, partner_ids, context=context)

        for partner_id in partners_obj:



            partner_invoice_amount = partner_id.invoice_statement_balance
            partner_ledgers_amount = partner_id.ledger_balance

            all_partner_ids = self.pool['res.partner'].search(cr, uid, [('id', 'child_of', partner_id.id)],
                                                              context=context)


            if partner_id.property_account_receivable.id != 7074 and partner_id.is_company == True:
                if partner_ledgers_amount > 0 and partner_ledgers_amount > partner_invoice_amount:
                    invoice_ids = self.pool['account.invoice'].search(cr, uid, [('partner_id', 'in', all_partner_ids),
                                                                                ('state', '!=', 'cancel'),('state', '!=', 'draft')], context=context)

                    invoicelines = self.pool['account.invoice'].browse(cr, uid, invoice_ids)
                    adjusted_amount = abs(partner_ledgers_amount)
                    for inv_items in invoicelines:
                        if len(inv_items.payment_ids) == 0 and inv_items.type == 'out_invoice' and inv_items.id not in opening_balance_data:

                            if adjusted_amount >= inv_items.amount_total and adjusted_amount != 0:
                                adjusted_amount = adjusted_amount - inv_items.amount_total

                                cr.execute(
                                    "update account_invoice set new_residual = %s, x_force_paid=TRUE,state='open' WHERE id=%s",
                                    ([inv_items.amount_total, inv_items.id]))
                                cr.commit()
                            elif adjusted_amount < inv_items.amount_total and adjusted_amount != 0:

                                cr.execute(
                                    "update account_invoice set new_residual = %s, x_force_paid=TRUE,state='open' WHERE id=%s",
                                    ([adjusted_amount, inv_items.id]))
                                cr.commit()
                                adjusted_amount = 0
                elif partner_ledgers_amount > 0 and partner_invoice_amount > partner_ledgers_amount:  ## Case 2
                    invoice_ids = self.pool['account.invoice'].search(cr, uid, [('partner_id', 'in', all_partner_ids),
                                                                                ('state', '=', 'open')], context=context)


                    invoice_ids.sort()
                    invoicelines = self.pool['account.invoice'].browse(cr, uid, invoice_ids)

                    adjusted_amount = abs(partner_invoice_amount - partner_ledgers_amount)

                    for inv_items in invoicelines:
                        if len(inv_items.payment_ids) == 0 and inv_items.type == 'out_invoice' and inv_items.id not in opening_balance_data:

                            if adjusted_amount >= inv_items.amount_total and adjusted_amount != 0:
                                adjusted_amount = adjusted_amount - inv_items.amount_total
                                new_amount = 0

                                cr.execute(
                                    "update account_invoice set new_residual = %s, x_force_paid=TRUE,state='paid' WHERE id=%s",
                                    ([new_amount, inv_items.id]))
                                cr.commit()
                            elif adjusted_amount < inv_items.amount_total and adjusted_amount != 0:
                                balance = inv_items.amount_total - adjusted_amount
                                cr.execute(
                                    "update account_invoice set new_residual = %s, x_force_paid=TRUE,state='open' WHERE id=%s",
                                    ([balance, inv_items.id]))
                                cr.commit()
                                adjusted_amount = 0
                elif partner_ledgers_amount <=0:
                    invoice_ids = self.pool['account.invoice'].search(cr, uid, [('partner_id', 'in', all_partner_ids),
                                                                                ('state', '=', 'open')],
                                                                      context=context)
                    for inv_items in invoice_ids:
                        new_amount = 0

                        cr.execute(
                            "update account_invoice set new_residual = %s, x_force_paid=TRUE,state='paid' WHERE id=%s",
                            ([new_amount, inv_items]))
                        cr.commit()


        return ''


    def individual_cus_map(self, cr, uid, ids=None, context=None):

        if uid !=1:
            raise osv.except_osv(_('Sorry, you  are not authorized'),
                                 _('Contact with Sindabad IT team'))

        opening_balance_data = [17594, 585, 14878, 40667, 426, 29087, 27488, 23383, 22227, 19569, 10490, 10560, 10563,
                                10564, 10566, 10568, 11310, 11306, 10983, 10749, 10670, 10576, 10448, 598, 469, 10334,
                                1288, 1276, 10293, 10287, 580, 594, 10263, 1284, 10251, 10192, 570, 10189, 10185, 10184,
                                10176, 601, 593, 10164, 602, 10163, 10160, 608, 10132, 10114, 10098, 10082, 10081, 470,
                                10039, 10037, 1285, 10027, 10022, 10021, 10020, 480, 10006, 10005, 1300, 9984, 9982,
                                442, 9947, 599, 605, 9891, 1282, 9863, 9858, 9855, 9852, 604, 9766, 453, 9752, 1289,
                                572, 592, 569, 9728, 9722, 9713, 9712, 9705, 9695, 9693, 9694, 9692, 9691, 9690, 1402,
                                533, 587, 530, 1317, 1269, 9627, 579, 9618, 591, 9597, 9583, 9573, 586, 1400, 475, 9254,
                                462, 9227, 534, 9136, 9101, 9096, 9057, 8988, 1275, 8969, 8967, 8951, 8930, 8929, 8928,
                                8926, 8923, 8915, 8909, 8905, 8903, 8902, 8900, 8899, 8897, 8896, 8895, 8892, 589, 8665,
                                8600, 8599, 468, 496, 455, 451, 8489, 447, 527, 8424, 8418, 8417, 8415, 8405, 1293, 583,
                                8395, 8382, 466, 8259, 445, 8094, 1279, 7888, 7564, 460, 519, 476, 467, 1280, 1296, 488,
                                7144, 7137, 517, 1314, 1270, 522, 7024, 463, 539, 1311, 1302, 528, 6747, 6746, 6745,
                                609, 450, 6738, 6734, 6733, 6728, 478, 425, 444, 6416, 412, 410, 409, 406, 405, 404,
                                2674, 398, 2664, 2660, 2586, 394, 2535, 2531, 2523, 387, 474, 384]

        partner_ids = ids
        partners_obj = self.pool['res.partner'].browse(cr, uid, partner_ids, context=context)

        for partner_id in partners_obj:

            partner_invoice_amount = partner_id.invoice_statement_balance
            partner_ledgers_amount = partner_id.ledger_balance

            if partner_id.supplier == True:
                break

            balance = round((partner_invoice_amount-partner_ledgers_amount),2)
            if balance !=0:

                all_partner_ids = self.pool['res.partner'].search(cr, uid, [('id', 'child_of', partner_id.id)],context=context)

                if partner_id.property_account_receivable.id != 7074 and partner_id.is_company == True:
                    if partner_ledgers_amount > 0 and partner_ledgers_amount > partner_invoice_amount:
                        invoice_ids = self.pool['account.invoice'].search(cr, uid, [('partner_id', 'in', all_partner_ids),
                                                                                    ('state', '!=', 'cancel'),
                                                                                    ('state', '!=', 'draft')],
                                                                          context=context)

                        invoicelines = self.pool['account.invoice'].browse(cr, uid, invoice_ids)
                        adjusted_amount = abs(partner_ledgers_amount)
                        for inv_items in invoicelines:
                            if len(
                                    inv_items.payment_ids) == 0 and inv_items.type == 'out_invoice' and inv_items.id not in opening_balance_data:

                                if adjusted_amount >= inv_items.amount_total and adjusted_amount != 0:
                                    adjusted_amount = adjusted_amount - inv_items.amount_total

                                    cr.execute(
                                        "update account_invoice set new_residual = %s, x_force_paid=TRUE,state='open' WHERE id=%s",
                                        ([inv_items.amount_total, inv_items.id]))
                                    cr.commit()
                                elif adjusted_amount < inv_items.amount_total and adjusted_amount != 0:

                                    cr.execute(
                                        "update account_invoice set new_residual = %s, x_force_paid=TRUE,state='open' WHERE id=%s",
                                        ([adjusted_amount, inv_items.id]))
                                    cr.commit()
                                    adjusted_amount = 0
                    elif partner_ledgers_amount > 0 and partner_invoice_amount > partner_ledgers_amount:  ## Case 2
                        invoice_ids = self.pool['account.invoice'].search(cr, uid, [('partner_id', 'in', all_partner_ids),('state', '=', 'open')],context=context)

                        invoice_ids.sort()
                        invoicelines = self.pool['account.invoice'].browse(cr, uid, invoice_ids)

                        adjusted_amount = abs(partner_invoice_amount - partner_ledgers_amount)

                        for inv_items in invoicelines:
                            if len(inv_items.payment_ids) == 0 and inv_items.type == 'out_invoice' and inv_items.id not in opening_balance_data:

                                if adjusted_amount >= inv_items.amount_total and adjusted_amount != 0:
                                    adjusted_amount = adjusted_amount - inv_items.amount_total
                                    new_amount = 0

                                    cr.execute(
                                        "update account_invoice set new_residual = %s, x_force_paid=TRUE,state='paid' WHERE id=%s",
                                        ([new_amount, inv_items.id]))
                                    cr.commit()
                                elif adjusted_amount < inv_items.amount_total and adjusted_amount != 0:
                                    balance = inv_items.amount_total - adjusted_amount
                                    cr.execute(
                                        "update account_invoice set new_residual = %s, x_force_paid=TRUE,state='open' WHERE id=%s",
                                        ([balance, inv_items.id]))
                                    cr.commit()
                                    adjusted_amount = 0
                    elif partner_ledgers_amount <= 0:
                        invoice_ids = self.pool['account.invoice'].search(cr, uid, [('partner_id', 'in', all_partner_ids),
                                                                                    ('state', '=', 'open')],
                                                                          context=context)
                        for inv_items in invoice_ids:
                            new_amount = 0

                            cr.execute(
                                "update account_invoice set new_residual = %s, x_force_paid=TRUE,state='paid' WHERE id=%s",
                                ([new_amount, inv_items]))
                            cr.commit()

        return True
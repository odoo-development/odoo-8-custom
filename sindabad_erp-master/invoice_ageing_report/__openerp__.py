{
    'name': "Invoices Ageing Report Module",
    'version': '1.0',
    'category': 'Information Technology',
    'author': "Md Rockibul Alam Babu",
    'description': """
Invoices Ageing Report Module for all users.
    """,
    'website': "http://www.sindabad.com",

    'depends': [
        'base', 'account', 'report'
    ],

    'data': [
        'security/invoiceageing_security.xml',
        'security/ir.model.access.csv',
        'report/invoice_ageing_report_menu.xml',
        'report/invoice_ageing_report_view.xml',
        'wizard/invoice_ageing_report_filter_view.xml',
        'wizard/upload_cal_view.xml',
        'wizard/invoice_mapping.xml',
        'wizard/individual_customer_mapping.xml',
        'views/customer_list.xml',
        'views/upload_documents_view.xml',
        'invoice_ageing_menus.xml'
    ],
    'installable': True,
    'auto_install': False,
}
